package com.example.app5grzx.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.me.ReviewInfo;
import com.sdzn.fzx.teacher.vo.me.TellSetVo;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface TextbookView extends BaseView {
    void setReviewInfo(ReviewInfo reviewInfo);
    void setTextbookInfoVo(TextbookInfoVo textbookInfoVo);
    void setTellSetVo(TellSetVo tellSetVo);
}
