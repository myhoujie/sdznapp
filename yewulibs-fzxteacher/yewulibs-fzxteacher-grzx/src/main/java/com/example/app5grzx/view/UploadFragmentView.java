package com.example.app5grzx.view;

import com.example.app5libbase.base.BaseView;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/27
 * 修改单号：
 * 修改内容:
 */
public interface UploadFragmentView extends BaseView {

    void setProgressBar(int progress);

    void uploadSuccess();

    void uploadFailure();
}
