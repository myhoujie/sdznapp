package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.app5grzx.R;
import com.example.app5grzx.adapter.TextbookAdapter;
import com.example.app5grzx.adapter.TextbookReviewAdapter;
import com.example.app5grzx.presenter.TextbookPresenter;
import com.example.app5grzx.view.TextbookView;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.me.ReviewInfo;
import com.sdzn.fzx.teacher.vo.me.TellSetVo;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.教材信息
 */
public class TextbookFragment1 extends MBaseFragment<TextbookPresenter> implements TextbookView, View.OnClickListener {

    ListView listview;
    //https://blog.csdn.net/qq_36621990/article/details/75258065
    private FragmentManager fmanager;
    private FragmentTransaction ftransaction;
    private TextbookAdapter textbookAdapter;
    private View footview;
    private ReviewInfoAddFragment reviewInfoAddFragment;
    private TextbookInfoAddFragment textbookInfoAddFragment;
    private int containerId = R.id.framelayoutMe;
    private TextbookInfoDeleteFragment textbookInfoDeleteFragment;
    private NoScrollGridView footGridView;
    private RelativeLayout rlError;
    private ImageView errorImg;
    private ImageView excellenceImg;
    private boolean errorFlag = true;
    private boolean excellenceFlag = true;
    private String code;
    private String value;
    private HashMap<String, String> hashMap;

    public static TextbookFragment1 newInstance(Bundle bundle) {
        TextbookFragment1 fragment = new TextbookFragment1();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public TextbookFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_textbook, container, false);
        listview = view.findViewById(R.id.listview);
        rlError = (RelativeLayout) view.findViewById(R.id.rl_error);
        errorImg = (ImageView) view.findViewById(R.id.error_img);
        excellenceImg = (ImageView) view.findViewById(R.id.excellence_img);
        excellenceImg.setOnClickListener(this);
        errorImg.setOnClickListener(this);
        initView();
        return view;
    }

    private void initView() {
        footview = LayoutInflater.from(getActivity()).inflate(R.layout.footview_review, null);
        footGridView = footview.findViewById(R.id.gridView);
        listview.addFooterView(footview);

    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {

        mPresenter.getTextbookList();

//        mPresenter.getReviewInfoList();//复习

        mPresenter.TellSetList();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void initPresenter() {
        mPresenter = new TextbookPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void setReviewInfo(ReviewInfo reviewInfo) {


        TextbookReviewAdapter textbootGridViewAdapter = new TextbookReviewAdapter(reviewInfo.getData(), getActivity());
        footGridView.setAdapter(textbootGridViewAdapter);

        footview.findViewById(R.id.subject_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addReviewInfoData();
            }
        });

    }

    @Override
    public void setTextbookInfoVo(TextbookInfoVo textbookInfoVo) {
        textbookAdapter = new TextbookAdapter(textbookInfoVo.getData(), getActivity());
        listview.setAdapter(textbookAdapter);
        textbookAdapter.setAddOnClick(new TextbookAdapter.addOnClick() {
            @Override
            public void onClick(String subjectName, Integer subjectId) {
                addTextbookInfoAddFragment(subjectName, subjectId);
            }

            @Override
            public void onDetils(String subjectName, Integer subjectId, int id, Integer versionId, String versionName) {

                fmanager = getActivity().getSupportFragmentManager();
                ftransaction = fmanager.beginTransaction();
                textbookInfoDeleteFragment = TextbookInfoDeleteFragment.newInstance(null);
                Bundle bundle = new Bundle();
                bundle.putString("subjectName", subjectName);
                bundle.putString("versionName", versionName);
                bundle.putInt("subjectId", subjectId);
                bundle.putInt("versionId", versionId);
                bundle.putInt("id", id);
                textbookInfoDeleteFragment.setArguments(bundle);
                ftransaction.replace(containerId, textbookInfoDeleteFragment);
                ftransaction.addToBackStack(null);
                ftransaction.commit();
            }
        });

    }

    @Override
    public void setTellSetVo(TellSetVo tellSetVo) {
        hashMap = new HashMap<>();
        for (TellSetVo.DataBean dataBean : tellSetVo.getData()) {

            hashMap.put(dataBean.getCode(), dataBean.getId() + "");


            if (dataBean.getCode().equals("GOOD_ANSWER")) {
                if (dataBean.getValue().equals("1")) {
                    excellenceFlag = false;
                    excellenceImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }

            if (dataBean.getCode().equals("BAD_ANSWER")) {
                if (dataBean.getValue().equals("1")) {
                    errorFlag = false;
                    errorImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }
        }
    }

    private void addReviewInfoData() {

        fmanager = getActivity().getSupportFragmentManager();
        ftransaction = fmanager.beginTransaction();
        reviewInfoAddFragment = ReviewInfoAddFragment.newInstance(null);
        ftransaction.replace(containerId, reviewInfoAddFragment);
        ftransaction.addToBackStack(null);
        ftransaction.commit();

    }

    private void addTextbookInfoAddFragment(String subjectName, Integer subjectId) {

        fmanager = getActivity().getSupportFragmentManager();
        ftransaction = fmanager.beginTransaction();
        textbookInfoAddFragment = TextbookInfoAddFragment.newInstance(null);
        Bundle bundle = new Bundle();
        bundle.putString("subjectName", subjectName);
        bundle.putInt("subjectId", subjectId);
        textbookInfoAddFragment.setArguments(bundle);
        ftransaction.replace(containerId, textbookInfoAddFragment);
        ftransaction.addToBackStack(null);
        ftransaction.commit();

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        if (id == R.id.error_img) {
            code = "BAD_ANSWER";
            if (errorFlag) {
                errorImg.setImageResource(R.mipmap.xianshi_icon);
                errorFlag = false;
                value = "1";
            } else {
                errorImg.setImageResource(R.mipmap.yincang_icon);
                errorFlag = true;
                value = "0";
            }
        } else if (id == R.id.excellence_img) {
            code = "GOOD_ANSWER";
            if (excellenceFlag) {
                excellenceImg.setImageResource(R.mipmap.xianshi_icon);
                excellenceFlag = false;
                value = "1";
            } else {
                excellenceImg.setImageResource(R.mipmap.yincang_icon);
                excellenceFlag = true;
                value = "0";
            }
        }
        mPresenter.updateInfo(hashMap.get(code), value);
    }
}
