package com.example.app5grzx.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5grzx.view.ReviewInfoAddView;
import com.sdzn.fzx.teacher.vo.me.ReviewInfoAdd;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ReviewInfoAddPresenter extends BasePresenter<ReviewInfoAddView, BaseActivity> {

    public void getReviewInfoAdd(Integer levelId, Integer educationId) {
        Network.createTokenService(NetWorkService.ReviewInfoAddService.class)
                .ReviewInfoAdd(levelId, educationId)
                .map(new StatusFunc<ReviewInfoAdd>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ReviewInfoAdd>(new SubscriberListener<ReviewInfoAdd>() {
                    @Override
                    public void onNext(ReviewInfoAdd reviewInfoAdd) {
                        mView.setReviewInfoAdd(reviewInfoAdd);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void SaveReviewInfoAdd(String reviewsJSON) {
        Network.createTokenService(NetWorkService.ReviewInfoAddService.class)
                .SaveReviewInfoAdd(reviewsJSON)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object object) {
                       mView.success();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }
}
