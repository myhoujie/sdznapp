package com.example.app5grzx.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.AppUtils;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.presenter.ChangepasswordPresenter1;
import com.sdzn.fzx.teacher.view.ChangepasswordViews;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.controller.UserController;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.ClearableEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangepasswordFragment extends BaseFragment implements ChangepasswordViews, View.OnClickListener {
    ChangepasswordPresenter1 mPresenter;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private LinearLayout confirmLy;
    private TextView passwordOld;
    private ClearableEditText passwordOldEdit;
    private TextView newPasswordTx;
    private ClearableEditText newPasswordEdit;
    private TextView confirmPasswordTx;
    private ClearableEditText confirmPasswordEdit;


    private String oldPassword;
    private String newPassword;
    private String confirmPassword;


    public ChangepasswordFragment() {
        // Required empty public constructor
    }

    public static ChangepasswordFragment newInstance(Bundle bundle) {
        ChangepasswordFragment fragment = new ChangepasswordFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_changepassword, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        title = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        passwordOld = (TextView) view.findViewById(R.id.password_old);
        passwordOldEdit = (ClearableEditText) view.findViewById(R.id.password_old_edit);
        newPasswordTx = (TextView) view.findViewById(R.id.new_password_tx);
        newPasswordEdit = (ClearableEditText) view.findViewById(R.id.new_password_edit);
        confirmPasswordTx = (TextView) view.findViewById(R.id.confirm_password_tx);
        confirmPasswordEdit = (ClearableEditText) view.findViewById(R.id.confirm_password_edit);
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        initPresenter();
        return view;
    }

    public void initPresenter() {
        mPresenter = new ChangepasswordPresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (id == R.id.confirm_ly) {
            changePassword();
        }
    }


    private void changePassword() {
        oldPassword = passwordOldEdit.getText().toString();
        newPassword = newPasswordEdit.getText().toString();
        confirmPassword = confirmPasswordEdit.getText().toString();

        if (!newPassword.equals(confirmPassword)) {
            ToastUtil.showLonglToast("两次密码输入不一致");
            return;
        }
        if (!StringUtils.vertifyPsw(newPassword)) {
            ToastUtil.showLonglToast("请输入6-16位数字、字母密码");
            return;
        }
        mPresenter.changePassword("Bearer "+(String) SPUtils.getInstance().getString("token", ""),
                UserController.getLoginBean().getData().getUser().getId(), oldPassword, newPassword);
    }

    @Override
    public void changePassswordSuccess() {
        reLogin();
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    private void reLogin() {

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
        startActivity(intent);
    }


    @Override
    public String getIdentifier() {
        return null;
    }
}
