package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.DeleteVolumeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/8
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextBookDeleteInfoAdapter extends BaseAdapter {
    private List<DeleteVolumeVo.DataBean> list;
    private Context context;
    private TextBookInfoDeleteGridViewAdapter textBookInfoGridViewAdapter;

    public TextBookDeleteInfoAdapter(List<DeleteVolumeVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_add_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(list.get(i).getBaseGradeName())) {
            holder.gradeName.setText(list.get(i).getBaseGradeName());
        } else {
            holder.gradeName.setText("");
        }
        textBookInfoGridViewAdapter = new TextBookInfoDeleteGridViewAdapter(list.get(i).getVolumeList(), context);
        holder.gridView.setAdapter(textBookInfoGridViewAdapter);

        return view;
    }

    static class ViewHolder {
        TextView gradeName;
        GridView gridView;

        ViewHolder(View view) {
            gradeName = view.findViewById(R.id.grade_name);
            gridView = view.findViewById(R.id.gridView);
        }
    }
}
