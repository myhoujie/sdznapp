package com.example.app5grzx.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5grzx.view.TextbookInfoAddView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.me.VersionVo;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextbookInfoAddPresenter extends BasePresenter<TextbookInfoAddView, BaseActivity> {

    public void getTextbookInfoAdd(Map<String, Object> params) {
        Network.createTokenService(NetWorkService.GetVersionListService.class)
                .VersionList(params)
                .map(new StatusFunc<VersionVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<VersionVo>(new SubscriberListener<VersionVo>() {
                    @Override
                    public void onNext(VersionVo versionVo) {
                        mView.setVersionVo(versionVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void getVolumeList(Map<String, Object> params) {
        Network.createTokenService(NetWorkService.GetVersionListService.class)
                .volumeList(params)
                .map(new StatusFunc<VolumeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<VolumeVo>(new SubscriberListener<VolumeVo>() {
                    @Override
                    public void onNext(VolumeVo volumeVo) {
                        mView.setVolumeVo(volumeVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void saveData(Map<String, Object> params) {
        Network.createTokenService(NetWorkService.GetVersionListService.class)
                .saveData(params)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object volumeVo) {
         mView.saveDataSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ToastUtil.showLonglToast("保存失败");
                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }
}
