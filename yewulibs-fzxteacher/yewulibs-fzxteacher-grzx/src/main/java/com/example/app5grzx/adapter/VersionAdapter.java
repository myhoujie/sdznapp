package com.example.app5grzx.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.TextBookClassVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class VersionAdapter extends BaseAdapter {
    private List<TextBookClassVo> list;
    private Context context;

    public VersionAdapter(List<TextBookClassVo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.version_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.versionsName.setText(list.get(i).getName() + "");
        return view;
    }

    static class ViewHolder {
        TextView versionsName;

        ViewHolder(View view) {
            versionsName = view.findViewById(R.id.versions_name);
        }
    }
}
