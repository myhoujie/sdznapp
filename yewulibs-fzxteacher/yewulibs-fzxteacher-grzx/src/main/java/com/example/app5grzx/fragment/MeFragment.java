package com.example.app5grzx.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5grzx.R;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.UpdataPhoto;
import com.example.app5grzx.presenter.MePresenter;
import com.example.app5grzx.view.MeView;
import com.example.app5libbase.util.CircleTransform;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.views.DelActivityDialog;
import com.sdzn.fzx.teacher.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 个人中心
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends MBaseFragment<MePresenter> implements MeView {
    private TextView tvTitle;
    private RelativeLayout rlName;
    private ImageView meHead;
    private TextView meName;
    private RelativeLayout gooutRl;
    private RelativeLayout basicsData;
    private ImageView basicsImg;
    private RelativeLayout schoolData;
    private ImageView schoolImg;
    private RelativeLayout writeData;
    private ImageView writeImg;
    private RelativeLayout classData;
    private ImageView classImg;
    private RelativeLayout tellData;
    private ImageView tellImg;
    private RelativeLayout uploadData;
    private ImageView uploadImg;
    private RelativeLayout clearData;
    private ImageView clearImg;
    private RelativeLayout aboutData;
    private ImageView aboutImg;
    private FrameLayout framelayoutMe;

    private List<Fragment> fragments;
    private int containerId = R.id.framelayoutMe;
    private Fragment currFragment;
    private DelActivityDialog delActivityDialog;

    public MeFragment() {
        // Required empty public constructor
    }

    public static MeFragment newInstance(Bundle bundle) {
        MeFragment fragment = new MeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_me, container, false);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        rlName = (RelativeLayout) view.findViewById(R.id.rlName);
        meHead = (ImageView) view.findViewById(R.id.me_head);
        meName = (TextView) view.findViewById(R.id.me_name);
        gooutRl = (RelativeLayout) view.findViewById(R.id.goout_rl);
        basicsData = (RelativeLayout) view.findViewById(R.id.basics_data);
        basicsImg = (ImageView) view.findViewById(R.id.basics_img);
        schoolData = (RelativeLayout) view.findViewById(R.id.school_data);
        schoolImg = (ImageView) view.findViewById(R.id.school_img);
        writeData = (RelativeLayout) view.findViewById(R.id.write_data);
        writeImg = (ImageView) view.findViewById(R.id.write_img);
        classData = (RelativeLayout) view.findViewById(R.id.class_data);
        classImg = (ImageView) view.findViewById(R.id.class_img);
        tellData = (RelativeLayout) view.findViewById(R.id.tell_data);
        tellImg = (ImageView) view.findViewById(R.id.tell_img);
        uploadData = (RelativeLayout) view.findViewById(R.id.upload_data);
        uploadImg = (ImageView) view.findViewById(R.id.upload_img);
        clearData = (RelativeLayout) view.findViewById(R.id.clear_data);
        clearImg = (ImageView) view.findViewById(R.id.clear_img);
        aboutData = (RelativeLayout) view.findViewById(R.id.about_data);
        aboutImg = (ImageView) view.findViewById(R.id.about_img);
        framelayoutMe = (FrameLayout) view.findViewById(R.id.framelayoutMe);
        setOnClick();
        initFragment();
        initData();
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new MePresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        EventBus.getDefault().unregister(this);
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(BasicDataFragment.newInstance(null));
        fragments.add(TakeOfficeFragment.newInstance(null));
        fragments.add(TextbookFragment.newInstance(null));
        fragments.add(ClassGroupingFragment.newInstance(null));
        fragments.add(TellSetFragment.newInstance(null));
        fragments.add(UploadFragment.newInstance(null));
        fragments.add(ClearDataFragment.newInstance(null));
        fragments.add(AboutFragment.newInstance(null));
        //   fragments.add(ReviewInfoAddFragment.newInstance(null));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.replace(containerId, currFragment).commit();
    }

    private void setOnClick() {
        aboutData.setOnClickListener(new ClickListener());
        clearData.setOnClickListener(new ClickListener());
        writeData.setOnClickListener(new ClickListener());
        schoolData.setOnClickListener(new ClickListener());
        basicsData.setOnClickListener(new ClickListener());
        classData.setOnClickListener(new ClickListener());
        tellData.setOnClickListener(new ClickListener());
        gooutRl.setOnClickListener(new ClickListener());
        uploadData.setOnClickListener(new ClickListener());

    }

    class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.basics_data) {
                showAssignedFragment(0);
                basicsData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.school_data) {
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                showAssignedFragment(1);
            } else if (id == R.id.write_data) {
                showAssignedFragment(2);
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.class_data) {
                showAssignedFragment(3);
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.tell_data) {
                showAssignedFragment(4);
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.upload_data) {
                showAssignedFragment(5);
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.clear_data) {
                showAssignedFragment(6);
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (id == R.id.about_data) {
                basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                aboutData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                gooutRl.setBackgroundColor(Color.parseColor("#FFFFFF"));
                uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                showAssignedFragment(7);
            } else if (id == R.id.goout_rl) {
                ActivityGvAdapter().show();
            }

        }
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
        ft.replace(containerId, fragment);

        ft.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(Event event) {
        if (event instanceof UpdataPhoto) {
            initData();
        }
    }

    private void initData() {
        LoginBean data = SPUtils.getLoginBean();
        if (!TextUtils.isEmpty(data.getData().getUser().getRealName())) {

            meName.setText(data.getData().getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getPhoto())) {
            RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(getActivity())).placeholder(R.mipmap.tx_img);
            Glide.with(getActivity()).load(data.getData().getUser().getPhoto()).apply(error).into(meHead);
        }

    }

    public DelActivityDialog ActivityGvAdapter() {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(getActivity())
                .buildText("确定退出登录？")
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        Intent intent2 = new Intent(getActivity(), MqttService.class);
                        getActivity().stopService(intent2);// 关闭服务
                        SPUtils.clear(getContext());
                        ActivityManager.exit();
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                        startActivity(intent);
                        SPUtils.remove(activity, SPUtils.CLASS_ID);
                    }

                    @Override
                    public void onCancel() {
                    }
                });
        return delActivityDialog;
    }

}
