package com.example.app5grzx.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5grzx.PopupwindowUtils;
import com.example.app5grzx.R;
import com.example.app5grzx.presenter.BasicDataPresenter1;
import com.sdzn.fzx.teacher.view.BasicDataViews;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.controller.UserController;
import com.example.app5libbase.util.GlideImageLoader;
import com.example.app5libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.UserBean;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.controller.PhotoPickConfig;


/**
 *
 */
public class DataSafetyFragment extends BaseFragment implements BasicDataViews, View.OnClickListener {

    BasicDataPresenter1 mPresenter;
    private RelativeLayout accountRl;
    private TextView accountTx;
    private RelativeLayout phoneRl;
    private TextView phoneTx;
    private RelativeLayout passwordRl;


    private int containerId = R.id.framelayoutMe;
    private FragmentManager fmanager;
    private FragmentTransaction ftransaction;
    private ChangePhoneFragment changePhoneFragment;
    private ChangepasswordFragment changepasswordFragment;
    private static final String TAG = "DataSafetyFragment";
    private UserBean loginUserBean;
    protected static final int CHOOSE_PICTURE = 0;
    protected static final int TAKE_PICTURE = 1;
    private static final int CROP_SMALL_PICTURE = 2;
    private static final int REQUECT_CODE_SDCARD = 1000;
    private static final int REQUECT_CODE_CAMERA = 1001;
    protected static Uri tempUri;
    private PopupWindow popupWindow;
    private File takeImageFile;

    public DataSafetyFragment() {
    }

    public static DataSafetyFragment newInstance(Bundle bundle) {
        DataSafetyFragment fragment = new DataSafetyFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initPresenter();
        View view = inflater.inflate(R.layout.fragment_data_safety, container, false);

        accountRl = (RelativeLayout) view.findViewById(R.id.account_rl);
        accountTx = (TextView) view.findViewById(R.id.account_tx);
        phoneRl = (RelativeLayout) view.findViewById(R.id.phone_rl);
        phoneTx = (TextView) view.findViewById(R.id.phone_tx);
        passwordRl = (RelativeLayout) view.findViewById(R.id.password_rl);

        phoneRl.setOnClickListener(this);
        passwordRl.setOnClickListener(this);
        return view;
    }


    public void initPresenter() {
        mPresenter = new BasicDataPresenter1();
        mPresenter.onCreate(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.head_rl) {
            showChoosePicDialog(view);
        } else if (id == R.id.phone_rl) {
            changePhone();
        } else if (id == R.id.password_rl) {
            changePassword();
        }
    }


    private void setDate(UserBean userBean) {
        loginUserBean = userBean;
        if (userBean == null) {
            return;
        }

        if (!TextUtils.isEmpty(loginUserBean.getAccountId())) {
            accountTx.setText(loginUserBean.getAccountId());
        }

        if (!TextUtils.isEmpty(loginUserBean.getMobile())) {
            phoneTx.setText(loginUserBean.getMobile());
        } else {
            phoneTx.setTextColor(Color.parseColor("#FF6D4A"));
            phoneTx.setText("未绑定");
        }
    }

    private void changePhone() {
        fmanager = getActivity().getSupportFragmentManager();
        ftransaction = fmanager.beginTransaction();
        changePhoneFragment = ChangePhoneFragment.newInstance(null);
        Bundle args = new Bundle();
        if (loginUserBean.getMobile()==null||TextUtils.isEmpty(loginUserBean.getMobile())) {
            args.putString("title", "绑定手机号");
            args.putInt("flag", 0);
        } else {
            args.putString("title", "修改手机号");
            args.putInt("flag", 1);
        }

        changePhoneFragment.setArguments(args);
        ftransaction.replace(containerId, changePhoneFragment);
        ftransaction.addToBackStack(null);
        ftransaction.commit();
    }

    private void changePassword() {

        fmanager = getActivity().getSupportFragmentManager();
        ftransaction = fmanager.beginTransaction();
        changepasswordFragment = ChangepasswordFragment.newInstance(null);
        ftransaction.replace(containerId, changepasswordFragment);
        ftransaction.addToBackStack(null);
        ftransaction.commit();
    }

    public void showChoosePicDialog(View v) {

        popupWindow = PopupwindowUtils.showHeadPopupWindow(getActivity(), getActivity());
        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

        PopupwindowUtils.setonClick(new PopupwindowUtils.onClick() {
            @Override
            public void onClickPhoto() {
//                MPermissions.requestPermissions(BasicDataFragment.this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissiontest();
                popupWindow.dismiss();

            }

            @Override
            public void onClickCamera() {
//                MPermissions.requestPermissions(BasicDataFragment.this, REQUECT_CODE_CAMERA, Manifest.permission.CAMERA);
                requestPermissiontest1();
                popupWindow.dismiss();
            }
        });
    }

    @AfterPermissionGranted(REQUECT_CODE_SDCARD)
    public void requestPermissiontest() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // 已经申请过权限，做想做的事
            new PhotoPickConfig.Builder(DataSafetyFragment.this)
                    .imageLoader(new GlideImageLoader())
                    .showCamera(false)
                    .maxPickSize(1)
                    .spanCount(8)
                    .clipPhoto(false)
                    .build();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_SDCARD, perms);
        }
//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public void requestPermissiontest1() {
        String[] perms = {
                Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // 已经申请过权限，做想做的事
            toSystemCamera();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_CAMERA, perms);
        }
//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(BasicDataFragment.this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

/*
    @PermissionGrant(REQUECT_CODE_SDCARD)
    public void requestSdcardSuccess() {
        new PhotoPickConfig.Builder(BasicDataFragment.this)
                .imageLoader(new GlideImageLoader())
                .showCamera(false)
                .maxPickSize(1)
                .spanCount(8)
                .clipPhoto(false)
                .build();
    }
*/

    /* @PermissionDenied(REQUECT_CODE_SDCARD)
     public void requestSdcardFailed() {
         ToastUtil.showShortlToast("访问SD卡权限被拒绝");
     }

     @PermissionGrant(REQUECT_CODE_CAMERA)
     public void requestCameraSuccess() {
         toSystemCamera();
     }

     @PermissionDenied(REQUECT_CODE_CAMERA)
     public void requestCamerAFailed() {
         ToastUtil.showShortlToast("访问相机权限被拒绝");
     }
 */


    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard())
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            if (takeImageFile != null) {
                // 默认情况下，即不需要指定intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                // 照相机有自己默认的存储路径，拍摄的照片将返回一个缩略图。如果想访问原始图片，
                // 可以通过dat extra能够得到原始图片位置。即，如果指定了目标uri，data就没有数据，
                // 如果没有指定uri，则data就返回有数据！
                Uri uri;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    uri = Uri.fromFile(takeImageFile);
                } else {
                    /**
                     * 7.0 调用系统相机拍照不再允许使用Uri方式，应该替换为FileProvider
                     * 并且这样可以解决MIUI系统上拍照返回size为0的情况
                     */
                    uri = FileProvider.getUriForFile(activity, BuildConfig3.APPLICATION_ID + ".fileProvider", takeImageFile);
                    //加入uri权限 要不三星手机不能拍照
                    List<ResolveInfo> resInfoList = activity.getPackageManager().queryIntentActivities
                            (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        activity.grantUriPermission(packageName, uri, Intent
                                .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            }
        }
        this.startActivityForResult(takePictureIntent, TAKE_PICTURE);
    }




    @Override
    public void getUserInfo(UserBean userBean) {
        LoginBean loginBean = SPUtils.getLoginBean();
        loginBean.getData().getUser().setPhoto(userBean.getPhoto());
        SPUtils.saveLoginBean(loginBean);
        setDate(userBean);
        EventBus.getDefault().post(new UpdataPhoto());
    }

    @Override
    public void updateSuccess() {
//        mPresenter.getUserInfo("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
//                UserController.getLoginBean().getData().getUser().getId());

    }

    @Override
    public void updateFailed() {

    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getUserInfo("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
                UserController.getLoginBean().getData().getUser().getId());

    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
