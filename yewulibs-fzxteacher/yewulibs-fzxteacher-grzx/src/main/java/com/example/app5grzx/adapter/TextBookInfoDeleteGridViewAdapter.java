package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.DeleteVolumeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/8
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextBookInfoDeleteGridViewAdapter extends BaseAdapter {
    private List<DeleteVolumeVo.DataBean.VolumeListBean> list;
    private Context context;
    private boolean flag = true;

    public TextBookInfoDeleteGridViewAdapter(List<DeleteVolumeVo.DataBean.VolumeListBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_del_gridview_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(list.get(i).getBaseVolumeName())) {
            holder.viewName.setText(list.get(i).getBaseVolumeName());
        } else {
            holder.viewName.setText("");
        }
        if (list.get(i).isStatus()) {
            holder.check.setImageResource(R.mipmap.lanse_sel);
        } else {
            holder.check.setImageResource(R.mipmap.lanse_nor);
        }

        return view;
    }


    static class ViewHolder {
        ImageView check;
        TextView viewName;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            check = view.findViewById(R.id.check);
            viewName = view.findViewById(R.id.viewName);
            relativeLayout = view.findViewById(R.id.all);
        }
    }

}
