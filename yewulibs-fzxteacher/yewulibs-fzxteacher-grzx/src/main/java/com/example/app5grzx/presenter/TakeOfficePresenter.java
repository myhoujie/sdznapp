package com.example.app5grzx.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5grzx.view.TakeOfficeView;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/10
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TakeOfficePresenter extends BasePresenter<TakeOfficeView, BaseActivity> {

   public void getTakeOfficeInfo() {
        Network.createTokenService(NetWorkService.TakeOfficeService.class)
                .TakeOffice()
                .map(new StatusFunc<TakeOfficeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<TakeOfficeVo>(new SubscriberListener<TakeOfficeVo>() {
                    @Override
                    public void onNext(TakeOfficeVo takeOfficeVo) {
                        mView.setTakeOfficeData(takeOfficeVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));

    }
}
