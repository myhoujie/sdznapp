package com.example.app5grzx.fragment;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5grzx.R;
import com.example.app5grzx.presenter.AboutPresenter;
import com.example.app5grzx.view.AboutView;
import com.example.app5libbase.ai.XieYiActivity;
import com.example.app5libbase.base.MBaseFragment;
import com.haier.cellarette.libwebview.hois2.HiosHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends MBaseFragment<AboutPresenter> implements AboutView, View.OnClickListener {
    TextView versions;
    RelativeLayout yinsiRl;
    RelativeLayout yonghuRl;
    RelativeLayout checkVersions;

    public AboutFragment() {
        // Required empty public constructor
    }

    public static AboutFragment newInstance(Bundle bundle) {
        AboutFragment fragment = new AboutFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        versions = view.findViewById(R.id.versions);
        yinsiRl = view.findViewById(R.id.yinsi_rl);
        yonghuRl = view.findViewById(R.id.yonghu_rl);
        checkVersions = view.findViewById(R.id.check_versions);
        yinsiRl.setOnClickListener(this);
        yonghuRl.setOnClickListener(this);
        checkVersions.setOnClickListener(this);
        initData();
        return view;
    }

    private void initData() {
        try {
            versions.setText("当前版本：" + getVersionName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initPresenter() {
        mPresenter = new AboutPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.yonghu_rl) {
//            Intent intent=new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
//            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuContract.html");
//            startActivity(intent);
            HiosHelper.resolveAd(getActivity(), XieYiActivity.class, "http://teach.fuzhuxian.com/stuContract.html");
        } else if (id == R.id.yinsi_rl) {
            HiosHelper.resolveAd(getActivity(), XieYiActivity.class, "http://teach.fuzhuxian.com/stuPrivacyContract.html");
//            Intent intent=new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
//            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuPrivacyContract.html");
//            startActivity(intent);

        } else if (id == R.id.check_versions) {
            ToastUtils.showLong("最新版本");
        }
    }

    private String getVersionName() throws Exception {
        // 获取packagemanager的实例
        PackageManager packageManager = getActivity().getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = packageManager.getPackageInfo(getActivity().getPackageName(), 0);
        String version = packInfo.versionName;
        return version;
    }

}
