package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.example.app5grzx.R;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.ClearDataPresenter;
import com.example.app5grzx.view.ClearDataView;

import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.MProgressDialog;
import com.sdzn.fzx.student.libutils.util.FileSizeUtil;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClearDataFragment extends MBaseFragment<ClearDataPresenter> implements ClearDataView, View.OnClickListener {
    TextView cacheDataTx;
    RelativeLayout cacheRl;
    private String storePath;
    private Handler handler;

    public ClearDataFragment() {
        // Required empty public constructor
    }

    public static ClearDataFragment newInstance(Bundle bundle) {
        ClearDataFragment fragment = new ClearDataFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clear_data, container, false);
        cacheDataTx = view.findViewById(R.id.cache_data_tx);
        cacheRl = view.findViewById(R.id.cache_rl);
        cacheRl.setOnClickListener(this);
        storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "dearxy";
        initData();
        return view;
    }

    private void initData() {

        double fileOrFilesSize = 0;
        try {
            fileOrFilesSize = FileSizeUtil.getFolderSize(new File(storePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        cacheDataTx.setText(fileOrFilesSize + "M");
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ToastUtil.showShortlToast("清除缓存成功");
            }
        };
    }

    @Override
    public void initPresenter() {
        mPresenter = new ClearDataPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cache_rl) {
            final MProgressDialog mProgressDialog = MProgressDialog.newInstance(activity);
            TextView txt = (TextView) mProgressDialog.findViewById(R.id.message);
            txt.setText("正在清除缓存...");
            mProgressDialog.show();
            try {
                int i = FileSizeUtil.deleteFolderFile(storePath, true);
                if (i == 0) {
                    initData();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    /**
                     *要执行的操作
                     */
                    mProgressDialog.cancel();
                    handler.sendEmptyMessage(1);
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, 3000);
        }
    }
}