package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.ReviewInfoAdd;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ReviewInfoAddGridviewAdapter extends BaseAdapter {

    private onCheckClick onCheckClick;
    private List<ReviewInfoAdd.DataBean.ReviewListBean> reviewsBeanList;
    private boolean flag;
    private Context context;

    public ReviewInfoAddGridviewAdapter(List<ReviewInfoAdd.DataBean.ReviewListBean> reviewsBeanList, Context context) {
        this.reviewsBeanList = reviewsBeanList;
        this.context = context;
    }

    public void setOnCheckClick(ReviewInfoAddGridviewAdapter.onCheckClick onCheckClick) {
        this.onCheckClick = onCheckClick;
    }

    @Override
    public int getCount() {
        return reviewsBeanList.size();
    }

    @Override
    public Object getItem(int i) {
        return reviewsBeanList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_del_gridview_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(reviewsBeanList.get(i).getReviewTypeName())) {
            holder.viewName.setText(reviewsBeanList.get(i).getReviewTypeName());
        } else {
            holder.viewName.setText("");
        }
        final ViewHolder finalHolder = holder;
        if (reviewsBeanList.get(i).isStatus()) {
            finalHolder.check.setImageResource(R.mipmap.lanse_sel);
        } else {
            finalHolder.check.setImageResource(R.mipmap.lanse_nor);
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = reviewsBeanList.get(i).isStatus();
                if (flag) {
                    flag = false;
                    onCheckClick.setReviewType(reviewsBeanList.get(i).getReviewType());
                    finalHolder.check.setImageResource(R.mipmap.lanse_nor);
                } else {
                    finalHolder.check.setImageResource(R.mipmap.lanse_sel);
                    onCheckClick.setReviewType(reviewsBeanList.get(i).getReviewType());
                    flag = true;
                }

            }
        });
        return view;
    }


    static class ViewHolder {
        ImageView check;
        TextView viewName;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            check = view.findViewById(R.id.check);
            viewName = view.findViewById(R.id.viewName);
            relativeLayout = view.findViewById(R.id.all);
        }
    }

    public interface onCheckClick {
        void setReviewType(int reviewType);
    }
}
