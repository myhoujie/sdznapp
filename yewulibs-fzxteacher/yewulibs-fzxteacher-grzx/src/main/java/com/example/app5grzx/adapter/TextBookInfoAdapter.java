package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/24
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextBookInfoAdapter extends BaseAdapter {
    private List<VolumeVo.DataBean> list;
    private Context context;
    private TextBookInfoGridViewAdapter textBookInfoGridViewAdapter;
    private setDataClick setDataClick;

    public TextBookInfoAdapter(List<VolumeVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setDataClick(setDataClick onCheckClick) {
        this.setDataClick = onCheckClick;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_add_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(list.get(i).getGradeName())) {
            holder.gradeName.setText(list.get(i).getGradeName());
        } else {
            holder.gradeName.setText("");
        }
        textBookInfoGridViewAdapter = new TextBookInfoGridViewAdapter(list.get(i).getVolumes(), context);
        holder.gridView.setAdapter(textBookInfoGridViewAdapter);
        textBookInfoGridViewAdapter.setOnCheckClick(new TextBookInfoGridViewAdapter.onCheckClick() {
            @Override
            public void setReviewType(int textBookInfoGridViewAdapter, int VolumeId, String VolumeName) {
                setDataClick.setData(textBookInfoGridViewAdapter, VolumeId, VolumeName, list.get(i).getGradeName(), list.get(i).getGradeId());
            }

            @Override
            public void deleteData(int textBookInfoGridViewAdapter) {
                setDataClick.deleteData(textBookInfoGridViewAdapter, list.get(i).getGradeId());
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView gradeName;
        GridView gridView;

        ViewHolder(View view) {
            gradeName = view.findViewById(R.id.grade_name);
            gridView = view.findViewById(R.id.gridView);
        }
    }

    public interface setDataClick {
        void setData(int i, int VolumeId, String VolumeName, String gradeName, int id);

        void deleteData(int i, int id);
    }
}
