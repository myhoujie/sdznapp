package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.example.app5grzx.R;
import com.example.app5grzx.adapter.ClassGroupingAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.pop.ClassPop;
import com.example.app5grzx.presenter.ClassGroupingPresenter;
import com.example.app5grzx.view.ClassGroupingView;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVoListVo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.班级分组
 */
public class ClassGroupingFragment extends MBaseFragment<ClassGroupingPresenter> implements ClassGroupingView, View.OnClickListener {

    TextView tvClass1;
    RelativeLayout rlTvClass1;
    ListView listview;
    TextView empty;
    private ClassPop classPop;
    private ClassGroupingAdapter classGroupingAdapter;
    private List<SyncClassVo.DataBean> classVos;

    public static ClassGroupingFragment newInstance(Bundle bundle) {
        ClassGroupingFragment fragment = new ClassGroupingFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public ClassGroupingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_class_grouping, container, false);
//        tvClass1 = view.findViewById(R.id.tvClass1);
//        rlTvClass1 = view.findViewById(R.id.rl_tvClass1);
//        listview = view.findViewById(R.id.listview);
//        empty = view.findViewById(R.id.empty);
//        rlTvClass1.setOnClickListener(this);
//        initData();
        return view;
    }

    private void initData() {
        mPresenter.getClassList();
    }

    private void showPop(View view, OnClassClickListener onClassClickListener) {
        if (classPop == null) {
            classPop = new ClassPop(activity, onClassClickListener);
        }
        classPop.setClassVos(classVos);
        classPop.showPopupWindow(view);
    }

    @Override
    public void initPresenter() {
        mPresenter = new ClassGroupingPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rl_tvClass1) {
            showPop(tvClass1, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    tvClass1.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
                    mPresenter.getClassGroupList(dataBean.getClassId());
                }
            });
        }
    }


    @Override
    public void setClassGropingData(ClassGroupingVo classGroupingVo) {

        List<ClassGroupingVoListVo> ClassGroupingVoListVoList = new ArrayList<>();

        if (classGroupingVo.getData() == null || classGroupingVo.getData().size() == 0) {
            empty.setVisibility(View.VISIBLE);
            return;
        }

        Map<Integer, ClassGroupingVoListVo> resultMap = new LinkedHashMap<>(16);

        for (ClassGroupingVo.DataBean dataBean : classGroupingVo.getData()) {

            ClassGroupingVoListVo listVo = resultMap.get(dataBean.getClassGroupId());
            if (listVo == null) {

                listVo = new ClassGroupingVoListVo();
            }

            listVo.setClassGroupName(dataBean.getClassGroupName());
            listVo.getList().add(dataBean);
            resultMap.put(dataBean.getClassGroupId(), listVo);
        }

        for (Map.Entry<Integer, ClassGroupingVoListVo> entry : resultMap.entrySet()) {

            ClassGroupingVoListVoList.add(entry.getValue());
        }
        classGroupingAdapter = new ClassGroupingAdapter(ClassGroupingVoListVoList, getActivity());
        listview.setAdapter(classGroupingAdapter);

    }

    @Override
    public void onClassSuccessed(SyncClassVo classVo) {
        classVos = classVo.getData();
        tvClass1.setText(classVo.getData().get(0).getBaseGradeName() + classVo.getData().get(0).getClassName());
        mPresenter.getClassGroupList(classVo.getData().get(0).getClassId());
    }

}
