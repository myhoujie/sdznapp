package com.example.app5grzx.presenter;

import android.text.TextUtils;

import com.example.app5grzx.view.UploadFragmentView;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.base.BaseProgressCallBack;
import com.example.app5libbase.controller.UserController;
import com.sdzn.fzx.student.libutils.util.Log;
import com.example.app5libbase.util.SubjectSPUtils;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.vo.uploadTokenVo;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/26
 * 修改单号：
 * 修改内容:
 */
public class UploadFragmentPresenter extends BasePresenter<UploadFragmentView, BaseActivity> {

    Callback.Cancelable post = null;

    public void uploadFile(String hash, String key) {

        RequestParams params = new RequestParams(Network.BASE_UPLOAS_VIDEO);
        params.setConnectTimeout(1000000000);
        params.setReadTimeout(1000000000);
        params.addBodyParameter("key", key);
        params.addBodyParameter("hash", hash);
        params.addBodyParameter("access_token", UserController.getAccessToken());
        params.addBodyParameter("baseSubjectId", SubjectSPUtils.getCurrentSubject().getSubjectId() + "");
        params.addBodyParameter("baseSubjectName", SubjectSPUtils.getCurrentSubject().getSubjectName());
        params.addBodyParameter("resourceType", "3");
        params.addBodyParameter("subjectStructureId", "2");
        params.addBodyParameter("subjectStructureName", "新授课学案");
        params.addBodyParameter("subjectStructureIdPath", "0.1.2");
        params.addBodyParameter("subjectStructureNamePath", "学案>新授课学案");
        params.addBodyParameter("scene", "1");
        params.addBodyParameter("baseEducationId", String.valueOf(UserController.getLoginBean().getData().getUser().getBaseEducationId()));
        params.addBodyParameter("baseEducationName", "六三制");

        post = x.http().post(params, new Callback.ProgressCallback<String>() {
            @Override
            public void onSuccess(String s) {
                Log.e("上传成功" + "上传成功" + s);
                mView.uploadSuccess();
            }

            @Override
            public void onError(Throwable throwable, boolean b) {
                Log.e("上传失败" + "上传失败");
                mView.uploadFailure();
            }

            @Override
            public void onCancelled(CancelledException e) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public void onWaiting() {

            }

            @Override
            public void onStarted() {

            }

            @Override
            public void onLoading(long total, long current, boolean b) {
                Log.e("参数11111111" + "参数11111111" + "已" + (int) (current * 100 / total) + "%");
                //mView.setProgressBar((int) (current * 100 / total));
            }
        });

    }

    public void concalUpload() {
        post.cancel();
    }

    private String getKey() {
        return UUID.randomUUID().toString();
    }

    public void getQiNiuToken(final File file) {

        Log.e("file.getPath()", "file.getPath()" + file.getPath());
        Log.e("file.getName()", "file.getName()" + file.getName());
        RequestParams params = new RequestParams(Network.BASE_GET_UPLOADTOKEN);
        final String key = getKey() + "/" + file.getName();
        params.addBodyParameter("key", key);
        params.addBodyParameter("realName", file.getName());
        params.addBodyParameter("access_token", UserController.getAccessToken());
        x.http().get(params, new BaseProgressCallBack<String>() {
            @Override
            public void onSuccess(String result) {

                uploadTokenVo uploadTokenVo = GsonUtil.fromJson(result, uploadTokenVo.class);
                if (uploadTokenVo == null || uploadTokenVo.getResult() == null || TextUtils.isEmpty(uploadTokenVo.getResult().getData().getToken())) {

                    //mView.networkError("上传失败: 获取upToken失败");
                    return;
                }
                UploadManager uploadManager = new UploadManager();
                final uploadTokenVo.ResultBean resultBean = uploadTokenVo.getResult();
                byte[] bytes = getBytes(file);
                uploadManager.put(bytes, key, resultBean.getData().getToken(), new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject response) {

                        if (info.isOK()) {
                            Log.e("上传成功", "上传成功" + key);
                            try {
                                String hash = response.getString("hash");
                                uploadFile(hash, key);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            response.hashCode();

                        } else {
                            mView.uploadFailure();
                        }
                    }
                }, new UploadOptions(null, null, false,
                        new UpProgressHandler() {
                            public void progress(String key, double percent) {
                                Log.e("qiniu", key + ": " + percent);

                                mView.setProgressBar((int) (percent * 100));
                            }
                        }, null));
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }
        });
    }


    /**
     * 获得指定文件的byte数组
     */
    private byte[] getBytes(File file) {
        byte[] buffer = null;
        try {

            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }


}
