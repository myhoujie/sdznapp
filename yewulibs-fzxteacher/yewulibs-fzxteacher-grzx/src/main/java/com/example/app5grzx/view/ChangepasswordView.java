package com.example.app5grzx.view;


import com.example.app5libbase.base.BaseView;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface ChangepasswordView extends BaseView {

    void changePassswordSuccess();
}
