package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.example.app5grzx.R;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.TellSetPresenter;
import com.example.app5grzx.view.TellSetView;
import com.sdzn.fzx.teacher.vo.me.TellSetVo;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.讲评设置
 */
public class TellSetFragment extends MBaseFragment<TellSetPresenter> implements TellSetView, View.OnClickListener {
    private RelativeLayout rlError;
    private ImageView errorImg;
    private RelativeLayout rlExcellence;
    private ImageView excellenceImg;
    private RelativeLayout rlCoarse;
    private ImageView coarseImg;
    private RelativeLayout rlExcellenceTwo;
    private ImageView excellenceTwoImg;

    private boolean errorFlag = true;
    private boolean excellenceFlag = true;
    private boolean coarseFlag = true;
    private boolean excellenceTwoFlag = true;
    private String code;
    private String value;
    private HashMap<String, String> hashMap;

    public static TellSetFragment newInstance(Bundle bundle) {
        TellSetFragment fragment = new TellSetFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public TellSetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tell_set, container, false);
//        rlError = (RelativeLayout) view.findViewById(R.id.rl_error);
//        errorImg = (ImageView) view.findViewById(R.id.error_img);
//        rlExcellence = (RelativeLayout) view.findViewById(R.id.rl_excellence);
//        excellenceImg = (ImageView) view.findViewById(R.id.excellence_img);
//        rlCoarse = (RelativeLayout) view.findViewById(R.id.rl_coarse);
//        coarseImg = (ImageView) view.findViewById(R.id.coarse_img);
//        rlExcellenceTwo = (RelativeLayout) view.findViewById(R.id.rl_excellence_two);
//        excellenceTwoImg = (ImageView) view.findViewById(R.id.excellence_two_img);
//        rlError.setOnClickListener(this);
//        rlExcellence.setOnClickListener(this);
//        rlCoarse.setOnClickListener(this);
//        rlExcellenceTwo.setOnClickListener(this);
//
//        mPresenter.TellSetList();
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new TellSetPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_error) {
            code = "BAD_ANSWER";
            if (errorFlag) {
                errorImg.setImageResource(R.mipmap.xianshi_icon);
                errorFlag = false;
                value = "1";
            } else {
                errorImg.setImageResource(R.mipmap.yincang_icon);
                errorFlag = true;
                value = "0";
            }
        } else if (id == R.id.rl_excellence) {
            code = "GOOD_ANSWER";
            if (excellenceFlag) {
                excellenceImg.setImageResource(R.mipmap.xianshi_icon);
                excellenceFlag = false;
                value = "1";
            } else {
                excellenceImg.setImageResource(R.mipmap.yincang_icon);
                excellenceFlag = true;
                value = "0";
            }
        } else if (id == R.id.rl_coarse) {
            code = "BAD_SURFACE";
            if (coarseFlag) {
                coarseImg.setImageResource(R.mipmap.xianshi_icon);
                coarseFlag = false;
                value = "1";
            } else {
                coarseImg.setImageResource(R.mipmap.yincang_icon);
                coarseFlag = true;
                value = "0";
            }
        } else if (id == R.id.rl_excellence_two) {
            code = "GOOD_SURFAC";
            if (excellenceTwoFlag) {
                excellenceTwoImg.setImageResource(R.mipmap.xianshi_icon);
                excellenceTwoFlag = false;
                value = "1";
            } else {
                excellenceTwoImg.setImageResource(R.mipmap.yincang_icon);
                excellenceTwoFlag = true;
                value = "0";
            }
        }
        mPresenter.updateInfo(hashMap.get(code), value);
    }

    @Override
    public void setTellSetVo(TellSetVo tellSetVo) {
        hashMap = new HashMap<>();
        for (TellSetVo.DataBean dataBean : tellSetVo.getData()) {

            hashMap.put(dataBean.getCode(), dataBean.getId() + "");

            if (dataBean.getCode().equals("BAD_SURFACE")) {
                if (dataBean.getValue().equals("1")) {
                    coarseFlag = false;
                    coarseImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }

            if (dataBean.getCode().equals("GOOD_SURFAC")) {
                if (dataBean.getValue().equals("1")) {
                    excellenceTwoFlag = false;
                    excellenceTwoImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }

            if (dataBean.getCode().equals("GOOD_ANSWER")) {
                if (dataBean.getValue().equals("1")) {
                    excellenceFlag = false;
                    excellenceImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }

            if (dataBean.getCode().equals("BAD_ANSWER")) {
                if (dataBean.getValue().equals("1")) {
                    errorFlag = false;
                    errorImg.setImageResource(R.mipmap.xianshi_icon);
                }
            }
        }
    }


}
