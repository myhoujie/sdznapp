package com.example.app5grzx.ai;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5grzx.R;
import com.example.app5grzx.fragment.AboutFragment;
import com.example.app5grzx.fragment.BasicDataFragment;
import com.example.app5grzx.fragment.ClassGroupingFragment;
import com.example.app5grzx.fragment.ClearDataFragment;
import com.example.app5grzx.fragment.DataSafetyFragment;
import com.example.app5grzx.fragment.TakeOfficeFragment;
import com.example.app5grzx.fragment.TellSetFragment;
import com.example.app5grzx.fragment.TextbookFragment;
import com.example.app5grzx.fragment.UploadFragment;
import com.example.app5grzx.presenter.MePresenter;
import com.example.app5grzx.view.MeView;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.util.CircleTransform;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.UpdataPhoto;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MeActivity extends MBaseActivity<MePresenter> implements MeView, View.OnClickListener {

    private TextView tvTitle;
    private RelativeLayout rlName;
    private ImageView meHead;
    private TextView meName;
    private TextView meSchool;
    private RelativeLayout gooutRl;
    private RelativeLayout basicsData;
    private RelativeLayout dataSafety;
    private ImageView basicsImg;
    private ImageView safetyImg;
    private RelativeLayout schoolData;
    private ImageView schoolImg;
    private RelativeLayout writeData;
    private ImageView writeImg;
    private RelativeLayout classData;
    private ImageView classImg;
    private RelativeLayout tellData;
    private ImageView tellImg;
    private RelativeLayout uploadData;
    private ImageView uploadImg;
    private RelativeLayout clearData;
    private ImageView clearImg;
    private RelativeLayout aboutData;
    private ImageView aboutImg;
    private FrameLayout framelayoutMe;
    private LinearLayout titleBackLy;
    private TextView tvmytitle;
    private TextView basicsTv;
    private TextView safetyTv;
    private TextView schoolTv;
    private TextView writeTv;
    private TextView clearTv;
    private TextView aboutTv;

    CustomDrawerPopupView customDrawerPopupView;//右侧抽屉

    private List<Fragment> fragments;
    private int containerId = R.id.framelayoutMe;
    private Fragment currFragment;
    private DelActivityDialog delActivityDialog;
    private int defId = 0;

    @Override
    public void initPresenter() {
        mPresenter = new MePresenter();
        mPresenter.attachView(this, activity);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_me);
        customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {

            }
        });
        defId = getIntent().getIntExtra("show", 0);
        initView();
        initFragment();
        initData();
    }

    public void initView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rlName = (RelativeLayout) findViewById(R.id.rlName);
        meHead = (ImageView) findViewById(R.id.me_head);
        meName = (TextView) findViewById(R.id.me_name);
        meSchool = (TextView) findViewById(R.id.me_school);
        gooutRl = (RelativeLayout) findViewById(R.id.goout_rl);
        basicsData = (RelativeLayout) findViewById(R.id.basics_data);
        dataSafety = (RelativeLayout) findViewById(R.id.data_safety);
        basicsImg = (ImageView) findViewById(R.id.basics_img);
        basicsTv = (TextView) findViewById(R.id.basics_tv);
        safetyImg = (ImageView) findViewById(R.id.safety_img);
        safetyTv = (TextView) findViewById(R.id.safety_tv);
        schoolData = (RelativeLayout) findViewById(R.id.school_data);
        schoolImg = (ImageView) findViewById(R.id.school_img);
        schoolTv = (TextView) findViewById(R.id.school_tv);
        writeData = (RelativeLayout) findViewById(R.id.write_data);
        writeImg = (ImageView) findViewById(R.id.write_img);
        writeTv = (TextView) findViewById(R.id.write_tv);
        classData = (RelativeLayout) findViewById(R.id.class_data);
        classImg = (ImageView) findViewById(R.id.class_img);
        tellData = (RelativeLayout) findViewById(R.id.tell_data);
        tellImg = (ImageView) findViewById(R.id.tell_img);
        uploadData = (RelativeLayout) findViewById(R.id.upload_data);
        uploadImg = (ImageView) findViewById(R.id.upload_img);
        clearData = (RelativeLayout) findViewById(R.id.clear_data);
        clearImg = (ImageView) findViewById(R.id.clear_img);
        clearTv = (TextView) findViewById(R.id.clear_tv);
        aboutData = (RelativeLayout) findViewById(R.id.about_data);
        aboutImg = (ImageView) findViewById(R.id.about_img);
        aboutTv = (TextView) findViewById(R.id.about_tv);
        framelayoutMe = (FrameLayout) findViewById(R.id.framelayoutMe);
        titleBackLy = (LinearLayout) findViewById(R.id.title_back_ly);
        tvmytitle = findViewById(R.id.tv_my_title);
        tvmytitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(MeActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(customDrawerPopupView)
                        .show();
            }
        });

        setOnClick();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(BasicDataFragment.newInstance(null));
        fragments.add(DataSafetyFragment.newInstance(null));
        fragments.add(TakeOfficeFragment.newInstance(null));
        fragments.add(TextbookFragment.newInstance(null));
        fragments.add(ClassGroupingFragment.newInstance(null));//*
        fragments.add(TellSetFragment.newInstance(null));//*
        fragments.add(UploadFragment.newInstance(null));//*
        fragments.add(ClearDataFragment.newInstance(null));
        fragments.add(AboutFragment.newInstance(null));
        //   fragments.add(ReviewInfoAddFragment.newInstance(null));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(defId);
        ft.replace(containerId, currFragment).commit();
        if (8 == defId) {
            initBgAndIc();
            aboutData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            aboutImg.setImageResource(R.mipmap.grzx_about);
            aboutTv.setTextColor(Color.parseColor("#FA541C"));
        }
    }

    private void setOnClick() {
        aboutData.setOnClickListener(this);
        clearData.setOnClickListener(this);
        writeData.setOnClickListener(this);
        schoolData.setOnClickListener(this);
        basicsData.setOnClickListener(this);
        dataSafety.setOnClickListener(this);
        classData.setOnClickListener(this);
        tellData.setOnClickListener(this);
        gooutRl.setOnClickListener(this);
        uploadData.setOnClickListener(this);
        titleBackLy.setOnClickListener(this);

    }


    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
        ft.replace(containerId, fragment);

        ft.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(Event event) {
        if (event instanceof UpdataPhoto) {
            initData();
        }
    }

    public void initData() {
        LoginBean data = SPUtils.getLoginBean();
        if (!TextUtils.isEmpty(data.getData().getUser().getRealName())) {

            meName.setText(data.getData().getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getCustomerSchoolName())){
            meSchool.setText(data.getData().getUser().getCustomerSchoolName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getPhoto())) {
            RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(this)).placeholder(R.mipmap.tx_img);
            Glide.with(this).load(data.getData().getUser().getPhoto()).apply(error).into(meHead);
        }

    }

    public DelActivityDialog ActivityGvAdapter() {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(MeActivity.this)
                .buildText("确定退出登录？")
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        Intent intent2 = new Intent(MeActivity.this, MqttService.class);
                        MeActivity.this.stopService(intent2);// 关闭服务
//                        SPUtils.clear(App2.get());
                        ActivityManager.exit();
                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                        startActivity(intent);
                        SPUtils.remove(App2.get(), SPUtils.CLASS_ID);
                    }

                    @Override
                    public void onCancel() {
                    }
                });
        return delActivityDialog;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.basics_data) {
            initBgAndIc();
            showAssignedFragment(0);
//            basicsData.setBackgroundColor(Color.parseColor("#EDF5FF"));
            basicsData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            basicsImg.setImageResource(R.mipmap.grzx_ziliao);
            basicsTv.setTextColor(Color.parseColor("#FA541C"));
        } else if (id == R.id.data_safety) {
            initBgAndIc();
            dataSafety.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            safetyImg.setImageResource(R.mipmap.grzx_zhaq);
            safetyTv.setTextColor(Color.parseColor("#FA541C"));
            showAssignedFragment(1);
        } else if (id == R.id.school_data) {
            initBgAndIc();
            schoolData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            schoolImg.setImageResource(R.mipmap.grzx_renzhi);
            schoolTv.setTextColor(Color.parseColor("#FA541C"));
            showAssignedFragment(2);
        } else if (id == R.id.write_data) {
            initBgAndIc();
            showAssignedFragment(3);
            writeData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            writeImg.setImageResource(R.mipmap.grzx_jiaocai);
            writeTv.setTextColor(Color.parseColor("#FA541C"));

        } else if (id == R.id.class_data) {
            initBgAndIc();
            showAssignedFragment(4);
            classData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);

        } else if (id == R.id.tell_data) {
            initBgAndIc();
            showAssignedFragment(5);
            tellData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);

        } else if (id == R.id.upload_data) {
            initBgAndIc();
            showAssignedFragment(6);
            uploadData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);

        } else if (id == R.id.clear_data) {
            initBgAndIc();
            showAssignedFragment(7);
            clearData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            clearImg.setImageResource(R.mipmap.grzx_qingchu);
            clearTv.setTextColor(Color.parseColor("#FA541C"));
        } else if (id == R.id.about_data) {
            initBgAndIc();
            showAssignedFragment(8);
            aboutData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            aboutImg.setImageResource(R.mipmap.grzx_about);
            aboutTv.setTextColor(Color.parseColor("#FA541C"));

        } else if (id == R.id.goout_rl) {
            ActivityGvAdapter().show();
        } else if (id == R.id.title_back_ly) {
            finish();
        }

    }

    private void initBgAndIc() {
        basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        dataSafety.setBackgroundColor(Color.parseColor("#FFFFFF"));
        schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        classData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        tellData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        gooutRl.setBackgroundColor(Color.parseColor("#FFFFFF"));
        uploadData.setBackgroundColor(Color.parseColor("#FFFFFF"));

        basicsImg.setImageResource(R.mipmap.grzx_ziliao_normal);
        safetyImg.setImageResource(R.mipmap.grzx_zhaq_normal);
        schoolImg.setImageResource(R.mipmap.grzx_renzhi_normal);
        writeImg.setImageResource(R.mipmap.grzx_jiaocai_normal);
        clearImg.setImageResource(R.mipmap.grzx_qingchu_normal);
        aboutImg.setImageResource(R.mipmap.grzx_about_normal);

        basicsTv.setTextColor(Color.parseColor("#323C47"));
        safetyTv.setTextColor(Color.parseColor("#323C47"));
        schoolTv.setTextColor(Color.parseColor("#323C47"));
        writeTv.setTextColor(Color.parseColor("#323C47"));
        clearTv.setTextColor(Color.parseColor("#323C47"));
        aboutTv.setTextColor(Color.parseColor("#323C47"));

    }
}
