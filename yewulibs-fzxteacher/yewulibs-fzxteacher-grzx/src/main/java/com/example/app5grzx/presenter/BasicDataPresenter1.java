package com.example.app5grzx.presenter;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

import com.sdzn.fzx.teacher.view.BasicDataViews;
import com.example.app5libbase.controller.UserController;
import com.example.app5libbase.util.ProgressDialogManager;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.api.ApiInterface;
import com.sdzn.fzx.teacher.vo.QiNiuUrlVo;
import com.sdzn.fzx.teacher.vo.UserBean;
import com.sdzn.fzx.teacher.vo.chatroom.ImageUploadInfoBean;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rain.coder.photopicker.bean.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class BasicDataPresenter1 extends Presenter<BasicDataViews> {
    private String tokenStr, idStr;

    public void getUserInfo(String token, String id) {
        tokenStr = token;
        idStr = id;
        RetrofitNetNew.build(Api.class, getIdentifier())
                .teacherInfo(token, id)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<UserBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<UserBean>> call, Response<ResponseSlbBean1<UserBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        getView().getUserInfo(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<UserBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().updateFailed();
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    private void uploadHead(String url) {

        RetrofitNetNew.build(Api.class, getIdentifier())
                .updateAvatar(tokenStr, idStr, url)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        getView().updateSuccess();
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().updateFailed();
                        t.printStackTrace();
                        call.cancel();
                    }
                });

    }

//    private String urlStr = "";
//
//    private void getUrlStr(UploadPicVo mUploadPicVo) {
//        if (mUploadPicVo.getData().size() > 0) {
//            urlStr = mUploadPicVo.getData().get(0).getOriginalPath();
//            uploadHead(urlStr);
//        }
//
//    }

    private String key = "";

    /**
     * 上传七牛 获取地址
     */
    public void saveHead(final File url, Activity activity) {
        String path = "";
        path = url.getPath();
        final List<Photo> photoLists = new ArrayList<>();
        Photo photo = new Photo();
        photo.setPath(path);
        photoLists.add(photo);
        final ProgressDialogManager pdm = new ProgressDialogManager(activity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("");//压缩中...
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
                RequestParams params = new RequestParams("http://49.4.1.81:7799" + ApiInterface.QINIU_TOKEN);
                key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    ResponseSlbBean1 responseSlbBean1 = GsonUtil.fromJson(result, ResponseSlbBean1.class);
                    if (responseSlbBean1 == null || responseSlbBean1.getResult() == null) {
                        throw new RuntimeException("");//上传失败: 获取upToken失败
                    }
                    return Observable.just(new ImageUploadInfoBean(key, responseSlbBean1.getResult().toString(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        getView().onFailed(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
//                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    Log.e("上传成功", "ResponseInfo" + info);
                                    Log.e("上传成功", "JSONObject" + response);
//                                    //imageView2/2/w/100/h/100/q/75|imageslim
//                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                    pdm.cancelWaiteDialog();

                                    getQiNiuUrl(key);
//                                }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    getView().onFailed(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }


    private void getQiNiuUrl(String key) {
        com.alibaba.fastjson.JSONObject requestData = new com.alibaba.fastjson.JSONObject();
        requestData.put("fileName", key);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .getQiNiuUrl(tokenStr, requestBody)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<QiNiuUrlVo>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<QiNiuUrlVo>> call, Response<ResponseSlbBean1<QiNiuUrlVo>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }

                        uploadHead(response.body().getResult().getFileUrl());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<QiNiuUrlVo>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }

                        t.printStackTrace();
                        call.cancel();
                    }
                });

    }


    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
}
