package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.app5grzx.R;
import com.example.app5grzx.adapter.TextBookInfoAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.TextbookInfoAddPresenter;
import com.example.app5grzx.view.TextbookInfoAddView;
import com.example.app5grzx.PopupwindowUtils;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.me.TextBookClassVo;
import com.sdzn.fzx.teacher.vo.me.VersionVo;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TextbookInfoAddFragment extends MBaseFragment<TextbookInfoAddPresenter> implements TextbookInfoAddView, View.OnClickListener {


    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private LinearLayout confirmLy;
    private TextView levelName;
    private RelativeLayout subjectRl;
    private TextView subjectName_tv;
    private RelativeLayout rlTextbook;
    private TextView textbookName;
    private RelativeLayout rlEductional;
    private TextView eductionalName;
    private ListView listview;


    public TextbookInfoAddFragment() {
        // Required empty public constructor
    }

    private Integer baseVersionId;
    private String baseVersionName;
    private List<TextBookClassVo> subjectList;
    private PopupWindow popupWindow;
    private List<TextBookClassVo> versionVoList;
    private TextBookInfoAdapter textBookInfoAdapter;
    private JSONArray reviewsJSONArray;
    LoginBean.DataBean.UserBean user;

    private int subjectId;
    private int startGradeId;
    private String startGradeName;
    private String subjectName;
    private Map<Integer, Object> paramsMap;

    public static TextbookInfoAddFragment newInstance(Bundle bundle) {
        TextbookInfoAddFragment fragment = new TextbookInfoAddFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_textbook_info_add, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        title = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        levelName = (TextView) view.findViewById(R.id.level_name);
        subjectRl = (RelativeLayout) view.findViewById(R.id.subject_rl);
        subjectName_tv = (TextView) view.findViewById(R.id.subject_name);
        rlTextbook = (RelativeLayout) view.findViewById(R.id.rl_textbook);
        textbookName = (TextView) view.findViewById(R.id.textbook_name);
        rlEductional = (RelativeLayout) view.findViewById(R.id.rl_eductional);
        eductionalName = (TextView) view.findViewById(R.id.eductional_name);
        listview = (ListView) view.findViewById(R.id.listview);
        subjectName_tv.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        rlTextbook.setOnClickListener(this);
        rlEductional.setOnClickListener(this);
        subjectRl.setOnClickListener(this);
        titleBackLy.setOnClickListener(this);
        initData();
        initView();
        return view;
    }

    private void initData() {

        Bundle arguments = getArguments();
        subjectId = arguments.getInt("subjectId");
        subjectName = arguments.getString("subjectName");
        user = SPUtils.getLoginBean().getData().getUser();
        paramsMap = new HashMap<>();
        reviewsJSONArray = new JSONArray();

        mPresenter.getTextbookInfoAdd(buildTextbookInfoParams());
    }

    private Map<String, Object> buildTextbookInfoParams() {
        Map<String, Object> params = new HashMap<>();
        params.put("customerSchoolId", user.getCustomerSchoolId());
        params.put("scope", user.getScope());
        params.put("baseSubjectId", subjectId);
        return params;
    }

    private void getVolumeList(String versionId) {

        mPresenter.getVolumeList(buildVolumeParams(versionId));
        subjectList = buildSubjectList();
    }

    private Map<String, Object> buildVolumeParams(String versionId) {

        Map<String, Object> volumeParams = new HashMap<>();
        volumeParams.put("subjectId", subjectId);
        volumeParams.put("versionId", versionId);
        volumeParams.put("educationId", user.getBaseEducationId());
        volumeParams.put("levelId", user.getBaseLevelId());
        return volumeParams;
    }

    private void initView() {

        if (!TextUtils.isEmpty(user.getBaseEducationName())) {
            eductionalName.setText(user.getBaseEducationName());
        }

        if (!TextUtils.isEmpty(user.getBaseLevelName())) {
            levelName.setText(user.getBaseLevelName());
        }

        subjectName_tv.setText(subjectName);
    }

    private List<TextBookClassVo> buildSubjectList() {
        subjectList = new ArrayList<>();

        TextBookClassVo textBookClassVoOne = new TextBookClassVo();
        textBookClassVoOne.setName("一年级起点");
        textBookClassVoOne.setId(1);
        TextBookClassVo textBookClassVoTwo = new TextBookClassVo();
        textBookClassVoTwo.setName("三年级起点");
        textBookClassVoTwo.setId(3);
        subjectList.add(textBookClassVoOne);
        subjectList.add(textBookClassVoTwo);
        return subjectList;
    }

    @Override
    public void initPresenter() {
        mPresenter = new TextbookInfoAddPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (id == R.id.confirm_ly) {
            confirmData(user.getBaseEducationId(), user.getBaseEducationName(), subjectId, subjectName, baseVersionId, baseVersionName, user.getBaseLevelId());
        } else if (id == R.id.subject_rl) {
            if (subjectId == 3) {
                setPopupWindowData(subjectList, 500, subjectName_tv);
            }
        } else if (id == R.id.rl_textbook) {
            setPopupWindowData(versionVoList, 800, textbookName);
        } else if (id == R.id.rl_eductional) {
        }
    }

    @Override
    public void setVersionVo(VersionVo versionVo) {
        versionVoList = new ArrayList<>();
        getVolumeList(versionVo.getData().get(0).getBaseVersionId() + "");
        baseVersionId = versionVo.getData().get(0).getBaseVersionId();
        baseVersionName = versionVo.getData().get(0).getBaseVersionName();
        textbookName.setText(versionVo.getData().get(0).getBaseVersionName());
        for (VersionVo.DataBean databean : versionVo.getData()) {
            if (!TextUtils.isEmpty(databean.getBaseVersionName())) {
                TextBookClassVo textBookClassVo = new TextBookClassVo();
                textBookClassVo.setName(databean.getBaseVersionName());
                textBookClassVo.setId(databean.getBaseVersionId());
                versionVoList.add(textBookClassVo);
            }
        }
    }

    @Override
    public void setVolumeVo(VolumeVo volumeVo) {
        textBookInfoAdapter = new TextBookInfoAdapter(volumeVo.getData(), getActivity());
        listview.setAdapter(textBookInfoAdapter);
        textBookInfoAdapter.setDataClick(new TextBookInfoAdapter.setDataClick() {
            @Override
            public void setData(int i, int VolumeId, String VolumeName, String gradeName, int id) {
                JSONObject reviewsJSONObject = new JSONObject();
                try {
                    reviewsJSONObject.put("baseGradeName", gradeName);
                    reviewsJSONObject.put("baseGradeId", id);
                    reviewsJSONObject.put("baseVolumeId", VolumeId);
                    reviewsJSONObject.put("baseVolumeName", VolumeName);
                    reviewsJSONObject.put("status", true);

                    paramsMap.put(i + id, reviewsJSONObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void deleteData(int i, int id) {
                paramsMap.remove(i + id);
//                paramsMap.put(i + id, null);
            }
        });
    }

    @Override
    public void saveDataSuccess() {
        getFragmentManager().popBackStack();
    }

    private void setPopupWindowData(List<TextBookClassVo> list, int higt, final TextView textView) {
        popupWindow = PopupwindowUtils.bottomPopupWindow(getActivity(), getActivity(), list, higt);
        popupWindow.showAtLocation(textView, Gravity.BOTTOM, 0, 0);
        PopupwindowUtils.SetOnBottomDialogClick(new PopupwindowUtils.onBottomDialogClick() {
            @Override
            public void onClickCancel() {
            }

            @Override
            public void onClickListViwe(String listItem, int id) {
                if (!TextUtils.isEmpty(listItem)) {
                    textView.setText(listItem);
                    baseVersionId = id;
                    baseVersionName = listItem;
                    startGradeId = id;
                    startGradeName = listItem;
                    getVolumeList(baseVersionId + "");
                }
            }
        });
    }

    private void confirmData(Integer baseEducationId, String baseEducationName, Integer baseSubjectId, String baseSubjectName, Integer baseVersionId, String baseVersionName, Integer baseLevelId) {

        Map<String, Object> params = new HashMap<>();
        params.put("baseEducationId", baseEducationId);
        params.put("baseLevelId", baseLevelId);
        params.put("baseEducationName", baseEducationName);
        params.put("baseSubjectId", baseSubjectId);
        params.put("baseSubjectName", baseSubjectName);
        params.put("baseVersionId", baseVersionId);
        params.put("baseVersionName", baseVersionName);
        if (baseSubjectId == 3) {
            if (startGradeName == null) {
                ToastUtil.showLonglToast("请选择起点");
                return;
            } else {
                params.put("startGradeId", startGradeId);
                params.put("startGradeName", startGradeName);
            }

        }
        JSONArray jsonArray = buildReviewsJsonArray();
        if (jsonArray.length() == 0) {
            ToastUtil.showLonglToast("请选择教材信息");
            return;
        } else {
            params.put("volumesJSON", jsonArray);
        }

        mPresenter.saveData(params);
    }

    private JSONArray buildReviewsJsonArray() {
        for (Map.Entry<Integer, Object> entry : paramsMap.entrySet()) {
            if (entry.getValue() != null) {
                reviewsJSONArray.put(entry.getValue());
            }
        }
        return reviewsJSONArray;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

}
