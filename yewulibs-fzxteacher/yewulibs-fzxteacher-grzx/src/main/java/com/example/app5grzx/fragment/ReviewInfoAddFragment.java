package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.app5grzx.R;
import com.example.app5grzx.adapter.ReviewInfoAddAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.ReviewInfoAddPresenter;
import com.example.app5grzx.view.ReviewInfoAddView;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.me.ReviewInfoAdd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} 复习信息添加
 */
public class ReviewInfoAddFragment extends MBaseFragment<ReviewInfoAddPresenter> implements ReviewInfoAddView, View.OnClickListener {


    TextView titleBackTxt;
    LinearLayout titleBackLy;
    TextView title;
    LinearLayout confirmLy;
    ListView listview;

    private ReviewInfoAddAdapter reviewInfoAddAdapter;
    private JSONArray reviewsJSONArray;
    private List<ReviewInfoAdd.DataBean.ReviewListBean> Jsonlist;
    private LoginBean.DataBean.UserBean user;

    public ReviewInfoAddFragment() {
        // Required empty public constructor
    }

    public static ReviewInfoAddFragment newInstance(Bundle bundle) {
        ReviewInfoAddFragment fragment = new ReviewInfoAddFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review_info_add, container, false);
        titleBackTxt = view.findViewById(R.id.title_back_txt);
        titleBackLy = view.findViewById(R.id.title_back_ly);
        title = view.findViewById(R.id.title);
        confirmLy = view.findViewById(R.id.confirm_ly);
        listview = view.findViewById(R.id.listview);
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        Jsonlist = new ArrayList<>();
        initData();
        return view;
    }

    private void initData() {
        reviewsJSONArray = new JSONArray();
        user = SPUtils.getLoginBean().getData().getUser();
        mPresenter.getReviewInfoAdd(user.getBaseLevelId(), user.getBaseEducationId());

    }

    @Override
    public void initPresenter() {
        mPresenter = new ReviewInfoAddPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (id == R.id.confirm_ly) {
            buildJson(Jsonlist);
        }
    }

    @Override
    public void setReviewInfoAdd(final ReviewInfoAdd reviewInfoAdd) {

        reviewInfoAddAdapter = new ReviewInfoAddAdapter(reviewInfoAdd.getData(), getActivity());
        listview.setAdapter(reviewInfoAddAdapter);

        for (ReviewInfoAdd.DataBean data : reviewInfoAdd.getData()) {
            for (int i = 0; i < data.getReviewList().size(); i++) {
                Jsonlist.add(data.getReviewList().get(i));
            }
        }

        reviewInfoAddAdapter.setDataClick(new ReviewInfoAddAdapter.setDataClick() {
            @Override
            public void setData(int reviewType, String gradeName, int id) {
                buildParams(id, reviewType);
            }
        });
    }

    @Override
    public void success() {
        getFragmentManager().popBackStack();
    }

    private void buildParams(Integer baseGradeId, Integer reviewType) {

        for (int i = 0; i < Jsonlist.size(); i++) {

            if (baseGradeId == Jsonlist.get(i).getBaseGradeId() && reviewType == Jsonlist.get(i).getReviewType()) {
                if (Jsonlist.get(i).isStatus() == true) {
                    Jsonlist.get(i).setStatus(false);
                } else {
                    Jsonlist.get(i).setStatus(true);
                }

            } else {
                Jsonlist.get(i).setStatus(Jsonlist.get(i).isStatus());
            }

        }

    }

    private void buildJson(List<ReviewInfoAdd.DataBean.ReviewListBean> list) {

        for (int i = 0; i < list.size(); i++) {
            JSONObject reviewsJSONObject = new JSONObject();
            try {
                reviewsJSONObject.put("baseGradeId", list.get(i).getBaseGradeId());
                if (!TextUtils.isEmpty(list.get(i).getBaseGradeName())) {
                    reviewsJSONObject.put("baseGradeName", list.get(i).getBaseGradeName());
                } else {
                    reviewsJSONObject.put("baseGradeName", list.get(i).getReviewTypeName());
                }
                reviewsJSONObject.put("reviewType", list.get(i).getReviewType());
                reviewsJSONObject.put("reviewTypeName", list.get(i).getReviewTypeName());
                reviewsJSONObject.put("status", list.get(i).isStatus());
                reviewsJSONArray.put(reviewsJSONObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mPresenter.SaveReviewInfoAdd(reviewsJSONArray.toString());
        try {
            reviewsJSONArray = new JSONArray("[]");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
