package com.example.app5grzx.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.TextbookInfoVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextbootGridViewAdapter extends BaseAdapter {

    private List<TextbookInfoVo.DataBean.VolumeListBean> dataBeanList;
    private Context context;

    public TextbootGridViewAdapter(List<TextbookInfoVo.DataBean.VolumeListBean> dataBeanList, Context context) {
        this.dataBeanList = dataBeanList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataBeanList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataBeanList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.textbook_gridview_itme, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(dataBeanList.get(i).getBaseVersionName() + "-" + dataBeanList.get(i).getBaseGradeName() + dataBeanList.get(i).getBaseVolumeName());
        holder.name.setSelected(true);
        return view;
    }


    static class ViewHolder {
        TextView name;

        ViewHolder(View view) {
            name = view.findViewById(R.id.name);
        }
    }
}
