package com.example.app5grzx.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5grzx.R;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/24
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TextBookInfoGridViewAdapter extends BaseAdapter {

    private List<VolumeVo.DataBean.VolumesBean> list;
    private Context context;
    private TextBookInfoGridViewAdapter.onCheckClick onCheckClick;

    public TextBookInfoGridViewAdapter(List<VolumeVo.DataBean.VolumesBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setOnCheckClick(TextBookInfoGridViewAdapter.onCheckClick onCheckClick) {
        this.onCheckClick = onCheckClick;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.review_info_add_gridview_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (!TextUtils.isEmpty(list.get(i).getVolumeName())) {
            holder.viewName.setText(list.get(i).getVolumeName());
        } else {
            holder.viewName.setText("");
        }

        final ViewHolder finalHolder = holder;
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (finalHolder.check.isChecked()) {
                    onCheckClick.deleteData(list.get(i).getVolumeId());
                    finalHolder.check.setChecked(false);
                } else {

                    onCheckClick.setReviewType(list.get(i).getVolumeId(), list.get(i).getVolumeId(), list.get(i).getVolumeName());
                    finalHolder.check.setChecked(true);
                }

            }
        });
        return view;
    }


    static class ViewHolder {
        CheckBox check;
        TextView viewName;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            check = view.findViewById(R.id.check);
            viewName = view.findViewById(R.id.check);
            relativeLayout = view.findViewById(R.id.all);
        }
    }

    public interface onCheckClick {
        void setReviewType(int i, int VolumeId, String VolumeName);

        void deleteData(int i);
    }
}
