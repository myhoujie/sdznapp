package com.example.app5grzx.presenter;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5grzx.view.BasicDataView;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.teacher.vo.UserBean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class BasicDataPresenter extends BasePresenter<BasicDataView, BaseActivity> {
    public void getUserInfo() {
        Network.createTokenService(NetWorkService.GetUserInfoService.class)
                .getUserInfo()
                .map(new StatusFunc<UserBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UserBean>(new SubscriberListener<UserBean>() {
                    @Override
                    public void onNext(UserBean loginBean) {
                        mView.getUserInfo(loginBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void saveHead(final File url) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        Observable.create(new Observable.OnSubscribe<RequestBody>() {
            @Override
            public void call(Subscriber<? super RequestBody> subscriber) {
                File file = getImage(url.getPath());
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/otcet-stream"), file);
                subscriber.onNext(requestBody);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RequestBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(RequestBody requestBody) {
                        pdm.cancelWaiteDialog();
                        Network.createTokenService(NetWorkService.HeadUploadService.class)
                                .uploadHead(requestBody)
                                .map(new StatusFunc<Object>())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                                    @Override
                                    public void onNext(Object o) {
                                        getUserInfo();
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onCompleted() {


                                    }
                                }, mActivity));
                    }
                });
    }

    /**
     * 压缩图片（质量压缩）
     *
     * @param bitmap
     */
    private File compressImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        File file = new File(FileUtil.getAppFilesDir(), System.currentTimeMillis() + ".png");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(baos.toByteArray());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (null != bitmap && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return file;
    }

    /**
     * 图片按比例大小压缩方法
     *
     * @param srcPath （根据路径获取图片并压缩）
     * @return
     */
    private File getImage(String srcPath) {
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 100;// 这里设置高度为800f
        float ww = 100;// 这里设置宽度为480f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
    }
}
