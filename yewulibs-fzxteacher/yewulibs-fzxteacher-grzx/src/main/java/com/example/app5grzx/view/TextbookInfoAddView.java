package com.example.app5grzx.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.me.VersionVo;
import com.sdzn.fzx.teacher.vo.me.VolumeVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface TextbookInfoAddView extends BaseView {
    void setVersionVo(VersionVo versionVo);
    void setVolumeVo(VolumeVo volumeVo);
    void saveDataSuccess();
}
