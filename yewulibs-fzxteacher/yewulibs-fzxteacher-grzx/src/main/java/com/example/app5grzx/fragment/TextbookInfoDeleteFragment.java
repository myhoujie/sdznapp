package com.example.app5grzx.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.app5grzx.R;
import com.example.app5grzx.adapter.TextBookDeleteInfoAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5grzx.presenter.TextbookInfoDeletePresenter;
import com.example.app5grzx.view.TextbookInfoDeleteView;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.views.DelActivityDialog;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.me.DeleteVolumeVo;
import com.sdzn.fzx.teacher.vo.me.TextBookClassVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TextbookInfoDeleteFragment extends MBaseFragment<TextbookInfoDeletePresenter> implements TextbookInfoDeleteView, View.OnClickListener {
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private LinearLayout confirmLy;
    private TextView levelName;
    private RelativeLayout subjectRl;
    private TextView subjectName_tv;
    private RelativeLayout rlTextbook;
    private TextView textbookName;
    private RelativeLayout rlEductional;
    private TextView eductionalName;
    private ListView listview;

    private DelActivityDialog delActivityDialog;
    private List<TextBookClassVo> versionVoList;
    private TextBookDeleteInfoAdapter textBookInfoAdapter;
    LoginBean.DataBean.UserBean user;

    private int subjectId;
    private int versionId;
    private int id;
    private String subjectName;
    private String versionName;

    public TextbookInfoDeleteFragment() {
        // Required empty public constructor
    }

    public static TextbookInfoDeleteFragment newInstance(Bundle bundle) {
        TextbookInfoDeleteFragment fragment = new TextbookInfoDeleteFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_textbook_info_delete, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        title = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        levelName = (TextView) view.findViewById(R.id.level_name);
        subjectRl = (RelativeLayout) view.findViewById(R.id.subject_rl);
        subjectName_tv = (TextView) view.findViewById(R.id.subject_name);
        rlTextbook = (RelativeLayout) view.findViewById(R.id.rl_textbook);
        textbookName = (TextView) view.findViewById(R.id.textbook_name);
        rlEductional = (RelativeLayout) view.findViewById(R.id.rl_eductional);
        eductionalName = (TextView) view.findViewById(R.id.eductional_name);
        listview = (ListView) view.findViewById(R.id.listview);
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        rlTextbook.setOnClickListener(this);
        rlEductional.setOnClickListener(this);
        subjectRl.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
        initView();
    }

    private void initView() {
        eductionalName.setText(user.getBaseEducationName());
        subjectName_tv.setText(subjectName);
        levelName.setText(user.getBaseLevelName());
        textbookName.setText(versionName);
    }

    private void initData() {

        Bundle arguments = getArguments();
        subjectId = arguments.getInt("subjectId");
        versionId = arguments.getInt("versionId");
        versionName = arguments.getString("versionName");
        subjectName = arguments.getString("subjectName");
        id = arguments.getInt("id");
        user = SPUtils.getLoginBean().getData().getUser();
        getVolumeList(versionId + "");
    }

    private void getVolumeList(String versionId) {

        Map<String, Object> volumeParams = new HashMap<>();
        volumeParams.put("baseSubjectId", subjectId);
        volumeParams.put("baseVersionId", versionId);
        volumeParams.put("baseEducationId", user.getBaseEducationId());
        volumeParams.put("levelId", user.getBaseLevelId());

        mPresenter.getVolumeList(volumeParams);

    }


    @Override
    public void initPresenter() {
        mPresenter = new TextbookInfoDeletePresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (viewId == R.id.confirm_ly) {
            ActivityGvAdapter().show();
        }

    }

    @Override
    public void setVolumeVo(DeleteVolumeVo volumeVo) {
        textBookInfoAdapter = new TextBookDeleteInfoAdapter(volumeVo.getData(), getActivity());
        listview.setAdapter(textBookInfoAdapter);
    }

    @Override
    public void saveDataSuccess() {
        getFragmentManager().popBackStack();
    }

    public DelActivityDialog ActivityGvAdapter() {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(getActivity())
                .buildText("确定要删除教材信息吗？")
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        mPresenter.deleteData(id + "");
                    }

                    @Override
                    public void onCancel() {
                    }
                });
        return delActivityDialog;
    }

}
