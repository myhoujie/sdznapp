package com.example.app5grzx.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.app5grzx.R;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVoListVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ClassGroupingAdapter extends BaseAdapter {
    private List<ClassGroupingVoListVo> list;
    private Context context;
    private ClassGroupingGridviewAdapter classGroupingGridviewAdapter;

    public ClassGroupingAdapter(List<ClassGroupingVoListVo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.class_grouping_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.groupName.setText(list.get(i).getClassGroupName());
        holder.groupCount.setText(list.get(i).getList().size() + "");
        classGroupingGridviewAdapter = new ClassGroupingGridviewAdapter(list.get(i).getList(), context);
        holder.gridView.setAdapter(classGroupingGridviewAdapter);
        return view;
    }

    static class ViewHolder {
        TextView groupName;
        TextView groupCount;
        NoScrollGridView gridView;

        ViewHolder(View view) {
            groupName = view.findViewById(R.id.group_name);
            groupCount = view.findViewById(R.id.group_count);
            gridView = view.findViewById(R.id.gridView);
        }
    }
}
