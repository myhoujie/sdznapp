package com.example.app5libpublic.event;

/**
 * @author Created by Reisen on 2019/2/27.
 */
public class TinkerResult {
    public boolean success;
    public long cost;

    public TinkerResult(boolean success, long cost) {
        this.success = success;
        this.cost = cost;
    }
}
