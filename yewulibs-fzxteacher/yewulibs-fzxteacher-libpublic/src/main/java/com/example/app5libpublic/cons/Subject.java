package com.example.app5libpublic.cons;

import android.util.SparseArray;

import com.example.app5libpublic.R;


/**
 * 学科
 *
 * @author wangchunxiao
 * @date 2018/1/9
 */
public enum Subject {
    other("其他", 0, R.mipmap.quanbu_nor),
    yuwen("语文", 1, R.mipmap.yuwen),
    shuxue("数学", 2, R.mipmap.shuxue),
    yingyu("英语", 3, R.mipmap.yingyu),
    wuli("物理", 4, R.mipmap.wuli),
    huaxue("化学", 5, R.mipmap.huaxue),
    shengwu("生物", 6, R.mipmap.shengwu),
    lishi("历史", 7, R.mipmap.lishi),
    dili("地理", 8, R.mipmap.dili),
    //    sixiangpinde("思想品德", 9, R.mipmap.zhengzhi, R.mipmap.zhengzhi_nor, R.mipmap.zhengzhi_sel),
    zhengzhi("思想政治", 10, R.mipmap.zhengzhi),
    yinyue("音乐", 11, R.mipmap.yinyue),
    meishu("美术", 12, R.mipmap.meishu),
    xinxijishu("信息技术", 13, R.mipmap.jisuanji),
    tiyuyujiankang("体育与健康", 14, R.mipmap.tiyu),
    kexue("科学", 15, R.mipmap.kexue),
    tongyongjishu("通用技术", 16, R.mipmap.jishu),
    daodeyufazhi("道德与法制", 18, R.mipmap.daode),
    zongheshijian("综合实践", 19, R.mipmap.zonghe_img);

    private String name;
    private int value;
    private int drawableId;

    private Subject(String name, int value, int drawableId) {
        this.name = name;
        this.value = value;
        this.drawableId = drawableId;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public static SparseArray<Subject> subjects = new SparseArray<>();

    static {
        subjects.put(0, other);
        subjects.put(1, yuwen);
        subjects.put(2, shuxue);
        subjects.put(3, yingyu);
        subjects.put(4, wuli);
        subjects.put(5, huaxue);
        subjects.put(6, shengwu);
        subjects.put(7, lishi);
        subjects.put(8, dili);
//        subjects.put(9, sixiangpinde);
        subjects.put(10, zhengzhi);
        subjects.put(11, yinyue);
        subjects.put(12, meishu);
        subjects.put(13, xinxijishu);
        subjects.put(14, tiyuyujiankang);
        subjects.put(15, kexue);
        subjects.put(16, tongyongjishu);
        subjects.put(18, daodeyufazhi);
        subjects.put(19, zongheshijian);
    }
}
