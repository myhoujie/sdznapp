package com.example.app5libpublic.event;

/**
 * 描述：EventBus事件抽象类
 * <p>
 * 创建人：wangchunxiao
 * 创建时间：16/8/3
 */
public abstract class Event {

    public static final String ONLOGIN_SUCCESS = "login_success";
    public static final String EVENT_TOKEN_MISS = "token_miss";
    public static final String EVENT_CLOSE_SCREEN = "close_screen";
    public static final String EVENT_OPEN_SCREEN = "open_screen";
}
