package com.example.app5libpublic.event;

/**
 * 传递选中的学生
 */

public class TiWenEvent {

    private int studentId;

    public TiWenEvent(int studentId) {
        this.studentId = studentId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public TiWenEvent() {
    }

}
