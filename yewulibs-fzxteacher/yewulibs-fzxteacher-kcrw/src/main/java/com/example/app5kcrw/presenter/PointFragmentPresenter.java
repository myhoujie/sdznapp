package com.example.app5kcrw.presenter;

import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.PointFragmentView;
import com.sdzn.fzx.teacher.vo.PointVo;
import com.sdzn.fzx.teacher.vo.StringData;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/7
 * 修改单号：
 * 修改内容:
 */
public class PointFragmentPresenter extends BasePresenter<PointFragmentView, BaseActivity> {
    private String json = "{\n" +
            "\t\"ability\": [{\n" +
            "\t\t\"abilityAccuracy\": 100.0,\n" +
            "\t\t\"abilityId\": 5090,\n" +
            "\t\t\"abilityName\": \"应用意识\"\n" +
            "\t}],\n" +
            "\t\"avgScore\": -1.0,\n" +
            "\t\"avgUseTime\": 0.0,\n" +
            "\t\"maxScore\": -1.0,\n" +
            "\t\"maxUseTime\": 308,\n" +
            "\t\"method\": [{\n" +
            "\t\t\"methodAccuracy\": 100.0,\n" +
            "\t\t\"methodId\": 5083,\n" +
            "\t\t\"methodName\": \"客观题解题方法\"\n" +
            "\t}],\n" +
            "\t\"myScore\": -1.0,\n" +
            "\t\"myUseTime\": 0,\n" +
            "\t\"point\": [{\n" +
            "\t\t\"pointAccuracy\": 100.0,\n" +
            "\t\t\"pointId\": 15618,\n" +
            "\t\t\"pointName\": \"有理数\",\n" +
            "\t\t\"pointParentId\": 15617,\n" +
            "\t\t\"pointParentName\": \"数与式\",\n" +
            "\t\t\"pointQuestionCount\": 1,\n" +
            "\t\t\"pointScore\": 94.24,\n" +
            "\t\t\"pointWrongCount\": 0\n" +
            "\t}, {\n" +
            "\t\t\"pointId\": 15617,\n" +
            "\t\t\"pointName\": \"数与式\",\n" +
            "\t\t\"pointParentId\": 0\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 50.0,\n" +
            "\t\t\"pointId\": 11289,\n" +
            "\t\t\"pointName\": \"正数和负数\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 10,\n" +
            "\t\t\"pointScore\": 50.0,\n" +
            "\t\t\"pointWrongCount\": 5\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}, {\n" +
            "\t\t\"pointAccuracy\": 60.0,\n" +
            "\t\t\"pointId\": 11290,\n" +
            "\t\t\"pointName\": \"数轴\",\n" +
            "\t\t\"pointParentId\": 15618,\n" +
            "\t\t\"pointParentName\": \"有理数\",\n" +
            "\t\t\"pointQuestionCount\": 5,\n" +
            "\t\t\"pointScore\": 60.0,\n" +
            "\t\t\"pointWrongCount\": 2\n" +
            "\t}\n" +
            "\n" +
            "\n" +
            "\n" +
            "\t]\n" +
            "}";

    public void getPointList(String teacherExamId) {
//        Gson gson = new Gson();
//        PointVo pointVo = gson.fromJson(json, PointVo.class);
//        mView.setPointVo(pointVo);
        Network.createTokenService(NetWorkService.GetTeacherAnalysisListService.class)
                .getAnalysis(teacherExamId)
                .map(new StatusFunc<StringData>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StringData>(new SubscriberListener<StringData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onError();
                    }

                    @Override
                    public void onNext(StringData o) {
                        if (o.getData()!=null){
                            Gson gson = new Gson();
                            PointVo pointVo = gson.fromJson(o.getData(), PointVo.class);
                            mView.setPointVo(pointVo);
                        }

                    }
                }, mActivity));


    }
}
