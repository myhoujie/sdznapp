package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;

/**
 * Created by wangc on 2018/6/27 0027.
 */

public interface CorrectView extends BaseView {
    void saveSuccess(String path);

    void networkError(String msg, boolean close);

    void undoSuccess(UndoCorrectDataVo undoCorrectDataVo);

    void saveBitmapSuccess(String b64Bitmap, boolean b);

    void saveFailed();
}
