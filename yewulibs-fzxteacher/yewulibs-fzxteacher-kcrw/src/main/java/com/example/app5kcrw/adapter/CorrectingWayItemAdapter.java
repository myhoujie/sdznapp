package com.example.app5kcrw.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.CorrectingWayVo;

import java.util.List;

public class CorrectingWayItemAdapter extends RecyclerView.Adapter<CorrectingWayItemAdapter.ViewHolder> {
    List<CorrectingWayVo.StudentsBean> StudentBeanList;
    private RecyclerView recyclerView;

    public CorrectingWayItemAdapter(List<CorrectingWayVo.StudentsBean> studentBeanList) {
        StudentBeanList = studentBeanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_correcting_option, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerView == null)
                    return;
                CorrectingWayItemAdapter.ViewHolder childViewHolder = (CorrectingWayItemAdapter.ViewHolder) recyclerView.getChildViewHolder(v);
                if (childViewHolder == null)
                    return;
                childViewHolder.statusCheckBox.setChecked(!childViewHolder.statusCheckBox.isChecked());
            }
        });
        viewHolder.statusCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (recyclerView == null)
                    return;
                CorrectingWayItemAdapter.ViewHolder childViewHolder = (CorrectingWayItemAdapter.ViewHolder) recyclerView.getChildViewHolder((View) buttonView.getParent().getParent());
                if (childViewHolder == null)
                    return;
                int layoutPosition = childViewHolder.getLayoutPosition();
                StudentBeanList.get(layoutPosition).setStatus(isChecked);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        CorrectingWayVo.StudentsBean studentBean = StudentBeanList.get(position);
        String name = studentBean.getStudentName();
        boolean status = studentBean.isStatus();
        holder.nameTextView.setText(name);
        if (holder.statusCheckBox.isChecked() != status)
            holder.statusCheckBox.setChecked(status);
    }

    @Override
    public int getItemCount() {
        return StudentBeanList.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public CheckBox statusCheckBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.tv_studioname);
            statusCheckBox = itemView.findViewById(R.id.cb_studion);
        }
    }
}