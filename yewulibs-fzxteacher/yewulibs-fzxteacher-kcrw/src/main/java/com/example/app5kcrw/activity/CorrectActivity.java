package com.example.app5kcrw.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.app5libbase.R;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.CorrectPresenter;
import com.example.app5kcrw.view.CorrectView;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CustomDialog;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libbase.views.graffiti.GraffitiOnTouchGestureListener;
import com.example.app5libbase.views.graffiti.GraffitiParams;
import com.example.app5libbase.views.graffiti.GraffitiTouchDetector;
import com.example.app5libbase.views.graffiti.GraffitiView;
import com.example.app5libbase.views.graffiti.IGraffitiListener;
import com.example.app5libbase.views.graffiti.core.IGraffiti;
import com.example.app5libbase.views.graffiti.core.IGraffitiTouchDetector;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;
import com.sdzn.fzx.student.libutils.app.App2;

/**
 * Created by wangc on 2018/6/20 0020.
 */

public class CorrectActivity extends MBaseActivity<CorrectPresenter> implements CorrectView, View.OnClickListener {
    private FrameLayout flGraffiti;
    private TextView tvTitle;
    private ImageView ivClose;
    private LinearLayout ll;
    private TextView tvXuanZhuan;
    private TextView tvCancel;
    private TextView tvUndo;
    private TextView tvSave;

    private IGraffiti mGraffiti;
    private GraffitiView mGraffitiView;

    private GraffitiParams mGraffitiParams;

    private GraffitiOnTouchGestureListener mTouchGestureListener;

    private CorrectDataVo correctData;
    private int vid;
    private String oldPath;
    private ProgressDialogManager mManager;

    private boolean undoBitmap;

    @Override
    public void initPresenter() {
        mPresenter = new CorrectPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReturnPop(false);
        getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_correct);
        flGraffiti = (FrameLayout) findViewById(R.id.fl_graffiti);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        ivClose = (ImageView) findViewById(R.id.ivClose);
        ll = (LinearLayout) findViewById(R.id.ll);
        tvXuanZhuan = (TextView) findViewById(R.id.tvXuanZhuan);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvUndo = (TextView) findViewById(R.id.tvUndo);
        tvSave = (TextView) findViewById(R.id.tvSave);

        tvXuanZhuan.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvUndo.setOnClickListener(this);
        tvSave.setOnClickListener(this);

        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入CorrectActivity成功");
                }
            }
        }
    }

    @Override
    protected void initData() {
        correctData = getIntent().getParcelableExtra("correctData");
        vid = getIntent().getIntExtra("vid", 0);
        oldPath = correctData.getSrc();
    }

    @Override
    protected void initView() {
        //  tvTitle.setText(correctData.getSeq());
        ll.setVisibility(vid == 1 ? View.VISIBLE : View.GONE);
        mManager = new ProgressDialogManager(this);
        mManager.showWaiteDialog();
        // 涂鸦参数
        mGraffitiParams = new GraffitiParams();
        mGraffitiParams.mIsFullScreen = true;
        mGraffitiParams.mImagePath = correctData.getSrc();
        mGraffitiParams.mPaintUnitSize = GraffitiView.DEFAULT_SIZE;
        mGraffitiParams.mAmplifierScale = 0;
        flGraffiti.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                flGraffiti.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setGraffitiBg();
            }
        });
    }

    private void setGraffitiBg() {
        Glide.with(App2.get()).asBitmap().load(correctData.getSrc()).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                mManager.cancelWaiteDialog();
                String msg = e == null ? "未知异常" : e.getMessage();
                ToastUtil.showShortlToast(msg);
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                initGraffiti(resource);
                mManager.cancelWaiteDialog();
                return false;
            }
        }).submit();
//        Glide.with(App2.get()).asBitmap().load(correctData.getSrc()).into(new SimpleTarget<Bitmap>() {
//            @Override
//            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                initGraffiti(resource);
//                mManager.cancelWaiteDialog();
//            }
//
//            @Override
//            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                mManager.cancelWaiteDialog();
//                ToastUtil.showShortlToast(e.getMessage());
//                super.onLoadFailed(e, errorDrawable);
//            }
//        });
    }

    private void initGraffiti(Bitmap resource) {
        mGraffiti = mGraffitiView = new GraffitiView(this, resource, new IGraffitiListener() {
            @Override
            public void onReady() {
                float size = mGraffitiParams.mPaintUnitSize > 0 ? mGraffitiParams.mPaintUnitSize * mGraffiti.getSizeUnit() : 0;
                if (size <= 0) {
                    size = mGraffitiParams.mPaintPixelSize > 0 ? mGraffitiParams.mPaintPixelSize : mGraffiti.getSize();
                }
                // 设置初始值
                mGraffiti.setSize(size);
            }

            @Override
            public void isUndo(boolean isUndo) {
                tvCancel.setEnabled(isUndo);
            }
        });
        mTouchGestureListener = new GraffitiOnTouchGestureListener(mGraffitiView);
        IGraffitiTouchDetector detector = new GraffitiTouchDetector(this, mTouchGestureListener);
        mGraffitiView.setDefaultTouchDetector(detector);
        mGraffitiView.setIsJustDrawOriginal(vid == 0);
        mGraffiti.setIsDrawableOutside(mGraffitiParams.mIsDrawableOutside);
        flGraffiti.addView(mGraffitiView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mGraffiti.setGraffitiMinScale(mGraffitiParams.mMinScale);
        mGraffiti.setGraffitiMaxScale(mGraffitiParams.mMaxScale);
    }

    @Override
    public void onClick(View view) {
        if (!buttonEnable) {
            return;
        }
        int id = view.getId();
        if (id == R.id.ivClose) {// 关闭
            saveAndClose();
        } else if (id == R.id.tvCancel) {// 撤销
            mGraffiti.undo();
        } else if (id == R.id.tvUndo) {// 还原，访问接口，拿到原始图片，重新加载
            new CustomDialog.Builder(this).setMessage("确定把批改结果还原成最初状态？")
                    .setPositive("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mPresenter.undoCorrectPic(oldPath, String.valueOf(correctData.getId()), String.valueOf(correctData.getList()));
                        }
                    }).setNegative("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        } else if (id == R.id.tvSave) {// 保存
//                if (undoBitmap ||mGraffiti.isUndo() || mGraffiti.getGraffitiRotation() != 0) {
            buttonEnable = false;
            mPresenter.saveBitmap(mGraffitiView, false);
//                }
        } else if (id == R.id.tvXuanZhuan) {
            mGraffiti.setGraffitiRotation(mGraffiti.getGraffitiRotation() + 90);
        }
    }

    private void saveAndClose() {
        if (vid == 0) {
            activity.finish();
            return;
        }
        if (undoBitmap || (mGraffiti != null && mGraffiti.isUndo())) {
            new DelActivityDialog()
                    .creatDialog(this)
                    .buildText("关闭前是否保存当前批改结果？")
                    .buildConfirmText("保存")
                    .buildCancelText("不保存")
                    .buildListener(new DelActivityDialog.OptionListener() {
                        @Override
                        public void onConfirmed() {
                            buttonEnable = false;
                            mPresenter.saveBitmap(mGraffitiView, true);
                        }

                        @Override
                        public void onCancel() {
                            activity.finish();
                        }
                    }).show();
        } else {
            activity.finish();
        }
    }

    private boolean buttonEnable = true;

    @Override
    public void saveBitmapSuccess(String b64Bitmap, boolean b) {
        mPresenter.saveCorrectPic(oldPath, b64Bitmap, String.valueOf(correctData.getId()), correctData.getEid(), b);
    }

    @Override
    public void saveFailed() {
        buttonEnable = true;
        ToastUtil.showShortlToast("保存失败");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            saveAndClose();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void saveSuccess(String path) {
        Intent intent = new Intent();
        intent.putExtra("path", path);
        intent.putExtra("oldPath", correctData);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void networkError(String msg, boolean close) {
        buttonEnable = true;
        ToastUtil.showShortlToast(msg);
        if (close) {
            finish();
        }
    }

    @Override
    public void undoSuccess(UndoCorrectDataVo undoCorrectDataVo) {
        undoBitmap = true;
//        Intent intent = new Intent();
//        intent.putExtra("path",undoCorrectDataVo.getData());
//        intent.putExtra("oldPath",correctData);
//        activity.setResult(Activity.RESULT_OK,intent);
        mGraffiti.clear();
        correctData.setSrc(undoCorrectDataVo.getData());
        mManager = new ProgressDialogManager(this);
        mManager.showWaiteDialog();
        Glide.with(App2.get()).asBitmap().load(correctData.getSrc()).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                mManager.cancelWaiteDialog();
                String msg = e == null ? "未知异常" : e.getMessage();
                ToastUtil.showShortlToast(msg);
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                if (mGraffitiView != null) {
                    flGraffiti.removeView(mGraffitiView);
                }
                initGraffiti(resource);
                mManager.cancelWaiteDialog();
                return false;
            }
        }).submit();
//        Glide.with(App2.get()).load(correctData.getSrc()).asBitmap().into(new SimpleTarget<Bitmap>() {
//            @Override
//            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                if (mGraffitiView != null) {
//                    flGraffiti.removeView(mGraffitiView);
//                }
//                initGraffiti(resource);
//                mManager.cancelWaiteDialog();
//            }
//
//            @Override
//            public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                mManager.cancelWaiteDialog();
//                ToastUtil.showShortlToast(e.getMessage());
//                super.onLoadFailed(e, errorDrawable);
//            }
//        });
    }
}
