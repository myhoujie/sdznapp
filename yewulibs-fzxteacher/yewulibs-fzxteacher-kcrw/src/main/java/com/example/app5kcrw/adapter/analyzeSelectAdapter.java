package com.example.app5kcrw.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.ExamAnalyzeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/25
 * 修改单号：
 * 修改内容:
 */
public class analyzeSelectAdapter extends BaseAdapter {

    private List<ExamAnalyzeVo.DataBean.ExamListBean.AnswerListBean.OptionListBean> mList;
    private Context mContext;

    public analyzeSelectAdapter(List<ExamAnalyzeVo.DataBean.ExamListBean.AnswerListBean.OptionListBean> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_student_analyze_select, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.select.setText(mList.get(position).getMyAnswer() + "");
        holder.percent.setText(mList.get(position).getRate() + "%");
        holder.person.setText(mList.get(position).getCount() + "人");
        holder.progress_bar_healthy.setProgress((int) mList.get(position).getRate());

        if (mList.get(position).isRight()) {

            holder.progress_bar_healthy.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.myprogressbar_1));
            holder.select.setBackgroundColor(Color.parseColor("#68C426"));
        } else {
            holder.select.setBackgroundColor(Color.parseColor("#FF6D4A"));
            if (mList.get(position).getCount() != 0) {
                holder.progress_bar_healthy.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.myprogressbar));
            } else {
                holder.progress_bar_healthy.setProgress(0);
                holder.progress_bar_healthy.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.myprogressbar));
            }

        }

        //  holder.progress_bar_healthy.setProgress(30);

        return convertView;
    }

    static class ViewHolder {
        TextView select;
        TextView percent;
        RelativeLayout rl;
        TextView person;
        ProgressBar progress_bar_healthy;

        ViewHolder(View view) {
            select = view.findViewById(R.id.select);
            percent = view.findViewById(R.id.percent);
            rl = view.findViewById(R.id.rl);
            person = view.findViewById(R.id.person);
            progress_bar_healthy = view.findViewById(R.id.progress_bar_healthy);
        }
    }
}
