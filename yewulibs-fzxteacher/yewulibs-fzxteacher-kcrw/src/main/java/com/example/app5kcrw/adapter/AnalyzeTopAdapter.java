package com.example.app5kcrw.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/31
 * 修改单号：
 * 修改内容:
 */
public class AnalyzeTopAdapter extends BaseRcvAdapter<AnalyzeNumberVo.DataBean> {
    public int AdapterPosition = 0;

    public AnalyzeTopAdapter(Context context) {
        super(context, new ArrayList<AnalyzeNumberVo.DataBean>());
    }

    public void clear() {
        mList.clear();
    }

    public void add(List<AnalyzeNumberVo.DataBean> list) {
        mList.addAll(list);
    }

    public AnalyzeNumberVo.DataBean get(int position) {
        return mList.get(position);
    }


    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        BaseViewHolder holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_top);
        setListener(parent, holder, viewType);
        return holder;

    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnalyzeNumberVo.DataBean bean) {

        TextView tv = holder.getView(R.id.tv_index);
        if (position == AdapterPosition) {
            switch (mList.get(position).getAccuracy()) {
                case 0:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_44));//基础
                    tv.setTextColor(Color.parseColor("#ffffff"));
                    break;
                case 1:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_11));//0
                    tv.setTextColor(Color.parseColor("#ffffff"));
                    break;
                case 2:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_33));//2
                    tv.setTextColor(Color.parseColor("#ffffff"));
                    break;
                case 3:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_22));//1
                    tv.setTextColor(Color.parseColor("#ffffff"));
                    break;
            }
        } else {

            switch (mList.get(position).getAccuracy()) {
                case 0:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_4));
                    tv.setTextColor(Color.parseColor("#B1B5BA"));// 0 没批改 1 正确率高  2 正确率中间 3 最低

                    break;
                case 1:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
                    tv.setTextColor(Color.parseColor("#68C426"));
                    break;
                case 2:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_3));
                    tv.setTextColor(Color.parseColor("#FFAC4C"));
                    break;
                case 3:
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_2));
                    tv.setTextColor(Color.parseColor("#FF6D4A"));
                    break;
            }
        }

        tv.setText((position + 1) + "");
    }

}
