package com.example.app5kcrw.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.example.app5kcrw.view.CorrectView;
import com.google.gson.Gson;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.controller.UserController;
import com.example.app5libbase.util.ImageUtils;
import com.example.app5libbase.util.ProgressDialogManager;
import com.example.app5libbase.views.graffiti.GraffitiView;
import com.sdzn.fzx.teacher.vo.CorrectPicVo;
import com.sdzn.fzx.teacher.vo.QiniuUptoken;
import com.sdzn.fzx.teacher.vo.UndoCorrectDataVo;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class CorrectPresenter extends BasePresenter<CorrectView, BaseActivity> {

    private Subscription subscribe1;
    private Subscription subscribe2;
    private String HostUrl = "";

    public void saveCorrectPic(String oldPicPath, String b64Bitmap, String id, String eid, final boolean close) {
        if (subscribe1 != null && subscribe1.isUnsubscribed()) {
            subscribe1.unsubscribe();
            subscriptions.remove(subscribe1);
        }
        uploadQiNiu(oldPicPath, b64Bitmap, id, eid, close);

    }


    public void saveBitmap(final GraffitiView view, final boolean b) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                Bitmap bitmap = view.save();
                if (bitmap.isRecycled()) {
                   Log.e("22222222222222222","222222222222222222");
                }
                String str = ImageUtils.toLosslessBase64(bitmap, 60);
                subscriber.onNext(str);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        mView.saveFailed();
                    }

                    @Override
                    public void onNext(String b64Bitmap) {
                        pdm.cancelWaiteDialog();
                        mView.saveBitmapSuccess(b64Bitmap, b);
                    }
                });
    }

    public void undoCorrectPic(String oldPicPath, String id, String seq) {
        if (subscribe2 != null && subscribe2.isUnsubscribed()) {
            subscribe2.unsubscribe();
            subscriptions.remove(subscribe2);
        }
        Map<String, String> params = new HashMap<>();
        params.put("oldPicPath", oldPicPath);
        params.put("id", id);
        params.put("seq", seq);

        subscribe2 = Network.createTokenService(NetWorkService.CorrectService.class)
                .undoCorrectPic(params)
                .map(new StatusFunc<UndoCorrectDataVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UndoCorrectDataVo>(new SubscriberListener<UndoCorrectDataVo>() {
                    @Override
                    public void onNext(UndoCorrectDataVo undoCorrectDataVo) {
                        mView.undoSuccess(undoCorrectDataVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg(), false);
                            } else {
                                mView.networkError("还原失败", false);
                            }
                        } else {
                            mView.networkError("还原失败", false);
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, false, ""));
        subscriptions.add(subscribe2);
    }


    public static Bitmap base64ToBitmap(String base64String) {

        byte[] bytes = Base64.decode(base64String, Base64.DEFAULT);

        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        return bitmap;

    }

    public byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static String getKey() {

        return UUID.randomUUID().toString();
    }

    private String uploadQiNiu(final String oldPicPath, String b64Bitmap, final String id, final String eid, final boolean close) {

        final byte[] bytes = Bitmap2Bytes(base64ToBitmap(b64Bitmap));
        RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
        final String key = getKey() + ".jpg";
        params.addBodyParameter("key", key);
        params.addBodyParameter("access_token", UserController.getAccessToken());
        //Upload(bytes, key, qiniuUptoken.getResult().getUpToken(), uploadManager);
        final Gson gson = new Gson();
        final UploadManager uploadManager = new UploadManager();
        x.http().get(params, new Callback.ProgressCallback<String>() {
            @Override
            public void onSuccess(String s) {

                QiniuUptoken o = gson.fromJson(s, QiniuUptoken.class);
                HostUrl = o.getResult().getDomain();

                Upload(bytes, key, o.getResult().getUpToken(), uploadManager, oldPicPath, id, eid, close);

            }


            @Override
            public void onError(Throwable e, boolean b) {
                if (e instanceof ApiException) {
                    ApiException apiException = (ApiException) e;
                    StatusVo status = apiException.getStatus();
                    if (status != null && status.getMsg() != null) {
                        mView.networkError(status.getMsg(), close);
                    } else {
                        mView.networkError("保存失败", close);
                    }
                } else {
                    mView.networkError("保存失败", close);
                }
            }

            @Override
            public void onCancelled(CancelledException e) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public void onWaiting() {

            }

            @Override
            public void onStarted() {

            }

            @Override
            public void onLoading(long l, long l1, boolean b) {

            }
        });

        return "";
    }

    private void Upload(byte[] bytes, String key, String upToken, UploadManager uploadManager, final String oldPicPath, final String id, final String eid, final boolean close) {

        uploadManager.put(bytes, key, upToken, new UpCompletionHandler() {
            public void complete(String key, ResponseInfo rinfo, JSONObject response) {
                if (rinfo.isOK()) {
                    Log.e("上传成功", "上传成功" + key);
                    Map<String, String> params = new HashMap<>();
                    params.put("oldPicPath", oldPicPath);
                    params.put("key", key);
                    params.put("id", id);
                    params.put("eid", eid);
                    subscribe1 = Network.createTokenService(NetWorkService.CorrectService.class)
                            .saveCorrectPic(params)
                            .map(new StatusFunc<CorrectPicVo>())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new ProgressSubscriber<CorrectPicVo>(new SubscriberListener<CorrectPicVo>() {
                                @Override
                                public void onNext(CorrectPicVo saveCorrectPicVo) {
                                    mView.saveSuccess(saveCorrectPicVo.getData().getRealPath());
                                }

                                @Override
                                public void onError(Throwable e) {
                                    e.printStackTrace();
                                    CrashReport.postCatchedException(e);
                                    if (e instanceof ApiException) {
                                        ApiException apiException = (ApiException) e;
                                        StatusVo status = apiException.getStatus();
                                        if (status != null && status.getMsg() != null) {
                                            mView.networkError(status.getMsg(), close);
                                        } else {
                                            mView.networkError("保存失败", close);
                                        }
                                    } else {
                                        mView.networkError("保存失败", close);
                                    }
                                }

                                @Override
                                public void onCompleted() {
                                }
                            }, mActivity, true, false, false, ""));
                    subscriptions.add(subscribe1);

                } else {

                    mView.networkError(rinfo.error, close);
                }

            }
        }, null);
    }

}