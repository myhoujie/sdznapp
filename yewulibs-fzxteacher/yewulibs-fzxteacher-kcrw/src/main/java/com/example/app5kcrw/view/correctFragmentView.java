package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.teacher.vo.StudentListVo;
import com.sdzn.fzx.teacher.vo.StudentTaskExamListVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public interface correctFragmentView extends BaseView {
    void setStudentListVo(StudentListVo studentListVo);//按人批改的学生列表结果

    void setStudentTaskExamList(StudentTaskExamListVo studentTaskExamList);//按题批改头部列表

    void setAnswerExamCorrectAllStudent(AnswerExamCorrectAllStudentVo answerExamCorrectAllStudent);//按题批改，查询某道题的所有学生作答结果，并组织老师批语

    void setStudentDetail(StudentDetails examVo);//学生做题列表

    void setCorrectSave();////按人提交批改更新结果

    void setCorrectTopicSave();//按题提交批改更新结果
}
