package com.example.app5kcrw.presenter;

import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.OverallFragmentView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.OverallVo;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class OverallFragmentPresenter extends BasePresenter<OverallFragmentView, BaseActivity> {

    public void getOverallList(String lessonTaskId, String correctType) {
        Network.createTokenService(NetWorkService.GetOverallExamListService.class)
                .AnalyzeStatisc(lessonTaskId, correctType)
                .map(new StatusFunc<OverallVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OverallVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(OverallVo o) {
                        //  mView.setExplainVo(o);
                        if (o.getData().size() == 0) {
                            mView.setNullOverallVo(o);
                        } else {
                            initData(o);
                            mView.setOverallVo(o);
                        }

                    }
                });
    }

    private void initData(OverallVo beans) {
        List<OverallVo.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return;
        }
        for (OverallVo.DataBean bean : list) {
            if (bean.getAnswerExamList() == null) {
                mView.setNullOverallVo(beans);
                return;
            }
            for (OverallVo.DataBean.AnswerExamListBean examListBean : bean.getAnswerExamList()) {
                String resourceText = examListBean.getExamText();
                ExamText examText = gson.fromJson(resourceText, ExamText.class);
                examListBean.setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
                //综合题额外进行一轮解析
                if (examListBean.getTemplateId() != 16) {
                    continue;
                }
            }

        }
    }


    public void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateStudentExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("删除成功");
                        mView.setdel();

                    }
                });
    }
}
