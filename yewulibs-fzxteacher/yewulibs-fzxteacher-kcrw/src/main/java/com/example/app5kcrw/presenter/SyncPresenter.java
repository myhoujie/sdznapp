package com.example.app5kcrw.presenter;

import android.text.TextUtils;

import com.example.app5kcrw.view.SyncView;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libpublic.cons.ListStatus;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * SyncPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SyncPresenter extends BasePresenter<SyncView, BaseActivity> {

    private String baseEducationId;
    private String baseGradeId;
    private String baseVolumeId;
    private String status;
    private String baseClassId;
    private String typeReview;
    private String startTime;
    private String endTime;
    private String chapterId;
    private String chapterNodeIdPath;
    private String type = "1";
    private int curPage;
    private int rows = 10;
    private String keyword;
    private ListStatus listStatus = ListStatus.Refresh;
    private int lastPage = 0;

    public String getBaseEducationId() {
        return dealNullStr(baseEducationId);
    }

    public void setBaseEducationId(String baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseGradeId() {
        return dealNullStr(baseGradeId);
    }

    public void setBaseGradeId(String baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseVolumeId() {
        return dealNullStr(baseVolumeId);
    }

    public void setBaseVolumeId(String baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getStatus() {
        return dealNullStr(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBaseClassId() {
        return dealNullStr(baseClassId);
    }

    public void setBaseClassId(String baseClassId) {
        this.baseClassId = baseClassId;
    }

    public String getTypeReview() {
        return dealNullStr(typeReview);
    }

    public void setTypeReview(String typeReview) {
        this.typeReview = typeReview;
    }

    public String getStartTime() {
        return dealNullStr(startTime);
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return dealNullStr(endTime);
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChapterId() {
        return dealNullStr(chapterId);
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterNodeIdPath() {
        return dealNullStr(chapterNodeIdPath);
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getKeyword() {
        return dealNullStr(keyword);
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void getClassList() {
        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(subject)
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }

    public void getTaskList() {
        getTaskList(ListStatus.Refresh);
    }

    public void getTaskList(ListStatus status) {
        if (status == ListStatus.Refresh) {
            curPage = 1;
        } else if (status == ListStatus.Loadmore) {
            curPage = lastPage + 1;
        }

        listStatus = status;

        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";

        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getTaskList(getBaseEducationId(), getBaseGradeId(), getBaseVolumeId(),
                        subject, getStatus(), "0",getBaseClassId(), getTypeReview(),
                        getStartTime(), getEndTime(), getChapterId(), getChapterNodeIdPath(),
                        getType(), curPage + "", getRows() + "", getKeyword())
                .map(new StatusFunc<SyncTaskVo>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<SyncTaskVo>(new SubscriberListener<SyncTaskVo>() {
                    @Override
                    public void onNext(SyncTaskVo o) {
                        if (o != null) {
                            mView.onTaskListSuccessed(o, listStatus, o.getData().size() < getRows());
                            lastPage = curPage;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTaskListFailed(listStatus);
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getVersionList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<VersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<VersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed();
                    }

                    @Override
                    public void onNext(VersionBean versionBean) {
                        dealVersionBean(versionBean);
                    }
                });

    }

    public void getNodeList(final String baseVersion) {
        if (TextUtils.isEmpty(baseVersion)) {
            mView.onNodeSuccessed(null);
            return;
        }

        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        final String baseLevelId = SPUtils.getLoginBean().getData().getUser().getBaseLevelId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getNodeList(subject, baseLevelId, getBaseGradeId(), baseVersion, getBaseVolumeId())
                .map(new StatusFunc<NodeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NodeBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed();
                    }

                    @Override
                    public void onNext(NodeBean o) {
                        mView.onNodeSuccessed(o.getData());
                    }
                });
    }

    public void delTaskItem(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delTaskItem(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        mView.onDelSuccessed();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onDelFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    public void sendAnswer(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .sendAnswer(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showShortlToast("答案已发送给学生");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    void dealVersionBean(VersionBean versionBean) {

        if (versionBean == null) {
            versionBean = new VersionBean();
        }
        if (versionBean.getData() == null) {
            versionBean.setData(new ArrayList<VersionBean.DataBean>());

            final ArrayList<VersionBean.DataBean.VolumeListBean> volumeListBeans = new ArrayList<>();

            final VersionBean.DataBean dataBean = new VersionBean.DataBean();
            dataBean.setVolumeList(volumeListBeans);

            versionBean.getData().add(dataBean);
        }

        VersionBean.DataBean.VolumeListBean volumeListBean = new VersionBean.DataBean.VolumeListBean();
        volumeListBean.setBaseVersionName("全部册别");
        volumeListBean.setBaseGradeName("");
        volumeListBean.setBaseVolumeName("");
        versionBean.getData().get(0).getVolumeList().add(0, volumeListBean);

        ArrayList<VersionBean.DataBean.VolumeListBean> retList = new ArrayList<>();

        for (int i = 0; i < versionBean.getData().size(); i++) {
            retList.addAll(versionBean.getData().get(i).getVolumeList());
        }

        mView.onVersionSuccessed(retList);
    }


    private String dealNullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;

    }

    private int getNodePointCount(String str) {
        final String newStr = str.replace(".", "");
        return str.length() - newStr.length();

    }

    public NodeBean.DataBeanXX getBaseNodeBean(NodeBean.DataBeanXX dataBeanXX) {
        if (dataBeanXX.isLeaf()) {
            return dataBeanXX;
        }
        if (getNodePointCount(dataBeanXX.getData().getNodeIdPath()) >= 3) {
            return dataBeanXX;
        }

        return getBaseNodeBean(dataBeanXX.getChildren().get(0));
    }

    public void getNodeBean(NodeBean.DataBeanXX.DataBean data) {

    }



}
