package com.example.app5kcrw.presenter;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.statisticsFragmentView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.GroupStudent;
import com.sdzn.fzx.teacher.vo.GroupStudentDetils;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentList;

import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class statisticsFragmentPresenter extends BasePresenter<statisticsFragmentView, BaseActivity> {
    //获取整体成绩列表
    public void getStudentList(Map<String, String> param) {
        Network.createTokenService(NetWorkService.GetStudentListService.class)
                .GetStudentList(param)
                .map(new StatusFunc<StudentList>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(StudentList o) {
                        mView.StudentList(o);

                    }
                });
    }

    /*获取整体查看列表*/
    public void getStudentStatistics(String id,String classGroupId) {

        Network.createTokenService(NetWorkService.GetStudentStatiscService.class)
                .StudentStatisc(id,classGroupId)
                .map(new StatusFunc<StatisticsVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StatisticsVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(StatisticsVo o) {
                        mView.StudentStatics(o);
                    }
                });
    }

    /*分组查看列表*/
    public void getGroupStudentList(String classId) {
        Network.createTokenService(NetWorkService.GetGroupStudentListService.class)
                .GetStudentList(classId)
                .map(new StatusFunc<GroupStudent>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GroupStudent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(GroupStudent o) {
                        mView.GroupStudentStatics(o);
                    }
                });
    }

    //获取学生信息
    public void getGroupDetilsListService(Map<String, Object> param) {

        Network.createTokenService(NetWorkService.GetGroupDetilsListService.class)
                .GetStudentList(param)
                .map(new StatusFunc<GroupStudentDetils>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GroupStudentDetils>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(GroupStudentDetils o) {
                        mView.GroupDetilsStatics(o);

                    }
                });
    }
}
