package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;
import com.sdzn.fzx.teacher.vo.AnalyzeStatisticsVo;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamAnalyzeVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public interface analyzeFragmentView extends BaseView {

   void setAnalyzeVo(AnalyzeVo analyzeVo);

   void setAnalyzeNumberVo(AnalyzeNumberVo analyzeNumberVo);

   void setAnalyzeStatisticsVo(AnalyzeStatisticsVo analyzeStatisticsVo);
   void setExamAnalyzeStatisticsVo(ExamAnalyzeVo examAnalyzeVo);
}
