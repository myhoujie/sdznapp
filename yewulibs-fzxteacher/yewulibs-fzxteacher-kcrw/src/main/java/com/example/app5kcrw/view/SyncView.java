package com.example.app5kcrw.view;

import com.example.app5libpublic.cons.ListStatus;
import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * SyncView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface SyncView extends BaseView {

    public void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> list);

    public void onNodeSuccessed(List<NodeBean.DataBeanXX> list);

    public void onTitleFailed();

    public void onClassSuccessed(SyncClassVo classVo);

    public void onTaskListSuccessed(SyncTaskVo taskVo, ListStatus status, boolean isAll);

    public void onTaskListFailed(ListStatus status);

    public void onDelSuccessed();

    public void onDelFailed();

    public void onSendAnswerSuccess();
}
