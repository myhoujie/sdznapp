package com.example.app5kcrw.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.ExplainVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class ExplainListViewAdapter extends BaseAdapter {
    public int index = 0;
    private List<ExplainVo.DataBean> mList;
    private Context mContext;

    public ExplainListViewAdapter(List<ExplainVo.DataBean> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.student_details_item, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imageView.setVisibility(View.GONE);
        holder.examName.setText(mList.get(position).getExamSeq() + " " + mList.get(position).getExamTemplateStyleName());
        if (index == position) {
            holder.examName.setTextColor(Color.parseColor("#5BA2EA"));
        } else {
            holder.examName.setTextColor(Color.parseColor("#323C47"));
        }
        return convertView;
    }

    static class ViewHolder {
        TextView examName;
        ImageView imageView;
        RelativeLayout relativeLayout;

        ViewHolder(View view) {
            examName = view.findViewById(R.id.exam_name);
            imageView = view.findViewById(R.id.img);
            relativeLayout = view.findViewById(R.id.rl_list);
        }
    }
}
