package com.example.app5kcrw.fragment.natives;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5kcrw.adapter.ExplainListViewAdapter;
import com.example.app5kcrw.adapter.ExplainDetailAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.ExplainFragmentPresenter;
import com.example.app5kcrw.view.ExplainFragmentView;
import com.sdzn.fzx.teacher.vo.ExamCorrectVo;
import com.sdzn.fzx.teacher.vo.ExplainVo;
import com.sdzn.fzx.teacher.vo.UserTeacherInfo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

/**
 * 讲解典型
 */
public class ExplainFragment extends MBaseFragment<ExplainFragmentPresenter> implements ExplainFragmentView {

    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private TextView tv;
    private ListView listview;
    private RecyclerView recyclerView;


    private Bundle bundle;
    private String classId;
    private String id;
    private ExplainDetailAdapter studentDetailAdapter;
    private List<ExamCorrectVo.DataBean> mList = new ArrayList<>();
    private List<ExplainVo.DataBean> dataBeans = new ArrayList<>();
    ExamCorrectVo examCorrect;
    private UserTeacherInfo userTeacherInfo = null;
    private int postion = 0;
    ExplainListViewAdapter explainListViewAdapter;

    public ExplainFragment() {
    }

    public static ExplainFragment newInstance(Bundle bundle) {
        ExplainFragment fragment = new ExplainFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explain, container, false);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        tv = (TextView) view.findViewById(R.id.tv);
        listview = (ListView) view.findViewById(R.id.listview);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);


        mPresenter.teacherInfo();
        studentDetailAdapter = new ExplainDetailAdapter(App2.get(), mList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        recyclerView.setAdapter(studentDetailAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                postion = position;
                explainListViewAdapter.index = position;
                explainListViewAdapter.notifyDataSetChanged();
                mPresenter.getexamCorrectList(dataBeans.get(position).getId() + "");


            }
        });
        studentDetailAdapter.setOnAddClickListener(new ExplainDetailAdapter.OnAddClickListener() {
            @Override
            public void onItemClick(int ids) {
                mPresenter.getexamList(id, "1");
            }
        });


        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new ExplainFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    private void initData() {

        bundle = getArguments();
        classId = bundle.getString("classId");
        id = bundle.getString("id");
        mPresenter.getexamList(id, "1");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void setExplainVo(ExplainVo explainVo) {
        if (explainVo.getData() == null) {
            llTaskEmpty.setVisibility(View.VISIBLE);
            tv.setVisibility(View.GONE);
        } else {
            explainListViewAdapter = new ExplainListViewAdapter(explainVo.getData(), getActivity());
            listview.setAdapter(explainListViewAdapter);
            dataBeans = explainVo.getData();
            mPresenter.getexamCorrectList(explainVo.getData().get(0).getId() + "");
        }

    }

    @Override
    public void setExamCorrectVo(ExamCorrectVo examCorrectVo) {
        studentDetailAdapter.setUserTeacherInfo(userTeacherInfo);
        mList.clear();
        examCorrect = examCorrectVo;
        mList.addAll(examCorrectVo.getData());
        //studentName.setText(examCorrect.getData().get(0).getUserStudentName());
        refreshAdapter();
    }

    @Override
    public void setdel() {
        mPresenter.getexamList(id, "1");
    }

    @Override
    public void setUserTeacherInfo(UserTeacherInfo teacherInfo) {
        userTeacherInfo = teacherInfo;

        initData();
    }

    private void refreshAdapter() {
        studentDetailAdapter.notifyDataSetChanged();

    }

    private boolean flagGoodImg = true;
}
