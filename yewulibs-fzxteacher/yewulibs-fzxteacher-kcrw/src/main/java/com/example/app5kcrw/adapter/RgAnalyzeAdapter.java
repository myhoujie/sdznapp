package com.example.app5kcrw.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.RgAnalyze;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class RgAnalyzeAdapter extends BaseAdapter {
    public int selectPosition = 0;
    private List<RgAnalyze.DataBean> mList;
    private Context Mcontext;

    public RgAnalyzeAdapter(List<RgAnalyze.DataBean> mList, Context mcontext) {
        this.mList = mList;
        Mcontext = mcontext;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(Mcontext).inflate(R.layout.rganaly_list_item, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mList.get(position).getResourceName());
        if (position == selectPosition) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#edf5ff"));
        } else {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        switch (mList.get(position).getResourceType()) {
            case 1:
                if (mList.get(position).getResourceName().indexOf("txt") != -1) {
                    holder.photo.setImageResource(R.drawable.txt);
                } else {
                    holder.photo.setImageResource(R.drawable.doc);
                }


                break;
            case 2:
                holder.photo.setImageResource(R.drawable.ppt);
                break;
            case 3:
                holder.photo.setImageResource(R.drawable.video);
                break;
            case 4:
                holder.photo.setImageResource(R.drawable.image);
                break;
            case 5:
                holder.photo.setImageResource(R.drawable.music);
                break;
            case 6:
                holder.photo.setImageResource(R.drawable.pdf);
                break;
            default:
                holder.photo.setImageResource(R.drawable.ppt);
                break;
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView photo;
        TextView name;
        LinearLayout relativeLayout;

        ViewHolder(View view) {
            photo = view.findViewById(R.id.photo);
            name = view.findViewById(R.id.name);
            relativeLayout = view.findViewById(R.id.rl);
        }
    }
}
