package com.example.app5kcrw.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.ActivityGvAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.listener.OnCalenderSelectListener;
import com.example.app5libbase.listener.OnSearchClickListener;
import com.example.app5libbase.pop.TaskSearchPop;
import com.example.app5kcrw.presenter.ReviewPresenter;
import com.example.app5kcrw.view.ReviewView;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.CalenderDialog;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.ImageHintEditText;
import com.example.app5libbase.views.LoadStatusView;
import com.example.app5libbase.views.ReviewVersionSpinner;
import com.example.app5libbase.views.SpinerPopWindow;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReviewFragment extends MBaseFragment<ReviewPresenter> implements ReviewView, View.OnClickListener {


    public static final String SAVE_CATEGORY = "save_category";
    private static final int subjectId = 0;
    private static final String[] TASK_STATUS = {"0", "1", "2", "3", "4"};

    private TextView versionText;
    private ImageHintEditText btnSearch;
    private LinearLayout statusLy;
    private RadioGroup statusRg;
    private RadioButton status1;
    private RadioButton status2;
    private RadioButton status3;
    private RadioButton status4;
    private RadioButton status5;
    private TextView classSpinnerTxt;
    private CalenderClearEditText dateChooseTxt;
    private SmartRefreshLayout refreshLayout;
    private GridView syncGridview;
    private LoadStatusView contentError;
    private ReviewVersionSpinner versionSpinner;

    private SpinerPopWindow classChoosePop;

    private ActivityGvAdapter mAdapter;

    private CalenderDialog calendarDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_review, container, false);


        versionText = (TextView) view.findViewById(R.id.version_text);
        btnSearch = (ImageHintEditText) view.findViewById(R.id.btnSearch);
        statusLy = (LinearLayout) view.findViewById(R.id.status_ly);
        statusRg = (RadioGroup) view.findViewById(R.id.status_rg);
        status1 = (RadioButton) view.findViewById(R.id.status_1);
        status2 = (RadioButton) view.findViewById(R.id.status_2);
        status3 = (RadioButton) view.findViewById(R.id.status_3);
        status4 = (RadioButton) view.findViewById(R.id.status_4);
        status5 = (RadioButton) view.findViewById(R.id.status_5);
        classSpinnerTxt = (TextView) view.findViewById(R.id.class_spinner_txt);
        dateChooseTxt = (CalenderClearEditText) view.findViewById(R.id.date_choose_txt);
        refreshLayout = (SmartRefreshLayout) view.findViewById(R.id.refreshLayout);
        syncGridview = (GridView) view.findViewById(R.id.sync_gridview);
        contentError = (LoadStatusView) view.findViewById(R.id.content_error);
        versionSpinner = (ReviewVersionSpinner) view.findViewById(R.id.version_spinner_view);
        versionText.setOnClickListener(this);
        classSpinnerTxt.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        dateChooseTxt.setOnClickListener(this);
        initView();
        initData();
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new ReviewPresenter();
        mPresenter.attachView(this, activity);
    }

    private void initView() {
        mAdapter = new ActivityGvAdapter(getActivity());
        syncGridview.setAdapter(mAdapter);
        mAdapter.setItemDisposalListener(new ActivityGvAdapter.ItemDisposalListener() {
            @Override
            public void onDelet(SyncTaskVo.DataBean dataBean) {
                mPresenter.delTaskItem(String.valueOf(dataBean.getId()));
            }

            @Override
            public void onPublishAnswer(int id) {
                mPresenter.sendAnswer(id + "");
            }
        });

        versionSpinner.setOnItemChoosedListener(new ReviewVersionSpinner.ItemChoosedListener() {
            @Override
            public void onItemChoosed(int pos) {
                onVersionChoose(versionList.get(pos));
            }

        });


        statusRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if (id == R.id.status_1) {
                    mPresenter.setStatus(TASK_STATUS[0]);
                } else if (id == R.id.status_2) {
                    mPresenter.setStatus(TASK_STATUS[1]);
                } else if (id == R.id.status_3) {
                    mPresenter.setStatus(TASK_STATUS[2]);
                } else if (id == R.id.status_4) {
                    mPresenter.setStatus(TASK_STATUS[3]);
                } else if (id == R.id.status_5) {
                    mPresenter.setStatus(TASK_STATUS[4]);
                }
                mPresenter.getTaskList();

            }
        });

        classChoosePop = new SpinerPopWindow(getActivity());
        classChoosePop.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (classVo == null)
                    return;
                onChooseClass(classVo.getData().get(pos));
            }
        });

        contentError.setReloadListener(new LoadStatusView.ReloadListener() {
            @Override
            public void onReloadClicked() {
                mPresenter.getTaskList();
            }
        });

        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                mPresenter.getTaskList();
            }
        });
    }

    private void initData() {
        mPresenter.setStatus(TASK_STATUS[0]);
        mPresenter.getVersionList();
        mPresenter.getClassList();
        mPresenter.getTaskList();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.version_text) {
            if (versionSpinner.getVisibility() == View.GONE) {
                mPresenter.getVersionList();
                versionSpinner.setList(versionList);
            } else
                versionSpinner.setVisibility(View.GONE);
        } else if (id == R.id.class_spinner_txt) {
            classChoosePop.showAsDropDown(classSpinnerTxt);
        } else if (id == R.id.btnSearch) {
            showPop();
        } else if (id == R.id.date_choose_txt) {
            showCalendarDialog();
        }

    }

    private TaskSearchPop taskSearchPop;

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(getActivity(), new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {
                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    mPresenter.setKeyword(searchStr);
                    mPresenter.getTaskList();
                }
            });
        }

        taskSearchPop.showPopupWindow(SAVE_CATEGORY, btnSearch, subjectId);
    }

    private List<ReviewVersionBean.DataBean> versionList;

    @Override
    public void onVersionSuccessed(ReviewVersionBean versionBean) {
        this.versionList = versionBean.getData();
        if (versionList == null || versionList.size() == 0) {
            return;
        }
        if (versionSpinner.getVisibility() == View.VISIBLE) {
            versionSpinner.setList(versionList);
        }
    }

    private void onVersionChoose(ReviewVersionBean.DataBean bean) {
        versionText.setText(bean.getReviewTypeName());
        mPresenter.setBaseGradeId(bean.getBaseGradeId());
        mPresenter.setTypeReview(bean.getReviewType());
        mPresenter.getTaskList();
    }


    @Override
    public void onVersionFailed() {
    }

    private SyncClassVo classVo;

    @Override
    public void onClassSuccessed(SyncClassVo classVo) {

        if (classVo.getData() == null || classVo.getData().size() == 0) {
            List<SyncClassVo.DataBean> list = new ArrayList<>();
            classVo.setData(list);
        }
        SyncClassVo.DataBean all = new SyncClassVo.DataBean();
        all.setClassName("全部班级");
        classVo.getData().add(0, all);
        this.classVo = classVo;

        List<String> list = new ArrayList<>();
        for (int i = 0; i < classVo.getData().size(); i++) {
            list.add(classVo.getData().get(i).getClassName());
        }
        classChoosePop.setPopDatas(list);
    }

    @Override
    public void onTaskListSuccessed(SyncTaskVo taskVo) {
        refreshLayout.finishRefresh(true);
        if (taskVo != null && taskVo.getData() != null && taskVo.getData().size() != 0) {
            contentError.onSuccess();
            mAdapter.setData(taskVo.getData());
        } else {
            contentError.onEmpty();
        }
    }

    @Override
    public void onTaskListFailed() {
        refreshLayout.finishRefresh(false);
        contentError.onError();
    }

    @Override
    public void onDelSuccessed() {
        ToastUtil.showShortlToast("删除成功");
    }

    @Override
    public void onDelFailed() {
        mPresenter.getTaskList();
    }

    public void onChooseClass(SyncClassVo.DataBean classBean) {

        classSpinnerTxt.setText(classBean.getClassName());
        mPresenter.setBaseClassId(classBean.getClassId());
        mPresenter.getTaskList();
    }

    private void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(activity, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    dateChooseTxt.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  至  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String startTime = sdf.format(startCalendar.getTimeInMillis());
                    String endTime = sdf.format(endCalendar.getTimeInMillis());

                    mPresenter.setStartTime(startTime);
                    mPresenter.setEndTime(endTime);
                    mPresenter.getTaskList();
                }
            });

            dateChooseTxt.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    mPresenter.setStartTime("");
                    mPresenter.setEndTime("");
                    mPresenter.getTaskList();
                }
            });
        }
        calendarDialog.show();
    }

}
