package com.example.app5kcrw.fragment.natives;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5kcrw.adapter.AnswerExamCorrectAllStudentAdapter;
import com.example.app5kcrw.adapter.AnswerTopBarAdapter;
import com.example.app5kcrw.adapter.CorrectScoreAdapter;
import com.example.app5kcrw.adapter.MarkingTopBarAdapter;
import com.example.app5kcrw.adapter.MarkingcorrectScoreAdapter;
import com.example.app5kcrw.adapter.StudentDetailAdapter;
import com.example.app5libbase.baseui.adapter.StudentListviewCorrectAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.correctFragmentPresenter;
import com.example.app5kcrw.view.correctFragmentView;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.teacher.vo.StudentListVo;
import com.sdzn.fzx.teacher.vo.StudentTaskExamListVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

/**
 * 批改
 */
public class correctFragment extends MBaseFragment<correctFragmentPresenter> implements correctFragmentView, View.OnClickListener {
    /*
     * 按人批改
     * */
    /*学生名称列表*/
    ListView studenListView;
    /*显示答案列表*/
    RecyclerView mRecyclerView;
    /*暂无内容图片*/
    ImageView ivTaskEmpty;
    /*暂无内容文字*/
    TextView tvTaskEmpty;
    /*暂无内容外层包裹*/
    LinearLayout llTaskEmpty;
    /*题头部外层列表*/
    RecyclerView rvAnswers;
    /*题头部外层包裹
    @BindView(R.id.rl_top)
    RelativeLayout rlTop;*/
    /*批改学生头像*/
    ImageView photo;
    /*批改学生名称*/
    TextView name;
    /*笑脸好*/
    ImageView happy;
    /*笑脸差*/
    ImageView sad;
    /*总题数*/
    TextView allNum;
    /*未批改*/
    TextView notNum;
    /*总体数外层包裹*/
    LinearLayout lyRight;
    /*批改类型列表*/
    ListView listview;
    /*保存*/
    TextView saveTv;
    /*客观题得分*/
    TextView scoreTv;
    /*显示卷面*/
    RelativeLayout rl;
    /*按题批改*/
    LinearLayout markingTitle;
    /*按人批改*/
    LinearLayout correctionPerson;


    /*
     * 按题批改
     *
     * */
    /*头部外层包裹
    @BindView(R.id.rl_marking_top)
    RelativeLayout rlmarkingtop;*/
    /*头部题列表*/
    RecyclerView rvMarkingAnswers;
    /*题解答列表*/
    RecyclerView recyclerViewMarking;
    /*总体术*/
    TextView allMarkingNum;
    /*未批改*/
    TextView notMarkingNum;
    /*批改学生选分列表*/
    ListView markingListview;
    TextView markingSaveTv;
    /*暂无数据图片*/
    ImageView ivTaskEmptyMarking;
    /*暂无数据文字*/
    TextView tvTaskEmptyMarking;
    /*暂无数据外层包裹*/
    LinearLayout llTaskEmptyMarking;
    /*卷面包裹*/
    LinearLayout llSurface;


    private LinearLayout lyMarkingRight;
    private View line1;
    private View lineeMarking;
    private RelativeLayout rlllMarking;
    private View line;
    private View linee;
    private RelativeLayout rlll;
    private RelativeLayout rlMarkingTop;
    private RelativeLayout rlTop;


    private Bundle bundle;
    private String classId;
    private String id;
    private StudentDetailAdapter studentDetailAdapter;//获取按人批改adapter
    private AnswerTopBarAdapter mTopAdapter;//按人批改top
    private List<StudentDetails.DataBean> mList = new ArrayList<>();//获取按人批改题

    private List<AnswerExamCorrectAllStudentVo.DataBean> answerlist = new ArrayList<>();//获取按题批改题
    private MarkingTopBarAdapter markingTopBarAdapter;//按题批改top
    private AnswerExamCorrectAllStudentAdapter answerExamCorrectAllStudentAdapter;//获取按题批改adapter
    private MarkingcorrectScoreAdapter markingcorrectScoreAdapter;

    private String updateId;
    private int correctUserType = 1;
    private CorrectScoreAdapter correctScoreAdapter;
    private int lessonTaskStudentId;
    private StudentListVo studentListOnclick = null;
    private int postions = 0;
    private boolean groupFlag = false;

    private boolean happyFlag = true;
    private boolean sadFlag = true;
    private StudentDetails Details;
    private AnswerExamCorrectAllStudentVo answerExamCorrectAllStudentVo;
    private List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> AnswerDetails = new ArrayList<>();

    public correctFragment() {
        // Required empty public constructor
    }

    public static correctFragment newInstance(Bundle bundle) {
        correctFragment fragment = new correctFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_correct, container, false);
        markingTitle = (LinearLayout) view.findViewById(R.id.marking_title);
        llTaskEmptyMarking = (LinearLayout) view.findViewById(R.id.llTaskEmptyMarking);
        ivTaskEmptyMarking = (ImageView) view.findViewById(R.id.ivTaskEmptyMarking);
        tvTaskEmptyMarking = (TextView) view.findViewById(R.id.tvTaskEmptyMarking);
        rlMarkingTop = (RelativeLayout) view.findViewById(R.id.rl_marking_top);
        rvMarkingAnswers = (RecyclerView) view.findViewById(R.id.rv_marking_answers);
        recyclerViewMarking = (RecyclerView) view.findViewById(R.id.recyclerView_Marking);
        lyMarkingRight = (LinearLayout) view.findViewById(R.id.ly_marking_right);
        allMarkingNum = (TextView) view.findViewById(R.id.all_marking_num);
        notMarkingNum = (TextView) view.findViewById(R.id.not_marking_num);
        line1 = (View) view.findViewById(R.id.line1);
        markingListview = (ListView) view.findViewById(R.id.marking_listview);
        lineeMarking = (View) view.findViewById(R.id.linee_marking);
        rlllMarking = (RelativeLayout) view.findViewById(R.id.rlll_marking);
        markingSaveTv = (TextView) view.findViewById(R.id.marking_save_tv);
        correctionPerson = (LinearLayout) view.findViewById(R.id.correction_person);
        studenListView = (ListView) view.findViewById(R.id.studen_listView);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        rl = (RelativeLayout) view.findViewById(R.id.rl);
        photo = (ImageView) view.findViewById(R.id.photo);
        name = (TextView) view.findViewById(R.id.name);
        scoreTv = (TextView) view.findViewById(R.id.score_tv);
        llSurface = (LinearLayout) view.findViewById(R.id.ll_surface);
        happy = (ImageView) view.findViewById(R.id.happy);
        sad = (ImageView) view.findViewById(R.id.sad);
        rlTop = (RelativeLayout) view.findViewById(R.id.rl_top);
        rvAnswers = (RecyclerView) view.findViewById(R.id.rv_answers);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        lyRight = (LinearLayout) view.findViewById(R.id.ly_right);
        allNum = (TextView) view.findViewById(R.id.all_num);
        notNum = (TextView) view.findViewById(R.id.not_num);
        line = (View) view.findViewById(R.id.line);
        listview = (ListView) view.findViewById(R.id.listview);
        linee = (View) view.findViewById(R.id.linee);
        rlll = (RelativeLayout) view.findViewById(R.id.rlll);
        saveTv = (TextView) view.findViewById(R.id.save_tv);

        happy.setOnClickListener(this);
        sad.setOnClickListener(this);
        saveTv.setOnClickListener(this);
        markingSaveTv.setOnClickListener(this);


        llSurface.setVisibility(View.GONE);
        //按人批改试题页面顶部序号
        mTopAdapter = new AnswerTopBarAdapter(App2.get());
        rvAnswers.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        rvAnswers.setAdapter(mTopAdapter);
        //按人批改题型列表
        studentDetailAdapter = new StudentDetailAdapter(App2.get(), mList, this, 1);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        mRecyclerView.setAdapter(studentDetailAdapter);

        /*按题批改试题页面顶部序号*/
        markingTopBarAdapter = new MarkingTopBarAdapter(App2.get());
        rvMarkingAnswers.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        rvMarkingAnswers.setAdapter(markingTopBarAdapter);

        /*按题批改题型列表*/
        answerExamCorrectAllStudentAdapter = new AnswerExamCorrectAllStudentAdapter(App2.get(), answerlist, getActivity(), 1);
        recyclerViewMarking.setLayoutManager(new LinearLayoutManager(App2.get()));
        recyclerViewMarking.setAdapter(answerExamCorrectAllStudentAdapter);


        //获取bundle参数
        bundle = getArguments();
        classId = bundle.getString("classId");
        id = bundle.getString("id");


        initDatatest();

        /*按人批改头部点击*/
        mTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mTopAdapter.current_index = position;
                mTopAdapter.notifyDataSetChanged();
                mRecyclerView.scrollToPosition(position);
                mRecyclerView.smoothScrollToPosition(position);
            }
        });
        /*左边学生名称列表*/
        studenListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                postions = position;
                mPresenter.getStudentDetailList(studentListOnclick.getData().get(position).getId() + "", "2");
            }
        });
        return view;
    }

    @Override
    public void initPresenter() {
        mPresenter = new correctFragmentPresenter();
        mPresenter.attachView(this, activity);
    }

    private StudentListVo listVo;

    /*按人批改*/
    @Override
    public void setStudentListVo(StudentListVo studentListVo) {
        listVo = studentListVo;
        if (studentListVo.getData() == null) {
            llTaskEmptyMarking.setVisibility(View.VISIBLE);
            return;
        }
        llTaskEmptyMarking.setVisibility(View.GONE);
        rl.setVisibility(View.VISIBLE);
        studentListOnclick = studentListVo;
        mPresenter.getStudentDetailList(studentListVo.getData().get(postions).getId() + "", "2");
        lessonTaskStudentId = studentListVo.getData().get(postions).getId();
        updateId = studentListVo.getData().get(postions).getId() + "";
        name.setText(studentListVo.getData().get(postions).getUserStudentName());
        if (Double.valueOf(studentListVo.getData().get(postions).getScoreTotal()).intValue() > -1) {
            scoreTv.setText("客观题得分：" + studentListVo.getData().get(postions).getScoreObjective() + "分");
        } else {
            scoreTv.setText("客观题得分：--");
        }
       /* if (studentListVo.getData().get(postions).getCorrectType() == 2) {
            sad.setImageResource(R.mipmap.cucao_sel);
            happy.setImageResource(R.mipmap.youxiu_nor);
        } else if (studentListVo.getData().get(postions).getCorrectType() == 1) {
            happy.setImageResource(R.mipmap.youxiu_sel);
            sad.setImageResource(R.mipmap.cucao_nor);
        } else if (studentListVo.getData().get(postions).getCorrectType() == 3) {
            happy.setImageResource(R.mipmap.youxiu_nor);
            sad.setImageResource(R.mipmap.cucao_nor);
        }*/
        //左边显示学员信息列表
        studenListView.setAdapter(new StudentListviewCorrectAdapter(studentListVo.getData(), getActivity()));
    }


    /*学生做题列表展示*/
    @Override
    public void setStudentDetail(StudentDetails examVo) {
        Details = examVo;
        updateId = studentListOnclick.getData().get(postions).getId() + "";
        name.setText(studentListOnclick.getData().get(postions).getUserStudentName());
        if (Double.valueOf(studentListOnclick.getData().get(postions).getScoreTotal()).intValue() > -1) {
            scoreTv.setText("客观题得分：" + studentListOnclick.getData().get(postions).getScoreObjective() + "分");
        } else {
            scoreTv.setText("客观题得分：--");
        }
        if (examVo.getExtend().equals("2")) {
            sad.setImageResource(R.mipmap.cucao_sel);
            happy.setImageResource(R.mipmap.youxiu_nor);
            sadFlag = false;
            happyFlag = true;

        } else if (examVo.getExtend().equals("1")) {
            happy.setImageResource(R.mipmap.youxiu_sel);
            sad.setImageResource(R.mipmap.cucao_nor);
            happyFlag = false;
            sadFlag = true;
        } else if (examVo.getExtend().equals("3")) {
            happy.setImageResource(R.mipmap.youxiu_nor);
            sad.setImageResource(R.mipmap.cucao_nor);
        }

        if (examVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < examVo.getData().size(); i++) {
                correctScoreAdapter = new CorrectScoreAdapter(examVo.getData(), examVo.getData().get(i).getAnswer().getExamList(), getActivity());
                //if (examVo.getData().get(0).getScore() > 0) {
            }
            listview.setAdapter(correctScoreAdapter);
            //打分adapter
            allNum.setText(examVo.getTotal() + "");
            mList.clear();
            mList.addAll(examVo.getData());
            mTopAdapter.clear();
            mTopAdapter.add(examVo.getData());
            refreshAdapter();
            notNum.setText(examVo.getNum());
            llTaskEmpty.setVisibility(View.GONE);

        }
    }


    private StudentTaskExamListVo studentTaskExamListVo;
    private int taskpostions = 0;

    /*按题批改，查询所有题号*/
    @Override
    public void setStudentTaskExamList(final StudentTaskExamListVo studentTaskExamList) {
        studentTaskExamListVo = studentTaskExamList;
        markingTopBarAdapter.clear();
        if (studentTaskExamList.getData().size() >= 1) {
            markingTopBarAdapter.add(studentTaskExamList.getData());
        }
        markingTopBarAdapter.notifyDataSetChanged();
        mPresenter.getAnswerExamCorrectAllStudent(studentTaskExamList.getData().get(taskpostions).getId());
        markingTopBarAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                taskpostions = position;
                markingTopBarAdapter.current_index = position;
                markingTopBarAdapter.notifyDataSetChanged();
                /*按题批改，查询某道题的所有学生作答结果，并组织老师批语*/
                mPresenter.getAnswerExamCorrectAllStudent(studentTaskExamList.getData().get(position).getId());
            }
        });
    }


    /*按题批改做题结果列表*/
    @Override
    public void setAnswerExamCorrectAllStudent(AnswerExamCorrectAllStudentVo examVo) {
        answerExamCorrectAllStudentVo = examVo;
         /*for (int i = 0; i < examVo.getData().size(); i++) {
            AnswerDetails.add(examVo.getData().get(i).getAnswer());
        }*/
        if (examVo.getData().size() == 0) {
            /*没有数据隐藏掉列表*/
            llTaskEmptyMarking.setVisibility(View.VISIBLE);
            recyclerViewMarking.setVisibility(View.GONE);
        } else {
            /*有数据隐藏掉暂无数据*/
            llTaskEmptyMarking.setVisibility(View.GONE);
            recyclerViewMarking.setVisibility(View.VISIBLE);
            //切换清空数据
            answerlist.clear();
            allMarkingNum.setText(examVo.getTotal() + "");//总题数
            notMarkingNum.setText(examVo.getNum() + "");//未批改
            for (int i = 0; i < examVo.getData().size(); i++) {
                markingcorrectScoreAdapter = new MarkingcorrectScoreAdapter(examVo.getData(), examVo.getData().get(i).getAnswer().getExamList(), getActivity());//打分adapter
            }
            markingListview.setAdapter(markingcorrectScoreAdapter);
            //集合添加数据
            answerlist.addAll(examVo.getData());
            markingTopBarAdapter.notifyDataSetChanged();
            answerExamCorrectAllStudentAdapter.notifyDataSetChanged();
            //更新列表
            // refreshAdapter();
        }
    }


    //按人批改提交以后刷新的界面
    @Override
    public void setCorrectSave() {
        //按人批改的学生列表
        mPresenter.getStudentList(id, classId);
    }

    //按题批改提交以后刷新的界面
    @Override
    public void setCorrectTopicSave() {
        //按题批改
        mPresenter.getStudentTaskExamList(id, 0, 1);
    }

    /*刷新界面*/
    private void refreshAdapter() {
        studentDetailAdapter.notifyDataSetChanged();
        mTopAdapter.notifyDataSetChanged();
        answerExamCorrectAllStudentAdapter.notifyDataSetChanged();
        markingTopBarAdapter.notifyDataSetChanged();
    }

    //初始获取传过来的参数
    private void initDatatest() {
        if (groupFlag == false) {
            correctionPerson.setVisibility(View.VISIBLE);
            markingTitle.setVisibility(View.GONE);
            //按人批改的学生列表
            mPresenter.getStudentList(id, classId);
        } else {
            markingTitle.setVisibility(View.VISIBLE);
            correctionPerson.setVisibility(View.GONE);
            mPresenter.getStudentTaskExamList(id, 0, 1);
        }
    }

    //初始加载数据
    @Override
    public void onResume() {
        super.onResume();
        initDatatest();
    }

/*    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && data != null && data.getStringArrayListExtra("imgs") != null
                && studentDetailAdapter != null
                && studentDetailAdapter.mClickStudentDetailAdapter != null) {

            studentDetailAdapter.mClickStudentDetailAdapter.clearAllData(data.getStringArrayListExtra("imgs"));
            int postion = data.getIntExtra("postion", 0);
            StringBuilder s = new StringBuilder();
            for (String s1 : data.getStringArrayListExtra("imgs")) {
                s.append(s1).append(",");
            }
            mList.get(postion).getAnswer().getExamOptionList().get(postion).setMyAnswer(s.toString());
            studentDetailAdapter.notifyItemChanged(postion);
        }
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    //按人批改切换
    public void allSee() {
        Log.e("aaatest", "隐藏按题批改");
      /*  correctionPerson.setVisibility(View.VISIBLE);
        markingTitle.setVisibility(View.GONE);*/
        groupFlag = false;
        initDatatest();
    }

    //按题批改切换
    public void groupSee() {
        Log.e("aaatest", "隐藏按人批改");
        /*markingTitle.setVisibility(View.VISIBLE);
        correctionPerson.setVisibility(View.GONE);*/
        groupFlag = true;
        initDatatest();
        // mPresenter.getStudentTaskExamList(id, 0, 1);
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.happy) {
            if (happyFlag) {
                happy.setImageResource(R.mipmap.youxiu_sel);
                sad.setImageResource(R.mipmap.cucao_nor);
                happyFlag = false;
                sadFlag = true;
                mPresenter.updateState(updateId, "1");
            } else {
                happy.setImageResource(R.mipmap.youxiu_nor);
                happyFlag = true;
                mPresenter.updateState(updateId, "3");
            }
        } else if (viewId == R.id.sad) {
            if (sadFlag) {
                sad.setImageResource(R.mipmap.cucao_sel);
                happy.setImageResource(R.mipmap.youxiu_nor);
                sadFlag = false;
                happyFlag = true;
                mPresenter.updateState(updateId, "2");
            } else {
                sad.setImageResource(R.mipmap.cucao_nor);
                sadFlag = true;
                mPresenter.updateState(updateId, "3");
            }
        } else if (viewId == R.id.save_tv) {
            if (listVo.getData() != null || Details != null) {
                correctScoreAdapter.getCorrectParams();
                mPresenter.correctSave(correctScoreAdapter.getCorrectParams(), id, updateId, correctUserType);
            }
        } else if (viewId == R.id.marking_save_tv) {
            if (studentTaskExamListVo.getData() != null || answerExamCorrectAllStudentVo != null) {
                markingcorrectScoreAdapter.getCorrectParams();
                for (int i = 0; i < answerExamCorrectAllStudentVo.getData().size(); i++) {

                }
                mPresenter.markingSave(markingcorrectScoreAdapter.getCorrectParams());// markingcorrectScoreAdapter.getAnswerCorrectParams()
            }
        }
    }
}
