package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.PointVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/7
 * 修改单号：
 * 修改内容:
 */
public interface PointFragmentView extends BaseView {

    void setPointVo(PointVo pointVo);

    void onError();
}
