package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.OverallVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.student.libutils.annotations.CheckState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class OverallDetailAdapter extends BaseRcvAdapter<OverallVo.DataBean.AnswerExamListBean> {

    private Activity mActivity = null;
    private boolean showAnswer = false;//答案

    public OverallDetailAdapter(Context context, List<OverallVo.DataBean.AnswerExamListBean> mList, Activity activity) {
        super(context, mList);
        mActivity = activity;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        OverallVo.DataBean.AnswerExamListBean bean = mList.get(position);
        return bean.getTemplateId();
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 14://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 16://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, OverallVo.DataBean.AnswerExamListBean bean) {

        switch (bean.getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                // bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                // bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindShortAnswer(holder, position, bean);//出错
                break;
        }
    }


    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final OverallVo.DataBean.AnswerExamListBean bean) {

        bindExamTitle(holder, position, bean);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        final TextView textView = holder.getView(R.id.score_tv_pigai);
        if (bean.getScore() > 0) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(bean.getScore() + "分");
        } else {
            textView.setVisibility(View.GONE);
        }
        RecyclerView rv = holder.getView(R.id.rv_add_pic);
        rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(context);
            rv.setAdapter(adapter);
            adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
                @Override
                public void clickImage(int index, String url) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(url);
                    correctData.setSeq(index + "");
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mActivity.startActivity(intent);
                }
            });
        }

        // List<StudentDetails.ExamOptionBean> examOptionList = bean.getExamOptionList();
        List<OverallVo.DataBean.AnswerExamListBean.ExamOptionListBean> examList = bean.getExamOptionList();
        for (OverallVo.DataBean.AnswerExamListBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
    }


    private boolean goodFlag = true;
    private boolean errorFlag = true;

    private void bindExamTitle(final BaseViewHolder holder, int position, OverallVo.DataBean.AnswerExamListBean bean) {
        final String id = String.valueOf(bean.getId());
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textType = holder.getView(R.id.tv_type);
        /*        textCount.setText(position + 1 + ". " + bean.getTemplateStyleName() + "");*/

        if (bean.getFullScore() > -1) {
            if (bean.getTemplateId() == 6) {
                textCount.setText(bean.getExamSeq() + ". (本题" + (bean.getFullScore() * bean.getExamOptionList().size()) + "分)")
                ;

            } else {
                textCount.setText(bean.getExamSeq() + ". (本题" + bean.getFullScore() + "分)");
            }

        } else {

            textCount.setText(bean.getExamSeq() + ".");
        }
        if (bean.getTemplateStyleName() != null) {
            textType.setText(bean.getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }


        //textType.setVisibility(View.GONE);
        // textType.setText(bean.getExamTextVo().getChapterNodeNamePath());
        holder.getView(R.id.error_ly).setVisibility(View.GONE);

        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                }
            }
        });
        final ImageView good = holder.getView(R.id.good);
        final ImageView error = holder.getView(R.id.error);
        holder.getView(R.id.good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {

                    good.setImageResource(R.mipmap.dx_sel);
                    goodFlag = false;
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                }
            }
        });

        holder.getView(R.id.error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    errorFlag = false;
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                }
            }
        });
        //答案部分
        holder.getView(R.id.rl_result).setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        ExamText examTextVo = bean.getExamTextVo();


        if (examTextVo == null) {
            return;
        }
        result.setHtmlText(examTextVo.getExamAnswer());
        analysis.setHtmlText(examTextVo.getExamAnalysis());
    }

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final StudentDetails.DataBean bean) {
        TextView textCount = holder.getView(R.id.tv_count);
        HtmlTextView htmlTextView = holder.getView(R.id.tv_exam);
        TextView textType = holder.getView(R.id.tv_type);
        RecyclerView examListView = holder.getView(R.id.lv);
        if (bean.getAnswer().getExamList().size() == 0) {
            textCount.setVisibility(View.GONE);
            htmlTextView.setVisibility(View.GONE);
            textType.setVisibility(View.GONE);
            examListView.setVisibility(View.GONE);
            return;
        }
        final List<StudentDetails.DataBean.AnswerBean> list = new ArrayList();
        final String id = String.valueOf(bean.getAnswer().getId());


        textCount.setText(position + 1 + "");
        textType.setText(bean.getAnswer().getExamTextVo().getTemplateStyleName());
        htmlTextView.setHtmlText(bean.getAnswer().getExamTextVo().getExamStem());

        list.addAll(bean.getAnswer().getExamList());
        examListView.setLayoutManager(new LinearLayoutManager(context));
        final StudentDetailAllAdapter allAnserAdapter = new StudentDetailAllAdapter(context, list, mActivity);
        examListView.setAdapter(allAnserAdapter);

    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final OverallVo.DataBean.AnswerExamListBean bean) {
        bindExamTitle(holder, position, bean);

        ExamText examTextVo = bean.getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            if (options.get(i).getSeq() == bean.getExamOptionList().get(0).getSeq()) {

                if (bean.getIsRight() == 2) {
                    tvNumber.setSelected(true);
                } else {
                    tvNumber.setSelected(true);
                    tvNumber.setBackground(mActivity.getResources().getDrawable(R.drawable.bg_select_selector_2));
                }
            } else {
                tvNumber.setSelected(false);
            }

        }
    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final OverallVo.DataBean.AnswerExamListBean bean) {
        bindExamTitle(holder, position, bean);
        FillHtmlTextView view = holder.getView(R.id.tv);
        view.setVisibility(View.GONE);
        final FillBlankCheckTextView tv = holder.getView(R.id.FillBlankCheckTextView);
        tv.setVisibility(View.VISIBLE);
        SparseArray<ClozeAnswerBean> array = new SparseArray<>();//作答结果

        List<OverallVo.DataBean.AnswerExamListBean.ExamOptionListBean> examOptionList = bean.getExamOptionList();
        // List<StudentDetails.ExamOptionBean> examOptionList = bean.getExamOptionList();

        for (int i = 0; i < examOptionList.size(); i++) {
            OverallVo.DataBean.AnswerExamListBean.ExamOptionListBean optionBean = examOptionList.get(i);

            int checkState;

            if (bean.getIsCorrect() != 0) {//todo,2019年2月27日15:39:323
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(), optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        tv.setHtmlBody(bean.getExamTextVo().getExamStem(), array);
        tv.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                CorrectDataVo correctData = new CorrectDataVo();

                correctData.setSrc(imageSrc);
                correctData.setSeq("");
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                intent.putExtra("correctData", correctData);
                intent.putExtra("vid", 0);
                mActivity.startActivity(intent);
            }
        });

    }
}
