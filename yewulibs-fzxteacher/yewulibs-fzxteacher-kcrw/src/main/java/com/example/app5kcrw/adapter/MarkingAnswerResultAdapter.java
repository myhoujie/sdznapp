package com.example.app5kcrw.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 简答题adapter
 *
 * @author Reisen at 2018-08-29
 */
public class MarkingAnswerResultAdapter extends BaseRcvAdapter<String> {
    private OnClickListener mListener;
    private ArrayList<String> rowList;

    public MarkingAnswerResultAdapter(Context context) {
        super(context, R.layout.item_marking_result_pic, new ArrayList<String>());
    }

    public void setList(String[] arr, String[] arr2) {
        mList.clear();
        mList.addAll(Arrays.asList(arr));
        if (arr2 != null) {
            rowList = new ArrayList<>(Arrays.asList(arr2));
        } else {
            rowList = null;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        convert(holder, position, mList.get(position));
    }

    @Override
    public void convert(BaseViewHolder holder, final int position, String s) {
        ImageView ivImage = holder.getView(R.id.iv_image);
        ivImage.setVisibility(View.VISIBLE);
       /* Glide.with(context).
                load(mList.get(position)).error(R.mipmap.cuo_sel). //异常时候显示的图片
                placeholder(R.mipmap.daojishi_nor). //加载成功前显示的图片
                fallback(R.mipmap.img). //url为空的时候,显示的图片
                into(ivImage);//在RequestBuilder 中使用自定义的ImageViewTarget*/
        Glide.with(context).load(mList.get(position)).into(ivImage);

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.clickImage(position, rowList == null ?
                            mList.get(position) : rowList.get(position));
                }
            }
        });
    }

    public void setListener(OnClickListener listener) {
        mListener = listener;
    }

    public interface OnClickListener {
        void clickImage(int index, String url);
    }
}
