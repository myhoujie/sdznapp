package com.example.app5kcrw.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5kcrw.adapter.StudentDetailAdapter;
import com.example.app5libbase.baseui.adapter.StudentDetailListViewAdapter;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.StudentDetailsPresenter;
import com.example.app5kcrw.view.StudentDetailsView;
import com.sdzn.fzx.teacher.vo.StatisticsVo;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

/**
 * 学生详情
 */
public class StudentDetailsActivity extends MBaseActivity<StudentDetailsPresenter> implements StudentDetailsView, View.OnClickListener {
    private TextView tvBack;
    private TextView tvTitle;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private ImageView photo;
    private TextView name;
    private TextView state;
    private TextView scoreTv;
    private TextView correctState;
    private TextView score;
    private TextView avg;
    private TextView avgTv;
    private TextView time;
    private TextView timeTv;
    private ImageView happy;
    private ImageView sad;
    private RecyclerView mRecyclerView;
    private ListView listview;


    private String lessonTaskStudentId;
    private String id;
    private String nametext;
    private int useTime;
    private String scoreTotal;
    private StudentDetailAdapter studentDetailAdapter;
    private List<StudentDetails.DataBean> mList = new ArrayList<>();

    @Override
    public void initPresenter() {
        mPresenter = new StudentDetailsPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_datil);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        photo = (ImageView) findViewById(R.id.photo);
        name = (TextView) findViewById(R.id.name);
        state = (TextView) findViewById(R.id.state);
        scoreTv = (TextView) findViewById(R.id.score_tv);
        correctState = (TextView) findViewById(R.id.correct_state);
        score = (TextView) findViewById(R.id.score);
        avg = (TextView) findViewById(R.id.avg);
        avgTv = (TextView) findViewById(R.id.avg_tv);
        time = (TextView) findViewById(R.id.time);
        timeTv = (TextView) findViewById(R.id.time_tv);
        happy = (ImageView) findViewById(R.id.happy);
        sad = (ImageView) findViewById(R.id.sad);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        listview = (ListView) findViewById(R.id.listview);
        happy.setOnClickListener(this);
        sad.setOnClickListener(this);
        tvBack.setOnClickListener(this);

        studentDetailAdapter = new StudentDetailAdapter(App2.get(), mList, this, 0);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        mRecyclerView.setAdapter(studentDetailAdapter);
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入StudentDetailsActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        lessonTaskStudentId = getIntent().getIntExtra("id", 0) + "";
        useTime = getIntent().getIntExtra("useTime", 0);
        id = getIntent().getStringExtra("lessonTaskId");
        id = getIntent().getStringExtra("lessonTaskId");

        tvTitle.setText(getIntent().getStringExtra("name"));
        scoreTotal = getIntent().getStringExtra("scoreTotal");
        nametext = getIntent().getStringExtra("userStudentName");
        mPresenter.getStudentDetailList(lessonTaskStudentId);
        mPresenter.getStudentStatistics(id);
    }

    @Override
    public void setStudentDetail(StudentDetails examVo) {


        if (examVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {


            if (examVo.getExtend().equals("2")) {
                sad.setImageResource(R.mipmap.cucao_sel);
                happy.setImageResource(R.mipmap.youxiu_nor);
                sadFlag = false;
                happyFlag = true;

            } else if (examVo.getExtend().equals("1")) {
                happy.setImageResource(R.mipmap.youxiu_sel);
                sad.setImageResource(R.mipmap.cucao_nor);
                happyFlag = false;
                sadFlag = true;
            } else if (examVo.getExtend().equals("3")) {
                happy.setImageResource(R.mipmap.youxiu_nor);
                sad.setImageResource(R.mipmap.cucao_nor);
            }


            llTaskEmpty.setVisibility(View.GONE);
            mList.clear();
            mList.addAll(examVo.getData());
            refreshAdapter();
            listview.setAdapter(new StudentDetailListViewAdapter(examVo.getData(), StudentDetailsActivity.this));
        }
    }

    @Override
    public void StudentStatics(StatisticsVo statisticsVo) {

        name.setText(nametext);
        if (TextUtils.equals(scoreTotal, "-1")) {
            scoreTv.setText("--");
        } else {
            scoreTv.setText("" + scoreTotal + "分");

        }
        if (statisticsVo.getData().getScope() == -1) {
            score.setText("--");
        } else {
            score.setText(statisticsVo.getData().getScope() + "分");
        }
        if (statisticsVo.getData().getScoreAvg() == -1) {
            avgTv.setText("--");
        } else {
            if (statisticsVo.getData().getScoreAvg() != 0) {
                avgTv.setText(statisticsVo.getData().getScoreAvg() + "分");

            }

        }

        timeTv.setText(useTime % (24 * 60 * 60) / (60 * 60) + "小时" + useTime % (24 * 60 * 60) % (60 * 60) / 60 + "分" + useTime % (24 * 60 * 60) % (60 * 60) % 60 + "秒");


    }

    @Override
    public void updateStatics() {
        initData();
    }

    private void refreshAdapter() {
        studentDetailAdapter.notifyDataSetChanged();
    }


    private boolean happyFlag = true;
    private boolean sadFlag = true;

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.happy) {
            if (happyFlag) {
                happy.setImageResource(R.mipmap.youxiu_sel);
                sad.setImageResource(R.mipmap.cucao_nor);
                happyFlag = false;
                sadFlag = true;
                mPresenter.updateState(lessonTaskStudentId, "1");
            } else {
                happy.setImageResource(R.mipmap.youxiu_nor);
                happyFlag = true;
            }
        } else if (viewId == R.id.sad) {
            if (sadFlag) {
                sad.setImageResource(R.mipmap.cucao_sel);
                happy.setImageResource(R.mipmap.youxiu_nor);
                sadFlag = false;
                happyFlag = true;
                mPresenter.updateState(lessonTaskStudentId, "2");
            } else {
                sad.setImageResource(R.mipmap.cucao_nor);
                sadFlag = true;
            }
        } else if (viewId == R.id.tvBack) {
            finish();
        }
    }
}
