package com.example.app5kcrw.presenter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.correctFragmentView;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.CorrectParam;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.MarkingCorrectParam;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.teacher.vo.StudentListVo;
import com.sdzn.fzx.teacher.vo.StudentTaskExamListVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class correctFragmentPresenter extends BasePresenter<correctFragmentView, BaseActivity> {

    //按人批改的头部信息
    public void getStudentList(String lessonTaskId, String classId) {
        Network.createTokenService(NetWorkService.GetCottectStudentListService.class)
                .StudentStatisc(lessonTaskId, classId)
                .map(new StatusFunc<StudentListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentListVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(StudentListVo studentListVo) {
                        mView.setStudentListVo(studentListVo);
                    }
                });

                        /*new ProgressSubscriber<StudentListVo>(new SubscriberListener<StudentListVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentListVo o) {
                        mView.setStudentListVo(o);
                    }
                }, mActivity));*/
    }

    //学生做题列表
    public void getStudentDetailList(String id, String examType) {
        Network.createTokenService(NetWorkService.GetStudentDetailTwoService.class)
                .StudentStatisc(id, examType)
                .map(new StatusFunc<StudentDetails>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StudentDetails>(new SubscriberListener<StudentDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentDetails o) {
                        int i = initData(o);
                        o.setNum(i + "");
                        mView.setStudentDetail(o);
                    }
                }, mActivity));
    }


    /*按题批改获取top列表数据*/
    public void getStudentTaskExamList(String lessonTaskId, int type, int queryExamType) {
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .GetStudentTaskExamList(lessonTaskId, type, queryExamType)
                .map(new StatusFunc<StudentTaskExamListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentTaskExamListVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(StudentTaskExamListVo studentTaskExamListVo) {
                        mView.setStudentTaskExamList(studentTaskExamListVo);
                    }
                });/*new ProgressSubscriber<StudentTaskExamListVo>(new SubscriberListener<StudentTaskExamListVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentTaskExamListVo o) {
                        mView.setStudentTaskExamList(o);
                    }
                }, mActivity));*/
    }

    /*按题批改，查询某道题的所有学生作答结果，并组织老师批语*/
    public void getAnswerExamCorrectAllStudent(int lessonTaskDetailsId) {
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .GetAnswerExamCorrectAllStudent(lessonTaskDetailsId)
                .map(new StatusFunc<AnswerExamCorrectAllStudentVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<AnswerExamCorrectAllStudentVo>(new SubscriberListener<AnswerExamCorrectAllStudentVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(AnswerExamCorrectAllStudentVo o) {
                        int i = initDataAnswer(o);
                        o.setNum(i + "");
                        mView.setAnswerExamCorrectAllStudent(o);
                    }
                }, mActivity));
    }

    //改变批改状态
    public void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateStudentExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("操作成功");

                    }
                });
    }

    private int examtype;

    //提交按人批改
    public void correctSave(List<CorrectParam> correctParams, String id, String lessonTaskStudentId, int correctUserType) {
        JsonArray array = new JsonArray();
        for (CorrectParam correctParam : correctParams) {
            if (correctParam.getExamTypeId() != 16) {
                JsonObject subJsonObj = new JsonObject();
                subJsonObj.addProperty("id", correctParam.getId());
                subJsonObj.addProperty("parentId", correctParam.getParentId());
                subJsonObj.addProperty("isRight", correctParam.getIsRight());
                subJsonObj.addProperty("score", correctParam.getScore());
                subJsonObj.addProperty("fullScore", correctParam.getFullScore());
                subJsonObj.addProperty("isCorrent", correctParam.getIsCorrent());
                subJsonObj.addProperty("examTypeId", correctParam.getExamTypeId());
                if (correctParam.getExamTypeId() == 6) {
                    JsonArray childrenArray = new JsonArray();
                    for (CorrectParam ChildrenCorrectParam : correctParam.getChildren()) {
                        JsonObject ChildrenJsonObj = new JsonObject();
                        // {"id":512053,"parentId":0,"isRight":3,"score":2,"fullScore":5,"isCorrent":1}
                        ChildrenJsonObj.addProperty("id", ChildrenCorrectParam.getId());
                        ChildrenJsonObj.addProperty("parentId", ChildrenCorrectParam.getParentId());
                        ChildrenJsonObj.addProperty("isRight", ChildrenCorrectParam.getIsRight());
                        if (ChildrenCorrectParam.getIsRight() == -1 || ChildrenCorrectParam.getIsRight() == 0) {
                            ChildrenJsonObj.addProperty("score", 0);
                        } else {
                            ChildrenJsonObj.addProperty("score", ChildrenCorrectParam.getScore());
                        }
                        ChildrenJsonObj.addProperty("fullScore", ChildrenCorrectParam.getFullScore());
                        ChildrenJsonObj.addProperty("isCorrent", ChildrenCorrectParam.getIsCorrent());
                        childrenArray.add(ChildrenJsonObj);
                    }
                    subJsonObj.add("children", childrenArray);
                }
                array.add(subJsonObj);
            }
        }
        HashMap<String, Object> param = new HashMap<>();
        param.put("lessonTaskId", id);
        param.put("lessonTaskStudentId", lessonTaskStudentId);
        param.put("correctUserType", 1);//correctUserType//1老师-2学生
        param.put("correctJson", array);
        Log.e("json", "json" + array);
        Network.createTokenService(NetWorkService.getCorrectSaveListService.class)
                .AnalyzeStatisc(param)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("操作成功");
                        mView.setCorrectSave();
                    }
                }, mActivity));
    }

    //提交按题批改
    public void markingSave(List<MarkingCorrectParam> correctParams) {
        String json = createJson(correctParams);
        Log.e("json", "json" + json);
        Network.createTokenService(NetWorkService.CorrectingWayService.class)
                .correctByExamSave(json)
                .map(new StatusFunc<AnswerExamCorrectAllStudentVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("操作成功");
                        mView.setCorrectTopicSave();

                    }
                }, mActivity));
    }

    private String createJson(List<MarkingCorrectParam> list) {
        List<AnswerExamCorrectAllStudentVo.SubmitBean> beans = new ArrayList<>();
        for (MarkingCorrectParam markingCorrectParam : list) {
            if (markingCorrectParam.getExamTypeId() != 16) {
                AnswerExamCorrectAllStudentVo.SubmitBean submitBean = new AnswerExamCorrectAllStudentVo.SubmitBean();//拿到的参数塞入submitBean
                submitBean.setLessonTaskId(markingCorrectParam.getLessonTaskId());
                submitBean.setLessonTaskStudentId(markingCorrectParam.getLessonTaskStudentId());

                JsonArray array = new JsonArray();
                JsonObject subJsonObj = new JsonObject();
                subJsonObj.addProperty("id", markingCorrectParam.getId());
                subJsonObj.addProperty("parentId", markingCorrectParam.getParentId());
                subJsonObj.addProperty("isRight", markingCorrectParam.getIsRight());
                subJsonObj.addProperty("score", markingCorrectParam.getScore());
                subJsonObj.addProperty("fullScore", markingCorrectParam.getFullScore());
                subJsonObj.addProperty("isCorrent", markingCorrectParam.getIsCorrent());
                subJsonObj.addProperty("examTypeId", markingCorrectParam.getExamTypeId());
                subJsonObj.addProperty("lessonTaskId", markingCorrectParam.getLessonTaskId());
                subJsonObj.addProperty("lessonTaskStudentId", markingCorrectParam.getLessonTaskStudentId());
                if (markingCorrectParam.getExamTypeId() == 6) {
                    JsonArray childrenArray = new JsonArray();
                    for (MarkingCorrectParam ChildrenCorrectParam : markingCorrectParam.getChildren()) {
                        JsonObject ChildrenJsonObj = new JsonObject();
                        ChildrenJsonObj.addProperty("id", ChildrenCorrectParam.getId());
                        ChildrenJsonObj.addProperty("parentId", ChildrenCorrectParam.getParentId());
                        ChildrenJsonObj.addProperty("isRight", ChildrenCorrectParam.getIsRight());
                        if (ChildrenCorrectParam.getIsRight() == -1 || ChildrenCorrectParam.getIsRight() == 0) {
                            ChildrenJsonObj.addProperty("score", 0);
                        } else {
                            ChildrenJsonObj.addProperty("score", ChildrenCorrectParam.getScore());
                        }
                        ChildrenJsonObj.addProperty("fullScore", ChildrenCorrectParam.getFullScore());
                        ChildrenJsonObj.addProperty("isCorrent", ChildrenCorrectParam.getIsCorrent());
                        ChildrenJsonObj.addProperty("lessonTaskId", markingCorrectParam.getLessonTaskId());
                        ChildrenJsonObj.addProperty("lessonTaskStudentId", markingCorrectParam.getLessonTaskStudentId());
                        childrenArray.add(ChildrenJsonObj);
                    }
                    subJsonObj.add("children", childrenArray);
                    array.add(subJsonObj);
                }
                String json = GsonUtil.toJson(subJsonObj);
                submitBean.setCorrectJson(String.valueOf(json));
                beans.add(submitBean);
            }
        }
        return GsonUtil.toJson(beans);
    }


    private int initDataAnswer(AnswerExamCorrectAllStudentVo beans) {
        int count = 0;
        List<AnswerExamCorrectAllStudentVo.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return 0;
        }
        for (AnswerExamCorrectAllStudentVo.DataBean bean : list) {
            if (bean.getAnswer().getIsCorrect() == 0) {
                count++;
            }
            String resourceText = bean.getAnswer().getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.getAnswer().setExamTextVo(examText);
            if (bean.getAnswer().getTemplateId() != 16) {
                continue;
            }
            for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean examListBean : bean.getAnswer().getExamList()) {
                ExamText jsons = gson.fromJson(examListBean.getExamText(), ExamText.class);
                examListBean.setExamTextVo(jsons);
            }
            //List<AnswerExamCorrectAllStudentVo.DataBean> examList = bean.getAnswer().getExamList();
            /*for (AnswerExamCorrectAllStudentVo.DataBean dataBean : bean.getAnswer().getExamList()) {
                AnswerExamCorrectAllStudentVo.DataBean.AnswerBean answer = dataBean.getAnswer();
                if (answer == null)
                    continue;
                String examTextStr = answer.getExamText();
                if (examTextStr == null)
                    continue;
                try {
                    ExamText json = gson.fromJson(examTextStr, ExamText.class);
                    answer.setExamTextVo(json);
                } catch (Exception e) {
                    System.out.println("异常:");
                    System.out.println("examText:\n" + examTextStr);
                    e.printStackTrace();
                    System.out.println("异常结束");
                }
            }*/
        }
        return count;
    }


    private int initData(StudentDetails beans) {
        int count = 0;
        List<StudentDetails.DataBean> list = beans.getData();//拿到外层data
        Gson gson = new Gson();
        if (list == null) {
            return 0;
        }
        for (StudentDetails.DataBean bean : list) {
            if (bean.getAnswer().getIsCorrect() == 0) {
                count++;
            }
            String resourceText = bean.getAnswer().getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.getAnswer().setExamTextVo(examText);
            if (bean.getAnswer().getTemplateId() != 16) {
                continue;
            }
            //   List<StudentDetails.DataBean.ExamListBean> examList = bean.getExamList();
            for (StudentDetails.DataBean.AnswerBean dataBean : bean.getAnswer().getExamList()) {
                ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
                dataBean.setExamTextVo(json);
            }
        }
        return count;
    }
}
