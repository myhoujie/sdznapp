package com.example.app5kcrw.presenter;

import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5kcrw.view.analyzeFragmentView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;
import com.sdzn.fzx.teacher.vo.AnalyzeStatisticsVo;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamAnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamText;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class analyzePresenter extends BasePresenter<analyzeFragmentView, BaseActivity> {

    public void getAnalyzeList(String lessonTaskId, String parentId, String type) {
        Network.createTokenService(NetWorkService.GetAnalyzeListService.class)
                .AnalyzeStatisc(lessonTaskId, parentId, type)
                .map(new StatusFunc<AnalyzeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AnalyzeVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(AnalyzeVo o) {
                        initData(o);
                        mView.setAnalyzeVo(o);

                    }
                });

    }

    private void initData(AnalyzeVo beans) {
        List<AnalyzeVo.DataBean> list = beans.getData();
        Gson gson = new Gson();
        if (list == null) {
            return;
        }
        for (AnalyzeVo.DataBean bean : list) {

            String resourceText = bean.getExamText();
            ExamText examText = gson.fromJson(resourceText, ExamText.class);
            bean.setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
            //综合题额外进行一轮解析
            if (bean.getExamTemplateId() != 16) {
                continue;
            }
            //   List<StudentDetails.DataBean.ExamListBean> examList = bean.getExamList();
            for (AnalyzeVo.DataBean dataBean : bean.getChildList()) {
                ExamText json = gson.fromJson(dataBean.getExamText(), ExamText.class);
                dataBean.setExamTextVo(json);
            }
        }
    }


    public void getexamNumberList(String lessonTaskId) {
        Network.createTokenService(NetWorkService.getexamNumberList.class)
                .AnalyzeStatisc(lessonTaskId)
                .map(new StatusFunc<AnalyzeNumberVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AnalyzeNumberVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(AnalyzeNumberVo o) {
                        mView.setAnalyzeNumberVo(o);

                    }
                });

    }


    public void getexamStudentList(String lessonTaskId,String lessonTaskDetailsId) {
        Network.createTokenService(NetWorkService.getexamStudentList.class)
                .AnalyzeStatisc(lessonTaskId,lessonTaskDetailsId)
                .map(new StatusFunc<AnalyzeStatisticsVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AnalyzeStatisticsVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(AnalyzeStatisticsVo o) {
                        mView.setAnalyzeStatisticsVo(o);

                    }
                });

    }

    public void getexamAnalyzeList(String lessonTaskDetailsId) {
        Network.createTokenService(NetWorkService.GetExamAnalyseListService.class)
                .GetStudentList(lessonTaskDetailsId)
                .map(new StatusFunc<ExamAnalyzeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ExamAnalyzeVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(ExamAnalyzeVo o) {
                        mView.setExamAnalyzeStatisticsVo(o);

                    }
                });

    }

}
