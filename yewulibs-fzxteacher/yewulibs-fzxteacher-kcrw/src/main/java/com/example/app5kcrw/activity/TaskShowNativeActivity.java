package com.example.app5kcrw.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5kcrw.adapter.PreviewNativeAdapter;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5kcrw.presenter.TaskShowNativePresenter;
import com.example.app5kcrw.view.TaskShowNativeView;
import com.sdzn.fzx.teacher.vo.ExamVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 原生预览界面
 */
public class TaskShowNativeActivity extends MBaseActivity<TaskShowNativePresenter> implements TaskShowNativeView {


    private TextView tvBack;
    private TextView tvTitle;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private RecyclerView mRecyclerView;


    private String name;
    private String lessonId;
    private String libId;
    private PreviewNativeAdapter previewNativeAdapter;
    private List<ExamVo.DataBean> mList = new ArrayList<>();

    @Override
    public void initPresenter() {
        mPresenter = new TaskShowNativePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_show_native);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        previewNativeAdapter = new PreviewNativeAdapter(App2.get(), mList, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        mRecyclerView.setAdapter(previewNativeAdapter);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入TaskShowNativeActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        name = getIntent().getStringExtra("name");
        lessonId = getIntent().getStringExtra("lessonId");
        libId = getIntent().getStringExtra("libId");
        HashMap<String, String> param = new HashMap<>();
        param.put("lessonId", lessonId);
        param.put("libId", libId);
        tvTitle.setText(name);
        mPresenter.getExamList(param);
    }


    @Override
    public void setExamList(ExamVo examVo) {
        if (examVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {
            llTaskEmpty.setVisibility(View.GONE);
            mList.clear();
            mList.addAll(examVo.getData());
            refreshAdapter();
            //errorSubjectNativeAdapter.refreshAdapter();
        }
    }

    private void refreshAdapter() {
        previewNativeAdapter.notifyDataSetChanged();
    }
}
