package com.example.app5kcrw.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.ExamText;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */

public class CopyAnswerExamStudentDetailAllAdapter extends BaseRcvAdapter<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> {
    private List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> mList = null;
    private Context mContext;
    private boolean showAnswer;
    AnswerExamCorrectAllStudentVo.DataBean answerbean;

    public CopyAnswerExamStudentDetailAllAdapter(Context context, List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> List, AnswerExamCorrectAllStudentVo.DataBean bean) {
        super(context, List);
        this.mList = List;
        this.mContext = context;
        this.answerbean = bean;
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean = mList.get(position);
        return bean.getTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text_jianda);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text);
            default:
                return null;

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        switch (bean.getTemplateId()) {//试题
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            default:
                bindFillBlank(holder, position, bean);//出错
                break;
        }
    }

    /*设置头部*/
    private void bindExamTitle(final BaseViewHolder holder, int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        final TextView textCount = holder.getView(R.id.tv_count);
        final TextView textType = holder.getView(R.id.tv_type);
        final LinearLayout rlresult = holder.getView(R.id.rl_result);
        final HtmlTextView tvresult = holder.getView(R.id.tv_result);
        final HtmlTextView tvanalysis = holder.getView(R.id.tv_analysis);

        if (Double.valueOf(mList.get(position).getScore()) > -1) {
            textCount.setText(mList.get(position).getExamSeq() + ". (本题" + mList.get(position).getFullScore() + "分)");
        }
        textType.setText(getTemplateStyleName(mList.get(position).getExamTextVo().getExamTypeId()));

        //===================== 答案 =====================
        rlresult.setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnswer())) {
            tvresult.setHtmlText("略...");
        } else {
            tvresult.setHtmlText(bean.getExamTextVo().getExamAnswer() == null ? "" : bean.getExamTextVo().getExamAnswer());
        }
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnalysis())) {
            tvanalysis.setHtmlText("略...");
        } else {
            tvanalysis.setHtmlText(bean.getExamTextVo().getExamAnalysis() == null ? "" : bean.getExamTextVo().getExamAnalysis());
        }
    }


    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        final FillBlankCheckTextView fllBlankCheckTextView = holder.getView(R.id.FillBlankCheckTextView);
        fllBlankCheckTextView.setVisibility(View.VISIBLE);
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean = mList.get(position);
        if (dataBean.getExamTextVo() != null && dataBean.getExamTextVo().getExamStem() != null) {
            fllBlankCheckTextView.setHtmlBody(dataBean.getExamTextVo().getExamStem());
        }
    }

    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        HtmlTextView tvExam = holder.getView(R.id.tv_exam);
        final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        tvExam.setHtmlText(examTextVo.getExamStem());
    }

    String getTemplateStyleName(int num) {
        switch (num) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知";
        }
    }
}