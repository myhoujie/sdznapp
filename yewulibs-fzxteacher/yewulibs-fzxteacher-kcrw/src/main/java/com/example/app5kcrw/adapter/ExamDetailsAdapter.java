package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.AnalyzeStatisticsVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/25
 * 修改单号：
 * 修改内容:
 */
public class ExamDetailsAdapter extends BaseAdapter {

    private List<AnalyzeStatisticsVo.DataBean.ErrorStudentListBean> mlist;
    private Context mContext;

    public ExamDetailsAdapter(List<AnalyzeStatisticsVo.DataBean.ErrorStudentListBean> mlist, Context mContext) {
        this.mlist = mlist;
        this.mContext = mContext;
    }

    public void clear() {
        mlist.clear();
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_student_analyze, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mlist.get(position).getUserStudentName());

        holder.llAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.analyzeStudentActivity");
                intent.putExtra("id", mlist.get(position).getLessonAnswerExamId());
                intent.putExtra("name", mlist.get(position).getUserStudentName());
                ((Activity) mContext).startActivity(intent);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        ImageView photo;
        TextView name;
        LinearLayout llAnswer;

        ViewHolder(View view) {
            photo = view.findViewById(R.id.photo);
            name = view.findViewById(R.id.name);
            llAnswer = view.findViewById(R.id.ll_answer);
        }
    }
}
