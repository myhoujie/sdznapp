package com.example.app5kcrw.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.AnswerExamCorrectAllStudentVo;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.student.libutils.annotations.CheckState;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */

public class AnswerExamStudentDetailAllAdapter extends BaseRcvAdapter<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> {
    private List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> mList = null;
    private Context mContext;
    private boolean showAnswer;
    AnswerExamCorrectAllStudentVo.DataBean answerbean;

    public AnswerExamStudentDetailAllAdapter(Context context, List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean> List, AnswerExamCorrectAllStudentVo.DataBean bean) {
        super(context, List);
        this.mList = List;
        this.mContext = context;
        this.answerbean = bean;
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean = mList.get(position);
        return bean.getTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text_jianda);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text);
            default:
                return null;

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {

        switch (bean.getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }

    /*设置头部*/
    private void bindExamTitle(final BaseViewHolder holder, int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        final TextView textCount = holder.getView(R.id.tv_count);
        final TextView textType = holder.getView(R.id.tv_type);
        final LinearLayout rlresult = holder.getView(R.id.rl_result);
        final HtmlTextView tvresult = holder.getView(R.id.tv_result);
        final HtmlTextView tvanalysis = holder.getView(R.id.tv_analysis);

        if (Double.valueOf(mList.get(position).getScore()) > -1) {
            textCount.setText(mList.get(position).getExamSeq() + ". (本题" + mList.get(position).getFullScore() + "分)");
        }
        textType.setText(getTemplateStyleName(mList.get(position).getExamTextVo().getExamTypeId()));

        //===================== 答案 =====================
        rlresult.setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnswer())) {
            tvresult.setHtmlText("略...");
        } else {
            tvresult.setHtmlText(bean.getExamTextVo().getExamAnswer() == null ? "" : bean.getExamTextVo().getExamAnswer());
        }
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnalysis())) {
            tvanalysis.setHtmlText("略...");
        } else {
            tvanalysis.setHtmlText(bean.getExamTextVo().getExamAnalysis() == null ? "" : bean.getExamTextVo().getExamAnalysis());
        }
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {

    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        final FillBlankCheckTextView fllBlankCheckTextView = holder.getView(R.id.FillBlankCheckTextView);
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean = mList.get(position);
        fllBlankCheckTextView.setVisibility(View.VISIBLE);

        SparseArray<ClozeAnswerBean> array = new SparseArray<>();//作答结果
        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean> examOptionList = dataBean.getExamOptionList();
        for (int i = 0; i < examOptionList.size(); i++) {
            AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean optionBean = examOptionList.get(i);

            int checkState;

            if (dataBean.getIsCorrect() != 0) {//todo,2019年2月27日15:39:323
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(),
                    optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        if (dataBean.getExamTextVo() != null && dataBean.getExamTextVo().getExamStem() != null) {
            fllBlankCheckTextView.setHtmlBody(dataBean.getExamTextVo().getExamStem(), array);
        }

        fllBlankCheckTextView.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                if (!TextUtils.isEmpty(imageSrc)) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(imageSrc);
                    correctData.setSeq("");
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mContext.startActivity(intent);
                } else {
                    ToastUtil.showLonglToast("图片地址为空");
                }
            }
        });
    }

    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        HtmlTextView tvExam = holder.getView(R.id.tv_exam);//题干
        RecyclerView rvaddpic = holder.getView(R.id.rv_add_pic);//信息列表

        final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        rvaddpic.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        if (bean.getExamOptionList().size() == 0) {
            holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.VISIBLE);
        } else {
            holder.getView(R.id.rv_add_pic).setVisibility(View.VISIBLE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        }

        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rvaddpic.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(mContext);
            rvaddpic.setAdapter(adapter);
        }

        adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(dataBean.getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(dataBean.getId() + "");
                correctData.setId(dataBean.getLessonTaskStudentId());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MarkingNativeActivity");
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("comment", answerbean.getComment());
                intent.putExtra("fullscore", dataBean.getFullScore());
                intent.putExtra("score", dataBean.getScore());
                intent.putExtra("vid", 1);
                intent.putExtra("lessonTaskDetailsId", answerbean.getAnswer().getLessonTaskDetailsId());
                mContext.startActivity(intent);
            }
        });
        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean> examList = dataBean.getExamOptionList();
        for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
        tvExam.setHtmlText(examTextVo.getExamStem());

    }

    String getTemplateStyleName(int num) {
        switch (num) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知";
        }
    }
}
/*
public class AnswerExamStudentDetailAllAdapter extends BaseAdapter {

    private List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamListBean> mList = null;
    private Context mContext;
    private boolean isOpen = true;
    private int vid = 1;


    public AnswerExamStudentDetailAllAdapter(List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamListBean> List, Context mContext) {
        this.mList = List;
        this.mContext = mContext;
    }

    void setGoneParse(boolean isChecked) {

        isOpen = isChecked;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getTemplateId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = createItemView(getItemViewType(position));
        }
        if (convertView == null) {
            return null;
        }
        Object tag = convertView.getTag();
        if (tag instanceof SynSelectorHolder) {
            bindSelectorData(position, convertView, (SynSelectorHolder) tag);
        } else if (tag instanceof SynShortAnswerHolder) {
            bindShortAnswerData(position, convertView, (SynShortAnswerHolder) tag);
        } else if (tag instanceof SynFillBlankHolder) {
            bindFillBlank(position, convertView, (SynFillBlankHolder) tag);
        }
        return convertView;
    }

    private View createItemView(int itemViewType) {
        View view;
        SynHolder holder;
        switch (itemViewType) {
            case 1://单选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;

            case 2://多选
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 3://判断
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_select_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 4://简答
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text_jianda, null);
                holder = new SynShortAnswerHolder(view);
                view.setTag(holder);
                return view;
            case 6://填空
                view = LayoutInflater.from(mContext).inflate(R.layout.item_all_exam_text, null);
                holder = new SynFillBlankHolder(view);
                view.setTag(holder);
                return view;
            default://综合题不嵌套综合题
                return null;
        }
    }

    private static class SynHolder {
        TextView tvCount;
        TextView tvType;

        SynHolder(View view) {
            tvCount = view.findViewById(R.id.tv_count);
            tvType = view.findViewById(R.id.tv_type);

        }
    }

    //单选多选判断
    private static class SynSelectorHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        RadioGroup rg;//选项


        SynSelectorHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rg = view.findViewById(R.id.rg_answer);
        }
    }

    //简答
    private static class SynShortAnswerHolder extends SynHolder {
        HtmlTextView tvExam;
        RecyclerView rvaddpic;

        SynShortAnswerHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rvaddpic = view.findViewById(R.id.rv_add_pic);
        }
    }

    //填空
    private static class SynFillBlankHolder extends SynHolder {
        FillHtmlTextView tvExam;
        FillBlankCheckTextView fllBlankCheckTextView;

        SynFillBlankHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv);
            fllBlankCheckTextView = view.findViewById(R.id.FillBlankCheckTextView);
        }
    }

    //统一设置标题
    private void bindItemTitle(int position, SynHolder holder) {
        // mList.get(position).getExamSeq()
        if (Double.valueOf(mList.get(position).getScore()) > -1) {
            // holder.tvCount.setText(position + ". (本题" + mList.get(position).getScore() + "分)");
            holder.tvCount.setText(mList.get(position).getExamSeq() + ". (本题" + mList.get(position).getFullScore() + "分)");
        }
        holder.tvType.setText(getTemplateStyleName(mList.get(position).getExamTextVo().getExamTypeId()));
    }

    //选择判断
    private void bindSelectorData(int position, View convertView, SynSelectorHolder holder) {
        bindItemTitle(position, holder);
        // StudentDetails.DataBean dataBean1 = mList.get(position);
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamListBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.tvExam.setHtmlText(examTextVo.getExamStem());

        //无选项情况
        List<ExamText.ExamOptionsBean> examOptions = examTextVo.getExamOptions();
        if (examOptions == null || examOptions.isEmpty()) {
            holder.rg.removeAllViews();
            return;
        }
        //有选项
        Collections.sort(examOptions);
        int size = examOptions.size();
        int childCount = holder.rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            holder.rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(mContext).inflate(R.layout.item_child_select, holder.rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = holder.rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(examOptions.get(i).getContent());
            List<ExamText.ExamOptionsBean> examOptionss = dataBean.getExamTextVo().getExamOptions();
            if (examOptionss == null || examOptionss.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptionss);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptionss.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptionss);
                for (ExamText.ExamOptionsBean optionBean : examOptionss) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }

        }
    }

    */
/**
 * 填空
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 * <p>
 * 简单题
 *//*

    private void bindFillBlank(int position, View convertView, SynFillBlankHolder holder) {
        bindItemTitle(position, holder);
        AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamListBean dataBean = mList.get(position);
        holder.tvExam.setVisibility(View.GONE);
        holder.fllBlankCheckTextView.setVisibility(View.VISIBLE);


        SparseArray<ClozeAnswerBean> array = new SparseArray<>();//作答结果
        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionBean> examOptionList = dataBean.getExamOptionList();
        for (int i = 0; i < examOptionList.size(); i++) {
            AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionBean optionBean = examOptionList.get(i);

            int checkState;

            if (dataBean.getIsCorrect() != 0) {//todo,2019年2月27日15:39:323
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(),

                    optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        if (dataBean.getExamTextVo() != null && dataBean.getExamTextVo().getExamStem() != null) {
            holder.fllBlankCheckTextView.setHtmlBody(dataBean.getExamTextVo().getExamStem(), array);
        }
        holder.fllBlankCheckTextView.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                if (!TextUtils.isEmpty(imageSrc)) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(imageSrc);
                    correctData.setSeq("");
                    Intent intent = new Intent(mContext, CorrectActivity.class);
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mContext.startActivity(intent);
                } else {
                    ToastUtil.showLonglToast("图片地址为空");
                }
            }
        });
    }

    */
/**
 * 简单题
 *//*

    private void bindShortAnswerData(int position, View convertView, SynShortAnswerHolder holder) {
        bindItemTitle(position, holder);
        final AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamListBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        holder.rvaddpic.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) holder.rvaddpic.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(mContext);
            holder.rvaddpic.setAdapter(adapter);
        }
        adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(dataBean.getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(dataBean.getId() + "");
                correctData.setId(dataBean.getLessonTaskStudentId());
                Intent intent = new Intent(mContext, CorrectNativeActivity.class);
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("vid", vid);
                mContext.startActivity(intent);
            }
        });
        List<AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionBean> examList = dataBean.getExamOptionList();
        for (AnswerExamCorrectAllStudentVo.DataBean.AnswerBean.ExamOptionBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
        holder.tvExam.setHtmlText(examTextVo.getExamStem());

    }

    String getTemplateStyleName(int num) {
        switch (num) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知";
        }
    }
}
*/
