package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.StudentDetailExam;
import com.sdzn.fzx.teacher.vo.StudentDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class analyzeStudentDetailAdapter extends BaseRcvAdapter<StudentDetailExam.DataBean> {

    private Activity mActivity = null;
    private boolean showAnswer = false;//答案

    public analyzeStudentDetailAdapter(Context context, List<StudentDetailExam.DataBean> mList, Activity activity) {
        super(context, mList);
        mActivity = activity;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        StudentDetailExam.DataBean bean = mList.get(position);
        return bean.getTemplateId();
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 14://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 16://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, StudentDetailExam.DataBean bean) {

        switch (bean.getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                //  bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }


    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final StudentDetailExam.DataBean bean) {
        bindExamTitle(holder, position, bean);

        ExamText examTextVo = bean.getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(examTextVo.getExamStem());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            String str = String.valueOf((char) (65 + i));
            tvNumber.setText(str);


            tvText.setHtmlText(options.get(i).getContent());
            final List<StudentDetailExam.DataBean.ExamOptionListBean> examList = bean.getExamOptionList();
            // final   List<StudentDetails.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                tvNumber.setBackground(context.getResources().getDrawable(
                        bean.getExamTextVo().getExamOptions().get(i).isRight() ?
                                R.drawable.bg_select_selector_2 :
                                R.drawable.bg_result_select_selector_n));
                tvNumber.setSelected(TextUtils.equals(str, examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (StudentDetailExam.DataBean.ExamOptionListBean optionBean : examList) {
                    if (TextUtils.equals(str, optionBean.getMyAnswer())) {
                        tvNumber.setBackground(context.getResources().getDrawable(
                                bean.getExamTextVo().getExamOptions().get(i).isRight() ?
                                        R.drawable.bg_select_selector_2 :
                                        R.drawable.bg_result_select_selector_n));
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }


    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final StudentDetailExam.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());

    }


    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final StudentDetailExam.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        final TextView textView = holder.getView(R.id.score_tv_pigai);
        if (bean.getScore() > 0) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(bean.getScore() + "分");
        } else {
            textView.setVisibility(View.GONE);
        }
        RecyclerView rv = holder.getView(R.id.rv_add_pic);
        rv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(context);
            rv.setAdapter(adapter);
        }
        adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(bean.getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(bean.getId() + "");
                correctData.setId(bean.getLessonTaskStudentId());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectNativeActivity2");
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("vid", 1);
                mActivity.startActivity(intent);
            }
        });
        // List<StudentDetails.ExamOptionBean> examOptionList = bean.getExamOptionList();
        List<StudentDetailExam.DataBean.ExamOptionListBean> examList = bean.getExamOptionList();
        // List<StudentDetailExam.ExamOptionBean> examList = bean.getExamOptionList();
        for (StudentDetailExam.DataBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
    }


    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position, final StudentDetailExam.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());


    }

    private boolean goodFlag = true;
    private boolean errorFlag = true;

    private void bindExamTitle(final BaseViewHolder holder, int position, final StudentDetailExam.DataBean bean) {
        final String id = String.valueOf(bean.getId());
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textType = holder.getView(R.id.tv_type);


        if (bean.getFullScore() > -1) {

            if (bean.getTemplateId() == 6) {
                textCount.setText(position + 1 + ". (本题" + ((int) bean.getFullScore() * bean.getEmptyCount()) + "分)");
            } else {
                textCount.setText(position + 1 + ". (本题" + (int) bean.getFullScore() + "分)");
            }

        } else {
            textCount.setText(position + 1 + ".");
        }
        if (bean.getExamTextVo().getTemplateStyleName() != null) {
            textType.setText(bean.getExamTextVo().getTemplateStyleName() + "");
        } else {
            textType.setText("");
        }

        final ImageView good = holder.getView(R.id.good);
        final ImageView error = holder.getView(R.id.error);
        if (bean.getCorrectType() == 1) {
            good.setImageResource(R.mipmap.dx_sel);
        }

        if (bean.getCorrectType() == 2) {
            error.setImageResource(R.mipmap.dx_sel);
        }
        holder.getView(R.id.rl_result).setVisibility(View.GONE);
        holder.getView(R.id.error_ly).setVisibility(View.GONE);
        final TextView show = holder.getView(R.id.show);
        holder.getView(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.getView(R.id.rl_result).getVisibility() == View.GONE) {
                    holder.getView(R.id.rl_result).setVisibility(View.VISIBLE);
                    show.setText("隐藏答案");
                } else {
                    holder.getView(R.id.rl_result).setVisibility(View.GONE);
                    show.setText("显示答案");
                }
            }
        });

        holder.getView(R.id.good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goodFlag) {
                    good.setImageResource(R.mipmap.dx_sel);
                    error.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = false;
                    errorFlag = true;
                    updateState(bean.getId() + "", "1");
                } else {
                    good.setImageResource(R.mipmap.dxs_nor);
                    goodFlag = true;
                }
            }
        });

        holder.getView(R.id.error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (errorFlag) {
                    error.setImageResource(R.mipmap.dx_sel);
                    good.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = false;
                    goodFlag = true;
                    updateState(bean.getId() + "", "2");
                } else {
                    error.setImageResource(R.mipmap.dxs_nor);
                    errorFlag = true;
                }
            }
        });
        //答案部分
        holder.getView(R.id.rl_result).setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        ExamText examTextVo = bean.getExamTextVo();


        if (examTextVo == null) {
            return;
        }
        result.setHtmlText(examTextVo.getExamAnswer());
        analysis.setHtmlText(examTextVo.getExamAnalysis());
    }

    private StudentDetailAllAdapter allAnserAdapter;

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final StudentDetailExam.DataBean bean) {
        TextView textCount = holder.getView(R.id.tv_count);
        HtmlTextView htmlTextView = holder.getView(R.id.tv_exam);
        TextView textType = holder.getView(R.id.tv_type);
        RecyclerView examListView = holder.getView(R.id.lv);
        if (bean.getExamList().size() == 0) {
            textCount.setVisibility(View.GONE);
            htmlTextView.setVisibility(View.GONE);
            textType.setVisibility(View.GONE);
            examListView.setVisibility(View.GONE);
            return;
        }
        final List<StudentDetails.DataBean.AnswerBean> list = new ArrayList();


        textCount.setText(position + 1 + "");
        textType.setText(bean.getExamTextVo().getTemplateStyleName());
        htmlTextView.setHtmlText(bean.getExamTextVo().getExamStem());


        // list.addAll(bean.getAnswer().getExamList());
        examListView.setLayoutManager(new LinearLayoutManager(context));
        allAnserAdapter = new StudentDetailAllAdapter(context, list, mActivity);
        examListView.setAdapter(allAnserAdapter);

      /*   list.addAll(bean.getExamList());
        examListView.setLayoutManager(new LinearLayoutManager(context));
        final StudentDetailAllAdapter allAnserAdapter = new StudentDetailAllAdapter(context, list);
        examListView.setAdapter(allAnserAdapter);*/
        // setListHeight(examListView);
    }

    //设置listview高度
    private void setListHeight(ListView lv) {
        ListAdapter la = lv.getAdapter();
        if (null == la) {
            return;
        }
        // calculate height of all items.
        int h = 0;
        final int cnt = la.getCount();
        for (int i = 0; i < cnt; i++) {
            View item = la.getView(i, null, lv);
            item.measure(0, 0);
            h += item.getMeasuredHeight();
        }
        // reset ListView height
        ViewGroup.LayoutParams lp = lv.getLayoutParams();
        lp.height = h + (lv.getDividerHeight() * (cnt - 1));
        lv.setLayoutParams(lp);
    }

    void updateState(String id, String type) {
        Network.createTokenService(NetWorkService.updateExamTypeService.class)
                .AnalyzeStatisc(id, type)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("操作成功");

                    }
                });
    }
}
