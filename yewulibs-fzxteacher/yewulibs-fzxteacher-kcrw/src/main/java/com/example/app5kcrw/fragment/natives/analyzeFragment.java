package com.example.app5kcrw.fragment.natives;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5kcrw.adapter.AnalyzeExamAdapter;
import com.example.app5kcrw.adapter.AnalyzeTopAdapter;
import com.example.app5kcrw.adapter.ExamAnalyzeAdapter;
import com.example.app5kcrw.adapter.ExamDetailsAdapter;
import com.example.app5kcrw.adapter.analyzeSelectAdapter;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5kcrw.presenter.analyzePresenter;
import com.example.app5kcrw.view.analyzeFragmentView;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libbase.views.exam.ExamListView;
import com.sdzn.fzx.teacher.vo.AnalyzeNumberVo;
import com.sdzn.fzx.teacher.vo.AnalyzeStatisticsVo;
import com.sdzn.fzx.teacher.vo.AnalyzeVo;
import com.sdzn.fzx.teacher.vo.ExamAnalyzeVo;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.ArrayList;
import java.util.List;

/**
 * 题目分析
 */
public class analyzeFragment extends MBaseFragment<analyzePresenter> implements analyzeFragmentView, View.OnClickListener {


    private RecyclerView mRecyclerView;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private RelativeLayout rlTop;
    private RecyclerView rvAnswers;
    private RecyclerView recyclerView;
    private TextView avgTv;
    private TextView numTv;
    private TextView numTvM;
    private RadioGroup radioGroup;
    private RadioButton leftRb;
    private RadioButton rightRb;
    private LinearLayout details;
    private ImageView correctImg;
    private TextView percentCorrect;
    private TextView numPersonCorrect;
    private NoScrollGridView correctGridview;
    private ImageView twoImg;
    private TextView percentTwo;
    private TextView numPersonTwo;
    private NoScrollGridView errorGridview;
    private ImageView oneImg;
    private TextView percent;
    private TextView numPerson;
    private NoScrollGridView gridView;
    private LinearLayout examStatistics;
    private LinearLayout examSelect;
    private TextView tixing;
    private ExamListView examList;
    private RelativeLayout rll;
    private TextView you;
    private ImageView goodImg;
    private NoScrollGridView goodGridView;
    private RelativeLayout rlli;
    private TextView dian;
    private ImageView errorImg;
    private NoScrollGridView errorGridView;


    private Bundle bundle;
    private String classId;
    private String id;
    private AnalyzeExamAdapter analyzeExamAdapter;
    private AnalyzeTopAdapter analyzeTopAdapter;
    private List<AnalyzeVo.DataBean> mList = new ArrayList<>();
    private String lessonTaskDetailsId;
    private AnalyzeNumberVo analyzeNumber;
    private int analyzeNumberVoId;
    private AnalyzeVo analyzeVoList;

    public analyzeFragment() {
        // Required empty public constructor
    }

    public static analyzeFragment newInstance(Bundle bundle) {
        analyzeFragment fragment = new analyzeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    ExamAnalyzeAdapter goodExamAnalyzeAdapter, ErrorexamAnalyzeAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_analyze, container, false);
        llTaskEmpty = (LinearLayout) view.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) view.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) view.findViewById(R.id.tvTaskEmpty);
        rlTop = (RelativeLayout) view.findViewById(R.id.rl_top);
        rvAnswers = (RecyclerView) view.findViewById(R.id.rv_answers);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        avgTv = (TextView) view.findViewById(R.id.avg_tv);
        numTv = (TextView) view.findViewById(R.id.num_tv);
        numTvM = (TextView) view.findViewById(R.id.num_tv_m);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        leftRb = (RadioButton) view.findViewById(R.id.left_rb);
        rightRb = (RadioButton) view.findViewById(R.id.right_rb);
        details = (LinearLayout) view.findViewById(R.id.details);
        correctImg = (ImageView) view.findViewById(R.id.correct_img);
        percentCorrect = (TextView) view.findViewById(R.id.percent_correct);
        numPersonCorrect = (TextView) view.findViewById(R.id.num_person_correct);
        correctGridview = (NoScrollGridView) view.findViewById(R.id.correct_gridview);
        twoImg = (ImageView) view.findViewById(R.id.two_img);
        percentTwo = (TextView) view.findViewById(R.id.percent_two);
        numPersonTwo = (TextView) view.findViewById(R.id.num_person_two);
        errorGridview = (NoScrollGridView) view.findViewById(R.id.error_gridview);
        oneImg = (ImageView) view.findViewById(R.id.one_img);
        percent = (TextView) view.findViewById(R.id.percent);
        numPerson = (TextView) view.findViewById(R.id.num_person);
        gridView = (NoScrollGridView) view.findViewById(R.id.gridView);
        examStatistics = (LinearLayout) view.findViewById(R.id.exam_statistics);
        examSelect = (LinearLayout) view.findViewById(R.id.exam_select);
        tixing = (TextView) view.findViewById(R.id.tixing);
        examList = (ExamListView) view.findViewById(R.id.exam_list);
        rll = (RelativeLayout) view.findViewById(R.id.rll);
        you = (TextView) view.findViewById(R.id.you);
        goodImg = (ImageView) view.findViewById(R.id.good_img);
        goodGridView = (NoScrollGridView) view.findViewById(R.id.good_gridView);
        rlli = (RelativeLayout) view.findViewById(R.id.rlli);
        dian = (TextView) view.findViewById(R.id.dian);
        errorImg = (ImageView) view.findViewById(R.id.error_img);
        errorGridView = (NoScrollGridView) view.findViewById(R.id.error_gridView);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        goodImg.setOnClickListener(this);
        errorImg.setOnClickListener(this);

        analyzeExamAdapter = new AnalyzeExamAdapter(App2.get(), mList, getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(analyzeExamAdapter);

        analyzeExamAdapter.setOnAddClickListener(new AnalyzeExamAdapter.OnAddClickListener() {
            @Override
            public void onItemClick(int id) {

                analyzeNumberVoId = id;
                mPresenter.getexamAnalyzeList(id + "");
                lessonTaskDetailsId = id + "";
                leftRb.setChecked(true);
                details.setVisibility(View.GONE);
                examStatistics.setVisibility(View.VISIBLE);
                setSocre();
            }
        });

        analyzeTopAdapter = new AnalyzeTopAdapter(App2.get());
        rvAnswers.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        rvAnswers.setAdapter(analyzeTopAdapter);
        analyzeTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mRecyclerView.scrollToPosition(position);
                analyzeTopAdapter.AdapterPosition = position;
                analyzeTopAdapter.notifyDataSetChanged();

                analyzeExamAdapter.selectPostion = position;
                analyzeExamAdapter.notifyDataSetChanged();
                analyzeNumberVoId = analyzeVoList.getData().get(position).getId();
                mPresenter.getexamAnalyzeList(analyzeVoList.getData().get(position).getId() + "");
                lessonTaskDetailsId = analyzeVoList.getData().get(position).getId() + "";
                leftRb.setChecked(true);
                details.setVisibility(View.GONE);
                examStatistics.setVisibility(View.VISIBLE);
                setSocre();
            }
        });
        bundle = getArguments();
        initData();
        return view;
    }

    private void initData() {
        classId = bundle.getString("classId");
        id = bundle.getString("id");
        mPresenter.getAnalyzeList(id, "0", "1");
        mPresenter.getexamNumberList(id);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.left_rb) {
                    details.setVisibility(View.GONE);
                    examStatistics.setVisibility(View.VISIBLE);
                } else if (i == R.id.right_rb) {
                    details.setVisibility(View.VISIBLE);
                    examStatistics.setVisibility(View.GONE);
                    mPresenter.getexamStudentList(id, lessonTaskDetailsId);
                }
            }
        });
    }

    @Override
    public void initPresenter() {
        mPresenter = new analyzePresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void setAnalyzeVo(AnalyzeVo analyzeVo) {
        analyzeVoList = analyzeVo;
        if (analyzeVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {

            llTaskEmpty.setVisibility(View.GONE);
            mList.clear();
            mList.addAll(analyzeVo.getData());

            refreshAdapter();
            lessonTaskDetailsId = analyzeVo.getData().get(0).getId() + "";
            mPresenter.getexamAnalyzeList(analyzeVo.getData().get(0).getId() + "");
        }
    }

    void setSocre() {

        for (AnalyzeNumberVo.DataBean analyzeNumberVo : analyzeNumber.getData()) {
            if (analyzeNumberVo.getExamId() == analyzeNumberVoId) {
                numTvM.setText("/" + analyzeNumberVo.getStudentCount());
                numTv.setText(analyzeNumberVo.getRightCount() + "");
                if (analyzeNumberVo.getScoreAvg() >= 0) {
                    avgTv.setText(analyzeNumberVo.getScoreAvg() + "");
                } else {
                    avgTv.setText("---");
                }


            }
        }

    }


    @Override
    public void setAnalyzeNumberVo(AnalyzeNumberVo analyzeNumberVo) {
        analyzeNumber = analyzeNumberVo;
        if (analyzeNumberVo.getData().size() != 0) {
            numTvM.setText("/" + analyzeNumberVo.getData().get(0).getStudentCount());
            //if ()
            numTv.setText(analyzeNumberVo.getData().get(0).getRightCount() + "");
            if (analyzeNumberVo.getData().get(0).getScoreAvg() >= 0) {
                avgTv.setText(analyzeNumberVo.getData().get(0).getScoreAvg() + "");
            } else {
                avgTv.setText("---");
            }


        }
        analyzeTopAdapter.clear();
        analyzeTopAdapter.add(analyzeNumberVo.getData());
        refreshAdapter();
    }


    private void refreshAdapter() {
        analyzeExamAdapter.notifyDataSetChanged();
        analyzeTopAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private boolean flagGoodImg = true;
    private boolean flagErrorImg = true;

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.good_img) {
            if (flagGoodImg) {
                goodImg.setImageResource(R.mipmap.yinc_icon);
                flagGoodImg = false;
                if (goodExamAnalyzeAdapter != null) {
                    goodExamAnalyzeAdapter.showName = 1;
                    goodExamAnalyzeAdapter.notifyDataSetChanged();
                }


            } else {
                goodImg.setImageResource(R.mipmap.xs_icon);
                flagGoodImg = true;
                if (goodExamAnalyzeAdapter != null) {
                    goodExamAnalyzeAdapter.showName = 0;
                    goodExamAnalyzeAdapter.notifyDataSetChanged();
                }
            }
        } else if (viewId == R.id.error_img) {
            if (flagErrorImg) {
                errorImg.setImageResource(R.mipmap.yinc_icon);
                flagErrorImg = false;
                if (ErrorexamAnalyzeAdapter != null) {
                    ErrorexamAnalyzeAdapter.showName = 1;
                    ErrorexamAnalyzeAdapter.notifyDataSetChanged();
                }


            } else {
                errorImg.setImageResource(R.mipmap.xs_icon);
                flagErrorImg = true;
                if (ErrorexamAnalyzeAdapter != null) {
                    ErrorexamAnalyzeAdapter.showName = 0;
                    ErrorexamAnalyzeAdapter.notifyDataSetChanged();
                }
            }
        }

    }


    @Override
    public void setExamAnalyzeStatisticsVo(ExamAnalyzeVo examAnalyzeVo) {
        if (goodExamAnalyzeAdapter != null) {
            goodExamAnalyzeAdapter.clear();
        }
        if (ErrorexamAnalyzeAdapter != null) {
            ErrorexamAnalyzeAdapter.clear();
        }

        if (examAnalyzeVo.getData().getExcellentAnswerList().size() != 0) {
            goodExamAnalyzeAdapter = new ExamAnalyzeAdapter(examAnalyzeVo.getData().getExcellentAnswerList(), getActivity());
            goodGridView.setAdapter(goodExamAnalyzeAdapter);
        }

        if (examAnalyzeVo.getData().getTypicalErrorList().size() != 0) {
            ErrorexamAnalyzeAdapter = new ExamAnalyzeAdapter(examAnalyzeVo.getData().getTypicalErrorList(), getActivity());
            errorGridView.setAdapter(ErrorexamAnalyzeAdapter);

        }
        if (goodExamAnalyzeAdapter != null) {
            goodExamAnalyzeAdapter.notifyDataSetChanged();
        }
        if (ErrorexamAnalyzeAdapter != null) {
            ErrorexamAnalyzeAdapter.notifyDataSetChanged();
        }
        setSelect(examAnalyzeVo);
    }

    private void setSelect(ExamAnalyzeVo examAnalyzeVo) {

        if (examAnalyzeVo.getData().getExamList().size() != 0 && examAnalyzeVo.getData().getExamList().get(0).getAnswerList().size() != 0) {
            examSelect.setVisibility(View.VISIBLE);
            tixing.setText(examAnalyzeVo.getData().getExamTemplateStyleName());
            examList.setAdapter(new analyzeSelectAdapter(examAnalyzeVo.getData().getExamList().get(0).getAnswerList().get(0).getOptionList(), getActivity()));

        } else {
            examSelect.setVisibility(View.GONE);
        }
    }

    @Override
    public void setAnalyzeStatisticsVo(AnalyzeStatisticsVo analyzeStatisticsVo) {

        gridView.setAdapter(new ExamDetailsAdapter(analyzeStatisticsVo.getData().getErrorStudentList(), getActivity()));

        errorGridview.setAdapter(new ExamDetailsAdapter(analyzeStatisticsVo.getData().getHalfRightStudentList(), getActivity()));
        correctGridview.setAdapter(new ExamDetailsAdapter(analyzeStatisticsVo.getData().getRightStudentList(), getActivity()));

        percent.setText(analyzeStatisticsVo.getData().getErrorRate() + "%");
        numPerson.setText(analyzeStatisticsVo.getData().getErrorCount() + "人");
        percentTwo.setText(analyzeStatisticsVo.getData().getHalfRightRate() + "%");
        numPersonTwo.setText(analyzeStatisticsVo.getData().getHalfRightCount() + "人");

        percentCorrect.setText(analyzeStatisticsVo.getData().getRightRate() + "%");
        numPersonCorrect.setText(analyzeStatisticsVo.getData().getRightCount() + "人");
    }

}


