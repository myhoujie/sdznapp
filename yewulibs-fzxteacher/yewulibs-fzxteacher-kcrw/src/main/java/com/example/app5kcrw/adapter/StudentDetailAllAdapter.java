package com.example.app5kcrw.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.BaseRcvAdapter;
import com.example.app5libbase.baseui.adapter.BaseViewHolder;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.exam.FillBlankCheckTextView;
import com.example.app5libbase.views.exam.FillHtmlTextView;
import com.example.app5libbase.views.exam.HtmlTextView;
import com.sdzn.fzx.teacher.vo.ClozeAnswerBean;
import com.sdzn.fzx.teacher.vo.CorrectDataVo;
import com.sdzn.fzx.teacher.vo.ExamText;
import com.sdzn.fzx.teacher.vo.StudentDetails;
import com.sdzn.fzx.student.libutils.annotations.CheckState;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentDetailAllAdapter extends BaseRcvAdapter<StudentDetails.DataBean.AnswerBean> {
    private List<StudentDetails.DataBean.AnswerBean> mList = null;
    private Context mContext;
    private Activity mActivity = null;
    private boolean showAnswer;
    public boolean isFragmentFrist = false;
    public ShortAnswerResultAdapter mClickStudentDetailAdapter;

    public StudentDetailAllAdapter(Context context, List<StudentDetails.DataBean.AnswerBean> mList, Activity activity) {
        super(context, mList);
        this.mList = mList;
        this.mContext = context;
        mActivity = activity;
    }

    public StudentDetailAllAdapter(Context context, List<StudentDetails.DataBean.AnswerBean> mList, Fragment fragment) {
        this(context, mList, fragment.getActivity());
        this.fragment = fragment;
        isFragmentFrist = true;
    }

    public void setShowAnswer(boolean showAnswer) {
        this.showAnswer = showAnswer;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        StudentDetails.DataBean.AnswerBean bean = mList.get(position);
        return bean.getTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 2://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 3://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_select_select);
            case 4://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text_jianda);
            case 6://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_all_exam_text);
            default:
                return null;
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, StudentDetails.DataBean.AnswerBean bean) {

        switch (bean.getTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            default:
                bindSelector(holder, position, bean);//出错
                break;
        }
    }

    /*设置头部*/
    private void bindExamTitle(final BaseViewHolder holder, int position, final StudentDetails.DataBean.AnswerBean bean) {
        final TextView textCount = holder.getView(R.id.tv_count);
        final TextView textType = holder.getView(R.id.tv_type);
        if (Double.valueOf(mList.get(position).getScore()) > -1) {
            textCount.setText(mList.get(position).getExamSeq() + ". (本题" + mList.get(position).getFullScore() + "分)");
        }
        textType.setText(getTemplateStyleName(mList.get(position).getExamTextVo().getExamTypeId()));
        HtmlTextView result = holder.getView(R.id.tv_result);
        HtmlTextView analysis = holder.getView(R.id.tv_analysis);
        final LinearLayout rlresult = holder.getView(R.id.rl_result);
        //===================== 答案 =====================
        rlresult.setVisibility(showAnswer ? View.VISIBLE : View.GONE);
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnswer())) {
            result.setHtmlText("略...");
        } else {
            result.setHtmlText(bean.getExamTextVo().getExamAnswer() == null ? "" : bean.getExamTextVo().getExamAnswer());
        }
        if (TextUtils.isEmpty(bean.getExamTextVo().getExamAnalysis())) {
            analysis.setHtmlText("略...");
        } else {
            analysis.setHtmlText(bean.getExamTextVo().getExamAnalysis() == null ? "" : bean.getExamTextVo().getExamAnalysis());
        }
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final StudentDetails.DataBean.AnswerBean bean) {

    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final StudentDetails.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tvExam = holder.getView(R.id.tv);
        final FillBlankCheckTextView fllBlankCheckTextView = holder.getView(R.id.FillBlankCheckTextView);

        StudentDetails.DataBean.AnswerBean dataBean = mList.get(position);
        tvExam.setVisibility(View.GONE);
        fllBlankCheckTextView.setVisibility(View.VISIBLE);

        SparseArray<ClozeAnswerBean> array = new SparseArray<>();//作答结果
        List<StudentDetails.DataBean.AnswerBean.ExamOptionListBean> examOptionList = dataBean.getExamOptionList();
        for (int i = 0; i < examOptionList.size(); i++) {
            StudentDetails.DataBean.AnswerBean.ExamOptionListBean optionBean = examOptionList.get(i);

            int checkState;

            if (dataBean.getIsCorrect() != 0) {//todo,2019年2月27日15:39:323
                if (optionBean.getIsRight() == 1) {
                    checkState = CheckState.TRUE;
                } else {
                    checkState = CheckState.FALSE;
                }
            } else {
                checkState = CheckState.UN_CHECK;
            }
            array.put(i, new ClozeAnswerBean(i, "" + optionBean.getSeq(),
                    optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(),
                    false, optionBean.getAnswerType(), checkState));
        }
        if (dataBean.getExamTextVo() != null && dataBean.getExamTextVo().getExamStem() != null) {
            fllBlankCheckTextView.setHtmlBody(dataBean.getExamTextVo().getExamStem(), array);
        }
        fllBlankCheckTextView.setFillBlankCheckClickListener(new FillBlankCheckTextView.ImageClickListener() {
            @Override
            public void clickImage(String imageSrc) {
                if (!TextUtils.isEmpty(imageSrc)) {
                    CorrectDataVo correctData = new CorrectDataVo();
                    correctData.setSrc(imageSrc);
                    correctData.setSeq("");
//                    Intent intent = new Intent(fllBlankCheckTextView.getContext(), CorrectActivity.class);
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectActivity");
                    intent.putExtra("correctData", correctData);
                    intent.putExtra("vid", 0);
                    mContext.startActivity(intent);
                } else {
                    ToastUtil.showLonglToast("图片地址为空");
                }
            }
        });
    }

    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final StudentDetails.DataBean.AnswerBean bean) {
        bindExamTitle(holder, position, bean);
        HtmlTextView tvExam = holder.getView(R.id.tv_exam);
        RecyclerView rvaddpic = holder.getView(R.id.rv_add_pic);
        final StudentDetails.DataBean.AnswerBean dataBean = mList.get(position);
        ExamText examTextVo = dataBean.getExamTextVo();
        rvaddpic.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rvaddpic.setNestedScrollingEnabled(false);//禁止滑动
        rvaddpic.setHasFixedSize(true);

        if (bean.getExamOptionList().size() == 0) {
            holder.getView(R.id.rv_add_pic).setVisibility(View.GONE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.VISIBLE);
        } else {
            holder.getView(R.id.rv_add_pic).setVisibility(View.VISIBLE);
            holder.getView(R.id.tv_un_answer).setVisibility(View.GONE);
        }

        ShortAnswerResultAdapter adapter = (ShortAnswerResultAdapter) rvaddpic.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerResultAdapter(mContext);
            rvaddpic.setAdapter(adapter);
        }
        final ShortAnswerResultAdapter finalAdapter = adapter;


        adapter.setListener(new ShortAnswerResultAdapter.OnClickListener() {
            @Override
            public void clickImage(int index, String url) {
                //这个是点击跳转的界面
                CorrectDataVo correctData = new CorrectDataVo();
                correctData.setSrc(dataBean.getExamOptionList().get(0).getId() + "");
                correctData.setSeq(index + "");
                correctData.setList(url);
                correctData.setEid(dataBean.getId() + "");
                correctData.setId(dataBean.getLessonTaskStudentId());
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CorrectNativeActivity2");
                // Intent intent = new Intent(mActivity, CorrectActivity.class);
                intent.putExtra("correctData", correctData);
                intent.putExtra("vid", 1);
                mClickStudentDetailAdapter = finalAdapter;
                if (mActivity != null && !isFragmentFrist) {
                    mActivity.startActivity(intent);
                } else if (fragment != null) {
                    fragment.startActivityForResult(intent, 1234);
                }
                // mContext.startActivity(intent);
            }
        });
        List<StudentDetails.DataBean.AnswerBean.ExamOptionListBean> examList = dataBean.getExamOptionList();
        for (StudentDetails.DataBean.AnswerBean.ExamOptionListBean optionBean : examList) {
            String str2 = optionBean.getMyAnswer();
            if (str2 == null) {
                break;
            }
            String[] arr = str2.split(";");
            if (arr.length == 1) {
                adapter.setList(arr[0].split(","), null);
            } else {
                adapter.setList(arr[1].split(","), arr[0].split(","));
            }
        }
        tvExam.setHtmlText(examTextVo.getExamStem());


    }


    String getTemplateStyleName(int num) {
        switch (num) {
            case 1:
                return "单选题";
            case 2:
                return "多选题";
            case 3:
                return "判断题";
            case 4:
                return "简答题";
            case 6:
                return "填空题";
            default:
                return "未知";
        }
    }
}
