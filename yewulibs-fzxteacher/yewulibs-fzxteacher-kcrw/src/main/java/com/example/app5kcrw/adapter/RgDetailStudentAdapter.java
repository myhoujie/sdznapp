package com.example.app5kcrw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.RgDetailsVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class RgDetailStudentAdapter extends BaseAdapter {

    private List<RgDetailsVo.DataBean.StudentListBean> mlist;
    private Context mContext;

    public RgDetailStudentAdapter(List<RgDetailsVo.DataBean.StudentListBean> mlist, Context mContext) {
        this.mlist = mlist;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.rg_student_list_item, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.name.setText(mlist.get(position).getUserStudentName());
        if (mlist.get(position).getIsRead() == 0) {
            holder.correctState.setText("未查看");
        } else {
            holder.correctState.setText("已完成");
        }
        if (mlist.get(position).getUseTime() == 0) {
            holder.time.setText("---");
        } else {
            holder.time.setText(mlist.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) / 60 + "分" + mlist.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        }


        return convertView;
    }

    static class ViewHolder {
        ImageView photo;
        TextView name;
        TextView correctState;
        TextView time;

        ViewHolder(View view) {
            photo = view.findViewById(R.id.photo);
            name = view.findViewById(R.id.name);
            correctState = view.findViewById(R.id.correct_state);
            time = view.findViewById(R.id.time);
        }
    }
}
