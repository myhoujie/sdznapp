package com.example.app5kcrw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.SloganBane;

import java.util.List;

public class SloganAdapter extends RecyclerView.Adapter<SloganAdapter.ViewHolder> {
    List<SloganBane> sloganBaneList;
    private Context mcontext;
    private OnItemClickLitener mOnItemClickLitener;

    public SloganAdapter(Context context, List<SloganBane> sloganBaneList) {
        this.mcontext = context;
        this.sloganBaneList = sloganBaneList;
    }

    //设置回调接口
    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_opting, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        SloganBane groupBean = sloganBaneList.get(position);
        String name = groupBean.getName();
        holder.nameTextView.setText(name);
        //通过为条目设置点击事件触发回调
        if (mOnItemClickLitener != null) {
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickLitener.onItemClick(view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return sloganBaneList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public LinearLayout itemLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.tv_option);
            itemLayout = itemView.findViewById(R.id.itemLayout);
        }
    }
}
