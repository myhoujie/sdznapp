package com.example.app5kcrw.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.RgAnalyze;
import com.sdzn.fzx.teacher.vo.RgDetailsVo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public interface RganalyzeFragmentView extends BaseView {

    void setRgAnalyze(RgAnalyze analyze);

    void setRgDetailAnalyze(RgDetailsVo rgDetailAnalyze);
}
