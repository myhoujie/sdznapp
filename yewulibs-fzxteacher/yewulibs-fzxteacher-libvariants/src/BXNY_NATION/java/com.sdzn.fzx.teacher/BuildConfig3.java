package com.sdzn.fzx.teacher;

public class BuildConfig3 {
    //    public static final VersionInfo versionInfoConfig = VersionInfo.CCC;
//    public static final String versionNameConfig = VersionChoose.getVersionNameString(VersionChoose.CCC);
    public static final String versionNameConfig = UrlManager.YYY;

    public static final String SERVER_ISERVICE = UrlManager.PREPROD_SERVER_ISERVICE;
//    public static final String SERVER_ISERVICE2 = UrlManager.TEST_SERVER_ISERVICE2;

    public static final String FLAVOR = UrlManager.FLAVOR2;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE2;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME2;
    public static final String AUTH = UrlManager.AUTH2;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS2;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL2;
    public static final String DOWN = UrlManager.DOWN2;
    public static final String MQTT = UrlManager.MQTT2;
    public static final int OFFICE_ID = UrlManager.OFFICE_ID2;
    public static final String PORT = UrlManager.PORT2;
    public static final int RABBIT_HOST = UrlManager.RABBIT_HOST2;
    public static final String RABBIT_MQ = UrlManager.RABBIT_MQ2;
    public static final String RABBIT_NAME = UrlManager.RABBIT_NAME2;
    public static final String RABBIT_PASSWORD = UrlManager.RABBIT_PASSWORD2;
    public static final String APPLICATION_ID = UrlManager.APPLICATION_ID2;
    // Fields from default config.
    public static final int TINKER_VERSION = UrlManager.TINKER_VERSION2;
}
