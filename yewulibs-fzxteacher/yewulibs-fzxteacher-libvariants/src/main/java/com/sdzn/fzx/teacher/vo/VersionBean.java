package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * VersionBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VersionBean {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * volumeList : [{"id":28,"createAccountId":56,"createUserName":"田斌","createTime":1517467388000,"userTeacherId":9,"userTeacherName":"李老师","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"baseVolumeId":1,"baseVolumeName":"上册"},{"id":29,"createAccountId":56,"createUserName":"田斌","createTime":1517467388000,"userTeacherId":9,"userTeacherName":"李老师","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"baseVolumeId":2,"baseVolumeName":"下册"}]
         * baseSubjectName : 人民教育版
         * baseSubjectId : 12
         */

        private String baseSubjectName;
        private String baseSubjectId;
        private List<VolumeListBean> volumeList;


        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public String getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(String baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public List<VolumeListBean> getVolumeList() {
            return volumeList;
        }

        public void setVolumeList(List<VolumeListBean> volumeList) {
            this.volumeList = volumeList;
        }

        public static class VolumeListBean {
            /**
             * id : 28
             * createAccountId : 56
             * createUserName : 田斌
             * createTime : 1517467388000
             * userTeacherId : 9
             * userTeacherName : 李老师
             * baseEducationId : 1
             * baseEducationName : 六三制
             * baseGradeId : 7
             * baseGradeName : 七年级
             * baseSubjectId : 1
             * baseSubjectName : 语文
             * baseVersionId : 12
             * baseVersionName : 人民教育版
             * startGradeId : null
             * startGradeName : null
             * baseVolumeId : 1
             * baseVolumeName : 上册
             */

            private String id;
            private String createAccountId;
            private String createUserName;
            private long createTime;
            private String userTeacherId;
            private String userTeacherName;
            private String baseEducationId;
            private String baseEducationName;
            private String baseGradeId;
            private String baseGradeName;
            private String baseSubjectId;
            private String baseSubjectName;
            private String baseVersionId;
            private String baseVersionName;
            private String startGradeId;
            private String startGradeName;
            private String baseVolumeId;
            private String baseVolumeName;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCreateAccountId() {
                return createAccountId;
            }

            public void setCreateAccountId(String createAccountId) {
                this.createAccountId = createAccountId;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public String getUserTeacherId() {
                return userTeacherId;
            }

            public void setUserTeacherId(String userTeacherId) {
                this.userTeacherId = userTeacherId;
            }

            public String getUserTeacherName() {
                return userTeacherName;
            }

            public void setUserTeacherName(String userTeacherName) {
                this.userTeacherName = userTeacherName;
            }

            public String getBaseEducationId() {
                return baseEducationId;
            }

            public void setBaseEducationId(String baseEducationId) {
                this.baseEducationId = baseEducationId;
            }

            public String getBaseEducationName() {
                return baseEducationName;
            }

            public void setBaseEducationName(String baseEducationName) {
                this.baseEducationName = baseEducationName;
            }

            public String getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(String baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public String getBaseSubjectId() {
                return baseSubjectId;
            }

            public void setBaseSubjectId(String baseSubjectId) {
                this.baseSubjectId = baseSubjectId;
            }

            public String getBaseSubjectName() {
                return baseSubjectName;
            }

            public void setBaseSubjectName(String baseSubjectName) {
                this.baseSubjectName = baseSubjectName;
            }

            public String getBaseVersionId() {
                return baseVersionId;
            }

            public void setBaseVersionId(String baseVersionId) {
                this.baseVersionId = baseVersionId;
            }

            public String getBaseVersionName() {
                return baseVersionName;
            }

            public void setBaseVersionName(String baseVersionName) {
                this.baseVersionName = baseVersionName;
            }

            public String getStartGradeId() {
                return startGradeId;
            }

            public void setStartGradeId(String startGradeId) {
                this.startGradeId = startGradeId;
            }

            public String getStartGradeName() {
                return startGradeName;
            }

            public void setStartGradeName(String startGradeName) {
                this.startGradeName = startGradeName;
            }

            public String getBaseVolumeId() {
                return baseVolumeId;
            }

            public void setBaseVolumeId(String baseVolumeId) {
                this.baseVolumeId = baseVolumeId;
            }

            public String getBaseVolumeName() {
                return baseVolumeName;
            }

            public void setBaseVolumeName(String baseVolumeName) {
                this.baseVolumeName = baseVolumeName;
            }
        }
    }
}
