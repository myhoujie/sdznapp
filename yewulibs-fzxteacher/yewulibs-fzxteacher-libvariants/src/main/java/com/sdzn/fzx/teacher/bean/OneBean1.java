package com.sdzn.fzx.teacher.bean;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class OneBean1 implements Serializable {
    private String tab_id;
    private String tab_name;
    private String url;
    private int tab_icon;
    private boolean enable;


    public OneBean1() {
    }

    public OneBean1(String tab_id, String tab_name, boolean enable, int tab_icon) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.tab_icon = tab_icon;
    }

    public OneBean1(String tab_id, String tab_name, boolean enable) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
    }
    public OneBean1(String tab_id, String tab_name,String url, boolean enable) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.url=url;
    }

    public int getTab_icon() {
        return tab_icon;
    }

    public void setTab_icon(int tab_icon) {
        this.tab_icon = tab_icon;
    }

    public String getTab_id() {
        return tab_id;
    }

    public void setTab_id(String tab_id) {
        this.tab_id = tab_id;
    }

    public String getTab_name() {
        return tab_name;
    }

    public void setTab_name(String tab_name) {
        this.tab_name = tab_name;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
