package com.sdzn.fzx.teacher.vo;

/**
 * Created by wangc on 2018/6/27 0027.
 */

public class SaveCorrectPicVo {
    /**
     * data : http://192.168.0.215:8886/newExam/2018/06/0WTNClIgGFTluSUT24FtBiZCRkU9I10E.jpg
     */

    private SavePicData data;

    public SavePicData getData() {
        return data;
    }

    public void setData(SavePicData data) {
        this.data = data;
    }

    private static class SavePicData {
        private String realPath;

        public String getRealPath() {
            return realPath;
        }

        public void setRealPath(String realPath) {
            this.realPath = realPath;
        }
    }
}
