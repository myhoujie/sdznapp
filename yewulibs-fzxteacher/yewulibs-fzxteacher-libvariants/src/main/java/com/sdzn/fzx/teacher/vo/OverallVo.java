package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/14
 * 修改单号：
 * 修改内容:
 */
public class OverallVo {
    /**
     * total : 1
     * data : [{"id":71149,"lessonTaskId":2053,"baseSubjectId":1,"baseSubjectName":"语文","status":2,"userStudentId":5166,"userStudentName":"学生62","userTeacherId":233,"userTeacherName":"王银勇","submitTime":1548850665000,"useTime":49,"scoreObjective":-1,"scoreSubjective":-1,"scoreTotal":-1,"countExam":null,"countResource":null,"countRight":0,"countError":0,"countHalf":0,"classGroupId":0,"classGroupName":"未分组","isCorrect":0,"correctTime":null,"correctType":1,"scope":2018,"lessonLibId":null,"answerExamList":[{"id":505408,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20000,"correctType":2,"useTime":0,"templateId":4,"templateStyleId":359,"templateStyleName":"默写","examSeq":1,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690350661,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":3,\"examAnalysis\":\"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）\",\"examAnswer\":\"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>\",\"examId\":\"5c121aaeb2c0131b25983330\",\"examOptions\":[],\"examStem\":\"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bb\",\"label\":\"（2016秋\u2022宁河县月考）\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":72,\"partnerExamId\":\"87116caa-c9a7-4694-9f89-d34d702f2387\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1544690381746,\"viewCount\":34,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185693,"lessonAnswerExamId":505408,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bb"},{"id":505409,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20001,"correctType":3,"useTime":0,"templateId":4,"templateStyleId":359,"templateStyleName":"默写","examSeq":2,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1548850099129,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":14,\"examAnalysis\":\"答案为：<br />（1）树木丛生，百草丰茂。&nbsp;&nbsp;日月之行，若出其中。星汉灿烂，若出其里&nbsp;（重点字：灿烂）<br />（2）夕阳西下，断肠人在天涯（重点字：涯）<br />&nbsp;（3）海日生残夜，江春入旧年；潮平两岸阔，风正一帆悬；乡书何处达？归雁洛阳边（重点字：悬）<br />（4）我寄愁心与明月，随君直到夜郎西（重点字：郎）\",\"examAnswer\":\"1. 树木丛生 <br>2. 百草丰茂 <br>3. 日月之行 <br>4. 若出其中 <br>5. 星汉灿烂 <br>6. 若出其里 <br>7. 夕阳西下 <br>8. 断肠人在天涯 <br>9. 海日生残夜 <br>10. 江春入旧年 <br>11. 潮平两岸阔 <br>12. 风正一帆悬 <br>13. 乡书何处达 <br>14. 归雁洛阳边 <br>15. 我寄愁心与明月 <br>16. 随君直到夜郎西 <br>\",\"examId\":\"5c5193b31cb07676c580b8b4\",\"examOptions\":[],\"examStem\":\"根据要求默写。<br />（1）《观沧海》中展现海岛勃勃生机的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；展现大海的壮丽景象和诗人博大胸怀的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（2）崔颢在《黄鹤楼》中写道\u201c日暮乡关何处是？烟波江上使人愁\u201d，而马致远在《天净沙\u2022秋思》中也有两句与此意境相似的句子：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（3）《次北固山下》一诗中即景抒情又蕴含自然理趣的两句诗是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；描写涨潮时水面宽阔的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；表达乡愁的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>？<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（4）《闻王昌龄左迁龙标遥有此寄》中借明月以抒发思念家乡、怀念朋友的感情的名句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c5195b21cb07676c580b8bc\",\"label\":\"\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":100,\"partnerExamId\":\"522af16d-63bc-4ab4-9110-fe1e35a036b5\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1548850099129,\"viewCount\":39,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185694,"lessonAnswerExamId":505409,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg,http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg,http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg,http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg,http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg,http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg,http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bc"},{"id":505410,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20002,"correctType":1,"useTime":0,"templateId":4,"templateStyleId":301,"templateStyleName":"简答题","examSeq":3,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690358274,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":2,\"downCount\":4,\"examAnalysis\":\"（1）通过阅读，理解诗歌内容，把握各个意象之间的关系，是解答本题的关键。前三行全是写景，十八字白描勾勒出这样一幅生动的深秋晚景图。\u201c枯藤老树昏鸦\u201d中\u201c枯\u201d\u201c老\u201d\u201c昏\u201d三个词描绘出当时诗人所处的悲凉氛围。\u201c小桥流水人家\u201d描绘了一幅安宁、和谐的景象，与沦落异乡的游子相映，使图景带上悲凉的气氛。使\u201c断肠人\u201d更添悲愁。\u201c古道西风瘦马\u201d正是诗人当时自己的真实写照，他长期奔波与劳累已不言而喻了。这与归巢的昏鸦与团聚的人家真可谓构成了鲜明的对照。作者寄情于物表达天涯沦落人的凄苦之情。因此描绘的是一幅萧瑟（萧索）、荒凉的深秋晚景图。<br />（2）通过反复诵读体会诗歌的情感，学生应全面了解背景，把握形象内涵，体会意境特点，明确抒情方式。这里通过秋天意象的描写，表达了游子在秋天思念故乡、倦于漂泊的孤寂愁苦之情。<br />答案：<br />（1）萧瑟（萧索）、荒凉。<br />（2）孤寂愁苦，思念家乡。（答\u201c思乡\u201d或\u201c飘零天涯的游子在秋天思念故乡、倦于漂泊的凄苦愁楚之情\u201d也可）<br />翻译：<br />&nbsp;&nbsp;&nbsp; 枯萎的藤蔓，垂老的古树，黄昏时的乌鸦，扑打着翅膀，落在光秃秃的枝桠上。纤巧别致的小桥，潺潺的流水，低矮破旧的几间茅屋，愈发显得安谧而温馨。荒凉的古道上，一匹消瘦憔悴的马载着同样疲惫憔悴的异乡游子，在异乡的西风里踌躇而行。夕阳渐渐落山了，但是在外的游子，何处是归宿？家乡在何方？念及此，天涯漂泊的游子怎能不愁肠寸断！\",\"examAnswer\":\"答案见解析\",\"examId\":\"5c121ab6b2c0131b25983331\",\"examOptions\":[],\"examStem\":\"<BDO class=mathjye-aligncenter>天净沙\u2022秋思<br />马致远<br />枯藤老树昏鸦，<br />小桥流水人家。<br />古道西风瘦马。<br />夕阳西下，<br />断肠人在天涯。<br /><\/BDO>（1）这首小令的前三句描绘出一幅怎样的深秋晚景图？<br />（2）这首小令中的\u201c断肠人在天涯\u201d抒发了作者什么样的思想感情？\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bd\",\"label\":\"（2014\u2022娄底）\",\"labelReportID\":\"3262b2d3-9bc3-4eb2-bff6-1fee01aaa889\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":82,\"partnerExamId\":\"e2775eba-c032-42fc-963a-e6f74af47003\",\"points\":[{\"eName\":\"chinese\",\"id\":3770,\"levelId\":2,\"name\":\"爱情闺怨诗\",\"no\":\"EN\",\"nodeIdPath\":\"0.3729.3747.3770\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":301,\"templateStyleName\":\"简答题\",\"type\":1,\"updateTime\":1544690381749,\"viewCount\":393,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185695,"lessonAnswerExamId":505410,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg,http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg,http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg,http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg,http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg,http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg,http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bd"}]}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 71149
         * lessonTaskId : 2053
         * baseSubjectId : 1
         * baseSubjectName : 语文
         * status : 2
         * userStudentId : 5166
         * userStudentName : 学生62
         * userTeacherId : 233
         * userTeacherName : 王银勇
         * submitTime : 1548850665000
         * useTime : 49
         * scoreObjective : -1.0
         * scoreSubjective : -1.0
         * scoreTotal : -1.0
         * countExam : null
         * countResource : null
         * countRight : 0
         * countError : 0
         * countHalf : 0
         * classGroupId : 0
         * classGroupName : 未分组
         * isCorrect : 0
         * correctTime : null
         * correctType : 1
         * scope : 2018
         * lessonLibId : null
         * answerExamList : [{"id":505408,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20000,"correctType":2,"useTime":0,"templateId":4,"templateStyleId":359,"templateStyleName":"默写","examSeq":1,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690350661,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":3,\"examAnalysis\":\"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）\",\"examAnswer\":\"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>\",\"examId\":\"5c121aaeb2c0131b25983330\",\"examOptions\":[],\"examStem\":\"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bb\",\"label\":\"（2016秋\u2022宁河县月考）\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":72,\"partnerExamId\":\"87116caa-c9a7-4694-9f89-d34d702f2387\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1544690381746,\"viewCount\":34,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185693,"lessonAnswerExamId":505408,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bb"},{"id":505409,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20001,"correctType":3,"useTime":0,"templateId":4,"templateStyleId":359,"templateStyleName":"默写","examSeq":2,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1548850099129,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":1,\"downCount\":14,\"examAnalysis\":\"答案为：<br />（1）树木丛生，百草丰茂。&nbsp;&nbsp;日月之行，若出其中。星汉灿烂，若出其里&nbsp;（重点字：灿烂）<br />（2）夕阳西下，断肠人在天涯（重点字：涯）<br />&nbsp;（3）海日生残夜，江春入旧年；潮平两岸阔，风正一帆悬；乡书何处达？归雁洛阳边（重点字：悬）<br />（4）我寄愁心与明月，随君直到夜郎西（重点字：郎）\",\"examAnswer\":\"1. 树木丛生 <br>2. 百草丰茂 <br>3. 日月之行 <br>4. 若出其中 <br>5. 星汉灿烂 <br>6. 若出其里 <br>7. 夕阳西下 <br>8. 断肠人在天涯 <br>9. 海日生残夜 <br>10. 江春入旧年 <br>11. 潮平两岸阔 <br>12. 风正一帆悬 <br>13. 乡书何处达 <br>14. 归雁洛阳边 <br>15. 我寄愁心与明月 <br>16. 随君直到夜郎西 <br>\",\"examId\":\"5c5193b31cb07676c580b8b4\",\"examOptions\":[],\"examStem\":\"根据要求默写。<br />（1）《观沧海》中展现海岛勃勃生机的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；展现大海的壮丽景象和诗人博大胸怀的诗句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（2）崔颢在《黄鹤楼》中写道\u201c日暮乡关何处是？烟波江上使人愁\u201d，而马致远在《天净沙\u2022秋思》中也有两句与此意境相似的句子：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（3）《次北固山下》一诗中即景抒情又蕴含自然理趣的两句诗是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；描写涨潮时水面宽阔的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>；表达乡愁的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>？<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。<br />（4）《闻王昌龄左迁龙标遥有此寄》中借明月以抒发思念家乡、怀念朋友的感情的名句是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>。\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5c5195b21cb07676c580b8bc\",\"label\":\"\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":100,\"partnerExamId\":\"522af16d-63bc-4ab4-9110-fe1e35a036b5\",\"points\":[{\"eName\":\"chinese\",\"id\":3762,\"levelId\":2,\"name\":\"名篇名句默写\",\"no\":\"EF\",\"nodeIdPath\":\"0.3729.3747.3762\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":359,\"templateStyleName\":\"默写\",\"type\":1,\"updateTime\":1548850099129,\"viewCount\":39,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185694,"lessonAnswerExamId":505409,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg,http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg,http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg,http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg,http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg,http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg,http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bc"},{"id":505410,"isAnswer":1,"isCorrect":0,"isRight":0,"timeAnswer":null,"timeSubmit":1548850665000,"score":-1,"fullScore":-1,"lessonTaskId":2053,"lessonTaskStudentId":71149,"lessonTaskDetailsId":20002,"correctType":1,"useTime":0,"templateId":4,"templateStyleId":301,"templateStyleName":"简答题","examSeq":3,"clozeSeq":null,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterName\":\"第一单元\",\"chapterNodeIdPath\":\"0.2888\",\"chapterNodeNamePath\":\"第一单元\",\"createTime\":1544690358274,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":36,\"customerSchoolName\":\"时代智囊测试\",\"delflag\":1,\"difficulty\":2,\"downCount\":4,\"examAnalysis\":\"（1）通过阅读，理解诗歌内容，把握各个意象之间的关系，是解答本题的关键。前三行全是写景，十八字白描勾勒出这样一幅生动的深秋晚景图。\u201c枯藤老树昏鸦\u201d中\u201c枯\u201d\u201c老\u201d\u201c昏\u201d三个词描绘出当时诗人所处的悲凉氛围。\u201c小桥流水人家\u201d描绘了一幅安宁、和谐的景象，与沦落异乡的游子相映，使图景带上悲凉的气氛。使\u201c断肠人\u201d更添悲愁。\u201c古道西风瘦马\u201d正是诗人当时自己的真实写照，他长期奔波与劳累已不言而喻了。这与归巢的昏鸦与团聚的人家真可谓构成了鲜明的对照。作者寄情于物表达天涯沦落人的凄苦之情。因此描绘的是一幅萧瑟（萧索）、荒凉的深秋晚景图。<br />（2）通过反复诵读体会诗歌的情感，学生应全面了解背景，把握形象内涵，体会意境特点，明确抒情方式。这里通过秋天意象的描写，表达了游子在秋天思念故乡、倦于漂泊的孤寂愁苦之情。<br />答案：<br />（1）萧瑟（萧索）、荒凉。<br />（2）孤寂愁苦，思念家乡。（答\u201c思乡\u201d或\u201c飘零天涯的游子在秋天思念故乡、倦于漂泊的凄苦愁楚之情\u201d也可）<br />翻译：<br />&nbsp;&nbsp;&nbsp; 枯萎的藤蔓，垂老的古树，黄昏时的乌鸦，扑打着翅膀，落在光秃秃的枝桠上。纤巧别致的小桥，潺潺的流水，低矮破旧的几间茅屋，愈发显得安谧而温馨。荒凉的古道上，一匹消瘦憔悴的马载着同样疲惫憔悴的异乡游子，在异乡的西风里踌躇而行。夕阳渐渐落山了，但是在外的游子，何处是归宿？家乡在何方？念及此，天涯漂泊的游子怎能不愁肠寸断！\",\"examAnswer\":\"答案见解析\",\"examId\":\"5c121ab6b2c0131b25983331\",\"examOptions\":[],\"examStem\":\"<BDO class=mathjye-aligncenter>天净沙\u2022秋思<br />马致远<br />枯藤老树昏鸦，<br />小桥流水人家。<br />古道西风瘦马。<br />夕阳西下，<br />断肠人在天涯。<br /><\/BDO>（1）这首小令的前三句描绘出一幅怎样的深秋晚景图？<br />（2）这首小令中的\u201c断肠人在天涯\u201d抒发了作者什么样的思想感情？\",\"examTypeId\":4,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":1,\"id\":\"5c5195b21cb07676c580b8bd\",\"label\":\"（2014\u2022娄底）\",\"labelReportID\":\"3262b2d3-9bc3-4eb2-bff6-1fee01aaa889\",\"lessonLibId\":2455,\"lessonTaskId\":2053,\"optionNumber\":0,\"paperCount\":82,\"partnerExamId\":\"e2775eba-c032-42fc-963a-e6f74af47003\",\"points\":[{\"eName\":\"chinese\",\"id\":3770,\"levelId\":2,\"name\":\"爱情闺怨诗\",\"no\":\"EN\",\"nodeIdPath\":\"0.3729.3747.3770\",\"parentId\":\"3747\",\"subjectId\":1}],\"score\":0,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":301,\"templateStyleName\":\"简答题\",\"type\":1,\"updateTime\":1544690381749,\"viewCount\":393,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","parentId":0,"emptyCount":0,"userStudentId":5166,"userStudentName":"学生62","examOptionList":[{"id":185695,"lessonAnswerExamId":505410,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg,http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg,http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg,http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg,http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg,http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg,http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}],"examList":[],"taskExamId":"5c5195b21cb07676c580b8bd"}]
         */

        private int id;
        private int lessonTaskId;
        private int baseSubjectId;
        private String baseSubjectName;
        private int status;
        private int userStudentId;
        private String userStudentName;
        private int userTeacherId;
        private String userTeacherName;
        private long submitTime;
        private int useTime;
        private double scoreObjective;
        private double scoreSubjective;
        private double scoreTotal;
        private Object countExam;
        private Object countResource;
        private int countRight;
        private int countError;
        private int countHalf;
        private int classGroupId;
        private String classGroupName;
        private int isCorrect;
        private Object correctTime;
        private int correctType;
        private int scope;
        private Object lessonLibId;
        private List<AnswerExamListBean> answerExamList;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public int getUserTeacherId() {
            return userTeacherId;
        }

        public void setUserTeacherId(int userTeacherId) {
            this.userTeacherId = userTeacherId;
        }

        public String getUserTeacherName() {
            return userTeacherName;
        }

        public void setUserTeacherName(String userTeacherName) {
            this.userTeacherName = userTeacherName;
        }

        public long getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(long submitTime) {
            this.submitTime = submitTime;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public double getScoreObjective() {
            return scoreObjective;
        }

        public void setScoreObjective(double scoreObjective) {
            this.scoreObjective = scoreObjective;
        }

        public double getScoreSubjective() {
            return scoreSubjective;
        }

        public void setScoreSubjective(double scoreSubjective) {
            this.scoreSubjective = scoreSubjective;
        }

        public double getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(double scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public Object getCountExam() {
            return countExam;
        }

        public void setCountExam(Object countExam) {
            this.countExam = countExam;
        }

        public Object getCountResource() {
            return countResource;
        }

        public void setCountResource(Object countResource) {
            this.countResource = countResource;
        }

        public int getCountRight() {
            return countRight;
        }

        public void setCountRight(int countRight) {
            this.countRight = countRight;
        }

        public int getCountError() {
            return countError;
        }

        public void setCountError(int countError) {
            this.countError = countError;
        }

        public int getCountHalf() {
            return countHalf;
        }

        public void setCountHalf(int countHalf) {
            this.countHalf = countHalf;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public Object getCorrectTime() {
            return correctTime;
        }

        public void setCorrectTime(Object correctTime) {
            this.correctTime = correctTime;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public Object getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(Object lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public List<AnswerExamListBean> getAnswerExamList() {
            return answerExamList;
        }

        public void setAnswerExamList(List<AnswerExamListBean> answerExamList) {
            this.answerExamList = answerExamList;
        }

        public static class AnswerExamListBean {
            /**
             * id : 505408
             * isAnswer : 1
             * isCorrect : 0
             * isRight : 0
             * timeAnswer : null
             * timeSubmit : 1548850665000
             * score : -1.0
             * fullScore : -1.0
             * lessonTaskId : 2053
             * lessonTaskStudentId : 71149
             * lessonTaskDetailsId : 20000
             * correctType : 2
             * useTime : 0
             * templateId : 4
             * templateStyleId : 359
             * templateStyleName : 默写
             * examSeq : 1
             * clozeSeq : null
             * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","baseVolumeId":1,"baseVolumeName":"上册","chapterId":2888,"chapterName":"第一单元","chapterNodeIdPath":"0.2888","chapterNodeNamePath":"第一单元","createTime":1544690350661,"createUserId":233,"createUserName":"王银勇","customerSchoolId":36,"customerSchoolName":"时代智囊测试","delflag":1,"difficulty":1,"downCount":3,"examAnalysis":"答案：<br />日月之行&nbsp; 若出其中&nbsp; 星汉灿烂&nbsp; 若出其里（重点字：星汉）","examAnswer":"1. 日月之行 <br>2. 若出其中 <br>3. 星汉灿烂 <br>4. 若出其里 <br>","examId":"5c121aaeb2c0131b25983330","examOptions":[],"examStem":"《观沧海》中表现作者伟大胸襟的句子是：<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>，<span style='text-decoration: underline;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>。","examTypeId":4,"favTime":0,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":1,"id":"5c5195b21cb07676c580b8bb","label":"（2016秋•宁河县月考）","lessonLibId":2455,"lessonTaskId":2053,"optionNumber":0,"paperCount":72,"partnerExamId":"87116caa-c9a7-4694-9f89-d34d702f2387","points":[{"eName":"chinese","id":3762,"levelId":2,"name":"名篇名句默写","no":"EF","nodeIdPath":"0.3729.3747.3762","parentId":"3747","subjectId":1}],"score":0,"sourceId":3,"sourceName":"菁优网","templateStyleId":359,"templateStyleName":"默写","type":1,"updateTime":1544690381746,"viewCount":34,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
             * parentId : 0
             * emptyCount : 0
             * userStudentId : 5166
             * userStudentName : 学生62
             * examOptionList : [{"id":185693,"lessonAnswerExamId":505408,"isRight":1,"score":-1,"myAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","myAnswerHtml":"","seq":2,"lessonTaskStudentId":71149,"myOldAnswer":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","clozeSeq":null,"templateStyleName":null,"examSeq":null,"answerType":null}]
             * examList : []
             * taskExamId : 5c5195b21cb07676c580b8bb
             */

            private int id;
            private int isAnswer;
            private int isCorrect;
            private int isRight;
            private Object timeAnswer;
            private long timeSubmit;
            private double score;
            private double fullScore;
            private int lessonTaskId;
            private int lessonTaskStudentId;
            private int lessonTaskDetailsId;
            private int correctType;
            private int useTime;
            private int templateId;
            private int templateStyleId;
            private String templateStyleName;
            private int examSeq;
            private Object clozeSeq;
            private String examText;
            private int parentId;
            private int emptyCount;
            private int userStudentId;
            private String userStudentName;
            private String taskExamId;
            private List<ExamOptionListBean> examOptionList;
            private List<?> examList;
            private ExamText ExamTextVo;

            public ExamText getExamTextVo() {
                return ExamTextVo;
            }

            public void setExamTextVo(ExamText examTextVo) {
                ExamTextVo = examTextVo;
            }
            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getIsAnswer() {
                return isAnswer;
            }

            public void setIsAnswer(int isAnswer) {
                this.isAnswer = isAnswer;
            }

            public int getIsCorrect() {
                return isCorrect;
            }

            public void setIsCorrect(int isCorrect) {
                this.isCorrect = isCorrect;
            }

            public int getIsRight() {
                return isRight;
            }

            public void setIsRight(int isRight) {
                this.isRight = isRight;
            }

            public Object getTimeAnswer() {
                return timeAnswer;
            }

            public void setTimeAnswer(Object timeAnswer) {
                this.timeAnswer = timeAnswer;
            }

            public long getTimeSubmit() {
                return timeSubmit;
            }

            public void setTimeSubmit(long timeSubmit) {
                this.timeSubmit = timeSubmit;
            }

            public double getScore() {
                return score;
            }

            public void setScore(double score) {
                this.score = score;
            }

            public double getFullScore() {
                return fullScore;
            }

            public void setFullScore(double fullScore) {
                this.fullScore = fullScore;
            }

            public int getLessonTaskId() {
                return lessonTaskId;
            }

            public void setLessonTaskId(int lessonTaskId) {
                this.lessonTaskId = lessonTaskId;
            }

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public int getLessonTaskDetailsId() {
                return lessonTaskDetailsId;
            }

            public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
                this.lessonTaskDetailsId = lessonTaskDetailsId;
            }

            public int getCorrectType() {
                return correctType;
            }

            public void setCorrectType(int correctType) {
                this.correctType = correctType;
            }

            public int getUseTime() {
                return useTime;
            }

            public void setUseTime(int useTime) {
                this.useTime = useTime;
            }

            public int getTemplateId() {
                return templateId;
            }

            public void setTemplateId(int templateId) {
                this.templateId = templateId;
            }

            public int getTemplateStyleId() {
                return templateStyleId;
            }

            public void setTemplateStyleId(int templateStyleId) {
                this.templateStyleId = templateStyleId;
            }

            public String getTemplateStyleName() {
                return templateStyleName;
            }

            public void setTemplateStyleName(String templateStyleName) {
                this.templateStyleName = templateStyleName;
            }

            public int getExamSeq() {
                return examSeq;
            }

            public void setExamSeq(int examSeq) {
                this.examSeq = examSeq;
            }

            public Object getClozeSeq() {
                return clozeSeq;
            }

            public void setClozeSeq(Object clozeSeq) {
                this.clozeSeq = clozeSeq;
            }

            public String getExamText() {
                return examText;
            }

            public void setExamText(String examText) {
                this.examText = examText;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getEmptyCount() {
                return emptyCount;
            }

            public void setEmptyCount(int emptyCount) {
                this.emptyCount = emptyCount;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public String getTaskExamId() {
                return taskExamId;
            }

            public void setTaskExamId(String taskExamId) {
                this.taskExamId = taskExamId;
            }

            public List<ExamOptionListBean> getExamOptionList() {
                return examOptionList;
            }

            public void setExamOptionList(List<ExamOptionListBean> examOptionList) {
                this.examOptionList = examOptionList;
            }

            public List<?> getExamList() {
                return examList;
            }

            public void setExamList(List<?> examList) {
                this.examList = examList;
            }

            public static class ExamOptionListBean {
                /**
                 * id : 185693
                 * lessonAnswerExamId : 505408
                 * isRight : 1
                 * score : -1.0
                 * myAnswer : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg
                 * myAnswerHtml :
                 * seq : 2
                 * lessonTaskStudentId : 71149
                 * myOldAnswer : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg,http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg,http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg,http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg
                 * clozeSeq : null
                 * templateStyleName : null
                 * examSeq : null
                 * answerType : null
                 */

                private int id;
                private int lessonAnswerExamId;
                private int isRight;
                private double score;
                private String myAnswer;
                private String myAnswerHtml;
                private int seq;
                private int lessonTaskStudentId;
                private String myOldAnswer;
                private Object clozeSeq;
                private Object templateStyleName;
                private Object examSeq;
                private int answerType;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getLessonAnswerExamId() {
                    return lessonAnswerExamId;
                }

                public void setLessonAnswerExamId(int lessonAnswerExamId) {
                    this.lessonAnswerExamId = lessonAnswerExamId;
                }

                public int getIsRight() {
                    return isRight;
                }

                public void setIsRight(int isRight) {
                    this.isRight = isRight;
                }

                public double getScore() {
                    return score;
                }

                public void setScore(double score) {
                    this.score = score;
                }

                public String getMyAnswer() {
                    return myAnswer;
                }

                public void setMyAnswer(String myAnswer) {
                    this.myAnswer = myAnswer;
                }

                public String getMyAnswerHtml() {
                    return myAnswerHtml;
                }

                public void setMyAnswerHtml(String myAnswerHtml) {
                    this.myAnswerHtml = myAnswerHtml;
                }

                public int getSeq() {
                    return seq;
                }

                public void setSeq(int seq) {
                    this.seq = seq;
                }

                public int getLessonTaskStudentId() {
                    return lessonTaskStudentId;
                }

                public void setLessonTaskStudentId(int lessonTaskStudentId) {
                    this.lessonTaskStudentId = lessonTaskStudentId;
                }

                public String getMyOldAnswer() {
                    return myOldAnswer;
                }

                public void setMyOldAnswer(String myOldAnswer) {
                    this.myOldAnswer = myOldAnswer;
                }

                public Object getClozeSeq() {
                    return clozeSeq;
                }

                public void setClozeSeq(Object clozeSeq) {
                    this.clozeSeq = clozeSeq;
                }

                public Object getTemplateStyleName() {
                    return templateStyleName;
                }

                public void setTemplateStyleName(Object templateStyleName) {
                    this.templateStyleName = templateStyleName;
                }

                public Object getExamSeq() {
                    return examSeq;
                }

                public void setExamSeq(Object examSeq) {
                    this.examSeq = examSeq;
                }

                public int getAnswerType() {
                    return answerType;
                }

                public void setAnswerType(int answerType) {
                    this.answerType = answerType;
                }
            }
        }
    }
}
