package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/10
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class TakeOfficeVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * subjectId : 0
         * classList : [{"classId":9,"baseGradeName":"七年级","className":"一班","baseGradeId":7,"subjectId":0,"subjectName":"班主任"}]
         * subjectName : 班主任
         */


        private String subjectName;
        private String role;
        private List<String> className;


        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public List<String> getClassList() {
            return className;
        }

        public void setClassList(List<String> classList) {
            this.className = classList;
        }


    }
}
