package com.sdzn.fzx.teacher.vo.chatroom;

import java.util.List;

public class DiscussionClassGroup {
    private String classId;
    private String className;

    private List<DataB> dataBList;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<DataB> getDataBList() {
        return dataBList;
    }

    public void setDataBList(List<DataB> dataBList) {
        this.dataBList = dataBList;
    }

    public static class DataB {
        /**
         *
         */
        private int id;
        private String groupId;
        private String groupName;
        private boolean isCheck;

        public DataB(String groupId, String groupName) {
            this.groupId = groupId;
            this.groupName = groupName;
        }

        public DataB(String groupId, String groupName, boolean isCheck) {
            this.groupId = groupId;
            this.groupName = groupName;
            this.isCheck = isCheck;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }
}
