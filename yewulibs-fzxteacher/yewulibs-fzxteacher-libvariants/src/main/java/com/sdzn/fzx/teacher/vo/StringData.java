package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/14
 * 修改单号：
 * 修改内容:
 */
public class StringData {
    /**
     * data : 2
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
