package com.sdzn.fzx.teacher.bean;

import java.util.List;

public class SubjectData {
    /**
     * subjectId : 2
     * subjectName : 语文
     * bookVolumeRes : [{"id":"51","bookVolumeId":1323,"bookVersionId":10228,"name":"人教版（2016）-九年级下册"},{"id":"52","bookVolumeId":537,"bookVersionId":10224,"name":"苏教版-七年级上册"},{"id":"53","bookVolumeId":556,"bookVersionId":10228,"name":"人教版（2016）-七年级上册"},{"id":"56","bookVolumeId":1218,"bookVersionId":10228,"name":"人教版（2016）-九年级上册"},{"id":"58","bookVolumeId":1144,"bookVersionId":10228,"name":"人教版（2016）-八年级下册"}]
     */

    private String subjectId;
    private String subjectName;
    private List<BookVolumeResBean> bookVolumeRes;

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<BookVolumeResBean> getBookVolumeRes() {
        return bookVolumeRes;
    }

    public void setBookVolumeRes(List<BookVolumeResBean> bookVolumeRes) {
        this.bookVolumeRes = bookVolumeRes;
    }

    public static class BookVolumeResBean {
        /**
         * id : 51
         * bookVolumeId : 1323
         * bookVersionId : 10228
         * name : 人教版（2016）-九年级下册
         */

        private String id;
        private int bookVolumeId;
        private int bookVersionId;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getBookVolumeId() {
            return bookVolumeId;
        }

        public void setBookVolumeId(int bookVolumeId) {
            this.bookVolumeId = bookVolumeId;
        }

        public int getBookVersionId() {
            return bookVersionId;
        }

        public void setBookVersionId(int bookVersionId) {
            this.bookVersionId = bookVersionId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }



    /* *//**
     * subjectId : 2
     * subjectName : 语文
     * bookVolumeRes : [{"id":"60","bookVolumeId":537,"bookVersionId":10224,"name":"苏教版-七年级上册"},{"id":"89","bookVolumeId":556,"bookVersionId":10228,"name":"人教版（2016）-七年级上册"},{"id":"90","bookVolumeId":1470,"bookVersionId":10316,"name":"人教版（部编版）-九年级上册"}]
     *//*

    private int subjectId;
    private String subjectName;
    private List<BookVolumeResBean> bookVolumeRes;

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<BookVolumeResBean> getBookVolumeRes() {
        return bookVolumeRes;
    }

    public void setBookVolumeRes(List<BookVolumeResBean> bookVolumeRes) {
        this.bookVolumeRes = bookVolumeRes;
    }

    public static class BookVolumeResBean {
        *//**
     * id : 60
     * bookVolumeId : 537
     * bookVersionId : 10224
     * name : 苏教版-七年级上册
     *//*

        private String id;
        private int bookVolumeId;
        private int bookVersionId;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getBookVolumeId() {
            return bookVolumeId;
        }

        public void setBookVolumeId(int bookVolumeId) {
            this.bookVolumeId = bookVolumeId;
        }

        public int getBookVersionId() {
            return bookVersionId;
        }

        public void setBookVersionId(int bookVersionId) {
            this.bookVersionId = bookVersionId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }*/
}
