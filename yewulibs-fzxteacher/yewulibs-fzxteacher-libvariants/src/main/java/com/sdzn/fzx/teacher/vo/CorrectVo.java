package com.sdzn.fzx.teacher.vo;

import java.util.List;

public class CorrectVo {

    /**
     * data : {"correctList":[{"day":1515828508300,"rate":0},{"day":1515914908300,"rate":0},{"day":1516001308300,"rate":0},{"day":1516087708300,"rate":0},{"day":1516174108300,"rate":0},{"day":1516260508300,"rate":0},{"day":1516346908300,"rate":0},{"day":1516433308300,"rate":0},{"day":1516519708300,"rate":0},{"day":1516606108300,"rate":0},{"day":1516692508300,"rate":0},{"day":1516778908300,"rate":0},{"day":1516865308300,"rate":0},{"day":1516951708300,"rate":0},{"day":1517038108300,"rate":0},{"day":1517124508300,"rate":0},{"day":1517210908300,"rate":0},{"day":1517297308300,"rate":0},{"day":1517383708300,"rate":0},{"day":1517470108300,"rate":0},{"day":1517556508300,"rate":0},{"day":1517642908300,"rate":0},{"day":1517729308300,"rate":0},{"day":1517815708300,"rate":0},{"day":1517902108300,"rate":0},{"day":1517988508300,"rate":0},{"day":1518074908300,"rate":0},{"day":1518161308300,"rate":0},{"day":1518247708300,"rate":0},{"day":1518334108300,"rate":0}],"completionList":[{"day":1515828508300,"rate":0},{"day":1515914908300,"rate":0},{"day":1516001308300,"rate":0},{"day":1516087708300,"rate":0},{"day":1516174108300,"rate":0},{"day":1516260508300,"rate":0},{"day":1516346908300,"rate":0},{"day":1516433308300,"rate":0},{"day":1516519708300,"rate":0},{"day":1516606108300,"rate":0},{"day":1516692508300,"rate":0},{"day":1516778908300,"rate":0},{"day":1516865308300,"rate":0},{"day":1516951708300,"rate":0},{"day":1517038108300,"rate":0},{"day":1517124508300,"rate":0},{"day":1517210908300,"rate":0},{"day":1517297308300,"rate":0},{"day":1517383708300,"rate":0},{"day":1517470108300,"rate":0},{"day":1517556508300,"rate":0},{"day":1517642908300,"rate":0},{"day":1517729308300,"rate":0},{"day":1517815708300,"rate":0},{"day":1517902108300,"rate":0},{"day":1517988508300,"rate":0},{"day":1518074908300,"rate":0},{"day":1518161308300,"rate":0},{"day":1518247708300,"rate":0},{"day":1518334108300,"rate":0}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<CorrectListBean> correctList;
        private List<CompletionListBean> completionList;

        public List<CorrectListBean> getCorrectList() {
            return correctList;
        }

        public void setCorrectList(List<CorrectListBean> correctList) {
            this.correctList = correctList;
        }

        public List<CompletionListBean> getCompletionList() {
            return completionList;
        }

        public void setCompletionList(List<CompletionListBean> completionList) {
            this.completionList = completionList;
        }

        public static class CorrectListBean {
            /**
             * day : 1515828508300
             * rate : 0
             */

            private long day;
            private float rate;

            public long getDay() {
                return day;
            }

            public void setDay(long day) {
                this.day = day;
            }

            public float getRate() {
                return rate;
            }

            public void setRate(float rate) {
                this.rate = rate;
            }
        }

        public static class CompletionListBean {
            /**
             * day : 1515828508300
             * rate : 0
             */

            private long day;
            private float rate;

            public long getDay() {
                return day;
            }

            public void setDay(long day) {
                this.day = day;
            }

            public float getRate() {
                return rate;
            }

            public void setRate(float rate) {
                this.rate = rate;
            }
        }
    }
}
