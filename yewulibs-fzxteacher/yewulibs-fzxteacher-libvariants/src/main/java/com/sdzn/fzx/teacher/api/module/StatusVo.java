package com.sdzn.fzx.teacher.api.module;

/**
 * 描述：公共返回参数
 * -
 * 创建人：wangchunxiao
 * 创建时间：2016/10/25
 */
public class StatusVo<T> {

    private String timestamp;
    private int code;
    private String msg;
    private T result;

    public StatusVo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "StatusVo{" +
                "timestamp='" + timestamp + '\'' +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                '}';
    }
}