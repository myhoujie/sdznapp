package com.sdzn.fzx.teacher.view;


import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.vo.UserBean;

/**
 *
 */

public interface BasicDataViews extends IView {
    void getUserInfo(UserBean loginBean);

    void updateSuccess();

    void updateFailed();

    void onFailed(String msg);
}
