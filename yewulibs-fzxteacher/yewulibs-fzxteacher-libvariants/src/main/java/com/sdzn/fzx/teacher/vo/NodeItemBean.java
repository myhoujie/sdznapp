package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * NodeItemBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class NodeItemBean {

    private String nodeId;

    private String nodeName;

    private List<? extends NodeItemBean> children;


    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<? extends NodeItemBean> getChildren() {
        return children;
    }

    public void setChildren(List<? extends NodeItemBean> children) {
        this.children = children;
    }
}
