package com.sdzn.fzx.teacher.presenter;

import com.blankj.utilcode.util.SPUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.view.CehuaViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class CehuaPresenter extends Presenter<CehuaViews> {

    public void queryCehua() {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryCehua()//"Bearer "+(String) SPUtils.getInstance().getString("token", "")
                .enqueue(new Callback<ResponseSlbBean1<GrzxRecActBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<GrzxRecActBean>> call, Response<ResponseSlbBean1<GrzxRecActBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCehuaNodata(response.body().getMessage());
                            return;
                        }
                        getView().onCehuaSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<GrzxRecActBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCehuaFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
