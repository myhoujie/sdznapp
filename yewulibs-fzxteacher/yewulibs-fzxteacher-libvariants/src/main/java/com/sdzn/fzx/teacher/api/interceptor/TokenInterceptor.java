package com.sdzn.fzx.teacher.api.interceptor;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.network.RefreshTokenHandler;
import com.sdzn.fzx.teacher.api.network.Status;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.RefreshTokenBean;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;

/**
 * TokenInterceptor〈当token过期后，重新刷新，获取token，然后再次访问网络〉
 * 〈token过期处理〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class TokenInterceptor implements Interceptor {
    private static final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        LoginBean loginBean = SPUtils.getLoginBean();

        if (loginBean == null) {
            SPUtils.saveLoginBean(null);
            gotoLogin();
            return chain.proceed(request);
        }
        final String accessToken = loginBean.getData().getAccessToken();
//        com.blankj.utilcode.util.SPUtils.getInstance().put("token",accessToken);

        if (loginBean != null && !TextUtils.isEmpty(accessToken)) {

            HttpUrl.Builder authorizedUrlBuilder = request.url()
                    .newBuilder()
                    .scheme(request.url().scheme())
                    .host(request.url().host())
                    .addQueryParameter("access_token", accessToken);

            request = request.newBuilder()
                    .method(request.method(), request.body())
                    .url(authorizedUrlBuilder.build())
                    .build();
            Log.e("带有access_token的URL", "带有access_token的URL  " + request.url());
        } else {
            SPUtils.saveLoginBean(null);
            gotoLogin();
            return chain.proceed(request);
        }


        /**
         * 通过如下的办法曲线取到请求完成的数据
         * 原本想通过  originalResponse.body().string()
         * 去取到请求完成的数据,但是一直报错,不知道是okhttp的bug还是操作不当
         * 然后去看了okhttp的源码,找到了这个曲线方法,取到请求完成的数据后,根据特定的判断条件去判断token过期
         */
        Response originalResponse = chain.proceed(request);

        StatusVo func = getResult(originalResponse);
        if (func != null) {
            if (Status.TOKEN_GIVE_ERROR.getCode() == func.getCode()) {
                SPUtils.saveLoginBean(null);
                gotoLogin();
            } else if (Status.TOKEN_Invalid.getCode() == func.getCode()) {
                Log.e("eg", "eg的URL  " + request.url());
                final Response newResponse = RefreshTokenHandler.getInstance().redirectNewToken(accessToken, request, chain);
                if (newResponse == null) {
                    return originalResponse;
                }
                return newResponse;
            }
        }
        return originalResponse;
    }


    /**
     * 访问该接口，获取到数据，用来判断token
     *
     * @param originalResponse
     * @return
     * @throws IOException
     */
    private StatusVo getResult(Response originalResponse) throws IOException {
        ResponseBody responseBody = originalResponse.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        Charset charset = UTF8;
        MediaType contentType = responseBody.contentType();
        if (contentType != null) {
            charset = contentType.charset(UTF8);
        }
        String bodyString = buffer.clone().readString(charset);
        Gson gson = new Gson();
        StatusVo func = gson.fromJson(bodyString, StatusVo.class);
        return func;
    }

    //刷新token
    private synchronized Response getNewToken(Request request, Chain chain) throws IOException {
        final LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean == null) {
            return null;
        }

        final String refreToken = loginBean.getData().getRefreshToken();
        final String accountId = loginBean.getData().getUser().getAccountId() + "";

        //请求刷新token 接口获取新token
        Call<RefreshTokenBean> objectObservable = Network.createService(NetWorkService.RefreshToken.class)
                .refresh(refreToken, accountId);
        RefreshTokenBean refreshTokenBean = objectObservable.execute().body();
        if (refreshTokenBean == null || refreshTokenBean.getCode() != 0) {
            SPUtils.saveLoginBean(null);
            gotoLogin();
            return null;
        }


        loginBean.getData().setRefreshToken(refreshTokenBean.getResult().getData().getRefresh_token());
        loginBean.getData().setAccessToken(refreshTokenBean.getResult().getData().getAccess_token());
        SPUtils.saveLoginBean(loginBean);
        final String access_token = refreshTokenBean.getResult().getData().getAccess_token();

        if (TextUtils.isEmpty(access_token)) {//获取token 失败
            SPUtils.saveLoginBean(null);
            gotoLogin();
            return null;
        } else {
            HttpUrl.Builder newUrlBuilder = request.url()
                    .newBuilder()
                    .scheme(request.url().scheme())
                    .host(request.url().host())
                    .removeAllQueryParameters("access_token")
                    .addQueryParameter("access_token", access_token);

            Request newRequest = request.newBuilder()
                    .method(request.method(), request.body())
                    .url(newUrlBuilder.build())
                    .build();
            return chain.proceed(newRequest);
        }

    }

    private void gotoLogin() {
        EventBus.getDefault().post("token_miss");//Event.EVENT_TOKEN_MISS
    }
}
