package com.sdzn.fzx.teacher.vo.chatroom;

import java.util.Objects;

/**
 * Created by admin on 2019/8/15.
 */

public class Home {
    String name;
    String url;

    public Home(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Home)) return false;
        Home home = (Home) o;
        return Objects.equals(name, home.name) &&
                Objects.equals(url, home.url);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, url);
    }
}
