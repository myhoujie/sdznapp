package com.sdzn.fzx.teacher.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/8/11
 */
public class VersionInfoBean {
    /**
     * id : 9
     * programTypeName : 直播
     * sysTypeName : 安卓
     * sysTypeId : 0
     * programName : 直播学生端-pad
     * programId : 2
     * versionInfo : 2.3.1
     * targetUrl : https://file.znclass.com/znxt-pad-bxn_nation-V2.3.1.apk
     * versionNum : 75
     * status : 1
     * createTime : 2020-08-19 17:21:27
     * forcedStatus : 0
     * examineStatus : 0
     * description : 123123
     */

    private int id;
    private String programTypeName;
    private String sysTypeName;
    private int sysTypeId;
    private String programName;
    private int programId;
    private String versionInfo;
    private String targetUrl;
    private String versionNum;
    private int status;
    private String createTime;
    private int forcedStatus;
    private int examineStatus;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProgramTypeName() {
        return programTypeName;
    }

    public void setProgramTypeName(String programTypeName) {
        this.programTypeName = programTypeName;
    }

    public String getSysTypeName() {
        return sysTypeName;
    }

    public void setSysTypeName(String sysTypeName) {
        this.sysTypeName = sysTypeName;
    }

    public int getSysTypeId() {
        return sysTypeId;
    }

    public void setSysTypeId(int sysTypeId) {
        this.sysTypeId = sysTypeId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(String versionInfo) {
        this.versionInfo = versionInfo;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(String versionNum) {
        this.versionNum = versionNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getForcedStatus() {
        return forcedStatus;
    }

    public void setForcedStatus(int forcedStatus) {
        this.forcedStatus = forcedStatus;
    }

    public int getExamineStatus() {
        return examineStatus;
    }

    public void setExamineStatus(int examineStatus) {
        this.examineStatus = examineStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * id : 1
     * versionNumber : 60
     * versionInfo : 第一版
     * type : 0
     * targetUrl : http://file.znclass.com/znxt-2.1.0-release60-2019.12.24-apk.apk
     * isOpen : 0
     */

  /*  private int id;
    private int versionNumber;
    private String versionInfo;
    private int type;
    private String targetUrl;
    private int isOpen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(String versionInfo) {
        this.versionInfo = versionInfo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }*/

  /*  private InfoBean versionInfo;

    public InfoBean getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(InfoBean versionInfo) {
        this.versionInfo = versionInfo;
    }

    public static class InfoBean {
        private String versionInfo;
        private int versionNumber;
        private String targetUrl;

        public String getVersionInfo() {
            return versionInfo;
        }

        public void setVersionInfo(String versionInfo) {
            this.versionInfo = versionInfo;
        }

        public int getVersionNumber() {
            return versionNumber;
        }

        public void setVersionNumber(int versionNumber) {
            this.versionNumber = versionNumber;
        }

        public String getTargetUrl() {
            return targetUrl;
        }

        public void setTargetUrl(String targetUrl) {
            this.targetUrl = targetUrl;
        }
    }
*/

}
