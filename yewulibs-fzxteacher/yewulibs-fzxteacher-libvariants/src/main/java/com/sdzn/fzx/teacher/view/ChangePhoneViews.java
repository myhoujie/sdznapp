package com.sdzn.fzx.teacher.view;


import com.haier.cellarette.libmvp.mvp.IView;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface ChangePhoneViews extends IView {
    public void onCountDownChanged(int str);

    void verifySuccess(final String phone, final String code);

    void savePhone();

    void onFailed(String msg);
}
