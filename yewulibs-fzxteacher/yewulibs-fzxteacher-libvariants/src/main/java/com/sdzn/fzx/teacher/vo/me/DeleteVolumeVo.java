package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/8
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class DeleteVolumeVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * volumeList : [{"id":135,"createAccountId":56,"createUserName":"张超","createTime":1520412840000,"userTeacherId":17,"userTeacherName":"张超","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"baseVolumeId":1,"baseVolumeName":"上册","status":true},{"id":136,"createAccountId":56,"createUserName":"张超","createTime":1520412840000,"userTeacherId":17,"userTeacherName":"张超","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"baseVolumeId":2,"baseVolumeName":"下册","status":true}]
         * baseGradeId : 7
         * baseGradeName : 七年级
         */

        private int baseGradeId;
        private String baseGradeName;
        private List<VolumeListBean> volumeList;

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public List<VolumeListBean> getVolumeList() {
            return volumeList;
        }

        public void setVolumeList(List<VolumeListBean> volumeList) {
            this.volumeList = volumeList;
        }

        public static class VolumeListBean {
            /**
             * id : 135
             * createAccountId : 56
             * createUserName : 张超
             * createTime : 1520412840000
             * userTeacherId : 17
             * userTeacherName : 张超
             * baseEducationId : 1
             * baseEducationName : 六三制
             * baseGradeId : 7
             * baseGradeName : 七年级
             * baseSubjectId : 1
             * baseSubjectName : 语文
             * baseVersionId : 12
             * baseVersionName : 人民教育版
             * startGradeId : null
             * startGradeName : null
             * baseVolumeId : 1
             * baseVolumeName : 上册
             * status : true
             */

            private int id;
            private int createAccountId;
            private String createUserName;
            private long createTime;
            private int userTeacherId;
            private String userTeacherName;
            private int baseEducationId;
            private String baseEducationName;
            private int baseGradeId;
            private String baseGradeName;
            private int baseSubjectId;
            private String baseSubjectName;
            private int baseVersionId;
            private String baseVersionName;
            private Object startGradeId;
            private Object startGradeName;
            private int baseVolumeId;
            private String baseVolumeName;
            private boolean status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCreateAccountId() {
                return createAccountId;
            }

            public void setCreateAccountId(int createAccountId) {
                this.createAccountId = createAccountId;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public int getUserTeacherId() {
                return userTeacherId;
            }

            public void setUserTeacherId(int userTeacherId) {
                this.userTeacherId = userTeacherId;
            }

            public String getUserTeacherName() {
                return userTeacherName;
            }

            public void setUserTeacherName(String userTeacherName) {
                this.userTeacherName = userTeacherName;
            }

            public int getBaseEducationId() {
                return baseEducationId;
            }

            public void setBaseEducationId(int baseEducationId) {
                this.baseEducationId = baseEducationId;
            }

            public String getBaseEducationName() {
                return baseEducationName;
            }

            public void setBaseEducationName(String baseEducationName) {
                this.baseEducationName = baseEducationName;
            }

            public int getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(int baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public int getBaseSubjectId() {
                return baseSubjectId;
            }

            public void setBaseSubjectId(int baseSubjectId) {
                this.baseSubjectId = baseSubjectId;
            }

            public String getBaseSubjectName() {
                return baseSubjectName;
            }

            public void setBaseSubjectName(String baseSubjectName) {
                this.baseSubjectName = baseSubjectName;
            }

            public int getBaseVersionId() {
                return baseVersionId;
            }

            public void setBaseVersionId(int baseVersionId) {
                this.baseVersionId = baseVersionId;
            }

            public String getBaseVersionName() {
                return baseVersionName;
            }

            public void setBaseVersionName(String baseVersionName) {
                this.baseVersionName = baseVersionName;
            }

            public Object getStartGradeId() {
                return startGradeId;
            }

            public void setStartGradeId(Object startGradeId) {
                this.startGradeId = startGradeId;
            }

            public Object getStartGradeName() {
                return startGradeName;
            }

            public void setStartGradeName(Object startGradeName) {
                this.startGradeName = startGradeName;
            }

            public int getBaseVolumeId() {
                return baseVolumeId;
            }

            public void setBaseVolumeId(int baseVolumeId) {
                this.baseVolumeId = baseVolumeId;
            }

            public String getBaseVolumeName() {
                return baseVolumeName;
            }

            public void setBaseVolumeName(String baseVolumeName) {
                this.baseVolumeName = baseVolumeName;
            }

            public boolean isStatus() {
                return status;
            }

            public void setStatus(boolean status) {
                this.status = status;
            }
        }
    }
}
