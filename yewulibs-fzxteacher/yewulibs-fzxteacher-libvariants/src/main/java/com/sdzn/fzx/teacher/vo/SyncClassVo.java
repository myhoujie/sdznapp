package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * SyncClassVo〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SyncClassVo {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * classId : 1
         * baseGradeName : 三年级
         * isClassManager : true
         * className : 时代一班
         * classManagerName : 地瓜
         * classManagerId : 1
         * baseGradeId : 3
         */

        private String classId;
        private String baseGradeName;
        private boolean isClassManager;
        private String className;
        private String classManagerName;
        private String classManagerId;
        private String baseGradeId;
        private boolean isChecked;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public boolean isIsClassManager() {
            return isClassManager;
        }

        public void setIsClassManager(boolean isClassManager) {
            this.isClassManager = isClassManager;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassManagerName() {
            return classManagerName;
        }

        public void setClassManagerName(String classManagerName) {
            this.classManagerName = classManagerName;
        }

        public String getClassManagerId() {
            return classManagerId;
        }

        public void setClassManagerId(String classManagerId) {
            this.classManagerId = classManagerId;
        }

        public String getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(String baseGradeId) {
            this.baseGradeId = baseGradeId;
        }
    }
}
