package com.sdzn.fzx.teacher.vo.chatroom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Administrator on 2019/8/21 0013.
 */

public class ChatDiscussBean {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        private int type;
        private String title;
        private int image;

        public DataBean(int type, String title) {
            this.type = type;
            this.title = title;
        }

        public DataBean(int type, String title, int image) {
            this.type = type;
            this.title = title;
            this.image = image;
        }

        protected DataBean(Parcel in) {
            type = in.readInt();
            title = in.readString();
            image = in.readInt();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
        public int getImage() {
            return image;
        }
        public void setImage(int image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(type);
            parcel.writeInt(image);
            parcel.writeString(title);

        }
    }


}
