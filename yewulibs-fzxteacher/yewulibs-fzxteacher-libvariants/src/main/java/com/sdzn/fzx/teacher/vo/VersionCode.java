package com.sdzn.fzx.teacher.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/30
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class VersionCode {

    /**
     * student-android : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"学生端","downUrl":"http://a.com/box.war"}
     * student-ios : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"学生端","downUrl":"http://a.com/box.war"}
     * teacher-android : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"老师端","downUrl":"http://a.com/box.war"}
     * teacher-ios : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"老师端","downUrl":"http://a.com/box.war"}
     * teacher-pc : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"老师端","downUrl":"http://a.com/box.war"}
     * box-center : {"version":"0.0.1","auotUpdate":true,"createTime":"2018-20-21","createName":"王小二","description":"中心盒子","downUrl":"http://192.168.0.215:8886/boxUpgrade/schoolbox.war"}
     */

    @SerializedName("student-android")
    private StudentandroidBean studentandroid;
    @SerializedName("student-ios")
    private StudentiosBean studentios;
    @SerializedName("teacher-android")
    private TeacherandroidBean teacherandroid;
    @SerializedName("teacher-ios")
    private TeacheriosBean teacherios;
    @SerializedName("teacher-pc")
    private TeacherpcBean teacherpc;
    @SerializedName("box-center")
    private BoxcenterBean boxcenter;

    public StudentandroidBean getStudentandroid() {
        return studentandroid;
    }

    public void setStudentandroid(StudentandroidBean studentandroid) {
        this.studentandroid = studentandroid;
    }

    public StudentiosBean getStudentios() {
        return studentios;
    }

    public void setStudentios(StudentiosBean studentios) {
        this.studentios = studentios;
    }

    public TeacherandroidBean getTeacherandroid() {
        return teacherandroid;
    }

    public void setTeacherandroid(TeacherandroidBean teacherandroid) {
        this.teacherandroid = teacherandroid;
    }

    public TeacheriosBean getTeacherios() {
        return teacherios;
    }

    public void setTeacherios(TeacheriosBean teacherios) {
        this.teacherios = teacherios;
    }

    public TeacherpcBean getTeacherpc() {
        return teacherpc;
    }

    public void setTeacherpc(TeacherpcBean teacherpc) {
        this.teacherpc = teacherpc;
    }

    public BoxcenterBean getBoxcenter() {
        return boxcenter;
    }

    public void setBoxcenter(BoxcenterBean boxcenter) {
        this.boxcenter = boxcenter;
    }

    public static class StudentandroidBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 学生端
         * downUrl : http://a.com/box.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }

    public static class StudentiosBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 学生端
         * downUrl : http://a.com/box.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }

    public static class TeacherandroidBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 老师端
         * downUrl : http://a.com/box.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }

    public static class TeacheriosBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 老师端
         * downUrl : http://a.com/box.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }

    public static class TeacherpcBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 老师端
         * downUrl : http://a.com/box.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }

    public static class BoxcenterBean {
        /**
         * version : 0.0.1
         * auotUpdate : true
         * createTime : 2018-20-21
         * createName : 王小二
         * description : 中心盒子
         * downUrl : http://192.168.0.215:8886/boxUpgrade/schoolbox.war
         */

        private String version;
        private boolean auotUpdate;
        private String createTime;
        private String createName;
        private String description;
        private String downUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isAuotUpdate() {
            return auotUpdate;
        }

        public void setAuotUpdate(boolean auotUpdate) {
            this.auotUpdate = auotUpdate;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateName() {
            return createName;
        }

        public void setCreateName(String createName) {
            this.createName = createName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }
    }
}
