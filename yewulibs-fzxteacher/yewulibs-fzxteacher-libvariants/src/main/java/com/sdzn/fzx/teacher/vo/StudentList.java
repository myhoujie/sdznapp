package com.sdzn.fzx.teacher.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class StudentList {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 18264
         * userStudentId : 97011
         * userStudentName : 王兔兔
         * status : 2
         * photo : null
         * isCorrect : 0
         * scoreTotal : 4
         * useTime : 23
         * classGroupId : 489
         * classGroupName : 2
         * correctType : 3
         * teacherCorrectStatus : 1
         * studentCorrectStatus : 0
         * studentCorrectName :
         * scoreObjective : 4
         * scoreSubjective : 0
         */

        private int id;
        private int userStudentId;
        private String userStudentName;
        private int status;
        private Object photo;
        private int isCorrect;
        private String scoreTotal;
        private int useTime;
        private int classGroupId;
        private String classGroupName;
        private int correctType;
        private int teacherCorrectStatus;
        private int studentCorrectStatus;
        private String studentCorrectName;
        private int scoreObjective;
        private int scoreSubjective;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getPhoto() {
            return photo;
        }

        public void setPhoto(Object photo) {
            this.photo = photo;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public String getScoreTotal() {

            return new BigDecimal(scoreTotal).stripTrailingZeros().toPlainString();
        }

        public void setScoreTotal(String scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public int getTeacherCorrectStatus() {
            return teacherCorrectStatus;
        }

        public void setTeacherCorrectStatus(int teacherCorrectStatus) {
            this.teacherCorrectStatus = teacherCorrectStatus;
        }

        public int getStudentCorrectStatus() {
            return studentCorrectStatus;
        }

        public void setStudentCorrectStatus(int studentCorrectStatus) {
            this.studentCorrectStatus = studentCorrectStatus;
        }

        public String getStudentCorrectName() {
            return studentCorrectName;
        }

        public void setStudentCorrectName(String studentCorrectName) {
            this.studentCorrectName = studentCorrectName;
        }

        public int getScoreObjective() {
            return scoreObjective;
        }

        public void setScoreObjective(int scoreObjective) {
            this.scoreObjective = scoreObjective;
        }

        public int getScoreSubjective() {
            return scoreSubjective;
        }

        public void setScoreSubjective(int scoreSubjective) {
            this.scoreSubjective = scoreSubjective;
        }
    }



    /*private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        *//**
         * id : 71033
         * userStudentId : 5165
         * userStudentName : 学生61
         * status : 0
         * photo : null
         * isCorrect : 0
         * scoreTotal : -1
         * useTime : 0
         * classGroupId : 0
         * classGroupName : 未分组
         * correctType : 3
         *//*

        private int id;
        private int userStudentId;
        private String userStudentName;
        private int status;
        private Object photo;
        private int isCorrect;
        private String scoreTotal;
        private int useTime;
        private int classGroupId;
        private String classGroupName;
        private int correctType;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getPhoto() {
            return photo;
        }

        public void setPhoto(Object photo) {
            this.photo = photo;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public String getScoreTotal() {

            return new BigDecimal(scoreTotal).stripTrailingZeros().toPlainString();
        }

        public void setScoreTotal(String scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }
    }*/
}
