package com.sdzn.fzx.teacher.presenter;

import com.sdzn.fzx.teacher.view.TakeOfficeViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class TakeOfficePresenter1 extends Presenter<TakeOfficeViews> {
//,
//                ,
   public void getTakeOfficeInfo(String token,String id,String schoolId) {
       RetrofitNetNew.build(Api.class, getIdentifier())
               .getTeacherClasses(token,id, schoolId)
               .enqueue(new Callback<ResponseSlbBean1<TakeOfficeVo>>() {
                   @Override
                   public void onResponse(Call<ResponseSlbBean1<TakeOfficeVo>> call, Response<ResponseSlbBean1<TakeOfficeVo>> response) {
                       if (!hasView()) {
                           return;
                       }
                       if (response.body() == null) {
                           return;
                       }
                       if (response.body().getCode() != 0) {
                           Log.d("TakeOfficePresenter1","11111"+ call.toString());
                           getView().onFailed(response.body().getMessage());
                           return;
                       }
                       getView().setTakeOfficeData(response.body().getResult());
//                       getView().changePassswordSuccess();

                       call.cancel();
                   }

                   @Override
                   public void onFailure(Call<ResponseSlbBean1<TakeOfficeVo>> call, Throwable t) {
                       if (!hasView()) {
                           return;
                       }
                       Log.d("TakeOfficePresenter1","22222"+ call.toString());
                       t.printStackTrace();
                       call.cancel();
                   }
               });


    }
}
