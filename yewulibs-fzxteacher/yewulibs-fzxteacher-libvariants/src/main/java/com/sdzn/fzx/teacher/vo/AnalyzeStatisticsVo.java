package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/20
 * 修改单号：
 * 修改内容:
 */
public class AnalyzeStatisticsVo {
    /**
     * data : {"halfRightStudentList":[],"rightRate":0,"studentTotalCount":10,"halfRightCount":0,"errorStudentList":[{"lessonTaskStudentId":7,"lessonAnswerExamId":19,"lessonTaskId":2,"lessonTaskDetailsId":7,"photo":null,"userStudentName":"测试15","id":19,"userStudentId":4596}],"errorRate":10,"halfRightRate":0,"rightStudentList":[],"rightCount":0,"errorCount":1}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * halfRightStudentList : []
         * rightRate : 0
         * studentTotalCount : 10
         * halfRightCount : 0
         * errorStudentList : [{"lessonTaskStudentId":7,"lessonAnswerExamId":19,"lessonTaskId":2,"lessonTaskDetailsId":7,"photo":null,"userStudentName":"测试15","id":19,"userStudentId":4596}]
         * errorRate : 10
         * halfRightRate : 0
         * rightStudentList : []
         * rightCount : 0
         * errorCount : 1
         */

        private int rightRate;
        private int studentTotalCount;
        private int halfRightCount;
        private int errorRate;
        private int halfRightRate;
        private int rightCount;
        private int errorCount;
        private List<ErrorStudentListBean> halfRightStudentList;
        private List<ErrorStudentListBean> errorStudentList;
        private List<ErrorStudentListBean> rightStudentList;

        public int getRightRate() {
            return rightRate;
        }

        public void setRightRate(int rightRate) {
            this.rightRate = rightRate;
        }

        public int getStudentTotalCount() {
            return studentTotalCount;
        }

        public void setStudentTotalCount(int studentTotalCount) {
            this.studentTotalCount = studentTotalCount;
        }

        public int getHalfRightCount() {
            return halfRightCount;
        }

        public void setHalfRightCount(int halfRightCount) {
            this.halfRightCount = halfRightCount;
        }

        public int getErrorRate() {
            return errorRate;
        }

        public void setErrorRate(int errorRate) {
            this.errorRate = errorRate;
        }

        public int getHalfRightRate() {
            return halfRightRate;
        }

        public void setHalfRightRate(int halfRightRate) {
            this.halfRightRate = halfRightRate;
        }

        public int getRightCount() {
            return rightCount;
        }

        public void setRightCount(int rightCount) {
            this.rightCount = rightCount;
        }

        public int getErrorCount() {
            return errorCount;
        }

        public void setErrorCount(int errorCount) {
            this.errorCount = errorCount;
        }

        public List<ErrorStudentListBean> getHalfRightStudentList() {
            return halfRightStudentList;
        }

        public void setHalfRightStudentList(List<ErrorStudentListBean> halfRightStudentList) {
            this.halfRightStudentList = halfRightStudentList;
        }

        public List<ErrorStudentListBean> getErrorStudentList() {
            return errorStudentList;
        }

        public void setErrorStudentList(List<ErrorStudentListBean> errorStudentList) {
            this.errorStudentList = errorStudentList;
        }

        public List<ErrorStudentListBean> getRightStudentList() {
            return rightStudentList;
        }

        public void setRightStudentList(List<ErrorStudentListBean> rightStudentList) {
            this.rightStudentList = rightStudentList;
        }

        public static class ErrorStudentListBean {
            /**
             * lessonTaskStudentId : 7
             * lessonAnswerExamId : 19
             * lessonTaskId : 2
             * lessonTaskDetailsId : 7
             * photo : null
             * userStudentName : 测试15
             * id : 19
             * userStudentId : 4596
             */

            private int lessonTaskStudentId;
            private int lessonAnswerExamId;
            private int lessonTaskId;
            private int lessonTaskDetailsId;
            private Object photo;
            private String userStudentName;
            private int id;
            private int userStudentId;

            public int getLessonTaskStudentId() {
                return lessonTaskStudentId;
            }

            public void setLessonTaskStudentId(int lessonTaskStudentId) {
                this.lessonTaskStudentId = lessonTaskStudentId;
            }

            public int getLessonAnswerExamId() {
                return lessonAnswerExamId;
            }

            public void setLessonAnswerExamId(int lessonAnswerExamId) {
                this.lessonAnswerExamId = lessonAnswerExamId;
            }

            public int getLessonTaskId() {
                return lessonTaskId;
            }

            public void setLessonTaskId(int lessonTaskId) {
                this.lessonTaskId = lessonTaskId;
            }

            public int getLessonTaskDetailsId() {
                return lessonTaskDetailsId;
            }

            public void setLessonTaskDetailsId(int lessonTaskDetailsId) {
                this.lessonTaskDetailsId = lessonTaskDetailsId;
            }

            public Object getPhoto() {
                return photo;
            }

            public void setPhoto(Object photo) {
                this.photo = photo;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }
        }
    }
}
