package com.sdzn.fzx.teacher.presenter;


import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.SubjectData;
import com.sdzn.fzx.teacher.bean.SubjectData1;
import com.sdzn.fzx.teacher.view.MainIndexViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class MainIndexPresenter extends Presenter<MainIndexViews> {

    public void getSubjectInfo(String id) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .getSubjectData(id)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<SubjectData1>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<SubjectData1>> call, Response<ResponseSlbBean1<SubjectData1>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnSubjectNodata(response.body().getMessage());
                            return;
                        }
                        getView().getSubjectInfo(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<SubjectData1>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onSubjectFailed("");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


}
