package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/18
 * 修改单号：
 * 修改内容:
 */
public class CorrectParam {
    private int id;
    private int parentId;
    private int isRight;
    private int score;
    private int fullScore;
    private int isCorrent;
    private List<CorrectParam> children;

    public List<CorrectParam> getChildren() {
        return children;
    }

    public void setChildren(List<CorrectParam> children) {
        this.children = children;
    }

    public int getExamTypeId() {
        return examTypeId;
    }

    public void setExamTypeId(int examTypeId) {
        this.examTypeId = examTypeId;
    }

    private int examTypeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getIsRight() {
        return isRight;
    }

    public void setIsRight(int isRight) {
        this.isRight = isRight;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getFullScore() {
        return fullScore;
    }

    public void setFullScore(int fullScore) {
        this.fullScore = fullScore;
    }

    public int getIsCorrent() {
        return isCorrent;
    }

    public void setIsCorrent(int isCorrent) {
        this.isCorrent = isCorrent;
    }
}
