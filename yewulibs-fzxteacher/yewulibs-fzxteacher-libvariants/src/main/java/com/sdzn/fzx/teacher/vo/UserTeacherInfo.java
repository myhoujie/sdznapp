package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/28
 * 修改单号：
 * 修改内容:
 */
public class UserTeacherInfo {
    /**
     * data : {"baseInfo":{"teacherId":216,"realName":"黄东月","photo":"http://118.190.68.12:9001/head/2018/11/087d4666ca874cdf8e3571cde1299085_thn.jpg","sex":2,"customerSchoolId":36,"customerSchoolName":"时代智囊测试","zoneNamePath":null,"telephone":"18366166528","accountId":5024,"accountName":"65370421"},"teachInfo":[{"subjectId":0,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":0,"subjectName":"班主任"}],"subjectName":"班主任"},{"subjectId":1,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":1,"subjectName":"语文"}],"subjectName":"语文"},{"subjectId":2,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":2,"subjectName":"数学"}],"subjectName":"数学"},{"subjectId":3,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":3,"subjectName":"英语"}],"subjectName":"英语"},{"subjectId":4,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":4,"subjectName":"物理"}],"subjectName":"物理"},{"subjectId":5,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":5,"subjectName":"化学"}],"subjectName":"化学"},{"subjectId":6,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":6,"subjectName":"生物"}],"subjectName":"生物"},{"subjectId":7,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":7,"subjectName":"历史"}],"subjectName":"历史"},{"subjectId":8,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":8,"subjectName":"地理"}],"subjectName":"地理"}],"reviews":[{"id":1920,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":1,"name":null,"reviewTypeName":"上学期期中复习","status":true},{"id":1936,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":1,"name":null,"reviewTypeName":"上学期期中复习","status":true},{"id":1921,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":2,"name":null,"reviewTypeName":"上学期期末复习","status":true},{"id":1937,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":2,"name":null,"reviewTypeName":"上学期期末复习","status":true},{"id":1922,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":3,"name":null,"reviewTypeName":"下学期期中复习","status":true},{"id":1938,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":3,"name":null,"reviewTypeName":"下学期期中复习","status":true},{"id":1923,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":4,"name":null,"reviewTypeName":"下学期期末复习","status":true},{"id":1939,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":4,"name":null,"reviewTypeName":"下学期期末复习","status":true}],"versionInfo":[{"baseSubjectId":1,"baseSubjectName":"语文","volumeList":[{"id":2119,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":1,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2120,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":2,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2121,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":4,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2122,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":5,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2123,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":7,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2124,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":8,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":2,"baseSubjectName":"数学","volumeList":[{"id":2115,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":28,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2116,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":29,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2117,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":31,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2118,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":32,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":3,"baseSubjectName":"英语","volumeList":[{"id":2125,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":10,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2126,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":11,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2127,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":13,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2128,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":14,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":4,"baseSubjectName":"物理","volumeList":[{"id":2129,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711179000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":4,"baseSubjectName":"物理","baseVersionId":8,"baseVersionName":"沪科版","startGradeId":null,"startGradeName":null,"baseVolumeId":40,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2130,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711179000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":4,"baseSubjectName":"物理","baseVersionId":8,"baseVersionName":"沪科版","startGradeId":null,"startGradeName":null,"baseVolumeId":41,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":5,"baseSubjectName":"化学","volumeList":[{"id":2131,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711191000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":5,"baseSubjectName":"化学","baseVersionId":20,"baseVersionName":"鲁教新版","startGradeId":null,"startGradeName":null,"baseVolumeId":232,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2132,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711191000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":5,"baseSubjectName":"化学","baseVersionId":20,"baseVersionName":"鲁教新版","startGradeId":null,"startGradeName":null,"baseVolumeId":233,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":6,"baseSubjectName":"生物","volumeList":[{"id":1469,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":73,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1470,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":74,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1471,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":76,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1472,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":77,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1543,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128309000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":79,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1544,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128309000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":80,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":7,"baseSubjectName":"历史","volumeList":[{"id":2133,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":19,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2134,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":20,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2135,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":22,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2136,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":23,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":8,"baseSubjectName":"地理","volumeList":[{"id":1545,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":46,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1546,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":47,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1547,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":49,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1548,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":50,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1549,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":52,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1550,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":53,"baseVolumeName":"下册","status":true,"baseLevelId":null}]}],"confInfo":[{"id":665,"code":"GOOD_SURFAC","name":"优秀卷面","value":"0","describe":null,"userTeacherId":216},{"id":666,"code":"BAD_SURFACE","name":"粗糙卷面","value":"0","describe":null,"userTeacherId":216},{"id":667,"code":"GOOD_ANSWER","name":"优秀作答","value":"0","describe":null,"userTeacherId":216},{"id":668,"code":"BAD_ANSWER","name":"典型错误","value":"1","describe":null,"userTeacherId":216}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * baseInfo : {"teacherId":216,"realName":"黄东月","photo":"http://118.190.68.12:9001/head/2018/11/087d4666ca874cdf8e3571cde1299085_thn.jpg","sex":2,"customerSchoolId":36,"customerSchoolName":"时代智囊测试","zoneNamePath":null,"telephone":"18366166528","accountId":5024,"accountName":"65370421"}
         * teachInfo : [{"subjectId":0,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":0,"subjectName":"班主任"}],"subjectName":"班主任"},{"subjectId":1,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":1,"subjectName":"语文"}],"subjectName":"语文"},{"subjectId":2,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":2,"subjectName":"数学"}],"subjectName":"数学"},{"subjectId":3,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":3,"subjectName":"英语"}],"subjectName":"英语"},{"subjectId":4,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":4,"subjectName":"物理"}],"subjectName":"物理"},{"subjectId":5,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":5,"subjectName":"化学"}],"subjectName":"化学"},{"subjectId":6,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":6,"subjectName":"生物"}],"subjectName":"生物"},{"subjectId":7,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":7,"subjectName":"历史"}],"subjectName":"历史"},{"subjectId":8,"classList":[{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":8,"subjectName":"地理"}],"subjectName":"地理"}]
         * reviews : [{"id":1920,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":1,"name":null,"reviewTypeName":"上学期期中复习","status":true},{"id":1936,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":1,"name":null,"reviewTypeName":"上学期期中复习","status":true},{"id":1921,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":2,"name":null,"reviewTypeName":"上学期期末复习","status":true},{"id":1937,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":2,"name":null,"reviewTypeName":"上学期期末复习","status":true},{"id":1922,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":3,"name":null,"reviewTypeName":"下学期期中复习","status":true},{"id":1938,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":3,"name":null,"reviewTypeName":"下学期期中复习","status":true},{"id":1923,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":4,"name":null,"reviewTypeName":"下学期期末复习","status":true},{"id":1939,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1540953260000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseGradeId":7,"baseGradeName":"七年级","reviewType":4,"name":null,"reviewTypeName":"下学期期末复习","status":true}]
         * versionInfo : [{"baseSubjectId":1,"baseSubjectName":"语文","volumeList":[{"id":2119,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":1,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2120,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":2,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2121,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":4,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2122,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":5,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2123,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":7,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2124,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":8,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":2,"baseSubjectName":"数学","volumeList":[{"id":2115,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":28,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2116,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":29,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2117,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":31,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2118,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711110000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":5,"baseVersionName":"北师大版","startGradeId":null,"startGradeName":null,"baseVolumeId":32,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":3,"baseSubjectName":"英语","volumeList":[{"id":2125,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":10,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2126,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":11,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2127,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":13,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2128,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711163000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":3,"baseVersionName":"人教新目标版","startGradeId":null,"startGradeName":null,"baseVolumeId":14,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":4,"baseSubjectName":"物理","volumeList":[{"id":2129,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711179000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":4,"baseSubjectName":"物理","baseVersionId":8,"baseVersionName":"沪科版","startGradeId":null,"startGradeName":null,"baseVolumeId":40,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2130,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711179000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":4,"baseSubjectName":"物理","baseVersionId":8,"baseVersionName":"沪科版","startGradeId":null,"startGradeName":null,"baseVolumeId":41,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":5,"baseSubjectName":"化学","volumeList":[{"id":2131,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711191000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":5,"baseSubjectName":"化学","baseVersionId":20,"baseVersionName":"鲁教新版","startGradeId":null,"startGradeName":null,"baseVolumeId":232,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2132,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711191000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":5,"baseSubjectName":"化学","baseVersionId":20,"baseVersionName":"鲁教新版","startGradeId":null,"startGradeName":null,"baseVolumeId":233,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":6,"baseSubjectName":"生物","volumeList":[{"id":1469,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":73,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1470,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":74,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1471,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":76,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1472,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541066156000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":77,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1543,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128309000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":79,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1544,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128309000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":6,"baseSubjectName":"生物","baseVersionId":12,"baseVersionName":"济南版","startGradeId":null,"startGradeName":null,"baseVolumeId":80,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":7,"baseSubjectName":"历史","volumeList":[{"id":2133,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":19,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2134,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":20,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2135,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":22,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2136,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711206000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":7,"baseSubjectName":"历史","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":23,"baseVolumeName":"下册","status":true,"baseLevelId":null}]},{"baseSubjectId":8,"baseSubjectName":"地理","volumeList":[{"id":1545,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":46,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1546,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":47,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1547,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":49,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1548,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":50,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":1549,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":52,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":1550,"createAccountId":5024,"createUserName":"huangdongyue","createTime":1541128320000,"userTeacherId":216,"userTeacherName":"huangdongyue","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":8,"baseSubjectName":"地理","baseVersionId":2,"baseVersionName":"人教版","startGradeId":null,"startGradeName":null,"baseVolumeId":53,"baseVolumeName":"下册","status":true,"baseLevelId":null}]}]
         * confInfo : [{"id":665,"code":"GOOD_SURFAC","name":"优秀卷面","value":"0","describe":null,"userTeacherId":216},{"id":666,"code":"BAD_SURFACE","name":"粗糙卷面","value":"0","describe":null,"userTeacherId":216},{"id":667,"code":"GOOD_ANSWER","name":"优秀作答","value":"0","describe":null,"userTeacherId":216},{"id":668,"code":"BAD_ANSWER","name":"典型错误","value":"1","describe":null,"userTeacherId":216}]
         */

        private BaseInfoBean baseInfo;
        private List<TeachInfoBean> teachInfo;
        private List<ReviewsBean> reviews;
        private List<VersionInfoBean> versionInfo;
        private List<ConfInfoBean> confInfo;

        public BaseInfoBean getBaseInfo() {
            return baseInfo;
        }

        public void setBaseInfo(BaseInfoBean baseInfo) {
            this.baseInfo = baseInfo;
        }

        public List<TeachInfoBean> getTeachInfo() {
            return teachInfo;
        }

        public void setTeachInfo(List<TeachInfoBean> teachInfo) {
            this.teachInfo = teachInfo;
        }

        public List<ReviewsBean> getReviews() {
            return reviews;
        }

        public void setReviews(List<ReviewsBean> reviews) {
            this.reviews = reviews;
        }

        public List<VersionInfoBean> getVersionInfo() {
            return versionInfo;
        }

        public void setVersionInfo(List<VersionInfoBean> versionInfo) {
            this.versionInfo = versionInfo;
        }

        public List<ConfInfoBean> getConfInfo() {
            return confInfo;
        }

        public void setConfInfo(List<ConfInfoBean> confInfo) {
            this.confInfo = confInfo;
        }

        public static class BaseInfoBean {
            /**
             * teacherId : 216
             * realName : 黄东月
             * photo : http://118.190.68.12:9001/head/2018/11/087d4666ca874cdf8e3571cde1299085_thn.jpg
             * sex : 2
             * customerSchoolId : 36
             * customerSchoolName : 时代智囊测试
             * zoneNamePath : null
             * telephone : 18366166528
             * accountId : 5024
             * accountName : 65370421
             */

            private int teacherId;
            private String realName;
            private String photo;
            private int sex;
            private int customerSchoolId;
            private String customerSchoolName;
            private Object zoneNamePath;
            private String telephone;
            private int accountId;
            private String accountName;

            public int getTeacherId() {
                return teacherId;
            }

            public void setTeacherId(int teacherId) {
                this.teacherId = teacherId;
            }

            public String getRealName() {
                return realName;
            }

            public void setRealName(String realName) {
                this.realName = realName;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public int getCustomerSchoolId() {
                return customerSchoolId;
            }

            public void setCustomerSchoolId(int customerSchoolId) {
                this.customerSchoolId = customerSchoolId;
            }

            public String getCustomerSchoolName() {
                return customerSchoolName;
            }

            public void setCustomerSchoolName(String customerSchoolName) {
                this.customerSchoolName = customerSchoolName;
            }

            public Object getZoneNamePath() {
                return zoneNamePath;
            }

            public void setZoneNamePath(Object zoneNamePath) {
                this.zoneNamePath = zoneNamePath;
            }

            public String getTelephone() {
                return telephone;
            }

            public void setTelephone(String telephone) {
                this.telephone = telephone;
            }

            public int getAccountId() {
                return accountId;
            }

            public void setAccountId(int accountId) {
                this.accountId = accountId;
            }

            public String getAccountName() {
                return accountName;
            }

            public void setAccountName(String accountName) {
                this.accountName = accountName;
            }
        }

        public static class TeachInfoBean {
            /**
             * subjectId : 0
             * classList : [{"classId":604,"baseGradeName":"七年级","className":"十一班","baseGradeId":7,"subjectId":0,"subjectName":"班主任"}]
             * subjectName : 班主任
             */

            private int subjectId;
            private String subjectName;
            private List<ClassListBean> classList;

            public int getSubjectId() {
                return subjectId;
            }

            public void setSubjectId(int subjectId) {
                this.subjectId = subjectId;
            }

            public String getSubjectName() {
                return subjectName;
            }

            public void setSubjectName(String subjectName) {
                this.subjectName = subjectName;
            }

            public List<ClassListBean> getClassList() {
                return classList;
            }

            public void setClassList(List<ClassListBean> classList) {
                this.classList = classList;
            }

            public static class ClassListBean {
                /**
                 * classId : 604
                 * baseGradeName : 七年级
                 * className : 十一班
                 * baseGradeId : 7
                 * subjectId : 0
                 * subjectName : 班主任
                 */

                private int classId;
                private String baseGradeName;
                private String className;
                private int baseGradeId;
                private int subjectId;
                private String subjectName;

                public int getClassId() {
                    return classId;
                }

                public void setClassId(int classId) {
                    this.classId = classId;
                }

                public String getBaseGradeName() {
                    return baseGradeName;
                }

                public void setBaseGradeName(String baseGradeName) {
                    this.baseGradeName = baseGradeName;
                }

                public String getClassName() {
                    return className;
                }

                public void setClassName(String className) {
                    this.className = className;
                }

                public int getBaseGradeId() {
                    return baseGradeId;
                }

                public void setBaseGradeId(int baseGradeId) {
                    this.baseGradeId = baseGradeId;
                }

                public int getSubjectId() {
                    return subjectId;
                }

                public void setSubjectId(int subjectId) {
                    this.subjectId = subjectId;
                }

                public String getSubjectName() {
                    return subjectName;
                }

                public void setSubjectName(String subjectName) {
                    this.subjectName = subjectName;
                }
            }
        }

        public static class ReviewsBean {
            /**
             * id : 1920
             * createAccountId : 5024
             * createUserName : huangdongyue
             * createTime : 1540953260000
             * userTeacherId : 216
             * userTeacherName : huangdongyue
             * baseGradeId : 7
             * baseGradeName : 七年级
             * reviewType : 1
             * name : null
             * reviewTypeName : 上学期期中复习
             * status : true
             */

            private int id;
            private int createAccountId;
            private String createUserName;
            private long createTime;
            private int userTeacherId;
            private String userTeacherName;
            private int baseGradeId;
            private String baseGradeName;
            private int reviewType;
            private Object name;
            private String reviewTypeName;
            private boolean status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCreateAccountId() {
                return createAccountId;
            }

            public void setCreateAccountId(int createAccountId) {
                this.createAccountId = createAccountId;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public int getUserTeacherId() {
                return userTeacherId;
            }

            public void setUserTeacherId(int userTeacherId) {
                this.userTeacherId = userTeacherId;
            }

            public String getUserTeacherName() {
                return userTeacherName;
            }

            public void setUserTeacherName(String userTeacherName) {
                this.userTeacherName = userTeacherName;
            }

            public int getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(int baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public int getReviewType() {
                return reviewType;
            }

            public void setReviewType(int reviewType) {
                this.reviewType = reviewType;
            }

            public Object getName() {
                return name;
            }

            public void setName(Object name) {
                this.name = name;
            }

            public String getReviewTypeName() {
                return reviewTypeName;
            }

            public void setReviewTypeName(String reviewTypeName) {
                this.reviewTypeName = reviewTypeName;
            }

            public boolean isStatus() {
                return status;
            }

            public void setStatus(boolean status) {
                this.status = status;
            }
        }

        public static class VersionInfoBean {
            /**
             * baseSubjectId : 1
             * baseSubjectName : 语文
             * volumeList : [{"id":2119,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":1,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2120,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":2,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2121,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":4,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2122,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":5,"baseVolumeName":"下册","status":true,"baseLevelId":null},{"id":2123,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":7,"baseVolumeName":"上册","status":true,"baseLevelId":null},{"id":2124,"createAccountId":5024,"createUserName":"黄东月","createTime":1547711139000,"userTeacherId":216,"userTeacherName":"黄东月","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","startGradeId":null,"startGradeName":null,"baseVolumeId":8,"baseVolumeName":"下册","status":true,"baseLevelId":null}]
             */

            private int baseSubjectId;
            private String baseSubjectName;
            private List<VolumeListBean> volumeList;

            public int getBaseSubjectId() {
                return baseSubjectId;
            }

            public void setBaseSubjectId(int baseSubjectId) {
                this.baseSubjectId = baseSubjectId;
            }

            public String getBaseSubjectName() {
                return baseSubjectName;
            }

            public void setBaseSubjectName(String baseSubjectName) {
                this.baseSubjectName = baseSubjectName;
            }

            public List<VolumeListBean> getVolumeList() {
                return volumeList;
            }

            public void setVolumeList(List<VolumeListBean> volumeList) {
                this.volumeList = volumeList;
            }

            public static class VolumeListBean {
                /**
                 * id : 2119
                 * createAccountId : 5024
                 * createUserName : 黄东月
                 * createTime : 1547711139000
                 * userTeacherId : 216
                 * userTeacherName : 黄东月
                 * baseEducationId : 1
                 * baseEducationName : 六三制
                 * baseGradeId : 7
                 * baseGradeName : 七年级
                 * baseSubjectId : 1
                 * baseSubjectName : 语文
                 * baseVersionId : 1
                 * baseVersionName : 人教版（部编版）
                 * startGradeId : null
                 * startGradeName : null
                 * baseVolumeId : 1
                 * baseVolumeName : 上册
                 * status : true
                 * baseLevelId : null
                 */

                private int id;
                private int createAccountId;
                private String createUserName;
                private long createTime;
                private int userTeacherId;
                private String userTeacherName;
                private int baseEducationId;
                private String baseEducationName;
                private int baseGradeId;
                private String baseGradeName;
                private int baseSubjectId;
                private String baseSubjectName;
                private int baseVersionId;
                private String baseVersionName;
                private Object startGradeId;
                private Object startGradeName;
                private int baseVolumeId;
                private String baseVolumeName;
                private boolean status;
                private Object baseLevelId;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getCreateAccountId() {
                    return createAccountId;
                }

                public void setCreateAccountId(int createAccountId) {
                    this.createAccountId = createAccountId;
                }

                public String getCreateUserName() {
                    return createUserName;
                }

                public void setCreateUserName(String createUserName) {
                    this.createUserName = createUserName;
                }

                public long getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(long createTime) {
                    this.createTime = createTime;
                }

                public int getUserTeacherId() {
                    return userTeacherId;
                }

                public void setUserTeacherId(int userTeacherId) {
                    this.userTeacherId = userTeacherId;
                }

                public String getUserTeacherName() {
                    return userTeacherName;
                }

                public void setUserTeacherName(String userTeacherName) {
                    this.userTeacherName = userTeacherName;
                }

                public int getBaseEducationId() {
                    return baseEducationId;
                }

                public void setBaseEducationId(int baseEducationId) {
                    this.baseEducationId = baseEducationId;
                }

                public String getBaseEducationName() {
                    return baseEducationName;
                }

                public void setBaseEducationName(String baseEducationName) {
                    this.baseEducationName = baseEducationName;
                }

                public int getBaseGradeId() {
                    return baseGradeId;
                }

                public void setBaseGradeId(int baseGradeId) {
                    this.baseGradeId = baseGradeId;
                }

                public String getBaseGradeName() {
                    return baseGradeName;
                }

                public void setBaseGradeName(String baseGradeName) {
                    this.baseGradeName = baseGradeName;
                }

                public int getBaseSubjectId() {
                    return baseSubjectId;
                }

                public void setBaseSubjectId(int baseSubjectId) {
                    this.baseSubjectId = baseSubjectId;
                }

                public String getBaseSubjectName() {
                    return baseSubjectName;
                }

                public void setBaseSubjectName(String baseSubjectName) {
                    this.baseSubjectName = baseSubjectName;
                }

                public int getBaseVersionId() {
                    return baseVersionId;
                }

                public void setBaseVersionId(int baseVersionId) {
                    this.baseVersionId = baseVersionId;
                }

                public String getBaseVersionName() {
                    return baseVersionName;
                }

                public void setBaseVersionName(String baseVersionName) {
                    this.baseVersionName = baseVersionName;
                }

                public Object getStartGradeId() {
                    return startGradeId;
                }

                public void setStartGradeId(Object startGradeId) {
                    this.startGradeId = startGradeId;
                }

                public Object getStartGradeName() {
                    return startGradeName;
                }

                public void setStartGradeName(Object startGradeName) {
                    this.startGradeName = startGradeName;
                }

                public int getBaseVolumeId() {
                    return baseVolumeId;
                }

                public void setBaseVolumeId(int baseVolumeId) {
                    this.baseVolumeId = baseVolumeId;
                }

                public String getBaseVolumeName() {
                    return baseVolumeName;
                }

                public void setBaseVolumeName(String baseVolumeName) {
                    this.baseVolumeName = baseVolumeName;
                }

                public boolean isStatus() {
                    return status;
                }

                public void setStatus(boolean status) {
                    this.status = status;
                }

                public Object getBaseLevelId() {
                    return baseLevelId;
                }

                public void setBaseLevelId(Object baseLevelId) {
                    this.baseLevelId = baseLevelId;
                }
            }
        }

        public static class ConfInfoBean {
            /**
             * id : 665
             * code : GOOD_SURFAC
             * name : 优秀卷面
             * value : 0
             * describe : null
             * userTeacherId : 216
             */

            private int id;
            private String code;
            private String name;
            private String value;
            private Object describe;
            private int userTeacherId;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public Object getDescribe() {
                return describe;
            }

            public void setDescribe(Object describe) {
                this.describe = describe;
            }

            public int getUserTeacherId() {
                return userTeacherId;
            }

            public void setUserTeacherId(int userTeacherId) {
                this.userTeacherId = userTeacherId;
            }
        }
    }
}
