package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class VersionVo {
    /**
     * total : 9
     * data : [{"id":19,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"scope":2017},{"id":22,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"scope":2017},{"id":25,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":12,"baseVersionName":"人民教育版","startGradeId":null,"startGradeName":null,"scope":2017},{"id":20,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017},{"id":23,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017},{"id":26,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":8,"baseGradeName":"八年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017},{"id":21,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017},{"id":24,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":2,"baseSubjectName":"数学","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017},{"id":27,"createAccountId":1,"createUserName":"超级管理员","createTime":1515642687000,"customerSchoolId":2,"customerSchoolName":"山东大学附属中学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":9,"baseGradeName":"九年级","baseSubjectId":3,"baseSubjectName":"英语","baseVersionId":null,"baseVersionName":null,"startGradeId":null,"startGradeName":null,"scope":2017}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 19
         * createAccountId : 1
         * createUserName : 超级管理员
         * createTime : 1515642687000
         * customerSchoolId : 2
         * customerSchoolName : 山东大学附属中学
         * baseEducationId : 1
         * baseEducationName : 六三制
         * baseGradeId : 7
         * baseGradeName : 七年级
         * baseSubjectId : 1
         * baseSubjectName : 语文
         * baseVersionId : 12
         * baseVersionName : 人民教育版
         * startGradeId : null
         * startGradeName : null
         * scope : 2017
         */

        private int id;
        private int createAccountId;
        private String createUserName;
        private long createTime;
        private int customerSchoolId;
        private String customerSchoolName;
        private int baseEducationId;
        private String baseEducationName;
        private int baseGradeId;
        private String baseGradeName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseVersionId;
        private String baseVersionName;
        private Object startGradeId;
        private Object startGradeName;
        private int scope;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(int createAccountId) {
            this.createAccountId = createAccountId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public int getBaseEducationId() {
            return baseEducationId;
        }

        public void setBaseEducationId(int baseEducationId) {
            this.baseEducationId = baseEducationId;
        }

        public String getBaseEducationName() {
            return baseEducationName;
        }

        public void setBaseEducationName(String baseEducationName) {
            this.baseEducationName = baseEducationName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseVersionId() {
            return baseVersionId;
        }

        public void setBaseVersionId(int baseVersionId) {
            this.baseVersionId = baseVersionId;
        }

        public String getBaseVersionName() {
            return baseVersionName;
        }

        public void setBaseVersionName(String baseVersionName) {
            this.baseVersionName = baseVersionName;
        }

        public Object getStartGradeId() {
            return startGradeId;
        }

        public void setStartGradeId(Object startGradeId) {
            this.startGradeId = startGradeId;
        }

        public Object getStartGradeName() {
            return startGradeName;
        }

        public void setStartGradeName(Object startGradeName) {
            this.startGradeName = startGradeName;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }
    }
}
