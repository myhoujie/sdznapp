package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/29
 * 修改单号：
 * 修改内容:
 */
public class QiniuUptoken1 {


    /**
     * code : 0
     * msg : success
     * timestamp : 1546841792273
     * result : {"bucket":"csfzx","upToken":"9Y2SKtnydf_RRrTLIpSoNDaXXgumHGvqvzcaHCbC:aqVeW0h8LDd9EIJmaWeMI7S0pNw=:eyJzY29wZSI6ImNzZnp4IiwiZGVhZGxpbmUiOjE1NDY5MjgxOTJ9","domain":"http://csfile.fuzhuxian.com","imageStyle":"imageView2/2/w/200/h/200/q/50"}
     */

    private int code;
    private String msg;
    private String timestamp;
    private ResultBean result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * bucket : csfzx
         * upToken : 9Y2SKtnydf_RRrTLIpSoNDaXXgumHGvqvzcaHCbC:aqVeW0h8LDd9EIJmaWeMI7S0pNw=:eyJzY29wZSI6ImNzZnp4IiwiZGVhZGxpbmUiOjE1NDY5MjgxOTJ9
         * domain : http://csfile.fuzhuxian.com
         * imageStyle : imageView2/2/w/200/h/200/q/50
         */

        private String bucket;
        private String upToken;
        private String domain;
        private String imageStyle;

        public String getBucket() {
            return bucket;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        public String getUpToken() {
            return upToken;
        }

        public void setUpToken(String upToken) {
            this.upToken = upToken;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getImageStyle() {
            return imageStyle;
        }

        public void setImageStyle(String imageStyle) {
            this.imageStyle = imageStyle;
        }
    }
}
