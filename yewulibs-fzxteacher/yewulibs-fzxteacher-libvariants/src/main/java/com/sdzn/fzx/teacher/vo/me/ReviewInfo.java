package com.sdzn.fzx.teacher.vo.me;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/6
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ReviewInfo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 16
         * createAccountId : 56
         * createUserName : 田斌
         * createTime : 1517909793000
         * userTeacherId : 17
         * userTeacherName : 田斌
         * baseGradeId : 7
         * baseGradeName : 七年级
         * reviewType : 1
         * name : null
         * reviewTypeStr : 上学期期中复习
         */

        private int id;
        private int createAccountId;
        private String createUserName;
        private long createTime;
        private int userTeacherId;
        private String userTeacherName;
        private int baseGradeId;
        private String baseGradeName;
        private int reviewType;
        private Object name;
        private String reviewTypeName;
        private boolean status;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getReviewTypeName() {
            return reviewTypeName;
        }

        public void setReviewTypeName(String reviewTypeName) {
            this.reviewTypeName = reviewTypeName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(int createAccountId) {
            this.createAccountId = createAccountId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getUserTeacherId() {
            return userTeacherId;
        }

        public void setUserTeacherId(int userTeacherId) {
            this.userTeacherId = userTeacherId;
        }

        public String getUserTeacherName() {
            return userTeacherName;
        }

        public void setUserTeacherName(String userTeacherName) {
            this.userTeacherName = userTeacherName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getReviewType() {
            return reviewType;
        }

        public void setReviewType(int reviewType) {
            this.reviewType = reviewType;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

    }
}
