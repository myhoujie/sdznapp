package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/23
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class UserBean {

    /**
     * data : {"id":7,"realName":"张超","accountId":31,"accountName":"07851634","available":1,"nickName":null,"sex":1,"photo":"http://192.168.0.215:8886/head/2018/01/a1db8912fd2d40d08cb83121d84fc9bb.jpg","createAccountId":3,"createUserId":null,"createUserName":"93746521","createTime":1515979145000,"updateTime":null,"userType":4,"telephone":"13260188138","accountRoleList":null,"email":null,"identityId":"210000199212024494","birthday":1516636800000,"zoneId":null,"zoneName":null,"zoneIdPath":null,"zoneNamePath":null,"baseLevelId":1,"baseLevelName":"小学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":3,"baseGradeName":"三年级","baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null,"scope":2017,"customerSchoolName":"山东大学附属中学","customerSchoolId":1}
     */

    private String id;
    private int scope;
    private String schoolName;
    private int schoolId;
    private String name;
    private String avatar;
    private String account;
    private String levelId;
    private String mobile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRealName() {
        return name;
    }

    public void setRealName(String realName) {
        this.name = realName;
    }

    public String getAccountId() {
        return account;
    }

    public void setAccountId(String accountId) {
        this.account = accountId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoto() {
        return avatar;
    }

    public void setPhoto(String photo) {
        this.avatar = photo;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    //无参数返回数据
    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

    public String getCustomerSchoolName() {
        return schoolName;
    }

    public void setCustomerSchoolName(String customerSchoolName) {
        this.schoolName = customerSchoolName;
    }

    public int getCustomerSchoolId() {
        return schoolId;
    }

    public void setCustomerSchoolId(int customerSchoolId) {
        this.schoolId = customerSchoolId;
    }
}
