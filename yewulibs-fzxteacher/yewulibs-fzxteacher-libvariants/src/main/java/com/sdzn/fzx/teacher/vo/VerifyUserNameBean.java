package com.sdzn.fzx.teacher.vo;

/**
 * VerifyUserNameBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyUserNameBean {


    /**
     * data : 18615205288
     */

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
