package com.sdzn.fzx.teacher.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.view.CheckverionViews;
import com.sdzn.fzx.teacher.view.ShouyeViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ShouyePresenter extends Presenter<ShouyeViews> {
    public void queryShouye(String token) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryShouye(token)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<ShouyeBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ShouyeBean>> call, Response<ResponseSlbBean1<ShouyeBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onIndexNodata(response.body().getMessage());
                            return;
                        }
                        getView().onIndexSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ShouyeBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onIndexFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
