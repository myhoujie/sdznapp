package com.sdzn.fzx.teacher.vo;

import com.sdzn.fzx.teacher.bean.SubjectData;

import java.util.ArrayList;
import java.util.List;

/**
 * LoginBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoginBean {


    /**
     * data : {"id":7,"name":"12345678","available":1,"loginTime":1517800259000,"modifyTime":1517800268000,"loginCount":260,"email":null,"telephone":null,"identityId":null,"description":null,"createTime":1515643048000,"createUserName":"超级管理员","accessToken":"b5127635fe144ee0b2a509e78225c86d","refreshToken":"4f1a8809cb1b42b8abea4f0706834a5f","expiresIn":7200,"user":{"id":2,"realName":"王老师","accountId":7,"accountName":"12345678","available":1,"nickName":null,"sex":2,"photo":null,"createAccountId":null,"createUserId":1,"createUserName":"small","createTime":1515643048000,"updateTime":null,"userType":3,"telephone":"15222222222","accountRoleList":null,"email":null,"identityId":null,"birthday":null,"zoneId":null,"zoneName":null,"zoneIdPath":null,"zoneNamePath":null,"baseLevelId":1,"baseLevelName":"小学","baseEducationId":1,"baseEducationName":"六三制","scope":2017,"customerSchoolName":"山东大学附属中学","customerSchoolId":1},"menuList":null,"isManager":1,"isVersion":0,"subjectList":[{"subjectId":3,"subjectName":"英语"},{"subjectId":1,"subjectName":"语文"}],"scope":2017}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 7
         * name : 12345678
         * available : 1
         * loginTime : 1517800259000
         * modifyTime : 1517800268000
         * loginCount : 260
         * email : null
         * telephone : null
         * identityId : null
         * description : null
         * createTime : 1515643048000
         * createUserName : 超级管理员
         * accessToken : b5127635fe144ee0b2a509e78225c86d
         * refreshToken : 4f1a8809cb1b42b8abea4f0706834a5f
         * expiresIn : 7200
         * user : {"id":2,"realName":"王老师","accountId":7,"accountName":"12345678","available":1,"nickName":null,"sex":2,"photo":null,"createAccountId":null,"createUserId":1,"createUserName":"small","createTime":1515643048000,"updateTime":null,"userType":3,"telephone":"15222222222","accountRoleList":null,"email":null,"identityId":null,"birthday":null,"zoneId":null,"zoneName":null,"zoneIdPath":null,"zoneNamePath":null,"baseLevelId":1,"baseLevelName":"小学","baseEducationId":1,"baseEducationName":"六三制","scope":2017,"customerSchoolName":"山东大学附属中学","customerSchoolId":1}
         * menuList : null
         * isManager : 1
         * isVersion : 0
         * subjectList : [{"subjectId":3,"subjectName":"英语"},{"subjectId":1,"subjectName":"语文"}]
         * scope : 2017
         */

        private String id;
        private String oldAccessToken;
        private String access_token;
        private String refresh_token;
        private UserBean userDetail;
        private int isManager;
//        private List<SubjectListBean> subjectList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getOldAccessToken() {
            return oldAccessToken;
        }

        public void setOldAccessToken(String oldAccessToken) {
            this.oldAccessToken = oldAccessToken;
        }

        public String getAccessToken() {
            return access_token;
        }

        public void setAccessToken(String accessToken) {
            this.access_token = accessToken;
        }

        public String getRefreshToken() {
            return refresh_token;
        }

        public void setRefreshToken(String refreshToken) {
            this.refresh_token = refreshToken;
        }


        public UserBean getUser() {
            return userDetail;
        }

        public void setUser(UserBean userDetail) {
            this.userDetail = userDetail;
        }


        public int getIsManager() {
            return isManager;
        }

        public void setIsManager(int isManager) {
            this.isManager = isManager;
        }


        public List<SubjectListBean> getSubjectList() {
            return new ArrayList<>();
//            return subjectList;
        }

//        public void setSubjectList(List<SubjectListBean> subjectList) {
//            this.subjectList = subjectList;
//        }

        public static class UserBean {
            /**
             * id : 2
             * realName : 王老师
             * accountId : 7
             * accountName : 12345678
             * available : 1
             * nickName : null
             * sex : 2
             * photo : null
             * createAccountId : null
             * createUserId : 1
             * createUserName : small
             * createTime : 1515643048000
             * updateTime : null
             * userType : 3
             * telephone : 15222222222
             * accountRoleList : null
             * email : null
             * identityId : null
             * birthday : null
             * zoneId : null
             * zoneName : null
             * zoneIdPath : null
             * zoneNamePath : null
             * baseLevelId : 1
             * baseLevelName : 小学
             * baseEducationId : 1
             * baseEducationName : 六三制
             * scope : 2017
             * customerSchoolName : 山东大学附属中学
             * customerSchoolId : 1
             */

            private String id;
            private int baseLevelId;
            private String baseLevelName;
            private int baseEducationId;
            private String baseEducationName;
            private int scope;
            private String schoolName;
            private String schoolId;
            private String name;
            private String avatar;
            private String account;
            private String levelId;
            private List<SubjectData> subjectList;
            private String subjectType;
            private String createTime;
            private String updateTime;
            private int sort;
            private int gender;
            private int status;
            private int isDelete;
            private String deleteTime;
            private String education;
            private String career;
            private String introduction;
            private String certificateNo;
            private String certificateImage;
            private int isShow;
            private int isSchoolmaster;
            private String mobile;
            private String email;
            private String qq;
            private String weixin;
            private String freeznTime;
            private String classManager;
            private String password;
            private String levelName;
            private int isFamous;
            private int liveSubjectId;
            private String liveSubjectName;
            private int isTypicalMistakes;
            private int isGoodAnswer;
            private int role;
            private List<?> schoolTeacherClasses;

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            public String getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(String schoolId) {
                this.schoolId = schoolId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getIsDelete() {
                return isDelete;
            }

            public void setIsDelete(int isDelete) {
                this.isDelete = isDelete;
            }

            public String getDeleteTime() {
                return deleteTime;
            }

            public void setDeleteTime(String deleteTime) {
                this.deleteTime = deleteTime;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getCareer() {
                return career;
            }

            public void setCareer(String career) {
                this.career = career;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public String getCertificateNo() {
                return certificateNo;
            }

            public void setCertificateNo(String certificateNo) {
                this.certificateNo = certificateNo;
            }

            public String getCertificateImage() {
                return certificateImage;
            }

            public void setCertificateImage(String certificateImage) {
                this.certificateImage = certificateImage;
            }

            public int getIsShow() {
                return isShow;
            }

            public void setIsShow(int isShow) {
                this.isShow = isShow;
            }

            public int getIsSchoolmaster() {
                return isSchoolmaster;
            }

            public void setIsSchoolmaster(int isSchoolmaster) {
                this.isSchoolmaster = isSchoolmaster;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getQq() {
                return qq;
            }

            public void setQq(String qq) {
                this.qq = qq;
            }

            public String getWeixin() {
                return weixin;
            }

            public void setWeixin(String weixin) {
                this.weixin = weixin;
            }

            public String getFreeznTime() {
                return freeznTime;
            }

            public void setFreeznTime(String freeznTime) {
                this.freeznTime = freeznTime;
            }

            public String getClassManager() {
                return classManager;
            }

            public void setClassManager(String classManager) {
                this.classManager = classManager;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getLevelName() {
                return levelName;
            }

            public void setLevelName(String levelName) {
                this.levelName = levelName;
            }

            public int getIsFamous() {
                return isFamous;
            }

            public void setIsFamous(int isFamous) {
                this.isFamous = isFamous;
            }

            public int getLiveSubjectId() {
                return liveSubjectId;
            }

            public void setLiveSubjectId(int liveSubjectId) {
                this.liveSubjectId = liveSubjectId;
            }

            public String getLiveSubjectName() {
                return liveSubjectName;
            }

            public void setLiveSubjectName(String liveSubjectName) {
                this.liveSubjectName = liveSubjectName;
            }

            public int getIsTypicalMistakes() {
                return isTypicalMistakes;
            }

            public void setIsTypicalMistakes(int isTypicalMistakes) {
                this.isTypicalMistakes = isTypicalMistakes;
            }

            public int getIsGoodAnswer() {
                return isGoodAnswer;
            }

            public void setIsGoodAnswer(int isGoodAnswer) {
                this.isGoodAnswer = isGoodAnswer;
            }

            public int getRole() {
                return role;
            }

            public void setRole(int role) {
                this.role = role;
            }

            public List<?> getSchoolTeacherClasses() {
                return schoolTeacherClasses;
            }

            public void setSchoolTeacherClasses(List<?> schoolTeacherClasses) {
                this.schoolTeacherClasses = schoolTeacherClasses;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRealName() {
                return name;
            }

            public void setRealName(String realName) {
                this.name = realName;
            }

            public String getAccountId() {
                return account;
            }

            public void setAccountId(String accountId) {
                this.account = accountId;
            }

            public String getPhoto() {
                return avatar;
            }

            public void setPhoto(String photo) {
                this.avatar = photo;
            }

            public String getLevelId() {
                return levelId;
            }

            public void setLevelId(String levelId) {
                this.levelId = levelId;
            }

            //无参数返回数据
            public int getBaseLevelId() {
                return baseLevelId;
            }

            public void setBaseLevelId(int baseLevelId) {
                this.baseLevelId = baseLevelId;
            }

            //无参数返回数据
            public String getBaseLevelName() {
                return baseLevelName;
            }

            public void setBaseLevelName(String baseLevelName) {
                this.baseLevelName = baseLevelName;
            }

            //无参数返回数据
            public int getBaseEducationId() {
                return baseEducationId;
            }

            public void setBaseEducationId(int baseEducationId) {
                this.baseEducationId = baseEducationId;
            }

            //无参数返回数据
            public String getBaseEducationName() {
                return baseEducationName;
            }

            public void setBaseEducationName(String baseEducationName) {
                this.baseEducationName = baseEducationName;
            }

            //无参数返回数据
            public int getScope() {
                return scope;
            }

            public void setScope(int scope) {
                this.scope = scope;
            }

            public String getCustomerSchoolName() {
                return schoolName;
            }

            public void setCustomerSchoolName(String customerSchoolName) {
                this.schoolName = customerSchoolName;
            }

            public String getCustomerSchoolId() {
                return schoolId;
            }

            public void setCustomerSchoolId(String customerSchoolId) {
                this.schoolId = customerSchoolId;
            }

            public List<SubjectData> getSubjectList() {
                return subjectList;
            }

            public void setSubjectList(List<SubjectData> subjectList) {
                this.subjectList = subjectList;
            }

            public String getSubjectType() {
                return subjectType;
            }

            public void setSubjectType(String subjectType) {
                this.subjectType = subjectType;
            }
        }

        public static class SubjectListBean {
            /**
             * subjectId : 3
             * subjectName : 英语
             */

            private int subjectId;
            private String subjectName;

            public int getSubjectId() {
                return subjectId;
            }

            public void setSubjectId(int subjectId) {
                this.subjectId = subjectId;
            }

            public String getSubjectName() {
                return subjectName;
            }

            public void setSubjectName(String subjectName) {
                this.subjectName = subjectName;
            }
        }

    }
}
