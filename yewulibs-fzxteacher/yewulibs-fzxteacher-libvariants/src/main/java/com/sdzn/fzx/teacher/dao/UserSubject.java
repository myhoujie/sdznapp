package com.sdzn.fzx.teacher.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
@Entity
public class UserSubject {
    @Id
    private Long id;
    private int userId;
    private int subjectId;
    @Generated(hash = 1336294696)
    public UserSubject(Long id, int userId, int subjectId) {
        this.id = id;
        this.userId = userId;
        this.subjectId = subjectId;
    }
    @Generated(hash = 913189261)
    public UserSubject() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getUserId() {
        return this.userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public int getSubjectId() {
        return this.subjectId;
    }
    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }
}
