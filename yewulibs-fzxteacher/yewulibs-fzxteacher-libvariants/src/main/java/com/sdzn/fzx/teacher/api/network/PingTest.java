package com.sdzn.fzx.teacher.api.network;



import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SaveLogUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * PingTest〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class PingTest {

    public static final int PACKAGE_COUNT = Integer.MAX_VALUE;
    public static final int PACKAGE_SIZE = 64;
    public static final int PACKAGE_TIME = 1;
    public static final String PACKAGE_IP = "192.168.0.209";

    private static PingTest mInstance;

    private PingTest() {

    }

    public static PingTest getInstance() {
        if (mInstance == null) {
            mInstance = new PingTest();
        }
        return mInstance;
    }

    public void startPing() {
        if (!Network.IS_TEST)
            return;

        String countCmd = " -c " + PACKAGE_COUNT + " ";
        String sizeCmd = " -s " + PACKAGE_SIZE + " ";
        String timeCmd = " -i " + PACKAGE_TIME + " ";

        final String ping = "ping" + countCmd + timeCmd + sizeCmd + PACKAGE_IP;

        new Thread(new Runnable() {
            @Override
            public void run() {
                ping(ping);
            }
        }).start();

    }

    private void ping(String ping) {

        Process process = null;
        BufferedReader successReader = null;
        BufferedReader errorReader = null;

        DataOutputStream dos = null;
        try {
            process = Runtime.getRuntime().exec(ping);
            // dos = new DataOutputStream(process.getOutputStream());
            Log.i("====receive====:");

            InputStream in = process.getInputStream();

            OutputStream out = process.getOutputStream();

            successReader = new BufferedReader(
                    new InputStreamReader(in));

            // error
            errorReader = new BufferedReader(new InputStreamReader(
                    process.getErrorStream()));

            String lineStr;

            while ((lineStr = successReader.readLine()) != null) {

                Log.i("====receive====:" + lineStr);
                SaveLogUtils.saveInfo("====receive====:" + lineStr);
//                result = result + lineStr + "\n";
                if (lineStr.contains("packet loss")) {
                    Log.i("=====Message=====" + lineStr.toString());
                    int i = lineStr.indexOf("received");
                    int j = lineStr.indexOf("%");
                    SaveLogUtils.saveInfo("====丢包率====:"
                            + lineStr.substring(i + 10, j + 1));
                    Log.i("====丢包率====:"
                            + lineStr.substring(i + 10, j + 1));//
                }
                if (lineStr.contains("avg")) {
                    int i = lineStr.indexOf("/", 20);
                    int j = lineStr.indexOf(".", i);
                    SaveLogUtils.saveInfo("====平均时延:===="
                            + lineStr.substring(i + 1, j));
                    Log.i("====平均时延:===="
                            + lineStr.substring(i + 1, j));
                }
                Thread.sleep(PACKAGE_TIME * 1000);
            }

            while ((lineStr = errorReader.readLine()) != null) {
                Log.i("==error======" + lineStr);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (dos != null) {
                    dos.close();
                }
                if (successReader != null) {
                    successReader.close();
                }
                if (errorReader != null) {
                    errorReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (process != null) {
                process.destroy();
            }
        }
        //重新执行
        ping(ping);
    }
}
