package com.sdzn.fzx.teacher.vo;

/*评语实体*/
public class SloganBane {
    private String name;

    public SloganBane(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
