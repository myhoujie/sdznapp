package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class RgDetailsVo {
    /**
     * data : {"studentList":[{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生68","userStudentId":5172},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生66","userStudentId":5170},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生65","userStudentId":5169},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生63","userStudentId":5167},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生62","userStudentId":5166},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生61","userStudentId":5165}],"useTimeAvg":0,"countStudentTotal":6,"countStudentSubmit":0,"countStudentRemain":6}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * studentList : [{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生68","userStudentId":5172},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生66","userStudentId":5170},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生65","userStudentId":5169},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生63","userStudentId":5167},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生62","userStudentId":5166},{"isRead":0,"photo":null,"useTime":0,"userStudentName":"学生61","userStudentId":5165}]
         * useTimeAvg : 0
         * countStudentTotal : 6
         * countStudentSubmit : 0
         * countStudentRemain : 6
         */

        private float useTimeAvg;
        private int countStudentTotal;
        private int countStudentSubmit;
        private int countStudentRemain;
        private List<StudentListBean> studentList;

        public float getUseTimeAvg() {
            return useTimeAvg;
        }

        public void setUseTimeAvg(float useTimeAvg) {
            this.useTimeAvg = useTimeAvg;
        }

        public int getCountStudentTotal() {
            return countStudentTotal;
        }

        public void setCountStudentTotal(int countStudentTotal) {
            this.countStudentTotal = countStudentTotal;
        }

        public int getCountStudentSubmit() {
            return countStudentSubmit;
        }

        public void setCountStudentSubmit(int countStudentSubmit) {
            this.countStudentSubmit = countStudentSubmit;
        }

        public int getCountStudentRemain() {
            return countStudentRemain;
        }

        public void setCountStudentRemain(int countStudentRemain) {
            this.countStudentRemain = countStudentRemain;
        }

        public List<StudentListBean> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<StudentListBean> studentList) {
            this.studentList = studentList;
        }

        public static class StudentListBean {
            /**
             * isRead : 0
             * photo : null
             * useTime : 0
             * userStudentName : 学生68
             * userStudentId : 5172
             */

            private int isRead;
            private Object photo;
            private int useTime;
            private String userStudentName;
            private int userStudentId;

            public int getIsRead() {
                return isRead;
            }

            public void setIsRead(int isRead) {
                this.isRead = isRead;
            }

            public Object getPhoto() {
                return photo;
            }

            public void setPhoto(Object photo) {
                this.photo = photo;
            }

            public int getUseTime() {
                return useTime;
            }

            public void setUseTime(int useTime) {
                this.useTime = useTime;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }
        }
    }
}
