package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ShouyeBean;

/**
 首页 view
 */
public interface ShouyeViews extends IView {
    void onIndexSuccess(ShouyeBean shouyeBean);

    void onIndexNodata(String msg);

    void onIndexFail(String msg);
}
