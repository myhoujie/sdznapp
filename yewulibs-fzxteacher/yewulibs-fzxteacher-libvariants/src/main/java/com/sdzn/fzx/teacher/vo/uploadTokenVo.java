package com.sdzn.fzx.teacher.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/6
 * 修改单号：
 * 修改内容:
 */
public class uploadTokenVo  {


    /**
     * code : 0
     * msg : success
     * timestamp : 1551835405571
     * result : {"data":{"domian":"http://csfile.fuzhuxian.com","token":"9Y2SKtnydf_RRrTLIpSoNDaXXgumHGvqvzcaHCbC:-XLdaoYg1SXKAFu03ta-tzFOYRw=:eyJwZXJzaXN0ZW50UGlwZWxpbmUiOiJmenhfY2VzaGkiLCJzY29wZSI6ImNzZnp4OjEyMTIxLm1wNCIsInBlcnNpc3RlbnROb3RpZnlVcmwiOiJodHRwOi8vMTM5LjEyOS4xMDQuNTEvdGVhY2gvUWluaXVDb250cm9sbGVyL2NhbGxiYWNrUWluaXUiLCJwZXJzaXN0ZW50T3BzIjoiYXZ0aHVtYi9tcDQvYWIvMTI4ay9hci80NDEwMC9hY29kZWMvbGliZmFhYy9yLzMwL3ZiLzEyMDBrL3Zjb2RlYy9saWJ4MjY0L3MvODU0eDQ4MC9hdXRvc2NhbGUvMS9zdHJpcG1ldGEvMHxzYXZlYXMvWTNObWVuZzZOV1ExWTJVMk5HUXRaRGN4TmkwMFpXTTRMV0ZqWmpndE1tVTBNR1k0T0RFNE5qQmhMekV5TXpRMU5pNXRjRFFcdTAwM2QiLCJkZWFkbGluZSI6MTU1MTgzOTAwNX0="}}
     */

    private int code;
    private String msg;
    private String timestamp;
    private ResultBean result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * data : {"domian":"http://csfile.fuzhuxian.com","token":"9Y2SKtnydf_RRrTLIpSoNDaXXgumHGvqvzcaHCbC:-XLdaoYg1SXKAFu03ta-tzFOYRw=:eyJwZXJzaXN0ZW50UGlwZWxpbmUiOiJmenhfY2VzaGkiLCJzY29wZSI6ImNzZnp4OjEyMTIxLm1wNCIsInBlcnNpc3RlbnROb3RpZnlVcmwiOiJodHRwOi8vMTM5LjEyOS4xMDQuNTEvdGVhY2gvUWluaXVDb250cm9sbGVyL2NhbGxiYWNrUWluaXUiLCJwZXJzaXN0ZW50T3BzIjoiYXZ0aHVtYi9tcDQvYWIvMTI4ay9hci80NDEwMC9hY29kZWMvbGliZmFhYy9yLzMwL3ZiLzEyMDBrL3Zjb2RlYy9saWJ4MjY0L3MvODU0eDQ4MC9hdXRvc2NhbGUvMS9zdHJpcG1ldGEvMHxzYXZlYXMvWTNObWVuZzZOV1ExWTJVMk5HUXRaRGN4TmkwMFpXTTRMV0ZqWmpndE1tVTBNR1k0T0RFNE5qQmhMekV5TXpRMU5pNXRjRFFcdTAwM2QiLCJkZWFkbGluZSI6MTU1MTgzOTAwNX0="}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * domian : http://csfile.fuzhuxian.com
             * token : 9Y2SKtnydf_RRrTLIpSoNDaXXgumHGvqvzcaHCbC:-XLdaoYg1SXKAFu03ta-tzFOYRw=:eyJwZXJzaXN0ZW50UGlwZWxpbmUiOiJmenhfY2VzaGkiLCJzY29wZSI6ImNzZnp4OjEyMTIxLm1wNCIsInBlcnNpc3RlbnROb3RpZnlVcmwiOiJodHRwOi8vMTM5LjEyOS4xMDQuNTEvdGVhY2gvUWluaXVDb250cm9sbGVyL2NhbGxiYWNrUWluaXUiLCJwZXJzaXN0ZW50T3BzIjoiYXZ0aHVtYi9tcDQvYWIvMTI4ay9hci80NDEwMC9hY29kZWMvbGliZmFhYy9yLzMwL3ZiLzEyMDBrL3Zjb2RlYy9saWJ4MjY0L3MvODU0eDQ4MC9hdXRvc2NhbGUvMS9zdHJpcG1ldGEvMHxzYXZlYXMvWTNObWVuZzZOV1ExWTJVMk5HUXRaRGN4TmkwMFpXTTRMV0ZqWmpndE1tVTBNR1k0T0RFNE5qQmhMekV5TXpRMU5pNXRjRFFcdTAwM2QiLCJkZWFkbGluZSI6MTU1MTgzOTAwNX0=
             */

            private String domian;
            private String token;

            public String getDomian() {
                return domian;
            }

            public void setDomian(String domian) {
                this.domian = domian;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }
        }
    }
}
