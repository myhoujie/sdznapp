package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/15
 * 修改单号：
 * 修改内容:
 */
public class ImgList {
    /**
     * total : 12
     * data : [{"id":185693,"lessonAnswerExamId":505408,"oldImgPath":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg","imgPath":"http://csfile.fuzhuxian.com/b9bda6e2-465c-9dfa-0e86-f3c64fd1cc4c.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/b9bda6e2-465c-9dfa-0e86-f3c64fd1cc4c.jpg?imageView2/5/w/200/h/200/q/50","seq":0,"examSeq":1,"templateName":"默写"},{"id":185693,"lessonAnswerExamId":505408,"oldImgPath":"http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg","imgPath":"http://csfile.fuzhuxian.com/385f73a8-0973-1ca1-86f0-d8e9d4969e97.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/b2b9432c-365f-4018-bf89-9b5a5318a30e.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/385f73a8-0973-1ca1-86f0-d8e9d4969e97.jpg?imageView2/5/w/200/h/200/q/50","seq":1,"examSeq":1,"templateName":"默写"},{"id":185693,"lessonAnswerExamId":505408,"oldImgPath":"http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg","imgPath":"http://csfile.fuzhuxian.com/1a915f04-3746-95a9-b1eb-b9b65eede912.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/3721e4b4-84da-48e0-8221-d64ee287e5b6.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/1a915f04-3746-95a9-b1eb-b9b65eede912.jpg?imageView2/5/w/200/h/200/q/50","seq":2,"examSeq":1,"templateName":"默写"},{"id":185693,"lessonAnswerExamId":505408,"oldImgPath":"http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg","imgPath":"http://csfile.fuzhuxian.com/c2dc6467-3537-b2b7-f9b7-a232726f1773.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/168bed4b-b7ed-4f84-ae33-f61a65606419.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/c2dc6467-3537-b2b7-f9b7-a232726f1773.jpg?imageView2/5/w/200/h/200/q/50","seq":3,"examSeq":1,"templateName":"默写"},{"id":185694,"lessonAnswerExamId":505409,"oldImgPath":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg","imgPath":"http://csfile.fuzhuxian.com/a0071dd8-ab3d-d9db-4a11-89314a7812df.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/9f62bce2-c7e2-4d90-aaf5-1c764b07dced.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/a0071dd8-ab3d-d9db-4a11-89314a7812df.jpg?imageView2/5/w/200/h/200/q/50","seq":0,"examSeq":2,"templateName":"默写"},{"id":185694,"lessonAnswerExamId":505409,"oldImgPath":"http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg","imgPath":"http://csfile.fuzhuxian.com/c5935872-5840-5873-6e0f-b8ef7695ae0f.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/8712f346-84b8-40ec-804c-bb020f0cfdd1.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/c5935872-5840-5873-6e0f-b8ef7695ae0f.jpg?imageView2/5/w/200/h/200/q/50","seq":1,"examSeq":2,"templateName":"默写"},{"id":185694,"lessonAnswerExamId":505409,"oldImgPath":"http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg","imgPath":"http://csfile.fuzhuxian.com/e716d5e4-8d4b-0716-28ca-baa56ae96472.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/8fabd993-e4ff-4ffd-81be-67432b8a7c9c.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/e716d5e4-8d4b-0716-28ca-baa56ae96472.jpg?imageView2/5/w/200/h/200/q/50","seq":2,"examSeq":2,"templateName":"默写"},{"id":185694,"lessonAnswerExamId":505409,"oldImgPath":"http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg","imgPath":"http://csfile.fuzhuxian.com/42070de6-2157-65ac-9e38-cf8a356a8351.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/e5e64d69-d636-4bf4-b3da-d35bb539c785.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/42070de6-2157-65ac-9e38-cf8a356a8351.jpg?imageView2/5/w/200/h/200/q/50","seq":3,"examSeq":2,"templateName":"默写"},{"id":185695,"lessonAnswerExamId":505410,"oldImgPath":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg","imgPath":"http://csfile.fuzhuxian.com/8989c100-a5d7-8a0e-303e-fdf895004a22.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/b3841a44-1bcf-4da3-b016-44f5ae185da4.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/8989c100-a5d7-8a0e-303e-fdf895004a22.jpg?imageView2/5/w/200/h/200/q/50","seq":0,"examSeq":3,"templateName":"简答题"},{"id":185695,"lessonAnswerExamId":505410,"oldImgPath":"http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg","imgPath":"http://csfile.fuzhuxian.com/8239ce31-5819-16b5-b746-6160be56801e.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/cc3a4728-c688-40f9-b986-e9130ec2bc92.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/8239ce31-5819-16b5-b746-6160be56801e.jpg?imageView2/5/w/200/h/200/q/50","seq":1,"examSeq":3,"templateName":"简答题"},{"id":185695,"lessonAnswerExamId":505410,"oldImgPath":"http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg","imgPath":"http://csfile.fuzhuxian.com/2a528208-4d25-6155-d2b5-cd96de21a759.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/d8a1993f-cce2-43ad-9be1-9fe1986ddc43.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/2a528208-4d25-6155-d2b5-cd96de21a759.jpg?imageView2/5/w/200/h/200/q/50","seq":2,"examSeq":3,"templateName":"简答题"},{"id":185695,"lessonAnswerExamId":505410,"oldImgPath":"http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg","imgPath":"http://csfile.fuzhuxian.com/1ebcf79e-10a5-abf4-4ccb-de650b23720c.jpg","oldImgPathThn":"http://csfile.fuzhuxian.com/f9da242c-2a6d-485b-a489-9b3a8157e858.jpg?imageView2/5/w/200/h/200/q/50","imgPathThn":"http://csfile.fuzhuxian.com/1ebcf79e-10a5-abf4-4ccb-de650b23720c.jpg?imageView2/5/w/200/h/200/q/50","seq":3,"examSeq":3,"templateName":"简答题"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 185693
         * lessonAnswerExamId : 505408
         * oldImgPath : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg
         * imgPath : http://csfile.fuzhuxian.com/b9bda6e2-465c-9dfa-0e86-f3c64fd1cc4c.jpg
         * oldImgPathThn : http://csfile.fuzhuxian.com/e3aa5c31-465b-4c39-b9df-b05be007cf10.jpg?imageView2/5/w/200/h/200/q/50
         * imgPathThn : http://csfile.fuzhuxian.com/b9bda6e2-465c-9dfa-0e86-f3c64fd1cc4c.jpg?imageView2/5/w/200/h/200/q/50
         * seq : 0
         * examSeq : 1
         * templateName : 默写
         */

        private int id;
        private int lessonAnswerExamId;
        private String oldImgPath;
        private String imgPath;
        private String oldImgPathThn;
        private String imgPathThn;
        private int seq;
        private int examSeq;
        private String templateName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLessonAnswerExamId() {
            return lessonAnswerExamId;
        }

        public void setLessonAnswerExamId(int lessonAnswerExamId) {
            this.lessonAnswerExamId = lessonAnswerExamId;
        }

        public String getOldImgPath() {
            return oldImgPath;
        }

        public void setOldImgPath(String oldImgPath) {
            this.oldImgPath = oldImgPath;
        }

        public String getImgPath() {
            return imgPath;
        }

        public void setImgPath(String imgPath) {
            this.imgPath = imgPath;
        }

        public String getOldImgPathThn() {
            return oldImgPathThn;
        }

        public void setOldImgPathThn(String oldImgPathThn) {
            this.oldImgPathThn = oldImgPathThn;
        }

        public String getImgPathThn() {
            return imgPathThn;
        }

        public void setImgPathThn(String imgPathThn) {
            this.imgPathThn = imgPathThn;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public String getTemplateName() {
            return templateName;
        }

        public void setTemplateName(String templateName) {
            this.templateName = templateName;
        }
    }
}
