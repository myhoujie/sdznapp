package com.sdzn.fzx.teacher.api;

import com.haier.cellarette.libretrofit.common.ResponseSlbBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.bean.HCategoryBean;
import com.sdzn.fzx.teacher.bean.RecyclerTabBean;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.bean.SubjectData;
import com.sdzn.fzx.teacher.bean.SubjectData1;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.QiNiuUrlVo;
import com.sdzn.fzx.teacher.vo.TakeOfficeVo;
import com.sdzn.fzx.teacher.vo.UserBean;
import com.sdzn.fzx.teacher.vo.VerifyUserMobile;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {
    //    String SERVER_ISERVICE = "https://m.hexiangjiaoyu.com/bcapi/";
    String SERVER_ISERVICE1 = "http://49.4.7.45:8080/#/";
    //    String SERVER_ISERVICE1 = "http://192.168.7.60:8083/#/";

    // 所有分类的列表
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("liveApp/enum/liveEnumLists")
    Call<ResponseSlbBean<HCategoryBean>> get_category(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_category2(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_update(@Body RequestBody body);

    // 检查更新
    @FormUrlEncoded
    @POST("http://doc.znclass.com" + ApiInterface.QUERY_VERSION_INFO)
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Field("programId") String programId, @Field("type") String type);//@Body RequestBody body@Header("Authorization") String token,

    // 首页
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_SHOUYE_INFO)
    Call<ResponseSlbBean1<ShouyeBean>> queryShouye(@Header("Authorization") String token);//@Body RequestBody body@Header("Authorization") String token, "Authorization", "Bearer "+

    // 登录
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.LOGIN_INFO)
    Call<ResponseSlbBean1<LoginBean.DataBean>> login(@Field("username") String username, @Field("password") String password);//@Body RequestBody body@Header("Authorization") String token,


    // 侧滑pop
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_CEHUA_INFO)
    Call<ResponseSlbBean1<GrzxRecActBean>> queryCehua();//@Header("Authorization") String token

    // 动态获取tab数据
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_TAB_INFO)
    Call<ResponseSlbBean1<RecyclerTabBean>> queryTab(@Body RequestBody body);//@Header("Authorization") String token,


    // 个人中心获取资料
    @GET("http://49.4.1.81:7799" + ApiInterface.TEACHER_INFO)
    Call<ResponseSlbBean1<UserBean>> teacherInfo(@Header("Authorization") String token, @Query("userId") String userId);

    // 更新头像
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.TEACHER_INFO_UPDATE)
    Call<ResponseSlbBean1<Object>> updateAvatar(@Header("Authorization") String token, @Field("teacherId") String teacherId, @Field("avatar") String avatar);

    // 发送验证码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.SEND_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> sendYZM(@Field("telephone") String telephone);

    // 验证验证码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.CHECK_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> checkYZM(@Field("mobile") String mobile, @Field("code") String code);

    // 教师更新手机号
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.TEACHER_MOBILE_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePhone(@Header("Authorization") String token, @Field("newTel") String mobile, @Field("code") String code);

    // 教师修改密码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.TEACHER_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePassword(@Header("Authorization") String token, @Field("id") String id,
                                                  @Field("oldPassword") String oldPassword, @Field("password") String password);

    // 七牛key获取地址
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("http://49.4.1.81:7799" + ApiInterface.QINIU_URL)
    Call<ResponseSlbBean1<QiNiuUrlVo>> getQiNiuUrl(@Header("Authorization") String token, @Body RequestBody body);  // 七牛key获取地址

    @GET("http://49.4.1.81:7799" + ApiInterface.TEACHER_CLASSES)
    Call<ResponseSlbBean1<TakeOfficeVo>> getTeacherClasses(@Header("Authorization") String token, @Query("teacherId") String teacherId, @Query("schoolId") String schoolId);

    // 忘记密码--获取手机号
    @Headers({"Content-Type: application/json", "Accept: application/json" })
    @POST("http://49.4.1.81:7799" + ApiInterface.TEACHER_LOGIN_MOBILE)
    Call<ResponseSlbBean1<VerifyUserMobile>> teacherFindMobile(@Body RequestBody body);

    // 忘记密码--更新密码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.TEACHER_LOGIN_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updateForgetPas(@Field("mobile") String mobile, @Field("password") String password, @Field("code") String code);

    //根据老师获取老师教学科目
    @GET("http://49.4.1.81:7799" + ApiInterface.TEACHER_SUBJECTDATA)
    Call<ResponseSlbBean1<SubjectData1>> getSubjectData(@Query("teacherId") String teacherId);
}
