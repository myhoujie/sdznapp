package com.sdzn.fzx.teacher.vo;

public class JsTopBean {
    public LoginBean.DataBean.UserBean content;
    public String dataType;
    public String dataTime;

    public LoginBean.DataBean.UserBean getContent() {
        return content;
    }

    public void setContent(LoginBean.DataBean.UserBean content) {
        this.content = content;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }
}
