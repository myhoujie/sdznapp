package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/20
 * 修改单号：
 * 修改内容:
 */
public class StudentTaskExamListVo {

    private  List<DataBean> data;

    public  List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5531
         * examId : null
         * examTemplateId : 4
         * examTemplateStyleId : 363
         * examTemplateStyleName : 现代文阅读
         * examEmptyCount : 0
         * seq : 2
         * examSeq : 1
         * lessonTaskId : 1102
         */

        private int id;
        private Object examId;
        private int examTemplateId;
        private int examTemplateStyleId;
        private String examTemplateStyleName;
        private int examEmptyCount;
        private int seq;
        private int examSeq;
        private int lessonTaskId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getExamId() {
            return examId;
        }

        public void setExamId(Object examId) {
            this.examId = examId;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(int examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }
    }
}
