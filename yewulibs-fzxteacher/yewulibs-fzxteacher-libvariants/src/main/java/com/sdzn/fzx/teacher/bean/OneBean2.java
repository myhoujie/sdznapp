package com.sdzn.fzx.teacher.bean;

import java.io.Serializable;
import java.util.List;

public class OneBean2 implements Serializable {
    private String tab_id;
    private String tab_name;
    private int tab_icon;
    private boolean enable;
    /**
     * desc : 自主学习统计
     * list : [{"desc":"作答统计","url":"/courseTeach/workbench/studyStatistics"},{"desc":"题目分析","url":"/courseTeach/workbench/topicAnalysis"},{"desc":"资料分析","url":"/courseTeach/workbench/materialAnalysis"}]
     * url : /courseTeach/workbench/typicalCase
     */

    private String desc;
    private String url;
    private List<ListBean> list;

    public OneBean2() {
    }

    public OneBean2(String tab_id, String tab_name, boolean enable, int tab_icon) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.tab_icon = tab_icon;
    }

    public OneBean2(String tab_id, String tab_name, boolean enable) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
    }

    public OneBean2(String tab_id, String tab_name, boolean enable, String url,List<ListBean> list) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.url = url;
        this.list=list;
    }


    public OneBean2(List<ListBean> list) {
        this.list=list;
    }

    public int getTab_icon() {
        return tab_icon;
    }

    public void setTab_icon(int tab_icon) {
        this.tab_icon = tab_icon;
    }

    public String getTab_id() {
        return tab_id;
    }

    public void setTab_id(String tab_id) {
        this.tab_id = tab_id;
    }

    public String getTab_name() {
        return tab_name;
    }

    public void setTab_name(String tab_name) {
        this.tab_name = tab_name;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * desc : 作答统计
         * url : /courseTeach/workbench/studyStatistics
         */

        private String desc;
        private String url;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

}
