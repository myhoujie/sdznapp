package com.sdzn.fzx.teacher.api;

/**
 * 描述：API请求地址
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ApiInterface {
    public static final String QUERY_VERSION_INFO = "/usercenter/sysVersionAppInfo/selectOne";//查询版本信息
    public static final String QUERY_SHOUYE_INFO = "/course/home/data";//查询首页信息
    public static final String QUERY_CEHUA_INFO = "/course/home/broadside/list";//侧边栏数据
    public static final String QUERY_TAB_INFO = "/course/home/tabJump/list";//Tab标题
    public static final String LOGIN_INFO = "/teacher/login/token";//老师端登录
    public static final String TEACHER_INFO = "/usercenter/teacher/teacherInfo";//个人中心获取资料
    public static final String TEACHER_INFO_UPDATE = "/usercenter/teacher/update/teacherInfo";//更新老师信息-头像
    public static final String TEACHER_MOBILE_UPDATE = "/usercenter/teacher/update/mobile";//更新老师信息-手机号
    public static final String TEACHER_PASSWORD_UPDATE = "/usercenter/teacher/update/password";//更新老师信息-密码
    public static final String SEND_VERIFYCODE = "/student/login/sendVerifyCode";//发送短信验证码
    public static final String CHECK_VERIFYCODE = "/teacher/check/smsToken";//验证验证码
    public static final String QINIU_TOKEN = "/knowledgecenter/resource/getUploadToken";//获取七牛token
    public static final String QINIU_URL = "/knowledgecenter/resource/getUrlByFileName";//七牛获取token
    public static final String TEACHER_CLASSES = "/usercenter/teacher/teach/classes";//任教信息
    public static final String TEACHER_LOGIN_MOBILE = "/usercenter/teacher/find/teacherMobile";//忘记密码--通过账号/手机号 获取手机号
    public static final String TEACHER_LOGIN_PASSWORD_UPDATE = "/usercenter/teacher/teacher/forget/password";//忘记密码--更新密码
    public static final String TEACHER_SUBJECTDATA = "/usercenter/teacherBookVersion/list";//忘记密码--更新密码


}
