package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by wangc on 2018/3/8 0008.
 */

public class ResourceVo {

    /**
     * id : 1861
     * examId : null
     * examTemplateId : null
     * examTemplateStyleId : null
     * examTemplateStyleName : null
     * examEmptyCount : null
     * examDifficulty : null
     * resourceId : 70
     * resourceType : 1
     * resourceName : 新建 Microsoft Word 文档.docx
     * type : 2
     * lessonLibId : 168
     * parentId : 0
     * seq : 1
     * lessonTaskId : 334
     * examText : null
     * resourceText : {"baseEducationId":2,"baseEducationName":"五四制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文老师","baseVersionId":12,"baseVersionName":"人民教育版","baseVolumeId":1,"baseVolumeName":"上册","chapterId":137,"chapterIdPath":"0.134.135.137","chapterName":"登黄鹤楼12","chapterNamePath":"第一章2222222>第一节111>登黄鹤楼12","collectCount":0,"convertDate":1521165153000,"convertDocumentPath":"http://192.168.0.215:8886/convert/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.pdf","convertPath":"http://192.168.0.215:8886/convert/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.pdf","convertStatus":2,"createDate":1521165321000,"createUserId":18,"createUserName":"王春晓","customerSchoolId":5,"customerSchoolName":"高新区第一实验中学","downloadCount":0,"hots":0,"id":70,"isShare":1,"resourceName":"新建 Microsoft Word 文档.docx","resourcePath":"http://192.168.0.215:8886/resource/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.docx","resourceSize":10973,"resourceSuffix":".docx","resourceType":1,"scene":1,"subjectStructureId":7,"subjectStructureIdPath":"0.1.7","subjectStructureName":"新授课","subjectStructureNamePath":"学案>新授课","updateDate":1521173063000,"useCount":0}
     * score : 0
     * examSeq : 0
     * childList : []
     * isAnswer : null
     * isCollect : false
     * studentBookId : null
     * lessonAnswerExamId : 36
     * lessonAnswerExamParentId : 0
     * isRight : null
     * examOptionList : null
     * examList : []
     */

    private int id;
    private Object examId;
    private Object examTemplateId;
    private Object examTemplateStyleId;
    private Object examTemplateStyleName;
    private Object examEmptyCount;
    private Object examDifficulty;
    private int resourceId;
    private int resourceType;
    private String resourceName;
    private int type;
    private int lessonLibId;
    private int parentId;
    private int seq;
    private int lessonTaskId;
    private Object examText;
    private String resourceText;
    private int score;
    private int examSeq;
    private Object isAnswer;
    private boolean isCollect;
    private Object studentBookId;
    private int lessonAnswerExamId;
    private int lessonAnswerExamParentId;
    private Object isRight;
    private Object examOptionList;
    private List<?> childList;
    private List<?> examList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getExamId() {
        return examId;
    }

    public void setExamId(Object examId) {
        this.examId = examId;
    }

    public Object getExamTemplateId() {
        return examTemplateId;
    }

    public void setExamTemplateId(Object examTemplateId) {
        this.examTemplateId = examTemplateId;
    }

    public Object getExamTemplateStyleId() {
        return examTemplateStyleId;
    }

    public void setExamTemplateStyleId(Object examTemplateStyleId) {
        this.examTemplateStyleId = examTemplateStyleId;
    }

    public Object getExamTemplateStyleName() {
        return examTemplateStyleName;
    }

    public void setExamTemplateStyleName(Object examTemplateStyleName) {
        this.examTemplateStyleName = examTemplateStyleName;
    }

    public Object getExamEmptyCount() {
        return examEmptyCount;
    }

    public void setExamEmptyCount(Object examEmptyCount) {
        this.examEmptyCount = examEmptyCount;
    }

    public Object getExamDifficulty() {
        return examDifficulty;
    }

    public void setExamDifficulty(Object examDifficulty) {
        this.examDifficulty = examDifficulty;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public int getResourceType() {
        return resourceType;
    }

    public void setResourceType(int resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLessonLibId() {
        return lessonLibId;
    }

    public void setLessonLibId(int lessonLibId) {
        this.lessonLibId = lessonLibId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getLessonTaskId() {
        return lessonTaskId;
    }

    public void setLessonTaskId(int lessonTaskId) {
        this.lessonTaskId = lessonTaskId;
    }

    public Object getExamText() {
        return examText;
    }

    public void setExamText(Object examText) {
        this.examText = examText;
    }

    public String getResourceText() {
        return resourceText;
    }

    public void setResourceText(String resourceText) {
        this.resourceText = resourceText;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getExamSeq() {
        return examSeq;
    }

    public void setExamSeq(int examSeq) {
        this.examSeq = examSeq;
    }

    public Object getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(Object isAnswer) {
        this.isAnswer = isAnswer;
    }

    public boolean isIsCollect() {
        return isCollect;
    }

    public void setIsCollect(boolean isCollect) {
        this.isCollect = isCollect;
    }

    public Object getStudentBookId() {
        return studentBookId;
    }

    public void setStudentBookId(Object studentBookId) {
        this.studentBookId = studentBookId;
    }

    public int getLessonAnswerExamId() {
        return lessonAnswerExamId;
    }

    public void setLessonAnswerExamId(int lessonAnswerExamId) {
        this.lessonAnswerExamId = lessonAnswerExamId;
    }

    public int getLessonAnswerExamParentId() {
        return lessonAnswerExamParentId;
    }

    public void setLessonAnswerExamParentId(int lessonAnswerExamParentId) {
        this.lessonAnswerExamParentId = lessonAnswerExamParentId;
    }

    public Object getIsRight() {
        return isRight;
    }

    public void setIsRight(Object isRight) {
        this.isRight = isRight;
    }

    public Object getExamOptionList() {
        return examOptionList;
    }

    public void setExamOptionList(Object examOptionList) {
        this.examOptionList = examOptionList;
    }

    public List<?> getChildList() {
        return childList;
    }

    public void setChildList(List<?> childList) {
        this.childList = childList;
    }

    public List<?> getExamList() {
        return examList;
    }

    public void setExamList(List<?> examList) {
        this.examList = examList;
    }

    public class ResourceTextVo {

        /**
         * baseEducationId : 2
         * baseEducationName : 五四制
         * baseGradeId : 7
         * baseGradeName : 七年级
         * baseLevelId : 2
         * baseLevelName : 初中
         * baseSubjectId : 1
         * baseSubjectName : 语文老师
         * baseVersionId : 12
         * baseVersionName : 人民教育版
         * baseVolumeId : 1
         * baseVolumeName : 上册
         * chapterId : 137
         * chapterIdPath : 0.134.135.137
         * chapterName : 登黄鹤楼12
         * chapterNamePath : 第一章2222222>第一节111>登黄鹤楼12
         * collectCount : 0
         * convertDate : 1521165153000
         * convertDocumentPath : http://192.168.0.215:8886/convert/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.pdf
         * convertPath : http://192.168.0.215:8886/convert/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.pdf
         * convertStatus : 2
         * createDate : 1521165321000
         * createUserId : 18
         * createUserName : 王春晓
         * customerSchoolId : 5
         * customerSchoolName : 高新区第一实验中学
         * downloadCount : 0
         * hots : 0
         * id : 70
         * isShare : 1
         * resourceName : 新建 Microsoft Word 文档.docx
         * resourcePath : http://192.168.0.215:8886/resource/document/2018/03/f3ce8b30d6c84c189aed78540ad57dab.docx
         * resourceSize : 10973
         * resourceSuffix : .docx
         * resourceType : 1
         * scene : 1
         * subjectStructureId : 7
         * subjectStructureIdPath : 0.1.7
         * subjectStructureName : 新授课
         * subjectStructureNamePath : 学案>新授课
         * updateDate : 1521173063000
         * useCount : 0
         */

        private int baseEducationId;
        private String baseEducationName;
        private int baseGradeId;
        private String baseGradeName;
        private int baseLevelId;
        private String baseLevelName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseVersionId;
        private String baseVersionName;
        private int baseVolumeId;
        private String baseVolumeName;
        private int chapterId;
        private String chapterIdPath;
        private String chapterName;
        private String chapterNamePath;
        private int collectCount;
        private long convertDate;
        private String convertDocumentPath;

        private int convertStatus;
        private long createDate;
        private int createUserId;
        private String createUserName;
        private int customerSchoolId;
        private String customerSchoolName;
        private int downloadCount;
        private int hots;
        private String id;
        private int isShare;
        private String resourceName;
        private String resourcePath;
        private int resourceSize;
        private String resourceSuffix;
        private int resourceType;
        private int scene;
        private int subjectStructureId;
        private String subjectStructureIdPath;
        private String subjectStructureName;
        private String subjectStructureNamePath;
        private long updateDate;
        private int useCount;

        public int getBaseEducationId() {
            return baseEducationId;
        }

        public void setBaseEducationId(int baseEducationId) {
            this.baseEducationId = baseEducationId;
        }

        public String getBaseEducationName() {
            return baseEducationName;
        }

        public void setBaseEducationName(String baseEducationName) {
            this.baseEducationName = baseEducationName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseVersionId() {
            return baseVersionId;
        }

        public void setBaseVersionId(int baseVersionId) {
            this.baseVersionId = baseVersionId;
        }

        public String getBaseVersionName() {
            return baseVersionName;
        }

        public void setBaseVersionName(String baseVersionName) {
            this.baseVersionName = baseVersionName;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterIdPath() {
            return chapterIdPath;
        }

        public void setChapterIdPath(String chapterIdPath) {
            this.chapterIdPath = chapterIdPath;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNamePath() {
            return chapterNamePath;
        }

        public void setChapterNamePath(String chapterNamePath) {
            this.chapterNamePath = chapterNamePath;
        }

        public int getCollectCount() {
            return collectCount;
        }

        public void setCollectCount(int collectCount) {
            this.collectCount = collectCount;
        }

        public long getConvertDate() {
            return convertDate;
        }

        public void setConvertDate(long convertDate) {
            this.convertDate = convertDate;
        }

        public String getConvertDocumentPath() {
            return convertDocumentPath;
        }

        public void setConvertDocumentPath(String convertDocumentPath) {
            this.convertDocumentPath = convertDocumentPath;
        }

        public String getConvertPath() {
            return resourcePath;
        }

        public void setConvertPath(String convertPath) {
            this.resourcePath = convertPath;
        }

        public int getConvertStatus() {
            return convertStatus;
        }

        public void setConvertStatus(int convertStatus) {
            this.convertStatus = convertStatus;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public int getDownloadCount() {
            return downloadCount;
        }

        public void setDownloadCount(int downloadCount) {
            this.downloadCount = downloadCount;
        }

        public int getHots() {
            return hots;
        }

        public void setHots(int hots) {
            this.hots = hots;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getIsShare() {
            return isShare;
        }

        public void setIsShare(int isShare) {
            this.isShare = isShare;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public String getResourcePath() {
            return resourcePath;
        }

        public void setResourcePath(String resourcePath) {
            this.resourcePath = resourcePath;
        }

        public int getResourceSize() {
            return resourceSize;
        }

        public void setResourceSize(int resourceSize) {
            this.resourceSize = resourceSize;
        }

        public String getResourceSuffix() {
            return resourceSuffix;
        }

        public void setResourceSuffix(String resourceSuffix) {
            this.resourceSuffix = resourceSuffix;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public int getScene() {
            return scene;
        }

        public void setScene(int scene) {
            this.scene = scene;
        }

        public int getSubjectStructureId() {
            return subjectStructureId;
        }

        public void setSubjectStructureId(int subjectStructureId) {
            this.subjectStructureId = subjectStructureId;
        }

        public String getSubjectStructureIdPath() {
            return subjectStructureIdPath;
        }

        public void setSubjectStructureIdPath(String subjectStructureIdPath) {
            this.subjectStructureIdPath = subjectStructureIdPath;
        }

        public String getSubjectStructureName() {
            return subjectStructureName;
        }

        public void setSubjectStructureName(String subjectStructureName) {
            this.subjectStructureName = subjectStructureName;
        }

        public String getSubjectStructureNamePath() {
            return subjectStructureNamePath;
        }

        public void setSubjectStructureNamePath(String subjectStructureNamePath) {
            this.subjectStructureNamePath = subjectStructureNamePath;
        }

        public long getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(long updateDate) {
            this.updateDate = updateDate;
        }

        public int getUseCount() {
            return useCount;
        }

        public void setUseCount(int useCount) {
            this.useCount = useCount;
        }
    }
}
