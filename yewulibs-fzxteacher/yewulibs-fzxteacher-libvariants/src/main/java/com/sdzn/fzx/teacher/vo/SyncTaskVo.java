package com.sdzn.fzx.teacher.vo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * SyncTaskVo〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SyncTaskVo {

    /**
     * total : 18
     * data : [{"id":543,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522219725000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522252800000,"endTime":1522339200000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":1,"countTeacherCorrect":0,"useTimeAvg":1720374,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"},{"id":539,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522217172000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522217172000,"endTime":1522252800000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":1,"countTeacherCorrect":0,"useTimeAvg":21,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"},{"id":524,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522216433000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522216433000,"endTime":1522339200000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":0,"countTeacherCorrect":0,"useTimeAvg":0,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"},{"id":523,"name":"哈哈哈","chapterId":2145,"chapterName":"预备篇","chapterNodeIdPath":"0.2145","chapterNodeNamePath":"预备篇","baseVolumeId":10,"baseVolumeName":"人教新目标版","baseSubjectId":3,"baseSubjectName":"英语老师","baseGradeId":7,"baseGradeName":"七年级","createUserId":41,"createUserName":"王春晓","createTime":1522216425000,"type":1,"typeReview":null,"typeReviewName":null,"classId":22,"className":"五班","startTime":1522216425000,"endTime":1522252800000,"countExam":2,"countRescoure":1,"score":2,"scoreAvg":0,"countStudentTotal":5,"countStudentSub":0,"countTeacherCorrect":0,"useTimeAvg":0,"answerViewType":1,"answerViewOpen":0,"lessonLibId":237,"lessonLibName":"哈哈哈","lessonId":320,"lessonName":"002","scope":2017,"status":3,"statusName":"已截止"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * id : 543
         * name : 哈哈哈
         * chapterId : 2145
         * chapterName : 预备篇
         * chapterNodeIdPath : 0.2145
         * chapterNodeNamePath : 预备篇
         * baseVolumeId : 10
         * baseVolumeName : 人教新目标版
         * baseSubjectId : 3
         * baseSubjectName : 英语老师
         * baseGradeId : 7
         * baseGradeName : 七年级
         * createUserId : 41
         * createUserName : 王春晓
         * createTime : 1522219725000
         * type : 1
         * typeReview : null
         * typeReviewName : null
         * classId : 22
         * className : 五班
         * startTime : 1522252800000
         * endTime : 1522339200000
         * countExam : 2
         * countRescoure : 1
         * score : 2
         * scoreAvg : 0
         * countStudentTotal : 5
         * countStudentSub : 1
         * countTeacherCorrect : 0
         * useTimeAvg : 1720374
         * answerViewType : 1
         * answerViewOpen : 0
         * lessonLibId : 237
         * lessonLibName : 哈哈哈
         * lessonId : 320
         * lessonName : 002
         * scope : 2017
         * status : 3
         * statusName : 已截止
         */

        private int id;
        private String name;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private int baseVolumeId;
        private String baseVolumeName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseGradeId;
        private String baseGradeName;
        private int createUserId;
        private String createUserName;
        private long createTime;
        private int type;
        private String typeReview;
        private String typeReviewName;
        private int classId;
        private String className;
        private long startTime;
        private long endTime;
        private int countExam;
        private int countRescoure;
        private int score;
        private double scoreAvg;
        private int countStudentTotal;
        private int countStudentSub;
        private int countTeacherCorrect;
        private double useTimeAvg;
        private int answerViewType;
        private int answerViewOpen;
        private int lessonLibId;
        private String lessonLibName;
        private int lessonId;
        private String lessonName;
        private int scope;
        private int status;
        private String statusName;
        private int sceneId;
        private String sceneName;
        private String teacherExamId;
        private int isHide;
        private int correctType;

        public int getCorrectType() {
            return correctType;
        }

        public void setCorrectType(int correctType) {
            this.correctType = correctType;
        }

        public String getTeacherExamId() {
            return teacherExamId;
        }

        public void setTeacherExamId(String teacherExamId) {
            this.teacherExamId = teacherExamId;
        }

        public int getIsHide() {
            return isHide;
        }

        public void setIsHide(int isHide) {
            this.isHide = isHide;
        }

        public int getSceneId() {
            return sceneId;
        }

        public void setSceneId(int sceneId) {
            this.sceneId = sceneId;
        }

        public String getSceneName() {
            return sceneName;
        }

        public void setSceneName(String sceneName) {
            this.sceneName = sceneName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getTypeReview() {
            return typeReview;
        }

        public void setTypeReview(String typeReview) {
            this.typeReview = typeReview;
        }

        public String getTypeReviewName() {
            return typeReviewName;
        }

        public void setTypeReviewName(String typeReviewName) {
            this.typeReviewName = typeReviewName;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public int getCountExam() {
            return countExam;
        }

        public void setCountExam(int countExam) {
            this.countExam = countExam;
        }

        public int getCountRescoure() {
            return countRescoure;
        }

        public void setCountRescoure(int countRescoure) {
            this.countRescoure = countRescoure;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public double getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(double scoreAvg) {
            this.scoreAvg = scoreAvg;
        }

        public int getCountStudentTotal() {
            return countStudentTotal;
        }

        public void setCountStudentTotal(int countStudentTotal) {
            this.countStudentTotal = countStudentTotal;
        }

        public int getCountStudentSub() {
            return countStudentSub;
        }

        public void setCountStudentSub(int countStudentSub) {
            this.countStudentSub = countStudentSub;
        }

        public int getCountTeacherCorrect() {
            return countTeacherCorrect;
        }

        public void setCountTeacherCorrect(int countTeacherCorrect) {
            this.countTeacherCorrect = countTeacherCorrect;
        }

        public double getUseTimeAvg() {
            return useTimeAvg;
        }

        public void setUseTimeAvg(double useTimeAvg) {
            this.useTimeAvg = useTimeAvg;
        }

        public int getAnswerViewType() {
            return answerViewType;
        }

        public void setAnswerViewType(int answerViewType) {
            this.answerViewType = answerViewType;
        }

        public int getAnswerViewOpen() {
            return answerViewOpen;
        }

        public void setAnswerViewOpen(int answerViewOpen) {
            this.answerViewOpen = answerViewOpen;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public String getLessonLibName() {
            return lessonLibName;
        }

        public void setLessonLibName(String lessonLibName) {
            this.lessonLibName = lessonLibName;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusName() {
            return statusName;
        }

        public void setStatusName(String statusName) {
            this.statusName = statusName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.name);
            dest.writeInt(this.chapterId);
            dest.writeString(this.chapterName);
            dest.writeString(this.chapterNodeIdPath);
            dest.writeString(this.chapterNodeNamePath);
            dest.writeInt(this.baseVolumeId);
            dest.writeString(this.baseVolumeName);
            dest.writeInt(this.baseSubjectId);
            dest.writeString(this.baseSubjectName);
            dest.writeInt(this.baseGradeId);
            dest.writeString(this.baseGradeName);
            dest.writeInt(this.createUserId);
            dest.writeString(this.createUserName);
            dest.writeLong(this.createTime);
            dest.writeInt(this.type);
            dest.writeString(this.typeReview);
            dest.writeString(this.typeReviewName);
            dest.writeInt(this.classId);
            dest.writeString(this.className);
            dest.writeLong(this.startTime);
            dest.writeLong(this.endTime);
            dest.writeInt(this.countExam);
            dest.writeInt(this.countRescoure);
            dest.writeInt(this.score);
            dest.writeDouble(this.scoreAvg);
            dest.writeInt(this.countStudentTotal);
            dest.writeInt(this.countStudentSub);
            dest.writeInt(this.countTeacherCorrect);
            dest.writeDouble(this.useTimeAvg);
            dest.writeInt(this.answerViewType);
            dest.writeInt(this.answerViewOpen);
            dest.writeInt(this.lessonLibId);
            dest.writeString(this.lessonLibName);
            dest.writeInt(this.lessonId);
            dest.writeString(this.lessonName);
            dest.writeInt(this.scope);
            dest.writeInt(this.status);
            dest.writeString(this.statusName);
            dest.writeString(this.teacherExamId);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.id = in.readInt();
            this.name = in.readString();
            this.chapterId = in.readInt();
            this.chapterName = in.readString();
            this.chapterNodeIdPath = in.readString();
            this.chapterNodeNamePath = in.readString();
            this.baseVolumeId = in.readInt();
            this.baseVolumeName = in.readString();
            this.baseSubjectId = in.readInt();
            this.baseSubjectName = in.readString();
            this.baseGradeId = in.readInt();
            this.baseGradeName = in.readString();
            this.createUserId = in.readInt();
            this.createUserName = in.readString();
            this.createTime = in.readLong();
            this.type = in.readInt();
            this.typeReview = in.readString();
            this.typeReviewName = in.readString();
            this.classId = in.readInt();
            this.className = in.readString();
            this.startTime = in.readLong();
            this.endTime = in.readLong();
            this.countExam = in.readInt();
            this.countRescoure = in.readInt();
            this.score = in.readInt();
            this.scoreAvg = in.readDouble();
            this.countStudentTotal = in.readInt();
            this.countStudentSub = in.readInt();
            this.countTeacherCorrect = in.readInt();
            this.useTimeAvg = in.readDouble();
            this.answerViewType = in.readInt();
            this.answerViewOpen = in.readInt();
            this.lessonLibId = in.readInt();
            this.lessonLibName = in.readString();
            this.lessonId = in.readInt();
            this.lessonName = in.readString();
            this.scope = in.readInt();
            this.status = in.readInt();
            this.statusName = in.readString();
            this.teacherExamId = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
