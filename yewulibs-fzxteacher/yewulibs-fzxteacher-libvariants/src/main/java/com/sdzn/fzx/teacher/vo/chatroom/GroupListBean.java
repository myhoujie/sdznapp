package com.sdzn.fzx.teacher.vo.chatroom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by admin on 2019/8/16.
 */

public class GroupListBean {

    public List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * id : 25
         * classId : 596
         * groupId : 1
         * lessonId : null
         * state : -1
         * score : 0
         * groupChatTaskId : 17
         * roomId : 17
         * createTime : 2019-08-14T09:46:51.000+0000
         * endTime : 2019-08-14T09:46:50.000+0000
         * chatTitle : 小组讨论标题
         * chatContent : 小组讨论内容
         * teachGroupChatResultPics : [{"id":2,"picUrl":"http://pic25.nipic.com/20121112/9252150_150552938000_2.jpg","groupChatResultId":25,"userStudentId":null,"createTime":null,"groupId":null,"groupName":null,"groupChatTaskId":null,"userStudentName":null}]
         *
         *
         * "chatTitle": "拜拜",
         *         "createUserId": 216,
         *         "subjectId": 1,
         *         "baseVolumeId": 44,
         *         "baseVolumeName": "上册",
         *         "chapterId": 4067,
         *         "chapterName": "春",
         *         "groupChatLibId": null,
         *         "endTime": "2019-08-29 17:35:31",
         *         "createTime": "2019-08-29 17:36:58",
         *         "classId": 604,
         *         "state": 1,
         */

        private int id;
        private int classId;
        private int groupId;
        private int lessonId;
        private int state;
        private float score;
        private int groupChatTaskId;
        private String roomId;
        private String createTime;
        private String endTime;
        private String chatTitle;
        private String chatContent;
        private String subjectId;

        private String baseVolumeName;
        private String baseVolumeId;
        private String chapterId;
        private String chapterName;


//        private List<TeachGroupChatResultPicsBean> teachGroupChatResultPics;
        private List<TeachGroupChatContentPicsBean> teachGroupChatContentPics;

        public List<TeachGroupChatContentPicsBean> getTeachGroupChatContentPics() {
            return teachGroupChatContentPics;
        }

        public void setTeachGroupChatContentPics(List<TeachGroupChatContentPicsBean> teachGroupChatContentPics) {
            this.teachGroupChatContentPics = teachGroupChatContentPics;
        }

        public static class TeachGroupChatContentPicsBean implements Parcelable {
            /**
             * id : 18
             * picUrl : http://csfile.fuzhuxian.com/ad38d4a8-db4a-4ee7-95c4-ea16e131f25d.jpg
             * groupChatLibId : 13
             * groupChatTaskId : null
             * createTime : 2019-08-24T09:37:55.000+0000
             */

            private int id;
            private String picUrl;
            private int groupChatLibId;
            private String groupChatTaskId;
            private String createTime;

            public TeachGroupChatContentPicsBean(String picUrl) {
                this.picUrl = picUrl;

            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public int getGroupChatLibId() {
                return groupChatLibId;
            }

            public void setGroupChatLibId(int groupChatLibId) {
                this.groupChatLibId = groupChatLibId;
            }

            public String getGroupChatTaskId() {
                return groupChatTaskId;
            }

            public void setGroupChatTaskId(String groupChatTaskId) {
                this.groupChatTaskId = groupChatTaskId;
            }


            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeString(this.picUrl);
                dest.writeInt(this.groupChatLibId);
                dest.writeString(this.groupChatTaskId);
                dest.writeString(this.createTime);
            }

            public TeachGroupChatContentPicsBean() {
            }


            protected TeachGroupChatContentPicsBean(Parcel in) {
                this.id = in.readInt();
                this.picUrl = in.readString();
                this.groupChatLibId = in.readInt();
                this.groupChatTaskId = in.readString();
                this.createTime = in.readString();
            }

            public static final Creator<TeachGroupChatContentPicsBean> CREATOR = new Creator<TeachGroupChatContentPicsBean>() {
                @Override
                public TeachGroupChatContentPicsBean createFromParcel(Parcel source) {
                    return new TeachGroupChatContentPicsBean(source);
                }

                @Override
                public TeachGroupChatContentPicsBean[] newArray(int size) {
                    return new TeachGroupChatContentPicsBean[size];
                }
            };
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public String getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(String baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getChapterId() {
            return chapterId;
        }

        public void setChapterId(String chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }



        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }



        public int getGroupChatTaskId() {
            return groupChatTaskId;
        }

        public void setGroupChatTaskId(int groupChatTaskId) {
            this.groupChatTaskId = groupChatTaskId;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public String getRoomId() {
            return roomId;
        }

        public void setRoomId(String roomId) {
            this.roomId = roomId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getChatTitle() {
            return chatTitle;
        }

        public void setChatTitle(String chatTitle) {
            this.chatTitle = chatTitle;
        }

        public String getChatContent() {
            return chatContent;
        }

        public void setChatContent(String chatContent) {
            this.chatContent = chatContent;
        }

//        public List<TeachGroupChatResultPicsBean> getTeachGroupChatResultPics() {
//            return teachGroupChatResultPics;
//        }
//
//        public void setTeachGroupChatResultPics(List<TeachGroupChatResultPicsBean> teachGroupChatResultPics) {
//            this.teachGroupChatResultPics = teachGroupChatResultPics;
//        }



        public static class TeachGroupChatResultPicsBean implements Parcelable {
            /**
             * id : 2
             * picUrl : http://pic25.nipic.com/20121112/9252150_150552938000_2.jpg
             * groupChatResultId : 25
             * userStudentId : null
             * createTime : null
             * groupId : null
             * groupName : null
             * groupChatTaskId : null
             * userStudentName : null
             */


            private int id;
            private String picUrl;
            private int groupChatResultId;
            private String userStudentId;
            private String createTime;
            private String groupId;
            private String groupName;
            private String groupChatTaskId;
            private String userStudentName;



            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public int getGroupChatResultId() {
                return groupChatResultId;
            }

            public void setGroupChatResultId(int groupChatResultId) {
                this.groupChatResultId = groupChatResultId;
            }

            public Object getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(String userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getGroupId() {
                return groupId;
            }

            public void setGroupId(String groupId) {
                this.groupId = groupId;
            }

            public String getGroupName() {
                return groupName;
            }

            public void setGroupName(String groupName) {
                this.groupName = groupName;
            }

            public String getGroupChatTaskId() {
                return groupChatTaskId;
            }

            public void setGroupChatTaskId(String groupChatTaskId) {
                this.groupChatTaskId = groupChatTaskId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.id);
                dest.writeString(this.picUrl);
                dest.writeInt(this.groupChatResultId);
                dest.writeString(this.userStudentId);
                dest.writeString(this.createTime);
                dest.writeString(this.groupId);
                dest.writeString(this.groupName);
                dest.writeString(this.groupChatTaskId);
                dest.writeString(this.userStudentName);
            }

            public TeachGroupChatResultPicsBean() {
            }

            protected TeachGroupChatResultPicsBean(Parcel in) {
                this.id = in.readInt();
                this.picUrl = in.readString();
                this.groupChatResultId = in.readInt();
                this.userStudentId = in.readString();
                this.createTime = in.readString();
                this.groupId = in.readString();
                this.groupName = in.readString();
                this.groupChatTaskId = in.readString();
                this.userStudentName = in.readString();
            }

            public static final Creator<TeachGroupChatResultPicsBean> CREATOR = new Creator<TeachGroupChatResultPicsBean>() {
                @Override
                public TeachGroupChatResultPicsBean createFromParcel(Parcel source) {
                    return new TeachGroupChatResultPicsBean(source);
                }

                @Override
                public TeachGroupChatResultPicsBean[] newArray(int size) {
                    return new TeachGroupChatResultPicsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.classId);
            dest.writeInt(this.groupId);
            dest.writeInt(this.lessonId);
            dest.writeInt(this.state);
            dest.writeFloat(this.score);
            dest.writeInt(this.groupChatTaskId);
            dest.writeString(this.roomId);
            dest.writeString(this.createTime);
            dest.writeString(this.endTime);
            dest.writeString(this.chatTitle);
            dest.writeString(this.chatContent);
            dest.writeString(this.subjectId);
            dest.writeString(this.baseVolumeName);
            dest.writeString(this.baseVolumeId);
            dest.writeString(this.chapterId);
            dest.writeString(this.chapterName);
//            dest.writeList(this.teachGroupChatResultPics);
        }
        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.id = in.readInt();
            this.classId = in.readInt();
            this.groupId = in.readInt();
            this.lessonId = in.readInt();
            this.state = in.readInt();
            this.score = in.readFloat();
            this.groupChatTaskId = in.readInt();
            this.roomId = in.readString();
            this.createTime = in.readString();
            this.endTime = in.readString();
            this.chatTitle = in.readString();
            this.chatContent = in.readString();
            this.subjectId = in.readString();
            this.baseVolumeName = in.readString();
            this.baseVolumeId = in.readString();
            this.chapterId = in.readString();
            this.chapterName = in.readString();
//            this.teachGroupChatResultPics = new ArrayList<TeachGroupChatResultPicsBean>();
//            in.readList(this.teachGroupChatResultPics, TeachGroupChatResultPicsBean.class.getClassLoader());
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

}
