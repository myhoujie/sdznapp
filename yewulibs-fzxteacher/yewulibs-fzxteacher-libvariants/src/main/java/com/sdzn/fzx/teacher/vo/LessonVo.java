package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/11
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonVo {

    /**
     * total : 1
     * data : [{"id":977,"name":"001","chapterId":null,"chapterName":null,"chapterNodeIdPath":null,"chapterNodeNamePath":null,"baseVolumeId":null,"baseVolumeName":null,"baseSubjectId":6,"baseSubjectName":"生物","count":1,"userCreateId":41,"userCreateName":"王春晓","timeCreate":1525309048000,"timeUpdate":1525309087000,"userHoldId":41,"userHoldName":"王春晓","flagShare":0,"flagSync":0,"type":2,"typeReview":300,"typeReviewName":"七年级上学期期中复习","baseGradeId":null,"baseGradeName":null,"hots":0,"customerSchoolId":6,"customerSchoolName":"老师端测试学校"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 977
         * name : 001
         * chapterId : null
         * chapterName : null
         * chapterNodeIdPath : null
         * chapterNodeNamePath : null
         * baseVolumeId : null
         * baseVolumeName : null
         * baseSubjectId : 6
         * baseSubjectName : 生物
         * count : 1
         * userCreateId : 41
         * userCreateName : 王春晓
         * timeCreate : 1525309048000
         * timeUpdate : 1525309087000
         * userHoldId : 41
         * userHoldName : 王春晓
         * flagShare : 0
         * flagSync : 0
         * type : 2
         * typeReview : 300
         * typeReviewName : 七年级上学期期中复习
         * baseGradeId : null
         * baseGradeName : null
         * hots : 0
         * customerSchoolId : 6
         * customerSchoolName : 老师端测试学校
         */

        private int id;
        private String name;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private int baseVolumeId;
        private String baseVolumeName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int count;
        private int userCreateId;
        private String userCreateName;
        private long timeCreate;
        private long timeUpdate;
        private int userHoldId;
        private String userHoldName;
        private int flagShare;
        private int flagSync;
        private int type;
        private int typeReview;
        private String typeReviewName;
        private int baseGradeId;
        private String baseGradeName;
        private int hots;
        private int customerSchoolId;
        private String customerSchoolName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getUserCreateId() {
            return userCreateId;
        }

        public void setUserCreateId(int userCreateId) {
            this.userCreateId = userCreateId;
        }

        public String getUserCreateName() {
            return userCreateName;
        }

        public void setUserCreateName(String userCreateName) {
            this.userCreateName = userCreateName;
        }

        public long getTimeCreate() {
            return timeCreate;
        }

        public void setTimeCreate(long timeCreate) {
            this.timeCreate = timeCreate;
        }

        public long getTimeUpdate() {
            return timeUpdate;
        }

        public void setTimeUpdate(long timeUpdate) {
            this.timeUpdate = timeUpdate;
        }

        public int getUserHoldId() {
            return userHoldId;
        }

        public void setUserHoldId(int userHoldId) {
            this.userHoldId = userHoldId;
        }

        public String getUserHoldName() {
            return userHoldName;
        }

        public void setUserHoldName(String userHoldName) {
            this.userHoldName = userHoldName;
        }

        public int getFlagShare() {
            return flagShare;
        }

        public void setFlagShare(int flagShare) {
            this.flagShare = flagShare;
        }

        public int getFlagSync() {
            return flagSync;
        }

        public void setFlagSync(int flagSync) {
            this.flagSync = flagSync;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getTypeReview() {
            return typeReview;
        }

        public void setTypeReview(int typeReview) {
            this.typeReview = typeReview;
        }

        public String getTypeReviewName() {
            return typeReviewName;
        }

        public void setTypeReviewName(String typeReviewName) {
            this.typeReviewName = typeReviewName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getHots() {
            return hots;
        }

        public void setHots(int hots) {
            this.hots = hots;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }
    }
}
