package com.sdzn.fzx.teacher.vo.me;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ClassGrouping {
    private ClassGroupingVoListVo classGroupingVoListVo;

    public ClassGroupingVoListVo getClassGroupingVoListVo() {
        return classGroupingVoListVo;
    }

    public void setClassGroupingVoListVo(ClassGroupingVoListVo classGroupingVoListVo) {
        this.classGroupingVoListVo = classGroupingVoListVo;
    }
}
