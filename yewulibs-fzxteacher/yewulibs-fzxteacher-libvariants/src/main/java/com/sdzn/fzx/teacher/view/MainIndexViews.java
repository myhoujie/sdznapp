package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.SubjectData;
import com.sdzn.fzx.teacher.bean.SubjectData1;

public interface MainIndexViews extends IView {
    void getSubjectInfo(SubjectData1 subjectData);

    void OnSubjectNodata(String msg);

    void onSubjectFailed(String msg);
}
