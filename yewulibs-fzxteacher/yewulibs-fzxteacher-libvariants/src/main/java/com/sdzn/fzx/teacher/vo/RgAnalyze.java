package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/13
 * 修改单号：
 * 修改内容:
 */
public class RgAnalyze {
    /**
     * total : 3
     * data : [{"id":16303,"examId":null,"examTemplateId":null,"examTemplateStyleId":null,"examTemplateStyleName":null,"examEmptyCount":null,"examDifficulty":null,"resourceId":1192,"resourceType":3,"resourceName":"VID_20181114_101240","type":2,"lessonLibId":1946,"parentId":0,"seq":2,"lessonTaskId":1481,"examText":null,"resourceText":"null","score":-1,"examSeq":0,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":null,"taskResourceId":"5c42f5389adbd305a4387edf"},{"id":16305,"examId":null,"examTemplateId":null,"examTemplateStyleId":null,"examTemplateStyleName":null,"examEmptyCount":null,"examDifficulty":null,"resourceId":1194,"resourceType":5,"resourceName":"1-01 九张机","type":2,"lessonLibId":1946,"parentId":0,"seq":4,"lessonTaskId":1481,"examText":null,"resourceText":"null","score":-1,"examSeq":0,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":null,"taskResourceId":"5c42f53e9adbd305a4387f6e"},{"id":16307,"examId":null,"examTemplateId":null,"examTemplateStyleId":null,"examTemplateStyleName":null,"examEmptyCount":null,"examDifficulty":null,"resourceId":1188,"resourceType":3,"resourceName":"123.mp4","type":2,"lessonLibId":1946,"parentId":0,"seq":6,"lessonTaskId":1481,"examText":null,"resourceText":"null","score":-1,"examSeq":0,"childList":[],"libExamId":null,"libResourceId":null,"taskExamId":null,"taskResourceId":"5c42f5409adbd305a4387fb6"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 16303
         * examId : null
         * examTemplateId : null
         * examTemplateStyleId : null
         * examTemplateStyleName : null
         * examEmptyCount : null
         * examDifficulty : null
         * resourceId : 1192
         * resourceType : 3
         * resourceName : VID_20181114_101240
         * type : 2
         * lessonLibId : 1946
         * parentId : 0
         * seq : 2
         * lessonTaskId : 1481
         * examText : null
         * resourceText : null
         * score : -1.0
         * examSeq : 0
         * childList : []
         * libExamId : null
         * libResourceId : null
         * taskExamId : null
         * taskResourceId : 5c42f5389adbd305a4387edf
         */

        private int id;
        private Object examId;
        private Object examTemplateId;
        private Object examTemplateStyleId;
        private Object examTemplateStyleName;
        private Object examEmptyCount;
        private Object examDifficulty;
        private int resourceId;
        private int resourceType;
        private String resourceName;
        private int type;
        private int lessonLibId;
        private int parentId;
        private int seq;
        private int lessonTaskId;
        private Object examText;
        private String resourceText;
        private double score;
        private int examSeq;
        private Object libExamId;
        private Object libResourceId;
        private Object taskExamId;
        private String taskResourceId;
        private List<?> childList;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getExamId() {
            return examId;
        }

        public void setExamId(Object examId) {
            this.examId = examId;
        }

        public Object getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(Object examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public Object getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(Object examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public Object getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(Object examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public Object getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(Object examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public Object getExamDifficulty() {
            return examDifficulty;
        }

        public void setExamDifficulty(Object examDifficulty) {
            this.examDifficulty = examDifficulty;
        }

        public int getResourceId() {
            return resourceId;
        }

        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(int lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public Object getExamText() {
            return examText;
        }

        public void setExamText(Object examText) {
            this.examText = examText;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public Object getLibExamId() {
            return libExamId;
        }

        public void setLibExamId(Object libExamId) {
            this.libExamId = libExamId;
        }

        public Object getLibResourceId() {
            return libResourceId;
        }

        public void setLibResourceId(Object libResourceId) {
            this.libResourceId = libResourceId;
        }

        public Object getTaskExamId() {
            return taskExamId;
        }

        public void setTaskExamId(Object taskExamId) {
            this.taskExamId = taskExamId;
        }

        public String getTaskResourceId() {
            return taskResourceId;
        }

        public void setTaskResourceId(String taskResourceId) {
            this.taskResourceId = taskResourceId;
        }

        public List<?> getChildList() {
            return childList;
        }

        public void setChildList(List<?> childList) {
            this.childList = childList;
        }
    }
}
