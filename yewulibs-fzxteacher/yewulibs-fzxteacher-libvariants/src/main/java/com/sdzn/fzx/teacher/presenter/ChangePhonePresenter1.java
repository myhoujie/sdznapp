package com.sdzn.fzx.teacher.presenter;

import android.os.Handler;
import android.os.Message;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.teacher.view.ChangePhoneViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.teacher.api.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class ChangePhonePresenter1 extends Presenter<ChangePhoneViews> {
    private long endTime;
    private String tokenStr,idStr;
    public void sendVerityCode(String token,String tel) {
        endTime = System.currentTimeMillis() + 60 * 1000;
        mHandler.sendEmptyMessageDelayed(0, 100);

        tokenStr=token;
//        JSONObject requestData = new JSONObject();
//        requestData.put("telephone",tel);//
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM(tel)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onFailed("验证码发送失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {

        savePhone(phoneNum,code);
    }

    public void savePhone(final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .updatePhone(tokenStr, phoneNum,code)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        ToastUtils.showShort("成功绑定手机号");
                        getView().verifySuccess(phoneNum, code);

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onFailed("绑定手机号码失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
//        Network.createTokenService(NetWorkService.SavePhoneService.class)
//                .SavePhone(phoneNum)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onNext(Object o) {
//                        ToastUtil.showShortlToast("成功绑定手机号");
//                        mView.savePhone();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getCode() == 21206) {
//                                ToastUtil.showShortlToast("该手机号已被绑定！");
//                            }
//                        }
//
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity));

    }

    private void stopCountDown() {
        endTime = System.currentTimeMillis();
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final int lessTime = (int) ((endTime - System.currentTimeMillis()) / 1000);

            getView().onCountDownChanged(lessTime);
            if (lessTime < 0) {
                mHandler.removeMessages(0);
            } else {
                mHandler.sendEmptyMessageDelayed(0, 100);
            }
        }
    };

//    @Override
//    public void detachView() {
//        super.detachView();
//
//    }

    @Override
    public void onDestory() {
        super.onDestory();
        mHandler.removeMessages(0);
    }
}
