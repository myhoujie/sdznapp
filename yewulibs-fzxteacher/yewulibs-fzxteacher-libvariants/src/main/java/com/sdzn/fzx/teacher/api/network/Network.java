package com.sdzn.fzx.teacher.api.network;

import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.interceptor.LoggingInterceptor;
import com.sdzn.fzx.teacher.api.interceptor.TokenInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 描述：自定义Subscriber
 * -
 * 创建人：zhangchao
 * 创建时间2017/3/20
 */
public class Network {//18212345678

    public final static boolean IS_TEST = true;

    //    public static String BASE_ADDRESS = IS_TEST ? "http://192.168.0.204" : "http://115.28.91.19";//测试环境
//    public static String BASE_ADDRESS = IS_TEST ? "http://118.190.68.12" : "http://115.28.91.19";//测试环境
//    public static final String BASE_ADDRESS = IS_TEST ? "http://192.168.0.115" : "http://115.28.91.19";//测试环境
    //    public static String HTML_BASE_ADDRESS = IS_TEST ? "http://192.168.0.91" : "http://115.28.91.19";
    // public static String HTML_BASE_ADDRESS = IS_TEST ? "http://192.168.0.91" : "http://115.28.91.19";
    public static String HTML_BASE_ADDRESS = "http://115.28.91.19";
    //    public static String BASE_MQTT_URL = IS_TEST ? "tcp://192.168.0.206:1883" : "tcp://115.28.84.106:1883";//mqttc测试地址
    public static final String BASE_MQTT_URL = BuildConfig3.MQTT;
    //    public static String BASE_SHARE_URL = IS_TEST ? "http://192.168.0.202:9096" : "http://share.runningtank.com";//分享地址
//public static String BASE_DOWN_URL = IS_TEST ? "http://192.168.0.207:8030/app.info" : "http://115.28.91.19:8020/app.info";//下载
    public static final String BASE_DOWN_URL = BuildConfig3.DOWN;
    //    public static final String POST = IS_TEST ? ":9000" : ":8000";
//    public static final String POST = IS_TEST ? ":8000" : ":8000";
    public static final String HTML_POST = ":8090";
    public final static String BASE_URL = BuildConfig3.BASE_ADDRESS + BuildConfig3.PORT;
    public final static String HTML_BASE_URL = HTML_BASE_ADDRESS + HTML_POST;

    public final static String BASE_MQTT_USENAME = "sdzn_tank";//mqtt正式用户名
    public final static String BASE_MQTT_PASSWORD = "sdzn_tank";//mqtt正式密码
    public final static String BASE_UPLOAS_VIDEO = BASE_URL + "/teach/QiniuController/save";//上传视频路径
    public final static String BASE_GET_UPTOKEN = BuildConfig3.BASE_ADDRESS + BuildConfig3.PORT + "/teach/QiniuController/getUptoken";
    public final static String BASE_GET_UPLOADTOKEN = BuildConfig3.BASE_ADDRESS + BuildConfig3.PORT + "/teach/QiniuController/getUploadToken";
    private static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    private static LoggingInterceptor loggingInterceptor = new LoggingInterceptor();

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    private static OkHttpClient tokenOkHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(tokenInterceptor)
            .build();

    private static Converter.Factory gsonConverterFactory = GsonConverterFactory.create();
    private static CallAdapter.Factory rxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();


    /**
     * 一般登陆和获取token的时候使用
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);
    }


    /**
     * 带token验证的services
     *
     * @param service
     * @param <S>
     * @return
     */
    public static <S> S createTokenService(Class<S> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(tokenOkHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit.create(service);
    }

}