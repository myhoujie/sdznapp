package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/23
 * 修改单号：
 * 修改内容:
 */
public class ExamVo {

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    private List<DataBean> data;

    /**
     * id : 132642
     * examId : null
     * examTemplateId : null
     * examTemplateStyleId : null
     * examTemplateStyleName : null
     * examEmptyCount : null
     * examDifficulty : null
     * resourceId : null
     * resourceType : null
     * resourceName : null
     * type : 3
     * parentId : 0
     * seq : 1
     * flagSync : 0
     * lessonLibId : 2199
     * lessonId : 1314
     * customerSchoolId : 36
     * customerSchoolName : 时代智囊测试
     * examText : null
     * resourceText : <p>
     * examSeq : 0
     * examTemplateName : null
     * score : null
     * childLessonLibDetail : null
     * libExamId : null
     * libResourceId : null
     */
    public static class DataBean {
        private int id;
        private Object examId;
        private int examTemplateId;
        private Object examTemplateStyleId;
        private Object examTemplateStyleName;
        private int examEmptyCount;
        private Object examDifficulty;
        private Object resourceId;
        private int resourceType;
        private String resourceName;
        private int type;
        private int parentId;
        private int seq;
        private int flagSync;
        private int lessonLibId;
        private int lessonId;
        private int customerSchoolId;
        private String customerSchoolName;
        private String examText;
        private String resourceText;
        private int examSeq;
        private Object examTemplateName;
        private int score;
        private List<DataBean> childLessonLibDetail;
        private Object libExamId;
        private Object libResourceId;
        private ExamText ExamTextVo;
        private List<DataBean> examList;
        private transient ResourceVo.ResourceTextVo resourceVoBean;

        public ResourceVo.ResourceTextVo getResourceVoBean() {
            return resourceVoBean;
        }

        public void setResourceVoBean(ResourceVo.ResourceTextVo resourceVoBean) {
            this.resourceVoBean = resourceVoBean;
        }

        public ExamText getExamTextVo() {
            return ExamTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            ExamTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getExamId() {
            return examId;
        }

        public void setExamId(Object examId) {
            this.examId = examId;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public Object getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(Object examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public Object getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(Object examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(int examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public Object getExamDifficulty() {
            return examDifficulty;
        }

        public void setExamDifficulty(Object examDifficulty) {
            this.examDifficulty = examDifficulty;
        }

        public Object getResourceId() {
            return resourceId;
        }

        public void setResourceId(Object resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getFlagSync() {
            return flagSync;
        }

        public void setFlagSync(int flagSync) {
            this.flagSync = flagSync;
        }

        public int getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(int lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public Object getExamTemplateName() {
            return examTemplateName;
        }

        public void setExamTemplateName(Object examTemplateName) {
            this.examTemplateName = examTemplateName;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public List<DataBean> getChildLessonLibDetail() {
            return childLessonLibDetail;
        }

        public void setChildLessonLibDetail(List<DataBean> childLessonLibDetail) {
            this.childLessonLibDetail = childLessonLibDetail;
        }

        public Object getLibExamId() {
            return libExamId;
        }

        public void setLibExamId(Object libExamId) {
            this.libExamId = libExamId;
        }

        public Object getLibResourceId() {
            return libResourceId;
        }

        public void setLibResourceId(Object libResourceId) {
            this.libResourceId = libResourceId;
        }


        public List<DataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<DataBean> examList) {
            this.examList = examList;
        }
    }
}
