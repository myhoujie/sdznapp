package com.sdzn.fzx.teacher.api.func;

import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.Status;

import rx.functions.Func1;

/**
 * 描述：将公共Vo转换为特定Vo
 * -
 * 创建人：wangchunxiao
 * 创建时间：2016/10/25
 */
public class StatusFunc<T> implements Func1<StatusVo<T>, T> {
    @Override
    public T call(StatusVo<T> statusVo) {

        if (statusVo != null && statusVo.getCode() == Status.SUCCESS.getCode()) {
            if (statusVo.getResult() != null) {
                return statusVo.getResult();
            } else {
                throw new ApiException(statusVo.getMsg());
            }
        } else if (statusVo != null && statusVo.getCode() == Status.TOKEN_TIME_OUT.getCode()) {
            throw new ApiException(statusVo.getCode(),statusVo.getMsg());
        } else if (statusVo != null && statusVo.getCode() == Status.TOKEN_Invalid.getCode()) {
            throw new ApiException(Status.TOKEN_Invalid.getCode());
        } else if (statusVo != null) {
            throw new ApiException(statusVo);
        } else {
            throw new ApiException(Status.SYSTEM_ERROR.getCode());
        }

    }
}