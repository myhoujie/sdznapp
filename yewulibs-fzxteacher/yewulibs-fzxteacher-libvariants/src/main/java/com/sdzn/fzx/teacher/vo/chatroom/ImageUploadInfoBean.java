package com.sdzn.fzx.teacher.vo.chatroom;

import android.graphics.Bitmap;

/**
 *
 */
public class ImageUploadInfoBean {
    private String key;
    private String upToken;
    private String domain;
    private String imageStyle;
    private Bitmap mBitmap;

    public ImageUploadInfoBean(String key, String upToken, Bitmap bitmap) {
        this.key = key;
        this.upToken = upToken;
        this.domain = domain;
        this.imageStyle = imageStyle;
        mBitmap = bitmap;
    }
 public ImageUploadInfoBean(String key, String upToken, String domain, String imageStyle, Bitmap bitmap) {
        this.key = key;
        this.upToken = upToken;
        this.domain = domain;
        this.imageStyle = imageStyle;
        mBitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUpToken() {
        return upToken;
    }

    public void setUpToken(String upToken) {
        this.upToken = upToken;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getImageStyle() {
        return imageStyle;
    }

    public void setImageStyle(String imageStyle) {
        this.imageStyle = imageStyle;
    }
}
