package com.sdzn.fzx.teacher.bean;

public class ShouyeBean {

    /**
     * tvDynamicTitle : 广告标题
     * llMyCurriculum : 我的课程
     * llAutonomyStudy : 自主学习
     * llClassroomTeaching : 课堂教学
     * llTask : 作业
     * rlExamination : "今日任务"+"<font color='#FC3C3C'>24</font>"
     * rlScienceAnalysis : 学情分析
     * rlMyClass : 我的班级
     * rlCorrecting : 批改
     * rlCexplore : 合作探究
     * llBaiduRncyclopedia : 百度百科
     */

    private String tvDynamicTitle;
    private String llMyCurriculum;
    private String llAutonomyStudy;
    private String llClassroomTeaching;
    private String llTask;
    private String rlExamination;
    private String rlScienceAnalysis;
    private String rlMyClass;
    private String rlCorrecting;
    private String rlCexplore;
    private String llBaiduRncyclopedia;

    public String getTvDynamicTitle() {
        return tvDynamicTitle;
    }

    public void setTvDynamicTitle(String tvDynamicTitle) {
        this.tvDynamicTitle = tvDynamicTitle;
    }

    public String getLlMyCurriculum() {
        return llMyCurriculum;
    }

    public void setLlMyCurriculum(String llMyCurriculum) {
        this.llMyCurriculum = llMyCurriculum;
    }

    public String getLlAutonomyStudy() {
        return llAutonomyStudy;
    }

    public void setLlAutonomyStudy(String llAutonomyStudy) {
        this.llAutonomyStudy = llAutonomyStudy;
    }

    public String getLlClassroomTeaching() {
        return llClassroomTeaching;
    }

    public void setLlClassroomTeaching(String llClassroomTeaching) {
        this.llClassroomTeaching = llClassroomTeaching;
    }

    public String getLlTask() {
        return llTask;
    }

    public void setLlTask(String llTask) {
        this.llTask = llTask;
    }

    public String getRlExamination() {
        return rlExamination;
    }

    public void setRlExamination(String rlExamination) {
        this.rlExamination = rlExamination;
    }

    public String getRlScienceAnalysis() {
        return rlScienceAnalysis;
    }

    public void setRlScienceAnalysis(String rlScienceAnalysis) {
        this.rlScienceAnalysis = rlScienceAnalysis;
    }

    public String getRlMyClass() {
        return rlMyClass;
    }

    public void setRlMyClass(String rlMyClass) {
        this.rlMyClass = rlMyClass;
    }

    public String getRlCorrecting() {
        return rlCorrecting;
    }

    public void setRlCorrecting(String rlCorrecting) {
        this.rlCorrecting = rlCorrecting;
    }

    public String getRlCexplore() {
        return rlCexplore;
    }

    public void setRlCexplore(String rlCexplore) {
        this.rlCexplore = rlCexplore;
    }

    public String getLlBaiduRncyclopedia() {
        return llBaiduRncyclopedia;
    }

    public void setLlBaiduRncyclopedia(String llBaiduRncyclopedia) {
        this.llBaiduRncyclopedia = llBaiduRncyclopedia;
    }
}
