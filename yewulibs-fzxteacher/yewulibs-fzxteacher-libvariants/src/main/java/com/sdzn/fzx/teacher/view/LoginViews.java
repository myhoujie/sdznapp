package com.sdzn.fzx.teacher.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.vo.InClassStatus;
import com.sdzn.fzx.teacher.vo.LoginBean;

/**
 登录  view
 */
public interface LoginViews extends IView {
    void loginSuccess(LoginBean.DataBean vo);

    void loginFailed(String msg);

    void inClassStatusResult(InClassStatus vo);
}
