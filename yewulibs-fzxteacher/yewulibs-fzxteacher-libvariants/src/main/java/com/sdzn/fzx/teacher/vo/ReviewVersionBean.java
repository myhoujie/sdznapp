package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * ReviewVersionBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ReviewVersionBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * createAccountId : 1
         * createUserName : zcq
         * createTime : 1516260945000
         * userTeacherId : 2
         * userTeacherName : 王老师
         * baseGradeId : 3
         * baseGradeName : 三年级
         * reviewType : 1
         * name : null
         * reviewTypeName : 上学期期中复习
         * status : true
         */

        private String id;
        private int createAccountId;
        private String createUserName;
        private long createTime;
        private int userTeacherId;
        private String userTeacherName;
        private String baseGradeId;
        private String baseGradeName = "";
        private String reviewType;
        private Object name;
        private String reviewTypeName;
        private boolean status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(int createAccountId) {
            this.createAccountId = createAccountId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getUserTeacherId() {
            return userTeacherId;
        }

        public void setUserTeacherId(int userTeacherId) {
            this.userTeacherId = userTeacherId;
        }

        public String getUserTeacherName() {
            return userTeacherName;
        }

        public void setUserTeacherName(String userTeacherName) {
            this.userTeacherName = userTeacherName;
        }

        public String getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(String baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public String getReviewType() {
            return reviewType;
        }

        public void setReviewType(String reviewType) {
            this.reviewType = reviewType;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getReviewTypeName() {
            return reviewTypeName;
        }

        public void setReviewTypeName(String reviewTypeName) {
            this.reviewTypeName = reviewTypeName;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }
    }
}
