package com.sdzn.fzx.teacher.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/2/21
 * 修改单号：
 * 修改内容:
 */
public class GroupStudent {
    /**
     * total : 5
     * data : [{"id":-1,"groupName":"全部人员","classId":604,"className":null,"createTime":null,"createAccountId":null,"createUserName":null,"classStudentCount":10},{"id":0,"groupName":"未分组","classId":604,"className":null,"createTime":null,"createAccountId":null,"createUserName":null,"classStudentCount":10},{"id":253,"groupName":"测试1组","classId":604,"className":"十一班","createTime":1543394235000,"createAccountId":5024,"createUserName":"黄东月","classStudentCount":0},{"id":254,"groupName":"测试2组","classId":604,"className":"十一班","createTime":1543394243000,"createAccountId":5024,"createUserName":"黄东月","classStudentCount":0},{"id":255,"groupName":"测试3组","classId":604,"className":"十一班","createTime":1543394327000,"createAccountId":5024,"createUserName":"黄东月","classStudentCount":0}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : -1
         * groupName : 全部人员
         * classId : 604
         * className : null
         * createTime : null
         * createAccountId : null
         * createUserName : null
         * classStudentCount : 10
         */

        private int id;
        private String groupName;
        private int classId;
        private Object className;
        private Object createTime;
        private Object createAccountId;
        private Object createUserName;
        private int classStudentCount;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public Object getClassName() {
            return className;
        }

        public void setClassName(Object className) {
            this.className = className;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(Object createAccountId) {
            this.createAccountId = createAccountId;
        }

        public Object getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(Object createUserName) {
            this.createUserName = createUserName;
        }

        public int getClassStudentCount() {
            return classStudentCount;
        }

        public void setClassStudentCount(int classStudentCount) {
            this.classStudentCount = classStudentCount;
        }
    }
}
