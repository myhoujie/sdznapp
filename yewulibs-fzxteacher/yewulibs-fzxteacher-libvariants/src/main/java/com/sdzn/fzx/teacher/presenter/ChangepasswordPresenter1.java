package com.sdzn.fzx.teacher.presenter;


import com.sdzn.fzx.teacher.view.ChangepasswordViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.api.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class ChangepasswordPresenter1 extends Presenter<ChangepasswordViews> {
    //"Bearer "+(String) SPUtils.getInstance().getString("token", "")
    // UserController.getLoginBean().getData().getUser().getId()
    public void changePassword(String token,String id,String oldPassword,String newPassword) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .updatePassword(token,id, oldPassword,newPassword)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        getView().changePassswordSuccess();

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        Log.d("密码修改","22222"+ call.toString());
                        getView().onFailed("密码修改失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });


    }
}
