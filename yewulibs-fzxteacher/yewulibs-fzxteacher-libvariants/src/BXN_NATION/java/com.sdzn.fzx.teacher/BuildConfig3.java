package com.sdzn.fzx.teacher;

public class BuildConfig3 {
    //    public static final VersionInfo versionInfoConfig = VersionInfo.OOO;
//    public static final String versionNameConfig = VersionChoose.getVersionNameString(VersionChoose.OOO);
    public static final String versionNameConfig = UrlManager.OOO;

    public static final String  SERVER_ISERVICE = UrlManager.OL_SERVER_ISERVICE;
//    public static final String SERVER_ISERVICE2 = UrlManager.OL_SERVER_ISERVICE2;


    public static final String FLAVOR = UrlManager.FLAVOR3;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE3;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME3;
    public static final String AUTH = UrlManager.AUTH3;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS3;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL3;
    public static final String DOWN = UrlManager.DOWN3;
    public static final String MQTT = UrlManager.MQTT3;
    public static final int OFFICE_ID = UrlManager.OFFICE_ID3;
    public static final String PORT = UrlManager.PORT3;
    public static final int RABBIT_HOST = UrlManager.RABBIT_HOST3;
    public static final String RABBIT_MQ = UrlManager.RABBIT_MQ3;
    public static final String RABBIT_NAME = UrlManager.RABBIT_NAME3;
    public static final String RABBIT_PASSWORD = UrlManager.RABBIT_PASSWORD3;
    public static final String APPLICATION_ID = UrlManager.APPLICATION_ID3;
    // Fields from default config.
    public static final int TINKER_VERSION = UrlManager.TINKER_VERSION3;

}
