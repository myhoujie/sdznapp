package com.example.app5xztl.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.ImageSlideBean;
import com.example.app5xztl.presenter.ImageSlidePresenter;
import com.example.app5libbase.util.chatroom.GlideHelper;
import com.example.app5xztl.view.ImageSlideView;
import com.example.app5libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CustomDialog;

import java.util.ArrayList;

/**
 * Created by admin on 2019/8/15.
 */

public class ImageSlideActivity extends MBaseActivity<ImageSlidePresenter> implements ImageSlideView, View.OnClickListener {

    //图片下标序号
    private int count = 0;
    private int num = 0;
    //定义手势监听对象
    private GestureDetector gestureDetector;
    private boolean isBarDis = false;

    private ImageView iv;
    private LinearLayout llBar;
    private ImageView ivBack;
    private TextView tvName;
    private TextView tvSelectNum;
    private ImageView ivDelete;

    ArrayList<ImageSlideBean> picsBeanList;

    @Override
    public void initPresenter() {
        mPresenter = new ImageSlidePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide);


        iv = (ImageView) findViewById(R.id.imageView);
        llBar = (LinearLayout) findViewById(R.id.ll_bar);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvSelectNum = (TextView) findViewById(R.id.tv_select_num);
        ivDelete = (ImageView) findViewById(R.id.iv_delete);


        Bundle bundle = getIntent().getExtras();
        picsBeanList = bundle.getParcelableArrayList("pics");
        count = bundle.getInt("position", 0);
        if (picsBeanList != null && picsBeanList.size() > 0) {
            initData();
            initView();
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入ImageSlideActivity成功");
                }
            }
        }
    }

    @Override
    protected void initData() {//
        num = picsBeanList.size();

    }

    @Override
    protected void initView() {
        ivBack.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        gestureDetector = new GestureDetector(onGestureListener);   //设置手势监听由onGestureListener处理

        //初始化
        GlideHelper.load(this, picsBeanList.get(count).getmPic(), iv);
//        if (picsBeanList.get(count).getmGroupId().equals(UserController.getLoginBean().getData().getUser().get())){
//            ivDelete.setVisibility(View.VISIBLE);
//        }
        tvSelectNum.setText((count + 1) + "/" + num);
    }


    //当Activity被触摸时回调
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return gestureDetector.onTouchEvent(event);
    }

    //自定义GestureDetector的手势识别监听器
    private GestureDetector.OnGestureListener onGestureListener
            = new GestureDetector.SimpleOnGestureListener() {
        //当识别的手势是滑动手势时回调onFinger方法
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            //得到手触碰位置的起始点和结束点坐标 x , y ，并进行计算
            float x = e2.getX() - e1.getX();
            float y = e2.getY() - e1.getY();
            //通过计算判断是向左还是向右滑动
            if (x > 0) {
                count--;
            } else if (x < 0) {
                count++;
            }
            if (count == -1) {//
                count = picsBeanList.size() - 1;
            }
            if (count > picsBeanList.size() - 1) {
                count = 0;
            }

            GlideHelper.load(ImageSlideActivity.this, picsBeanList.get(count).getmPic(), iv);
            tvSelectNum.setText((count + 1) + "/" + num);
            //再次根据字段判断是否是自己  .get(count).     可删除   ivDelete.setVisibility(View.VISIBLE);  invisible
//            if (picsBeanList.get(count).getmGroupId().equals(UserController.getLoginBean().getData().getUser().getClassGroupId())){
//                ivDelete.setVisibility(View.VISIBLE);
//            }else {
//                ivDelete.setVisibility(View.INVISIBLE);
//            }

            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (isBarDis) {
                llBar.setVisibility(View.VISIBLE);
                isBarDis = false;
            } else {
                llBar.setVisibility(View.GONE);
                isBarDis = true;
            }
            return true;
        }
    };

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.iv_back) {
            ImageSlideActivity.this.finish();
        } else if (id == R.id.iv_delete) {
            String msg = "确认删除自己的作答结果？";
            CustomDialog.Builder builder = new CustomDialog.Builder(ImageSlideActivity.this);
            builder.setMessage(msg);
            builder.setPositive("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mPresenter.getDelete(picsBeanList.get(count).getId());

                }
            });
            builder.setNegative("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            CustomDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void getDeleteResult() {
        finish();
    }

    @Override
    public void networkError(String s) {
        ToastUtil.showShortlToast(s);
        finish();
    }
}
