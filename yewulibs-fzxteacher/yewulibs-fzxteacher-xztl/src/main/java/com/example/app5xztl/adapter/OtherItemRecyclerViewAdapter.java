package com.example.app5xztl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.example.app5libbase.util.chatroom.GlideHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2019/8/15.
 */
public class OtherItemRecyclerViewAdapter extends
        RecyclerView.Adapter<OtherItemRecyclerViewAdapter.MyViewHolder> implements View.OnClickListener {

    private int mInt;
    private Context mContext;
    private List<ChatOtherBean.DataBean.TeachGroupChatResultPicsBean> mHomes;


    //新建一个私有变量用于保存用户设置的监听器
    private OnItemClickListener mOnItemClickListener = null;

    //set方法：
    public void setOnItemClickListener(Context mContext, OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
        this.mContext = mContext;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(v, (int) v.getTag(), mHomes);//mHomes.get((int)v.getTag())   具体某一个

        }
    }

    //define interface 自定义一个接口
    public static interface OnItemClickListener {
        void onItemClick(View view, int position, List<ChatOtherBean.DataBean.TeachGroupChatResultPicsBean> str);
    }

    public OtherItemRecyclerViewAdapter(int pI, List<ChatOtherBean.DataBean.TeachGroupChatResultPicsBean> picsBeans) {
        mHomes = new ArrayList<>();
        mHomes.clear();
        if (picsBeans.size() > 0) {
            mHomes.addAll(picsBeans);
        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler_i,null);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler_i, parent, false);
        //将创建的View注册点击事件
        view.setOnClickListener(this);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tv_name.setText(mHomes.get(position).getUserStudentName());
        GlideHelper.load(mContext, mHomes.get(position).getPicUrl(), holder.imageView);
//        GlideHelper.load(true,mContext,"", holder.iv_head);
        //将position保存在itemView的Tag中，以便点击时进行获取
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mHomes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        ImageView imageView;
        ImageView iv_head;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            iv_head = (ImageView) itemView.findViewById(R.id.iv_head);
        }
    }
}
