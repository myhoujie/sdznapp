package com.example.app5xztl.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.ChatOtherBean;
import com.sdzn.fzx.teacher.vo.chatroom.GroupListBean;
import com.sdzn.fzx.teacher.vo.chatroom.Home;
import com.sdzn.fzx.teacher.vo.chatroom.ImageSlideBean;
import com.example.app5libbase.listener.chatroom.OtherScoreListener;
import com.example.app5libbase.util.chatroom.SpacesItemThreeDecoration;
import com.example.app5libbase.views.CustomGridView;
import com.example.app5libbase.views.NoScrollGridView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OtherRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ChatOtherBean.DataBean> mDatas;
    private ItemViewHolder holder;
    private OtherItemRecyclerViewAdapter mOtherItemRecyclerViewAdapter;
    List<GroupListBean.DataBean.TeachGroupChatContentPicsBean> mPics;
    private String mTitle, mContent, mEndTime,mChapterName;
    private int mState;
    private  EllListGridViewAdapter ellGridViewAdapter;

    //Type
    private int TYPE_HEADER = 1001;
    private int TYPE_ONE = 1002;
    private int TYPE_BOTTOM = 1003;

    public OtherRecyclerViewAdapter(Context pContext, List<ChatOtherBean.DataBean> data) {
        this.mContext = pContext;
        this.mDatas = data;
    }

    public OtherRecyclerViewAdapter(Context pContext, String title, String content, String endTime, int state, List<GroupListBean.DataBean.TeachGroupChatContentPicsBean> pics,String chapterName,List<ChatOtherBean.DataBean> data) {
        this.mContext = pContext;
        this.mTitle = title;
        this.mContent = content;
        this.mEndTime = endTime;
        this.mState = state;
        this.mPics = pics;
        this.mDatas = data;
        this.mChapterName = chapterName;
    }

    public void setListData(List<ChatOtherBean.DataBean> data) {
        this.mDatas = data;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvClass1;
        TextView mTextView;
        RecyclerView mRecyclerView;
        CustomGridView customGridView;
        LinearLayout llContent;

        ItemViewHolder(View itemView) {
            super(itemView);
            tvClass1 = (TextView) itemView.findViewById(R.id.tvClass1);
            mTextView = (TextView) itemView.findViewById(R.id.tv_name);
            mRecyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            customGridView = (CustomGridView) itemView.findViewById(R.id.gridView);
            llContent = itemView.findViewById(R.id.ll_content);
        }
    }

    /**
     * 头布局的viewholder
     */
    class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvContent;
        TextView tvDate;
        TextView tvStatus;
        TextView tvChapterName;
        NoScrollGridView gridView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvContent = (TextView) itemView.findViewById(R.id.tv_content);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvChapterName = (TextView) itemView.findViewById(R.id.tv_chapter_name);
            gridView = (NoScrollGridView) itemView.findViewById(R.id.gridView);
        }
    }
    class BottomViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;
        public BottomViewHolder(View itemView){
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
        }
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.discuss_item_other_top, parent, false);
            return new HeaderViewHolder(headerView);
        }else if (viewType==TYPE_ONE){
            View eView=LayoutInflater.from(parent.getContext()).inflate(R.layout.discuss_item_other_empty, parent, false);
            return new EmptyViewHolder(eView);
        }else if (viewType==TYPE_BOTTOM){
            View bView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_discussion_perch, parent, false);
            return new BottomViewHolder(bView);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler, parent, false);
        //holder = new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler, parent, false));
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            if (mDatas.get(position-1).getTeachGroupChatResultPics().size() > 0) {
                ((ItemViewHolder) holder).mTextView.setText(mDatas.get(position - 1).getTeachGroupChatResultPics().get(0).getGroupName() + "");

                ((ItemViewHolder) holder).mRecyclerView.setHasFixedSize(true);
//                RecyclerView.ItemDecoration itemDecoration = ((ItemViewHolder) holder).mRecyclerView.getItemDecorationAt(0);
//                if (itemDecoration == null) {
                    ((ItemViewHolder) holder).mRecyclerView.addItemDecoration(new SpacesItemThreeDecoration(8));
//                }

                ((ItemViewHolder) holder).mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                mOtherItemRecyclerViewAdapter = new OtherItemRecyclerViewAdapter(position - 1, mDatas.get(position - 1).getTeachGroupChatResultPics());
                ((ItemViewHolder) holder).mRecyclerView.setAdapter(mOtherItemRecyclerViewAdapter);
                drawRecyclerView();

                //
                OtherGridAdapter gridAdapter = new OtherGridAdapter(mContext);
                List<Home> list = new ArrayList<>();
                for (int i = 0; i < mDatas.get(position - 1).getTeachGroupChatResultPics().size(); i++) {
                    list.add(new Home(mDatas.get(position - 1).getTeachGroupChatResultPics().get(i).getUserStudentName(), ""));
                }
                Set<Home> ts = new HashSet<Home>();
                ts.addAll(list);
                list.clear();
                for (Home home : ts) {
                    list.add(home);
                }
                gridAdapter.setListData(list);
                ((ItemViewHolder) holder).customGridView.setNumColumns(15);

                ((ItemViewHolder) holder).customGridView.setAdapter(gridAdapter);
                ((ItemViewHolder) holder).tvClass1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((OtherScoreListener) mContext).onMyScore(view, String.valueOf(mDatas.get(position - 1).getId()));
                    }
                });
                if (mDatas.get(position - 1).getScore() > -1) {
                    ((ItemViewHolder) holder).tvClass1.setText("" + mDatas.get(position - 1).getScore()+" 分");
                } else {
                    ((ItemViewHolder) holder).tvClass1.setText("未打分");
                }
            }

        } else if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).tvTitle.setText(mTitle);
            ((HeaderViewHolder) holder).tvContent.setText(mContent==null?"":mContent);
            ((HeaderViewHolder) holder).tvDate.setText("截止时间：" + mEndTime);
            ((HeaderViewHolder) holder).tvChapterName.setText(mChapterName);


            if (mState == 0) {//进行
                ((HeaderViewHolder) holder).tvStatus.setVisibility(View.VISIBLE);
            } else {
                ((HeaderViewHolder) holder).tvStatus.setVisibility(View.GONE);
            }
            if (mPics.size() > 0) {
                ellGridViewAdapter = new EllListGridViewAdapter(mContext, mPics);
                ((HeaderViewHolder) holder).gridView.setAdapter(ellGridViewAdapter);

            }

        }else if (holder instanceof BottomViewHolder){

        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }else if (position==mDatas.size()+1){
            return TYPE_BOTTOM;
        }else if (mDatas.get(position-1).getTeachGroupChatResultPics().size()<1&&mState==0){
            return TYPE_ONE;
        }
        return super.getItemViewType(position);
    }

    /**
     * RecyclerView 内层点击事件方法
     */
    private void drawRecyclerView() {
        //RecyclerView点击事件
        mOtherItemRecyclerViewAdapter.setOnItemClickListener(mContext, new OtherItemRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, List<ChatOtherBean.DataBean.TeachGroupChatResultPicsBean> picList) {
//                Toast.makeText(mContext, "" + position + "str--" + str, Toast.LENGTH_SHORT).show();
                final ArrayList<ImageSlideBean> picsBeanList = new ArrayList<>();
                for (int i = 0; i < picList.size(); i++) {
                    picsBeanList.add(new ImageSlideBean(picList.get(i).getId(),
                            picList.get(i).getPicUrl(), "0"));
                }

                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ImageSlideActivity");
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putParcelableArrayList("pics", picsBeanList);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }
}