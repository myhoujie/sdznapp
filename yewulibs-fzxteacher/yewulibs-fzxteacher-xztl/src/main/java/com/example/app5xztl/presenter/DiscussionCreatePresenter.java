package com.example.app5xztl.presenter;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.FileProvider;

import com.donkingliang.labels.LabelsView;
import com.example.app5libbase.R;
import com.example.app5libbase.listener.chatroom.AlbumOrCameraListener;
import com.sdzn.fzx.teacher.vo.chatroom.ImageUploadInfoBean;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.controller.UserController;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5xztl.view.DiscussionCreateView;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.QiniuUptoken;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.utils.UCropUtils;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 讨论 新建
 */
public class DiscussionCreatePresenter extends BasePresenter<DiscussionCreateView, BaseActivity> {

    private String HostUrl = "";
    public static final int REQ_CODE_CAMERA = 1003;

    public void createGroupDiscussion(Map<String, Object> param) {
        if (getBaseVolumeId() == null || getBaseVolumeId().isEmpty()) {
            ToastUtil.showShortlToast("请选择册别");
        } else if (getChapterNodeIdPath() == null || getChapterNodeIdPath().isEmpty()) {
            ToastUtil.showShortlToast("请选择章节");
        } else {
            param.put("baseVolumeId", getBaseVolumeId());
            param.put("baseVolumeName", getBaseVolumeName());
            param.put("chapterId", getChapterNodeIdPath());
            param.put("chapterName", getChapterNodeName());//getNodeNamePath
            Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                    .publishGroupDiscussion(param)
                    .map(new StatusFunc<Object>())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof ApiException) {
                                ApiException apiException = (ApiException) e;
                                StatusVo status = apiException.getStatus();
                                if (status != null && status.getMsg() != null) {
                                    if (status.getCode() == 24015) {
                                        mView.onTitleFailed("发布聊天室失败");
                                    } else {
                                        mView.onTitleFailed(status.getMsg());
                                    }
                                } else {
                                    mView.onTitleFailed(e.toString());
                                }
                            }

                        }

                        @Override
                        public void onNext(Object o) {
                            mView.createSuccess();

                        }
                    }, mActivity, true, false, false, ""));
        }

    }

    public void getClassGroupList(final String classId, final String className) {

        Network.createTokenService(NetWorkService.GetClassGroupingService.class)
                .GetClassGrouping(classId)
                .map(new StatusFunc<ClassGroupingVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ClassGroupingVo>(new SubscriberListener<ClassGroupingVo>() {
                    @Override
                    public void onNext(ClassGroupingVo classGroupingVo) {
                        mView.setClassGropingData(classGroupingVo, classId, className);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void getClassList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getAllClassList()
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }

    public void bindHolder(int rvHeight, final GeneralRecyclerViewHolder holder, final DiscussionClassGroup classGroup, int position) {
//        group_item; labels;
        final LabelsView labelsView = holder.getChildView(R.id.labels);
        final LabelsView group_item = holder.getChildView(R.id.group_item);

        final List<Integer> positions = new ArrayList<>();
        final List<String> myDataList = new ArrayList<>();
        final List<String> myDataListName = new ArrayList<>();

        int aInt = 0;
        for (int i = 0; i < classGroup.getDataBList().size(); i++) {
            if (!myDataList.contains(classGroup.getDataBList().get(i).getGroupId())) {
                myDataList.add(classGroup.getDataBList().get(i).getGroupId());
                myDataListName.add(classGroup.getDataBList().get(i).getGroupName());
                positions.add(aInt);
                aInt++;
            }
        }


        labelsView.setLabels(myDataListName, new LabelsView.LabelTextProvider<String>() {
            @Override
            public CharSequence getLabelText(TextView label, int position, String data) {
                return data;
            }
        });
        labelsView.clearAllSelect();
        labelsView.setSelectType(LabelsView.SelectType.MULTI);
        labelsView.setMaxSelect(0);
        labelsView.setMinSelect(0);


        ArrayList<String> group = new ArrayList<>();
        group.add(classGroup.getClassName());
        group_item.setLabels(group);

        labelsView.setOnLabelClickListener(new LabelsView.OnLabelClickListener() {
            @Override
            public void onLabelClick(TextView label, Object data, int position) {
                //label是被点击的标签，data是标签所对应的数据，position是标签的位置。
                if (labelsView.getSelectLabels().size() > 0) {//存数据
                    group_item.setSelects(0);
                } else {
                    group_item.clearAllSelect();
                }

                mView.getClassGroupInfo(classGroup.getClassId(), getGroupsId(labelsView.getSelectLabels(), myDataList));

            }
        });

        group_item.setOnLabelSelectChangeListener(new LabelsView.OnLabelSelectChangeListener() {
            @Override
            public void onLabelSelectChange(TextView label, Object data, boolean isSelect, int position) {
                if (isSelect) {
                    if (labelsView.getSelectLabels().size() == 0) {
                        labelsView.setSelects(positions);
                    }
                } else {
                    labelsView.clearAllSelect();
                }
                mView.getClassGroupInfo(classGroup.getClassId(), getGroupsId(labelsView.getSelectLabels(), myDataList));
            }
        });


    }

    private List<String> getGroupsId(List<Integer> pos, List<String> myDataList) {
        List<String> myList = new ArrayList<>();
        for (int a = 0; a < pos.size(); a++) {
            myList.add(myDataList.get(pos.get(a)));
        }
        return myList;
    }

    public void uploadSubjectivePhoto(final List<Photo> photoLists) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
                String key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
                        throw new RuntimeException("上传失败: 获取upToken失败");
                    }
                    QiniuUptoken.ResultBean bean = uptoken.getResult();
                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        mView.networkError(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create(bean.getDomain(), key, bean.getImageStyle());

                                    uploadPicVoList.add(dataBean);

                                    if (uploadPicVoList.size() == photoLists.size()) {
                                        pdm.cancelWaiteDialog();
                                        UploadPicVo mUploadPicVo = new UploadPicVo();
                                        mUploadPicVo.setData(uploadPicVoList);
                                        mView.onUploadPicSuccess(mUploadPicVo);
                                    }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    mView.networkError(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    private File takeImageFile;

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard()) {
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            } else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(mActivity, BuildConfig3.APPLICATION_ID + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mActivity.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        mActivity.startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {

        } else {
            String imagePath = rain.coder.photopicker.utils.ImageUtils.getImagePath(mActivity, "/Crop/");
            File corpFile = new File(imagePath + rain.coder.photopicker.utils.ImageUtils.createFile());
            UCropUtils.start(mActivity, new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    /**
     * 发送 图片
     */
    public void showSelectImgDialog(final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera_open, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getVersionList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<VersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<VersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed(e.toString());
                    }

                    @Override
                    public void onNext(VersionBean versionBean) {
                        dealVersionBean(versionBean);
                    }
                });

    }

    public void getNodeList(final String baseVersion) {
        if (TextUtils.isEmpty(baseVersion)) {
            mView.onNodeSuccessed(null);
            return;
        }

        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        final String baseLevelId = SPUtils.getLoginBean().getData().getUser().getBaseLevelId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getNodeList(subject, baseLevelId, getBaseGradeId(), baseVersion, getBaseVolumeId())
                .map(new StatusFunc<NodeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NodeBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed(e.toString());
                    }

                    @Override
                    public void onNext(NodeBean o) {
                        mView.onNodeSuccessed(o.getData());
                    }
                });

    }

    void dealVersionBean(VersionBean versionBean) {

        if (versionBean == null) {
            versionBean = new VersionBean();
        }
        if (versionBean.getData() == null) {
            versionBean.setData(new ArrayList<VersionBean.DataBean>());

            final ArrayList<VersionBean.DataBean.VolumeListBean> volumeListBeans = new ArrayList<>();

            final VersionBean.DataBean dataBean = new VersionBean.DataBean();
            dataBean.setVolumeList(volumeListBeans);

            versionBean.getData().add(dataBean);
        }

        VersionBean.DataBean.VolumeListBean volumeListBean = new VersionBean.DataBean.VolumeListBean();
        volumeListBean.setBaseVersionName("全部册别");
        volumeListBean.setBaseGradeName("");
        volumeListBean.setBaseVolumeName("");
        versionBean.getData().get(0).getVolumeList().add(0, volumeListBean);

        ArrayList<VersionBean.DataBean.VolumeListBean> retList = new ArrayList<>();

        for (int i = 0; i < versionBean.getData().size(); i++) {
            retList.addAll(versionBean.getData().get(i).getVolumeList());
        }

        mView.onVersionSuccessed(retList);
    }


    private String dealNullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;

    }

    private int getNodePointCount(String str) {
        final String newStr = str.replace(".", "");
        return str.length() - newStr.length();

    }

    public String getBaseGradeId() {
        return dealNullStr(baseGradeId);
    }

    public void setBaseGradeId(String baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseVolumeId() {
        return dealNullStr(baseVolumeId);
    }

    public void setBaseVolumeId(String baseVolumeId) {

        this.baseVolumeId = baseVolumeId;
    }

    public String getChapterNodeIdPath() {
        return dealNullStr(chapterNodeIdPath);
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getBaseVolumeName() {
        return baseVolumeName;
    }

    public void setBaseVolumeName(String baseVolumeName) {
        this.baseVolumeName = baseVolumeName;
    }

    public String getChapterNodeName() {
        return chapterNodeName;
    }

    public void setChapterNodeName(String chapterNodeName) {
        this.chapterNodeName = chapterNodeName;
    }

    private String baseGradeId;
    private String baseVolumeId = "";
    private String baseVolumeName = "";
    private String chapterNodeIdPath = "";
    private String chapterNodeName = "";

}