package com.example.app5xztl.view;


import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * View  GroupDiscussionFragment
 *
 * @author zhaosen
 */
public interface GroupDiscussionView extends BaseView {

    void networkError(String msg);

    void onVersionSuccessed(ArrayList<VersionBean.DataBean.VolumeListBean> list);

    void onNodeSuccessed(List<NodeBean.DataBeanXX> list);

    void onTitleFailed();

    void onClassonError();

    void onClassSuccessed(SyncClassVo o);
}
