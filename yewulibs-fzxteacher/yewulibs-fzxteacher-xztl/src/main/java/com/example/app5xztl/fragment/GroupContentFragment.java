package com.example.app5xztl.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.app5libbase.R;
import com.example.app5xztl.adapter.PagerAdapter;
import com.example.app5libbase.listener.chatroom.OnGroupListener;
import com.example.app5libbase.listener.chatroom.OnPageChangeListener;
import com.example.app5libbase.base.BaseFragment;

import org.xutils.x;

/**
 * 小组讨论内容
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class GroupContentFragment extends BaseFragment implements OnGroupListener {

    ViewPager viewPager;

    private PagerAdapter taskPagerAdapter;      //Adapter 可保留

    private OnPageChangeListener onPageChangeListener;

    private int status;

    public static GroupContentFragment newInstance(Bundle bundle) {
        GroupContentFragment fragment = new GroupContentFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_content, container, false);
        x.view().inject(this, rootView);
        viewPager = rootView.findViewById(R.id.group_pager);
        initView();
        return rootView;
    }

    private void initView() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelected(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onSearch(String searchStr) {
//        GroupListFragment groupListFragment = (GroupListFragment) taskPagerAdapter.getItem(viewPager.getCurrentItem());
//        groupListFragment.search(searchStr);
    }

    @Override
    public void onVersionId(String version) {

    }

    @Override
    public void onNodeId(String node) {

    }


    private void setSelected(int pos) {
        if (onPageChangeListener != null) {
            onPageChangeListener.onPageChange(pos);
        }
    }

    /**
     * 可在此处更改成  选择章节
     */
//    @Override
//    public void onSearch(String searchStr) {
//        TaskListFragment taskListFragment = (TaskListFragment) taskPagerAdapter.getItem(viewPager.getCurrentItem());
//        taskListFragment.search(searchStr);
//    }
//
//    @Override
//    public void onStatusChanged(int status) {
//        if (taskPagerAdapter != null) {
//            TaskListFragment taskListFragment = (TaskListFragment) taskPagerAdapter.getItem(viewPager.getCurrentItem());
//            taskListFragment.setStatus(status);
//        } else {
//            this.status = status;
//        }
//    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }
}
