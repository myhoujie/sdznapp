package com.example.app5xztl.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.event.FullImageEvBus;
import com.example.app5libbase.listener.chatroom.AlbumOrCameraListener;
import com.example.app5libbase.listener.chatroom.DiscussionDeleteImage;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.util.GlideImageLoader;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libbase.views.SpinnerView;
import com.example.app5libbase.views.VersionSpinner;
import com.example.app5xztl.adapter.NewImageGridViewAdapter;
import com.example.app5xztl.presenter.DiscussionNewPresenter;
import com.example.app5xztl.view.DiscussionNewView;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;

import static com.example.app5xztl.presenter.DiscussionNewPresenter.REQ_CODE_CAMERA;

/**
 * 小组讨论  内容新建保存（课程 - 发起讨论）
 *
 * @author zs
 */

public class DiscussionNewActivity extends MBaseActivity<DiscussionNewPresenter> implements DiscussionNewView,
        View.OnClickListener, DiscussionDeleteImage, EasyPermissions.PermissionCallbacks {
    private RelativeLayout rl;
    private TextView tvBack;
    private TextView tvSave;
    private View view;
    private RelativeLayout head;
    private TextView tvMaterials;
    private TextView versionText;
    private View line;
    private TextView nodeText;
    private TextView tvTitleNum;
    private EditText etTitle;
    private TextView tvTextTitle;
    private TextView tvContentNum;
    private EditText etContent;
    private TextView tvTextContent;
    private NoScrollGridView gridView;
    private SpinnerView spinnerView;
    private VersionSpinner versionSpinnerView;

    NewImageGridViewAdapter gridViewAdapter;
    final List<String> mDatas = new ArrayList<>();
    private String lessonId = "";
    private String volumeName, volumeId, chapterName, chapterId;
    private boolean isEdit = false;
    private String title, content;
    private ArrayList<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> pics;
    private static final int REQUECT_CODE_CAMERA = 1100;
    private int deletePos;
    private int numSelect = 6;

    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Dialog dialog;

    @Override
    public void initPresenter() {
        mPresenter = new DiscussionNewPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_new);
        dialog = new Dialog(DiscussionNewActivity.this, R.style.notice_dialog);
        rl = (RelativeLayout) findViewById(R.id.rl);
        tvBack = (TextView) findViewById(R.id.tv_back);
        tvSave = (TextView) findViewById(R.id.tv_save);
        view = (View) findViewById(R.id.view);
        head = (RelativeLayout) findViewById(R.id.head);
        tvMaterials = (TextView) findViewById(R.id.tv_materials);
        versionText = (TextView) findViewById(R.id.version_text);
        line = (View) findViewById(R.id.line);
        nodeText = (TextView) findViewById(R.id.node_text);
        tvTitleNum = (TextView) findViewById(R.id.tv_title_num);
        etTitle = (EditText) findViewById(R.id.et_title);
        tvTextTitle = (TextView) findViewById(R.id.tv_text_title);
        tvContentNum = (TextView) findViewById(R.id.tv_content_num);
        etContent = (EditText) findViewById(R.id.et_content);
        tvTextContent = (TextView) findViewById(R.id.tv_text_content);
        gridView = (NoScrollGridView) findViewById(R.id.gridView);
        spinnerView = (SpinnerView) findViewById(R.id.spinner_view);
        versionSpinnerView = (VersionSpinner) findViewById(R.id.version_spinner_view);

        EventBus.getDefault().register(this);
        lessonId = getIntent().getStringExtra("lessonId");
        volumeName = getIntent().getStringExtra("volumeName");
        volumeId = getIntent().getStringExtra("volumeId");
        chapterName = getIntent().getStringExtra("chapterName");
        chapterId = getIntent().getStringExtra("chapterId");
        isEdit = getIntent().getBooleanExtra("isEdit", false);
        if (isEdit) {
            Bundle bundle = getIntent().getExtras();
            title = bundle.getString("title");
            content = bundle.getString("content");
            pics = bundle.getParcelableArrayList("pic");
            numSelect = 6 - pics.size();
        }

        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入DiscussionNewActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        if (isEdit) {
            for (int i = 0; i < pics.size(); i++) {
                mDatas.add(pics.get(i).getPicUrl());
            }
            if (gridViewAdapter != null) gridViewAdapter.notifyDataSetChanged();
            etTitle.setText(title);
            etContent.setText(content);
        }

        versionText.setText(volumeName);
        nodeText.setText(chapterName);

        versionText.setOnClickListener(this);
        nodeText.setOnClickListener(this);
        tvBack.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        etTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvTitleNum.setText(String.valueOf(150 - charSequence.length()) + "字");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvContentNum.setText(String.valueOf(500 - charSequence.length()) + "字");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    protected void initData() {
        gridViewAdapter = new NewImageGridViewAdapter(this, mDatas);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == adapterView.getChildCount() - 1) {//添加
                    mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
                        @Override
                        public void selectAlbum() {
                            if (numSelect == 0) {
                                ToastUtil.showShortlToast("图片最多选6张");
                            } else {
                                new PhotoPickConfig.Builder(mPresenter.mActivity)
                                        .imageLoader(new GlideImageLoader())
                                        .showCamera(false)
                                        .maxPickSize(numSelect)
                                        .spanCount(8)
                                        .clipPhoto(false)
                                        .build();
                            }
                        }

                        @Override
                        public void selectCamera() {
                            requestPermissiontest();
                        }
                    });
                } else {
                    deletePos = i;
                    Intent intentImage = new Intent(AppUtils.getAppPackageName() + ".hs.act.FullImageActivity");
                    intentImage.putExtra("photoUrl", mDatas.get(i));
                    intentImage.putExtra("showDel", true);
                    startActivity(intentImage);
                }

            }
        });

    }


    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }


    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) {

        for (int a = 0; a < uploadVos.getData().size(); a++) {
            mDatas.add(uploadVos.getData().get(a).getOriginalPath());
            if (mDatas.size() > 6) {
                ToastUtil.showShortlToast("图片最多选6张");
                for (int i = mDatas.size() - 1; i > 5; i--) {
                    mDatas.remove(i);
                }
            }
        }
        numSelect = 6 - mDatas.size();
        if (gridViewAdapter != null) {
            gridViewAdapter.notifyDataSetChanged();
        }

//        ToastUtil.showShortlToast(uploadVos.getData().get(0).getOriginalPath());

    }

    @Override
    public void success(String str) {
        DiscussionNewActivity.this.finish();
    }

    private void resultSend(List<Photo> pathList) {
        mPresenter.uploadSubjectivePhoto(pathList);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {
            //相册返回图片  图片选择器
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(path);
                    paths.add(photo);
                    resultSend(paths);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        resultSend(photoLists);
                    }
                }

            }
        } else if (requestCode == REQ_CODE_CAMERA) {
            //拍照返回
            if (resultCode == Activity.RESULT_OK) {
                mPresenter.startClipPic(false);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {

                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(UCrop.getOutput(data));
                paths.add(photo);
                resultSend(paths);
            }
        } else if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                mPresenter.toSystemCamera();
                dialog.dismiss();
            } else {
                requestPermissiontest();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(FullImageEvBus event) {
        if (event.isDel()) {
            deleteImage(deletePos);
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.version_text) {
        } else if (id == R.id.node_text) {
        } else if (id == R.id.tv_back) {
            if (isEdit) {
                isEdit = false;
                saveDiscuss();
            } else {
                DiscussionNewActivity.this.finish();
            }
        } else if (id == R.id.tv_save) {
            saveDiscuss();
        }

    }

    private void saveDiscuss() {
        if (mDatas.size() > 6) {
            ToastUtil.showShortlToast("最多上传六张图");
        }
        if (etTitle.getText().toString().trim().length() < 1) {
            ToastUtil.showShortlToast("标题不能为空");
        } else {
            Map<String, Object> param = new HashMap<>();
            param.put("chatTitle", etTitle.getText().toString());
            param.put("chatContent", etContent.getText().toString());
            param.put("lessonId", lessonId);
            param.put("subjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
            param.put("chatPic", buildChatPic(mDatas).toString());
            mPresenter.saveGroupDiscussion(param);
        }
    }

    private JSONArray buildChatPic(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String url : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pic", url);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    @Override
    public void deleteImage(int position) {
        if (mDatas.size() > 0) {
            mDatas.remove(position);
            numSelect = 6 - mDatas.size();
        }
        gridViewAdapter.notifyDataSetChanged();
    }

    public void requestPermissiontest() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            mPresenter.toSystemCamera();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }
//        MPermissions.requestPermissions(this, REQUECT_CODE_CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }

    }

    /* @PermissionGrant(REQUECT_CODE_CAMERA)
     public void requestSdcardSuccess() {
         mPresenter.toSystemCamera();
     }

     @PermissionDenied(REQUECT_CODE_CAMERA)
     public void requestSdcardFailed() {
         ToastUtil.showShortlToast("请设置相机权限");
     }
 */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     *
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
