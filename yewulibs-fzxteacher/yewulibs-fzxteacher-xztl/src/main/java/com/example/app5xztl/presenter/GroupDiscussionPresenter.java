package com.example.app5xztl.presenter;


import android.text.TextUtils;

import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5xztl.view.GroupDiscussionView;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 小组讨论Presenter--》主模块
 *
 * @author zhaosen
 */
public class GroupDiscussionPresenter extends BasePresenter<GroupDiscussionView, BaseActivity> {

    public void getClassList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getAllClassList()
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onClassonError();
                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }
    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getVersionList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<VersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<VersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed();
                    }

                    @Override
                    public void onNext(VersionBean versionBean) {
                        dealVersionBean(versionBean);
                    }
                });

    }

    public void getNodeList(final String baseVersion) {
        if (TextUtils.isEmpty(baseVersion)) {
            mView.onNodeSuccessed(null);
            return;
        }

        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        final String baseLevelId = SPUtils.getLoginBean().getData().getUser().getBaseLevelId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getNodeList(subject, baseLevelId, getBaseGradeId(), baseVersion, getBaseVolumeId())
                .map(new StatusFunc<NodeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NodeBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onTitleFailed();
                    }

                    @Override
                    public void onNext(NodeBean o) {
                        mView.onNodeSuccessed(o.getData());
                    }
                });

    }

    void dealVersionBean(VersionBean versionBean) {

        if (versionBean == null) {
            versionBean = new VersionBean();
        }
        if (versionBean.getData() == null) {
            versionBean.setData(new ArrayList<VersionBean.DataBean>());

            final ArrayList<VersionBean.DataBean.VolumeListBean> volumeListBeans = new ArrayList<>();

            final VersionBean.DataBean dataBean = new VersionBean.DataBean();
            dataBean.setVolumeList(volumeListBeans);

            versionBean.getData().add(dataBean);
        }

        VersionBean.DataBean.VolumeListBean volumeListBean = new VersionBean.DataBean.VolumeListBean();
        volumeListBean.setBaseVersionName("全部册别");
        volumeListBean.setBaseGradeName("");
        volumeListBean.setBaseVolumeName("");
        versionBean.getData().get(0).getVolumeList().add(0, volumeListBean);

        ArrayList<VersionBean.DataBean.VolumeListBean> retList = new ArrayList<>();

        for (int i = 0; i < versionBean.getData().size(); i++) {
            retList.addAll(versionBean.getData().get(i).getVolumeList());
        }

        mView.onVersionSuccessed(retList);
    }


    private String dealNullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;

    }

    private int getNodePointCount(String str) {
        final String newStr = str.replace(".", "");
        return str.length() - newStr.length();

    }

    public String getBaseGradeId() {
        return dealNullStr(baseGradeId);
    }

    public void setBaseGradeId(String baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseVolumeId() {
        return dealNullStr(baseVolumeId);
    }

    public void setBaseVolumeId(String baseVolumeId) {

        this.baseVolumeId = baseVolumeId;
    }

    public String getChapterNodeIdPath() {
        return dealNullStr(chapterNodeIdPath);
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getBaseVolumeName() {
        return baseVolumeName;
    }

    public void setBaseVolumeName(String baseVolumeName) {
        this.baseVolumeName = baseVolumeName;
    }

    public String getChapterNodeName() {
        return chapterNodeName;
    }

    public void setChapterNodeName(String chapterNodeName) {
        this.chapterNodeName = chapterNodeName;
    }

    private String baseGradeId;
    private String baseVolumeId = "";
    private String baseVolumeName = "";
    private String chapterNodeIdPath = "";
    private String chapterNodeName = "";
}
