package com.sdzn.fzx.teacher.app;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.example.app5libbase.app.CrashHandler;
import com.example.app5libbase.listener.MyDefaultLoadReporter;
import com.example.app5libbase.listener.MyTinkerPatchReporter;
import com.example.app5libbase.util.dao.DaoInitutil;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.tinker.anno.DefaultLifeCycle;
import com.tencent.tinker.entry.DefaultApplicationLike;
import com.tencent.tinker.lib.listener.DefaultPatchListener;
import com.tencent.tinker.lib.patch.UpgradePatch;
import com.tencent.tinker.lib.service.DefaultTinkerResultService;
import com.tencent.tinker.lib.tinker.TinkerInstaller;
import com.tencent.tinker.loader.shareutil.ShareConstants;

import org.xutils.x;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.unit.Subunits;
import rain.coder.photopicker.PhotoPick;

//import com.sdzn.fzx.teacher.BuildConfig;

/**
 * AppLike〈一句话功能简述〉
 * 〈功能详细描述〉
 * 接入tinker
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
@SuppressWarnings("unused")
@DefaultLifeCycle(application = "com.sdzn.fzx.teacher.app.App",
        flags = ShareConstants.TINKER_ENABLE_ALL,
        loadVerifyFlag = false)
public class AppLike extends DefaultApplicationLike {


    public AppLike(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag,
                   long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {
        super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime, tinkerResultIntent);
    }

    /**
     * install multiDex before install tinker
     * so we don't need to put the tinker lib classes in the main dex
     *
     * @param base
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onBaseContextAttached(Context base) {
        super.onBaseContextAttached(base);
        //you must install multiDex whatever tinker is installed!
        MultiDex.install(base);

        //installTinker after load multiDex
        //or you can put com.tencent.tinker.** to main dex
        TinkerInstaller.install(this,
                new MyDefaultLoadReporter(getApplication()),
                new MyTinkerPatchReporter(getApplication()),
                new DefaultPatchListener(getApplication()),
                DefaultTinkerResultService.class,
                new UpgradePatch());
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void registerActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks callback) {
        getApplication().registerActivityLifecycleCallbacks(callback);
    }

    public static Application mContext;
    public static AppLike mAppLike;


//    public static boolean showCountDown = false;
//    public static boolean showHeadCountDown = false;//头部
//    public static boolean showSmallCountDown = true;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplication();
        mAppLike = this;
        configShipei();
//        Log.setEnabled(BuildConfig.DEBUG);
        MultiDex.install(getApplication());
//        Resources resources = getApplication().getResources();
//        if (resources != null) {
//            Configuration conf = resources.getConfiguration();
//            conf.fontScale = 1.0f;
//            resources.updateConfiguration(conf, resources.getDisplayMetrics());
//        }
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(getApplication());
        strategy.setAppChannel(BuildConfig3.BUGLY_CHANNEL);
        CrashReport.initCrashReport(mContext, "7a95ed3482", true);//bugly

//        if (!AndroidUtil.isApkInDebug(this)) {
//            initCrashHandler();
//            Log.setEnabled(false);
//        } else {
//            Log.setEnabled(true);
//        }
        configmmkv();
        //网络初始化
        configRetrofitNet();
        configHios();//初始化Hios协议
        initImageLoader();
        x.Ext.init(getApplication());
        PhotoPick.init(getApplication());
        DaoInitutil.getInstance();
//        EmojiManager.install(new GoogleEmojiProvider());//GoogleEmojiProvider
        //悬浮窗
//        FloatWindowUtil.getInstance();
    }

    private void configmmkv() {
        MmkvUtils.getInstance().get("");
        MmkvUtils.getInstance().get_demo();
    }

    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }

    private void configHios() {
        HiosHelper.config(App2.get().getPackageName() + ".ad.web.page", App2.get().getPackageName() + ".web.page");
//        HiosHelper.config(AppUtils.getAppPackageName() + ".slbapp.ad.web.page", AppUtils.getAppPackageName() + ".slbapp.web.page");
//        HiosHelperzb.config(AppUtils.getAppPackageName() + ".slbpad.ad.web.page", AppUtils.getAppPackageName() + ".slbpad.agentweb.page");
    }


    private void configShipei() {
        AutoSizeConfig.getInstance().setCustomFragment(true);
        AutoSizeConfig.getInstance().getUnitsManager()
                .setSupportDP(false)
                .setSupportSP(false)
                .setSupportSubunits(Subunits.MM);
        AutoSize.initCompatMultiProcess(getApplication());
    }


    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplication());
    }

    /**
     * 初始化图片加载器
     */
    private void initImageLoader() {
        ImageLoaderConfiguration configuration = ImageLoaderConfiguration
                .createDefault(getApplication());
        ImageLoader.getInstance().init(configuration);
    }


    //        MultiDex.install(this);
    //        super.attachBaseContext(base);
    //    protected void attachBaseContext(Context base) {
//    @Override

//    }


//    @Override
//    public void onTerminate() {
//        getApplication().unregisterReceiver(networkReceiver);
//        super.onTerminate();
//    }

//    @Override
//    public Resources getResources(Resources resources) {
//        if (resources != null && resources.getConfiguration().fontScale != 1.0f) {
//            Configuration conf = resources.getConfiguration();
//            conf.fontScale = 1.0f;
//            resources.updateConfiguration(conf, resources.getDisplayMetrics());
//        }
//        return resources;
//    }


}
