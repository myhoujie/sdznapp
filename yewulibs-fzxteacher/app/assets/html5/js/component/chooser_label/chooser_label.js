/**
 * Created by ming on 2018/1/26.
 */
//单选选择组
//data中包含code+name

var ChooserLabel = function (parentView, data, templateHtml, createNameFunc) {
    this.data = data;
    this.parentView = parentView;
    this.templateHtml = templateHtml;
    this.createNameFunc = createNameFunc;
    //初始化
    this.init();
};


//初始化 绘制、注册监听
ChooserLabel.prototype.init = function () {
    this.phtml = $(this.templateHtml).clone(true);
    //赋值
    this.createNameFunc(this.phtml, this.data.name);
    //添加按钮
    this.parentView.append(this.phtml);
    //注册监听
    this.phtml.on("click", this, this.onSelect);
};

//点击选择事件，触发具体过程在父组件处理。
ChooserLabel.prototype.onSelect = function (eve) {
    var self = eve.data;
    //触发事件
    if (self.listenerFunc)
        self.listenerFunc.call(self.listener, self.data.code);
};

//控制选择,添加样式
ChooserLabel.prototype.select = function () {
    if (!this.phtml.hasClass('on'))
        this.phtml.addClass('on');
};

//控制不选择,去除样式
ChooserLabel.prototype.unSelect = function () {
    if (this.phtml.hasClass('on'))
        this.phtml.removeClass('on');
};

//监听
ChooserLabel.prototype.addListener = function (listener, listenerFunc) {
    this.listener = listener;
    this.listenerFunc = listenerFunc;
};
