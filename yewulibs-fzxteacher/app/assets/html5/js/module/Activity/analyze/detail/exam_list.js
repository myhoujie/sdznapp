/**
 * Created by ming on 2018/2/7.
 */
    var ExamList = function (containerView, activityId) {
        this.containerView = containerView;
        this.view = this.containerView.find('[sid=exam_list]');
        this.activityId = activityId;

        //设置默认数据
        this.defaultId = -1;
        //navLabel集合
        this.labelList = [];
        //选择值
        this.selectLabel = null;

        this.init();
    };

    ExamList.prototype.init = function () {
        //设置滚动条
        this.setHeight(this.containerView);
        var that = this;
        $(window).resize(function () {
            that.setHeight(that.containerView);
        });
        //    请求数据
        var param = {};
        param.lessonTaskId = this.activityId;
        param.parentId = 0;
        param.type = 1;

        ApiActivity.taskDetail(param, this, function (flag, data) {
            var that = this;
            if (flag) {
                if (data != null && data != undefined) {
                    var examArray = data.data;
                    $(examArray).each(function (key, value) {
                        var examLabel = new ExamLabel(value.id, JSON.parse(value.examText), key + 1,false,value.score);
                        that.view.append(examLabel.view);
                        examLabel.addListener(that, that.onValueChange);
                        that.labelList.push(examLabel);
                    });
                    that.view.find("img").off("click").on("click",function (e) {
                        e.stopPropagation();
                        console.log(this.src);
                        html5SendOpenImageNative(this.src);
                    });
                    //设置默认值
                    if (examArray.length > 0)
                        that.defaultId = examArray[0].id;
                    //进行初始选择,如果没有初始选额，那么选择默认值
                    that.select(that.defaultId,1);
                }
                else
                    alert(2, "数据为空");
            }
            else
                alert(2, data.msg);

        });
    };

    //设置滚动条
    ExamList.prototype.setHeight = function (obj) {
        var repre_right_top = obj.offset().top;
        var heig = $(document).height() - repre_right_top;
        obj.height(heig);
    };


    //触发事件:如果选择项之前选择过则不触发
    ExamList.prototype.onValueChange = function (id) {
        if (this.select(id,1))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener, this.getValue());
    };

    //选择某个题目
    ExamList.prototype.select = function (id,model) {
        //model为1是内部调用，不切换滚动条位置。
        if (this.selectLabel != null && this.selectLabel != undefined)
            this.selectLabel.unSelect();
        //如果id匹配，选择上id对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.labelList).each(function (key, value) {
            if (!flag && value.examId == id) {
                value.select();
                //滚动高度
                if(model != 1)
                {
                    //不带动效
                    // that.containerView.scrollTop(that.containerView.scrollTop() + value.view.offset().top - that.containerView.offset().top);
                    //带动效
                    that.containerView.animate({scrollTop:that.containerView.scrollTop() + value.view.offset().top - that.containerView.offset().top},200);
                }
                that.selectLabel = value;
                flag = true;
            };
        });
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectLabel = null;
        return false;
    };

    //得到选择值
    ExamList.prototype.getValue = function () {
        if (this.selectLabel != null && this.selectLabel != undefined)
            return this.selectLabel.examId;
        //如果未选择任何数据
        return null;
    };

    //监听
    ExamList.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };
