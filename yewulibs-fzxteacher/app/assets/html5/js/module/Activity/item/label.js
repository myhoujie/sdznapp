/**
 * 〈一句话功能简述〉
 * 〈功能详细描述〉
 * @param  [参数1]   [参数1说明]
 * @param  [参数2]   [参数2说明]
 * @return [返回类型说明]
 * @exception/throws [违例类型] [违例说明]
 * @see          [类、类#方法、类#成员]
 * @deprecated
 */

define(function (require, exports, module) {

  var annular = require('../../../util/annular.js');
  var Label = function (view, parent) {
    this.view = $(view);
    this.parent = parent;
    this.init();
  }

  module.exports = annular;

  Label.prototype.init = function () {
    this.phtml = this.html.clone(true);
    this.view.append(this.phtml);
    this.plate = this.phtml.find("[cid=plate]");
    this.plate.append(this.item.clone(true));
    this.initEvent();
  };

  Label.prototype.initEvent = function () {
    this.phtml.on("click", this.parent, this.parent.listTiggerEvent);
  };
  Label.prototype.html = $(
    '<div class="list" cid="list">' +
    '    <div class="row">' +
    '    <div class="row_t">' +
    '    <div class="row_text_ico"></div>' +
    '    <div class="row_text">有理数方程，这个地方可以直接通过来</div>' +
    '</div>' +
    '<div class="row_t">' +
    '    <div class="row_text">三年级二班</div>' +
    '    </div>' +
    '    <div class="row_t">' +
    '    <div class="row_text">数量：<span>8</span></div>' +
    '</div>' +
    '<div class="but_group">' +
    '    <div class="but_ico">' +
    '    <div class="but_remove"></div>' +
    '    <div class="but_text">删除</div>' +
    '    </div>' +
    '    <div class="but_ico">' +
    '    <div class="but_preview"></div>' +
    '   <div class="but_text">预览</div>' +
    '    </div>' +
    '    <div class="but_ico">' +
    '    <div class="but_explain"></div>' +
    '    <div class="but_text">讲解</div>' +
    '    </div>' +
    '    <div class="but_ico">' +
    '    <div class="but_answer"></div>' +
    '    <div class="but_text">答案</div>' +
    '    </div>' +
    '    <div class="but_ico">' +
    '    <div class="but_correct"></div>' +
    '    <div class="but_text">批改</div>' +
    '    </div>' +
    '    </div>' +
    '<div class="plate" cid="plate"></div>' +
    '    </div>' +
    '    </div>');
  Label.prototype.item = $(
    '<svg viewBox="0 0 100 100">' +
    '        <defs>' +
    '        <linearGradient id="orange_yellow" x1="0" y1="1" x2="0" y2="0">' +
    '        <stop offset="0%" stop-color="#FFD012"></stop>' +
    '        <stop offset="100%" stop-color="#FF6D4A"></stop>' +
    '        </linearGradient>' +
    '        </defs>' +
    '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eaeef2"' +
    '    stroke-width="5" fill-opacity="0"></path>' +
    '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"' +
    '    stroke-linecap="round" stroke="url(#orange_yellow)" stroke-width="6"' +
    '    fill-opacity="0" style="stroke-dasharray: 300px, 300px;stroke-dashoffset: 150px;transition: stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease;"></path>' +
    '        </svg>' +
    '        <div class="mi-box-label">' +
    '        <div class="mi-box-progress">' +
    '        <span class="progress-now">15</span>' +
    '        <span>/</span>' +
    '        <span class="progress-total">28</span>' +
    '        </div>' +
    '        <div class="mi-box-status">' +
    '        <span>进行中</span>' +
    '        </div>' +
    '        </div>');
  return Label;
});