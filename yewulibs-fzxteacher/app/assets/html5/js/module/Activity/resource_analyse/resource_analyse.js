var Datum = function (lessonTaskId) {
  this.lessonTaskId = lessonTaskId;
  this.view = $('[cid=container]');
  this.initView();
};
Datum.prototype.initView = function () {

  this.view.load('html/activity/activity_detail/resource_analyse/resource_analyse.html', this.init.bind(this))
};

Datum.prototype.init = function () {
  // this.svg = this.view.find("[sid=datum_svg]");
  this.list = this.view.find("[sid=left_list]");
  this.rightBox = this.view.find("[sid=datum_right]");
  //初始化页面组件及结构
  this.getResourceList();
  // this.svg.append(this.html.clone(true));
  this.path = this.view.find("[sid=path]");
  //处理页面数据及绘制内容
};

Datum.prototype.getResourceList = function () {
  var param = {};
  // param.lessonTaskId = this.lessonTaskId; TODO
  param.lessonTaskId = this.lessonTaskId;
  param.type = 2;
  param.parentId = 0;
  ApiActivity.getResourceList(param, this, this.detailResourceList);
};

Datum.prototype.detailResourceList = function (flag, res) {
  if (flag) {
    if (res.data.length && res.data.length > 0) {
      var arr = [];
      for (var i = 0, len = res.data.length; i < len; i++) {
        this.rightBox.append(this.rightHtml);
        this.tableView = this.view.find("[class=tble]");


        var that = this;
        $(window).resize(function () {
          that.setHeight(that.tableView);

        });
        this.setHeight(that.tableView);


        this.svg = this.view.find("[sid=datum_svg]");
        arr.push(new ListSelectItem(this.list, res.data[i], this.lessonTaskId, this.rightBox, this.path, this.html.clone(true), this.svg));
      }
      arr[0].view.trigger('click');
    } else {
      console.log('此处加载空页面！');
      this.view.append($(this.blankTemplate).clone(true));
    }
  } else {
    alert('获取资源列表失败');
  }
};

Datum.prototype.html = $(
  '<svg viewBox="0 0 100 100">' +
  '        <defs>' +
  '        <linearGradient id="orange_yellow" x1="0" y1="1" x2="0" y2="0">' +
  '        <stop offset="0%" stop-color="#68C426"></stop>' +
  '        <stop offset="100%" stop-color="#68C426"></stop>' +
  '        </linearGradient>' +
  '        </defs>' +
  '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eaeef2"' +
  '    stroke-width="5" fill-opacity="0"></path>' +
  '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"' +
  '    stroke-linecap="round" stroke="url(#orange_yellow)" stroke-width="6"' +
  '    fill-opacity="0" style="stroke-dasharray: 300px, 300px;stroke-dashoffset: 300px;transition: stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease;" sid="path"></path>' +
  '        </svg>' +
  '        <div class="mi-box-label">' +
  '        <div class="mi-box-progress" style="margin-top:15px;">' +
  '        <span class="progress-now"  sid="submit_count_svg"></span>' +
  '        <span>/</span>' +
  '        <span class="progress-total" sid="all_count_svg"></span>' +
  '        </div>' +
  '        <div class="mi-box-status">' +
  '        </div>' +
  '        </div>'
)

Datum.prototype.rightHtml = $(
  '<div style="width: 145px;height: 145px;position: absolute;margin: 20px 0px 0px 35px;" sid="datum_svg">' +
  '</div><div style="margin: 26px 0px 0px 214px; font-size: 16px;color: #323C47;letter-spacing: 0;">' +
  '<span sid="submit_count"></span> <br><span style="font-size: 14px;position:absolute;top:32px;left:214px;height: 15px;display:block;color: #B1B5BA;letter-spacing: 0;display:block;line-height: 40px;">已完成</span></div>' +
  '<div style="margin: 48px 0px 0px 214px; font-size: 16px;color: #323C47;letter-spacing: 0;"><span' +
  '    sid="remain_count"></span><br><span  style="font-size: 14px;position:absolute;top:90px;left:214px;height: 15px;color: #B1B5BA;letter-spacing: 0;display:block;line-height: 50px;">未查看</span>' +
  '</div>       <div style="margin: 45px 0px 12px 212px; font-size: 16px;color: #323C47;letter-spacing: 0;">' +
  '<span sid="avg_time"></span></div>        <div style="position: absolute;top: 0px; right:20px;">' +
  '   <div class="number_text_gray text_all">未查看</div>' +
  '        <div class = "number_text_full text_all" > 已完成 </div ></div ></div >' +
  '<div class = "stu_list" style="margin-top: 30px;"> <table class    = "tble"    border = "0"    cellspacing = "0"     cellpadding = "0"    align = "center" >' +
  '<thead><tr><td> 学生 </td ><td > 状态 </td ><td > 用时 </td ></tr ></thead><tbody sid = "stu_list"> </tbody ></table>' +
  '</div >')
Datum.prototype.blankTemplate = '<div class="mi-blank-container" sid="blank" style="height:100%;"> ' +
  '<div class="mi-blank"> ' +
  '<div class="mi-blank-label">暂无内容~</div> ' +
  '</div> ' +
  '</div>';

Datum.prototype.setHeight = function (obj) {
  var repre_right_top = obj.offset().top;
  var heig = $(document).height() - repre_right_top;
  obj.height(heig);
};