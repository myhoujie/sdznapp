/**
 * Created by wuhai on 2018/2/6.
 */

var ListSelectItem = function (parentView, data, lessonTaskId, rightBox, path, html, svg) {

    this.parentView = parentView;
    this.data = data;
    this.lessonTaskId = lessonTaskId;
    this.rightBox = rightBox;
    // this.path = path;
    this.parentHtml = html;
    this.parentSvg = svg;
    this.init();
};

ListSelectItem.template = '<div class="datum_item"><div sid="item"></div><div sid="name" class="item_text"></div></div>';

ListSelectItem.prototype.init = function () {
    // for (var i = 0, len = this.data.length; i < len; i++) {
    this.view = $(ListSelectItem.template);
    this.addIcon(this.data, this.view.find("[sid=item]"));
    this.view.find("[sid=name]").text(this.data.resourceName);
    this.parentView.append(this.view);
    this.view.off('click').on('click',this,this.handleClick);
    // }

};

ListSelectItem.prototype.handleClick = function (evt) {
    var self = evt.data;
    var param = {};
    //param.lessonTaskId = this.lessonTaskId;
    //param.resourceId = this.data.resourceId;
    $(this).siblings().removeClass("on");
    $(this).addClass("on");
    param.lessonTaskDetailsId=self.data.id;
    ApiActivity.resourceAnalyse(param, self, self.resourceAnalyse);
};

ListSelectItem.prototype.resourceAnalyse = function (flag, res) {

    if (flag && res.data) {
        this.parentSvg.empty();
        this.parentSvg.append(this.parentHtml);
        this.rightBox.removeClass("dn");
        this.rightBox.find("[sid=submit_count]").text(res.data.countStudentSubmit + '人');
        this.rightBox.find("[sid=submit_count_svg]").text(res.data.countStudentSubmit);
        this.rightBox.find("[sid=all_count_svg]").text(res.data.countStudentTotal);
        this.rightBox.find("[sid=remain_count]").text(res.data.countStudentRemain + '人');
        this.rightBox.find("[sid=avg_time]").text('查看平均时长:' + parseInt(res.data.useTimeAvg / 60) + '分' + parseInt(res.data.useTimeAvg % 60) + '秒');
        // this.getStudentList(res.data.studentList);
        this.path = this.parentSvg.find("[sid=path]");
        this.path.css('stroke-dashoffset', parseInt(300 * (res.data.countStudentRemain / res.data.countStudentTotal)) + 'px');
        this.stu_list = this.rightBox.find("[sid=stu_list]");
        new StudentListItems(this.stu_list , res.data.studentList);
        this.setHeight(this.stu_list);
    } else {
        this.rightBox.addClass("dn");
        alert( '获取资源分析失败');
    }
};
// ListSelectItem.prototype.getStudentList = function (list) {
//     new StudentListItem(this.rightBox.find("[sid=stu_list]"), list);
// };
ListSelectItem.prototype.addIcon = function (data, view) {
    var text = JSON.parse(data.resourceText);
    switch (data.resourceType) {
        case 1:
            if (text.resourceSuffix == "txt") {
                view.addClass('datum_item_icon icon_txt');
            } else {
                view.addClass('datum_item_icon icon_doc');
            }
            break;
        case 2:
            view.addClass('datum_item_icon icon_ppt');
            break;
        case 3:
            view.addClass('datum_item_icon icon_mp4');
            break;
        case 4:
            view.addClass('datum_item_icon icon_def');
            break;
        case 5:
            view.addClass('datum_item_icon icon_mp3');
            break;
        case 6:
            view.addClass("datum_item_icon icon_pdf");
            break;
        default:
            break;
    }
};
ListSelectItem.prototype.setHeight = function (obj) {
    var repre_right_top = obj.offset().top;
    var heig = $(document).height() - repre_right_top;
    obj.height(heig);
    console.log(obj.height());
};
