/**
 * Created by ming on 2018/1/26.
 */
/*
 * */

define(function (require, exports, module) {
    //引用
    var ActivityLabel = require('../activity_index/item/activity_label.js');

    var Content = function (view, param) {
        this.view = view;
        this.labelListContainer = this.view.find('.labelList');
        this.labelListView = this.view.find('[cid=labelList]');

        //当前页
        this.currentPage = 0;
        //已持有的条数
        this.labelCount = 0;
        //当前查询条件
        this.param = param;
        //每页条数
        this.param.rows = 20;
        //记录总数据,用于处理重复
        this.activityDatas = [];

        this.lastQueryPage = 0;


        this.init();
    };

    module.exports = Content;

    Content.prototype.init = function () {
        //清空之前所有内容
        this.labelListView.empty();
        this.labelListContainer.find('[sid=blank]').remove();
        this.setContentInfo(0);
        //设置滚动条
        this.setHeight(this.labelListContainer);
        var that = this;
        $(window).resize(function () {
            that.setHeight(that.labelListContainer);
        });
        //执行首次查询
        this.query();
    };

    Content.prototype.clearRepeat = function (datas) {
        var that = this;
        $(datas).each(function (key1, value1) {
            $(that.activityDatas).each(function (key2, value2) {
                if (value1.id == value2.id)
                    datas.splice(key1, 1);
            });
        });
    };

    Content.prototype.changeContent = function (type, datas) {
        var size = 0;
        //失败的情况计算内容状态
        if (type == 0) {
            // 失败
            if (this.labelCount == 0)
                this.setBlankPage();
            else
                this.setContentInfo(1);
        }
        //成功的情况计算内容状态
        else {
            this.clearRepeat(datas);
            // 成功
            size = datas.length;
            //如果之前无内容，首次查询根据返回值的内容判断info和空页状态。
            if (this.labelCount == 0) {
                if (size == 0) {
                    this.setBlankPage();
                }
                else if (size < this.param.rows) {
                    this.setContentInfo(3);
                    this.currentPage++;
                }
                else if (size == this.param.rows) {
                    this.setContentInfo(1);
                    this.currentPage++;
                }
            }
            //如果有内容，首次查询根据返回值的内容判断info和空页状态。
            else {
                if (size == 0) {
                    this.setContentInfo(3);
                }
                else if (size < this.param.rows) {
                    this.setContentInfo(3);
                    this.currentPage++;
                }
                else if (size == this.param.rows) {
                    this.setContentInfo(1);
                    this.currentPage++;
                }
            }
            this.labelCount += size;

            //添加数据绘制label
            if (datas.length != 0) {
                var that = this;
                $(datas).each(function (key, value) {
                    var label = new ActivityLabel(that.labelListView, value);
                    that.activityDatas.push(value);
                });
            }
        }
    };

    Content.prototype.query = function () {
        this.param.page = this.currentPage + 1;
        console.log(this.param);
        ApiActivity.activityList(this.param, this, this.handleQuery);
    };

    Content.prototype.handleQuery = function (flag, data) {
        if (flag) {
            if (data.data != null && data.data != undefined) {
                this.changeContent(1, data.data);
            }
            else {
                alert("返回结果为null");
                this.changeContent(0);
            }
        }
        else {
            alert(data.msg);
            this.changeContent(0);
        }
    };

    //设置滚动条
    Content.prototype.setHeight = function (obj) {
        var repre_right_top = obj.offset().top;
        var heig = $(document).height() - repre_right_top;
        obj.height(heig - 10);
    };

    //点击加载更多
    Content.prototype.onQueryMore = function (evt) {
        var self = evt.data;
        //设置正在加载中。
        self.setContentInfo(2);
        self.query();
    };

    Content.prototype.setBlankPage = function () {
        //清空列表内容
        this.labelListView.empty();
        //添加空页面
        this.labelListContainer.append($(Content.blankTemplate).clone(true));
        //    info区删除
        this.setContentInfo(0);
    };


    Content.prototype.setContentInfo = function (type) {
        //不显示info条
        if (type == 0) {
            this.labelListContainer.off('scroll');
            if (this.labelListContainer.find('[sid=content_info]'))
                this.labelListContainer.find('[sid=content_info]').remove();
        }
        else {
            var contentInfoView = null;
            if (this.labelListContainer.find('[sid=content_info]').length > 0)
                contentInfoView = this.labelListContainer.find('[sid=content_info]');
            else
                contentInfoView = $(Content.infoTemplate).clone(true);
            this.labelListContainer.append(contentInfoView);
            //显示加载更多，并且绑定事件（滚动+按钮）
            if (type == 1) {
                contentInfoView.text("点击加载更多");
                contentInfoView.on('click', this, this.onQueryMore);
            }
            //显示正在加载，并且取消绑定事件
            if (type == 2) {
                contentInfoView.off('click');
                contentInfoView.text("正在加载...");
            }
            //显示没有更多，并取消绑定事件
            if (type == 3) {
                // this.labelListContainer.off('scroll');
                contentInfoView.off('click');
                contentInfoView.text("没有更多了哟~");
            }
        }
    };

    Content.blankTemplate = '<div class="mi-blank-container" sid="blank" style="height:100%;"> ' +
        '<div class="mi-blank"> ' +
        '<div class="mi-blank-label">暂无内容~</div> ' +
        '</div> ' +
        '</div>';

    Content.infoTemplate = '<div sid="content_info" class="content_info">' +
        '点击加载更多... ' +
        '</div>';


});