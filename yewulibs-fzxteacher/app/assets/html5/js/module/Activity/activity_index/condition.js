/**
 * Created by ming on 2018/1/26.
 */
/*
 * 页面上方查询区域组件
 * 获取初始化的条件getInitParam()
 * 查询事件：addListener()*/
    var Condition = function (parentView, subjectId) {
        this.view = parentView;
        this.subjectId = subjectId;

        this.init();
    };
    Condition.prototype.init = function () {
        this.selectConditionView = this.view.find('[sid=select-condition]');
        this.classConditionView = this.view.find('[sid=class-condition]');
        this.statusConditionView = this.view.find('[sid=status-condition]');

        //创建对象
        this.selectCondition = new SelectCondition(this.selectConditionView,1);
        this.classCondition = new ClassCondition(this.classConditionView, this.subjectId);
        this.statusCondition = new StatusCondition(this.statusConditionView);

        //事件
        this.selectCondition.addLoadedListener(this, this.onloaded);
        this.selectCondition.addListener(this, this.onQuery);
        this.statusCondition.addListener(this, this.onParamChange);
        this.classCondition.addListener(this, this.onParamChange);
    };

    //加载完成事件
    Condition.prototype.onloaded = function () {
        if (this.loadedListenerFunc)
            this.loadedListenerFunc.call(this.loadedListener);
    };

    //点击查询后重置班级、状态查询到默认值。触发查询事件
    Condition.prototype.onQuery = function () {
        this.statusCondition.reset();
        this.classCondition.reset();
        if (this.listenerFunc)
            this.listenerFunc.call(this.listener, this.getParam());
    };

    //班级、状态变化时触发查询事件
    Condition.prototype.onParamChange = function () {
        if (this.listenerFunc)
            this.listenerFunc.call(this.listener, this.getParam());
    };

    //返回初始化查询参数
    Condition.prototype.getInitParam = function () {
        //章节选择初始化未添加
        return this.calcuParam(this.selectCondition.getValue(), this.classCondition.getInitValue(), this.statusCondition.getInitValue());
    };

    //返回用户选择的查询参数
    Condition.prototype.getParam = function () {
        return this.calcuParam(this.selectCondition.getValue(), this.classCondition.getValue(), this.statusCondition.getValue());
    };

    Condition.prototype.calcuParam = function (selectConditionValue, classConditionValue, statusConditionValue) {
        var param = {};

        //学科条件
        if (this.subjectId != null && this.subjectId != undefined)
            param.baseSubjectId = this.subjectId;

        //班级条件
        if (classConditionValue != null && classConditionValue != undefined && classConditionValue != -1)
            param.classId = classConditionValue;


        //状态条件
        if (statusConditionValue != null && statusConditionValue != undefined)
            param.status = statusConditionValue;

        //章节、复习、时间等
        if (selectConditionValue != null && selectConditionValue != undefined) {
            var flag = selectConditionValue.flag;
            if (flag != null && flag != undefined) {
                //章节
                if (flag == 1) {
                    if (selectConditionValue.books != null && selectConditionValue.books != undefined)
                        param.baseVolumeId = selectConditionValue.books.baseVolumeId;
                    if (selectConditionValue.tree != null && selectConditionValue.tree != undefined)
                        param.chapterNodeIdPath = selectConditionValue.tree.nodeIdPath;
                }
                //复习
                else if (flag == 2) {
                    if (selectConditionValue.Review != null && selectConditionValue.Review != undefined)
                        //typeReviewa 应该为typeReview，为了让查询条件失效特设置。
                        param.typeReview = selectConditionValue.Review.id;
                }
            }
            //起止时间
            if (selectConditionValue.yDate != null && selectConditionValue.yDate != undefined)
                param.startTime = selectConditionValue.yDate;
            if (selectConditionValue.tDate != null && selectConditionValue.tDate != undefined)
                param.endTime = selectConditionValue.tDate;
            //关键字(缺少接口)
            if (selectConditionValue.search != null && selectConditionValue.search != undefined)
                param.keyword = selectConditionValue.search;
        }
        return param;
    };

    //监听加载完成
    Condition.prototype.addLoadedListener = function (loadedListener, loadedListenerFunc) {
        this.loadedListener = loadedListener;
        this.loadedListenerFunc = loadedListenerFunc;
    };
    //监听改变
    Condition.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };
