/**
 * Created by ming on 2018/1/26.
 */
/*
 * 班级选择组件
 * 初始化调用：getInitValue();
 * 响应选择变化addListener()
 * 复位到默认值：reset();
 *
 * note：此组件中默认值和初始值可以不一样 */
define(function (require, exports, module) {
    //引用
    var ClassLabel =  require('../item/class_label.js');

    var ClassCondition = function (view, subjectId) {
        this.view = view;
        this.subjectId = subjectId;
        //设置默认数据
        this.defaultValue = -1;
        //置入初始数据
        this.initValue = -1;
        //classLabel集合
        this.classLabelList = [];
        //选择值
        this.selectClassLabel = null;
        this.init();
    };
    module.exports = ClassCondition;

    ClassCondition.prototype.init = function () {
        //    listView盛放按钮View
        this.listView = this.view.find('ul');
        //    创建初始数据
        var param = {};
        param.subjectId = this.subjectId;
        ApiActivity.classList(param, this, this.handleClassList);
    };

    ClassCondition.prototype.handleClassList = function (flag, data) {
        var that = this;
        if (flag) {
            if (data != null && data != undefined) {
                var classDatas = data.data;
                if (classDatas.length > 0) {
                    //添加全部标签，并将id置为-1；
                    that.classLabelList.push(new ClassLabel(that.listView, {
                        'id': -1,
                        'name': '全部'
                    }));
                    //添加班级标签
                    $(classDatas).each(function (key, value) {
                        that.classLabelList.push(new ClassLabel(that.listView, value));
                    })

                    //添加监听
                    $(that.classLabelList).each(function (key, value) {
                        value.addListener(that, that.onValueChange);
                    })

                    //    将初始值选择上
                    that.select(that.initValue);
                }
            }
        }
        else
            Information.show(2, data.msg);
    };

    //触发事件:如果选择项之前选择过则不触发
    ClassCondition.prototype.onValueChange = function (classId) {
        if (this.select(classId))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener);
    };

    //选择某个班级
    ClassCondition.prototype.select = function (classId) {


        if (this.selectClassLabel != null && this.selectClassLabel != undefined) {
            //如果选择项为之前选择项,不做任何改变，返回未执行更新flag。
            if (this.selectClassLabel.classData.id == classId)
                return false;
            //如果之前有选择，取消之前选择
            if (this.selectClassLabel != null)
                this.selectClassLabel.unSelect();
        }

        //如果ClassId匹配，选择上ClassId对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.classLabelList).each(function (key, value) {
            if (!flag && value.classData.id == classId) {
                value.select();
                that.selectClassLabel = value;
                flag = true;
            }
        })
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectClassLabel = null;
        return false;
    };

    //恢复到默认值
    ClassCondition.prototype.reset = function () {
        this.select(this.defaultValue);
    };

    //得到初始值
    ClassCondition.prototype.getInitValue = function () {
        return this.initValue;
    };

    //得到选择值
    ClassCondition.prototype.getValue = function () {
        if (this.selectClassLabel != null && this.selectClassLabel != undefined)
            return this.selectClassLabel.classData.id;
        //如果未选择任何数据
        return null;
    };

    //监听
    ClassCondition.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };
})