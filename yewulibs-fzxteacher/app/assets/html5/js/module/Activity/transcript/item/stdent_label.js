/**
 * Created by ming on 2018/2/2.
 */
    var StudentLabel = function (parentView, data, number) {
        this.parentView = parentView;
        this.data = data;
        this.labelView = null;
        //序号
        this.number = number;

        this.init();
    };

    StudentLabel.prototype.init = function () {
        this.labelView = $(StudentLabel.labelTemplate).clone(true);
        //绘制序号
        this.numberView = this.labelView.find('[sid=number]');
        this.numberView.text(this.number);
        //绘制头像，如果没有绘制默认图像
        this.studentImageView = this.labelView.find('[sid=student_image]');
        if(this.data.photo == null || this.data.photo == undefined)
            this.studentImageView.attr('src','img/ico/4b90f603738da977d0d4af33b251f8198618e32b.jpg');
        else
            this.studentImageView.attr("src",this.data.photo);

        //绘制名称
        this.studentNameView = this.labelView.find('[sid=student_name]');
        this.studentNameView.text(this.data.userStudentName);
        this.name_boder = this.labelView.find("[cid=name_boder]");
        this.name_boder.off("click").on('click', this, this.onClick);
        //绘制完成状态
        this.answerStatusView = this.labelView.find('[sid=answer_status]');
        var answerStatus = "";
        switch (this.data.status)
        {
            case 0:
                answerStatus = "未作答"
                break;
            case 1:
                answerStatus = "作答中"
                break;
            case 2:
                answerStatus = "已完成"
                break;
            case 3:
                answerStatus = "补交"
                break;
            default:break;
        }
        this.answerStatusView.text(answerStatus);
        //绘制批改状态
        this.correctStatusView = this.labelView.find('[sid=correct_status]');
        var correctStatus = "";
        switch (this.data.isCorrect)
        {
            case 0:
                correctStatus = "未批改"
                break;
            case 1:
                correctStatus = "已批改"
                break;
            default:break;
        }
        this.correctStatusView.text(correctStatus);
        //绘制得分
        this.scoreView = this.labelView.find('[sid=score]');
        if (this.data.scoreTotal < 0) {
            this.scoreView.text("—");
        } else {
            this.scoreView.text(this.data.scoreTotal);
        }
        //绘制用时
        this.spendTimeView = this.labelView.find('[sid=spend_time]');
        this.spendTimeView.text(this.formatSpendTime(this.data.useTime));

        this.parentView.append(this.labelView);
    };
    
    StudentLabel.prototype.onClick = function (evt) {
        pageStatus("false");
        $("body").find("[cid=tab_li]").hide();
        var self = evt.data;
        if(self.listenerFunc)
            self.listenerFunc.call(self.listener, self.data);
    }

    StudentLabel.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    StudentLabel.prototype.formatSpendTime = function (seconds) {
        var spendTime = '';
        if(parseInt(seconds / 3600) > 0)
            spendTime += parseInt(seconds / 3600) + "小时";
        seconds = seconds % 3600;

        if(parseInt(seconds / 60) > 0)
            spendTime += parseInt(seconds / 60) + '分';
        seconds = seconds % 60;

        spendTime += seconds +'秒';
        return spendTime;
    };


    StudentLabel.labelTemplate = '<tr> ' +
        '<td sid="number"></td> ' +
        '<td> ' +
        '<div class="tab_td_text" cid="name_boder" style="cursor: pointer;"> ' +
        '<div class="sysIco img_sys_ico" ><img sid="student_image" src="" /></div> ' +
        '<span cid="studentElement" sid="student_name" ></span> ' +
        '</div> ' +
        '</td> ' +
        '<td sid="answer_status"></td> ' +
        '<td sid="correct_status"></td> ' +
        '<td sid="score"></td> ' +
        '<td sid="spend_time"></td> ' +
        '</tr>';
