/**
 * Created by wjh on 2018/3/15.
 */
var STUDENT_CORRECT_LIST = '/teach/lesson/task/student/correct/list';    // 批改学生列表接口

var ANSWER_EXAM_ERROR = '/teach/lesson/answer/exam/list';                //题目列表接口

var EXAM_TYPE_UPDATE = '/teach/lesson/answer/exam/type/update';          // 优秀解答/典型错误接口

var STUDENT_TYPE_UPDATE = '/teach/lesson/task/student/type/update';      // 卷面优秀/卷面粗糙接口

var EXAM_NUMBER_LIST = '/teach/lesson/answer/exam/number/list';          // 题号列表接口

var EXAM_CORRECT_SAVE = '/teach/lesson/answer/exam/correct/save';        // 批改保存接口


var GET_STUDENT_UPTOKEN = 'teach/QiniuController/getUptoken';

var CorrectHandler = function () {

};
CorrectHandler.getStudentUptoken = function (data, listener, listenerFun) {

    doAjax({
        url: GET_STUDENT_UPTOKEN,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};


CorrectHandler.getStudentCorrectList = function (data, listener, listenerFun) {

    doAjax({
        url: STUDENT_CORRECT_LIST,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("STUDENT_CORRECT_LIST");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            console.log("fasdfasd");
            listenerFun.call(listener, false, data);
        }
    });
};


CorrectHandler.getAnswerExamList = function (data, listener, listenerFun) {

    doAjax({
        url: ANSWER_EXAM_ERROR,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("ANSWER_EXAM_ERROR");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};


CorrectHandler.examTypeUpdate = function (data, listener, listenerFun) {

    doAjax({
        url: EXAM_TYPE_UPDATE,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_TYPE_UPDATE");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};

CorrectHandler.studentTypeUpdate = function (data, listener, listenerFun) {

    doAjax({
        url: STUDENT_TYPE_UPDATE,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("STUDENT_TYPE_UPDATE");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};


CorrectHandler.getExamNumberList = function (data, listener, listenerFun) {

    doAjax({
        url: EXAM_NUMBER_LIST,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_NUMBER_LIST");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};


CorrectHandler.examCorrectSave = function (data, listener, listenerFun) {

    doAjax({
        url: EXAM_CORRECT_SAVE,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("EXAM_CORRECT_SAVE");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};





