/**
 * Created by ming on 2018/1/31.
 */
//model = 0 从活动中进入的面包屑
// model = 1 从课程中进入的面包屑。
//model == 2 从首页进入的面包屑。

//前置加载导致循环依赖
var DetailCrumb = function (view, model, lessonData, activityData) {
    this.view = view;
    this.model = model;
    this.lessonData = lessonData;
    this.activityData = activityData;

    this.lessonIndexView = null;
    this.activityIndexView = null;
    this.lessonView = null;
    this.activityView = null;
    this.init();
};


DetailCrumb.prototype.init = function () {
    this.view.empty();
    if (this.model == 0) {
        //教学活动index页
        this.activityIndexView = $(DetailCrumb.linkTemplate).clone(true);
        this.activityIndexView.text('课程任务');
        this.activityIndexView.on('click', {'obj': this, 'type': 'activity_index'}, this.onClickCrumb);
        this.view.append(this.activityIndexView);
        //分隔号
        this.view.append($(DetailCrumb.splitTemplate).clone(true));
        //教学活动名称
        this.activityView = $(DetailCrumb.normalTemplate).clone(true);
        this.activityView.text(this.activityData.name);
        this.view.append(this.activityView);
    }
    if (this.model == 1) {
        //课程首页
        this.LessonIndexView = $(DetailCrumb.linkTemplate).clone(true);
        this.LessonIndexView.text('我的课程');
        this.LessonIndexView.on('click', {'obj': this, 'type': 'lesson_index'}, this.onClickCrumb);
        this.view.append(this.LessonIndexView);
        //分隔号
        this.view.append($(DetailCrumb.splitTemplate).clone(true));
        //课程
        this.LessonView = $(DetailCrumb.linkTemplate).clone(true);
        this.LessonView.text(this.activityData.lessonName);
        this.LessonView.on('click', {'obj': this, 'type': 'lesson'}, this.onClickCrumb);
        this.view.append(this.LessonView);
        //分隔号
        this.view.append($(DetailCrumb.splitTemplate).clone(true));
        //教学活动名称
        this.activityView = $(DetailCrumb.normalTemplate).clone(true);
        this.activityView.text(this.activityData.name);
        this.view.append(this.activityView);
    }
    if (this.model == 2) {
        //教学活动index页
        this.activityIndexView = $(DetailCrumb.linkTemplate).clone(true);
        this.activityIndexView.text('首页');
        this.activityIndexView.on('click', {'obj': this, 'type': 'index'}, this.onClickCrumb);
        this.view.append(this.activityIndexView);
        //分隔号
        this.view.append($(DetailCrumb.splitTemplate).clone(true));
        //教学活动名称
        this.activityView = $(DetailCrumb.normalTemplate).clone(true);
        this.activityView.text(this.activityData.name);
        this.view.append(this.activityView);
    }
};

DetailCrumb.prototype.onClickCrumb = function (evt) {
    var self = evt.data.obj;
    var type = evt.data.type;
    switch (type) {
        case 'index':
            //暂时不实现
            // var Index = require();
            break;
        case 'activity_index':
            // module.activityLoader(that);
            var Activity = require("../activity.js");
            new Activity();
            break;
        case 'lesson_index':
            var Lesson = require('../../myLesson/lesson.js');
            new Lesson(self.lessonData.myLessonObj.lessonObj.menuObj);
            break;
        case 'lesson':
            var LessonDetail = require('../../myLesson/lesson-details.js');
            new LessonDetail(self.lessonData);
            break;
        default:
            break;
    }
};
DetailCrumb.linkTemplate = '<a></a>';
DetailCrumb.normalTemplate = '<span></span>';
DetailCrumb.splitTemplate = '<span> > </span>';
