/**
 * Created by ming on 2018/3/13.
 */

//典型错误更新和选择控件
    var TypicalCase = function (view, answerStudentId, initType) {
        this.view = view;
        this.initType = initType;
        this.answerStudentId = answerStudentId;

        this.isTypicalCaseGood = false;
        this.isTypicalCaseBad = false;

        this.typicalCaseGoodView = null;
        this.typicalCaseBadView = null;

        this.init();
    };

    TypicalCase.prototype.init = function () {
        this.typicalCaseGoodView = this.view.find('[sid=typical_case_good]');
        this.typicalCaseBadView = this.view.find('[sid=typical_case_bad]');

        if (this.initType == 1) {
            this.isTypicalCaseGood = true;
            this.isTypicalCaseBad = false;
        }
        else if (this.initType == 2) {
            this.isTypicalCaseGood = false;
            this.isTypicalCaseBad = true;
        }
        else {
            this.isTypicalCaseGood = false;
            this.isTypicalCaseBad = false;
        }
        //根据初始化数据，改变view;
        this.changView();
        //    添加监听
        this.typicalCaseGoodView.on('click',{'obj':this, 'type':'good'},this.onClick);
        this.typicalCaseBadView.on('click',{'obj':this,'type':'bad'},this.onClick);
    };

    TypicalCase.prototype.onClick = function (evt) {
        var self = evt.data.obj;
        var type = evt.data.type;
        if(type == 'good')
        {
            self.isTypicalCaseGood = !self.isTypicalCaseGood;
            if(self.isTypicalCaseGood && self.isTypicalCaseBad)
                self.isTypicalCaseBad = false;
        }
        if(type== 'bad')
        {
            self.isTypicalCaseBad = !self.isTypicalCaseBad;
            if(self.isTypicalCaseBad && self.isTypicalCaseGood)
                self.isTypicalCaseGood = false;
        }
        self.changView();
        self.updateTypicalCaseData();
    };

    TypicalCase.prototype.changView = function () {
        if(this.isTypicalCaseGood)
            this.typicalCaseGoodView.find('[sid=pic]').addClass('active');
        else
            this.typicalCaseGoodView.find('[sid=pic]').removeClass('active');

        if(this.isTypicalCaseBad)
            this.typicalCaseBadView.find('[sid=pic]').addClass('active');
        else
            this.typicalCaseBadView.find('[sid=pic]').removeClass('active');
    };

    TypicalCase.prototype.updateTypicalCaseData = function () {
        var param = {};
        param.id = this.answerStudentId;
        if(!this.isTypicalCaseBad && this.isTypicalCaseGood)
            param.correctType = 1;
        else if(this.isTypicalCaseBad && !this.isTypicalCaseGood)
            param.correctType = 2;
        else if(!this.isTypicalCaseBad && !this.isTypicalCaseGood)
            param.correctType = 3;
        else{
            console.log("同时选择了优秀和错误，错误！！！！");
            return;
        }
        ApiActivity.updateTypicalCase(param, this, function (flag, data) {
            if(flag) {
//                Information.show(1,"操作成功");
                html5SendSystemInfoToNative("操作成功")
            } else {
//                Information.show(2,data.msg);
                 html5SendSystemInfoToNative(data.msg.toString())
            }
//                hideLoading();
        });
    };
