/**
 * name :活动名称 param {'lessonId': '课程id', 'libId': '活动id'}
 */
var PreviewActivity = function () {
  this.view = $('.pre-container');
  this.getData();
};

PreviewActivity.prototype.getData = function () {
  var param = {};
  param.lessonId = lessonId;
  param.libId = libId;
  PreviewActivityHandler.operate(param, this, this.getDataCallBack);
};

PreviewActivity.prototype.getDataCallBack = function (flag, data) {
  if (flag) {
    this.data = data.data;
    this.init();
  } else {
    html5SendSystemInfoToNative(JSON.stringify(data));
  }
};

PreviewActivity.prototype.init = function () {
  // this.view.append($('<p class="pre-act-name">' + this.actName + '</p>'));
  var examCount = 1;
  for (var i = 0; i < this.data.length; i++) {
    switch (this.data[i].type) {
      case 1:
        var temp = new ExamPreview(JSON.parse(this.data[i].examText),this.data[i].score, examCount + '.');
        var tempBox = $('<div class="exam-scan question-item"></div>');
        tempBox.append(temp.view);

        this.view.append(tempBox);
        examCount++;
        break;
      case 2:
        var tempRes = this.initResView(this.data[i]);
        this.view.append(tempRes);
        break;
      case 3:
        this.checkStr(this.view, this.data[i].resourceText);
        break;
      default:
        console.log('不能判断数据类型');
    }
  }

};

PreviewActivity.prototype.checkStr = function (obj, str) {
  if (str == '<br>' || str == '<p><br/></p>' || str == '<p></p>') {

  } else {
    obj.append($('<div>'+str+'</div>'));
  }
};

PreviewActivity.prototype.initResView = function (data) {
  data = JSON.parse(data.resourceText);
  this.resView = $('<div><div class="inner-res clearfix" sid="inner-res">' +
    '    <span class="pic-back" sid="pic"></span>' +
    '    <span class="name" sid="name"></span>' +
    '    <span class="" sid="error"></span>' +
    '</div></div>');
  this.resView.find('[sid=name]').text(data.resourceName);
  this.resView.find('[sid=name]').attr('title', data.resourceName);
  this.picView = this.resView.find('[sid=pic]');
  this.initType(data.resourceType, this.picView);
  this.errorView = this.resView.find('[sid=error]');
  if (data.convertStatus == 1) {
    this.errorView.addClass('ing')
  } else if (data.convertStatus == 3) {
    this.errorView.addClass('error')
  } else {
  }
  this.resView.find('[sid=inner-res]').on('click', function () {
    html5ScanResource(data);
  });
  return this.resView;
};

PreviewActivity.prototype.initType = function (type, view) {
  switch (type) {
    case 5:
      view.addClass('music');
      break;
    case 4:
      view.addClass('pic');
      break;
    case 3:
      view.addClass('mp4');
      break;
    case 2:
      view.addClass('pdf');
      break;
    case 1:
      view.addClass('word');
      break;
  }
};

