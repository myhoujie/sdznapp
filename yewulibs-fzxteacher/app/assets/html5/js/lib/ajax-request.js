var host,
    access_token,
    refresh_token,
    accountId,
    answerTime,
    teacherId,
    studentId,
    levelId,
    chapterName,//章节名称
    chapterId,//章节ID
    baseSubjectId,//学科ID
    status,//场景 全部不传值 只看待完成传 2
    lessonName,
    lessonId,
    model,
    page,
    rows, libId;

var requestArr = [];
var editIdArray = [];

function refreshToken(options) {
    var param = {};
    param.accountId = parem.accountId;
    param.refresh_token = parem.refresh_token;
    $.ajax({
        type: "get",
        url: host + "login/refresh/token/teacher/pad",
        data: param,
        dataType: 'json',
        success: function (data) {
            if (data.code === 0) {
                parem.access_token = data.data.access_token;
                parem.refresh_token = data.data.refresh_token;
                //setAppMessage();
                setAppMessage(JSON.stringify({
                    "access_token": parem.access_token,
                    "refresh_token": parem.refresh_token
                }));
                $.ajax(options)
            } else {
                alert('刷新token失败！');
            }
        },
        error: function (err) {
            alert(err.toString());
        }
    });
}


function doAjax(options) {

    if (!options.dataType) {
        options.dataType = 'json';
    }
    if (!options.data) {
        options.data = {};
    }
    if (parem != undefined)
        host = parem.host;
    options.url = host + options.url;

    if (parem != undefined)
        access_token = parem.access_token;
    options.data.access_token = access_token;

    var successFun = options.success;
    options.success = function (data) {
        if (data.code == 20005) {
            console.log('token失效  去刷新token,token=' + access_token);
            var tempOption = {};
            tempOption.key = JSON.stringify(checkArray(new Date().getTime()));
            tempOption.value = options;
            requestArr.push(tempOption);
            mobileRefreshTokenFun(access_token, tempOption.key);
            return;
        }
        successFun(data);
    };

    var errorFun = options.error;
    options.error = function (data) {
        errorFun(data);
    };


    if (!options.beforeSend) {
        options.beforeSend = function () {
            // GlobalLoad.show();
          showLoading();
        };
    }

    if (!options.complete) {
        options.complete = function () {
            // GlobalLoad.hide();
          hideLoading();
        };
    }
    options.async = true;
    options.crossDomain = true;
    $.ajax(options);
}

var GlobalLoad = function () {
};

GlobalLoad.loadingTemplate = $('<div class="global-loading"></div>');

GlobalLoad.show = function () {
    $("body").append(GlobalLoad.loadingTemplate);
};

GlobalLoad.hide = function () {
    GlobalLoad.loadingTemplate.remove();
};

function getUrlParam(name) {

    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg); // 匹配目标参数
    if (r != null) {
        return unescape(r[2]);
    }
    return null; // 返回参数值
}

function formatDate(dateTime, fmt) {
    const date = new Date(dateTime)
    const o = {
        'M+': date.getMonth() + 1, // 月份
        'd+': date.getDate(), // 日
        'h+': date.getHours(), // 小时
        'm+': date.getMinutes(), // 分
        's+': date.getSeconds(), // 秒
        'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
        'S': date.getMilliseconds() // 毫秒
    }
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    for (var k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
    return fmt
}

var learningGuide,
    collectionExam,
    errorExam;

//初始化调用获取所有需要参数
function getAllParams(options) {
    options = $.extend({
        token: ""
    }, options);
    host = options.host ? options.host : '';
    access_token = options.access_token ? options.access_token : '';
    refresh_token = options.refresh_token ? options.refresh_token : '';
    accountId = options.accountId ? options.accountId : '';
    lessonId = options.lessonId ? options.lessonId : '';
    lessonName = options.lessonName ? options.lessonName : '';
    answerTime = options.answerTime ? options.answerTime : '';
    lessonTaskId = options.lessonTaskId ? options.lessonTaskId : '';
    lessonTaskStudentId = options.lessonTaskStudentId ? options.lessonTaskStudentId : '';
    userStudentId = options.userStudentId ? options.userStudentId : '';
    userStudentName = options.userStudentName ? options.userStudentName : '';
    chapterId = options.chapterId ? options.chapterId : '';
    baseSubjectId = options.baseSubjectId ? options.baseSubjectId : '';
    status = options.status ? options.status : '';
    model = options.model ? options.model : '';
    page = options.page ? options.page : 1;
    rows = options.rows ? options.rows : 30;
    examTemplateStyleId = options.examTemplateStyleId ? options.examTemplateStyleId : '';
    timeType = options.timeType ? options.timeType : '';
    count = options.count ? options.count : '';
    chapterName = options.chapterName ? options.chapterName : '';
    libId = options.libId ? options.libId : '';
}

function set_focus(el) {
    var range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}

//更新综合题答题进度（前端）
function updateComStatus() {
    for (var i = 0; i < learningGuide.examArr.length; i++) {
        if (JSON.parse(learningGuide.examArr[i].allData.examText).examTypeId == 16) {
            learningGuide.examArr[i].answerCallBack();
        }
    }
}

function formatSeconds(value) {
    var theTime = parseInt(value);// 秒
    var theTime1 = 0;// 分
    var theTime2 = 0;// 小时
// alert(theTime);
    if (theTime > 59) {
        theTime1 = parseInt(theTime / 60);
        theTime = parseInt(theTime % 60);
// alert(theTime1+"-"+theTime);
        if (theTime1 > 59) {
            theTime2 = parseInt(theTime1 / 60);
            theTime1 = parseInt(theTime1 % 60);
        }
    }
    var result = "" + parseInt(theTime) + " ";
    if (theTime1 > 0) {
        result = "" + parseInt(theTime1) + ":" + result;
    } else {
        result = "00:" + result;
    }
    if (theTime2 > 0) {
        result = "" + parseInt(theTime2) + ":" + result;
    } else {
        result = "00:" + result;
    }
    return result;
}

//调取移动端刷新token的方法
function mobileRefreshTokenFun(token, key) {
    var data = {
        "token": token,
        "key": key
    };
    try {
        window.webkit.messageHandlers.mobileRefreshTokenFun.postMessage(data);
    } catch (e) {
        console.log(e)
    }
    try {
        interfaceName.mobileRefreshTokenFun(token, key);
    } catch (e) {
        console.log(e)
    }
};

//移动端刷新完成后的回调 flag：true:成功！ false：失败！
function refreshH5TokenFun(flag, token, key) {
    var mOptions = null;
    for (var i = 0; i < requestArr.length; i++) {
        if (requestArr[i].key == key) {
            mOptions = requestArr[i].value;
        }
    }
    if (flag) {
        access_token = token;
        mOptions.data.access_token = token;
        $.ajax(mOptions);
    } else {
        console.log('移动端刷新token失败!');
    }
};

function checkArray(id) {
    if (editIdArray.indexOf(id) > -1) {
        //找到（不可用）
        return checkArray(new Date().getTime());
    } else {
        //可用
        editIdArray.push(id);
        return id;
    }
};
