var Utils = function () {
};

/**
 * 忘记密码跳转
 */
Utils.passWordRedirect = function () {

    window.passWordRedirect();
};

/**
 * 登陆成功
 */
Utils.loginOk = function (s) {

    window.loginOk(s);
};


Utils.getSubjectInfo = function (s) {
    try {
        window.getSubjectInfo(s);
    } catch (e) {
        console.log(1);
    }
};

Utils.getCookies = function (str) {
    var cok = $.cookie(str);
    if (typeof(cok) == "undefined") {
        if (str == "subject")
            return {id: "-1", name: "无"};
        return "";
    }
    return cok;
};

//获取开发模式 pc web
Utils.getMode = function () {
    return localStorage.getItem("developMode");
};
Utils.getTimes = function () {
    return "n=" + new Date().getTime();
};
function dataFormat(tigger) {
    $(tigger).daterangepicker({
        //"autoUpdateInput":false,
        "autoUpdateInput": false,
        "singleDatePicker": true,
        "opens": "center",
        "locale": {
            format: "YYYY-MM-DD",
            applyLabel: '确认',
            cancelLabel: '取消',
            fromLabel: '从',
            toLabel: '到',
            weekLabel: 'W',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        }
    }, function (start, end, label) {
        this.autoUpdateInput = true;
    });
};

function dataDoubleFormat(tigger) {
    $(tigger).daterangepicker({
        "autoUpdateInput": false,
        "showISOWeekNumbers": true,
        "timePickerIncrement": 2,
        "autoApply": true,
        "opens": "center",
        "locale": {
            format: "YYYY-MM-DD",
            applyLabel: '确认',
            cancelLabel: '取消',
            fromLabel: '从',
            toLabel: '到',
            weekLabel: 'W',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
            monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        }
    }, function (start, end, label) {
        this.autoUpdateInput = true;
    })
};

Array.prototype.indexOf = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};

Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]);
    return null; //返回参数值
}