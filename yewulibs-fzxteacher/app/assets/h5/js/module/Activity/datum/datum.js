var Datum = function (lessonTaskId) {
    this.lessonTaskId = lessonTaskId;
    this.view = $('[cid=container]');
    this.initView();
};


Datum.prototype.initView = function () {

    this.view.load('html/activity/activity_detail/resource_analyse/resource_analyse.html', this.init.bind(this))
};

Datum.prototype.init = function () {
    this.svg = this.view.find("[sid=datum_svg]");
    this.list = this.view.find("[sid=left_list]");
    this.rightBox = this.view.find("[sid=datum_right]");
    //初始化页面组件及结构
    this.getResourceList();
    // this.svg.append(this.html.clone(true));
    this.path = this.view.find("[sid=path]");
    //处理页面数据及绘制内容
};

Datum.prototype.getResourceList = function () {
    var param = {};
    // param.lessonTaskId = this.lessonTaskId; TODO
    param.lessonTaskId = this.lessonTaskId;
    param.type = 2;
    param.parentId = 0;
    ApiActivity.getResourceList(param, this, this.detailResourceList);
};

Datum.prototype.detailResourceList = function (flag, res) {
    if (flag) {
        if (res.data.length && res.data.length > 0) {
            var arr = [];
            for (var i = 0, len = res.data.length; i < len; i++) {

                arr.push(new ListSelectItem(this.list, res.data[i], this.lessonTaskId, this.rightBox, this.path, this.html, this.svg));
            }
            arr[0].view.trigger('click');
        } else {
            console.log('此处加载空页面！');
        }
    } else {
        Information.show(2, '获取资源列表失败');
    }
};

Datum.prototype.html = $(
    '<svg viewBox="0 0 100 100">' +
    '        <defs>' +
    '        <linearGradient id="orange_yellow" x1="0" y1="1" x2="0" y2="0">' +
    '        <stop offset="0%" stop-color="#68C426"></stop>' +
    '        <stop offset="100%" stop-color="#68C426"></stop>' +
    '        </linearGradient>' +
    '        </defs>' +
    '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#eaeef2"' +
    '    stroke-width="5" fill-opacity="0"></path>' +
    '        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"' +
    '    stroke-linecap="round" stroke="url(#orange_yellow)" stroke-width="6"' +
    '    fill-opacity="0" style="stroke-dasharray: 300px, 300px;stroke-dashoffset: 300px;transition: stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease;" sid="path"></path>' +
    '        </svg>' +
    '        <div class="mi-box-label">' +
    '        <div class="mi-box-progress" style="margin-top:10px;">' +
    '        <span class="progress-now"  sid="submit_count_svg"></span>' +
    '        <span>/</span>' +
    '        <span class="progress-total" sid="all_count_svg"></span>' +
    '        </div>' +
    '        <div class="mi-box-status">' +
    '        </div>' +
    '        </div>')
