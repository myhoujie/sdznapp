/**
 * 活动详情页
 * mode = 0活动进入，1课程中进入，面包屑会有不同。课程中进入的应该有lesssonData。
 * 〈功能详细描述〉
 * @param  [参数1]   [参数1说明]
 * @param  [参数2]   [参数2说明]
 * @return [返回类型说明]
 * @exception/throws [违例类型] [违例说明]
 * @see          [类、类#方法、类#成员]
 * @deprecated
 */

function main(pem) {
    if (param.host == undefined) {
        alert("host 错误!");return ;
    } else if (param.access_token == undefined) {
        alert("access_token 错误!");
        return ;
    } else if (param.data.classId == undefined) {
        alert("classId 错误!");
        return ;
    } else if (param.data.id == undefined) {
        alert("id 错误!");
        return ;
    }else {
        new ActivityDetail(0, null, pem.data, pem.type, null, pem);
    }
}
////引用
//var Navigation = require('../Activity/navigation/navigation.js');
//var Analyze = require('../Activity/analyze/analyze.js');
//var Datum = require('../Activity/resource_analyse/resource_analyse.js');
//var Representative = require('../Activity/representative/representative.js');
//var Steamed = require('../Activity/steamed/steamed.js');
//var Correct = require('../Activity/correct/correct.js');
//var DetailCrumb = require('../Activity/activity_detail/detail_crumb.js');
//var Transcript = require('../Activity/transcript/transcript.js');
//
//var Nav = require('../Activity/activity_detail/nav.js');
//var ViewModelNav = require('../Activity/activity_detail/view_model_nav.js');

/*model =0 lessonData=null activityData= JSON navCode=null navData=null*/
var parem;
var ActivityDetail = function (model, lessonData, activityData, navCode, navData, pem) {
    this.view = $('body');
    //用于课程和活动跳转过来，面包屑不同的处理。0-活动，1-lesson::暂时不做处理
    this.model = model;
    this.lessonData = lessonData;
    this.activityData = activityData;
    this.navCode = navCode;
    this.navData = navData;
    parem = pem;

    //面包屑
    this.crumbView = null;
    this.crumb = null;

    //导航
    this.navView = null;
    this.nav = null;

    //二级导航（整体+分组）
    this.viewModelNavView = null;
    this.viewModelNav = null;


    //刷新按钮
    this.refreshButton = null;
    this.initView();
};

ActivityDetail.prototype.initView = function () {
    this.view.load('html/activity/activity_detail.html', this.init.bind(this));
};

ActivityDetail.prototype.init = function () {
    //构建面包屑
    this.crumbView = this.view.find('[sid=crumb]');
    this.crumb = new DetailCrumb(this.crumbView, this.model, this.lessonData, this.activityData);
    this.crumbView.css("display", "none")
    //构建导航并选择默认值
    this.navView = this.view.find('[sid=nav]');
    this.nav = new Nav(this.navView, this.navCode);
    this.nav.addListener(this, this.onNavChange);

    //刷新按钮+事件
    this.refreshButton = this.view.find('[sid=refresh]');
    this.refreshButton.on('click', this, this.onRefresh);

    //成绩单二级导航view，并且初始化，如果有选择，则使用选择项，如果没有，使用default
    this.viewModelNavView = this.view.find('[sid=view_model_nav]');
    var navCode = this.nav.getValue();
    if (navCode == 'transcript' && this.navData != null && this.navData != undefined)
        this.viewModelNav = new ViewModelNav(this.viewModelNavView, this.navData.viewModel);
    else
        this.viewModelNav = new ViewModelNav(this.viewModelNavView);
    //添加监听
    this.viewModelNav.addListener(this, this.onViewModelChange);

    //如果是成绩单，则显示分类,反之不显示;并执行绘制内容区
    if (navCode == 'transcript') {
        this.toggleObj(this.viewModelNavView, true);
        this.updateContent(this.nav.getValue(), this.viewModelNav.getValue());
    }
    else {
        this.toggleObj(this.viewModelNavView, false);
        this.updateContent(this.nav.getValue());
    }

    //navData只有初始化的时候使用，之后失效。
    this.navData = null;
};

ActivityDetail.prototype.toggleObj = function (obj, flag) {
    if (flag)
        obj.show();
    else
        obj.hide();
}

//当一级导航栏发生改变时
ActivityDetail.prototype.onNavChange = function (navCode) {
    if (navCode == null || navCode == undefined)
        return;
    //如果是成绩单，则显示分类,反之不显示;并执行第一次查询
    if (navCode == 'transcript') {
        //显示成绩单二级导航
        this.toggleObj(this.viewModelNavView, true);
        //二级导航切换回defaultValue
        this.viewModelNav.reset();
        //执行绘制内容区
        var viewModel = this.viewModelNav.getValue();
        if (viewModel != null && viewModel != undefined)
            this.updateContent(navCode, viewModel);
    }
    else {
        //隐藏二级导航
        this.toggleObj(this.viewModelNavView, false);
        //执行绘制内容区
        this.updateContent(navCode);
    }
};

ActivityDetail.prototype.onViewModelChange = function (viewModel) {
    var navCode = this.nav.getValue();

    if (navCode == null || navCode == undefined)
        return;
    //如果是成绩单
    if (navCode == 'transcript') {
        //执行绘制内容区
        if (viewModel != null && viewModel != undefined)
            this.updateContent(navCode, viewModel);
    }
    //执行绘制内容区
    else
        this.updateContent(navCode);
}

ActivityDetail.prototype.onRefresh = function (evt) {
    var self = evt.data;
    var navCode = self.nav.getValue();
    if (navCode == null || navCode == undefined)
        return;
    if (navCode == 'transcript') {
        //重置+查询:
        // self.viewModelNav.reset();
        var viewModel = self.viewModelNav.getValue();
        if (viewModel != null && viewModel != undefined)
            self.updateContent(navCode, viewModel);
    }
    else
        self.updateContent(navCode);
};

ActivityDetail.prototype.updateContent = function (navCode, viewModel) {
    var that = this;
    console.log(navCode, viewModel);
    switch (navCode) {
        case "transcript"://成绩单
            if (this.navData != null && this.navData != undefined)
                new Transcript(viewModel, this.navData.groupId, that.model, that.lessonData, that.activityData);
            else
                new Transcript(viewModel, null, that.model, that.lessonData, that.activityData);
            break;
        case "resource_analyse"://资料分析
            new Datum(that.activityData.id);
            break;
        case "analyze"://题目分析
            new Analyze(that.activityData);
            break;
        case "representative"://讲解典型
            new Representative(that.activityData.id);
            break;
        case "steamed"://卷面
            new Steamed(that.activityData);
            break;
        case "correct"://批改
            new Correct(that.activityData);
            break;
        default:
    }
};
//});