/**
 * Created by zhangxia on 2018/1/8.
 */
    var Transcript = function (viewModel, groupId, model, lessonData, activityData) {
        //viewModel :global,group对应分组查看还是整体查看
        this.view = $('[cid=container]');
        this.viewModel = viewModel;
        this.groupId = groupId;
        this.model = model;
        this.lessonData = lessonData;
        this.activityData = activityData;

        this.groupNavView = null;
        this.groupNav = null;

        //右侧统计人数view
        this.censusView = null;

        //左侧table区
        this.contentView = null;
        this.initView();
    }


    Transcript.prototype.initView = function () {
        var that = this;
        this.view.load('html/activity/activity_detail/transcript/transcript.html', function () {
            that.init();
        })
    };

    Transcript.prototype.init = function () {
        //初始化分组
        this.groupNavView = this.view.find('[sid=group_nav]');
        this.globalContentView = this.view.find('[sid=global_content]');
        this.contentView = this.view.find('[sid=content]');
        this.tableContentView = this.contentView.find('[sid=table_content]');
        var that = this;
        $(window).resize(function(){
            that.setHeight(that.tableContentView);
            that.setHeight(that.contentView);
        });
        this.setHeight(this.tableContentView);
        this.setHeight(this.contentView);

        this.graphView = this.view.find('[sid=graph]');

        //创建分组，如果是整体，则删除分组view
        if (this.viewModel == 'group') {
            this.groupNav = new GroupNav(this.groupNavView, this.activityData.classId, this.groupId);
            this.groupNav.addListener(this, this.onGroupChange);

            //数据加载完后，根据选择小组初始绘制下方区域
            var that = this;
            this.groupNav.addLoadedListener(this, function (groupId) {
                if(groupId != null)
                {
                    that.createContent(that.activityData.id, groupId);
                }
                else {
                    that.groupNavView.hide();
                    that.setBlankPage(true);
                }
                that.setHeight(that.tableContentView);
                that.setHeight(that.contentView);
            });
        }
        else {
            //删除分组区域
            this.groupNavView.remove();
            this.contentView.hide();
            this.graphView.hide();
            //查询整体，绘制下方区域
            this.createContent(this.activityData.id);
        }
    };


    Transcript.prototype.createContent = function (activityId, groupId) {
        //整体
        var that = this;
        if (groupId == null || groupId == undefined) {
            //生成表格
            var param = {};
            param.lessonTaskId = activityId;
            param.scoreOrder = 0;
            param.classId = this.activityData.classId;
            console.log(param);
            ApiActivity.classTranscript(param, this, function (flag, data) {
                if (flag) {
                    if (data != null && data != undefined) {
                        if(data.data.length == 0)
                            this.setBlankPage(true);
                        else
                            that.createTable(data.data);
                    }
                    else {
                        alert("返回数据为NULL");
                        that.createTable([]);
                    }
                }
                else {
                    alert(data.msg);
                    that.createTable([]);
                }
            });

            //处理右侧chart表
            var param = {};
            param.id = activityId;
            ApiActivity.completion(param, this, function (flag, data) {
                if (flag) {
                    if (data != null && data != undefined) {
                        that.createChartContent(0, data.data);
                    }
                    else {
                        alert("返回数据为NULL");
                        that.createChartContent(0, {});
                    }
                }
                else {
                    alert(data.msg);
                    that.createChartContent(0, {});
                }

            });
        }
        //分组查看
        else {
            console.log('Group查询' + groupId);
            //生成表格
            var param = {};
            param.lessonTaskId = activityId;
            param.scoreOrder = 0;
            param.classId = this.activityData.classId;
            param.classGroupId = groupId;
            console.log(param);
            ApiActivity.groupTranscript(param, this, function (flag, data) {
                if (flag) {
                    if (data != null && data != undefined) {
                        if (data.data[0] != null && data.data[0] != undefined
                            && data.data[0].students != null && data.data[0].students != undefined)
                                that.createTable(data.data[0].students);
                        else
                            that.createTable([]);
                    }
                    else {
                        alert("返回数据为NULL");
                        that.createTable([]);
                    }
                }
                else {
                    alert(data.msg);
                    that.createTable([]);
                }

            });

            //处理右侧chart
            var param = {};
            param.id = activityId;
            param.classGroupId = groupId;
            ApiActivity.completion(param, this, function (flag, data) {
                if (flag) {
                    if (data != null && data != undefined) {

                        that.createChartContent(1, data.data);
                    }
                    else {
                        alert("返回数据为NULL");
                        that.createChartContent(1, {});
                    }
                }
                else {
                    alert(data.msg);
                    that.createChartContent(1, {});
                }

            });
        }

    };
    Transcript.prototype.createTable = function (data) {
        this.tableContentView.empty();
        if(data.length == 0)
            this.setBlankPage(true);
        else
            this.setBlankPage(false);
        var that = this;
        $(data).each(function (key, value) {
            var tableLabel = new StudentLabel(that.tableContentView, value, key + 1);
            tableLabel.addListener(that, that.onViewStudent);
        });
    };

    Transcript.prototype.createChartContent = function (type, data) {

        if (data.complete == null || data.complete == undefined)
            data.complete = 0;
        if (data.unAnswer == null || data.unAnswer == undefined)
            data.unAnswer = 0;
        if (data.answer == null || data.answer == undefined)
            data.answer = 0;
        if (data.replenish == null || data.replenish == undefined)
            data.replenish = 0;
        if (data.scope == null || data.scope == undefined)
            data.scope = 0;
        if (data.scoreAvg == null || data.scoreAvg == undefined)
            data.scoreAvg = 0;

        //绘制图表chart
        var chartArray = [];
        if (data.status == 0) {
            var data1 = {};
            data1.value = data.complete;
            data1.name = '已完成';
            chartArray.push(data1);

            var data2 = {};
            data2.value = data.answer;
            data2.name = '作答中';
            chartArray.push(data2);

            var data3 = {};
            data3.value = data.unAnswer;
            data3.name = '未开始';
            chartArray.push(data3);
        }
        else {
            var data1 = {};
            data1.value = data.complete;
            data1.name = '已完成';
            chartArray.push(data1);

            var data2 = {};
            data2.value = data.answer + data.unAnswer;
            data2.name = '未完成';
            chartArray.push(data2);

            var data3 = {};
            data3.value = data.replenish;
            data3.name = '补交';
            chartArray.push(data3);
        }

        var option = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                selectedMode: false,
                data: ['', '', '', '', '']
            },
            series: [
                {
                    name: '完成情况',
                    type: 'pie',
                    radius: ['50%', '70%'],
                    avoidLabelOverlap: true,
                    color: ['#68c426', '#24BBF4', '#FF6D4A'],
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '20',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data: chartArray
                }
            ]
        };
        myChart.setOption(option);

        //绘制各种人数
        this.censusView = this.view.find('[sid=census]');
        this.censusView.empty();
        if (data.status == 0) {
            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('已完成');
            censusLabel.find('[sid=status]').addClass('number_text_full');
            censusLabel.find('[sid=person_count]').text(data.complete);
            censusLabel.find('[sid=person_count]').css("color", "#68C426");
            this.censusView.append(censusLabel);

            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('作答中');
            censusLabel.find('[sid=status]').addClass('number_text_unfinished');
            censusLabel.find('[sid=person_count]').text(data.answer);
            censusLabel.find('[sid=person_count]').css("color", "#24BBF4");
            this.censusView.append(censusLabel);

            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('未开始');
            censusLabel.find('[sid=status]').addClass('number_text_replenish');
            censusLabel.find('[sid=person_count]').text(data.unAnswer);
            censusLabel.find('[sid=person_count]').css("color", "#FF6D4A");
            this.censusView.append(censusLabel);
        }
        else {
            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('已完成');
            censusLabel.find('[sid=status]').addClass('number_text_full');
            censusLabel.find('[sid=person_count]').text(data.complete);
            censusLabel.find('[sid=person_count]').css("color", "#68C426");
            this.censusView.append(censusLabel);

            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('未完成');
            //缺少Class，支持颜色的变化
            censusLabel.find('[sid=status]').addClass('number_text_unfinished');
            if (data.answer == null || data.answer == undefined || data.unAnswer == null || data.unAnswer == undefined)
                censusLabel.find('[sid=person_count]').text();
            else
                censusLabel.find('[sid=person_count]').text(data.answer + data.unAnswer);
            censusLabel.find('[sid=person_count]').css("color", "#24BBF4");
            this.censusView.append(censusLabel);

            var censusLabel = $(Transcript.censusLabelTemplate).clone(true);
            censusLabel.find('[sid=status]').text('补交');
            //缺少Class，支持颜色的变化
            censusLabel.find('[sid=status]').addClass('number_text_replenish');
            censusLabel.find('[sid=person_count]').text(data.replenish);
            censusLabel.find('[sid=person_count]').css("color", "#FF6D4A");
            this.censusView.append(censusLabel);
        }
        //绘制平均分+总分
        if (type == 0)
            this.view.find('[sid=type]').text('班级');
        else
            this.view.find('[sid=type]').text('小组');

        this.view.find('[sid=average]').text(data.scoreAvg);
        this.view.find('[sid=total]').text(data.scope);
    }

    Transcript.prototype.onGroupChange = function (groupId) {
        if (groupId == null || groupId == undefined) {
            alert("未选择任何小组！")
            return;
        }
        this.createContent(this.activityData.id, groupId);
    };

    //点击学生详情
    Transcript.prototype.onViewStudent = function (data) {
        //缺少传值参数
        var navData = {};
        navData.viewModel = this.viewModel;
        if (this.viewModel == 'group')
            navData.groupId = this.groupNav.getValue();
        this.studentDetail = new StudentDetail(data, this.model, this.lessonData, this.activityData, 'transcript', navData);
    };

    Transcript.prototype.setBlankPage = function (flag) {
        if(flag)
        {
            this.contentView.hide();
            this.graphView.hide();
            this.globalContentView.append($(this.blankTemplate).clone(true));
        }
        else {
            this.contentView.show();
            this.graphView.show();
            this.globalContentView.find('[sid=blank]').remove();
        }
    };

    Transcript.prototype.setHeight=function(obj){
        var repre_right_top=obj.offset().top;
        var heig=$(document).height()-repre_right_top;
        obj.height(heig);
    };

    Transcript.censusLabelTemplate = '<div class="number"> ' +
        '<span class="" sid="person_count"></span> ' +
        '<div class="number_text_full" sid="status"></div> ' +
        '</div>';

    Transcript.prototype.blankTemplate = '<div class="mi-blank-container" sid="blank" style="height:100%;"> ' +
        '<div class="mi-blank"> ' +
        '<div class="mi-blank-label">暂无内容~</div> ' +
        '</div> ' +
        '</div>';
