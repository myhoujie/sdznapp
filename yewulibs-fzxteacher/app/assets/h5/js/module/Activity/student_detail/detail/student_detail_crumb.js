/**
 * Created by ming on 2018/2/7.
 */
/**
 * Created by ming on 2018/1/31.
 */
//model = 0 从活动中进入的面包屑
// model = 1 从课程中进入的面包屑。
//model == 2 从首页进入的面包屑
//前置加载导致循环依赖
    var StudentDetailCrumb = function (view, model, lessonData, activityData, navCode, navData) {
        this.view = view;
        this.model = model;
        this.lessonData = lessonData;
        this.activityData = activityData;
        this.navCode = navCode;
        this.navData = navData;

        this.lessonIndexView = null;
        this.activityIndexView = null;
        this.lessonView = null;
        this.activityView = null;
        this.studentDetailView = null;
        this.init();
    };


    StudentDetailCrumb.prototype.init = function () {
        this.view.empty();
        if (this.model == 0) {
            //教学活动index页
            this.activityIndexView = $(StudentDetailCrumb.linkTemplate).clone(true);
            this.activityIndexView.text('教学活动');
            this.activityIndexView.on('click', {'obj': this, 'type': 'activity_index'}, this.onClickCrumb);
            this.view.append(this.activityIndexView);
        }
        if(this.model == 1) {
            //课程首页
            this.LessonIndexView = $(StudentDetailCrumb.linkTemplate).clone(true);
            this.LessonIndexView.text('我的课程');
            this.LessonIndexView.on('click', {'obj': this, 'type': 'lesson_index'}, this.onClickCrumb);
            this.view.append(this.LessonIndexView);
            //分隔号
            this.view.append($(StudentDetailCrumb.splitTemplate).clone(true));
            //课程
            this.LessonView = $(StudentDetailCrumb.linkTemplate).clone(true);
            this.LessonView.text(this.activityData.lessonName);
            this.LessonView.on('click', {'obj': this, 'type': 'lesson'}, this.onClickCrumb);
            this.view.append(this.LessonView);
        }
        if(this.model == 2){
            //教学活动index页
            this.activityIndexView = $(DetailCrumb.linkTemplate).clone(true);
            this.activityIndexView.text('首页');
            this.activityIndexView.on('click',{'obj':this,'type':'index'},this.onClickCrumb);
            this.view.append(this.activityIndexView);
        }
        //分隔号
        this.view.append($(StudentDetailCrumb.splitTemplate).clone(true));
        //教学活动名称
        this.activityView = $(StudentDetailCrumb.linkTemplate).clone(true);
        this.activityView.text(this.activityData.name);
        this.activityView.on('click', {'obj': this, 'type': 'activity'}, this.onClickCrumb);
        this.view.append(this.activityView);
        //分隔号
        this.view.append($(StudentDetailCrumb.splitTemplate).clone(true));
        //学生详情
        this.studentDetailView = $(StudentDetailCrumb.normalTemplate).clone(true);
        this.studentDetailView.text('学生详情');
        this.view.append(this.studentDetailView);
    };

    StudentDetailCrumb.prototype.onClickCrumb = function (evt) {
        var self = evt.data.obj;
        var type = evt.data.type;
        switch (type) {
            case 'index':
                //暂时不处理
                break;
            case 'activity_index':
                // module.activityLoader(that);
                var Activity = require("../../activity.js");
                new Activity();
                break;
            case 'lesson_index':
                var Lesson = require('../../../myLesson/lesson.js');
                new Lesson(self.lessonData.myLessonObj.lessonObj.menuObj);
                break;
            case 'lesson':
                var LessonDetail = require('../../../myLesson/lesson-details.js');
                new LessonDetail(self.lessonData);
                break;
            case 'activity':
                var ActivityDetail = require('../../activity_detail.js');
                new ActivityDetail(self.model, self.lessonData, self.activityData, self.navCode, self.navData);
                break;
            default:
                break;
        }
    };
    StudentDetailCrumb.linkTemplate = '<a></a>';
    StudentDetailCrumb.normalTemplate = '<span></span>';
    StudentDetailCrumb.splitTemplate = '<span> > </span>';
