/**
 * Created by ming on 2018/3/15.
 */

    //model=1是显示大题对错，model=0或者不传值代表大题不显示对错
    var TitleNav = function (view, datas, model) {
        this.view = view;
        this.datas = datas;
        this.model = model;

       /* this.datas = [{
            "templateId": 6,
            "templateStyleId": 6,
            "templateStyleName": "填空题",
            "isRight": 1,
            "emptyCount": 3,
            "examSeq": 1,
            "examOptionList": [
                {
                    "id": 536,
                    "lessonAnswerExamId": 1641,
                    "isRight": 1,
                    "score": 1,
                    "myAnswer": "错误",
                    "myAnswerHtml": null,
                    "seq": 1
                }, {
                    "id": 536,
                    "lessonAnswerExamId": 1641,
                    "isRight": 1,
                    "score": 1,
                    "myAnswer": "错误",
                    "myAnswerHtml": null,
                    "seq": 2
                }, {
                    "id": 536,
                    "lessonAnswerExamId": 1641,
                    "isRight": 1,
                    "score": 1,
                    "myAnswer": "错误",
                    "myAnswerHtml": null,
                    "seq": 3
                }
            ]
        }, {
            "templateId": 16,
            "templateStyleId": 16,
            "templateStyleName": "综合题",
            "isRight": 3,
            "examSeq": 2,
            "examList": [
                {
                    "templateId": 1,
                    "templateStyleId": 1,
                    "templateStyleName": "选择题",
                    "isRight": 2,
                    "examSeq": 1,
                }, {
                    "templateId": 1,
                    "templateStyleId": 1,
                    "templateStyleName": "选择题",
                    "isRight": 2,
                    "examSeq": 2,
                }, {
                    "templateId": 6,
                    "templateStyleId": 6,
                    "templateStyleName": "选择题",
                    "isRight": 2,
                    "emptyCount": 3,
                    "examSeq": 3,
                    "examOptionList": [
                        {
                            "id": 536,
                            "lessonAnswerExamId": 1641,
                            "isRight": 1,
                            "score": 1,
                            "myAnswer": "错误",
                            "myAnswerHtml": null,
                            "seq": 1
                        }, {
                            "id": 536,
                            "lessonAnswerExamId": 1641,
                            "isRight": 1,
                            "score": 1,
                            "myAnswer": "错误",
                            "myAnswerHtml": null,
                            "seq": 2
                        }, {
                            "id": 536,
                            "lessonAnswerExamId": 1641,
                            "isRight": 1,
                            "score": 1,
                            "myAnswer": "错误",
                            "myAnswerHtml": null,
                            "seq": 3
                        }]
                }
            ]
        }];*/
        this.init();
    };



    TitleNav.prototype.init = function () {

        var that = this;

        $(this.datas).each(function (key, value) {
            //题目总体
            that.createLine(value.examSeq, null, null, value.templateStyleName, value.isRight);

            //填空题多空
            if (value.templateId == 6 && value.emptyCount > 1)
                $(value.examOptionList).each(function (k, v) {
                    that.createLine(null, null, v.seq, null, v.isRight);
                });
            //    综合题小题
            if (value.templateId == 16) {
                $(value.examList).each(function (k, v) {
                    //如果不是填空题
                    if (v.templateId != 6)
                        that.createLine(null, v.examSeq, null, null, v.isRight);
                    //    如果小题为填空题，但只有一个空,同上处理
                    if (v.templateId == 6 && v.emptyCount == 1)
                        that.createLine(null, v.examSeq, null, null, v.isRight);
                    //    如果为填空题，多于一个空
                    if (v.templateId == 6 && v.emptyCount > 1) {
                        $(v.examOptionList).each(function (k1, v1) {
                            that.createLine(null, v.examSeq, v1.seq, null, v1.isRight);
                        });
                    }
                });
            }

        });
    };


    TitleNav.prototype.createLine = function (bigSeq, smallSeq, blankSeq, templateStyleName, isRight) {
        var phtml = $(this.plate).clone(true);
        var seqNameView = phtml.find('[sid=seq_name]');
        var blankView = phtml.find('[sid=blank]');
        var isRightView = phtml.find('[sid=is_right]');

        //是否显示大题对错标记
        var flag = true;
        //大题（6种）
        if (bigSeq != null && smallSeq == null && blankSeq == null) {
            seqNameView.text(bigSeq + '. ' + templateStyleName);
            flag = false;
        }

        //独立填空题小空
        if (bigSeq == null && smallSeq == null && blankSeq != null) {
            blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
            seqNameView.text(this.getBlankSeq(blankSeq));
        }

        //综合题独立小题，不包含填空小空
        if (bigSeq == null && smallSeq != null && blankSeq == null) {
            blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
            seqNameView.text(this.getSmallSeq(smallSeq));
        }

        //综合题，独立小题，小空
        if (bigSeq == null && smallSeq != null && blankSeq != null) {
            if (blankSeq == 1) {
                blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;");
                seqNameView.text(this.getSmallSeq(smallSeq) + this.getBlankSeq(blankSeq));
            }
            else {
                blankView.html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                seqNameView.text(this.getBlankSeq(blankSeq));
            }
        }

        if (!flag && this.model != 1) {
            //   不显示大题的正确情况
        }
        else {
            //对错信息
            if (isRight == 1)
                isRightView.addClass('succ');
            if (isRight == 2)
                isRightView.addClass('fault');
            if (isRight == 3)
                isRightView.addClass('noright');
        }
        this.view.append(phtml);
    };

    TitleNav.prototype.getSmallSeq = function (num) {
        return '(' + num + ')';
    };

    TitleNav.prototype.getBlankSeq = function (num) {
        return '<' + num + '>';
    };


    TitleNav.prototype.plate = '<div class="item"><span sid="blank"></span><span sid="seq_name"></span> ' +
        '<div class="f_right " sid="is_right"></div> ' +
        '</div>';
