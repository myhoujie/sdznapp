/**
 * Created by ming on 2018/3/15.
 */
//学生作答的题目详情列表
    var AnswerList = function (view, datas) {
        this.view = view;
        this.datas = datas;
        this.init();
    };


    AnswerList.prototype.init = function () {
        var that = this;
        $(this.datas).each(function (key,value) {
            //此处绘制题目相关内容：
            var label = $(that.plate).clone(true);
            var tempExamObj = new ExamAnswerPreview(value,value.examSeq + '.');
            label.find('[sid=box]').append(tempExamObj.view);
            label.find('[sid=answer-btn]').on('click',{'obj':that,'examObj':tempExamObj},that.changeAnswerFun);
            //典型错误
            var typicalCaseView = label.find('[sid=typical_case]');
            new TypicalCase(typicalCaseView,value.id, value.correctType);

            that.view.append(label);
        });
    };

    AnswerList.prototype.changeAnswerFun = function (e) {
      // var self = e.data.obj;
      var examObj = e.data.examObj;
      var flag = e.data.flag;
      if(!$(this).hasClass('active')){
        examObj.showAnswer();
        $(this).addClass('active');
        $(this).text('隐藏答案');
      }else{
        examObj.hideAnswer();
        $(this).removeClass('active');
        $(this).text('显示答案');
      }
    };

    AnswerList.prototype.plate = '<div> ' +
        '<div style="background-color: #ffffff;min-height: 30px;margin: 10px;padding: 10px;position: relative" sid="box"> ' +
        '<div style="position: absolute;right:0px;" class="cus-radio s-d-show-answer-con" sid="typical_case"> ' +
        '<div class="f_left"><span class="cus-radio-con answer-btn" sid="answer-btn">显示答案&nbsp;&nbsp;&nbsp;</span></div> ' +
        '<div class="f_left" sid="typical_case_good"><span class="cus-radio-pic" sid="pic" key="1" ></span><span ' +
        'class="cus-radio-con">优秀解答</span></div> ' +
        '<div class="f_left"  sid="typical_case_bad"><span class="cus-radio-pic" sid="pic" key="1"></span> ' +
        '<span class="cus-radio-con">典型错误</span></div> ' +
        '</div></div>';
