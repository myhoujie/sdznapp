/**
 * Created by wuhai on 2018/2/6.
 */
    var ListSelectItem = function (parentView, data, lessonTaskId, rightBox, path,html,svg) {

        this.parentView = parentView;
        this.data = data;
        this.lessonTaskId =lessonTaskId;
        this.rightBox = rightBox;
        this.path = path;
        this.parentHtml=html;
        this.parentSvg=svg;
        this.init();
    };

    ListSelectItem.template = '<div class="datum_item"><div sid="item"></div><span sid="name"></span></div>';

    ListSelectItem.prototype.init = function () {
        // for (var i = 0, len = this.data.length; i < len; i++) {
        this.view = $(ListSelectItem.template);
        this.addIcon(this.data.resourceType, this.view.find("[sid=item]"));
        this.view.find("[sid=name]").text(this.data.resourceName);
        this.parentView.append(this.view);
        this.view.off('click').on('click', this.handleClick.bind(this));
        // }
    };

    ListSelectItem.prototype.handleClick = function () {
        var param = {};
        param.lessonTaskId = this.lessonTaskId;
        param.resourceId = this.data.resourceId;
        ApiActivity.resourceAnalyse(param, this, this.resourceAnalyse);
    };

    ListSelectItem.prototype.resourceAnalyse = function (flag, res) {

        if (flag&&res.length>0) {
            this.parentSvg.append(this.parentHtml.clone(true));
            this.rightBox.removeClass("dn");
            this.rightBox.find("[sid=submit_count]").text(res.data.countStudentSubmit + '人');
            this.rightBox.find("[sid=submit_count_svg]").text(res.data.countStudentSubmit);
            this.rightBox.find("[sid=all_count_svg]").text(res.data.countStudentTotal);
            this.rightBox.find("[sid=remain_count]").text(res.data.countStudentRemain + '人');
            this.rightBox.find("[sid=avg_time]").text('查看平均时长:' + parseInt(res.data.useTimeAvg / 60) + '分' + res.data.useTimeAvg % 60 + '秒');
            // this.getStudentList(res.data.studentList);
            this.path.css('stroke-dashoffset', parseInt(300 * (res.data.countStudentRemain / res.data.countStudentTotal)) + 'px');
            new StudentListItem(this.rightBox.find("[sid=stu_list]"), res.data.studentList);
        } else {
            this.rightBox.addClass("dn");
            alert('获取资源分析失败');
        }
    };
// ListSelectItem.prototype.getStudentList = function (list) {
//     new StudentListItem(this.rightBox.find("[sid=stu_list]"), list);
// };
    ListSelectItem.prototype.addIcon = function (type, view) {
        switch (type) {
            case 1:
                view.addClass('datum_item_icon icon_doc');
                break;
            case 2:
                view.addClass('datum_item_icon icon_ppt');
                break;
            case 3:
                view.addClass('datum_item_icon icon_mp4');
                break;
            case 4:
                view.addClass('datum_item_icon icon_mp3');
                break;
            case 5:
                view.addClass('datum_item_icon icon_def');
                break;
        }
    };
