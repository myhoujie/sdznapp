/**
 * Created by ming on 2018/2/7.
 // */
/*题号导航列表
* 初始化new ExamNav(view)
* 添加点击监听 addListener
* 获取点击数据 getValue（）
*/
    var ExamNav = function (view, activityId) {
        this.view = view;
        this.activityId = activityId;

        //设置默认数据
        this.defaultValue = -1;
        //navLabel集合
        this.navLabelList = [];
        //选择值
        this.selectLabel = null;

        this.initView();
    };


    ExamNav.prototype.initView = function () {
        this.view.load('html/activity/activity_detail/analyze_nav.html', this.init.bind(this));
    };

    ExamNav.prototype.init = function () {
        this.left = this.view.find("[cid=left]");
        this.right = this.view.find("[cid=right]");
        this.center = this.view.find("[cid=center]");
        //清除
        this.center.empty();
        //创建初始数据
        var param = {};
        param.lessonTaskId = this.activityId;
        console.log(param);
        ApiActivity.examNumberList(param, this, this.handleExamList);
    };

    ExamNav.prototype.handleExamList = function (flag, data) {
        var that = this;
        if (flag) {
            if (data != null && data != undefined) {
                var navArray = data.data;
                $(navArray).each(function (key, value) {
                    var navLabel = new AnalyzeNavItem(that.center, value, key + 1);
                    that.navLabelList.push(navLabel);
                    navLabel.addListener(that, that.onValueChange)
                });
                //设置默认值
                if (navArray.length > 0)
                    this.defaultValue = navArray[0].id;
                //进行初始选择,如果没有初始选额，那么选择默认值
                this.select(this.defaultValue);
            }
            else
                Information.show(2, "返回数据为NULL");
        }
        else
            Information.show(2, data.msg);
        // 初始化事件，并且触发加载完成事件
        this.initEvent();
        if(this.loadedListenerFunc)
            this.loadedListenerFunc.call(this.loadedListener, this.getValue());
    };
    ExamNav.prototype.initEvent = function () {
        this.left.on("click", this, this.leftTiggerEvent);
        this.right.on("click", this, this.rightTiggerEvent);
    }
    ExamNav.prototype.leftTiggerEvent = function (evt) {
        var self = evt.data;
        var w = self.center[0].scrollLeft;
        $(self.center).animate({scrollLeft: w - 200}, 100);
    }
    ExamNav.prototype.rightTiggerEvent = function (evt) {
        var self = evt.data;
        var w = self.center[0].scrollLeft;
        $(self.center).animate({scrollLeft: 200 + w}, 100);
    }

    //触发事件:如果选择项之前选择过则不触发
    ExamNav.prototype.onValueChange = function (id) {
        if (this.select(id))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener, this.getValue());
    };

    //选择某个题目
    ExamNav.prototype.select = function (id) {
        if (this.selectLabel != null && this.selectLabel != undefined)
                this.selectLabel.unSelect();

        //如果id匹配，选择上id对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.navLabelList).each(function (key, value) {
            if (!flag && value.data.id == id) {
                value.select();
                that.selectLabel = value;
                flag = true;
            }
        })
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectLabel = null;
        return false;
    };

    //得到选择值
    ExamNav.prototype.getValue = function () {
        if (this.selectLabel != null && this.selectLabel != undefined)
            return this.selectLabel.data.id;
        //如果未选择任何数据
        return null;
    };

    //得到data值
    ExamNav.prototype.getSelectData = function () {
        if (this.selectLabel != null && this.selectLabel != undefined)
            return this.selectLabel.data;
        //如果未选择任何数据
        return null;
    };

    //监听
    ExamNav.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };
    
    ExamNav.prototype.addLoadedListener = function (loadedListener, loadedListenerFunc) {
        this.loadedListener = loadedListener;
        this.loadedListenerFunc = loadedListenerFunc;
    };
