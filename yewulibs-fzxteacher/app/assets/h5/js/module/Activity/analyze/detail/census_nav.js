/**
 * Created by ming on 2018/2/1.
 */

/**
 * Created by ming on 2018/1/26.
 */
    var CensusNav = function (view, initValue) {
        this.view = view;

        //设置默认数据
        this.defaultValue = 'census';
        //置入初始数据
        this.initValue = initValue;
        //navLabel集合
        this.navLabelList = [];
        //选择值
        this.selectChooserLabel = null;

        this.init();
    };


    CensusNav.prototype.init = function () {
        //    listView盛放按钮View
        this.listView = this.view;
        this.listView.empty();
        //    创建初始数据
        var navArray = [
            {
                'code': 'census',
                'name': ' 统计 '
            }, {
                'code': 'student_info',
                'name': '学生详情'
            }
        ];
        var that = this;
        $(navArray).each(function (key, value) {
            var navLabel = new ChooserLabel(that.listView, value, CensusNav.phtml, function(labelView, name){
                labelView.text(name);
            });
            that.navLabelList.push(navLabel);
            navLabel.addListener(that, that.onValueChange)
        });

        //进行初始选择,如果没有初始选额，那么选择默认值
        if(this.initValue != null && this.initValue != undefined)
            this.select(this.initValue);
        else
            this.select(this.defaultValue);
    };

    //触发事件:如果选择项之前选择过则不触发
    CensusNav.prototype.onValueChange = function (navCode) {
        if (this.select(navCode))
            if (this.listenerFunc)
                this.listenerFunc.call(this.listener, this.getValue());
    };

    //选择某个
    CensusNav.prototype.select = function (navCode) {
        if (this.selectChooserLabel != null && this.selectChooserLabel != undefined) {
            //如果选择项为之前选择项,不做任何改变，返回未执行更新flag。
            if (this.selectChooserLabel.data.code == navCode)
                return false;
            //如果之前有选择，取消之前选择
            if (this.selectChooserLabel != null)
                this.selectChooserLabel.unSelect();
        }

        //如果ChooserCode匹配，选择上ChooserCode对应选项。
        var that = this;
        //记录是否找到元素
        var flag = false;
        $(this.navLabelList).each(function (key, value) {
            if (!flag && value.data.code == navCode) {
                value.select();
                that.selectChooserLabel = value;
                flag = true;
            }
        })
        if (flag)
            return true;
        //如果选项没有对应上，不做任何选择
        this.selectChooserLabel = null;
        return false;
    };

    //恢复到默认值
    CensusNav.prototype.reset = function () {
        this.select(this.defaultValue);
    };

    //得到选择值
    CensusNav.prototype.getValue = function () {

        if (this.selectChooserLabel != null && this.selectChooserLabel != undefined)
            return this.selectChooserLabel.data.code;
        //如果未选择任何数据
        return null;
    };

    //监听
    CensusNav.prototype.addListener = function (listener, listenerFunc) {
        this.listener = listener;
        this.listenerFunc = listenerFunc;
    };

    CensusNav.phtml = '<div class="button"></div>';
