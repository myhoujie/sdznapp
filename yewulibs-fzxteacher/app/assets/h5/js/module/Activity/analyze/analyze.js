    var Analyze = function (activityData) {
        this.view = $('[cid=container]');
        this.activityData = activityData;
        this.initView();

        //题号导航按钮条
        this.examNavView = null;
        this.examNav = null;

        //题目列表
        this.examListView = null;
        this.examList = null;

        this.examTypeView = null;
        this.examPersonView = null;

        this.lastExamId = null;
    };


    Analyze.prototype.initView = function () {
        this.view.load('html/activity/activity_detail/analyze/analyze.html', this.init.bind(this));
    };

    Analyze.prototype.init = function () {

        var that = this;
        //初始化上方题号导航列表
        this.examNavView = this.view.find('[sid=exam_nav]');
        //先隐藏界面，防止空页的跳动。
        this.examNavView.hide();
        this.rightAnaView = this.view.find('[sid=ana_content]');
        this.rightAnaView.hide();

        this.examNav = new ExamNav(this.examNavView, this.activityData.id);
        this.examNav.addListener(this, function (id) {
            that.examList.select(id);
            //    刷新右侧
            this.updateCensus(id);
        });

        //初始化题目列表
        this.examContainerView = this.view.find('[sid=exam_container]');
        $(window).resize(function () {
            that.setHeight(that.examContainerView);
            that.setHeight(that.rightAnaView)
        });
        this.examList = new ExamList(this.examContainerView, this.activityData.id);
        this.examList.addListener(this, function (id) {
            that.examNav.select(id);
            //    刷新右侧
            this.updateCensus(id);
        });

        // 初始化右侧
        this.examNav.addLoadedListener(this, function (id) {
            //空白页面
            if (id != null) {
                this.examNavView.show();
                this.view.find('[sid=ana_content]').show();
            }
            if (id == null)
                this.view.append($(this.blankTemplate).clone(true));
            this.updateCensus(id);

            this.setHeight(this.examContainerView);
            this.setHeight(this.rightAnaView);
        });
    };

    //刷新右侧区域
    Analyze.prototype.updateCensus = function (id) {
        if (this.lastExamId == id)
            return;
        this.lastExamId = id;

        var data = this.examNav.getSelectData();

        //统计
        this.examAvgView = this.view.find('[sid=exam_avg]');
        this.examAvgView.text(data.scoreAvg);

        this.rightPersonView = this.view.find('[sid=right_person]');
        this.rightPersonView.text(data.rightCount);

        this.totalPersonView = this.view.find('[sid=total_person]');
        console.log(data.studentCount + "总人数");
        this.totalPersonView.text(data.studentCount);

        //统计、学生详情切换nav
        this.censusNavView = this.view.find('[sid=census_nav]');
        this.censusNav = new CensusNav(this.censusNavView, 'census');
        this.censusNav.addListener(this, function (viewType) {
            this.updateCensusDetail(viewType, id);
        });

        //初始化下部分的统计详情区
        this.updateCensusDetail(this.censusNav.getValue(), id);
    };

    Analyze.prototype.setHeight = function (obj) {
        var repre_right_top = obj.offset().top;
        var heig = $(document).height() - repre_right_top;
        obj.height(heig);
    };


    Analyze.prototype.updateCensusDetail = function (viewType, id) {
        this.censusDetailView = this.view.find('[sid=census_detail]');
        this.censusDetailView.empty();

        if (viewType == 'census')
            this.typeCensus = new TypeCensus(this.censusDetailView, this.examNav.getValue(), id);
        if (viewType == 'student_info')
            this.personCensus = new PersonCensus(this.censusDetailView, this.activityData.id, id);
    };
    Analyze.prototype.blankTemplate = '<div class="mi-blank-container" sid="blank" style="height:100%;"> ' +
        '<div class="mi-blank"> ' +
        '<div class="mi-blank-label">暂无内容~</div> ' +
        '</div> ' +
        '</div>';

