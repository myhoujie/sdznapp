/**
 * Created by ming on 2018/2/9.
 */
    var PersonCensus = function (view, activityId, detailId) {
        this.view = view;
        this.activityId = activityId;
        this.detailId = detailId;

        this.initView();
    };


    PersonCensus.prototype.initView = function () {
        this.view.empty();
        this.view.load('html/activity/activity_detail/analyze/icon/wrong.html', this.init.bind(this));
    };

    PersonCensus.prototype.init = function () {
        //错误
        this.faultlistView = this.view.find('[sid=fault_list]');
        this.faultCountView = this.view.find('[sid=fault_count]');
        this.faultPercentView = this.view.find('[sid=fault_percent]');

        //正确
        this.rightlistView = this.view.find('[sid=right_list]');
        this.rightCountView = this.view.find('[sid=right_count]');
        this.rightPercentView = this.view.find('[sid=right_percent]');

        //半对
        this.partRightListView = this.view.find('[sid=part_right_list]');
        this.partRightCountView = this.view.find('[sid=part_right_count]');
        this.partRightPercentView = this.view.find('[sid=part_right_percent]');

        var param = {};
        param.lessonTaskId = this.activityId;
        param.lessonTaskDetailsId = this.detailId;
        console.log(param);
        ApiActivity.getExamCensusStudentList(param, this, function (flag, data) {
            if(flag)
            {
                if(data != null && data != undefined)
                {
                    var censusData = data.data;
                //  错误
                    this.faultCountView.text(censusData.errorCount);
                    this.faultPercentView.text(censusData.errorRate);
                    this.updateList(censusData.errorStudentList, 'fault');
                //  半对
                    this.partRightCountView.text(censusData.halfRightCount);
                    this.partRightPercentView.text(censusData.halfRightRate);
                    this.updateList(censusData.halfRightStudentList,'part_right');
                //  正确
                    this.rightCountView.text(censusData.rightCount);
                    this.rightPercentView.text(censusData.rightRate);
                    this.updateList(censusData.rightStudentList,'right');
                }
                else
                    Information.show(2, "数据为空");
            }
            else
                Information.show(2, data.msg);
        });
    };

    PersonCensus.prototype.updateList = function (listData, type) {

        var that = this;
        $(listData).each(function (key, value) {
            var label = null;
            if(type == 'fault')
                label = new StudentLabelOne( that.faultlistView, value, true);
            if(type == 'part_right')
                label = new StudentLabelOne( that.partRightListView, value, true);
            if(type == 'right')
                label = new StudentLabelOne( that.rightlistView, value, true);
            if(label!=null)
                label.addListener(that, that.viewStudentDetail);
        });
    };

    //学生详情分析 头像弹框 TODO
    PersonCensus.prototype.viewStudentDetail = function (data) {
        var examId = data.lessonAnswerExamId;
        if(examId == null || examId == undefined)
            console.log("学生无作答内容");
        else
        {
            var param = {};
            param.lessonAnswerExamId = examId;
            ApiActivity.examAnswerInfo(param, this, function (flag, data) {
                if(flag)
                {
                    if(data != null && data != undefined)
                    {
                        var info = data.data;
                        console.log(info);
                        this.scanExamFun(info);
                    }
                    else
                        Information.show(2, "无数据");
                }
                else
                    Information.show(2, data.msg);
            });
        }
    };

  PersonCensus.prototype.scanExamFun = function (data) {
    if ($('#del-box').length == 0) {
      this.examView = $(PersonCensus.template);
      if (data.userStudentName) {
        this.examView.find('[sid=title]').text(data.userStudentName);
      }else{
        this.examView.find('[sid=title]').text('未知');
      }
      var tempExamObj = new ExamAnswerPreview(data,data.examSeq + '.');
      this.examView.find('[sid=exam]').empty().append(tempExamObj.view);
      $("body").append(this.examView);
      var that = this;
      this.examView.find('[sid=cancel]').on('click', function () {
        that.examView.remove();
      });
    } else {
      console.log('已存在弹出框');
    }
  };

  PersonCensus.template = '<div id="del-box">' +
    '    <div class="back-level"></div>' +
    '    <div class="box-out">' +
    '        <div class="box-inner" style="width: 750px">' +
    '            <div class="box-inner-top clearfix">' +
    '                <span class="send-title l" sid="title">make</span>' +
    '                <span class="close-send r" sid="cancel"></span>' +
    '            </div>' +
    '            <div class="edit-box-inner-con cus-form" style="width:96%;min-height:200px;max-height:400px;margin-bottom: 30px;overflow-y: auto" sid="exam"></div>' +
    '        </div>' +
    '    </div>' +
    '</div>';
