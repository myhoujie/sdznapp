/**
 * Created by ming on 2018/2/9.
 */
    var TypeCensus = function (view, numberData, detailId) {
        this.view = view;
        this.detailId = detailId;
        this.numberData = numberData;

        this.goodStudentLabels = [];
        this.badStudentLabels = [];

        //眼睛是否开启
        this.isGoodOpen = false;
        this.isBadOpen = false;

        //眼睛
        this.goodOpenView = null;
        this.badOpenView = null;

               //题目分析对象
        this.analyze = null;

        this.initView();
    };


    TypeCensus.prototype.initView = function () {
        this.view.empty();
        this.view.load('html/activity/activity_detail/analyze/icon/answer.html?'+ Utils.getTimes(), this.init.bind(this));
    };

    TypeCensus.prototype.init = function () {
        var param = {};
        param.lessonTaskDetailsId = this.detailId;
        ApiActivity.getExamAnalyse(param, this, function (flag, data) {
            if(flag)
            {
                if(data != null && data != undefined)
                {
                    var censusData = data.data;
                    if(censusData != null && censusData != undefined)
                    {
                        //绘制题目分析
                        this.updateExamAna(censusData);
                        //绘制典型数据
                        this.updateTypeAna(censusData.typicalErrorList, censusData.excellentAnswerList);
                    }

                }
                else
                {
                    this.updateTypeAna([], []);
                    alert( "数据为空");
                }
            }
            else
            {
                this.updateTypeAna([], []);
                alert( data.msg);
            }
        });
    };

    TypeCensus.prototype.updateTypeAna = function (badList, goodList) {

        //获取教师的小眼睛配置信息
        var param = {};
        ApiActivity.getTeachInfo(param, this, function (flag, data) {
            if(flag)
            {
                if(data != null && data != undefined)
                {
                    var teacherInfo = data.data;
                    var that = this;
                    $(teacherInfo.confInfo).each(function (key,value) {
                        if(value.code == 'GOOD_ANSWER')
                            that.isGoodOpen = value.value==0?true:false;
                        if(value.code == 'BAD_ANSWER')
                            that.isBadOpen = value.value==0?true:false;
                    });
                }
                else
                    alert( "教师信息数据为空");
            }
            else
                alert( data.msg);

            //绘制两个典型
            //绘制眼睛并初始化和事件
            this.goodOpenView = this.view.find('[sid=good_open]');
            this.badOpenView = this.view.find('[sid=bad_open]');
            //初始化样式。
            this.controlOpenView('good',this.isGoodOpen);
            this.controlOpenView('bad',this.isBadOpen);
            this.goodOpenView.on('click',{'obj':this,'type':'good'},this.toggleOpenView);
            this.badOpenView.on('click',{'obj':this,'type':'bad'},this.toggleOpenView);

            //绘制人员列表并初始化
            this.badListView = this.view.find('[sid=bad_list]');
            this.goodListView = this.view.find('[sid=good_list]');
            var that = this;
            $(badList).each(function (key, value) {
                var label = new StudentLabelOne(that.badListView, value, that.isBadOpen);
                label.addListener(that,that.viewStudentDetail);
                that.badStudentLabels.push(label);
            });

            $(goodList).each(function (key, value) {
                var label = new StudentLabelOne(that.goodListView, value, that.isGoodOpen);
                label.addListener(that,that.viewStudentDetail);
                that.goodStudentLabels.push(label);
            });
        });
    };

    //反置眼睛
    TypeCensus.prototype.toggleOpenView = function (evt) {
        var self = evt.data.obj;
        var type = evt.data.type;

        if(type == 'good')
        {
            self.controlOpenView(type, !self.isGoodOpen);
            self.isGoodOpen = !self.isGoodOpen;
        }
        if(type == 'bad')
        {
            self.controlOpenView(type, !self.isBadOpen);
            self.isBadOpen = !self.isBadOpen;
        }
    };

    TypeCensus.prototype.controlOpenView = function (type, flag) {
        if(type=='good')
        {
            if(flag)
            {
                this.goodOpenView.removeClass('shut');
                $(this.goodStudentLabels).each(function (key,value) {
                    value.controlName(true);
                });
            }
            else
            {
                this.goodOpenView.addClass('shut');
                $(this.goodStudentLabels).each(function (key,value) {
                    value.controlName(false);
                });
            }
        }

        if(type=='bad')
        {
            if(flag)
            {
                this.badOpenView.removeClass('shut');
                $(this.badStudentLabels).each(function (key,value) {
                    value.controlName(true);
                });
            }
            else
            {
                this.badOpenView.addClass('shut');
                $(this.badStudentLabels).each(function (key,value) {
                    value.controlName(false);
                });
            }
        }
    };

    //题目分析
    TypeCensus.prototype.updateExamAna = function (censusData) {
        this.examAnalyze = new ExamAnalyze(this.view, censusData);
    };

    //点击学生头像 弹框 TODO
    TypeCensus.prototype.viewStudentDetail = function (data) {
        console.log(data);
        var examId = data.lessonAnswerExamId;
        if(examId == null || examId == undefined)
            console.log("学生无作答内容");
        else
        {
            var param = {};
            param.lessonAnswerExamId = examId;
            ApiActivity.examAnswerInfo(param, this, function (flag, data) {
                if(flag)
                {
                    if(data != null && data != undefined)
                    {
                        var info = data.data;
                        console.log(info);
                        this.scanExamFun(info);
                    }
                    else
                        alert( "无数据");
                }
                else
                    alert( data.msg);
            });
        }
    };

    TypeCensus.prototype.scanExamFun = function (data) {
      if ($('#del-box').length == 0) {
        this.examView = $(TypeCensus.template);
        if (data.userStudentName) {
          this.examView.find('[sid=title]').text(data.userStudentName);
        }else{
          this.examView.find('[sid=title]').text('未知');
        }
        var tempExamObj = new ExamAnswerPreview(data,data.examSeq + '.');
        this.examView.find('[sid=exam]').empty().append(tempExamObj.view);
        $("body").append(this.examView);
        var that = this;
        this.examView.find('[sid=cancel]').on('click', function () {
          that.examView.remove();
        });
      } else {
        console.log('已存在弹出框');
      }
    };

    TypeCensus.template = '<div id="del-box">' +
    '    <div class="back-level"></div>' +
    '    <div class="box-out">' +
    '        <div class="box-inner" style="width: 750px">' +
    '            <div class="box-inner-top clearfix">' +
    '                <span class="send-title l" sid="title">make</span>' +
    '                <span class="close-send r" sid="cancel"></span>' +
    '            </div>' +
      '            <div class="edit-box-inner-con cus-form" style="width:96%;min-height:200px;max-height:400px;margin-bottom: 30px;overflow-y: auto" sid="exam"></div>' +
    '        </div>' +
    '    </div>' +
    '</div>';
