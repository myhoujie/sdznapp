function getPreview (param) {
  getAllParams(param);
  PreviewActivity = new PreviewActivity();
}

/**
 *  共用
 */
//发送系统消息
function html5SendSystemInfoToNative (msg) {
    interfaceName.showMessage(msg);
}

//点击页面上的图片调用原生放大查看
function html5SendPicToNative (src) {
    interfaceName.showImage(src);
}

//预览资源
function html5ScanResource (data) {
    interfaceName.showResource(JSON.stringify(data));
}

//调取移动端刷新token的方法
function mobileRefreshTokenFun (token,key) {
    interfaceName.mobileRefreshTokenFun(token, key);
}

//移动端刷新完成后的回调 flag：true:成功！ false：失败！
function refreshH5TokenFun (flag,token,key) {
  var mOptions=null;
  for(var i=0;i<requestArr.length;i++){
    if(requestArr[i].key == key){
      mOptions=requestArr[i].value;
    }
  }
  if(flag){
    access_token = token;
    mOptions.data.access_token = token;
    $.ajax(mOptions);
  }else{
    //alert('移动端刷新token失败!');
  }
}


//显示loading
function showLoading () {
  interfaceName.showLoading();
}

//隐藏loading
function hideLoading () {
  interfaceName.hideLoading();
}
