/**
 * Created by zhangxia on 2018/1/31.
 */
var ExamPreViewTem = function () {};

ExamPreViewTem.boxTemplate = '<div class="exam-scan"></div>';

ExamPreViewTem.introTemplate = '<div class="exam-scan-intro">' +
  '                             <span class="exam-flag" sid="index"></span>' +
  '                             <span class="exam-score" sid ="score" ></span>'+
  '                             <span class="exam-i" sid="type"></span>' +
  '                         </div>';

ExamPreViewTem.stemTemplate = '<div class="exam-scan-stem" sid="exam-scan-stem"></div>';

ExamPreViewTem.optionConTemplate = '<div class="exam-scan-option" sid="exam-scan-option"></div>';

ExamPreViewTem.optionAllConTemp = '<div class="exam-scan-small-box" sid="exam-scan-small-box"></div>';

ExamPreViewTem.optionJudgeConTem = '<div class="exam-scan-option" sid="exam-scan-option">' +
  '                            <div class="exam-scan-o-li">' +
  '                                <span class="exam-tip judge-tip" sid="index"></span>' +
  '                                <span class="exam-text" sid="stem">正确</span>' +
  '                            </div>' +
  '                            <div class="exam-scan-o-li">' +
  '                                <span class="exam-tip judge-tip" sid="index"></span>' +
  '                                <span class="exam-text" sid="stem">错误</span>' +
  '                            </div>' +
  '                        </div>';

ExamPreViewTem.optionJudgeTemplate = '<div class="exam-scan-o-li">' +
'                                <span class="exam-tip judge-tip" sid="index"></span>' +
'                                <span class="exam-text" sid="stem">正确</span>' +
'                            </div>';

ExamPreViewTem.optionTemplate = ' <div class="exam-scan-o-li">' +
  '                                 <span class="exam-tip" sid="index">A.</span>' +
  '                                 <span class="exam-text" sid="stem">若若方程组的解的解满足</span>' +
  '                             </div>';

ExamPreViewTem.amswerTemplate = '<div class="exam-scan-h">' +
  '                             <span class="exam-ana">[答案]</span>' +
  '                             <span class="exam-text clearfix" sid="exam-scan-answer"></span>' +
  '                         </div>';

ExamPreViewTem.amswerTemplate1 = '<div class="exam-scan-hh">' +
  '                             <span class="exam-ana">[作答]</span>' +
  '                             <div class="exam-text clearfix" sid="exam-scan-answer-s"><span class="flag" sid="flag"></span></div>' +
  '                         </div>';

ExamPreViewTem.analyzeTemplate = '<div class="exam-scan-h">' +
  '                             <span class="exam-ana">[解析]</span>' +
  '                             <span class="exam-text clearfix" sid="exam-scan-analyse">有理数的加减混合运算</span>' +
  '                         </div>';

ExamPreViewTem.openAnswer = '<div class="openAnswer" cid="openAnswer"></div>';

ExamPreViewTem.clozeAnswerTemplate = '<div class="scan-cloze-answer">' +
  '                                      <div class="scan-cloze-flag" sid="flag"></div>' +
  '                                      <div class="scan-cloze-option"></div>' +
  '                                 </div>';

ExamPreViewTem.clozeOptionItemTemplate = '<div class="scan-cloze-item"><span class="scan-cloze-item-index" sid="index"></span><span class="scan-cloze-content"></span></div>';
