/**
 * Created by zhangxia on 2018/2/1.
 */
/**
 * Created by zhangxia on 2018/1/31.
 * examInfo:试题信息 index:序号 从1开始
 * 例如：
 * var temp = new ExamPreview(examInfo,1);
 this.view.append(temp.view);
 */
var UExamPreview = function (examInfo, index) {
    this.examInfo = examInfo;
    this.index = index;
    this.serialNum1 = ["A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.", "P.", "Q.", "I.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z"];
    this.serialNum3 = ["正确", "错误"];
    this.initType();
};

UExamPreview.prototype.initType = function () {
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    switch (this.examInfo.examTypeId) {
        case 1:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 2:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 6:
            this.scanComExam(this.view, this.examInfo, this.index);
            break;
        case 3:
            this.scanJudgeExam(this.view, this.examInfo, this.index);
            break;
        case 4:
            this.scanShortExam(this.view, this.examInfo, this.index);
            break;
        case 16:
            this.scanAllExam(this.view, this.examInfo, this.index);
            break;
        case 14:
            this.scanClozeExam(this.view, this.examInfo, this.index);
            break;
        default:
            console.log('未找到对应题型：' + this.examInfo.examTypeId);
    }
};

UExamPreview.prototype.initData = function (indexFlag, data) {
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView.find('[sid=index]').text(indexFlag + '. ');
    viewArr.stemView.find('[sid=type]').html(data.examStem);
    viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    return viewArr;
};

//预览单选,多选
UExamPreview.prototype.scanSingleExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);
    var arr = [];
    for (var i = 0; i < data.examOptions.length; i++) {
        var option = $(ExamPreViewTem.optionTemplate).clone();
        option.find('[sid=index]').text(this.serialNum1[i]);
        option.find('[sid=stem]').html(data.examOptions[i].content);
        viewArr.optionConView.append(option);
        if (data.examOptions[i].right) {
            arr.push(this.serialNum1[i]);
        }
    }
};

//预览判断
UExamPreview.prototype.scanJudgeExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.stemView);
    parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
    var arr = [];
    for (var i = 0; i < data.examOptions.length; i++) {
        if (data.examOptions[i].right) {
            arr.push(this.serialNum3[i]);
        }
    }
};

//预览填空
UExamPreview.prototype.scanComExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.stemView);
};

//预览简答
UExamPreview.prototype.scanShortExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.stemView);
};

// 预览完形填空
UExamPreview.prototype.scanClozeExam = function (parentView, data, indexFlag, type) {
    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);

    for (var i = 0; i < data.examOptions.length; i++) {
        var smallOption = data.examOptions[i].list;
        var clozeOption = $(ExamPreViewTem.clozeAnswerTemplate).clone();
        clozeOption.find('[sid=flag]').text("（" + (i + 1) + "）");
        for (var j = 0; j < smallOption.length; j++) {
            var option = $(ExamPreViewTem.clozeOptionItemTemplate).clone();
            option.find('[sid=index]').text(this.serialNum1[j] + ".");
            option.find(".scan-cloze-content").html(smallOption[j].content);
            clozeOption.find(".scan-cloze-option").append(option)
        }
        viewArr.optionConView.append(clozeOption);
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(data.examAnswer) ? trimStr(data.examAnswer) : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};


//预览综合
UExamPreview.prototype.scanAllExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.stemView);
    viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
    parentView.append(viewArr.optionConView);

    for (var i = 0; i < data.examBases.length; i++) {
        switch (data.examBases[i].examTypeId) {
            case 1:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 2:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 6:
                this.scanComExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 3:
                this.scanJudgeExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            case 4:
                this.scanShortExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')');
                break;
            default:
                console.log('未找到对应的小题型：' + data.examTypeId);
        }
    }
};