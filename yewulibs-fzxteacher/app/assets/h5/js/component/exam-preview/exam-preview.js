/**
 * Created by zhangxia on 2018/1/31.
 * examInfo:试题信息 index:序号 从1开始
 * 例如：
 * var temp = new ExamPreview(examInfo,1);
 this.view.append(temp.view);
 */
var ExamPreview = function (examInfo, index) {
    this.examInfo = examInfo;
    this.index = index;
    this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    this.serialNum3 = ["正确", "错误"];
    this.initType();
};

ExamPreview.prototype.initType = function () {
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    var typeId = this.examInfo.examTypeId ? this.examInfo.examTypeId : this.examInfo.templateId;
    switch (typeId) {
        case 1:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 2:
            this.scanSingleExam(this.view, this.examInfo, this.index);
            break;
        case 6:
            this.scanComExam(this.view, this.examInfo, this.index);
            break;
        case 3:
            this.scanJudgeExam(this.view, this.examInfo, this.index);
            break;
        case 4:
            this.scanShortExam(this.view, this.examInfo, this.index);
            break;
        case 16:
            this.scanAllExam(this.view, this.examInfo, this.index);
            break;
        case 14:
            this.scanClozeExam(this.view, this.examInfo, this.index);
            break;
        default:
            console.log('未找到对应题型：' + typeId);
    }
};

ExamPreview.prototype.initData = function (indexFlag, data, type) {
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
    viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
    viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();


    if (indexFlag) {
        viewArr.introView.find('[sid=index]').text(indexFlag);
    }
    if (type) {
        viewArr.introView.find('[sid=type]').text(this.stage(data.examTypeId));
    } else {
        viewArr.introView.find('[sid=type]').text(data.templateStyleName);
    }
    viewArr.stemView.html(data.examStem);
    viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis ? data.examAnalysis : '略');
    return viewArr;
};

//预览单选,多选
ExamPreview.prototype.scanSingleExam = function (parentView, data, indexFlag, type) {


    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);
    for (var i = 0; i < data.examOptions.length; i++) {
        var option = $(ExamPreViewTem.optionTemplate).clone();
        option.find('[sid=index]').text(this.serialNum1[i]);
        option.find('[sid=stem]').html(data.examOptions[i].content);
        viewArr.optionConView.append(option);
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览判断
ExamPreview.prototype.scanJudgeExam = function (parentView, data, indexFlag, type) {
    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览填空
ExamPreview.prototype.scanComExam = function (parentView, data, indexFlag, type) {
    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览简答
ExamPreview.prototype.scanShortExam = function (parentView, data, indexFlag, type) {
    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(data.examAnswer ? data.examAnswer : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};

//预览综合
ExamPreview.prototype.scanAllExam = function (parentView, data, indexFlag) {
    var viewArr = this.initData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.optionConView = $(ExamPreViewTem.optionAllConTemp).clone();
    parentView.append(viewArr.optionConView);

    for (var i = 0; i < data.examBases.length; i++) {
        switch (data.examBases[i].examTypeId) {
            case 1:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 2:
                this.scanSingleExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 6:
                this.scanComExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 3:
                this.scanJudgeExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 4:
                this.scanShortExam(viewArr.optionConView, data.examBases[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            default:
                console.log('未找到对应的小题型：' + data.examTypeId);
        }
    }
};

// 预览完形填空
ExamPreview.prototype.scanClozeExam = function (parentView, data, indexFlag, type) {
    var viewArr = this.initData(indexFlag, data, type);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);

    for (var i = 0; i < data.examOptions.length; i++) {
        var smallOption = data.examOptions[i].list;
        var clozeOption = $(ExamPreViewTem.clozeAnswerTemplate).clone();
        clozeOption.find('[sid=flag]').text("（" + (i + 1) + "）");
        for (var j = 0; j < smallOption.length; j++) {
            var option = $(ExamPreViewTem.clozeOptionItemTemplate).clone();
            option.find('[sid=index]').text(this.serialNum1[j] + ".");
            option.find(".scan-cloze-content").html(smallOption[j].content);
            clozeOption.find(".scan-cloze-option").append(option)
        }
        viewArr.optionConView.append(clozeOption);
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(data.examAnswer) ? trimStr(data.examAnswer) : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
};


//显示答案和解析
ExamPreview.prototype.showAnswer = function () {
    this.view.find('.exam-scan-h').show();
};

ExamPreview.prototype.hideAnswer = function () {
    this.view.find('.exam-scan-h').hide();
};

ExamPreview.prototype.stage = function (num) {
    switch (num) {
        case 1:
            return '单选题';
            break;
        case 2:
            return '多选题';
            break;
        case 3:
            return '判断题';
            break;
        case 4:
            return '简答题';
            break;
        case 6:
            return '填空题';
            break;
        default:
            return '未知';
            break;
    }
};