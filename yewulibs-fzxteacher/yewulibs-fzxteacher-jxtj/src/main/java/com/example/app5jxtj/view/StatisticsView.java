package com.example.app5jxtj.view;

import com.example.app5libbase.base.BaseView;
import com.example.app5libbase.views.StatisticsGraphView;
import com.sdzn.fzx.teacher.vo.TeachClassVo;
import com.sdzn.fzx.teacher.vo.TeachStatisticsVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/8
 * 修改单号：
 * 修改内容:
 */
public interface StatisticsView extends BaseView {

    void setTeachStatisticsVo(TeachStatisticsVo teachStatisticsVo);
    void setLineViewBean(List<StatisticsGraphView.LineViewBean> lineViewBean);
    void setTeachClassVo(TeachClassVo teachClassVo);
    void setClassGroupingVo(ClassGroupingVo classGroupingVo);
}
