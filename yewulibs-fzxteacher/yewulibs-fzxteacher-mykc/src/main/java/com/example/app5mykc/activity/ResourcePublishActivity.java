package com.example.app5mykc.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5mykc.adapter.PublishAdapter;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5mykc.adapter.ResourcePublishAdapter;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ResourcePublishActivity extends Activity implements View.OnClickListener {

    private RelativeLayout activityResourcePublish;
    private RelativeLayout head;
    private LinearLayout resourceRl;
    private GridView publishResource;
    private LinearLayout object;
    private GridView publishObject;
    private View ll;
    private LinearLayout cancel;
    private LinearLayout confirm;

    private PublishAdapter publishAdapter;
    private Map<String, Object> classMap;
    private Map<Integer, Object> resourceMap;
    private String lessonId;
    private String lessonName;
    private ResourcePublishAdapter resourcePublishAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_publish);
        activityResourcePublish = (RelativeLayout) findViewById(R.id.activity_resource_publish);
        head = (RelativeLayout) findViewById(R.id.head);
        resourceRl = (LinearLayout) findViewById(R.id.resource_rl);
        publishResource = (GridView) findViewById(R.id.publish_resource);
        object = (LinearLayout) findViewById(R.id.object);
        publishObject = (GridView) findViewById(R.id.publish_object);
        ll = (View) findViewById(R.id.ll);
        cancel = (LinearLayout) findViewById(R.id.cancel);
        confirm = (LinearLayout) findViewById(R.id.confirm);
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);

        initView();
        initData();
        requestResource();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入ResourcePublishActivity成功");
                }
            }
        }
    }


    private void initView() {
        resourcePublishAdapter = new ResourcePublishAdapter(ResourcePublishActivity.this);
        publishResource.setAdapter(resourcePublishAdapter);
        publishAdapter = new PublishAdapter(ResourcePublishActivity.this);
        publishObject.setAdapter(publishAdapter);
    }


    protected void initData() {

        lessonId = getIntent().getStringExtra("lessonId");
        lessonName = getIntent().getStringExtra("lessonName");
        resourceMap = new HashMap<>();
        classMap = new HashMap<>();
        requestClass();
        requestResource();


    }


    private void requestClass() {

        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {

                        publishAdapter.setList(o.getData());

                    }
                });
    }

    private void requestResource() {
        Network.createTokenService(NetWorkService.ResourceLessonsService.class)
                .getResourceLessons(lessonId)
                .map(new StatusFunc<LessonResource>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<LessonResource>(new SubscriberListener<LessonResource>() {
                    @Override
                    public void onNext(LessonResource lessonResource) {
                        resourcePublishAdapter.setList(lessonResource.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, ResourcePublishActivity.this));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.cancel) {
            finish();
        } else if (id == R.id.confirm) {
            confirmResource();
        }
    }

    private Map<String, Object> buildeParams() {
        final List<LessonResource.DataBean> resourceCheckList = resourcePublishAdapter.getCheckList();
        final List<SyncClassVo.DataBean> classCheckList = publishAdapter.getCheckedList();

        if (resourceCheckList.size() == 0) {
            ToastUtil.showShortlToast("未选择资源");
            return null;
        }

        if (classCheckList.size() == 0) {
            ToastUtil.showShortlToast("未选择班级");
            return null;
        }

        JSONArray jsonArray = new JSONArray();
        Map<String, Object> params = new HashMap<>();


        for (SyncClassVo.DataBean classBean : classCheckList) {

            for (LessonResource.DataBean resourceBean : resourceCheckList) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("classId", classBean.getClassId());
                    jsonObject.put("className", classBean.getClassName());
                    jsonObject.put("lessonResourceId", resourceBean.getId());
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        params.put("lessonId", lessonId);
        params.put("lessonName", lessonName);
        params.put("releaseJson", jsonArray.toString());
        return params;
    }


    private void confirmResource() {
        final Map<String, Object> paramsMap = buildeParams();
        if (paramsMap == null)
            return;

        Network.createTokenService(NetWorkService.resourceReleaseService.class)
                .resourceRelease(paramsMap)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showLonglToast("发布成功");
                        finish();
                    }
                });
    }

}
