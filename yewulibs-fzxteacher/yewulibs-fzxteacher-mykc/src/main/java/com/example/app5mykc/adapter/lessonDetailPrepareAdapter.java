package com.example.app5mykc.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.teacher.vo.PrepareLessonsVo;

import java.util.List;


/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/28
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class lessonDetailPrepareAdapter extends BaseAdapter {
    private List<PrepareLessonsVo.DataBean> list;
    private Context context;
    private ItemDisposalListener listener;

    public lessonDetailPrepareAdapter(List<PrepareLessonsVo.DataBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setItemDisposalListener(ItemDisposalListener l) {
        this.listener = l;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.prepare_gridview_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final PrepareLessonsVo.DataBean dataBean = list.get(i);
        holder.sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PublishActivity");
                intent.putExtra("lessonLibId", dataBean.getId() + "");
                intent.putExtra("lessonLibName", dataBean.getName() + "");
                intent.putExtra("name", dataBean.getName() + "");
                context.startActivity(intent);
            }
        });
        holder.preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                skipActivity(i, context);
                Intent answerIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.TaskShowNativeActivity");
                Bundle bundle = new Bundle();
                bundle.putString("name", dataBean.getName());
                bundle.putString("lessonId", String.valueOf(dataBean.getLessonId()));
                bundle.putString("libId", String.valueOf(dataBean.getId()));
                answerIntent.putExtras(bundle);
                context.startActivity(answerIntent);
            }
        });
        holder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent answerIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.TaskShowNativeActivity");
                Bundle bundle = new Bundle();
                bundle.putString("name", dataBean.getName());
                bundle.putString("lessonId", String.valueOf(dataBean.getLessonId()));
                bundle.putString("libId", String.valueOf(dataBean.getId()));
                answerIntent.putExtras(bundle);
                context.startActivity(answerIntent);
            }
        });
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLessonDel(dataBean.getId());
            }
        });
        if (!TextUtils.isEmpty(dataBean.getName())) {
            holder.name.setText(dataBean.getName());
        }
        if (!TextUtils.isEmpty(dataBean.getUserCreateName())) {
            holder.name.setText("试题" + dataBean.getCountExam() + "／资源" + dataBean.getCountResource());
        }
        if (!TextUtils.isEmpty(dataBean.getName())) {
            holder.tvChapter.setText(dataBean.getName());
        }
        holder.time.setText(DateUtil.getTimeStrByTimemillis(dataBean.getTimeCreate(), "yyyy-MM-dd"));
        return view;
    }


    static class ViewHolder {
        private RelativeLayout contentView;
        private ImageView imgOne;
        private TextView tvChapter;
        private ImageView imgTwo;
        private TextView name;
        private ImageView imgTime;
        private TextView time;
        private View line;
        private RelativeLayout preview;
        private View line1;
        private RelativeLayout del;
        private TextView delImg;
        private View line2;
        private RelativeLayout sync;
        private TextView syncImg;

        ViewHolder(View view) {
            contentView = (RelativeLayout) view.findViewById(R.id.content_view);
            imgOne = (ImageView) view.findViewById(R.id.img_one);
            tvChapter = (TextView) view.findViewById(R.id.tv_chapter);
            imgTwo = (ImageView) view.findViewById(R.id.img_two);
            name = (TextView) view.findViewById(R.id.name);
            imgTime = (ImageView) view.findViewById(R.id.img_time);
            time = (TextView) view.findViewById(R.id.time);
            line = (View) view.findViewById(R.id.line);
            preview = (RelativeLayout) view.findViewById(R.id.preview);
            line1 = (View) view.findViewById(R.id.line_1);
            del = (RelativeLayout) view.findViewById(R.id.del);
            delImg = (TextView) view.findViewById(R.id.del_img);
            line2 = (View) view.findViewById(R.id.line_2);
            sync = (RelativeLayout) view.findViewById(R.id.sync);
            syncImg = (TextView) view.findViewById(R.id.sync_img);
        }
    }

    public interface ItemDisposalListener {
        void onLessonDel(int id);
    }
}
