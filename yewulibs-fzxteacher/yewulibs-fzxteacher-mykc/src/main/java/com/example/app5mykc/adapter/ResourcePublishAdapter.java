package com.example.app5mykc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.LessonResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/7
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ResourcePublishAdapter extends BaseAdapter {
    private List<LessonResource.DataBean> list;
    private Context context;
    private boolean flag = true;

    public ResourcePublishAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<LessonResource.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.resource_publish_item, null);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final LessonResource.DataBean dataBean = list.get(i);
        holder.viewName.setText(dataBean.getResourceName());

        holder.check.setEnabled(dataBean.isChecked());

        holder.all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataBean.setChecked(!dataBean.isChecked());
                notifyDataSetChanged();
            }
        });

        if (dataBean.getResourceType() == 1) {
            if ("txt".equals(dataBean.getResourceSuffix())) {
                holder.resourceImg.setImageResource(R.mipmap.txt_shen);
            } else {
                holder.resourceImg.setImageResource(R.mipmap.word_copy);
            }

        }
        if (dataBean.getResourceType() == 2) {
            holder.resourceImg.setImageResource(R.mipmap.ppt_copy);
        }
        if (dataBean.getResourceType() == 3) {
            holder.resourceImg.setImageResource(R.mipmap.shipin_copy);
        }
        if (dataBean.getResourceType() == 4) {
            holder.resourceImg.setImageResource(R.mipmap.img_copy);
        }
        if (dataBean.getResourceType() == 5) {
            holder.resourceImg.setImageResource(R.mipmap.yinpin_copy);
        }
        return view;
    }

    public List<LessonResource.DataBean> getCheckList() {
        List<LessonResource.DataBean> retList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isChecked() && list.get(i).getConvertStatus() == 2) {
                retList.add(list.get(i));
            }

        }

        return retList;

    }


    static class ViewHolder {
        private LinearLayout all;
        private ImageView check;
        private ImageView resourceImg;
        private TextView viewName;

        ViewHolder(View view) {
            all = (LinearLayout) view.findViewById(R.id.all);
            check = (ImageView) view.findViewById(R.id.check);
            resourceImg = (ImageView) view.findViewById(R.id.resource_img);
            viewName = (TextView) view.findViewById(R.id.viewName);
        }
    }

}
