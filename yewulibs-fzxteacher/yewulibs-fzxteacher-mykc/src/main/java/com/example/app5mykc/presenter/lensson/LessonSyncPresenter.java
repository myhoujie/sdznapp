package com.example.app5mykc.presenter.lensson;

import android.content.Intent;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5mykc.view.lesson.LessonSyncView;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.LessonVo;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.VersionBean;

import java.util.ArrayList;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/9
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonSyncPresenter extends BasePresenter<LessonSyncView, BaseActivity> {

    public void getLessonVo(Map<String, Object> map) {
        Network.createTokenService(NetWorkService.LessonVOService.class)
                .getLessonVo(map)
                .map(new StatusFunc<LessonVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<LessonVo>(new SubscriberListener<LessonVo>() {
                    @Override
                    public void onNext(LessonVo lessonVo) {
                        mView.setLessonVoData(lessonVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public class MyCountDownTimer extends CountDownTimer {
        TextView textView;

        public MyCountDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.textView = textView;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (textView != null) {
                textView.setText(DateUtil.millsecondsToStr(millisUntilFinished));
            }
        }

        @Override
        public void onFinish() {
            textView.setText("超时");
            cancel();
        }
    }

    /**
     *
     */

    private String baseEducationId;
    private String baseGradeId;
    private String baseVolumeId;
    private String baseSubjectId;
    private String status;
    private String baseClassId;
    private String typeReview;
    private String startTime;
    private String endTime;
    private String chapterId;
    private String chapterNodeIdPath;
    private String type;
    private String page;
    private String rows;
    private String keyword;

    public String getBaseEducationId() {
        return dealNullStr(baseEducationId);
    }

    public void setBaseEducationId(String baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseGradeId() {
        return dealNullStr(baseGradeId);
    }

    public void setBaseGradeId(String baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseVolumeId() {
        return dealNullStr(baseVolumeId);
    }

    public void setBaseVolumeId(String baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getBaseSubjectId() {
        return dealNullStr(baseSubjectId);

    }

    public void setBaseSubjectId(String baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getStatus() {
        return dealNullStr(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBaseClassId() {
        return dealNullStr(baseClassId);
    }

    public void setBaseClassId(String baseClassId) {
        this.baseClassId = baseClassId;
    }

    public String getTypeReview() {
        return dealNullStr(typeReview);
    }

    public void setTypeReview(String typeReview) {
        this.typeReview = typeReview;
    }

    public String getStartTime() {
        return dealNullStr(startTime);
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return dealNullStr(endTime);
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChapterId() {
        return dealNullStr(chapterId);
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterNodeIdPath() {
        return dealNullStr(chapterNodeIdPath);
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPage() {
        return dealNullStr(page);

    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRows() {
        return dealNullStr(rows);
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getKeyword() {
        return dealNullStr(keyword);
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getVersionList(String.valueOf(SubjectSPUtils.getCurrentSubject().getSubjectId()))
                .map(new StatusFunc<VersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<VersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(VersionBean versionBean) {
                        dealVersionBean(versionBean);
                    }
                });

    }

    public void getNodeList(final String baseVersion) {
        if (TextUtils.isEmpty(baseVersion)) {
            mView.onNodeSuccessed(null);
            return;
        }
        final String baseLevelId = SPUtils.getLoginBean().getData().getUser().getBaseLevelId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getNodeList(getBaseSubjectId(), baseLevelId, getBaseGradeId(), baseVersion, getBaseVolumeId())
                .map(new StatusFunc<NodeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NodeBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(NodeBean o) {
                        mView.onNodeSuccessed(o.getData());
                    }
                });

    }

    void dealVersionBean(VersionBean versionBean) {

        if (versionBean == null) {
            versionBean = new VersionBean();
        }
        if (versionBean.getData() == null) {
            versionBean.setData(new ArrayList<VersionBean.DataBean>());

            final ArrayList<VersionBean.DataBean.VolumeListBean> volumeListBeans = new ArrayList<>();

            final VersionBean.DataBean dataBean = new VersionBean.DataBean();
            dataBean.setVolumeList(volumeListBeans);

            versionBean.getData().add(dataBean);
        }

        VersionBean.DataBean.VolumeListBean volumeListBean = new VersionBean.DataBean.VolumeListBean();
        volumeListBean.setBaseVersionName("全部册别");
        volumeListBean.setBaseGradeName("");
        volumeListBean.setBaseVolumeName("");
        versionBean.getData().get(0).getVolumeList().add(0, volumeListBean);

        ArrayList<VersionBean.DataBean.VolumeListBean> retList = new ArrayList<>();

        for (int i = 0; i < versionBean.getData().size(); i++) {
            retList.addAll(versionBean.getData().get(i).getVolumeList());
        }

        mView.onVersionSuccessed(retList);
    }


    private String dealNullStr(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str;

    }

    public NodeBean.DataBeanXX getBaseNodeBean(NodeBean.DataBeanXX dataBeanXX) {
        if (dataBeanXX.isLeaf()) {
            return dataBeanXX;
        }
        if (getNodePointCount(dataBeanXX.getData().getNodeIdPath()) >= 3) {
            return dataBeanXX;
        }

        return getBaseNodeBean(dataBeanXX.getChildren().get(0));
    }

    private int getNodePointCount(String str) {
        final String newStr = str.replace(".", "");
        return str.length() - newStr.length();

    }

    public void bindTaskListHolder(GeneralRecyclerViewHolder holder, final LessonVo.DataBean itemData) {
        Log.e("", "");
        if (!TextUtils.isEmpty(itemData.getName())) {
            holder.setText(R.id.tv_chapter, itemData.getName());
        }

        if (!TextUtils.isEmpty(itemData.getUserCreateName())) {
            holder.setText(R.id.name, itemData.getUserCreateName());
        }
        if (!TextUtils.isEmpty(itemData.getChapterNodeNamePath())) {
            holder.setText(R.id.tv_chapter_name, itemData.getChapterNodeNamePath());
        }
        if (!TextUtils.isEmpty(itemData.getCount() + "")) {
            holder.setText(R.id.count, itemData.getCount() + "");
        }
        if (!TextUtils.isEmpty(itemData.getCount() + "")) {
            holder.setText(R.id.time, DateUtil.getTimeStrByTimemillis(itemData.getTimeUpdate(), "yyyy-MM-dd"));
        }
       /* if (itemData.getFlagSync() == 0) {//未同步
            holder.setImg(R.id.sync_img, "未同步", mActivity, R.mipmap.weitongbu_icon);
        } else {
            holder.setImg(R.id.sync_img, "已同步", mActivity, R.mipmap.yitongbu_icon);
        }*/
        if (itemData.getUserHoldId() != itemData.getUserCreateId()) {
            holder.setImageView(R.id.jieyong);
        } else {
            holder.setGoneImageView(R.id.jieyong);
        }
        holder.getChildView(R.id.preview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemData instanceof LessonVo.DataBean) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LessonDetailsFragment");
                    intent.putExtra("lessonId", itemData.getId() + "");
                    intent.putExtra("lessonName", itemData.getName() + "");
                    intent.putExtra("titleName", itemData.getChapterName() + "");

                    intent.putExtra("volumeName", itemData.getBaseVolumeName() + "");
                    intent.putExtra("volumeId", itemData.getBaseVolumeId() + "");
                    intent.putExtra("chapterName", itemData.getChapterName() + "");
                    intent.putExtra("chapterId", itemData.getChapterId() + "");
                    mActivity.startActivity(intent);
                }
            }
        });
        holder.getChildView(R.id.content_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LessonDetailsFragment");
                intent.putExtra("lessonId", itemData.getId() + "");
                intent.putExtra("lessonName", itemData.getName() + "");
                intent.putExtra("titleName", itemData.getChapterName() + "");

                intent.putExtra("volumeName", itemData.getBaseVolumeName() + "");
                intent.putExtra("volumeId", itemData.getBaseVolumeId() + "");
                intent.putExtra("chapterName", itemData.getChapterName() + "");
                intent.putExtra("chapterId", itemData.getChapterId() + "");
                mActivity.startActivity(intent);
            }
        });
/**
 * 删除//todo
 */
        holder.getChildView(R.id.sync).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sync(itemData.getId() + "");
            }
        });
    }

    private void sync(String lessonId) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delLesson(lessonId)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ToastUtil.showLonglToast(((ApiException) e).getStatus().getMsg());
                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.onSyncSuccessed();
                    }
                });
    }
}
