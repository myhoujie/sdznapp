package com.example.app5mykc.view.lesson;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.LessonVo;
import com.sdzn.fzx.teacher.vo.ReviewVersionBean;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/27
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface LessonReviewView extends BaseView {
    void setLessonVoData(LessonVo lessonVo);

    public void onVersionSuccessed(ReviewVersionBean reviewVersionBean);
    public void onVersionFailed();

    public void onSyncSuccessed();

}
