package com.example.app5mykc.view;

import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.List;

public interface PublishDiscussionView extends BaseView {


    void networkError(String msg);

    void onClassSuccessed(SyncClassVo o);

    void setClassGropingData(ClassGroupingVo classGroupingVo, String classId, String className);

    void getClassGroupInfo(String classId, List<DiscussionClassGroup.DataB> groupsList);
    void publishSuccessed();

}
