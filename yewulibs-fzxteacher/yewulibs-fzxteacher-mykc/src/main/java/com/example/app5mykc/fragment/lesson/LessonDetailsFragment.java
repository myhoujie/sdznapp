package com.example.app5mykc.fragment.lesson;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.baseui.adapter.NewDiscussionGridViewAdapter;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libpublic.constant.Constant;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.example.app5libbase.R;

import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.example.app5libbase.listener.chatroom.OnDiscussionSaveListener;
import com.example.app5libbase.baseui.activity.inClass.SetinClassActivity;
import com.example.app5libbase.baseui.adapter.ActivityGvAdapter;
import com.example.app5mykc.adapter.LessonDatailResourceAdapter;
import com.example.app5mykc.adapter.lessonDetailPrepareAdapter;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libpublic.event.LessonDetailsGoClassEvent;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.pop.ClassPop;
import com.example.app5mykc.presenter.lensson.LessonDetailsPresenter;
import com.example.app5mykc.view.lesson.LessonDetailsViwe;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.PrepareLessonsVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class LessonDetailsFragment extends MBaseActivity<LessonDetailsPresenter> implements LessonDetailsViwe, OnRefreshListener, OnLoadMoreListener,
        OnDiscussionSaveListener, View.OnClickListener {
    private RelativeLayout head;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private RelativeLayout goToClass;
    private SmartRefreshLayout refreshLayout;
    private LinearLayout lyOne;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private NoScrollGridView prepareGridView;
    private RelativeLayout lyTwo;
    private RelativeLayout rlTvClass1;
    private TextView tvClass1;
    private NoScrollGridView activityGridView;
    private NoScrollGridView gridView;
    private LinearLayout llTaskEmptyTwo;
    private ImageView ivTaskEmptyTwo;
    private TextView tvTaskEmptyTwo;
    private RelativeLayout rlResource;
    private RecyclerView resourceGridView;
    private RelativeLayout publishResource;


    private lessonDetailPrepareAdapter lessonDetailPrepareAdapter;
    private ActivityGvAdapter mAdapter;
    private LessonDatailResourceAdapter lessonDatailResourceAdapter;
    private NewDiscussionGridViewAdapter discussionGridViewAdapter;
    private ClassPop classPop;
    private List<SyncClassVo.DataBean> classVos;
    private String lessonId;
    private String lessonName;
    private String baseClassId;
    private String titleName;
    private int currPage = 0;
    private int pageSize = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //删除保存的Fragment，也可以重写onSaveInstanceState方法不让其保存，目的是防止activity被回收造成fragment数据或栈异常
        if (savedInstanceState != null) {
            savedInstanceState.putParcelable("android:support:fragments", null);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_lesson_details);
        head = (RelativeLayout) findViewById(R.id.head);
        titleBackLy = (LinearLayout) findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) findViewById(R.id.title_back_txt);
        title = (TextView) findViewById(R.id.title);
        goToClass = (RelativeLayout) findViewById(R.id.go_to_class);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refreshLayout);
        lyOne = (LinearLayout) findViewById(R.id.ly_one);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        prepareGridView = (NoScrollGridView) findViewById(R.id.prepare_gridView);
        lyTwo = (RelativeLayout) findViewById(R.id.ly_two);
        rlTvClass1 = (RelativeLayout) findViewById(R.id.rl_tvClass1);
        tvClass1 = (TextView) findViewById(R.id.tvClass1);
        activityGridView = (NoScrollGridView) findViewById(R.id.activity_gridView);
        gridView = (NoScrollGridView) findViewById(R.id.gridView);
        llTaskEmptyTwo = (LinearLayout) findViewById(R.id.llTaskEmpty_two);
        ivTaskEmptyTwo = (ImageView) findViewById(R.id.ivTaskEmpty_two);
        tvTaskEmptyTwo = (TextView) findViewById(R.id.tvTaskEmpty_two);
        rlResource = (RelativeLayout) findViewById(R.id.rl_resource);
        resourceGridView = (RecyclerView) findViewById(R.id.resource_gridView);
        publishResource = (RelativeLayout) findViewById(R.id.publish_resource);
        rlTvClass1.setOnClickListener(this);
        EventBus.getDefault().register(this);
        initView();
        goToClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SetinClassActivity.class);
                startActivity(intent);
            }
        });
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入LessonDetailsFragment成功");
                }
            }
        }

    }


    @Override
    protected void initView() {

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));


        publishResource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ResourcePublishActivity");
                intent.putExtra("lessonId", lessonId);
                intent.putExtra("lessonName", lessonName);
                startActivity(intent);
            }
        });
        titleBackLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAdapter = new ActivityGvAdapter(activity);
        activityGridView.setAdapter(mAdapter);
        mAdapter.setItemDisposalListener(new ActivityGvAdapter.ItemDisposalListener() {
            @Override
            public void onDelet(SyncTaskVo.DataBean dataBean) {
                mPresenter.delTaskItem(String.valueOf(dataBean.getId()));
            }

            @Override
            public void onPublishAnswer(int id) {
                mPresenter.sendAnswer(id + "");
            }
        });

    }

    private String volumeName, volumeId, chapterName, chapterId;

    @Override
    protected void initData() {
        lessonId = getIntent().getStringExtra("lessonId");
        lessonName = getIntent().getStringExtra("lessonName");
        titleName = getIntent().getStringExtra("titleName");
        volumeName = getIntent().getStringExtra("volumeName");
        volumeId = getIntent().getStringExtra("volumeId");
        chapterName = getIntent().getStringExtra("chapterName");
        chapterId = getIntent().getStringExtra("chapterId");
        title.setText(titleName);
        mPresenter.getClassList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "");
        mPresenter.getPrepareLessonsList(lessonId);
        mPresenter.getResourceLessonsList(lessonId);
        currPage = 1;
        mPresenter.getGroupChapList(lessonId, currPage);
        getActivityList();

    }

    private void getActivityList() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("baseSubjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
//        map.put("status", 0);
//        map.put("lessonId", lessonId);
//        map.put("page", String.valueOf(currPage));
//        map.put("rows", String.valueOf(pageSize));
//        if (!TextUtils.isEmpty(baseClassId)) {
//            map.put("classId", baseClassId);
//        }
        Map<String, Object> map = new HashMap<>();
        map.put("baseSubjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
        map.put("status", 0);
        map.put("lessonId", lessonId);
        map.put("page", String.valueOf(currPage));
        map.put("rows", String.valueOf(pageSize));
        if (!TextUtils.isEmpty(baseClassId)) {
            map.put("classId", baseClassId);
        }

        mPresenter.getLessonsActivityList(map);
    }

    @Override
    public void initPresenter() {
        mPresenter = new LessonDetailsPresenter();
        mPresenter.attachView(this, activity);
    }


    @Override
    public void onClassSuccessed(SyncClassVo classVo) {
        SyncClassVo.DataBean dataBean = new SyncClassVo.DataBean();
        dataBean.setBaseGradeName("");
        dataBean.setClassName("全部班级");
        dataBean.setClassId("");

        classVo.getData().add(dataBean);
        classVos = classVo.getData();
    }

    /**
     * 备课信息
     *
     * @param prepareLessonsVo
     */
    @Override
    public void onPrepareLessonsVoSuccessed(PrepareLessonsVo prepareLessonsVo) {
        cancilLoadState();
        lessonDetailPrepareAdapter = new lessonDetailPrepareAdapter(prepareLessonsVo.getData(), activity);
        if (prepareLessonsVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
            prepareGridView.setVisibility(View.GONE);
            return;
        } else {
            prepareGridView.setVisibility(View.VISIBLE);
            llTaskEmpty.setVisibility(View.GONE);

            lessonDetailPrepareAdapter.setItemDisposalListener(new lessonDetailPrepareAdapter.ItemDisposalListener() {
                @Override
                public void onLessonDel(int id) {
                    mPresenter.delLessonDetail(id);
                }
            });
            prepareGridView.setAdapter(lessonDetailPrepareAdapter);
        }


    }

    @Override
    public void onLessonResourceSuccessed(LessonResource lessonResource) {
        if (lessonResource.getData() == null || lessonResource.getData().size() == 0) {
            rlResource.setVisibility(View.GONE);
            return;
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        resourceGridView.setLayoutManager(linearLayoutManager);
        //设置适配器
        lessonDatailResourceAdapter = new LessonDatailResourceAdapter(lessonResource.getData(), activity);
        resourceGridView.setAdapter(lessonDatailResourceAdapter);
    }

    @Override
    public void onSyncTaskVoSuccessed(SyncTaskVo syncTaskVo) {
        if (syncTaskVo.getData().size() < pageSize) {
            refreshLayout.setEnableLoadMore(false);
        } else {
            refreshLayout.setEnableLoadMore(true);
        }
        cancilLoadState();
        if (syncTaskVo.getData().size() == 0) {
//            llTaskEmptyTwo.setVisibility(View.VISIBLE);//更改  无内容
            mAdapter = new ActivityGvAdapter(activity);
            activityGridView.setAdapter(mAdapter);
            mAdapter.setData(null);
            return;
        }
        llTaskEmptyTwo.setVisibility(View.GONE);

        mAdapter.setData(syncTaskVo.getData());
    }

    @Override
    public void onDelSuccessed() {
        ToastUtil.showShortlToast("删除成功");
    }

    @Override
    public void onDelFailed() {
        getActivityList();
    }

    /**
     * 小组讨论列表成功
     */
    @Override
    public void onGroupChatSuccessed(GroupChatBean chatBean) {

        if (currPage == 1) {
            discussionGridViewAdapter = new NewDiscussionGridViewAdapter(this, chatBean.getData());
            gridView.setAdapter(discussionGridViewAdapter);
        } else {
            if (discussionGridViewAdapter != null) {
                discussionGridViewAdapter.notifyDataSetChanged();
            }
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == adapterView.getChildCount() - 1) {//添加
                if (i == 0) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.DiscussionNewActivity");
                    intent.putExtra("isEdit", false);
                    intent.putExtra("lessonId", lessonId);
                    intent.putExtra("volumeName", volumeName);
                    intent.putExtra("volumeId", volumeId);
                    intent.putExtra("chapterName", chapterName);
                    intent.putExtra("chapterId", chapterId);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rl_tvClass1) {
            showPop(tvClass1, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    baseClassId = dataBean.getClassId();
                    tvClass1.setText(dataBean.getClassName());
                    getActivityList();
                }
            });
        }
    }

    private void showPop(View view, OnClassClickListener onClassClickListener) {
        if (classPop == null) {
            classPop = new ClassPop(activity, onClassClickListener);
        }
        classPop.setClassVos(classVos);
        classPop.showPopupWindow(view);
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        getActivityList();
        mPresenter.getPrepareLessonsList(lessonId);
        mPresenter.getResourceLessonsList(lessonId);

        mPresenter.getGroupChapList(lessonId, currPage);
    }


    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getActivityList();

        mPresenter.getGroupChapList(lessonId, currPage);

    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        initData();
        if (Constant.getShowHeadCountDown() == true) {
            goToClass.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        currPage = 1;
        mPresenter.getGroupChapList(lessonId, currPage);
        mPresenter.getPrepareLessonsList(lessonId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLessonEvent(LessonDetailsGoClassEvent event) {
        goToClass.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEdit(String id, String mTitle, String content, String endTime, List<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> list) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.DiscussionNewActivity");
        intent.putExtra("isEdit", true);
        intent.putExtra("lessonId", lessonId);
        intent.putExtra("volumeName", volumeName);
        intent.putExtra("volumeId", volumeId);
        intent.putExtra("chapterName", chapterName);
        intent.putExtra("chapterId", chapterId);
        Bundle bundle = new Bundle();
        bundle.putString("title", mTitle);
        bundle.putString("content", content);
        bundle.putString("endTime", endTime);
        bundle.putParcelableArrayList("pic", (ArrayList) list);
        bundle.putString("id", id);
        intent.putExtras(bundle);
        startActivity(intent);
        mPresenter.deleteGroupList(id);
    }

    @Override
    public void onPreview(String id, String mTitle, String content, String endTime, List<GroupChatBean.DataBean.TeachGroupChatContentPicsBean> list) {
        Intent previewIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PreviewDiscussionActivity");
        Bundle bundle = new Bundle();
        bundle.putString("title", mTitle);
        bundle.putString("content", content);
        bundle.putString("endTime", endTime);
        bundle.putString("chapterName", chapterName);
        bundle.putParcelableArrayList("pic", (ArrayList) list);
        previewIntent.putExtras(bundle);
        startActivity(previewIntent);


    }

    @Override
    public void onDelete(String id) {
        mPresenter.deleteGroupList(id);
    }

    @Override
    public void onPublish(String id) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PublishDiscussionActivity");
        intent.putExtra("teachGroupChatLibId", id);
        intent.putExtra("lessonId", lessonId);
        intent.putExtra("volumeName", volumeName);
        intent.putExtra("volumeId", volumeId);
        intent.putExtra("chapterName", chapterName);
        intent.putExtra("chapterId", chapterId);
        startActivity(intent);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
