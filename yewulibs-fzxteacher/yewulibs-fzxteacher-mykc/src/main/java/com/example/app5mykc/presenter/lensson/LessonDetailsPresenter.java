package com.example.app5mykc.presenter.lensson;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.chatroom.GroupChatBean;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5mykc.view.lesson.LessonDetailsViwe;
import com.sdzn.fzx.teacher.vo.DelTask;
import com.sdzn.fzx.teacher.vo.LessonResource;
import com.sdzn.fzx.teacher.vo.PrepareLessonsVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.SyncTaskVo;

import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/2/28
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class LessonDetailsPresenter extends BasePresenter<LessonDetailsViwe, BaseActivity> {

    public void getClassList(String subject) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(subject)
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                      
                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }

    public void getPrepareLessonsList(String lessonId) {
        Network.createTokenService(NetWorkService.PrepareLessonsService.class)
                .getPrepareLessons(lessonId)
                .map(new StatusFunc<PrepareLessonsVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<PrepareLessonsVo>(new SubscriberListener<PrepareLessonsVo>() {
                    @Override
                    public void onNext(PrepareLessonsVo prepareLessonsVo) {
                        mView.onPrepareLessonsVoSuccessed(prepareLessonsVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void getGroupChapList(String lessonId,int page) {
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .GroupChatLib(lessonId,String.valueOf(page))
                .map(new StatusFunc<GroupChatBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<GroupChatBean>(new SubscriberListener<GroupChatBean>() {
                    @Override
                    public void onNext(GroupChatBean groupChatBean) {
                        mView.onGroupChatSuccessed(groupChatBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }


    public void getResourceLessonsList(String lessonId) {
        Network.createTokenService(NetWorkService.ResourceLessonsService.class)
                .getResourceLessons(lessonId)
                .map(new StatusFunc<LessonResource>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<LessonResource>(new SubscriberListener<LessonResource>() {
                    @Override
                    public void onNext(LessonResource lessonResource) {
                        mView.onLessonResourceSuccessed(lessonResource);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }


    public void getLessonsActivityList(Map<String,Object> map) {
        Network.createTokenService(NetWorkService.LessonsActivityService.class)
                .getLessonsActivity(map)
                .map(new StatusFunc<SyncTaskVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<SyncTaskVo>(new SubscriberListener<SyncTaskVo>() {
                    @Override
                    public void onNext(SyncTaskVo syncTaskVo) {
                        mView.onSyncTaskVoSuccessed(syncTaskVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void delTaskItem(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delTaskItem(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        mView.onDelSuccessed();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onDelFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    public void sendAnswer(String id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .sendAnswer(id)
                .map(new StatusFunc<Object>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    public void deleteGroupList(String id) {
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .deleteGroupLib(id)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onDelFailed();
                    }

                    @Override
                    public void onNext(Object o) {
                        mView.onRefresh();

                    }
                },mActivity,true,false,false,""));
    }

    public void delLessonDetail(int id) {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .delLessonTask(String.valueOf(id))
                .map(new StatusFunc<DelTask>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<DelTask>(new SubscriberListener<DelTask>() {
                    @Override
                    public void onNext(DelTask o) {
                        mView.onRefresh();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onDelFailed();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }
}
