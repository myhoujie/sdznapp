package com.example.app5mykc.presenter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5mykc.adapter.PublishCheckboxAdapter;
import com.example.app5mykc.view.PublishDiscussionView;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.chatroom.DiscussionClassGroup;
import com.example.app5libbase.listener.chatroom.OnItemPublishListener;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.views.NoScrollGridView;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class PublishDiscussionPresenter extends BasePresenter<PublishDiscussionView, BaseActivity> {


    public void getClassGroupList(final String classId, final String className) {
        Network.createTokenService(NetWorkService.GetClassGroupingService.class)
                .GetClassGrouping(classId)
                .map(new StatusFunc<ClassGroupingVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ClassGroupingVo>(new SubscriberListener<ClassGroupingVo>() {
                    @Override
                    public void onNext(ClassGroupingVo classGroupingVo) {
                        mView.setClassGropingData(classGroupingVo, classId, className);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
    }

    public void getClassList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getAllClassList()
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        mView.onClassSuccessed(o);

                    }
                });
    }

    public void bindHolder(int rvHeight, final GeneralRecyclerViewHolder holder, final DiscussionClassGroup classGroup) {
        LinearLayout llCheck = holder.getChildView(R.id.ll_check);
        final ImageView ivCheck = holder.getChildView(R.id.check);
        NoScrollGridView publishObject = holder.getChildView(R.id.publish_object);
        ivCheck.setEnabled(false);
        TextView textView = holder.getChildView(R.id.viewName);
        textView.setText(classGroup.getClassName());

        final List<String> addList = new ArrayList<>();
        final List<DiscussionClassGroup.DataB> myDataList = new ArrayList<>();
        mView.getClassGroupInfo(classGroup.getClassId(), myDataList);
        for (int i = 0; i < classGroup.getDataBList().size(); i++) {
            if (!addList.contains(classGroup.getDataBList().get(i).getGroupId())) {
                myDataList.add(new DiscussionClassGroup.DataB(classGroup.getDataBList().get(i).getGroupId(),
                        classGroup.getDataBList().get(i).getGroupName(), false));
                addList.add(classGroup.getDataBList().get(i).getGroupId());
            }
        }

        final PublishCheckboxAdapter adapter = new PublishCheckboxAdapter(mActivity);
        publishObject.setAdapter(adapter);
//       mView.getClassGroupInfo(classGroup.getClassId(), myDataList);
        adapter.setList(myDataList, new OnItemPublishListener() {

            @Override
            public void setOnCheckedListener(int listSize) {
                if (myDataList.size() > listSize) {
                    ivCheck.setEnabled(false);
                } else {
                    ivCheck.setEnabled(true);
                }
                mView.getClassGroupInfo(classGroup.getClassId(), adapter.getCheckedList());
            }
        });
        llCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myDataList.size() > adapter.getCheckedList().size()) {//未全选
                    ivCheck.setEnabled(true);
                    adapter.setCheckedIsTrue(true);
                } else {
                    ivCheck.setEnabled(false);
                    adapter.setCheckedIsTrue(false);
                }
                mView.getClassGroupInfo(classGroup.getClassId(), adapter.getCheckedList());
            }
        });


    }

    public void publishGroupList(Map<String, Object> pa) {
        Network.createTokenService(NetWorkService.GroupDiscussionService.class)
                .publishGroup(pa)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.networkError(e.toString());

                    }

                    @Override
                    public void onNext(Object o) {
                        mView.publishSuccessed();

                    }
                }, mActivity, true, false, false, ""));
    }


}