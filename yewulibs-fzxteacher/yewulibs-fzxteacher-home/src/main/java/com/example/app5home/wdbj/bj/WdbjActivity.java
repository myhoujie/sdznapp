package com.example.app5home.wdbj.bj;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.baselibrary.emptyview.EmptyView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.interfaces.XPopupCallback;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

import java.util.ArrayList;
import java.util.List;

public class WdbjActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;
    protected SmartRefreshLayout refreshLayout1;
    protected LinearLayout llDownTitle;
    private int checkedPos;//默认选中 年级
    private int checkRemovePos;//移除班级解除绑定


    @Override
    protected int getLayoutId() {
        return com.example.app5libbase.R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        refreshLayout1 = findViewById(com.example.app5libbase.R.id.refreshLayout1_order);
        llDownTitle = findViewById(R.id.ll_down_title);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("3", "0");
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                checkverionPresenter.checkVerion("3", "0");
            }
        });
        //使上拉加载具有弹性效果
        refreshLayout1.setEnableAutoLoadMore(false);
        //禁止越界拖动（1.0.4以上版本）
        refreshLayout1.setEnableOverScrollDrag(false);
        //关闭越界回弹功能
        refreshLayout1.setEnableOverScrollBounce(false);
        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
            @Override
            public boolean canRefresh(View content) {
                //webview滚动到顶部才可以下拉刷新
                MyLogUtil.e("ssssss", "" + mAgentWeb.getWebCreator().getWebView().getScrollY());
                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
            }

            @Override
            public boolean canLoadMore(View content) {
                return false;
            }

        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                checkverionPresenter.checkVerion("3", "0");
            }
        });
        initView();
    }

    /* 重载业务部分*/
    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        TitleShowHideState(12);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            List<GradeJson> grades = getGrades(aaa);
            aaa = getIntent().getStringExtra("title");

        }
    }

    private String aaa = "";

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initView() {
//        if (grades != null) {
//            for (GradeJson p1Info : grades) {
//                ToastUtils.showLong(p1Info.getDesc());
//            }
//        }
//        String aaa = getTitle1();
//        tvDownTitle.setText(metrics111.get(0).getDesc());
        checkedPos = 0;


        findViewById(R.id.tv_class_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(WdbjActivity.this)
                        .hasShadowBg(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .offsetY(20)
                        .offsetX(40)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList1(new String[]{"移除班级", "解除绑定"},
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        checkRemovePos = position;
                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    }
                                }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkRemovePos)
                        .show();

            }


        });

        findViewById(R.id.tv_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.showShort("更多");

            }


        });
    }

    //设备搜索
    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SheBeiSearchActivity");
        intent.putExtra("show", "str");
        startActivity(intent);
    }


    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        ToastUtils.showLong("数据已更新");
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title,"我的班级");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    private boolean is_onclik;

    @Override
    public void TitleDropdown() {
        super.TitleDropdown();
        ArrayList<String> stringArrayList = new ArrayList<String>();
//        for (int i = 0; i < metrics111.size(); i++) {
//            stringArrayList.add(metrics111.get(i).getDesc());
//        }
        stringArrayList.add("下拉标题一");
        stringArrayList.add("下拉标题二");
        stringArrayList.add("下拉标题三");

        String[] strArray = stringArrayList.toArray(new String[stringArrayList.size()]);
        if (!is_onclik) {
            arrowIv.setRotation(180);
            is_onclik = true;
        } else {
            arrowIv.setRotation(0);
            is_onclik = false;
        }
        new XPopup.Builder(WdbjActivity.this)
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .setPopupCallback(new XPopupCallback() {
                    @Override
                    public void onCreated(BasePopupView popupView) {

                    }

                    @Override
                    public void beforeShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                        arrowIv.setRotation(0);
                        is_onclik = false;
                    }

                    @Override
                    public void beforeDismiss(BasePopupView popupView) {

                    }

                    @Override
                    public boolean onBackPressed(BasePopupView popupView) {
                        return false;
                    }
                })

                .offsetY(30)
                .offsetX(-60)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                .atView(tvDownTitle)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList1(strArray,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
//                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                tvDownTitle.setText(text);
                                arrowIv.setRotation(0);
                                is_onclik = false;
                            }
                        }, com.example.app5libbase.R.layout.popup_adapter_rv, com.example.app5libbase.R.layout.popup_adapter_item, checkedPos)
                .show();
    }

    public GradeJson metrics;
    public List<GradeJson> metrics111;

    public List<GradeJson> getGrades(String aaaa) {
        metrics111 = new ArrayList<>();
        metrics = new GradeJson();
//        try {
//            InputStream open = this.getResources().getAssets().open("grade.json");
//            byte[] buffer = new byte[open.available()];
//            int read = open.read(buffer);
//            if (read != 0) {
////                String json = new String(buffer, "utf-8");
////                String json = getTitle1();
//                String json = URLDecoder.decode(aaaa,"UTF-8");
//                //
//                Gson gson = new GsonBuilder().create();
//                JsonParser jsonParser = new JsonParser();
//                JsonArray array = jsonParser.parse(json).getAsJsonArray();
//                for (JsonElement element : array) {
//                    metrics = gson.fromJson(element, GradeJson.class);
//                    //do something with the metric object. probably read all the datapoints and display
//                    if (metrics.getList() == null) {
//                        metrics.setList(new ArrayList<GradeJson.ListBean>());
//                    }
//                    if (TextUtils.isEmpty(metrics.getUrl())) {
//                        metrics.setUrl("");
//                    }
//                    metrics111.add(metrics);
//                }
//
////                List<GradeJson> lists = new Gson().fromJson(json, new TypeToken<GradeJson>() {
////                }.getType());
////                Log.e("aaatest", String.valueOf(lists));
////                return new Gson().fromJson(json, new TypeToken<GradeJson>() {
////                }.getType());
//            }
//        } catch (Exception e) {
//            LogUtils.e(e);
//        }
        return metrics111;
    }
}