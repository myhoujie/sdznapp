package com.example.app5home.hztj;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;


public class HeZuoNewItem1 extends BaseActFragment1 implements CheckverionViews {
    CheckverionFzxPresenter checkverionFzxPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newitem1;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionFzxPresenter.checkVerion("3", "0");
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) getActivity().findViewById(R.id.ll_base_container1);
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
