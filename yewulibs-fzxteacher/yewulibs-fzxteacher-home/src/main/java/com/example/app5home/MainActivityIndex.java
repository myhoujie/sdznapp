package com.example.app5home;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5home.adapter.ShouyeFooterAdapter;
import com.example.app5home.adapter.ShouyeFooterBean;
import com.example.app5home.renwu.RenwuFragment;
import com.example.app5home.shouye.ShouyeFragment;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.service.UpdataCommonservices;
import com.example.app5libbase.util.CircleTransform;
import com.google.gson.Gson;
import com.just.agentweb.WebViewClient;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.sdzn.fzx.teacher.bean.SubjectData;
import com.sdzn.fzx.teacher.bean.SubjectData1;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.MainIndexPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;
import com.sdzn.fzx.teacher.view.MainIndexViews;
import com.sdzn.fzx.teacher.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.AutoSizeCompat;

public class MainActivityIndex extends BaseActWebActivity1 implements CheckverionViews, MainIndexViews, View.OnClickListener {

    private RecyclerView recyclerView;//
    private ShouyeFooterAdapter mAdapter;

    private FragmentManager mFragmentManager;

    private int current_pos = 0;
    private String tag_ids;
    private List<ShouyeFooterBean> mList;
    public static final String id1 = "11";
    public static final String id2 = "22";
    private static final String LIST_TAG1 = "list11";
    private static final String LIST_TAG2 = "list22";

    private ShouyeFragment mFragment1; //
    private RenwuFragment mFragment2; //

    private LinearLayout llUserInformation;//个人信息
    private ImageView ivSetup;//设置
    private LinearLayout llSubject;//选择学科
    private TextView tvSubject;
    private int checkedPos = 2;//默认学科
    String morenStr = "物理";//实际项目应去掉
    private ImageView ivMeHead;
    private TextView tvMeName;
    MainIndexPresenter mainIndexPresenter;
    private LoginBean data;

    private String userInfo;//添加学科的用户信息
    List<SubjectData> dataList;//学科集合
    private String subjectid;//学科id


    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, com.example.app5libbase.R.color.color_FB6215), 0);
        data = com.sdzn.fzx.teacher.utils.SPUtils.getLoginBean();
        mainIndexPresenter = new MainIndexPresenter();
        mainIndexPresenter.onCreate(this);
        mainIndexPresenter.getSubjectInfo(data.getData().getUser().getId());

        findview();
        onclick();
        mFragmentManager = getSupportFragmentManager();
        // 解决fragment布局重叠错乱
        if (savedInstanceState != null) {
            mFragment1 = (ShouyeFragment) mFragmentManager.findFragmentByTag(LIST_TAG1);
            mFragment2 = (RenwuFragment) mFragmentManager.findFragmentByTag(LIST_TAG2);
        }
        doNetWork();
        checkLatestMedal();//启动服务
    }

    private void checkLatestMedal() {
        Intent intent = new Intent(this, UpdataCommonservices.class);
        intent.setAction(UpdataCommonservices.HUIBEN_READINGTIME_ACTION);
        startService(intent);
    }

    private void doNetWork() {
        mList = new ArrayList<>();
        addList();
        recyclerView.setLayoutManager(new GridLayoutManager(this, mList.size(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();
        current_pos = 0;
        footer_onclick();
        if (BuildConfig3.versionNameConfig == "线上") {

        }
    }

    //点击item
    private void footer_onclick() {
        final ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(current_pos);
        if (model.isEnselect()) {
            // 不切换当前的item点击 刷新当前页面
            showFragment(model.getText_id(), true);
        } else {
            // 切换到另一个item
            if (model.getText_id().equalsIgnoreCase(id2)) {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            } else {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            }
        }
    }

    private void set_footer_change(ShouyeFooterBean model) {
        //设置为选中
        initList();
        model.setEnselect(true);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();

    }

    private void findview() {
        recyclerView = findViewById(R.id.recycler_view1);
        mAdapter = new ShouyeFooterAdapter(this);
        tvSubject = (TextView) findViewById(R.id.tv_subject);
        llUserInformation = (LinearLayout) findViewById(R.id.ll_user_information);
        ivSetup = (ImageView) findViewById(R.id.iv_setup);
        llSubject = (LinearLayout) findViewById(R.id.ll_subject);
        ivMeHead = findViewById(R.id.iv_meHead);
        tvMeName = findViewById(R.id.tv_meName);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
        }
//        mAgentWeb.getWebCreator().getWebView().loadUrl("file:///android_asset/js_interaction/wdkc.html");
        mAgentWeb.getWebCreator().getWebView().loadUrl("http://192.168.6.77:8080/#/setToken");
        final String token = SPUtils.getInstance().getString("token", "");

        mAgentWeb.getWebCreator().getWebView().setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:getTokenAndUserInfo('" + token + "','" + userInfo + "')");
            }
        });


        if (data == null) {
            return;
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getRealName())) {
            tvMeName.setText(data.getData().getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getPhoto())) {
            RequestOptions error = new RequestOptions().error(com.example.app5grzx.R.mipmap.tx_img).transform(new CircleTransform(this)).placeholder(com.example.app5grzx.R.mipmap.tx_img);
            Glide.with(this).load(data.getData().getUser().getPhoto()).apply(error).into(ivMeHead);
        }
        llUserInformation.setOnClickListener(this);
        ivSetup.setOnClickListener(this);
        llSubject.setOnClickListener(this);
    }


    private void onclick() {
        mAdapter.setOnItemClickLitener(new ShouyeFooterAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item
                current_pos = position;
                footer_onclick();
            }
        });
    }

    private void addList() {
        mList.add(new ShouyeFooterBean(id1, "我的首页", true));
        mList.add(new ShouyeFooterBean(id2, "AI智能学习", false));
    }

    private void initList() {
        for (int i = 0; i < mList.size(); i++) {
            ShouyeFooterBean item = mList.get(i);
            if (item.isEnselect()) {
                item.setEnselect(false);
            }
        }
    }

    private void showFragment(final String tag, final boolean isrefresh) {
        tag_ids = tag;
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragments(transaction);

        if (tag.equalsIgnoreCase("-1")) {
//            if (mFragment1 == null) {
//                mFragment1 = new FragmentContent1();
//                transaction.add(R.id.container, mFragment1, LIST_TAG0);
//            } else {
//                transaction.show(mFragment1);
//                mFragment1.initData();
//            }
        } else if (tag.equalsIgnoreCase(id1)) {
            if (mFragment1 == null) {
                mFragment1 = new ShouyeFragment();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment1.setArguments(args);
                transaction.add(R.id.container, mFragment1, LIST_TAG1);
            } else {
                transaction.show(mFragment1);
                mFragment1.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id2)) {
            if (mFragment2 == null) {
                mFragment2 = new RenwuFragment();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment2.setArguments(args);
                transaction.add(R.id.container, mFragment2, LIST_TAG2);
            } else {
                transaction.show(mFragment2);
                mFragment2.getCate(tag, isrefresh);
            }
        }
        transaction.commitAllowingStateLoss();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mFragment1 != null) {
            transaction.hide(mFragment1);
            mFragment1.give_id(tag_ids);
        }
        if (mFragment2 != null) {
            transaction.hide(mFragment2);
            mFragment2.give_id(tag_ids);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private long exitTime;

    /**
     * 连按两次退出程序
     */
    private void exit() {
        ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(0);
        if (model != null && !tag_ids.equals(model.getText_id())) {
            set_footer_change(model);
            showFragment(model.getText_id(), false);
        } else {
            if ((System.currentTimeMillis() - exitTime) < 1500) {
//                RichText.recycle();
                ActivityManager.exit();
//                BaseAppManager.getInstance().closeApp();
            } else {
                ToastUtils.showShort("再按一次退出程序 ~");
                exitTime = System.currentTimeMillis();
            }
        }
    }


    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {

    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ll_user_information) {//个人中心
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            intent.putExtra("show", 0);
            startActivity(intent);
        } else if (id == R.id.iv_setup) {//系统设置
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//            intent.putExtra("show", 8);
//            startActivity(intent);
            new XPopup.Builder(MainActivityIndex.this)
                    //.dismissOnBackPressed(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .autoOpenSoftInput(true)
//                        .autoFocusEditText(false) //是否让弹窗内的EditText自动获取焦点，默认是true
                    .isRequestFocus(false)
                    //.moveUpToKeyboard(false)   //是否移动到软键盘上面，默认为true
                    .asInputConfirm("标题", "http://49.4.7.45:8090/#/", SPUtils.getInstance().getString("url", "https://www.baidu.com/"), "默认Hint文字",
                            new OnInputConfirmListener() {
                                @Override
                                public void onConfirm(String text) {
                                    SPUtils.getInstance().put("url", text);
                                }
                            })
                    .show();
        } else if (id == R.id.ll_subject) {//设置学科
            List<String> stringList = new ArrayList<>();
            if (dataList.size() != 0) {
                for (int i = 0; i < dataList.size(); i++) {
                    stringList.add(dataList.get(i).getSubjectName());
                }
            }
            String[] title1 = stringList.toArray(new String[stringList.size()]);
            new XPopup.Builder(MainActivityIndex.this)
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .offsetY(20)
                    .offsetX(40)
                    .atView(view)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                    .asAttachList1(title1,
                            new int[]{},
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
                                    subjectid = dataList.get(position).getSubjectId();
                                    tvSubject.setText(text);
                                    //实际项目中  list集合中获取 position 的text
//                                    ToastUtils.showShort(text + "--->刷新当前页学科" + subjectid);
                                    SPUtils.getInstance().put("shouye_class_name", subjectid);
//                                    SPUtils.getInstance().getString("shouye_class_name","0");
                                }
                            }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                    .show();

        }
    }


    @Override
    public void getSubjectInfo(SubjectData1 subjectData) {
        if (subjectData != null) {
            dataList = subjectData.getData();
            userInfo = jsUserBean(subjectData);
//            LogUtils.e(userInfo);
        }
    }

    @Override
    public void OnSubjectNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onSubjectFailed(String msg) {
        ToastUtils.showLong(msg);
    }

    public String jsUserBean(SubjectData1 subjectData) {
//        List<JsTopBean> jsonList=new ArrayList<>();
        LoginBean data = com.sdzn.fzx.teacher.utils.SPUtils.getLoginBean();
        data.getData().getUser().setSubjectList(subjectData.getData());
        data.getData().getUser().setSubjectType(subjectData.getData().get(0).getSubjectId());

//        JsTopBean jsTopBean=new JsTopBean();
//        jsTopBean.setContent(data.getData().getUser());
//        jsTopBean.setDataType("object");
//        jsTopBean.setDataTime(data.getData().getUser().getCreateTime());
//        jsonList.add(jsTopBean);
        final String loginJsonStr = new Gson().toJson(data.getData().getUser());
//        ToastUtils.showLong(loginJsonStr);
        return loginJsonStr;
    }
}
