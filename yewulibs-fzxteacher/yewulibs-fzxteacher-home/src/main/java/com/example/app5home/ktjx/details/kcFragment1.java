package com.example.app5home.ktjx.details;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.baselibrary.LocalBroadcastManagers;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

/*
 * 备课记录首页
 *
 * */
public class kcFragment1 extends BaseActFragment1 implements CheckverionViews {
    CheckverionFzxPresenter checkverionFzxPresenter;

    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("kcFragment1".equals(intent.getAction())) {
                    String url = intent.getStringExtra("url_key");
                    updateurl(url);
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_jxkc;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("kcFragment1");
        LocalBroadcastManagers.getInstance(getActivity()).registerReceiver(mMessageReceiver, filter);
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionFzxPresenter.checkVerion("3", "0");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManagers.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        LogUtils.e("BaseActFragment------getAgentWebParent");
        return (ViewGroup) getActivity().findViewById(R.id.ll_base_container_jxkc);
    }

    private void updateurl(String url) {
        AgentwebRefresh(url);
    }

    public String getUrl() {
        Bundle bundle = this.getArguments();
        String target = bundle.getString("url_key");
        LogUtils.e("targetaaaaaaa=" + target);
        if (TextUtils.isEmpty(target)) {
            target = "http://www.jd.com/";
        }
        return target;
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
