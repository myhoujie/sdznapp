package com.example.app5home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5grzx.fragment.MeFragment;
import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.app.NetworkReceiver;
import com.example.app5xztl.fragment.GroupDiscussionFragment;
import com.example.app5libbase.service.DownService;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.base.BaseProgressCallBack;
import com.example.app5libbase.base.BaseView;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.controller.UserController;
import com.example.app5libpublic.event.BaiBanScreenEvent;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.OnBackEvent;
import com.example.app5libpublic.event.OnScreenCaptureEvent;
import com.example.app5libpublic.event.TinkerResult;
import com.example.app5libpublic.event.ToLessonEvent;
import com.example.app5libpublic.event.ToTaskEvent;
import com.example.app5libpublic.event.UpdataPhoto;
import com.example.app5libpublic.event.UpdateSubject;
import com.example.app5mykc.fragment.LessonFragment;
import com.example.app5home.fragment.MainFragment;
import com.example.app5xzph.fragment.RankFragment;
import com.example.app5jxtj.fragment.StatisticsFragment;
import com.example.app5kcrw.fragment.TaskFragment;
import com.example.app5libbase.listener.OnSubjectClickListener;
import com.example.app5libbase.pop.SubjectPop;

import com.example.app5libbase.util.CircleTransform;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.CustomDialog;
import com.example.app5libbase.views.RoundAngleImageView;
import com.example.app5libbase.views.UpdateDialog;
import com.sdzn.fzx.teacher.vo.CharactersListVo;
import com.sdzn.fzx.teacher.vo.HotFixVo;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.UpdateVo;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.tencent.tinker.lib.tinker.TinkerInstaller;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.EasyPermissions;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.example.app5libbase.util.base.TimeBaseUtil.REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN;
import static com.example.app5libbase.util.base.TimeBaseUtil.REQUEST_MEDIA_PROJECTION_CAPTURE;


public class MainActivity extends MBaseActivity<BasePresenter> implements BaseView, RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private RelativeLayout netErrorRy;
    private RelativeLayout rlPhoto;
    private RoundAngleImageView ivPhoto;
    private ImageView ivManager;
    private TextView tvName;
    private RadioGroup radioGroup;
    private RadioButton rbMain;
    private RadioButton rbLesson;
    private RadioButton rbTask;
    private RadioButton rbGroup;
    private RadioButton statistical;
    private RadioButton rbRank;

    private FrameLayout framelayoutMain;

    private List<BaseFragment> fragments;
    private int containerId = R.id.framelayoutMain;
    private Fragment currFragment;
    private SubjectPop subjectPop;

    private static boolean isExit;//标示是否退出

    private static final int REQUECT_CODE_SDCARD = 1000;
//    private static final int TUYA_SCREEN = 101;
//    private static final int BAIBAN_SCREEN = 102;
    ProgressDialogManager manager;
//    private int type;

    @Override
    public void initPresenter() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //删除保存的Fragment，也可以重写onSaveInstanceState方法不让其保存，目的是防止activity被回收造成fragment数据或栈异常
        if (savedInstanceState != null) {
            savedInstanceState.putParcelable("android:support:fragments", null);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        rlPhoto = (RelativeLayout) findViewById(R.id.rlPhoto);
        ivPhoto = (RoundAngleImageView) findViewById(R.id.ivPhoto);
        ivManager = (ImageView) findViewById(R.id.ivManager);
        tvName = (TextView) findViewById(R.id.tvName);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbMain = (RadioButton) findViewById(R.id.rbMain);
        rbLesson = (RadioButton) findViewById(R.id.rbLesson);
        rbTask = (RadioButton) findViewById(R.id.rbTask);
        rbGroup = (RadioButton) findViewById(R.id.rbGroup);
        statistical = (RadioButton) findViewById(R.id.statistical);
        rbRank = (RadioButton) findViewById(R.id.rbRank);
        framelayoutMain = (FrameLayout) findViewById(R.id.framelayoutMain);
        rlPhoto.setOnClickListener(this);

        EventBus.getDefault().register(this);
        x.view().inject(this);
        manager = new ProgressDialogManager(MainActivity.this);
        initFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
        initView();
        saveTeacherInfo();
        getCharactersList();
        checkUpdate();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入MainActivity成功");
                }
            }
        }
    }

    private void getCharactersList() {
        Network.createTokenService(NetWorkService.MainService.class)
                .getCharactersList()
                .map(new StatusFunc<CharactersListVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<CharactersListVo>(new SubscriberListener<CharactersListVo>() {
                    @Override
                    public void onNext(CharactersListVo bean) {
                        if (bean != null && bean.getData() != null && !bean.getData().isEmpty()) {
                            SubjectSPUtils.saveFileRuleSet(bean.getData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {



                    }

                    @Override
                    public void onCompleted() {
                    }
                }, MainActivity.this, false, true, false, ""));
    }

    private ServiceConnection conn;

    private void checkUpdate() {
        //tinker
        File dir = Environment.getExternalStorageDirectory();
        TinkerInstaller.onReceiveUpgradePatch(this, dir.getAbsolutePath() + "/patchFrom" + BuildConfig3.VERSION_CODE);

        Map<String, String> params = new HashMap<>();
        params.put("appVersionNum", String.valueOf(AndroidUtil.getVerCode(App2.get())));
        params.put("programName", "辅助线教师端APP");
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            params.put("customerSchoolId", String.valueOf(loginBean.getData().getUser().getCustomerSchoolId()));
        }
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .checkUpdate(params)
                .map(new StatusFunc<UpdateVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UpdateVo>(new SubscriberListener<UpdateVo>() {
                    @Override
                    public void onNext(UpdateVo updateVo) {
                        if (updateVo != null && updateVo.getData() != null && updateVo.getData().getAppVersionNum() > AndroidUtil.getVerCode(App2.get())) {
                            downloadApk(updateVo);
                        } else {
                            checkHotFix();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, MainActivity.this, false, true, false, ""));
    }


    private void checkHotFix() {
        Network.createTokenService(NetWorkService.MainService.class)
                .checkHotfix(1)
                .map(new StatusFunc<HotFixVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<HotFixVo>(new SubscriberListener<HotFixVo>() {
                    @Override
                    public void onNext(HotFixVo hotFixVo) {
                        if (hotFixVo != null && hotFixVo.getData() != null && hotFixVo.getData().getAppVersionNum() > BuildConfig3.TINKER_VERSION) {
                            downloadHotFix(hotFixVo.getData().getAppPath());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, MainActivity.this, false, true, false, ""));
    }

    private void downloadHotFix(String url) {
        File dir = new File(FileUtil.getAppFilesDir() + "/sdzn/teacher");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File file = new File(dir, "hotfix.fix");
        if (file.exists()) {
            FileUtil.delete(file);
        }
        RequestParams params = new RequestParams(url);
        //设置断点续传
        params.setAutoResume(true);
        params.setSaveFilePath(file.getAbsolutePath());
        x.http().get(params, new BaseProgressCallBack<File>() {
            @Override
            public void onSuccess(File result) {
                if (result.getAbsoluteFile().exists()) {
                    manager.showWaiteDialog("正在安装补丁, 请稍后...", false);
                    TinkerInstaller.onReceiveUpgradePatch(MainActivity.this, result.getAbsolutePath());
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                ToastUtil.showShortlToast("下载补丁包失败");
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTinkerCallback(TinkerResult event) {
        if (manager == null) {
            return;
        }
        manager.cancelWaiteDialog();
        ToastUtil.showShortlToast(event.success ? "补丁安装成功, 请重启应用" : "补丁安装失败");
    }

    private ProgressDialog pd;
    private UpdateVo vo;

    private void downloadApk(final UpdateVo updateVo) {
        vo = updateVo;
        // 需要更新，弹出更新窗口
        UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
        if (!TextUtils.isEmpty(updateVo.getData().getAppVersionDesc())) {
            builder.setMessage(updateVo.getData().getAppVersionDesc());
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 下载更新包
                Intent intent = new Intent(App2.get(), DownService.class);
                intent.putExtra(DownService.BOX_PATH, updateVo.getData().getBoxPath());
                intent.putExtra(DownService.APP_PATH, updateVo.getData().getAppPath());
                if (isServiceRunning(DownService.class.getName())) {
                    App2.get().stopService(intent);
                }
                App2.get().startService(intent);

                conn = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        final DownService.DownBinder downBinder = (DownService.DownBinder) service;
                        DownService downService = downBinder.getService();
                        downService.setOnProgressLintener(new DownService.OnProgressListener() {
                            @Override
                            public void onProgressChanged(int progress) {
                                if (pd != null) {
                                    pd.setProgress(progress);
                                }
                            }

                            @Override
                            public void onFailed(Throwable throwable) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                                if (updateVo.getData().getMustUpgrade() == 1) {
                                    ActivityManager.exit();
                                }
                            }

                            @TargetApi(26)
                            @Override
                            public void requestPermission(Uri data) {
                                downloadUri = data;
                                startInstallPermissionSettingActivity();
                            }
                        });
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {

                    }
                };
                App2.get().bindService(intent, conn, Service.BIND_AUTO_CREATE);
                // 如果是强制升级，显示一个进度条
                if (updateVo.getData().getMustUpgrade() == 1) {
                    pd = new ProgressDialog(MainActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
            }
        });
        builder.setNegative(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (updateVo.getData().getMustUpgrade() == 1) {
                    ActivityManager.exit();
                }
            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private Uri downloadUri;

    public void installApk(boolean isok) {
        if (downloadUri == null) {
            return;
        }
        if (!isok) {
            if (vo.getData().getMustUpgrade() == 1) {
                //request
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission =  App2.get().getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                return;
                //return
            }
        }
        //install
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(downloadUri, "application/vnd.android.package-archive");
        MainActivity.this.startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {//注意这个是8.0新API
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainActivity.this.startActivityForResult(intent, REQUEST_INSTALL_PERMISSION);
    }

    public static final int REQUEST_INSTALL_PERMISSION = 13131;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_INSTALL_PERMISSION) {
            installApk(resultCode == RESULT_OK);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 用来判断服务是否运行.
     *
     * @param className 判断的服务名字：包名+类名
     * @return true 在运行, false 不在运行
     */
    public boolean isServiceRunning(String className) {
        boolean isRunning = false;
        android.app.ActivityManager activityManager = (android.app.ActivityManager) MainActivity.this
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<android.app.ActivityManager.RunningServiceInfo> serviceList = activityManager
                .getRunningServices(Integer.MAX_VALUE);
        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    @Override
    protected void initView() {
        radioGroup.setOnCheckedChangeListener(this);
        rlPhoto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showSubjectPop(rlPhoto, new OnSubjectClickListener() {
                    @Override
                    public void onSubjectClick(LoginBean.DataBean.SubjectListBean subject) {
                        resetApp();
                    }
                });
                return false;
            }
        });
        if (UserController.isManager()) {
            ivManager.setVisibility(View.VISIBLE);
        } else {
            ivManager.setVisibility(View.GONE);
        }
        tvName.setText(SubjectSPUtils.getCurrentSubject() != null ? SubjectSPUtils.getCurrentSubject().getSubjectName() : "");
        setPhoto();
    }

    private void resetApp() {
        tvName.setText(SubjectSPUtils.getCurrentSubject() != null ? SubjectSPUtils.getCurrentSubject().getSubjectName() : "");
        // TODO 更新界面
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        for (Fragment fragment : fragments) {
            if (fragment.isAdded()) {
                ft.remove(fragment);
            }
        }
        ft.commit();
        initFragment();
        if (radioGroup.getCheckedRadioButtonId() == R.id.rbMain) {
            showAssignedFragment(0);
        } else {
            radioGroup.check(R.id.rbMain);
        }
    }

    @Override
    protected void initData() {

    }

    private void showSubjectPop(View view, OnSubjectClickListener onSubjectClickListener) {
        if (subjectPop == null) {
            subjectPop = new SubjectPop(activity, onSubjectClickListener);
        }
        List<LoginBean.DataBean.SubjectListBean> subjectList = UserController.getSubjectList();
        subjectPop.showPopupWindow(view, subjectList);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbMain) {
            showAssignedFragment(0);
        } else if (checkedId == R.id.rbLesson) {
            showAssignedFragment(1);
        } else if (checkedId == R.id.rbTask) {
            showAssignedFragment(2);
        } else if (checkedId == R.id.statistical) {
            showAssignedFragment(5);
        } else if (checkedId == R.id.rbRank) {
            showAssignedFragment(3);
        } else if (checkedId == R.id.rbGroup) {
            showAssignedFragment(6);
        }
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        //首页
        fragments.add(MainFragment.newInstance(null));
        //我的课程
        fragments.add(LessonFragment.newInstance(null));
        //课程任务
        fragments.add(TaskFragment.newInstance(null));
        //小组排行
        fragments.add(RankFragment.newInstance(null));
        //个人中心
        fragments.add(MeFragment.newInstance(null));
        //教学统计
        fragments.add(StatisticsFragment.newInstance(null));
        //小组讨论
        fragments.add(GroupDiscussionFragment.newInstance(null));
    }

    private void showAssignedFragment(int fragmentIndex) {
        BaseFragment fragment = fragments.get(fragmentIndex);
        if (currFragment != fragment) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (!fragment.isAdded()) {
                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
            } else {
                ft.hide(currFragment).show(fragment);
            }
            currFragment = fragment;
            ft.commit();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 连按两次退出程序
     */
    public void exit() {
        if (!isExit) {
            isExit = true;
            ToastUtil.showShortlToast("再按一次退出程序");
            MyHandler myHandler = new MyHandler();
            myHandler.sendEmptyMessageDelayed(100, 2000);
        } else {
            //9.26 增加调用退出登录接口
            //友盟退出登录
            ActivityManager.exit();
        }
    }

    private static class MyHandler extends Handler {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            switch (msg.what) {
                case 100:
                    isExit = false;
                    break;
                default:
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(Event event) {
        if (event instanceof OnBackEvent) {
            exit();
        } else if (event instanceof OnScreenCaptureEvent) {
//            type = TUYA_SCREEN;
            requestPermissiontest();
        } else if (event instanceof BaiBanScreenEvent) {
//            type = BAIBAN_SCREEN;
            requestPermissiontest1();
        } else if (event instanceof ToTaskEvent) {
            radioGroup.check(R.id.rbTask);
        } else if (event instanceof ToLessonEvent) {
            radioGroup.check(R.id.rbLesson);
        } else if (event instanceof UpdateSubject) {
            resetApp();
        } else if (event instanceof UpdataPhoto) {
            setPhoto();
        }
    }

    public void requestPermissiontest() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事
            if (ActivityManager.getForegroundActivity() != null) {
                if (TimeBaseUtil.getInstance().mMediaProjection == null) {
                    ActivityManager.getForegroundActivity().startActivityForResult(
                            TimeBaseUtil.getInstance().mMediaProjectionManager.createScreenCaptureIntent(),
                            REQUEST_MEDIA_PROJECTION_CAPTURE);
                } else {
                    TimeBaseUtil.getInstance().getScreen();
                }
            }
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_SDCARD, perms);
        }
    }

    public void requestPermissiontest1() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事
            if (ActivityManager.getForegroundActivity() != null) {
                if (TimeBaseUtil.getInstance() == null) {
                    ActivityManager.getForegroundActivity().startActivityForResult(
                            TimeBaseUtil.getInstance().mMediaProjectionManager.createScreenCaptureIntent(),
                            REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN);
                } else {
                    TimeBaseUtil.getInstance().getBaiBANScreen();
                }
            }
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置访问SD卡权限", REQUECT_CODE_SDCARD, perms);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    /*   @PermissionGrant(REQUECT_CODE_SDCARD)
       public void requestSdcardSuccess() {
           switch (type){
               case TUYA_SCREEN:
                   if (ActivityManager.getForegroundActivity() != null) {
                       if (AppLike.mMediaProjection == null) {
                           ActivityManager.getForegroundActivity().startActivityForResult(
                                   mMediaProjectionManager.createScreenCaptureIntent(),
                                   REQUEST_MEDIA_PROJECTION_CAPTURE);
                       } else {
                           ((BaseActivity) ActivityManager.getForegroundActivity()).getScreen();
                       }
                   }
                   break;
               case BAIBAN_SCREEN:
                   if (ActivityManager.getForegroundActivity() != null) {
                       if (AppLike.mMediaProjection == null) {
                           ActivityManager.getForegroundActivity().startActivityForResult(
                                   mMediaProjectionManager.createScreenCaptureIntent(),
                                   REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN);
                       } else {
                           ((BaseActivity) ActivityManager.getForegroundActivity()).getBaiBANScreen();
                       }
                   }
                   break;
           }

       }



       @PermissionDenied(REQUECT_CODE_SDCARD)
       public void requestSdcardFailed() {
           ToastUtil.showShortlToast("访问SD卡权限被拒绝");
       }
   */
    private void setPhoto() {
        Glide.with(activity).load(UserController.getPhoto())
                .apply(new RequestOptions().error(R.mipmap.mrtx_img).transform(new CircleTransform(activity)).placeholder(R.mipmap.mrtx_img))
//                .error(R.mipmap.mrtx_img)
//                .transform(new CircleTransform(activity))
                .into(ivPhoto);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (netErrorRy == null)
            return;
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.rlPhoto) {
            radioGroup.clearCheck();
            showAssignedFragment(4);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    public void saveTeacherInfo() {

        Network.createTokenService(NetWorkService.SaveUserInfoService.class)
                .getUserInfo(SPUtils.getLoginBean().getData().getId() + "", getVersionName() + "")
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object object) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {

                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, this, false));
    }

    private int getVersionName() {
        //获取packagemanager的实例
        PackageManager packageManager = getPackageManager();
        //getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.e("TAG", "版本号" + packInfo.versionCode);
        Log.e("TAG", "版本名" + packInfo.versionName);
        return packInfo.versionCode;
    }

}
