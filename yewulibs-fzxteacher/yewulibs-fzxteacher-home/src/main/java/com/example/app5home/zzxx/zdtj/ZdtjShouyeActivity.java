package com.example.app5home.zzxx.zdtj;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActNoWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.HCategoryBean;
import com.sdzn.fzx.teacher.bean.HCategoryBean1;
import com.sdzn.fzx.teacher.bean.OneBean1;

import java.util.ArrayList;
import java.util.List;

/*
 * 自主学习批改首页
 * */
public class ZdtjShouyeActivity extends BaseActNoWebActivity1 implements BaseOnClickListener {
    private ViewPagerSlide viewpagerBaseactTablayout;

    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    private MessageReceiverIndex mMessageReceiver;
    private int defCurrentItem = 0;//设置默认见面

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("zdtjActivity".equals(intent.getAction())) {
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("zdtjActivity");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        if ("1".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 0;
        } else if ("2".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 1;
        }
        TitleShowHideState(3);
        setBaseOnClickListener(this);
        initviw();
    }

    private void initviw() {
        tvTitleName.setText("自主学习");
        onclick();
        donetworkdemo();
    }

    private void donetworkdemo() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "作答统计"));
        mDataTablayout1.add(new HCategoryBean1("2", "题目分析"));
        mDataTablayout1.add(new HCategoryBean1("3", "资料分析"));
        mDataTablayout1.add(new HCategoryBean1("4", "典型讲解"));
        mDataTablayout1.add(new HCategoryBean1("5", "问题记录"));
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        //
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }

    public static final String URL_KEY = "url_key";

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, "file:///android_asset/js_interaction/zdtj.html");
                ZdtjFragment fragment1 = FragmentHelper.newFragment(ZdtjFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, "file:///android_asset/js_interaction/tmfx.html");
                TmfxFragment fragment1 = FragmentHelper.newFragment(TmfxFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 2) {
                bundle.putString(URL_KEY, "file:///android_asset/js_interaction/zlfx.html");
                ZlfxFragment fragment2 = FragmentHelper.newFragment(ZlfxFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 3) {
                bundle.putString(URL_KEY, "file:///android_asset/js_interaction/dxjj.html");
                DxjjFragment fragment2 = FragmentHelper.newFragment(DxjjFragment.class, bundle);
                mFragmentList.add(fragment2);
            } else {
                bundle.putString(URL_KEY, "file:///android_asset/js_interaction/wtjl.html");
                WtjlFragment fragment2 = FragmentHelper.newFragment(WtjlFragment.class, bundle);
                mFragmentList.add(fragment2);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(6);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcSouSuoActivity");
        intent.putExtra("show", "str");
        startActivity(intent);
    }

}