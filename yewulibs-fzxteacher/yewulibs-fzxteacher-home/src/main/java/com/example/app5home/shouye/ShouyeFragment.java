package com.example.app5home.shouye;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.presenter.ShouyePresenter;
import com.sdzn.fzx.teacher.view.ShouyeViews;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 首页
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class ShouyeFragment extends Fragment implements View.OnClickListener, ShouyeViews {
    private TextView tvDynamicTitle;//广告标题
    private ImageView ivMyIconCurriculum;//我的课程
    private LinearLayout llAutonomyStudy;//自主学习
    private LinearLayout llClassroomTeaching;//课堂教学
    private LinearLayout llTask;//作业
    private RelativeLayout rlExamination;//考试
    private RelativeLayout rlScienceAnalysis;//学情分析
    private RelativeLayout rlMyClass;//我的班级
    private RelativeLayout rlCorrecting;//批改
    private RelativeLayout rlCexplore;//合作探究
    private LinearLayout llThinkTankSchool;//智囊学堂跳转项目
    private LinearLayout llBaiduRncyclopedia;//百度百科

    private TextView tvShouyeZzxxTask;
    private TextView tvKtjxTask;
    private TextView tvJrkcTask;
    private TextView tvZuoyeTask;
    private TextView tvHztjTask;
    private TextView tvPigiaTask;
    private TextView tvXqfxTask;


    ShouyePresenter shouyePresenter;
    private long mCurrentMs = System.currentTimeMillis();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shouye, container, false);
        tvDynamicTitle = view.findViewById(R.id.tv_dynamic_title);
        ivMyIconCurriculum = view.findViewById(R.id.iv_my_icon_curriculum);
        llAutonomyStudy = view.findViewById(R.id.ll_autonomy_study);
        llClassroomTeaching = view.findViewById(R.id.ll_Classroom_teaching);
        llTask = view.findViewById(R.id.ll_task);
        rlExamination = view.findViewById(R.id.rl_examination);
        rlScienceAnalysis = view.findViewById(R.id.rl_scienceAnalysis);
        rlMyClass = view.findViewById(R.id.rl_myClass);
        rlCorrecting = view.findViewById(R.id.rl_correcting);
        rlCexplore = view.findViewById(R.id.rl_cexplore);
        llThinkTankSchool = view.findViewById(R.id.ll_think_tank_school);
        llBaiduRncyclopedia = view.findViewById(R.id.ll_baidu_rncyclopedia);

        /*首页更新的textView*/
        tvShouyeZzxxTask = view.findViewById(R.id.tv_shouye_zzxx_task);
        tvKtjxTask = view.findViewById(R.id.tv_ktjx_task);
        tvJrkcTask = view.findViewById(R.id.tv_jrkc_task);
        tvZuoyeTask = view.findViewById(R.id.tv_zuoye_task);
        tvHztjTask = view.findViewById(R.id.tv_hztj_task);
        tvPigiaTask = view.findViewById(R.id.tv_pigia_task);
        tvXqfxTask = view.findViewById(R.id.tv_xqfx_task);

        ivMyIconCurriculum.setOnClickListener(this);
        llAutonomyStudy.setOnClickListener(this);
        llClassroomTeaching.setOnClickListener(this);
        llTask.setOnClickListener(this);
        rlExamination.setOnClickListener(this);
        rlScienceAnalysis.setOnClickListener(this);
        rlMyClass.setOnClickListener(this);
        rlCorrecting.setOnClickListener(this);
        rlCexplore.setOnClickListener(this);
        llThinkTankSchool.setOnClickListener(this);
        llBaiduRncyclopedia.setOnClickListener(this);
        set_update_progress_ui();
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);
        shouyePresenter.queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""));
        return view;
    }

    private Handler mHandler;
    private ScheduledExecutorService scheduledExecutorService;
    private ShouyeBean mmkvshouyeBean;

    private Handler.Callback mCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 10) {
                mmkvshouyeBean = MmkvUtils.getInstance().get_common_json("ceshi", ShouyeBean.class);
                if (mmkvshouyeBean == null) {
                    return true;
                }
                android.util.Log.e("onIndexSuccess", "onIndexSuccess: " + mmkvshouyeBean.getTvDynamicTitle());
                if (!TextUtils.isEmpty(mmkvshouyeBean.getTvDynamicTitle())) {
                    tvDynamicTitle.setText(Html.fromHtml(mmkvshouyeBean.getTvDynamicTitle()));//顶部标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlMyCurriculum())) {
                    tvJrkcTask.setText(Html.fromHtml(mmkvshouyeBean.getLlMyCurriculum()));//今日课程
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlAutonomyStudy())) {
                    tvShouyeZzxxTask.setText(Html.fromHtml(mmkvshouyeBean.getLlAutonomyStudy()));//自主学习标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlClassroomTeaching())) {
                    tvKtjxTask.setText(Html.fromHtml(mmkvshouyeBean.getLlClassroomTeaching()));//课堂教学标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getLlTask())) {
                    tvZuoyeTask.setText(Html.fromHtml(mmkvshouyeBean.getLlTask()));//作业标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlCexplore())) {
                    tvHztjTask.setText(Html.fromHtml(mmkvshouyeBean.getRlCexplore()));//合作探究标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlCorrecting())) {
                    tvPigiaTask.setText(Html.fromHtml(mmkvshouyeBean.getRlCorrecting()));//批改标题
                }
                if (!TextUtils.isEmpty(mmkvshouyeBean.getRlScienceAnalysis())) {
                    tvXqfxTask.setText(Html.fromHtml(mmkvshouyeBean.getRlScienceAnalysis()));//学期分析标题
                }
            }
            return true;
        }
    };


    private void set_update_progress_ui() {
        if (mHandler != null) {
            return;
        }
        if (scheduledExecutorService != null) {
            return;
        }
        mHandler = new WeakRefHandler(mCallback);
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    Message msg1 = Message.obtain();
                    msg1.what = 10;
                    mHandler.sendMessage(msg1);
                } catch (Exception e) {
                    Log.e("houjie", "scheduleWithFixedDelay:" + e.getMessage());
                }
            }
        }, 1, 50, TimeUnit.SECONDS);//SECONDS--秒);//MILLISECONDS--毫秒
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.iv_my_icon_curriculum) {//我的课程
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdkcActivity"));
            startActivity(intent);
        } else if (id == R.id.ll_autonomy_study) {//自主学习
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZzxxShouyeActivity"));
            startActivity(intent);
        } else if (id == R.id.ll_Classroom_teaching) {//课堂学习
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KtjxShouyeActivity"));
            startActivity(intent);
        } else if (id == R.id.ll_task) {//作业
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.ZyJiLuActivity"));
            startActivity(intent);
        } else if (id == R.id.rl_examination) {//考试
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.KaoShiActivity"));
            startActivity(intent);
        } else if (id == R.id.rl_scienceAnalysis) {//学情分析
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.XqfxActivity?show=" + "1"));
            startActivity(intent);
        } else if (id == R.id.rl_myClass) {//我的班级
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdbjActivity"));
            startActivity(intent);
        } else if (id == R.id.rl_correcting) {//批改
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.TeacherpgActivity"));
            startActivity(intent);
        } else if (id == R.id.rl_cexplore) {//
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.HeZuoActivity?show=" + "1"));
            startActivity(intent);
        } else if (id == R.id.ll_think_tank_school) {//智囊学堂
            if (AppUtils.isAppInstalled("com.sdzn.pkt.teacher.hd")) {
                String name = String.valueOf(com.sdzn.fzx.teacher.utils.SPUtils.get(App2.get(), com.sdzn.fzx.teacher.utils.SPUtils.LOGIN_USER_NUM, ""));
                String pwd = String.valueOf(com.sdzn.fzx.teacher.utils.SPUtils.get(App2.get(), com.sdzn.fzx.teacher.utils.SPUtils.LOGIN_PAS_CHECK, ""));
                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.teacher.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd));
                startActivity(intent);
            } else {
//                ToastUtils.showLong("跳转异常，请检查跳转配置、包名及Activity访问权限");
                ToastUtils.showLong("请先安装智囊学堂老师端APP");
            }
        } else if (id == R.id.ll_baidu_rncyclopedia) {//百度百科
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.jxkctitledetailsActivity");
//            startActivity(intent);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WebBaiduEncyclopediaActivity"));
            startActivity(intent);
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebBaiduEncyclopediaActivity");
//            intent.putExtra("url_key", "https://baike.baidu.com/vbaike#");
//            startActivity(intent);
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }

    @Override
    public void onIndexSuccess(ShouyeBean shouyeBean) {
        if (shouyeBean == null) {
            return;
        }
        android.util.Log.e("onIndexSuccess", "onIndexSuccess: " + shouyeBean.getTvDynamicTitle());
        if (!TextUtils.isEmpty(shouyeBean.getTvDynamicTitle())) {
            tvDynamicTitle.setText(Html.fromHtml(shouyeBean.getTvDynamicTitle()));//顶部标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlMyCurriculum())) {
            tvJrkcTask.setText(Html.fromHtml(shouyeBean.getLlMyCurriculum()));//今日课程
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlAutonomyStudy())) {
            tvShouyeZzxxTask.setText(Html.fromHtml(shouyeBean.getLlAutonomyStudy()));//自主学习标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlClassroomTeaching())) {
            tvKtjxTask.setText(Html.fromHtml(shouyeBean.getLlClassroomTeaching()));//课堂教学标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getLlTask())) {
            tvZuoyeTask.setText(Html.fromHtml(shouyeBean.getLlTask()));//作业标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlCexplore())) {
            tvHztjTask.setText(Html.fromHtml(shouyeBean.getRlCexplore()));//合作探究标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlCorrecting())) {
            tvPigiaTask.setText(Html.fromHtml(shouyeBean.getRlCorrecting()));//批改标题
        }
        if (!TextUtils.isEmpty(shouyeBean.getRlScienceAnalysis())) {
            tvXqfxTask.setText(Html.fromHtml(shouyeBean.getRlScienceAnalysis()));//学期分析标题
        }
    }

    @Override
    public void onIndexNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onIndexFail(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shouyePresenter.onDestory();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}
