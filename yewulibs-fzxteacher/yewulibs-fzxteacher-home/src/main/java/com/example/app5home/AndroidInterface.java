package com.example.app5home;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.ktjx.jxkc.JxkcDetailsActivity;
import com.example.app5home.pg.timupg.HpActivity;
import com.example.app5home.pg.timupg.StudentPGShouyeActivity;
import com.example.app5home.pg.timupg.StudentPgCompleteShouyeActivity;
import com.example.app5home.wdkc.kcfb.WdkcFaBuActivity;
import com.example.app5home.wdkc.kcjg.WdkcJieGuoActivity;
import com.example.app5home.wdkc.kcjl.WdkcJiLuActivity;
import com.example.app5home.wdkc.kcyl.WdkcYulanActivity;
import com.example.app5home.zy.zyfb.ZyFaBuActivity;
import com.example.app5home.zy.zyyl.ZyYuLanActivity;
import com.example.app5home.zzxx.bkjlyulan.bkjlyulanActivity;
import com.example.app5home.zzxx.fbjlyulan.fbjlyulanActivity;
import com.example.app5home.zzxx.zdtj.TmfxDetailsActivity;
import com.example.app5home.zzxx.zdtj.ZdtjShouyeActivity;
import com.google.gson.Gson;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.just.agentweb.AgentWeb;
import com.sdzn.fzx.teacher.vo.LoginBean;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidInterface {
    private Handler deliver = new Handler(Looper.getMainLooper());
    private AgentWeb agent;
    private Activity context;

    public AndroidInterface(AgentWeb agent, Activity context) {
        this.agent = agent;
        this.context = context;
    }
    /*-------------New开始----------------------*/

//    @JavascriptInterface
//    public void getTokenAndUserInfo(String userKey) {
//        ToastUtils.showLong(userKey);
//        Log.e("Tag", "读取到userKey : " + userKey);
//    }

    /**
     * 统一跳转二级界面方法
     */
    @JavascriptInterface
    public void JumpNewAndroidAct(final String url) {
//        ToastUtils.showLong(url);
        LogUtils.e("JumpNewAndroidAct",url);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcJieGuoActivity");
        intent.putExtra("url_key", url);
        context.startActivity(intent);
//        HiosHelper.resolveAd(context, WdkcJieGuoActivity.class, "hios://com.example.app5home.wdkc.kcjg.WdkcJieGuoActivity?url_key={s}" +  url);
    }

    /**
     * 统一方法
     */
    @JavascriptInterface
    public String activitySetting() {
        String ClassName = SPUtils.getInstance().getString("shouye_class_name", "0");
//        ToastUtils.showLong(ClassName);
        return ClassName;
    }

    /**
     * web端拿token方法
    */
    @JavascriptInterface
    public String activityToken() {
        String Token = SPUtils.getInstance().getString("token", "");
//        ToastUtils.showLong(Token);
        return Token;
    }

    /**
     * web端拿用户实体方法
     */
    @JavascriptInterface
    public String activityUserBean() {
        LoginBean data = com.sdzn.fzx.teacher.utils.SPUtils.getLoginBean();
        final String loginJsonStr = new Gson().toJson(data);
        ToastUtils.showLong(loginJsonStr);
        return loginJsonStr;
    }

    /*-------------New结束----------------------*/


    @JavascriptInterface
    public void jxkcdetails(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.jxkctitledetailsActivity");
        context.startActivity(intent);

//        HiosHelper.resolveAd(context, context, "hios://com.exam.student.applinks.dynamirecyclerview.ShouyeActivity?show={i}" + 0);
    }

    @JavascriptInterface
    public void BackToAndroid(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
        intent.putExtra("show", 0);
        context.startActivity(intent);

//        HiosHelper.resolveAd(context, context, "hios://com.exam.student.applinks.dynamirecyclerview.ShouyeActivity?show={i}" + 0);
    }

    /**
     * 我的课程 跳转到 课程发布
     */

    @JavascriptInterface
    public void BackToAndroid1(final String str) {
//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcFaBuActivity");
//        intent.putExtra("show", str);
//        context.startActivity(intent);
        HiosHelper.resolveAd(context, WdkcFaBuActivity.class, "hios://com.example.app5home.wdkc.kcfb.WdkcFaBuActivity?show={s}" + str);
    }

    /**
     * 我的课程 跳转到 课程预览
     */
    @JavascriptInterface
    public void BackToAndroid2(final String str) {
//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcYulanActivity");
//        intent.putExtra("show", str);
//        context.startActivity(intent);

        HiosHelper.resolveAd(context, WdkcYulanActivity.class, "hios://com.example.app5home.wdkc.kcyl.WdkcYulanActivity?show={s}" + str);
    }

    /**
     * 课程发布 跳转到  课程记录
     */
    @JavascriptInterface
    public void BackToAndroid3(final String str) {
//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcJiLuActivity");
//        intent.putExtra("show", str);
//        context.startActivity(intent);
        HiosHelper.resolveAd(context, WdkcJiLuActivity.class, "hios://com.example.app5home.wdkc.kcjl.WdkcJiLuActivity?show={s}" + str);

    }

    /**
     * 课程发布 跳转到  课程结果
     */
    @JavascriptInterface
    public void BackToAndroid4(final String str) {

        HiosHelper.resolveAd(context, WdkcJieGuoActivity.class, "hios://com.example.app5home.wdkc.kcjg.WdkcJieGuoActivity?show={s}" + str);

//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcJieGuoActivity");
//        intent.putExtra("show", str);
//        context.startActivity(intent);
    }

    /**
     * 作业记录 跳转到  预览
     */
    @JavascriptInterface
    public void BackToAndroidZY1(final String str) {//需要把类型show 传入

        HiosHelper.resolveAd(context, ZyYuLanActivity.class, "hios://com.example.app5home.zy.zyyl.ZyYuLanActivity?show={s}" + str);
    }

    /**
     * 跳转到  发布页面  1仅资料  2含试题
     */
    @JavascriptInterface
    public void BackToAndroidZY2(final String str) {

        HiosHelper.resolveAd(context, ZyFaBuActivity.class, "hios://com.example.app5home.zy.zyfb.ZyFaBuActivity?show={s}" + str);

    }

    /**
     * 作业发布记录 跳转到  跳转到统计详情
     */
    @JavascriptInterface
    public void BackToAndroidZY3(final String str) {
        ToastUtils.showShort("发布记录----跳转到统计详情");
    }

    /*-------------自主学习跳转开始-----------*/

    /**
     * 自主学习预览
     */
    @JavascriptInterface
    public void BackToAndroidzzxx1(final String str) {
        HiosHelper.resolveAd(context, bkjlyulanActivity.class, "hios://com.example.app5home.zzxx.bkjlyulan.bkjlyulanActivity?show={s}" + str);
    }

    /**
     * 自主学习作答统计
     */
    @JavascriptInterface
    public void BackToAndroidzzxxzdtj(final String str) {
        HiosHelper.resolveAd(context, ZdtjShouyeActivity.class, "hios://com.example.app5home.zzxx.zdtj.ZdtjShouyeActivity?show={s}" + str);
    }

    /**
     * 自主学习发布记录预览详情
     */
    @JavascriptInterface
    public void BackToAndroidzzxxfbjlxiangq(final String str) {
        HiosHelper.resolveAd(context, fbjlyulanActivity.class, "hios://com.example.app5home.zzxx.fbjlyulan.fbjlyulanActivity?show={s}" + str);
    }

    /**
     * 题目分析详情
     */
    @JavascriptInterface
    public void TmfxDetails(final String str) {
        HiosHelper.resolveAd(context, TmfxDetailsActivity.class, "hios://com.example.app5home.zzxx.zdtj.TmfxDetailsActivity?show={s}" + str + "&url_key={s}" + "https://www.baidu.com/");
    }

    /*-------------自主学习跳转结束-----------*/

    /**
     * 考试管理到 预览页
     */
    @JavascriptInterface
    public void BackToAndroidKS1(final String str) {
        HiosHelper.resolveAd(context, fbjlyulanActivity.class, "hios://com.example.app5home.ks.ksyl.KaoShiYuLanActivity?show={s}" + str);
    }

    /**
     * 考试管理到 发布页
     */
    @JavascriptInterface
    public void BackToAndroidKS2(final String str) {
        HiosHelper.resolveAd(context, fbjlyulanActivity.class, "hios://com.example.app5home.ks.ksfb.KaoShiFaBuActivity?show={s}" + str);
    }


    /**
     * 合作探究到   内容
     */
    @JavascriptInterface
    public void BackToAndroidHZ1(final String str) {
        HiosHelper.resolveAd(context, fbjlyulanActivity.class, "hios://com.example.app5home.hztj.hzxq.HeZuoXiangQingActivity?show={s}" + str);
    }

    /**
     * 合作探究到   探究内容详情
     */
    @JavascriptInterface
    public void BackToAndroidHZ2(final String str) {
        HiosHelper.resolveAd(context, fbjlyulanActivity.class, "hios://com.example.app5home.hztj.hzrwxq.HeZuoRenWuXQActivity?show={s}" + str);
    }


    /*---------------教学课程开始---------------------*/

    /**
     * 教学课程预览
     */
    @JavascriptInterface
    public void jxkcyulan(final String str) {
//        HiosHelper.resolveAd(context, jxkcyulanActivity.class, "hios://com.example.app5home.ktjx.jxkc.jxkcyulanActivity?show={s}" + str);
        HiosHelper.resolveAd(context, JxkcDetailsActivity.class, "hios://com.example.app5home.ktjx.jxkc.JxkcDetailsActivity?show={s}" + str + "&url_key={s}" + "https://www.baidu.com/");
    }

    /**
     * 课堂检测统计
     */
    @JavascriptInterface
    public void ktcjtj(final String str) {
        HiosHelper.resolveAd(context, ZdtjShouyeActivity.class, "hios://com.example.app5home.ktjx.ktjc.ktjctj.ZdtjShouyeActivity?show={s}" + str);
    }

    /*---------------教学课程结束---------------------*/

    /*---------------批改开始---------------------*/

    /**
     * 设置互批
     */
    @JavascriptInterface
    public void pghpfangshi(final String str) {
        HiosHelper.resolveAd(context, HpActivity.class, "hios://com.example.app5home.pg.HpActivity?show={s}" + str + "&url_key={s}" + "https://www.baidu.com/");
    }

    /**
     * 批改方式/学生批改，按题批改
     */
    @JavascriptInterface
    public void xspg(final String str) {
        HiosHelper.resolveAd(context, StudentPGShouyeActivity.class, "hios://com.example.app5home.pg.StudentPGShouyeActivity?show={s}" + str + "&url_key={s}" + "https://www.baidu.com/");
    }

    /**
     * 批改方式跳转详情/学生批改，按题批改
     */
    @JavascriptInterface
    public void pigaicomplete(final String str) {
        HiosHelper.resolveAd(context, StudentPgCompleteShouyeActivity.class, "hios://com.example.app5home.pg.StudentPgCompleteShouyeActivity?show={s}" + str + "&url_key={s}" + "https://www.baidu.com/");
    }




    /*---------------批改结束---------------------*/

    @JavascriptInterface
    public String callAndroid() {
        deliver.post(new Runnable() {
            @Override
            public void run() {
                Log.i("Info", "main Thread:" + Thread.currentThread());
//                Toast.makeText(context.getApplicationContext(), "" + msg, Toast.LENGTH_LONG).show();
            }
        });

        Log.i("Info", "Thread:" + Thread.currentThread());
        return "进入此方法";
    }

}
