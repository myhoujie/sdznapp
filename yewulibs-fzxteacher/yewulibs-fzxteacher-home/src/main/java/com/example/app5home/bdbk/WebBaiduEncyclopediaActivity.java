package com.example.app5home.bdbk;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.example.app5libbase.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;


public class WebBaiduEncyclopediaActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webbaidubaike;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(13);
        setBaseOnClickListener(this);
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }


    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "百度百科");
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        mAgentWeb.getWebCreator().getWebView().loadUrl("https://baike.baidu.com/vbaike#");

    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
