package com.example.app5home.ktjx.details;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter1;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActNoWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.just.agentweb.App2;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.interfaces.XPopupCallback;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.HCategoryBean;
import com.sdzn.fzx.teacher.bean.HCategoryBean1;
import com.sdzn.fzx.teacher.bean.OneBean2;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/*
 * 课堂教学详情
 * */
public class jxkctitledetailsActivity extends BaseActNoWebActivity1 implements BaseOnClickListener {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter1 mAdapter11;
    private List<OneBean2> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    private MessageReceiverIndex mMessageReceiver;
    private int defCurrentItem = 0;//设置默认见面

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("jxkctitledetailsActivity".equals(intent.getAction())) {
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("jxkctitledetailsActivity");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }

    private String aaa = "";

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
//        if ("1".equals(getIntent().getStringExtra("show"))) {
//            defCurrentItem = 0;
//        } else if ("2".equals(getIntent().getStringExtra("show"))) {
//            defCurrentItem = 1;
//        }
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        initviw();
    }

    List<OneBean2> oneBean2List;

    private void initviw() {
        tvTitleName.setText("课程预览");
        oneBean2List = new ArrayList<>();
        try {
            aaa = URLDecoder.decode(getIntent().getStringExtra("title"),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        List<OneBean2> oneBean2List = getGrades(aaa);
        oneBean2List = getGrades1();
        onclick();
        donetworkdemo(oneBean2List);
    }

    private void donetworkdemo(List<OneBean2> bean2List) {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        for (int i = 0; i < bean2List.size(); i++) {
            mDataTablayout1.add(new HCategoryBean1(String.valueOf(i), bean2List.get(i).getDesc(), bean2List.get(i).getUrl(), bean2List.get(i).getList()));
        }
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean2(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true, hCategoryBean.getList().get(i).getUrl(), hCategoryBean.getList().get(i).getList()));

            } else {
                mDataTablayout.add(new OneBean2(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false, hCategoryBean.getList().get(i).getUrl(), hCategoryBean.getList().get(i).getList()));
            }

        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter1();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                OneBean2 bean1 = (OneBean2) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
        mAdapter11.seetTablayoutdapter1Listener(new Tablayoutdapter1.ViewCallBack() {
            @Override
            public void viewOnClick(View view, TextView tv, int position) {
                popxuanxiang(tv, position);
            }
        });
    }


    public static final String URL_KEY = "url_key";

    private void init_viewp(List<OneBean2> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
//                bundle.putString(URL_KEY, "file:///android_asse t/js_ktjx/jxkc.html");
                kcFragment1 fragment1 = FragmentHelper.newFragment(kcFragment1.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                kcFragment2 fragment1 = FragmentHelper.newFragment(kcFragment2.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 2) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                kcFragment3 fragment1 = FragmentHelper.newFragment(kcFragment3.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 3) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                kcFragment4 fragment1 = FragmentHelper.newFragment(kcFragment4.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());
                kcFragment5 fragment2 = FragmentHelper.newFragment(kcFragment5.class, bundle);
                mFragmentList.add(fragment2);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(5);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        OneBean2 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }


    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean2 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean2 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面

            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    private int checkedPos = 0;//默认选中 年级

    @Override
    public void Titletijiao() {
//        popxuanxiang();
    }


    @Override
    public void Titlegrzx() {
        CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");
            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }

    public OneBean2 metrics;
    public List<OneBean2> metrics111;

    public List<OneBean2> getGrades1() {
        metrics111 = new ArrayList<>();
        metrics = new OneBean2();
        try {
            InputStream open = this.getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
//                String json = getTitle1();
//                String json = URLDecoder.decode(aaaa, "UTF-8");
                Gson gson = new GsonBuilder().create();
                JsonParser jsonParser = new JsonParser();
                JsonArray array = jsonParser.parse(json).getAsJsonArray();
                for (JsonElement element : array) {
                    metrics = gson.fromJson(element, OneBean2.class);
                    //do something with the metric object. probably read all the datapoints and display
                    if (metrics.getList() == null) {
                        metrics.setList(new ArrayList<OneBean2.ListBean>());
                    }
                    if (TextUtils.isEmpty(metrics.getUrl())) {
                        metrics.setUrl("");
                    }
                    metrics111.add(metrics);
                }

//                List<GradeJson> lists = new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
//                Log.e("aaatest", String.valueOf(lists));
//                return new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return metrics111;
    }


    public List<OneBean2> getGrades(String aaaa) {
        metrics111 = new ArrayList<>();
        metrics = new OneBean2();
        try {
            InputStream open = this.getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
//                String json = new String(buffer, "utf-8");
//                String json = getTitle1();
                String json = URLDecoder.decode(aaaa, "UTF-8");
                Gson gson = new GsonBuilder().create();
                JsonParser jsonParser = new JsonParser();
                JsonArray array = jsonParser.parse(json).getAsJsonArray();
                for (JsonElement element : array) {
                    metrics = gson.fromJson(element, OneBean2.class);
                    //do something with the metric object. probably read all the datapoints and display
                    if (metrics.getList() == null) {
                        metrics.setList(new ArrayList<OneBean2.ListBean>());
                    }
                    if (TextUtils.isEmpty(metrics.getUrl())) {
                        metrics.setUrl("");
                    }
                    metrics111.add(metrics);
                }

//                List<GradeJson> lists = new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
//                Log.e("aaatest", String.valueOf(lists));
//                return new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return metrics111;
    }

    private void popxuanxiang(final TextView atView, final int pos) {
        List<String> stringList = new ArrayList<>();
        if (oneBean2List.get(pos).getList().size() != 0) {
            for (int i = 0; i < oneBean2List.get(pos).getList().size(); i++) {
                stringList.add(oneBean2List.get(pos).getList().get(i).getDesc());
            }
        }
        String[] title1 = stringList.toArray(new String[stringList.size()]);
        new XPopup.Builder(jxkctitledetailsActivity.this)
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .setPopupCallback(new XPopupCallback() {
                    @Override
                    public void onCreated(BasePopupView popupView) {

                    }

                    @Override
                    public void beforeShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {

                    }

                    @Override
                    public void beforeDismiss(BasePopupView popupView) {

                    }

                    @Override
                    public boolean onBackPressed(BasePopupView popupView) {
                        return false;
                    }
                })
                .offsetY(30)
                .offsetX(-60)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                .atView(atView)// 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList1(title1,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
                                atView.setText(text);
                                String aaaaaaa = oneBean2List.get(pos).getList().get(position).getUrl();
                                Intent msgIntent = new Intent();
                                msgIntent.putExtra("url_key", aaaaaaa);
                                msgIntent.setAction("kcFragment1");
                                com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
                            }
                        }, com.example.app5libbase.R.layout.popup_adapter_rv, com.example.app5libbase.R.layout.popup_adapter_item, checkedPos)
                .show();
    }

}