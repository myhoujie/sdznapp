package com.example.app5home.wdbj.bj;

import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sdzn.fzx.student.libutils.app.App2;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GradeJsonutils {

    public GradeJson metrics;
    public List<GradeJson> metrics111;

    public List<GradeJson> getGrades(String aa) {
        metrics111 = new ArrayList<>();
        metrics = new GradeJson();
        try {
            InputStream open = App2.get().getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
//                String json = new String(buffer, "utf-8");
//                String json = getTitle1();
                String json = aa;
                //
                Gson gson = new GsonBuilder().create();
                JsonParser jsonParser = new JsonParser();
                JsonArray array = jsonParser.parse(json).getAsJsonArray();
                for (JsonElement element : array) {
                    metrics = gson.fromJson(element, GradeJson.class);
                    //do something with the metric object. probably read all the datapoints and display
                    if (metrics.getList() == null) {
                        metrics.setList(new ArrayList<GradeJson.ListBean>());
                    }
                    if (TextUtils.isEmpty(metrics.getUrl())) {
                        metrics.setUrl("");
                    }
                    metrics111.add(metrics);
                }

//                List<GradeJson> lists = new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
//                Log.e("aaatest", String.valueOf(lists));
//                return new Gson().fromJson(json, new TypeToken<GradeJson>() {
//                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return metrics111;
    }
}
