package com.example.app5home.xqfx;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.SPUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.R;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter;
import com.example.app5home.xqfx.xq.XqfxNewItem1;
import com.example.app5home.xqfx.xq.XqfxNewItem3;
import com.example.app5home.xqfx.xq.XqfxNewItem4;
import com.example.app5home.xqfx.xq.XqfxNewItem5;
import com.example.app5home.zy.ZyJiLuNewItem2;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.newbase.BaseActNoWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.app5libbase.views.ViewPagerSlide;
import com.just.agentweb.LocalBroadcastManagers;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.OneBean1;
import com.sdzn.fzx.teacher.bean.RecyclerTabBean;
import com.sdzn.fzx.teacher.presenter.TabPresenter;
import com.sdzn.fzx.teacher.view.TabViews;

import java.util.ArrayList;
import java.util.List;

public class XqfxActivity extends BaseActNoWebActivity1 implements BaseOnClickListener, TabViews {
    private ViewPagerSlide viewpagerBaseactTablayout;

    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    private MessageReceiverIndex mMessageReceiver;
    private int defCurrentItem = 0;//设置默认见面
    public static final String URL_KEY = "url_key";
    TabPresenter tabPresenter;
    private String type = "7";//查询

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("XqfxActivity".equals(intent.getAction())) {
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_tablayout;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("XqfxActivity");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
        tabPresenter = new TabPresenter();
        tabPresenter.onCreate(this);
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        if ("1".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 0;
        } else if ("2".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 1;
        }
        TitleShowHideState(3);
        setBaseOnClickListener(this);
        tvTitleName.setText("学情分析");
        onclick();
        tabPresenter.querytab(type);
    }

    private void setData(List<RecyclerTabBean.ListBean> recyclerTabBeans) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < recyclerTabBeans.size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), SPUtils.getInstance().getString("url", "http://49.4.7.45:8080/#/") + recyclerTabBeans.get(i).getUrl(), true));
            } else {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), SPUtils.getInstance().getString("url", "http://49.4.7.45:8080/#/") + recyclerTabBeans.get(i).getUrl(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
//        Log.e("---geekyun----", current_id);
    }

    private void onclick() {
        //
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
//        JackSnapHelper mLinearSnapHelper = new JackSnapHelper(JackSnapHelper.TYPE_SNAP_START);
//        mLinearSnapHelper.attachToRecyclerView(recyclerView1Order11);
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }


    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());//"analysisLearning/home?step=0");
                XqfxNewItem1 fragment1 = FragmentHelper.newFragment(XqfxNewItem1.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 1) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());//"analysisLearning/promotion?step=1");
                ZyJiLuNewItem2 fragment2 = FragmentHelper.newFragment(ZyJiLuNewItem2.class, bundle);
                mFragmentList.add(fragment2);
            } else if (i == 2) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());//"analysisLearning/stage?step=2");
                XqfxNewItem3 fragment3 = FragmentHelper.newFragment(XqfxNewItem3.class, bundle);
                mFragmentList.add(fragment3);
            } else if (i == 3) {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());//"analysisLearning/examinationReport?step=3");
                XqfxNewItem4 fragment4 = FragmentHelper.newFragment(XqfxNewItem4.class, bundle);
                mFragmentList.add(fragment4);
            } else {
                bundle.putString(URL_KEY, mlist.get(i).getUrl());//"analysisLearning/curriculumStage/index?step=4");
                XqfxNewItem5 fragment5 = FragmentHelper.newFragment(XqfxNewItem5.class, bundle);
                mFragmentList.add(fragment5);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        tabPresenter.onDestory();
        super.onDestroy();
    }

    @Override
    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
        setData(recyclerTabBean.getList());
    }

    @Override
    public void onTabNodata(String msg) {

    }

    @Override
    public void onTabFail(String msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }
}

