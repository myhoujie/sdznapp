package com.example.app5home.hztj.hzcj;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.example.baselibrary.emptyview.EmptyView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

public class HeZuoCreateActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return com.example.app5libbase.R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(5);
        setBaseOnClickListener(this);
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }


    @Nullable
    @Override
    public String getUrl() {
//        return super.getUrl();
        return "file:///android_asset/js_interaction/hezuocj.html";
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}