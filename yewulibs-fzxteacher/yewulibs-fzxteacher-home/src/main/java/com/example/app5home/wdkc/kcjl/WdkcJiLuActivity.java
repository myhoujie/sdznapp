package com.example.app5home.wdkc.kcjl;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

/**
 * 我的课程发布记录
 */
public class WdkcJiLuActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(4);
        setBaseOnClickListener(this);
        tvTitleName.setText("课程发布记录");
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }

    @Override
    public void Titleshijian() {
        showCalendarDialog();
        BaseActWebActivity1.setOnDisplayRefreshListener(new BaseActWebActivity1.refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    /*展开时间*/
    @Override
    public void Titlezankaishijian() {
        showCalendarDialog();
        BaseActWebActivity1.setOnDisplayRefreshListener(new BaseActWebActivity1.refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });

    }

    @Nullable
    @Override
    public String getUrl() {
//        return super.getUrl();
        return "file:///android_asset/js_interaction/goback.html";
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        ToastUtils.showLong("数据已更新");
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
