package com.example.app5home.hztj.hzrwxq;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

public class HeZuoRenWuXQActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return com.example.app5libbase.R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(5);
        setBaseOnClickListener(this);
    }


    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }


    @Nullable
    @Override
    public String getUrl() {
//        return super.getUrl();
        return "file:///android_asset/js_interaction/hezuorwxq.html";
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}