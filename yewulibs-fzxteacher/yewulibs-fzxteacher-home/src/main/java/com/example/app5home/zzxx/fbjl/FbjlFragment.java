package com.example.app5home.zzxx.fbjl;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.AndroidInterface;
import com.example.app5home.R;
import com.example.app5libbase.newbase.BaseActFragment1;
import com.example.baselibrary.emptyview.EmptyView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

/*
 * 发布记录首页*/
public class FbjlFragment extends BaseActFragment1 implements CheckverionViews {
    CheckverionFzxPresenter checkverionFzxPresenter;
    protected SmartRefreshLayout refreshLayout1;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_fbjl;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        refreshLayout1 = rootView.findViewById(com.example.app5home.R.id.refreshLayout1_order);
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                checkverionFzxPresenter.checkVerion("3", "0");
            }
        });
//使上拉加载具有弹性效果
        refreshLayout1.setEnableAutoLoadMore(false);
        //禁止越界拖动（1.0.4以上版本）
        refreshLayout1.setEnableOverScrollDrag(false);
        //关闭越界回弹功能
        refreshLayout1.setEnableOverScrollBounce(false);
        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
            @Override
            public boolean canRefresh(View content) {
                //webview滚动到顶部才可以下拉刷新
                MyLogUtil.e("ssssss",""+mAgentWeb.getWebCreator().getWebView().getScrollY());
                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
            }

            @Override
            public boolean canLoadMore(View content) {
                return false;
            }

        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                checkverionFzxPresenter.checkVerion("3", "0");
            }
        });
    }

    @Nullable
    @Override
    protected void getJsInterface() {
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, getActivity()));
        }
        super.getJsInterface();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        LogUtils.e("BaseActFragment------getAgentWebParent");
        return (ViewGroup) getActivity().findViewById(R.id.ll_base_containe_fbjl);
    }

    @Nullable
    protected String getUrl() {
        Bundle bundle = this.getArguments();
        String target = bundle.getString("url_key");
        LogUtils.e("targetaaaaaaa=" + target);
        if (TextUtils.isEmpty(target)) {
            target = "http://www.jd.com/";
        }
        return target;
    }


    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
        //
//        set_re_data();
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        ToastUtils.showLong("数据已更新");
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
//        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
