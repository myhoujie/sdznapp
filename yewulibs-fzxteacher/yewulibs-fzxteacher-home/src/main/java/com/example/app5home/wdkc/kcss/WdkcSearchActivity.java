package com.example.app5home.wdkc.kcss;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

//import com.example.app4pkt.R;
//import com.example.app4pkt.adapter.SearchHistoryAdapter;
//import com.example.app4public.manager.SPManager;
//import com.example.app4public.manager.constant.CourseCons;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.R;
import com.example.app5libbase.views.search.ClearEditText;
import com.example.app5libbase.views.search.KeyboardUtils;
import com.example.app5libbase.views.search.flowlayout.FlowLayout;
import com.example.app5libbase.views.search.flowlayout.TagFlowLayout;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.utils.SPUtils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 描述：
 * - 课程搜索（包含主页、直播、点播课程搜搜）
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WdkcSearchActivity extends AppCompatActivity implements View.OnClickListener {

    ClearEditText etSearch;
    TagFlowLayout tagSearchHistory;
    RelativeLayout rlHistory;
    ImageView ivClearHistory;
    TextView tvSearch;
    TextView tvSearchHistory;
    FrameLayout viewFrame;
    //    TextView tvCancel;
    private SearchListView listViewMohu;

    private List<String> searchHistories;
    private SearchHistoryAdapter<String> searchHistoryAdapter;

    private int courseType;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    public static final String IS_SEARCH_TEACHER = "isSearchTeacher";

    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        setContentView(R.layout.activity_search);
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入SearchActivity成功");
                }
            }
        }
        etSearch = (ClearEditText) findViewById(R.id.et_search);
        rlHistory = (RelativeLayout) findViewById(R.id.Rl_history);
        tagSearchHistory = (TagFlowLayout) findViewById(R.id.tag_search_history);
        ivClearHistory = (ImageView) findViewById(R.id.iv_clear_history);
        tvSearch = (TextView) findViewById(R.id.tv_search);
        tvSearchHistory = (TextView) findViewById(R.id.tv_search_history);
        textView = (TextView) findViewById(R.id.tv);
        //模糊列表
        listViewMohu = (SearchListView) findViewById(R.id.listView_mohu);
        viewFrame = (FrameLayout) findViewById(R.id.view_frame);
        tvSearch.setOnClickListener(this);
//        tvCancel = (TextView) findViewById(R.id.tv_cancel);
//        tvCancel.setOnClickListener(this);
        ivClearHistory.setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
            }
        });
        initData();
        initView();
    }


    private void initData() {
        searchHistories = new ArrayList<>();
//        searchHistories.addAll(isSearchTeacher ? SPManager.getSearchTeacherStr() : SPUtils.getSearchStr());
        searchHistories.addAll(SPUtils.getSearchStr());
        if (searchHistories.isEmpty()) {
            rlHistory.setVisibility(View.INVISIBLE);
        } else {
            rlHistory.setVisibility(View.VISIBLE);
        }
        initHistory();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initView() {
        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
        etSearch.requestFocusFromTouch();
        searchHistoryAdapter = new SearchHistoryAdapter<String>(this, searchHistories);
        tagSearchHistory.setAdapter(searchHistoryAdapter);
        tagSearchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                search(searchHistories.get(position));
                return true;
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        String searchStr = etSearch.getText().toString().trim();
                        search(searchStr);
                        KeyboardUtils.hideSoftInput(WdkcSearchActivity.this);
                    }
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnFocusChangedListener(new ClearEditText.onFocusChangedListener() {
            @Override
            public void onFocusChanged(View v, boolean hasFocus) {
                if (etSearch == v) {
                    if (hasFocus) {
                        KeyboardUtils.showSoftInput(WdkcSearchActivity.this, etSearch);
                    } else {
                        KeyboardUtils.hideSoftInput(WdkcSearchActivity.this);
                    }
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyboardUtils.showSoftInput(WdkcSearchActivity.this, etSearch);
            }
        }, 200);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // 输入文本后调用该方法
            @Override
            public void afterTextChanged(Editable s) {
                rlHistory.setVisibility(View.VISIBLE);
                if (s.toString().isEmpty()) {
                    listViewMohu.setVisibility(View.GONE);
                    initHistory();
                } else {

                    List<String> list = new ArrayList<>();
                    list.add("我");
                    list.add("我的");
                    list.add("123");
                    list.add("234");
                    list.add("3456789");
                    list.add("45678");
                    //网络请求后
                    setMohuList(list);
                    listViewMohu.setVisibility(View.VISIBLE);
                }
                setViewFrame();
            }
        });

        viewFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listViewMohu.setVisibility(View.GONE);
                viewFrame.setVisibility(View.GONE);
            }
        });
        listViewMohu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listViewMohu.setVisibility(View.GONE);
                viewFrame.setVisibility(View.GONE);

                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                String name = textView.getText().toString();
                etSearch.setText(name);//下一步默认进行模糊搜索
                search(name);

            }
        });

    }

    private void initHistory(){
        if (SPUtils.getSearchStr().size() > 0) {
            tvSearchHistory.setVisibility(View.VISIBLE);
            ivClearHistory.setVisibility(View.VISIBLE);
        } else {
            tvSearchHistory.setVisibility(View.INVISIBLE);
            ivClearHistory.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 半透明
     */
    private void setViewFrame() {
        if (View.VISIBLE == listViewMohu.getVisibility()) {
            viewFrame.setVisibility(View.VISIBLE);
        } else {
            viewFrame.setVisibility(View.GONE);
        }
    }

    private void showClearDialot() {
//        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
//        builder.setMessage("确定删除全部历史记录？")
//                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
//                    @Override
//                    public void onClick(Dialog dialog, int which) {
//                        clearHistory();
//                    }
//                }).setCancelable(true).show();
        clearHistory();
    }

    private void search(String searchStr) {
        KeyboardUtils.hideSoftInput(this);
        etSearch.setText(searchStr);
        rlHistory.setVisibility(View.GONE);
        saveSearchStr(searchStr);
        searchHistoryAdapter.notifyDataChanged();

        textView.setText(searchStr);
        ToastUtils.showShort("searchStr--" + searchStr);
    }

    private void saveSearchStr(String searchStr) {
        if (searchStr.isEmpty()) {
            return;
        }
        if (searchHistories.contains(searchStr)) {
            searchHistories.remove(searchStr);
        }
        searchHistories.add(0, searchStr);

        if (searchHistories.size() > 20) {
            for (int i = searchHistories.size() - 1; i > 19; i--) {
                searchHistories.remove(i);
            }
        }

        SPUtils.saveSearchStr(searchHistories);


    }

    private void clearHistory() {
        rlHistory.setVisibility(View.INVISIBLE);
        searchHistories.clear();
        SPUtils.saveSearchStr(searchHistories);
        searchHistoryAdapter.notifyDataChanged();
    }

    @Override
    public void onClick(View view) {
//        if (R.id.tv_cancel == view.getId()) {
//            KeyboardUtils.hideSoftInput(this);
//            onBackPressed();
//        } else
        if (R.id.iv_clear_history == view.getId()) {
            showClearDialot();

        } else if (R.id.tv_search == view.getId()) {
            String searchStr = etSearch.getText().toString().trim();
            search(searchStr);
        }
    }


    List<String> mhList = new ArrayList<>();
    private SimpleAdapter simpleAdapter;

    private void setMohuList(List<String> list) {
        if (mhList != null) {
            mhList.clear();
            mhList.addAll(list);

            List<Map<String, Object>> listitem = new ArrayList<Map<String, Object>>();
            for (int i = 0; i < mhList.size(); i++) {
                Map<String, Object> showitem = new HashMap<String, Object>();
                showitem.put("name", mhList.get(i));
                listitem.add(showitem);
            }


            /**
             * 模糊... 实例化
             */
            simpleAdapter = new SimpleAdapter(this, listitem, android.R.layout.simple_list_item_1, new String[]{"name"},
                    new int[]{android.R.id.text1});
            // 3. 设置适配器
            listViewMohu.setAdapter(simpleAdapter);

        }

    }
}
