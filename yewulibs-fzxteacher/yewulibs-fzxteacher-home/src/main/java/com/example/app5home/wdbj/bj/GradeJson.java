package com.example.app5home.wdbj.bj;

import java.io.Serializable;
import java.util.List;

public class GradeJson implements Serializable {

    /**
     * desc : 自主学习统计
     * list : [{"desc":"作答统计","url":"/courseTeach/workbench/studyStatistics"},{"desc":"题目分析","url":"/courseTeach/workbench/topicAnalysis"},{"desc":"资料分析","url":"/courseTeach/workbench/materialAnalysis"}]
     * url : /courseTeach/workbench/typicalCase
     */

    private String desc;
    private String url;
    private List<ListBean> list;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * desc : 作答统计
         * url : /courseTeach/workbench/studyStatistics
         */

        private String desc;
        private String url;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}

