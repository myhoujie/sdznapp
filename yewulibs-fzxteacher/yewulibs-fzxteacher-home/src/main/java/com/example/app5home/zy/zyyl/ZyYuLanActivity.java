package com.example.app5home.zy.zyyl;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5home.R;
import com.example.app5home.zy.zyfb.ZyFaBuActivity;
import com.example.app5libbase.newbase.BaseActWebActivity1;
import com.example.app5libbase.newbase.BaseOnClickListener;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

public class ZyYuLanActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;
    private String strShow = "1";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setThisStatusBarColor(this, com.example.app5libbase.R.drawable.bg_title_jianbian);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(7);
        setBaseOnClickListener(this);
        strShow = getIntent().getStringExtra("show");
    }

    @Override
    protected void RrefreshLoad() {
        super.RrefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }

    @Override
    public void onResume() {
        super.onResume();
        tvTitleName.setText("作业预览");
    }

    @Nullable
    @Override
    public String getUrl() {
//        return super.getUrl();
        return "file:///android_asset/js_interaction/goback.html";
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void Titletijiao() {
        super.Titletijiao();
        HiosHelper.resolveAd(this, ZyFaBuActivity.class, "hios://com.example.app5home.zy.zyfb.ZyFaBuActivity?show={s}" + strShow);
    }
}
