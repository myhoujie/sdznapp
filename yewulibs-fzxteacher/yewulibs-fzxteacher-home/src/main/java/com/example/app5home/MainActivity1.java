package com.example.app5home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5home.adapter.OrderFragmentPagerAdapter;
import com.example.app5home.adapter.Tablayoutdapter;
import com.example.app5home.renwu.RenwuFragment;
import com.example.app5home.shouye.ShouyeFragment;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.FragmentHelper;
import com.example.app5libbase.views.ViewPagerSlide;
import com.example.app5libbase.views.XRecyclerView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.sdzn.fzx.teacher.bean.HCategoryBean;
import com.sdzn.fzx.teacher.bean.HCategoryBean1;
import com.sdzn.fzx.teacher.bean.OneBean1;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;

import java.util.ArrayList;
import java.util.List;

public class MainActivity1 extends BaseActivity implements View.OnClickListener, CheckverionViews {
    private XRecyclerView recyclerView1Order11;
    private ViewPagerSlide viewpagerMy1Orderf2;

    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;


    private LinearLayout llUserInformation;//个人信息
    private ImageView ivSetup;//设置
    private LinearLayout llSubject;//选择学科
    private TextView tvSubject;
    CheckverionFzxPresenter checkverionFzxPresenter;
    private int checkedPos = 2;//默认学科
    String morenStr = "物理";//实际项目应去掉

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        recyclerView1Order11 = (XRecyclerView) findViewById(R.id.recycler_view1_order11);
        viewpagerMy1Orderf2 = (ViewPagerSlide) findViewById(R.id.viewpager_my1_orderf2);

        tvSubject = (TextView) findViewById(R.id.tv_subject);
        llUserInformation = (LinearLayout) findViewById(R.id.ll_user_information);
        ivSetup = (ImageView) findViewById(R.id.iv_setup);
        llSubject = (LinearLayout) findViewById(R.id.ll_subject);
        llUserInformation.setOnClickListener(this);
        ivSetup.setOnClickListener(this);
        llSubject.setOnClickListener(this);
        onclick();
        donetwork();
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        checkverionFzxPresenter.checkVerion("2", "0");
        tvSubject.setText(morenStr);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    private void donetwork() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "首页", R.drawable.new_icon_home));
        mDataTablayout1.add(new HCategoryBean1("2", "任务", R.drawable.new_icon_renwu));
        hCategoryBean.setList(mDataTablayout1);
        //
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true, hCategoryBean.getList().get(i).getIcon()));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false, hCategoryBean.getList().get(i).getIcon()));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        recyclerView1Order11.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerView1Order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerMy1Orderf2.setCurrentItem(position, true);
            }
        });
    }

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                ShouyeFragment fragment1 = FragmentHelper.newFragment(ShouyeFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                RenwuFragment fragment1 = FragmentHelper.newFragment(RenwuFragment.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerMy1Orderf2.setAdapter(orderFragmentPagerAdapter);
        viewpagerMy1Orderf2.setOffscreenPageLimit(4);
        viewpagerMy1Orderf2.setScroll(false);
        viewpagerMy1Orderf2.setCurrentItem(0, false);
        viewpagerMy1Orderf2.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                OneBean1 bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }

    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        checkverionFzxPresenter.onDestory();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ll_user_information) {//个人中心
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            intent.putExtra("show", 0);
            startActivity(intent);
        } else if (id == R.id.iv_setup) {//系统设置
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            intent.putExtra("show", 8);
            startActivity(intent);
        } else if (id == R.id.ll_subject) {//设置学科

            new XPopup.Builder(MainActivity1.this)
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .offsetY(20)
                    .offsetX(40)
                    .atView(view)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                    .asAttachList1(new String[]{"语文", "数学", "物理", "化学"},
                            new int[]{},
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
                                    tvSubject.setText(text);
                                    //实际项目中  list集合中获取 position 的text
                                    ToastUtils.showShort(text + "--->刷新当前页学科" + position);
                                    SPUtils.getInstance().put("shouye_class_name", position + "");
//                                    SPUtils.getInstance().getString("shouye_class_name","0");
                                }
                            }, com.example.app5libbase.R.layout.popup_adapter_rv, com.example.app5libbase.R.layout.popup_adapter_item, checkedPos)
                    .show();

        }
    }


    /**
     * 集合转 String[]数组
     */
    private String[] getStringData(List<String> list) {
        String[] strArray = list.toArray(new String[list.size()]);
        return strArray;
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }
}
