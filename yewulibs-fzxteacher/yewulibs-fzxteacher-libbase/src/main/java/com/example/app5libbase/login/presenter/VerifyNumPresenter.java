package com.example.app5libbase.login.presenter;

import android.os.Handler;
import android.os.Message;

import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.login.activity.ForgetPswActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.login.view.VerifyNumView;
import com.example.app5libbase.util.DialogUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * VerifyNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyNumPresenter extends BasePresenter<VerifyNumView, ForgetPswActivity> {
    private long endTime;

    public void sendVerityCode(String tel) {
        endTime = System.currentTimeMillis() + 60 * 1000;
        mHandler.sendEmptyMessageDelayed(0, 100);
        Network.createService(NetWorkService.ForgetPswService.class)
                .sendVerityCode(tel, "3")
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        stopCountDown();
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getCode() == 21202) {
                                new DialogUtils().createTipDialog(mActivity).buildText("今天获取验证码次数已达上限，\n" +
                                        "请明天重试。").show();
                                return;
                            }
                        }
                        ToastUtil.showShortlToast("验证码发送失败");
                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {

        Network.createService(NetWorkService.ForgetPswService.class)
                .checkVerityCode(phoneNum, code)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        mView.verifySuccess(phoneNum, code);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getCode() == 21204) {
                                stopCountDown();
                                ToastUtil.showShortlToast("验证码已过期，请重新获取");
                                return;
                            }
                        }
                        ToastUtil.showShortlToast("验证码错误，请重新输入");
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }


    private void stopCountDown() {
        endTime = System.currentTimeMillis();
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("时间差：" + (endTime - System.currentTimeMillis()));
            final int lessTime = (int) ((endTime - System.currentTimeMillis()) / 1000);

            mView.onCountDownChanged(lessTime);
            if (lessTime < 0) {
                mHandler.removeMessages(0);
            } else {
                mHandler.sendEmptyMessageDelayed(0, 100);
            }
        }
    };

    @Override
    public void detachView() {
        super.detachView();
        mHandler.removeMessages(0);
    }
}
