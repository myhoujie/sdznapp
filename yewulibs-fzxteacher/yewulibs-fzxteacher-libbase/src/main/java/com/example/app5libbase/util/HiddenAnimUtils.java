package com.example.app5libbase.util;

import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sdzn.fzx.student.libutils.util.UiUtils;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/2/5
 */
public class HiddenAnimUtils {

    private int mHeight;//伸展高度

    private View hideView;

    private AnimatorListenerAdapter animatorListenerAdapter;

    /**
     * 构造器(可根据自己需要修改传参)
     *
     * @param context  上下文
     * @param hideView 需要隐藏或显示的布局view
     * @param height   布局展开的高度(根据实际需要传)
     */
    public static HiddenAnimUtils newInstance(Context context, View hideView, int height, AnimatorListenerAdapter animatorListenerAdapter) {
        return new HiddenAnimUtils(context, hideView, height, animatorListenerAdapter);
    }

    private HiddenAnimUtils(Context context, View hideView, int height, AnimatorListenerAdapter animatorListenerAdapter) {
        this.hideView = hideView;
        this.animatorListenerAdapter = animatorListenerAdapter;
        mHeight = UiUtils.dp2px(height);
    }

    /**
     * 开关
     */
    public void toggle() {
        if (View.VISIBLE == hideView.getVisibility()) {
            closeAnimate(hideView);//布局隐藏
        } else {
            openAnim(hideView);//布局铺开
        }
    }

    private void openAnim(View v) {
        v.setVisibility(View.VISIBLE);
        ValueAnimator animator = createDropAnimator(v, 0, mHeight);
        animator.start();
    }

    private void closeAnimate(final View view) {
        int origHeight = view.getHeight();
        ValueAnimator animator = createDropAnimator(view, origHeight, 0);
        animator.addListener(animatorListenerAdapter);
        animator.start();
    }

    private ValueAnimator createDropAnimator(final View v, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                int value = (int) arg0.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                layoutParams.height = value;
                v.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
}
