package com.example.app5libbase.ai.pop.bean;

import android.content.res.ColorStateList;

public class TextModel {
    public String text; //RadioButton的文字
    public boolean isChecked;//是否选中
    public boolean hiddenRadio;//是否需要隐藏radio

    public TextModel(String text, boolean isChecked, boolean hiddenRadio) {
        this.text = text;
        this.isChecked = isChecked;
        this.hiddenRadio = hiddenRadio;
    }

    public TextModel(String text) {
        this.isChecked = false;
        this.hiddenRadio = false;
        this.text = text;
    }


    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public boolean isChecked() {
        return isChecked;
    }
}
