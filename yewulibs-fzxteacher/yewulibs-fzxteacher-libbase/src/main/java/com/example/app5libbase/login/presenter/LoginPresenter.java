package com.example.app5libbase.login.presenter;

import android.content.Intent;
import android.util.Base64;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.base.BaseActivity;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.login.activity.LoginActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.login.view.LoginView;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.InClassStatus;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;
import com.tencent.bugly.crashreport.CrashReport;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * LoginPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoginPresenter extends BasePresenter<LoginView, BaseActivity> {

    public void login(final String name, String psw) {
        if (!vertifyNum(name, psw)) {
            return;
        }
        final String deviceId = AndroidUtil.getDeviceID( App2.get());
        CrashReport.putUserData( App2.get(), "studentName", name);
        CrashReport.putUserData( App2.get(), "studentPwd", psw);
        if ("/auth".equals(BuildConfig3.AUTH)) {
            psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
        }
        Network.createService(NetWorkService.LoginService.class)
                .login(name, psw, deviceId, "Android")
                .map(new StatusFunc<LoginBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
                    @Override
                    public void onNext(LoginBean o) {
                        mView.loginSuccess(o);
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof ApiException) {
                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
                            return;
                        }
                        ToastUtil.showShortlToast("登录失败");

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));


    }

    public void forgetPsw() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ForgetPswActivity");
        mActivity.startActivity(intent);
    }


    private boolean vertifyNum(final String userName, final String psw) {
        if (userName.length() == 8 || StringUtils.isMobile(userName)) {
            if (StringUtils.vertifyPsw(psw)) {
                return true;
            } else {
                ToastUtil.showShortlToast("账号或密码错误");
            }
        } else {
            ToastUtil.showShortlToast("账号或密码错误");
        }
        return false;

    }

    /**
     * 启动mqtt
     */
    public void startMqttService() {
        Intent intent = new Intent(mActivity, MqttService.class);
        mActivity.startService(intent);

    }
    public void getInClassStatus(){
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getInClassStatus(String.valueOf(SPUtils.getLoginBean().getData().getUser().getId()))
                .map(new StatusFunc<InClassStatus>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<InClassStatus>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(InClassStatus o) {
                        mView.inClassStatusResult(o);
                    }
                });

    }


}
