package com.example.app5libbase.listener;

import com.example.app5libbase.views.datepick.DatePickWheelDialog;

public interface OnDatePickListener {
    void onClick(DatePickWheelDialog dialog);
}
