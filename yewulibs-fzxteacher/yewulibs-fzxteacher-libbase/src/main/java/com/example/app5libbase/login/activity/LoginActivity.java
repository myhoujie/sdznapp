package com.example.app5libbase.login.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.login.presenter.LoginPresenter;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.api.network.PingTest;
import com.example.app5libbase.app.NetworkReceiver;
import com.example.app5libbase.service.TimerHeadService;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.controller.UserController;
import com.example.app5libpublic.event.Event;
import com.example.app5libbase.login.view.LoginView;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.teacher.presenter.LoginNewPresenter;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.ServiceUtil;
import com.sdzn.fzx.teacher.view.LoginViews;
import com.sdzn.fzx.teacher.vo.InClassStatus;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.example.app5libpublic.constant.Constant;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.dao.UserSubject;
import com.tencent.bugly.crashreport.CrashReport;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class LoginActivity extends BaseActivity implements LoginViews, TextWatcher, View.OnClickListener, EasyPermissions.PermissionCallbacks {

    private LoginNewPresenter mPresenter;
    private static final int REQUECT_CODE_SDCARD = 2;
    private RelativeLayout activityLogin;
    private RelativeLayout netErrorRy;
    private Button userNumIcon;
    private EditText userNumEdit;
    private ImageView userNumEditDelImg;
    private Button pswIcon;
    private EditText pswEdit;
    private ImageView pswNumEditDelImg;
    private TextView forgetPsw;
    private Button loginBtn;
    private TextView tvVersion;
    private LinearLayout llPememberPas;
    private CheckBox check;
    private TextView tvYonghu;
    private TextView tvYinsi;
    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private Dialog dialog;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void initPresenter() {
        mPresenter = new LoginNewPresenter();
        mPresenter.onCreate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.whiteF5F6FF), 0);
        setReturnPop(false);
        setContentView(R.layout.activity_login);
        dialog = new Dialog(LoginActivity.this, R.style.notice_dialog);
        activityLogin = (RelativeLayout) findViewById(R.id.activity_login);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        userNumIcon = (Button) findViewById(R.id.user_num_icon);
        userNumEdit = (EditText) findViewById(R.id.user_num_edit);
        userNumEditDelImg = (ImageView) findViewById(R.id.user_num_edit_del_img);
        pswIcon = (Button) findViewById(R.id.psw_icon);
        pswEdit = (EditText) findViewById(R.id.psw_edit);
        pswNumEditDelImg = (ImageView) findViewById(R.id.psw_num_edit_del_img);
        forgetPsw = (TextView) findViewById(R.id.forget_psw);
        loginBtn = (Button) findViewById(R.id.login_btn);
        tvVersion = (TextView) findViewById(R.id.tv_version);
        check = (CheckBox) findViewById(R.id.check);
        llPememberPas = (LinearLayout) findViewById(R.id.ll_remember_pas);
        tvYonghu = (TextView) findViewById(R.id.tv_yonghu);
        tvYinsi = (TextView) findViewById(R.id.tv_yinsi);
        forgetPsw.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        llPememberPas.setOnClickListener(this);
        userNumEditDelImg.setOnClickListener(this);
        pswNumEditDelImg.setOnClickListener(this);
        tvYonghu.setOnClickListener(this);
        tvYinsi.setOnClickListener(this);
        initPresenter();
        initView();
        initData();
        EventBus.getDefault().register(this);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入LoginActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        userNumEdit.addTextChangedListener(this);
        pswEdit.addTextChangedListener(this);
        methodRequiresTwoPermission2();
//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                PingTest.getInstance().startPing();
                dialog.dismiss();
            } else {
                methodRequiresTwoPermission2();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void methodRequiresTwoPermission2() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            PingTest.getInstance().startPing();
            dialog.dismiss();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }

    }
   /* @PermissionGrant(REQUECT_CODE_SDCARD)
    public void requestSdcardSuccess() {
//        PingTest.getInstance().startPing();
    }*/

    @Override
    protected void initData() {
        String versionName = getVersionName();
        if (versionName == null) {
            tvVersion.setVisibility(View.GONE);
        } else {
            tvVersion.setVisibility(View.VISIBLE);
            tvVersion.setText("辅助线教师 " + versionName);
        }
        final String loginUserNum = (String) SPUtils.get(this, SPUtils.LOGIN_USER_NUM, "");
        if (!TextUtils.isEmpty(loginUserNum)) {
            userNumEdit.setText(loginUserNum);
        }
        if ((boolean) SPUtils.get(this, SPUtils.LOGIN_CHECK_STATUS, false)) {
            check.setChecked(true);
            pswEdit.setText(String.valueOf(SPUtils.get(this, SPUtils.LOGIN_PAS_CHECK, "")));
        }
    }

    private String getVersionName() {
        try {
            PackageInfo packInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.forget_psw) {
            mPresenter.forgetPsw();
        } else if (id == R.id.login_btn) {
            final String userName = userNumEdit.getText().toString();
            final String userPsw = pswEdit.getText().toString();

            if (!mPresenter.vertifyNum(userName, userPsw)) {
                return;
            }
            CrashReport.putUserData( App2.get(), "studentName", userName);
            CrashReport.putUserData(App2.get(), "studentPwd", userPsw);
            mPresenter.login(userName, userPsw,LoginActivity.this);
        } else if (id == R.id.user_num_edit_del_img) {
            userNumEdit.setText("");
        } else if (id == R.id.psw_num_edit_del_img) {
            pswEdit.setText("");
        } else if (id == R.id.ll_remember_pas) {
            if (check.isChecked()) {
                check.setChecked(false);
                SPUtils.put(this, SPUtils.LOGIN_CHECK_STATUS, false);
            } else {
                check.setChecked(true);
                SPUtils.put(this, SPUtils.LOGIN_CHECK_STATUS, true);
            }
        } else if (id == R.id.tv_yonghu) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuContract.html");
            startActivity(intent);
        } else if (id == R.id.tv_yinsi) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuPrivacyContract.html");
            startActivity(intent);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        changeLoginStatus();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void changeLoginStatus() {
        final Editable userNumText = userNumEdit.getText();
        final Editable pswText = pswEdit.getText();
        boolean canLogin;
        /**
         * 思路：默认为 不可登录（false）状态，用户名有文字时，设为true，没有文字设置为false，
         * 接着判断密码，如果没文字，设置为false，如果有文字，则不改变之前的状态
         */
        if (TextUtils.isEmpty(userNumText)) {
            canLogin = false;
            userNumIcon.setEnabled(false);
        } else {
            canLogin = true;
            userNumIcon.setEnabled(true);
        }

        if (TextUtils.isEmpty(pswText)) {
            canLogin = false;
            pswIcon.setEnabled(false);
        } else {
            pswIcon.setEnabled(true);
        }

        loginBtn.setEnabled(canLogin);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ServiceUtil.isServiceRunning(App2.get(), TimerHeadService.class.getName())) {
            Intent intent2 = new Intent(this, TimerHeadService.class);
            stopService(intent2);
        }
    }

    @Override
    public void loginSuccess(LoginBean.DataBean vo) {
        LoginBean loginBean = new LoginBean();
        loginBean.setData(vo);
        SPUtils.saveLoginBean(loginBean);

        final Editable userNumText = userNumEdit.getText();
        final Editable pswText = pswEdit.getText();
        SPUtils.put(this, SPUtils.LOGIN_USER_NUM, userNumText);
        if (check.isChecked()) {
            SPUtils.put(this, SPUtils.LOGIN_PAS_CHECK, pswText);
        } else {
            SPUtils.put(this, SPUtils.LOGIN_PAS_CHECK, "");
        }
        com.blankj.utilcode.util.SPUtils.getInstance().put("token", vo.getAccessToken());

//        if (vo != null && vo != null && vo.getSubjectList() != null && vo.getSubjectList().size() > 0) {
//            UserSubject userSubject = UserController.queryByUserId();
//            LoginBean.DataBean.SubjectListBean subjectListBean = null;
//            if (userSubject != null) {
//                for (LoginBean.DataBean.SubjectListBean bean : vo.getSubjectList()) {
//                    if (bean.getSubjectId() == userSubject.getSubjectId()) {
//                        subjectListBean = bean;
//                    }
//                }
//            }
//            if (subjectListBean == null) {
//                subjectListBean = vo.getSubjectList().get(0);
//            }
//            SubjectSPUtils.saveCurrentSubject(subjectListBean);
//        }
        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);

//        mPresenter.getInClassStatus();
        startMqttService();

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1");
        startActivity(intent);

        finish();
    }

    private void startMqttService() {
        Intent intent = new Intent(this, MqttService.class);
        startService(intent);

    }


    @Override
    public void loginFailed(String msg) {
        if (msg!=null){
            ToastUtils.showShort(msg);
        }else {
            ToastUtils.showShort("登录失败");
        }
    }

    @Override
    public void inClassStatusResult(InClassStatus classStatus) {
        if (classStatus.getData() != null && classStatus.getData().getStatus() == 1) {

            Date date = new Date();
            final String now = sdf.format(date);
            int inClassDate = (int) DateUtil.getTimeMillisByStr(classStatus.getData().getClassTime(), "yyyy-MM-dd HH:mm") / 1000;
            int nowDate = (int) DateUtil.getTimeMillisByStr(now, "yyyy-MM-dd HH:mm:ss") / 1000;
            if (inClassDate > nowDate) {
                int differTime = inClassDate - nowDate;
                Constant.setShowHeadCountDown(true);
                BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
                TimeBaseUtil.getInstance().showHeadCountView(differTime);
                SPUtils.put(LoginActivity.this, SPUtils.CLASS_ID, String.valueOf(classStatus.getData().getClassId()));
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
