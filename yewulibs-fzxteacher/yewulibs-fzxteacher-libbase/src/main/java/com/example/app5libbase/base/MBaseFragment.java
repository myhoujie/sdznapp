package com.example.app5libbase.base;

import android.os.Bundle;

/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * -
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class MBaseFragment<P extends BasePresenter> extends BaseFragment {

    public P mPresenter;

    public abstract void initPresenter();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }
}