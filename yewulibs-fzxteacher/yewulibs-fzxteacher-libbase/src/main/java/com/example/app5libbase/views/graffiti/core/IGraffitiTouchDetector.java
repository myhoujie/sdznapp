package com.example.app5libbase.views.graffiti.core;

import android.view.MotionEvent;

public interface IGraffitiTouchDetector {
    public boolean onTouchEvent(MotionEvent event);
}
