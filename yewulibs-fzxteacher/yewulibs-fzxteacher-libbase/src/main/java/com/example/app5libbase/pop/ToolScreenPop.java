package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;

import net.ossrs.yasea.SimplePublisher;


/**
 * 投屏弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class ToolScreenPop extends PopupWindow implements View.OnClickListener {
    private View viewBg;
    private TextView tvHighQuality;
    private TextView tvNormalQuality;
    private TextView tvExit;

    private Activity activity;
    private ToolsPop toolsPop;

    public ToolScreenPop(final Activity activity, ToolsPop toolsPop) {
        this.activity = activity;
        this.toolsPop = toolsPop;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tool_screen, null);
        viewBg = (View) contentView.findViewById(R.id.viewBg);
        tvHighQuality = (TextView) contentView.findViewById(R.id.tvHighQuality);
        tvNormalQuality = (TextView) contentView.findViewById(R.id.tvNormalQuality);
        tvExit = (TextView) contentView.findViewById(R.id.tvExit);

        tvHighQuality.setOnClickListener(this);
        tvNormalQuality.setOnClickListener(this);
        tvExit.setOnClickListener(this);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        viewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvHighQuality.setTextColor(activity.getResources().getColor(R.color.blue));
        tvNormalQuality.setTextColor(activity.getResources().getColor(R.color.activity_main_text1));
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvHighQuality) {// 高清
            SimplePublisher.getInstance().setHighQuality(0);
            tvHighQuality.setTextColor(activity.getResources().getColor(R.color.blue));
            tvNormalQuality.setTextColor(activity.getResources().getColor(R.color.activity_main_text1));
            dismiss();
        } else if (id == R.id.tvNormalQuality) {// 流畅
            SimplePublisher.getInstance().setHighQuality(1);
            tvHighQuality.setTextColor(activity.getResources().getColor(R.color.activity_main_text1));
            tvNormalQuality.setTextColor(activity.getResources().getColor(R.color.blue));
            dismiss();
        } else if (id == R.id.tvExit) {// 退出投屏
            SimplePublisher.getInstance().stop();
            TimeBaseUtil.getInstance().mMediaProjection = null;
            if (toolsPop != null) {
                toolsPop.setScreen(false);
            }
            dismiss();
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            this.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public void dismissToolScreenPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }
}