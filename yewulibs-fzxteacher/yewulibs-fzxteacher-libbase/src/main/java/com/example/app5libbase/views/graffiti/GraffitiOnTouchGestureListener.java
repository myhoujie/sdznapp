package com.example.app5libbase.views.graffiti;

import android.animation.ValueAnimator;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.example.app5libbase.views.graffiti.core.IGraffitiSelectableItem;


/**
 * GraffitiView的涂鸦手势监听
 * Created on 30/06/2018.
 */

public class GraffitiOnTouchGestureListener extends TouchGestureDetector.OnTouchGestureListener {

    // 触摸的相关信息
    private float mTouchX, mTouchY;
    private float mLastTouchX, mLastTouchY;

    // 缩放相关
    private Float mLastFocusX;
    private Float mLastFocusY;
    private float mTouchCentreX, mTouchCentreY;

    private Path mCurrPath; // 当前手写的路径
    private GraffitiPath mCurrGraffitiPath;

    private GraffitiView mGraffiti;

    // 动画相关
    private ValueAnimator mScaleAnimator;
    private float mScaleAnimTransX, mScaleAnimTranY;
    private ValueAnimator mTranslateAnimator;
    private float mTransAnimOldY, mTransAnimY;

    private IGraffitiSelectableItem mSelectedItem; // 当前选中的item

    public GraffitiOnTouchGestureListener(GraffitiView graffiti) {
        mGraffiti = graffiti;
    }

    public void setSelectedItem(IGraffitiSelectableItem selectedItem) {
        IGraffitiSelectableItem old = mSelectedItem;
        mSelectedItem = selectedItem;

        if (old != null) { // 取消选定
            old.setSelected(false);
        }
        if (mSelectedItem != null) {
            mSelectedItem.setSelected(true);
        }

    }

    public IGraffitiSelectableItem getSelectedItem() {
        return mSelectedItem;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        mTouchX = e.getX();
        mTouchY = e.getY();
        return true;
    }

    /**
     * 开始滚动
     */
    @Override
    public void onScrollBegin(MotionEvent event) {
        mLastTouchX = mTouchX = event.getX();
        mLastTouchY = mTouchY = event.getY();

        // 初始化绘制
        mCurrPath = new Path();
        mCurrPath.moveTo(mGraffiti.toX(mTouchX), mGraffiti.toY(mTouchY));
        mCurrGraffitiPath = GraffitiPath.toPath(mGraffiti, mCurrPath);
        mGraffiti.addItem(mCurrGraffitiPath);
        mGraffiti.invalidate();
    }

    @Override
    public void onScrollEnd(MotionEvent e) {
        mLastTouchX = mTouchX;
        mLastTouchY = mTouchY;
        mTouchX = e.getX();
        mTouchY = e.getY();

        if (mCurrGraffitiPath != null) {
            mCurrGraffitiPath = null;
        }
        mGraffiti.invalidate();
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        mLastTouchX = mTouchX;
        mLastTouchY = mTouchY;
        mTouchX = e2.getX();
        mTouchY = e2.getY();

        mCurrPath.quadTo(
                mGraffiti.toX(mLastTouchX),
                mGraffiti.toY(mLastTouchY),
                mGraffiti.toX((mTouchX + mLastTouchX) / 2),
                mGraffiti.toY((mTouchY + mLastTouchY) / 2));
        mCurrGraffitiPath.updatePath(mCurrPath);
        mGraffiti.invalidate();
        return true;
    }


    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        mLastTouchX = mTouchX;
        mLastTouchY = mTouchY;
        mTouchX = e.getX();
        mTouchY = e.getY();

        // 模拟一次滑动
        onScrollBegin(e);
        onScroll(e, e, 0, 0);
        onScrollEnd(e);
        mGraffiti.invalidate();
        return true;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        mLastFocusX = null;
        mLastFocusY = null;
        return true;
    }

    private float pendingX, pendingY, pendingScale = 1;

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        // 屏幕上的焦点
        mTouchCentreX = detector.getFocusX();
        mTouchCentreY = detector.getFocusY();

        if (mLastFocusX != null && mLastFocusY != null) { // 焦点改变
            final float dx = mTouchCentreX - mLastFocusX;
            final float dy = mTouchCentreY - mLastFocusY;
            // 移动图片
            if (Math.abs(dx) > 1 || Math.abs(dy) > 1) {
                mGraffiti.setGraffitiTranslationX(mGraffiti.getGraffitiTranslationX() + dx + pendingX);
                mGraffiti.setGraffitiTranslationY(mGraffiti.getGraffitiTranslationY() + dy + pendingY);
                pendingX = pendingY = 0;
            } else {
                pendingX += dx;
                pendingY += dy;
            }
        }

        if (Math.abs(1 - detector.getScaleFactor()) > 0.005f) {
            // 缩放图片
            float scale = mGraffiti.getGraffitiScale() * detector.getScaleFactor() * pendingScale;
            mGraffiti.setGraffitiScale(scale, mGraffiti.toX(mTouchCentreX), mGraffiti.toY(mTouchCentreY));
            pendingScale = 1;
        } else {
            pendingScale *= detector.getScaleFactor();
        }

        mLastFocusX = mTouchCentreX;
        mLastFocusY = mTouchCentreY;

        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
        if (mGraffiti.getGraffitiScale() < 1) { //
            if (mScaleAnimator == null) {
                mScaleAnimator = new ValueAnimator();
                mScaleAnimator.setDuration(100);
                mScaleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (float) animation.getAnimatedValue();
                        float fraction = animation.getAnimatedFraction();
                        mGraffiti.setGraffitiScale(value, mGraffiti.toX(mTouchCentreX), mGraffiti.toY(mTouchCentreY));
                        mGraffiti.setGraffitiTranslation(mScaleAnimTransX * (1 - fraction), mScaleAnimTranY * (1 - fraction));
                    }
                });
            }
            mScaleAnimator.cancel();
            mScaleAnimTransX = mGraffiti.getGraffitiTranslationX();
            mScaleAnimTranY = mGraffiti.getGraffitiTranslationY();
            mScaleAnimator.setFloatValues(mGraffiti.getGraffitiScale(), 1);
            mScaleAnimator.start();
        } else { //
            limitBound(true);
        }
    }

    /**
     * 限定边界
     *
     * @param anim 动画效果
     */
    public void limitBound(boolean anim) {
        if (mGraffiti.getGraffitiRotation() % 90 != 0) { // 只处理0,90,180,270
            return;
        }

        final float oldX = mGraffiti.getGraffitiTranslationX(), oldY = mGraffiti.getGraffitiTranslationY();
        RectF bound = mGraffiti.getGraffitiBound();
        float x = mGraffiti.getGraffitiTranslationX(), y = mGraffiti.getGraffitiTranslationY();
        float width = mGraffiti.getCenterWidth() * mGraffiti.getRotateScale(), height = mGraffiti.getCenterHeight() * mGraffiti.getRotateScale();

        // 上下都在屏幕内
        if (bound.height() <= mGraffiti.getHeight()) {
            if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                y = (height - height * mGraffiti.getGraffitiScale()) / 2;
            } else {
                x = (width - width * mGraffiti.getGraffitiScale()) / 2;
            }
        } else {
            float heightDiffTop = bound.top;
            // 只有上在屏幕内
            if (bound.top > 0 && bound.bottom >= mGraffiti.getHeight()) {
                if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                    if (mGraffiti.getGraffitiRotation() == 0) {
                        y = y - heightDiffTop;
                    } else {
                        y = y + heightDiffTop;
                    }
                } else {
                    if (mGraffiti.getGraffitiRotation() == 90) {
                        x = x - heightDiffTop;
                    } else {
                        x = x + heightDiffTop;
                    }
                }
            } else if (bound.bottom < mGraffiti.getHeight() && bound.top <= 0) { // 只有下在屏幕内
                float heightDiffBottom = mGraffiti.getHeight() - bound.bottom;
                if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                    if (mGraffiti.getGraffitiRotation() == 0) {
                        y = y + heightDiffBottom;
                    } else {
                        y = y - heightDiffBottom;
                    }
                } else {
                    if (mGraffiti.getGraffitiRotation() == 90) {
                        x = x + heightDiffBottom;
                    } else {
                        x = x - heightDiffBottom;
                    }
                }
            }
        }

        // 左右都在屏幕内
        if (bound.width() <= mGraffiti.getWidth()) {
            if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                x = (width - width * mGraffiti.getGraffitiScale()) / 2;
            } else {
                y = (height - height * mGraffiti.getGraffitiScale()) / 2;
            }
        } else {
            float widthDiffLeft = bound.left;
            // 只有左在屏幕内
            if (bound.left > 0 && bound.right >= mGraffiti.getWidth()) {
                if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                    if (mGraffiti.getGraffitiRotation() == 0) {
                        x = x - widthDiffLeft;
                    } else {
                        x = x + widthDiffLeft;
                    }
                } else {
                    if (mGraffiti.getGraffitiRotation() == 90) {
                        y = y + widthDiffLeft;
                    } else {
                        y = y - widthDiffLeft;
                    }
                }
            } else if (bound.right < mGraffiti.getWidth() && bound.left <= 0) { // 只有右在屏幕内
                float widthDiffRight = mGraffiti.getWidth() - bound.right;
                if (mGraffiti.getGraffitiRotation() == 0 || mGraffiti.getGraffitiRotation() == 180) {
                    if (mGraffiti.getGraffitiRotation() == 0) {
                        x = x + widthDiffRight;
                    } else {
                        x = x - widthDiffRight;
                    }
                } else {
                    if (mGraffiti.getGraffitiRotation() == 90) {
                        y = y - widthDiffRight;
                    } else {
                        y = y + widthDiffRight;
                    }
                }
            }
        }
        if (anim) {
            if (mTranslateAnimator == null) {
                mTranslateAnimator = new ValueAnimator();
                mTranslateAnimator.setDuration(100);
                mTranslateAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (float) animation.getAnimatedValue();
                        float fraction = animation.getAnimatedFraction();
                        mGraffiti.setGraffitiTranslation(value, mTransAnimOldY + (mTransAnimY - mTransAnimOldY) * fraction);
                    }
                });
            }
            mTranslateAnimator.setFloatValues(oldX, x);
            mTransAnimOldY = oldY;
            mTransAnimY = y;
            mTranslateAnimator.start();
        } else {
            mGraffiti.setGraffitiTranslation(x, y);
        }
    }
}
