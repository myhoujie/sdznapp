package com.example.app5libbase.util;

import android.widget.SeekBar;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/4/1
 * 修改单号：
 * 修改内容:
 */
public abstract class MySeekBarChangeListener implements SeekBar.OnSeekBarChangeListener  {
    public int position;
    public MySeekBarChangeListener(int position) {
        this.position =position;
    }
}
