package com.example.app5libbase.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;

/**
 * 描述：带图标hint的搜索框
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ImageHintEditText extends AppCompatTextView {

    float searchSize = 0;
    float textSize = 0;
    int textColor = 0xFF000000;
    boolean isCenter;
    String text = "";
    Drawable mDrawable;
    Paint paint;

    public ImageHintEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitResource(context, attrs);
        InitPaint();
    }

    private void InitResource(Context context, AttributeSet attrs) {
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.ImageHintEditText);
        searchSize = mTypedArray.getDimension(R.styleable.ImageHintEditText_searchViewSize, UiUtils.sp2px(18));
        textColor = mTypedArray.getColor(R.styleable.ImageHintEditText_searchViewHintColor, 0xFF848484);
        textSize = mTypedArray.getDimension(R.styleable.ImageHintEditText_searchViewHintSize, UiUtils.sp2px(14));
        text = mTypedArray.getString(R.styleable.ImageHintEditText_searchViewHint) == null ? "" : mTypedArray.getString(R.styleable.ImageHintEditText_searchViewHint);
        isCenter = mTypedArray.getBoolean(R.styleable.ImageHintEditText_isCenter, false);
        mTypedArray.recycle();
    }

    private void InitPaint() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setTextSize(textSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        DrawSearchIcon(canvas);
    }

    private void DrawSearchIcon(Canvas canvas) {
        if (this.getText().toString().length() == 0 || !isCenter) {
            float textWidth = paint.measureText(text);

            float dx = UiUtils.sp2px(10);
            if (isCenter) {
                dx = (getWidth() - searchSize - textWidth - UiUtils.sp2px(8)) / 2;
            }
            float dy = (getHeight() - searchSize) / 2;

            canvas.save();
            canvas.translate(getScrollX() + dx, getScrollY() + dy);
            if (mDrawable != null) {
                mDrawable.draw(canvas);
            }
        }
        if (this.getText().toString().length() == 0) {
            float textHeight = getFontLeading(paint);
            float dy = (getHeight() - searchSize) / 2;
            canvas.drawText(text, getScrollX() + searchSize + UiUtils.sp2px(6), getScrollY() + (getHeight() - (getHeight() - textHeight) / 2) - paint.getFontMetrics().bottom - dy, paint);
            canvas.restore();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mDrawable == null) {
            try {
                mDrawable = getContext().getResources().getDrawable(R.mipmap.sousuo_icon);
                mDrawable.setBounds(0, 0, (int) searchSize, (int) searchSize);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mDrawable != null) {
            mDrawable.setCallback(null);
            mDrawable = null;
        }
        super.onDetachedFromWindow();
    }

    public float getFontLeading(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.bottom - fm.top;
    }

    public void setCenter(boolean isCenter) {
        this.isCenter = isCenter;
        invalidate();
    }
}