package com.example.app5libbase.login.fragment;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.login.activity.ForgetPswActivity;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.login.presenter.ForgetPswNumPresenter;
import com.example.app5libbase.login.presenter.ForgetPswNumPresenter1;
import com.example.app5libbase.login.view.ForgetPswNumView;
import com.example.app5libbase.login.view.ForgetPswNumViews;
import com.example.app5libbase.util.DialogUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPswNumFragment extends BaseFragment implements ForgetPswNumViews, View.OnClickListener {
    ForgetPswNumPresenter1 mPresenter;
    LinearLayout container;
    TextView titleNameTxt;
    Button userNumIcon;
    EditText userNumEdit;
    Button nextBtn;
    LinearLayout TitleBackly;
    ImageView editDelImg;

    public ForgetPswNumFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_psw_num, container, false);
        container = view.findViewById(R.id.container_ly);
        titleNameTxt = view.findViewById(R.id.title_name_txt);
        userNumIcon = view.findViewById(R.id.user_num_icon);
        userNumEdit = view.findViewById(R.id.user_num_edit);
        nextBtn = view.findViewById(R.id.next_btn);
        TitleBackly = view.findViewById(R.id.title_back_ly);
        editDelImg = view.findViewById(R.id.edit_del_img);
        TitleBackly.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        editDelImg.setOnClickListener(this);
        initPresenter();
        initView();
        return view;
    }


    public void initPresenter() {
        mPresenter = new ForgetPswNumPresenter1();
        mPresenter.onCreate(this);//, (ForgetPswActivity) getActivity()
    }


    private void initView() {
        titleNameTxt.setText("找回密码");
        userNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                userNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getActivity().finish();
        } else if (id == R.id.next_btn) {
            final String userName = userNumEdit.getText().toString();
            mPresenter.verityUserName(userName);
        } else if (id == R.id.edit_del_img) {
            userNumEdit.setText("");
        }

    }


    @Override
    public void verifySuccess(String tel) {
        if (TextUtils.isEmpty(tel)) {
            new DialogUtils().createTipDialog(getActivity()).buildText("该账号未绑定手机号，\n" +
                    "请联系学校管理员找回密码。").show();
            return;
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        VerifyNumFragment fragment = new VerifyNumFragment();
        Bundle bundle = new Bundle();
        bundle.putString(VerifyNumFragment.USER_PHONE, tel);
        fragment.setArguments(bundle);

        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
        transaction.addToBackStack("tag");
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
