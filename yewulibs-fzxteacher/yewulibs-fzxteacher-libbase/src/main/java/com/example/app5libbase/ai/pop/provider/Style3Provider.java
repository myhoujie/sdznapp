package com.example.app5libbase.ai.pop.provider;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActBean;

public class Style3Provider extends BaseItemProvider<GRZXBaseRecActBean, BaseViewHolder> {

    @Override
    public int viewType() {
        return GRZXBaseRecActAdapter.STYLE_THREE;
    }

    @Override
    public int layout() {
        return R.layout.popup_right_recycleview_item3;
    }

    @Override
    public void convert(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {
//        if (position % 2 == 0) {
//            helper.setImageResource(R.id.iv_provider3, R.drawable.animation_img1);
//        } else {
//            helper.setImageResource(R.id.iv_provider3, R.drawable.animation_img2);
//        }
//        helper.setText(R.id.tv_provider3, data.getmBean().getUserName());
        helper.addOnClickListener(R.id.iv_provider3);
        helper.addOnClickListener(R.id.tv_provider3);
        helper.addOnLongClickListener(R.id.iv_provider3);
        helper.addOnLongClickListener(R.id.tv_provider3);
    }

//    @Override
//    public void onClick(BaseViewHolder helper, BaseRecActDemo42Bean data, int position) {
//        if (helper.getChildClickViewIds().contains(R.id.iv)) {
//            Toasty.normal(BaseApp.get(), position + "item click=" + data.getmBean().getUserAvatar()).show();
//        } else if (helper.getChildClickViewIds().contains(R.id.tv)) {
//            Toasty.normal(BaseApp.get(), position + "item click=" + data.getmBean().getUserName()).show();
//        } else {
//        }
//
//    }

//    @Override
//    public boolean onLongClick(BaseViewHolder helper, BaseRecActDemo42Bean data, int position) {
//        Toast.makeText(mContext, "longClick", Toast.LENGTH_SHORT).show();
//        return true;
//    }
}
