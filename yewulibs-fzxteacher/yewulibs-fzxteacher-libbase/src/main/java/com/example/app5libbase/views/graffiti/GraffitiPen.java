package com.example.app5libbase.views.graffiti;


import com.example.app5libbase.views.graffiti.core.IGraffitiPen;

/**
 * 常用画笔
 */
public enum GraffitiPen implements IGraffitiPen {

    HAND; // 手绘

    @Override
    public IGraffitiPen copy() {
        return this;
    }
}
