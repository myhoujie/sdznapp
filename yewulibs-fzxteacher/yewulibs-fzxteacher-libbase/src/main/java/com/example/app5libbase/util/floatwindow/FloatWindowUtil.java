package com.example.app5libbase.util.floatwindow;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.app.SchoolBoxWatcher;
import com.example.app5libbase.login.activity.LoginActivity;
import com.example.app5libbase.baseui.activity.WelcomeActivity;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.listener.OnCountDownListener;
import com.example.app5libbase.views.datepick.CountDownPickWheelDialog;
import com.example.app5libpublic.constant.Constant;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.OnScreenCaptureEvent;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.yhao.floatwindow.FloatWindow;
import com.yhao.floatwindow.IFloatWindowImpl;
import com.yhao.floatwindow.MoveType;
import com.yhao.floatwindow.PermissionListener;
import com.yhao.floatwindow.Screen;
import com.yhao.floatwindow.ViewStateListener;
import com.yhao.floatwindow.radioview.RadioLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.blankj.utilcode.util.ActivityUtils.startActivity;

public class FloatWindowUtil implements View.OnClickListener {
    private static FloatWindowUtil floatWindowUtil = null;

    private SchoolBoxWatcher schoolBoxWatcher = new SchoolBoxWatcher();
    private static final String floatWindowTag = "floatWindowTag";
    private boolean windowShow = false;//悬浮窗是否展开
    private int offset;

    private FrameLayout container;
    private LinearLayout llBg;
    private ImageView ivTool;
    private RadioLayout rg;
    private TextView tvSuoping, tvScreen;

    public FloatWindowUtil() {
        init();
    }


    public static FloatWindowUtil getInstance() {
        synchronized (FloatWindowUtil.class) {
            if (floatWindowUtil == null) {
                floatWindowUtil = new FloatWindowUtil();
            }
        }
        return floatWindowUtil;
    }

    public void init(){
        container = (FrameLayout) LayoutInflater.from(App2.get()).inflate(R.layout.layout_floatwindow, null);
        ivTool = container.findViewById(R.id.iv_tool);
        llBg = container.findViewById(R.id.ll_bg);
        rg = container.findViewById(R.id.rg);

        FloatWindow
                .with(App2.get())
                .setTag(floatWindowTag)
                .setView(container)
                .setX(Screen.width, 0.8f)
                .setY(Screen.height, 0.5f)
                .setMoveType(MoveType.slide)
                .setFilter(false, WelcomeActivity.class, LoginActivity.class)//过滤指定页面显示悬浮窗/隐藏悬浮窗
//                .setViewStateListener(mViewStateListener)
//                .setPermissionListener(mPermissionListener)
                .setDesktopShow(false)
                .build();

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFloatWindowState();
            }
        });

        rg.findViewById(R.id.rb_suoping).setOnClickListener(this);
        rg.findViewById(R.id.rb_jiankong).setOnClickListener(this);
        rg.findViewById(R.id.rb_fenxiang).setOnClickListener(this);
        rg.findViewById(R.id.rb_baiban).setOnClickListener(this);
        rg.findViewById(R.id.rb_tuya).setOnClickListener(this);
        rg.findViewById(R.id.rb_weike).setOnClickListener(this);
        rg.findViewById(R.id.rb_daojishi).setOnClickListener(this);
        rg.findViewById(R.id.rb_tiwen).setOnClickListener(this);
        rg.findViewById(R.id.rb_pingfen).setOnClickListener(this);

        tvSuoping = rg.findViewById(R.id.tv_suoping);
        tvScreen = rg.findViewById(R.id.tvScreen);


    }

    private void changeFloatWindowState() {
        windowShow = !windowShow;
        if (windowShow) {//展开
            IFloatWindowImpl window = (IFloatWindowImpl) FloatWindow.get(floatWindowTag);
            window.setMoveType(MoveType.inactive);
            llBg.setBackground(App2.get().getResources().getDrawable(R.drawable.bj));
            rg.setVisibility(View.VISIBLE);
            ivTool.setSelected(true);
            offset = window.getX();


            //960
            window.updateX(800);
        } else {//关闭
            IFloatWindowImpl window = (IFloatWindowImpl) FloatWindow.get(floatWindowTag);
            window.setMoveType(MoveType.slide);
            llBg.setBackground(null);
            rg.setVisibility(View.GONE);
            ivTool.setSelected(false);
            //1776
            window.updateX(offset);
        }
    }

    private boolean isSuoping = false;//默认正常使用

    @Override
    public void onClick(View v) {
        int id = v.getId();//监控
//微课
        if (id == R.id.rb_suoping) {//锁屏
//                rg.cleanRadioChecked();
            if (!SPUtils.get(App2.get(), SPUtils.CLASS_ID, "").toString().isEmpty()) {
                if (isSuoping) {//去解锁
                    getBehavior(4);
                    isSuoping = false;
                } else {
                    getBehavior(3);
                    isSuoping = true;
                }
            } else {
                ToastUtil.showShortlToast("请开启上课状态");
            }
        } else if (id == R.id.rb_jiankong) {
        } else if (id == R.id.rb_fenxiang) {//屏幕分享
            changeFloatWindowState();
            Intent intent2 = new Intent(AppUtils.getAppPackageName() + ".hs.act.ScreenConfigActivity");
            intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent2);


//                if (SimplePublisher.getInstance().isRecording()) {
//                    SimplePublisher.getInstance().stop();
//                    rg.findViewById(R.id.rb_fenxiang).setSelected(false);
//                    tvScreen.setText("投屏");
//                    BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
//                    foregroundActivity.stopScreen();
//                } else {
//                    // TODO: 2019/7/16 弹窗
//                    rg.findViewById(R.id.rb_fenxiang).setSelected(false);
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        if (Settings.canDrawOverlays(mContext)) {
//                            Log.i("------------------L164");
//                            Intent intent2 = new Intent(mContext, FadeService.class);
//                            mContext.startService(intent2);
//                        } else {
//                            //若没有权限，提示获取.
//                            Log.i("------------------L169");
//                            CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
//                            builder.setTitle("权限提示");
//                            builder.setMessage("开启悬浮窗权限可提升投屏体验，是否去开启?");
//                            builder.setPositive("去开启", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    Intent intent3 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
//                                    Toast.makeText(mContext, "需要取得权限以使用悬浮窗", Toast.LENGTH_SHORT).show();
//                                    mContext.startActivity(intent3);
//                                    dialogInterface.dismiss();
//                                }
//                            });
//                            builder.setNegative("放弃", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    if (ActivityManager.getForegroundActivity() != null) {
//                                        ((BaseActivity) ActivityManager.getForegroundActivity()).customScan();
//                                    }
//                                    dialogInterface.dismiss();
//                                }
//                            });
//                            CustomDialog dialog = builder.create();
//                            dialog.show();
//                            return;
//                        }
//                    } else {
//                        //SDK在23以下，不用管.
//                        Log.i("------------------L196");
//                        Intent intent1 = new Intent(mContext, FadeService.class);
//                        Log.i("------------------L197");
//                        mContext.startService(intent1);
//                    }
//
//                    // 扫描二维码
//                    if (ActivityManager.getForegroundActivity() != null) {
//                        ((BaseActivity) ActivityManager.getForegroundActivity()).customScan();
//                    }
//                }
        } else if (id == R.id.rb_baiban) {//白板
            changeFloatWindowState();
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PaletteActivity");
            intent.putExtra("type", "baiban");
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.rb_tuya) {//涂鸦
            changeFloatWindowState();
            EventBus.getDefault().post(new OnScreenCaptureEvent());
        } else if (id == R.id.rb_weike) {
        } else if (id == R.id.rb_daojishi) {//倒计时
            changeFloatWindowState();
            showCountDownPopView();
        } else if (id == R.id.rb_tiwen) {//提问
            if (!SPUtils.get(App2.get(), SPUtils.CLASS_ID, "").toString().isEmpty()) {
                changeFloatWindowState();
                Intent tiwenIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.TiWenActivity");
                startActivity(tiwenIntent);
            } else {
                ToastUtil.showShortlToast("请开启上课状态");
            }
        } else if (id == R.id.rb_pingfen) {//评分
            changeFloatWindowState();
            Intent scoreIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ScoreActivity");
            scoreIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(scoreIntent);
        } else {
            rg.cleanRadioChecked();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginSuccess(String msg) {
        if (TextUtils.equals(Event.EVENT_TOKEN_MISS, msg)) {
            Intent intent = new Intent(App2.get(), LoginActivity.class);
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ActivityManager.finishAll();
        } else if (TextUtils.equals(Event.ONLOGIN_SUCCESS, msg)) {
            schoolBoxWatcher.requestBoxUrl();
            schoolBoxWatcher.stopWatch();
            schoolBoxWatcher.startWatch();
            schoolBoxWatcher.getQiniuConfig();
        } else if (TextUtils.equals(Event.EVENT_CLOSE_SCREEN, msg)) {
            rg.findViewById(R.id.rb_fenxiang).setSelected(true);
            tvScreen.setText("关闭投屏");
        } else if (TextUtils.equals(Event.EVENT_OPEN_SCREEN, msg)) {
            rg.findViewById(R.id.rb_fenxiang).setSelected(false);
            tvScreen.setText("投屏");
        }

    }

    private void showCountDownPopView() {
        BaseActivity activity = (BaseActivity) ActivityManager.getForegroundActivity();
        if (Constant.getShowCountDown()) {
            ToastUtil.showShortlToast(activity.getString(R.string.count_down_is_running));
            return;
        }
        CountDownPickWheelDialog countDownPickWheelDialog = new CountDownPickWheelDialog.Builder(activity)
                .setPositiveButton(new OnCountDownListener() {
                    @Override
                    public void onClick(CountDownPickWheelDialog dialog) {
                        if (dialog.getTime() != 0) {
                            BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
                            if (foregroundActivity != null) {
                                TimeBaseUtil.getInstance().showCountDownPop(dialog.getTime());
                                Constant.setShowCountDown(true);
                            }
                            dialog.dismiss();
                        } else {
                            ToastUtil.showShortlToast(App2.get().getString(R.string.time_is_zero));
                        }
                    }
                })
                .setNegativeButton(new OnCountDownListener() {
                    @Override
                    public void onClick(CountDownPickWheelDialog dialog) {
                        dialog.dismiss();
                    }
                }).create();
        countDownPickWheelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        countDownPickWheelDialog.setCanceledOnTouchOutside(false);

        countDownPickWheelDialog.show();

        WindowManager m = activity.getWindowManager();
        Display d = m.getDefaultDisplay();

        Window window = countDownPickWheelDialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = d.getWidth();
        lp.height = UiUtils.dp2px(256);
        window.setGravity(Gravity.BOTTOM);
        window.setAttributes(lp);
    }

    /**
     *
     */
    private void getBehavior(final int type) {
          /*
         1 上课 2 下课 3 锁屏 4 解锁
         */
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getClassBehavior(SPUtils.get(App2.get(), SPUtils.CLASS_ID, "").toString(), String.valueOf(type), "")
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        switch (type) {
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                ToastUtil.showShortlToast("学生端锁屏");
                                tvSuoping.setText("解锁");
                                rg.findViewById(R.id.rb_suoping).setSelected(true);
                                break;
                            case 4:
                                ToastUtil.showShortlToast("学生端解锁");
                                tvSuoping.setText("锁屏");
                                rg.findViewById(R.id.rb_suoping).setSelected(false);
                                break;
                            default:
                                break;
                        }

                    }
                });
    }

    private static PermissionListener mPermissionListener = new PermissionListener() {
        @Override
        public void onSuccess() {
        }

        @Override
        public void onFail() {
        }
    };

    private static ViewStateListener mViewStateListener = new ViewStateListener() {
        @Override
        public void onPositionUpdate(int x, int y) {
        }

        @Override
        public void onShow() {
        }

        @Override
        public void onHide() {
        }

        @Override
        public void onDismiss() {
        }

        @Override
        public void onMoveAnimStart() {
        }

        @Override
        public void onMoveAnimEnd() {
        }

        @Override
        public void onBackToDesktop() {
        }
    };


}
