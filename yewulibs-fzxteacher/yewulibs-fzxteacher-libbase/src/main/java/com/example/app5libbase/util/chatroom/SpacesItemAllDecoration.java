package com.example.app5libbase.util.chatroom;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by admin on 2019/8/15.
 */

public class SpacesItemAllDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemAllDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildPosition(view) == 0){
//            outRect.top = space;
        }else {
            outRect.top = space;
        }
    }
}