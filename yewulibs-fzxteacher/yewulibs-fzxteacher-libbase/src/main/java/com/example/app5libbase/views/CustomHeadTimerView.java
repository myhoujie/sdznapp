package com.example.app5libbase.views;

import android.app.Activity;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.sdzn.fzx.student.libutils.util.DateUtil;

import org.xutils.x;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * baseActivity基类中  头部倒计时工具  (吊起 popubwindow)
 */

public class CustomHeadTimerView extends PopupWindow {
    private TextView tv_timer_head;
    private ImageView im_timer_head;
    private Button bt_class_down;//下课
    private LinearLayout ll_student_online;
    private Activity activity;
    View contentView;
    private View viewParent;//

    public CustomHeadTimerView(Activity activity) {
        this.activity = activity;
        contentView = LayoutInflater.from(activity).inflate(R.layout.view_timer_head_custom, null);
        x.view().inject(this, contentView);
        this.setContentView(contentView);
        this.setWidth(1500);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
//        this.setFocusable(true);
//        this.setTouchable(true);

        tv_timer_head = contentView.findViewById(R.id.tv_timer_head);
        im_timer_head = contentView.findViewById(R.id.im_timer_head);
        bt_class_down = contentView.findViewById(R.id.bt_class_down);
        ll_student_online = contentView.findViewById(R.id.ll_student_online);

        bt_class_down.setTextColor(Color.parseColor("#FF6D4A"));

        tv_timer_head.setTextColor(Color.parseColor("#68C426"));
        tv_timer_head.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);

        bt_class_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBaseUtil.getInstance().classDg();

            }
        });

    }


    public void iniHeadView(View parent) {
        viewParent = parent;
        if (contentView != null) {
            this.showAtLocation(parent, Gravity.TOP | Gravity.END, 200, 5);
        }
    }


    /**
     * 隐藏--倒计时
     */
    public void disHeadView() {
        this.dismiss();
    }


    public void setTime(int time) {
        String str = DateUtil.millsecondsToStr(time * 1000);
        richText(tv_timer_head, str, ":");

    }

    /**
     * 设置富文本，改变textView部分文字颜色
     *
     * @param tv
     * @param str
     * @param regExp
     */
    public static void richText(TextView tv, String str, String regExp) {
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        Pattern p = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);
//            style.setSpan(new ForegroundColorSpan(Color.GREEN), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色
            style.setSpan(new ForegroundColorSpan(Color.parseColor("#68C426")), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色

        }
        tv.setText(style);
    }

    public static void richHeadText(TextView tv,
                                    String str, String regExp) {
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        String pattern = "-?[0-9]\\d*";
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        while (m.find()) {
            int start = m.start(0);
            int end = m.end(0);
            style.setSpan(new ForegroundColorSpan(Color.GREEN), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //指定位置文本的字体颜色
        }
        tv.setText(style);
    }


}
