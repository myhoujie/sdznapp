package com.example.app5libbase.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.app5libbase.R;

/**
 * 描述：仿iphone带进度的进度条，线程安全的View，可直接在线程中更新进度
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/15
 */
public class RoundProgressBar extends View {
    /**
     * 画笔对象的引用
     */
    private Paint circlePaint;
    private Paint progressPaint;

    /**
     * 圆环的颜色
     */
    private int roundColor;

    /**
     * 圆环进度的颜色
     */
    private int roundProgressColor;

    /**
     * 中间进度百分比的字符串的颜色
     */
    private int textColor;

    /**
     * 中间进度百分比的字符串的字体
     */
    private float textSize;

    /**
     * 圆环的宽度
     */
    private float roundWidth;

    /**
     * 最大进度
     */
    private int max;

    /**
     * 当前进度
     */
    private int progress;

    /**
     * 进度的风格，实心或者空心
     */
    private int style;

    public static final int STROKE = 0;
    public static final int FILL = 1;

    public RoundProgressBar(Context context) {
        this(context, null);
    }

    public RoundProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        circlePaint = new Paint();
        progressPaint = new Paint();

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs,
                R.styleable.RoundProgressBar);

        //获取自定义属性和默认值
        roundColor = mTypedArray.getColor(R.styleable.RoundProgressBar_roundColor, Color.RED);
        roundProgressColor = mTypedArray.getColor(R.styleable.RoundProgressBar_roundProgressColor, Color.GREEN);
        textColor = mTypedArray.getColor(R.styleable.RoundProgressBar_textColor, Color.GREEN);
        textSize = mTypedArray.getDimension(R.styleable.RoundProgressBar_textSize, 16);
        roundWidth = mTypedArray.getDimension(R.styleable.RoundProgressBar_roundEdgeWidth, 5);
        max = mTypedArray.getInteger(R.styleable.RoundProgressBar_max, 100);
        progress = mTypedArray.getInteger(R.styleable.RoundProgressBar_progress, 100);
        style = mTypedArray.getInt(R.styleable.RoundProgressBar_style, 0);

        circlePaint.setColor(roundColor); //设置圆环的颜色
        circlePaint.setStyle(Paint.Style.STROKE); //设置空心
        circlePaint.setStrokeWidth(roundWidth); //设置圆环的宽度
        circlePaint.setAntiAlias(true);  //消除锯齿

        progressPaint.setStrokeWidth(roundWidth); //设置圆环的宽度
        progressPaint.setAntiAlias(true);

        mTypedArray.recycle();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画最外层的大圆环
        int centre = getWidth() / 2; //获取圆心的x坐标
        int radius = (int) (centre - roundWidth / 2); //圆环的半径
        canvas.drawCircle(centre, centre, radius, circlePaint); //画出圆环

        Log.e("log", centre + "");

        //画圆弧 ，画圆环的进度
        //设置进度是实心还是空心
        RectF oval = new RectF(centre - radius, centre - radius, centre
                + radius, centre + radius);  //用于定义的圆弧的形状和大小的界限

        switch (style) {
            case STROKE: {
                progressPaint.setStyle(Paint.Style.STROKE);
                LinearGradient linearGradient = new LinearGradient(0, 0,
                        getMeasuredWidth(),
                        getMeasuredHeight(),
                        new int[]{0xFFFF6D4A, 0xFFFFD012}, null, Shader.TileMode.MIRROR);
                progressPaint.setShader(linearGradient);
                canvas.drawArc(oval, 90, 360f * progress / max, false, progressPaint);  //根据进度画圆弧
                break;
            }
            case FILL: {
                progressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                if (progress != 0)
                    canvas.drawArc(oval, 90, 360f * progress / max, true, progressPaint);  //根据进度画圆弧
                break;
            }
            default:
                break;
        }

    }


    public int getMax() {
        return max;
    }

    /**
     * 设置进度的最大值
     *
     * @param max
     */
    public void setMax(int max) {
        if (max < 0) {
            throw new IllegalArgumentException("max not less than 0");
        }
        this.max = max;
    }

    /**
     * 获取进度.需要同步
     *
     * @return
     */
    public synchronized int getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        if (progress < 0) {
            throw new IllegalArgumentException("progress not less than 0");
        }
        if (progress > max) {
            progress = max;
        }
        if (progress <= max) {
            this.progress = progress;
            postInvalidate();
        }

    }


    public int getCricleColor() {
        return roundColor;
    }

    public void setCricleColor(int cricleColor) {
        this.roundColor = cricleColor;
    }

    public int getCricleProgressColor() {
        return roundProgressColor;
    }

    public void setCricleProgressColor(int cricleProgressColor) {
        this.roundProgressColor = cricleProgressColor;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public float getRoundWidth() {
        return roundWidth;
    }

    public void setRoundWidth(float roundWidth) {
        this.roundWidth = roundWidth;
    }

}