package com.example.app5libbase.login.view;


import com.example.app5libbase.base.BaseView;
import com.haier.cellarette.libmvp.mvp.IView;

/**
 * ResetPswView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface ResetPswViews extends IView {
    void changePswSucced();
}
