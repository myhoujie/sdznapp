package com.example.app5libbase.msg;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.controller.UserController;
import com.example.app5libpublic.event.TiWenEvent;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class MqttService extends Service implements MqttCallback {
    public MqttService() {
    }
    private String queueName = String.valueOf(UserController.getLoginBean().getData().getUser().getId());
    ConnectionFactory factory = new ConnectionFactory();
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setupConnectionFactory();
        new Thread(new Runnable() {
            @Override
            public void run() {
                connectMqtt();
            }
        }).start();
        //rabbitmq
        new Thread(new Runnable() {
            @Override
            public void run() {
                basicConsume();
            }
        }).start();

        return START_NOT_STICKY;
    }
    /**
     * Rabbit配置
     */
    private void setupConnectionFactory() {
        factory.setUsername(BuildConfig3.RABBIT_NAME);
        factory.setPassword(BuildConfig3.RABBIT_PASSWORD);
        factory.setHost(BuildConfig3.RABBIT_MQ);
        factory.setPort(BuildConfig3.RABBIT_HOST);
        factory.setAutomaticRecoveryEnabled(true);// 设置连接恢复

    }
    private void connectMqtt() {
        if (SPUtils.getLoginBean()!=null&&SPUtils.getLoginBean().getData()!=null&&SPUtils.getLoginBean().getData().getUser()!=null) {
            MqttManager.getInstance(this).createConnect(Network.BASE_MQTT_URL);
            final String deviceID = AndroidUtil.getDeviceID( App2.get());
            final String accountId = String.valueOf(SPUtils.getLoginBean().getData().getUser().getAccountId());
            final String other_login_topic = "T-" + accountId + "-" + deviceID;
            MqttManager.getInstance(this).subscribe(other_login_topic, 1);
        }
    }


    @Override
    public void connectionLost(Throwable cause) {
        MqttManager.getInstance(this).reconnect();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        final String deviceID = AndroidUtil.getDeviceID( App2.get());
        final String accountId = String.valueOf(SPUtils.getLoginBean().getData().getUser().getAccountId());
        final String other_login_topic = "T-" + accountId + "-" + deviceID;

        if (!message.isRetained()) {
            if (topic.equals(other_login_topic)) {

//                onOtherLogin(new String(message.toString().getBytes(), "UTF-8"));
                onOtherLogin(message.toString());
            }
        } else {
            Log.e("收到老消息", "。。。。。");
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    private void onOtherLogin(String msg) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.OtherLoginTipActivity");
        intent.putExtra("other_login_msg", msg);//OtherLoginTipActivity.EXTRA_OTHER_LOGIN_MSG
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        try {
            MqttManager.getInstance(this).disConnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    /**
     * 收消息
     */
    private void basicConsume() {

        try {
            //连接
            Connection connection = factory.newConnection();
            //通道
            final Channel channel = connection.createChannel();
            Map<String, Object> queueArgs = new HashMap<String, Object>();
            //queueArgs.put("x-dead-letter-exchange", "refreshDispatcherDeadExchange");  //死信队列
            //queueArgs.put("x-message-ttl", 10000);     // 消息超时：让发布的message在队列中可以存活多长时间，以毫秒为单位。
            queueArgs.put("x-expires", 5000);          // 队列超时：当前的queue在指定的时间内，没有消费者订阅就会被删除，以毫秒为单位。
            //queueArgs.put("x-max-length", 100);        // 队列最大长度：当超过了这个大小的时候，会删除之前最早插入的消息为本次的留出空间。
            //queueArgs.put("x-queue-mode", "lazy");     //延迟加载：queue的信息尽可能的都保存在磁盘上，仅在有消费者订阅的时候才会加载到RAM中。

            //3.声明队列。-将队列参数传到队列 （队列名字，是否持久化，是否排外，是否自动清理，参数）
            channel.queueDeclare(queueName, false, false, true, queueArgs);
            //实现Consumer的最简单方法是将便捷类DefaultConsumer子类化。可以在basicConsume 调用上传递此子类的对象以设置订阅：
            channel.basicConsume(queueName, false, "administrator", new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    super.handleDelivery(consumerTag, envelope, properties, body);


                    String rountingKey = envelope.getRoutingKey();
                    String contentType = properties.getContentType();
                    String msg = new String(body);
                    long deliveryTag = envelope.getDeliveryTag();

                    android.util.Log.e("TAG", rountingKey + "：rountingKey");
                    android.util.Log.e("TAG", contentType + "：contentType");
                    android.util.Log.e("TAG", msg + "：msg");
                    android.util.Log.e("TAG", deliveryTag + "：deliveryTag");
                    android.util.Log.e("TAG", "isRede" + ""+envelope.isRedeliver());

                    getIntentMsf(msg, channel, deliveryTag);
//                    channel.basicAck(deliveryTag, false);


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }

    private void getIntentMsf(String msg, Channel channel, long deliveryTag) {
        String mName;
        int mId;
        int type; // 1 抢答      2  随机
        if(!msg.isEmpty()){
            JSONObject object = null;

            try {
                object = new JSONObject(msg);
                mName = object.getString("name");
                mId = object.getInt("id");
                if ("抢答状态".equals(mName)){
                    EventBus.getDefault().post(new TiWenEvent(mId));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                try {
                    channel.basicAck(deliveryTag, false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }

}
