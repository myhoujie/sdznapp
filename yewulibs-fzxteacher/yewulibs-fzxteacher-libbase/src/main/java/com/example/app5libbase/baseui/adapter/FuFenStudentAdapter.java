package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5libbase.R;
import com.example.app5libbase.util.CircleTransform;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/20.
 */
public class FuFenStudentAdapter extends BaseRcvAdapter<ClassGroupingVo.DataBean> {
    public FuFenStudentAdapter(Context context, List<ClassGroupingVo.DataBean> mList) {
        super(context, R.layout.item_student, mList);
    }

    public void setData(List<ClassGroupingVo.DataBean> list) {
        if (list == null) {
            return;
        }
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public void convert(BaseViewHolder holder, int position, ClassGroupingVo.DataBean dataBean) {
        if (dataBean.getStudentId() == -1) {
            holder.setVisible(R.id.iv_head, false);
            holder.setVisible(R.id.tv_head, true);
            holder.setText(R.id.tv_name, dataBean.getStudentName());
        } else {
            holder.setVisible(R.id.iv_head, true);
            holder.setVisible(R.id.tv_head, false);
            holder.setText(R.id.tv_name, dataBean.getStudentName());
            ImageView iv = holder.getView(R.id.iv_head);
            Glide.with(context).load(dataBean.getPhoto())
                    .apply(new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(context)))
                    .into(iv);
        }
        if (dataBean.isChecked()) {
            holder.getView(R.id.fl_head).setSelected(true);
            holder.getView(R.id.tv_name).setSelected(true);
        } else {
            holder.getView(R.id.fl_head).setSelected(false);
            holder.getView(R.id.tv_name).setSelected(false);
        }
    }

    @Override
    protected void setListener(ViewGroup parent, final BaseViewHolder holder, int viewType) {
        if (!isEnabled(viewType)) return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    int position = getPosition(holder);
                    if (onItemClickListener instanceof OnStudentClickListener) {
                        ((OnStudentClickListener) onItemClickListener).onStudentClick(mList.get(position));
                    } else {
                        onItemClickListener.onItemClick(v, position);
                    }
                }
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (onLongClickListener != null) {
                    int position = getPosition(holder);
                    onLongClickListener.onItemLongClick(v, position);
                    return true;
                }
                return false;
            }
        });
    }

    public static abstract class OnStudentClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(View view, int position) {
        }

        public abstract void onStudentClick(ClassGroupingVo.DataBean bean);
    }
}
