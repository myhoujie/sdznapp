package com.example.app5libbase.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.bean.VersionInfoBean;
import com.sdzn.fzx.teacher.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.teacher.presenter.ShouyePresenter;
import com.sdzn.fzx.teacher.view.CheckverionViews;
import com.sdzn.fzx.teacher.view.ShouyeViews;


public class UpdataCommonservices1 extends Service implements CheckverionViews, ShouyeViews {

    public static final String HUIBEN_READINGTIME_ACTION = "HUIBEN_READINGTIME_ACTION";
    CheckverionFzxPresenter checkverionFzxPresenter;
    ShouyePresenter shouyePresenter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MsgBinder();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
//        ToastUtils.showLong(versionInfoBean.getId() + "请求成功");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    public void onIndexSuccess(ShouyeBean shouyeBean) {
        ToastUtils.showLong(shouyeBean.getLlAutonomyStudy() + "请求成功");
    }

    @Override
    public void onIndexNodata(String msg) {

    }

    @Override
    public void onIndexFail(String msg) {

    }

    public class MsgBinder extends Binder {
        public UpdataCommonservices1 getService() {
            return UpdataCommonservices1.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);

    }

    //启动service
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            String action = intent.getAction();
            if (action.equals(HUIBEN_READINGTIME_ACTION)) {
                checkverionFzxPresenter.checkVerion("2", "0");

//                shouyePresenter.queryShouye();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        checkverionFzxPresenter.onDestory();
        super.onDestroy();
    }
}
