package com.example.app5libbase.ai;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.AddTabPopupView;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.ai.pop.MoveGroupPopupView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.sdzn.fzx.teacher.vo.CorrectVo;

import java.util.ArrayList;
import java.util.List;

public class PopupActivity extends AppCompatActivity {
    CustomDrawerPopupView customDrawerPopupView;//右侧抽屉
    private int checkedPos = 1;//默认选中
    MoveGroupPopupView moveGroupPopupView;//分组
    AddTabPopupView addTabPopupView;//标签


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);
//        customDrawerPopupView = new CustomDrawerPopupView(this);
        customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("点击个人中心");
            }
        });
        moveGroupPopupView = new MoveGroupPopupView(this);
        addTabPopupView = new AddTabPopupView(this);

        /*中间文字*/
        findViewById(R.id.iv6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .asConfirm("移除提醒", "确认移除该班级吗？解除后无法进行该班级的管理操作。",
                                "取消", "确定",
                                new OnConfirmListener() {
                                    @Override
                                    public void onConfirm() {
                                        ToastUtils.showShort("click confirm");
                                    }
                                }, null, false, R.layout.popup_center_text) //最后一个参数绑定已有布局
                        .show();

            }
        });

        /*添加标签*/
        findViewById(R.id.iv5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(addTabPopupView)
                        .show();

            }
        });

        /*移动分组*/
        findViewById(R.id.iv4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(moveGroupPopupView)
                        .show();

            }
        });

        /*侧滑*/
        findViewById(R.id.iv3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(customDrawerPopupView)
                        .show();

            }
        });


        /*中间点击     asAttachList1 传 checkedPos  有默认选中*/
        findViewById(R.id.iv2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .hasShadowBg(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .offsetY(20)
                        .offsetX(-120)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList1(new String[]{"班级", "班级2", "班级3", "班级4"},
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        checkedPos = position;
                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    }
                                }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                        .show();
            }
        });

        /*右侧点击      asAttachList无默认选中   */
        findViewById(R.id.iv1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .hasShadowBg(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .offsetY(20)
                        .offsetX(40)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList(new String[]{"移除班级", "解除绑定"},
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    }
                                }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item)
                        .show();
            }


        });

        findViewById(R.id.title_back_ly).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 集合转 String[]数组
     */
    private String[] getStringData(List<String> list) {
        String[] strArray = list.toArray(new String[list.size()]);
        return strArray;
    }

    private String[] getStringBean(CorrectVo correctVo) {
        List<String> list = new ArrayList<>();
        for (CorrectVo.DataBean.CorrectListBean correctListBean : correctVo.getData().getCorrectList()) {
            list.add(String.valueOf(correctListBean.getRate()));
        }

        String[] strArray = list.toArray(new String[list.size()]);
        return strArray;
    }


}
