package com.example.app5libbase.listener;

import android.content.Context;

import com.tencent.tinker.lib.reporter.DefaultLoadReporter;

import java.io.File;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/29
 * 修改单号：
 * 修改内容:
 */
public class MyDefaultLoadReporter extends DefaultLoadReporter {


    public MyDefaultLoadReporter(Context context) {
        super(context);
    }

    @Override
    public void onLoadPatchListenerReceiveFail(File patchFile, int errorCode) {
       // EventBus.getDefault().post(new TinkerResult(false, 0));
        super.onLoadPatchListenerReceiveFail(patchFile, errorCode);
    }
}
