package com.example.app5libbase.views.graffiti;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.example.app5libbase.views.graffiti.core.IGraffiti;


/**
 * 涂鸦轨迹
 * Created by huangziwei on 2017/3/16.
 */

public class GraffitiPath extends GraffitiItemBase {
    private Path mPath; // 画笔的路径

    private Paint mPaint = new Paint();

    public GraffitiPath(IGraffiti graffiti) {
        super(graffiti);
    }

    public GraffitiPath(IGraffiti graffiti, GraffitiPaintAttrs attrs) {
        super(graffiti, attrs);
    }

    public void updatePath(Path path) {
        this.mPath = path;
    }

    public Path getPath() {
        return mPath;
    }

    public static GraffitiPath toPath(IGraffiti graffiti, Path p) {
        GraffitiPath path = new GraffitiPath(graffiti);
        path.setPen(graffiti.getPen().copy());
        path.setShape(graffiti.getShape().copy());
        path.setSize(graffiti.getSize());
        path.setColor(graffiti.getColor().copy());

        path.mPath = p;
        return path;
    }

    @Override
    protected void doDraw(Canvas canvas) {
        mPaint.reset();
        mPaint.setStrokeWidth(getSize());
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setAntiAlias(true);

        getColor().config(this, mPaint);
        getShape().draw(canvas, this, mPaint);
    }


}

