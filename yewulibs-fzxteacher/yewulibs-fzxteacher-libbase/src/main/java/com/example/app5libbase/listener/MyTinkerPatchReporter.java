package com.example.app5libbase.listener;

import android.content.Context;


import com.example.app5libpublic.event.TinkerResult;
import com.tencent.tinker.lib.reporter.DefaultPatchReporter;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * @author Created by Reisen on 2019/2/27.
 */
public class MyTinkerPatchReporter extends DefaultPatchReporter {

    public MyTinkerPatchReporter(Context context) {
        super(context);
    }

    @Override
    public void onPatchResult(File patchFile, boolean success, long cost) {
        EventBus.getDefault().post(new TinkerResult(success,cost));
        super.onPatchResult(patchFile, success, cost);
    }

}
