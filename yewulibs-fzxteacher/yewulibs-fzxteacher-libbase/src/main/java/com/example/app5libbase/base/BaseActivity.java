package com.example.app5libbase.base;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.app5libbase.R;
import com.example.app5libbase.util.ProgressDialogManager;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

import me.jessyan.autosize.AutoSizeCompat;

//import cn.hzw.doodle.DoodleParams;

/**
 * 描述：所有Activity的基类，任何Activity必须继承它
 * －
 * import android.app.Activity;
 * import android.os.Build;
 * import android.os.Bundle;
 * <p>
 * <p>
 * import android.support.v4.app.FragmentActivity;
 * import android.view.WindowManager;
 * <p>
 * import com.sdzn.tank.teacher.app.App2.get();
 * import com.sdzn.tank.teacher.ui.activity.ActivityManager;
 * import com.sdzn.tank.teacher.utils.ProgressDialogManager;
 * 创建人：zhangchao
 * 创建时间：17/3/20
 */
public abstract class BaseActivity extends AppCompatActivity {

    //    protected Application appContext;
    protected BaseActivity activity;
    protected float density;
    protected ProgressDialogManager mManager;
    private long mCurrentMs = System.currentTimeMillis();
//    protected ToolButtonPop toolButtonPop;

    public BaseActivity() {
    }

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.color_FB6215), 0);
//        StatusBarUtil.setLightMode(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            //透明状态栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
        activity = this;
//        appContext = App2.get();

        density = getResources().getDisplayMetrics().density;
        mManager = new ProgressDialogManager(this);
        ActivityManager.addLiveActivity(this);

        //悬浮窗
//        initFloatWindow();
        TimeBaseUtil.getInstance().init(activity);

    }

    /**
     * 初始化组件
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    @Override
    protected void onStart() {
        super.onStart();
        ActivityManager.addVisibleActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityManager.removeVisibleActivity(this);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityManager.addForegroundActivity(this);
        TimeBaseUtil.getInstance().resume(activity);

    }

    @Override
    protected void onPause() {
        super.onPause();
        TimeBaseUtil.getInstance().dismissCountDownPop();
        ActivityManager.removeForegroundActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimeBaseUtil.getInstance().destroy();
        ActivityManager.removeLiveActivity(this);
    }

    /**
     * activity是否显示 initpop    false即 不显示, 默认是显示
     */

    public void setReturnPop(boolean isShowPop) {
        TimeBaseUtil.getInstance().setReturnPop(isShowPop);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        TimeBaseUtil.getInstance().activityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}