package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActBean;
import com.example.app5libbase.util.CircleTransform;
import com.lxj.xpopup.core.DrawerPopupView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean1;
import com.sdzn.fzx.teacher.presenter.CehuaPresenter;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.view.CehuaViews;
import com.sdzn.fzx.teacher.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 自定义抽屉弹窗
 * <p>
 * //通过设置topMargin，可以让Drawer弹窗进行局部阴影展示
 * //        ViewGroup.MarginLayoutParams params = (MarginLayoutParams) getPopupContentView().getLayoutParams();
 * //        params.topMargin = 450;
 */
public class CustomDrawerPopupView extends DrawerPopupView implements CehuaViews {
    private RecyclerView mRecyclerView;
    private List<GRZXBaseRecActBean> mList;
    private GRZXBaseRecActAdapter mAdapter;
    GrzxNextCallBack grzxNextCallBack;
    private ImageView ivHead;
    private TextView tvName;


    private long mCurrentMs = System.currentTimeMillis();
    CehuaPresenter cehuaPresenter;

    public CustomDrawerPopupView(@NonNull Context context) {
        super(context);
    }

    public CustomDrawerPopupView(@NonNull Context context, GrzxNextCallBack mGrzxNextCallBack) {
        super(context);
        this.grzxNextCallBack = mGrzxNextCallBack;
    }


    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_right_drawer;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        cehuaPresenter = new CehuaPresenter();
        cehuaPresenter.onCreate(this);
        cehuaPresenter.queryCehua();
        mList = new ArrayList<>();
        findview();


    }

    private void findview() {
        mRecyclerView = findViewById(R.id.rv_list);
        ivHead = findViewById(R.id.iv_head);
        tvName = findViewById(R.id.tv_name);
        LoginBean data = com.sdzn.fzx.teacher.utils.SPUtils.getLoginBean();
        if (data == null) {
            return;
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getRealName())) {
            tvName.setText(data.getData().getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getData().getUser().getPhoto())) {
            RequestOptions error = new RequestOptions().error(R.mipmap.tx_img).transform(new CircleTransform(getContext())).placeholder(R.mipmap.tx_img);
            Glide.with(this).load(data.getData().getUser().getPhoto()).apply(error).into(ivHead);
        }
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));

    }

    private void onclicklistener() {
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                GRZXBaseRecActBean addressBean = mList.get(position);
                int i = view.getId();
                if (i == R.id.iv_item) {
                    if (!addressBean.getGrzxRecActBean2().getAct().isEmpty()) {
                        if (addressBean.getGrzxRecActBean2().getName().equals("智囊学堂")) {
                            if (AppUtils.isAppInstalled("com.sdzn.pkt.teacher.hd")) {
                                String name = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_USER_NUM, ""));
                                String pwd = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_PAS_CHECK, ""));
                                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.teacher.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd));
                                getContext().startActivity(intent);
                            } else {
                                ToastUtils.showLong("请先安装智囊学堂老师端APP");
                            }
                        } else if (addressBean.getGrzxRecActBean2().getName().equals("百度百科")) {
                            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebBaiduEncyclopediaActivity");
                            intent.putExtra("url_key", "https://baike.baidu.com/vbaike#");
                            getContext().startActivity(intent);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(addressBean.getGrzxRecActBean2().getAct()));
                            getContext().startActivity(intent);
                        }
                    }
                    dismiss();
                }
            }
        });

        findViewById(R.id.btn11).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
//                onDestroy();
            }
        });
        findViewById(R.id.rl_data).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//                intent.putExtra("show", 0);
                getContext().startActivity(intent);
            }
        });
    }

    public void donetwork() {
        mAdapter = new GRZXBaseRecActAdapter(mList);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        mAdapter.setNotDoAnimationCount(3);
        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int type = mList.get(position).type;
                if (type == GRZXBaseRecActBean.style1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        onclicklistener();
    }

    @Override
    public void onCehuaSuccess(GrzxRecActBean grzxRecActBean) {
        for (int i = 0; i < grzxRecActBean.getList().size(); i++) {
            GrzxRecActBean1 grzxRecActBean1 = grzxRecActBean.getList().get(i);
            for (int j = 0; j < grzxRecActBean1.getList().size(); j++) {
                if (j == 0) {
                    GrzxRecActBean1 classCommonbean = new GrzxRecActBean1();
                    classCommonbean.setTitle(grzxRecActBean1.getTitle());
                    classCommonbean.setList(grzxRecActBean1.getList());
                    mList.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, classCommonbean));
                }
                mList.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, grzxRecActBean1.getList().get(j)));
            }
        }
        donetwork();
    }

    @Override
    public void onCehuaNodata(String msg) {

    }

    @Override
    public void onCehuaFail(String msg) {

    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

    public interface GrzxNextCallBack {
        void toGrzxNextClick();
    }


    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    @Override
    public void onDestroy() {
        cehuaPresenter.onDestory();
        super.onDestroy();
    }
}