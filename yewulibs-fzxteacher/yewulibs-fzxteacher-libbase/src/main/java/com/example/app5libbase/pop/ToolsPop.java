package com.example.app5libbase.pop;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.service.FadeService;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.util.AndroidTeacherUtils;
import com.example.app5libpublic.event.OnScreenCaptureEvent;
import com.example.app5libbase.util.HiddenAnimUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.example.app5libbase.views.CustomDialog;

import net.ossrs.yasea.SimplePublisher;

import org.greenrobot.eventbus.EventBus;

/**
 * 工具弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class ToolsPop extends PopupWindow implements View.OnClickListener {
    private View viewBg;
    private ImageView ivToolOpen;
    private LinearLayout llTools;
    private LinearLayout llTuya;
    private TextView tvTuya;
    private LinearLayout llBaiban;
    private TextView tvBaiban;
    private LinearLayout llTouping;
    private ImageView ivTouping;
    private TextView tvTouping;
    private LinearLayout llDaojishi;
    private TextView tvDaojishi;
    private LinearLayout llXiaozu;
    private TextView tvXiaozu;

    private Activity activity;

    private AnimatorListenerAdapter animatorListenerAdapter;

    private String action;

    private ToolScreenPop toolScreenPop;

    public ToolsPop(final Activity activity) {
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tools, null);
        viewBg = (View) contentView.findViewById(R.id.viewBg);
        ivToolOpen = (ImageView) contentView.findViewById(R.id.ivToolOpen);
        llTools = (LinearLayout) contentView.findViewById(R.id.llTools);
        llTuya = (LinearLayout) contentView.findViewById(R.id.llTuya);
        tvTuya = (TextView) contentView.findViewById(R.id.tvTuya);
        llBaiban = (LinearLayout) contentView.findViewById(R.id.llBaiban);
        tvBaiban = (TextView) contentView.findViewById(R.id.tvBaiban);
        llTouping = (LinearLayout) contentView.findViewById(R.id.llTouping);
        ivTouping = (ImageView) contentView.findViewById(R.id.ivTouping);
        tvTouping = (TextView) contentView.findViewById(R.id.tvTouping);
        llDaojishi = (LinearLayout) contentView.findViewById(R.id.llDaojishi);
        tvDaojishi = (TextView) contentView.findViewById(R.id.tvDaojishi);
        llXiaozu = (LinearLayout) contentView.findViewById(R.id.llXiaozu);
        tvXiaozu = (TextView) contentView.findViewById(R.id.tvXiaozu);

        llTuya.setOnClickListener(this);
        llBaiban.setOnClickListener(this);
        llTouping.setOnClickListener(this);
        llDaojishi.setOnClickListener(this);
        llXiaozu.setOnClickListener(this);

        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        viewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HiddenAnimUtils.newInstance(ToolsPop.this.activity, llTools, 280, animatorListenerAdapter).toggle();
            }
        });

        if (SimplePublisher.getInstance().isRecording()) {
            ivTouping.setBackground(activity.getResources().getDrawable(R.mipmap.toupinging_icon));
        } else {
            ivTouping.setBackground(activity.getResources().getDrawable(R.mipmap.touping_icon));
        }

        llTouping.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (SimplePublisher.getInstance().isRecording()) {
                    // TODO 弹出
                    if (toolScreenPop == null) {
                        toolScreenPop = new ToolScreenPop(activity, ToolsPop.this);
                    }
                    toolScreenPop.showPopupWindow(TimeBaseUtil.getInstance().getContentView(activity));
                }
                return true;
            }
        });

        animatorListenerAdapter = new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                llTools.setVisibility(View.GONE);
                dismiss();
            }
        };

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                if ("tuya".equals(action)) {
                    // 涂鸦
                    EventBus.getDefault().post(new OnScreenCaptureEvent());
                    action = "";
                }
                llTools.setVisibility(View.GONE);
            }
        });

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivToolOpen.getLayoutParams();
        if (checkDeviceHasNavigationBar()) {
            layoutParams.setMargins(UiUtils.dp2px(11), 0, 0, UiUtils.dp2px(10) + AndroidTeacherUtils.getDaoHangHeight(activity));
        } else {
            layoutParams.setMargins(UiUtils.dp2px(11), 0, 0, UiUtils.dp2px(10));
        }
        llTools.setVisibility(View.GONE);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.llTuya) {// 截屏
            action = "tuya";
        } else if (id == R.id.llBaiban) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PaletteActivity");
            intent.putExtra("type", "baiban");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
        } else if (id == R.id.llTouping) {
            if (SimplePublisher.getInstance().isRecording()) {
                SimplePublisher.getInstance().stop();
                TimeBaseUtil.getInstance().mMediaProjection = null;
                ivTouping.setBackground(activity.getResources().getDrawable(R.mipmap.touping_icon));
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (Settings.canDrawOverlays(activity)) {
                        Log.i("------------------L164");
                        Intent intent2 = new Intent(activity, FadeService.class);
                        activity.startService(intent2);
                    } else {
                        //若没有权限，提示获取.
                        Log.i("------------------L169");
                        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
                        builder.setTitle("权限提示");
                        builder.setMessage("开启悬浮窗权限可提升投屏体验，是否去开启?");
                        builder.setPositive("去开启", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent3 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                                Toast.makeText(activity, "需要取得权限以使用悬浮窗", Toast.LENGTH_SHORT).show();
                                activity.startActivity(intent3);
                                dialogInterface.dismiss();
                            }
                        });
                        builder.setNegative("放弃", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (ActivityManager.getForegroundActivity() != null) {
                                    TimeBaseUtil.getInstance().customScan();
                                }
                                HiddenAnimUtils.newInstance(activity, llTools, 280, animatorListenerAdapter).toggle();
                                dialogInterface.dismiss();
                            }
                        });
                        CustomDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }
                } else {
                    //SDK在23以下，不用管.
                    Log.i("------------------L196");
                    Intent intent1 = new Intent(activity, FadeService.class);
                    Log.i("------------------L197");
                    activity.startService(intent1);
                }

                // 扫描二维码
                if (ActivityManager.getForegroundActivity() != null) {
                    TimeBaseUtil.getInstance().customScan();
                }
            }
        } else if (id == R.id.llDaojishi) {
//            showCountDownPop();
        } else if (id == R.id.llXiaozu) {
            Intent scoreIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ScoreActivity");
            activity.startActivity(scoreIntent);
        }
        HiddenAnimUtils.newInstance(activity, llTools, 280, animatorListenerAdapter).toggle();
    }



    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            this.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
            HiddenAnimUtils.newInstance(activity, llTools, 280, animatorListenerAdapter).toggle();
            setScreen(SimplePublisher.getInstance().isRecording());
        } else {
            this.dismiss();
        }
    }

    public void dismissToolsPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    public void setScreen(boolean isRecording) {
        if (ivTouping != null) {
            if (isRecording) {
                ivTouping.setBackground(activity.getResources().getDrawable(R.mipmap.toupinging_icon));
            } else {
                ivTouping.setBackground(activity.getResources().getDrawable(R.mipmap.touping_icon));
            }
        }
    }

    //判断屏幕下方是否有虚拟按键
    private boolean checkDeviceHasNavigationBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            Point realSize = new Point();
            display.getSize(size);
            display.getRealSize(realSize);
            return realSize.y != size.y;
        } else {
            boolean menu = ViewConfiguration.get(activity).hasPermanentMenuKey();
            boolean back = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            return !menu && !back;
        }
    }


}