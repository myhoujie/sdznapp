package com.example.app5libbase.views.timer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app5libbase.R;


/**
 * 倒计时 --赵森
 */
public class TimerAlertView extends Dialog {

    private Context context;
    private CircleTextProgressbar mTvRedFrame;
    private ClickListenerInterfaces clickListenerInterface;
    public TimerAlertView(Context context) {
        super(context, R.style.UIAlertViewStyle);
        this.context = context;

    }

    public interface ClickListenerInterfaces {
        void doCancel();

        void doNext();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        inite();
    }

    public void inite() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_alert_timer, null);
        setContentView(view);


        TextView tvLeft = (TextView) view.findViewById(R.id.iv_close);
        ImageView im_coupon = (ImageView) view.findViewById(R.id.im_coupon);

        tvLeft.setOnClickListener(new clickListener());
        im_coupon.setOnClickListener(new clickListener());

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();

        lp.width = (int) (d.widthPixels * 0.8);
        dialogWindow.setAttributes(lp);

        //进度
        // 红色边框进度条。
        mTvRedFrame = (CircleTextProgressbar) findViewById(R.id.tv_red_frame);
        mTvRedFrame.setOutLineColor(Color.parseColor("#eeeeee"));
        mTvRedFrame.setOutLineWidth(6);

        mTvRedFrame.setProgressColor(Color.WHITE);
        mTvRedFrame.setProgressLineWidth(6);//写入宽度。
        mTvRedFrame.setTimeMillis(60*1000);// 把倒计时时间改长一点。
        mTvRedFrame.start();

        mTvRedFrame.setCountdownProgressListener(2, new CircleTextProgressbar.OnCountdownProgressListener() {
            @Override
            public void onProgress(int what, int progress) {
                mTvRedFrame.setText(progress+"s");
            }
        });

    }

    public void setClicklistener(ClickListenerInterfaces clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    private class clickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            int id = v.getId();
            if (id == R.id.iv_close) {
                clickListenerInterface.doCancel();
            } else if (id == R.id.im_coupon) {
                clickListenerInterface.doNext();
            }
        }
    }

}
