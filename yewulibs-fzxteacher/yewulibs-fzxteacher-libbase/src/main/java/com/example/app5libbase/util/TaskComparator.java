package com.example.app5libbase.util;


import com.sdzn.fzx.teacher.vo.LessonVo;

import java.util.Comparator;

/**
 * 任务排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class TaskComparator implements Comparator<Object> {
    @Override
    public int compare(Object t1, Object t2) {
        long startTime1 = ((LessonVo.DataBean)t1).getTimeUpdate();
        long startTime2 = ((LessonVo.DataBean)t2).getTimeUpdate();
        if (startTime1 > startTime2) {
            return -1;
        } else if (startTime1 < startTime2) {
            return 1;
        } else {
            return 0;
        }
    }
}
