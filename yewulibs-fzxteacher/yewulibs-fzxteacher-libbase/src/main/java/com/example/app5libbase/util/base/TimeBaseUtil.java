package com.example.app5libbase.util.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.SoundPool;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.AppUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.service.TimerHeadService;
import com.example.app5libbase.service.TimerService;
import com.example.app5libbase.baseui.activity.CustomScanActivity;
import com.example.app5libbase.baseui.activity.ScreenConfigActivity;
import com.example.app5libbase.baseui.activity.TiWenActivity;
import com.example.app5libbase.baseui.activity.tools.DoodleActivity;
import com.example.app5libbase.pop.CountDownOverPop;
import com.example.app5libbase.pop.CountDownPop;
import com.example.app5libbase.util.AndroidTeacherUtils;
import com.example.app5libbase.util.ServiceUtil;
import com.example.app5libbase.views.CustomDialog;
import com.example.app5libbase.views.CustomHeadTimerView;
import com.example.app5libbase.views.timer.TimerAlertView;
import com.example.app5libpublic.constant.Constant;
import com.example.app5libpublic.event.Event;
import com.example.app5libpublic.event.LessonDetailsGoClassEvent;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.qiniu.android.utils.StringUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.UiUtils;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import net.ossrs.yasea.SimplePublisher;
import net.ossrs.yasea.SimpleRtmpListener;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import cn.forward.androids.utils.ImageUtils;
import cn.forward.androids.utils.Util;
import cn.hzw.doodle.DoodleParams;
import cn.hzw.doodle.DoodleView;
import okhttp3.Call;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;
import static com.blankj.utilcode.util.ServiceUtils.unbindService;
import static com.blankj.utilcode.util.ViewUtils.runOnUiThread;

public class TimeBaseUtil {

    private static TimeBaseUtil timeBaseUtil = null;

    protected boolean hasOnCreate;

    protected boolean isReturnPop;

    private View contentView = null;


    private TimerService timerService;
    private ServiceConnection timerConn;
    protected CountDownPop countDownPop;//计时

    protected CustomHeadTimerView customHeadTimer = null;
    private TimerHeadService timerHeadService;//头部计时
    private ServiceConnection timerHeadConn;

    private int reclenPub = -1;//提前下课时间
    private TimerAlertView alertView;//60s 弹出窗
    private Activity activity;

    private MyHandler myHandler;
    public static final int REQ_CODE_SELECT_IMAGE = 100;
    public static final int REQ_CODE_DOODLE = 101;

    //applike
    public static MediaProjection mMediaProjection;


    public TimeBaseUtil() {
        myHandler = new MyHandler();
    }


    public static TimeBaseUtil getInstance() {
        synchronized (TimeBaseUtil.class) {
            if (timeBaseUtil == null) {
                timeBaseUtil = new TimeBaseUtil();
            }
        }
        return timeBaseUtil;
    }

    /**
     * 多个页面  activity是否显示 initpop    false即 不显示, 默认是显示
     */

    public void setReturnPop(boolean isReturn) {
        isReturnPop = isReturn;
    }

    public void init(Activity mActivity) {
        this.activity = mActivity;
        mMediaProjectionManager = (MediaProjectionManager) activity.getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        hasOnCreate = false;
        isReturnPop=true;
    }

    /**
     * onResume
     */

    public void resume(Activity mActivity) {
        this.activity = mActivity;
        contentView = getContentView(activity);

        if (hasOnCreate) {
            initPop(contentView);
        } else {
            addTools(contentView);
        }

        if (!Constant.getShowCountDown()) {
            dismissCountDownPop();
        }


        if (!Constant.getShowHeadCountDown()) {// false
            dismissHeadCountView();
        }

        hasOnCreate = true;
    }


    /**
     * 绑定倒计时服务
     */
    private void bindTimerService(final int time) {
        if (timerConn != null && timerService != null) {
            timerService.setTime(time);
            startTimer();
            return;
        }
        Intent service = new Intent(activity, TimerService.class);
        timerConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timerService = timerBinder.getService();
                timerService.setTime(time);
                startTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(App2.get(), TimerService.class.getName())) {
            App2.get().startService(service);
        }
        App2.get().bindService(service, timerConn, Context.BIND_AUTO_CREATE);
    }

    /**
     * 绑定倒计时服务
     */
    private void bindTimerService() {
        if (timerConn != null) {
            return;
        }
        Intent service = new Intent(activity, TimerService.class);
        timerConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timerService = timerBinder.getService();
                startTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(App2.get(), TimerService.class.getName())) {
            App2.get().startService(service);
        }
        App2.get().bindService(service, timerConn, Context.BIND_AUTO_CREATE);
    }

    /**
     * 开始计时
     */
    private void startTimer() {
        if (timerService != null) {
            timerService.setHandler(myHandler);
            if (timerService != null) {
                timerService.startTimer();
            }
        }
    }

    private class MyHandler extends Handler {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TimerService.MSG_TIMER_RUNNING:
                    int recLen = (int) msg.obj;
                    if (countDownPop != null) {
                        countDownPop.setTime(recLen);
                    }
                    break;
                case TimerService.MSG_TIMER_FINISHED:
                    dismissCountDownPop();
                    if (!Constant.getShowCountDown()){
                        break;
                    }
                    // 显示时间到
                    CountDownOverPop countDownOverPop = new CountDownOverPop(activity);
                    countDownOverPop.showPopupWindow(getContentView(activity));
                    // 播放声音
//                    SoundPool sp=new SoundPool(10, AudioManager.STREAM_MUSIC,0);
                    SoundPool.Builder sb = new SoundPool.Builder();
                    sb.setMaxStreams(10);
                    AudioAttributes.Builder audioAttributesBuilder = new AudioAttributes.Builder();
                    audioAttributesBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
                    AudioAttributes aa = audioAttributesBuilder.build();
                    sb.setAudioAttributes(aa);//默认是media类型，其他的可以看看源码。都有
                    final SoundPool sp = sb.build();
                    final int count_over = sp.load(activity, R.raw.count_over, 1);
                    sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int i, int i1) {
                            sp.play(count_over, 1, 1, 1, 0, 1);
                        }
                    });
                    break;


                case TimerHeadService.MSG_TIMER_RUNNING_HEAD://倒计时中-- 头部
                    int recLenHead = (int) msg.obj;
                    reclenPub = recLenHead;

                    if (customHeadTimer != null) {//
                        customHeadTimer.setTime(recLenHead);
                        if (recLenHead == 60) {
                            alertView.setCancelable(false);
                            alertView.show();
                            alertView.setClicklistener(new TimerAlertView.ClickListenerInterfaces() {
                                @Override
                                public void doCancel() {//我知道了--后续操作
                                    alertView.dismiss();

                                }

                                @Override
                                public void doNext() {

                                }
                            });
                        }
                    }
                    break;
                case TimerHeadService.MSG_TIMER_FINISHED_HEAD://结束-- 头部
                    dismissHeadCountView();
                    if (alertView.isShowing() && alertView != null) {
                        alertView.dismiss();
                    }
                    if (activity instanceof TiWenActivity) {
                        activity.finish();
                    }
                    if (!SPUtils.get(activity, SPUtils.CLASS_ID, "").equals("")) {

                        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.OutClassActivity");
                        startActivity(intent);
                        getBehavior(2, false);
                    }
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }

    }

    private void initPop(View contentView) {
//        if (activity instanceof WelcomeActivity
//                || activity instanceof ForgetPswActivity
//                || activity instanceof LoginActivity
//                || activity instanceof CustomScanActivity
//                || activity instanceof PaletteActivity
//                || activity instanceof ScoreActivity
//                || activity instanceof CorrectActivity
//                || activity instanceof PlayerActivity) {
//
//            return;
//        }

        if (!isReturnPop) {
            return;
        }
//        if (toolButtonPop == null) {oolButtonPop
//            //toolButtonPop = new ToolButtonPop(activity);
//            //toolButtonPop.showPopupWindow(contentView);
//        }

        if (Constant.getShowCountDown()) {
            showCountDownPop();
        }

        //显示--头部-->初始化
        if (Constant.getShowHeadCountDown()) {
            showHeadCountView();
        }
    }

    public void showCountDownPop(int time) {
        initCountDownPop();
        bindTimerService(time);
    }

    private void initCountDownPop() {
        if (countDownPop == null) {
            countDownPop = new CountDownPop(activity);
        }
        try {
            countDownPop.showPopupWindow(getContentView(activity));
        } catch (Exception e) {

        }

    }


    /**
     * 显示倒计时窗口(记时弹出框)
     */
    private void showCountDownPop() {
        initCountDownPop();
        bindTimerService();
    }

    /**
     * 移除倒计时窗口
     */
    public void dismissCountDownPop() {
        if (countDownPop != null) {
            countDownPop.dismissPop();
            countDownPop = null;
        }
        if (timerConn != null) {
            unbindService(timerConn);
            timerConn = null;
        }
    }

    /*
    destroy  销毁方法
     */
    public void destroy() {
        removeTools();
        if (myHandler != null) {
            myHandler.removeCallbacksAndMessages(null);
        }
    }

    /**
     * 显示工具栏
     *
     * @param contentView
     */
    protected void addTools(final View contentView) {
        if (contentView != null) {
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    initPop(contentView);
                    contentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    /**
     * 移除工具栏
     */
    protected void removeTools() {
//        if (toolButtonPop != null) {
//            toolButtonPop.dismissToolsPop();
//            toolButtonPop = null;
//        }
    }


    /**
     * 获取activity的ContentView
     *
     * @param context
     * @return
     */
    public View getContentView(Activity context) {
        return ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
    }


    public static final int REQUEST_MEDIA_PROJECTION_CAPTURE = 1001;
    public static final int REQUEST_MEDIA_PROJECTION_SCREEN = 1002;
    public static final int REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN = 11003;
    public MediaProjectionManager mMediaProjectionManager;
    private ImageReader imageReader;
    private String rtmpUrl;

    public void customScan() {
        new IntentIntegrator(activity)
                .setOrientationLocked(false)
                .setCaptureActivity(CustomScanActivity.class) // 设置自定义的activity是CustomActivity
                .initiateScan(); // 初始化扫描
    }

    /**
     * 截屏
     */
    public void getScreen() {
        if (mMediaProjection == null) {
            return;
        }
        imageReader = ImageReader.newInstance(UiUtils.getScreenW(App2.get()), UiUtils.getScreenH(App2.get()) + AndroidTeacherUtils.getDaoHangHeight(activity), 0x1, 1);
        final VirtualDisplay mVirtualDisplay = mMediaProjection.createVirtualDisplay("ScreenCapture",
                UiUtils.getScreenW(App2.get()), UiUtils.getScreenH(App2.get()) + AndroidTeacherUtils.getDaoHangHeight(activity), 1,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY,
                imageReader.getSurface(), null, null);
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startCapture();
              /*          mVirtualDisplay.release();
                        Intent intent = new Intent(activity, PaletteActivity.class);
                        intent.putExtra("type", "tuya");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);*/

                        // 涂鸦参数
                        DoodleParams params = new DoodleParams();
                        params.mIsFullScreen = true;
                        // 图片路径
                        try {
                            params.mImagePath = FileUtil.getResPath();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // 初始画笔大小
                        params.mPaintUnitSize = DoodleView.DEFAULT_SIZE;
                        // 画笔颜色
                        params.mPaintColor = Color.RED;
                        // 是否支持缩放item
                        params.mSupportScaleItem = true;
                        // 启动涂鸦页面
                        DoodleActivity.startActivityForResult(activity, params, REQ_CODE_DOODLE);

                    }
                });
            }
        }, 1000);
    }

    /**
     * 白板截屏
     */
    public void getBaiBANScreen() {
        if (mMediaProjection == null) {
            return;
        }
        imageReader = ImageReader.newInstance(UiUtils.getScreenW(App2.get()), UiUtils.getScreenH(App2.get()) + AndroidTeacherUtils.getDaoHangHeight(activity), 0x1, 1);
        final VirtualDisplay mVirtualDisplay = mMediaProjection.createVirtualDisplay("ScreenCapture",
                UiUtils.getScreenW(App2.get()), UiUtils.getScreenH(App2.get()) + AndroidTeacherUtils.getDaoHangHeight(activity), 1,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY,
                imageReader.getSurface(), null, null);
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                App2.get().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        startBaiCapture();
//
//                    }
//                });
            }
        }, 1000);
    }

    /**
     * 白板截屏   bitmap__
     */
    private void startBaiCapture() {
        Image image = null;
        while (image == null) {
            image = imageReader.acquireLatestImage();
        }
        int width = image.getWidth();
        int height = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height - AndroidTeacherUtils.getDaoHangHeight(activity));
        image.close();
        if (bitmap != null) {
            get(bitmap);
        }
    }

    /**
     * 白板截屏  保存&发布跳转
     */
    private void get(Bitmap bitmap) {
        File doodleFile = null;
        File file = null;
        String savePath = "";
        boolean isDir = false;
        if (TextUtils.isEmpty(savePath)) {
            File dcimFile = new File(Environment.getExternalStorageDirectory(), "DCIM");
            doodleFile = new File(dcimFile, "BaiBan");
            //　保存的路径
            file = new File(doodleFile, System.currentTimeMillis() + ".jpg");
        } else {
            if (isDir) {
                doodleFile = new File(savePath);
                //　保存的路径
                file = new File(doodleFile, System.currentTimeMillis() + ".jpg");
            } else {
                file = new File(savePath);
                doodleFile = file.getParentFile();
            }
        }
        doodleFile.mkdirs();

        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, outputStream);
            ImageUtils.addImage(App2.get().getContentResolver(), file.getAbsolutePath());
            // file.getAbsolutePath()
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PublishDoodleActivity");
            intent.putExtra("path", file.getAbsolutePath());
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            Util.closeQuietly(outputStream);
        }
    }

    /**
     * 获取截屏图片
     */
    private void startCapture() {
        Image image = null;
        while (image == null) {
            image = imageReader.acquireLatestImage();
        }
        int width = image.getWidth();
        int height = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height - AndroidTeacherUtils.getDaoHangHeight(activity));
        image.close();
        if (bitmap != null) {
            try {
                File file = new File(FileUtil.getResPath(), "capture.png");
                if (file.exists()) {
                    FileUtil.delete(file);
                }
                file.createNewFile();
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private ArrayList<AppCompatActivity> activityArrayList = new ArrayList<>();

    /**
     * 显示头部倒计时-->初始化时
     */
    public void showHeadCountView() {
        if (customHeadTimer == null) {
            customHeadTimer = new CustomHeadTimerView(activity);
        } else {
        }
        if (alertView == null) {
            alertView = new TimerAlertView(App2.get());
        }
        customHeadTimer.iniHeadView(getContentView(activity));

        bindHeadTimerService();
    }

    /**
     * 显示 头部倒计时
     */
    public void showHeadCountView(int time) {
        bindHeadTimerService(time);
    }

    /**
     * 移除 头部倒计时
     */
    private void dismissHeadCountView() {
        if (customHeadTimer != null) {
            customHeadTimer.disHeadView();
            customHeadTimer = null;
        }
        if (timerHeadConn != null) {
            unbindService(timerHeadConn);
            timerHeadConn = null;
        }
        Constant.setShowHeadCountDown(false);
    }

    /**
     * 提前下课 弹出窗
     */
    public void classDg() {
        String str = DateUtil.millsecondsToString(reclenPub * 1000);

        new CustomDialog.Builder(App2.get()).setSpannableMessage("距您设置的下课时长剩余" + str + "秒\n" + "确定提前下课吗？")
                .setPositive("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getBehavior(2, true);
                        if (activity instanceof TiWenActivity) {
                            activity.finish();
                        }
                        dismissHeadCountView();
                        dialog.dismiss();
                    }
                }).setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    /**
     * 头部--绑定倒计时服务
     */
    private void bindHeadTimerService(final int time) {

        if (timerHeadConn != null && timerHeadService != null) {
            timerHeadService.setTime(time);
            startHeadTimer();
            return;
        }
        Intent service = new Intent(activity, TimerHeadService.class);
        timerHeadConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerHeadService.TimerBinder timerBinder = (TimerHeadService.TimerBinder) service;
                timerHeadService = timerBinder.getService();
                timerHeadService.setTime(time);
                startHeadTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(App2.get(), TimerHeadService.class.getName())) {
            App2.get().startService(service);
        }
        App2.get().bindService(service, timerHeadConn, Context.BIND_ABOVE_CLIENT);
    }

    /**
     * 头部--绑定倒计时服务
     */
    private void bindHeadTimerService() {
        if (timerHeadConn != null) {
            startHeadTimer();
            return;
        }
        Intent service = new Intent(activity, TimerHeadService.class);
        timerHeadConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerHeadService.TimerBinder timerBinder = (TimerHeadService.TimerBinder) service;
                timerHeadService = timerBinder.getService();
                startHeadTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(App2.get(), TimerHeadService.class.getName())) {
            App2.get().startService(service);
        }
        App2.get().bindService(service, timerHeadConn, Context.BIND_ABOVE_CLIENT);
    }

    /**
     * 头部--开始计时
     */
    private void startHeadTimer() {
        if (timerHeadService != null) {
            timerHeadService.setHandler(myHandler);
            if (timerHeadService != null) {
                timerHeadService.startTimer();
            }
        }
    }


    public void activityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_MEDIA_PROJECTION_BAIBAN_SCREEN) {
            mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
            getBaiBANScreen();
        }
        if (requestCode == REQUEST_MEDIA_PROJECTION_CAPTURE) {
            mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
            getScreen();
        } else if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (intentResult != null) {
                if (intentResult.getContents() == null) {
                    Toast.makeText(App2.get(), "内容为空", Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(App2.get(), "扫描成功", Toast.LENGTH_LONG).show();
                    SPUtils.remove(activity, "toupingType");
                    try {
                        // ScanResult 为 获取到的字符串
                        String url = intentResult.getContents();
                        Gson gson = new Gson();
                        Test test = gson.fromJson(url, Test.class);
                        // 如果需要根据电脑分辨率变更投屏大小，可以把下面两行代码放开
//                        Config.currHeight = Integer.parseInt(test.getWeight());
//                        Config.currWidth = Integer.parseInt(test.getHeight());
                        myUrl = "http://" + test.getServerIP() + ":" + test.getPort();
                        getRtmpUrl(myUrl);
                    } catch (Exception e) {
                        // 适配老投屏客户端
                        SPUtils.put(activity, "toupingType", "old");
                        rtmpUrl = intentResult.getContents();
                        if (mMediaProjection == null) {
                            activity.startActivityForResult(
                                    mMediaProjectionManager.createScreenCaptureIntent(),
                                    REQUEST_MEDIA_PROJECTION_SCREEN);
                        } else {
                            EventBus.getDefault().post(Event.EVENT_CLOSE_SCREEN);
                            SimplePublisher.getInstance().start(activity, rtmpUrl, false, listener, mMediaProjection);
                        }
                    }
                }
            }
        } else if (requestCode == REQUEST_MEDIA_PROJECTION_SCREEN) {
            mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
            if (mMediaProjection != null) {
                EventBus.getDefault().post(Event.EVENT_CLOSE_SCREEN);
                SimplePublisher.getInstance().start(activity, rtmpUrl, false, listener, mMediaProjection);
            }
        } else if (requestCode == REQ_CODE_DOODLE) {
            if (data == null) {
                return;
            }
            if (resultCode == DoodleActivity.RESULT_OK) {
                String path = data.getStringExtra(DoodleActivity.KEY_IMAGE_PATH);
                if (TextUtils.isEmpty(path)) {
                    return;
                }


                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PublishDoodleActivity");
                intent.putExtra("path", path);
                startActivity(intent);

            } else if (resultCode == DoodleActivity.RESULT_ERROR) {
                Toast.makeText(App2.get(), "error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    SimpleRtmpListener listener = new SimpleRtmpListener() {

        @Override
        public void onRtmpConnecting(String msg) {
//            mButton.setText("重连中");
        }

        @Override
        public void onRtmpConnected(String msg) {
//            mButton.setText("录制中");
            if (SPUtils.get(activity, "toupingType", "") != null) {
                String toupingType = SPUtils.get(activity, "toupingType", "").toString();
                SPUtils.remove(activity, "toupingType");
                if ("old".equals(toupingType)) {
                    if (activity instanceof ScreenConfigActivity) {
                        activity.finish();
                    }
                    return;
                }
            }
            startScreen();
        }

        @Override
        public void onRtmpStopped() {
//            Log.d("TAG_RTMP", "onRtmpStopped");
        }

        @Override
        public void onRtmpDisconnected() {
            Log.d("TAG_RTMP", "onRtmpDisconnected");
        }

        @Override
        public void onRtmpException(Exception e) {
            handleException(e);
        }
    };

    private void handleException(Exception e) {
//        mButton.setText("连接断开");
//        Log.d("TAG_RTMP", "连接断开");
    }

    /**
     * 下课
     */
    private void getBehavior(final int type, final boolean isToast) {
        /*
         1 上课 2 下课 3 锁屏 4 解锁
         */
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getClassBehavior(SPUtils.get(activity, SPUtils.CLASS_ID, "").toString(), String.valueOf(type), "")
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {//25001   未下课
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        switch (type) {
                            case 1:
                                ToastUtil.showShortlToast("上课");
                                break;
                            case 2:
                                if (isToast) {
                                    ToastUtil.showShortlToast("下课");
                                }
                                EventBus.getDefault().post(new LessonDetailsGoClassEvent());
                                /// 下课同时  取消锁屏
                                getBehavior(4, true);
                                break;
                            case 3:
                                ToastUtil.showShortlToast("学生端锁屏");
                                break;
                            case 4:
//                                ToastUtil.showShortlToast("学生端解锁");
                                SPUtils.remove(activity, SPUtils.CLASS_ID);
                                break;
                            default:
                                break;
                        }

                    }
                });
    }

    /**
     * 投屏
     */
    class Test {
        private String serverIP;
        private String Port;

        public String getServerIP() {
            return serverIP;
        }

        public void setServerIP(String serverIP) {
            this.serverIP = serverIP;
        }

        public String getPort() {
            return Port;
        }

        public void setPort(String port) {
            Port = port;
        }

        private String Weight;
        private String Height;

        public String getWeight() {
            return this.Weight;
        }

        public void setWeight(String weight) {
            this.Weight = weight;
        }

        public String getHeight() {
            return Height;
        }

        public void setHeight(String height) {
            Height = height;
        }
    }

    private void getRtmpUrl(String url) {
        OkHttpUtils.post().url(url + "/RtmpAddr?")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {


                    }

                    @Override
                    public void onResponse(String response, int id) {
                        rtmpUrl = response;
                        if (mMediaProjection == null) {
                            activity.startActivityForResult(
                                    mMediaProjectionManager.createScreenCaptureIntent(),
                                    REQUEST_MEDIA_PROJECTION_SCREEN);
                        } else {
                            EventBus.getDefault().post(Event.EVENT_CLOSE_SCREEN);
                            SimplePublisher.getInstance().start(activity, rtmpUrl, false, listener, mMediaProjection);

                        }
                    }
                });


    }

    private String myUrl;

    private void startScreen() {//rtmpUrl
        SPUtils.put(activity, "url_touping", myUrl);
        OkHttpUtils.post().url(myUrl + "/RtmpPlay?")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ToastUtil.showShortlToast("投屏链接失败");

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (response != null) {//地址
//                            ToastUtil.showShortlToast("RtmpPlay---");

                        }
                    }
                });
        if (activity instanceof ScreenConfigActivity) {
            activity.finish();
        }
    }

    public void stopScreen() {
        myUrl = SPUtils.get(activity, "url_touping", "").toString();
        if (StringUtils.isNullOrEmpty(myUrl)) {
            EventBus.getDefault().post(Event.EVENT_OPEN_SCREEN);
            Toast.makeText(App2.get(), "结束投屏", Toast.LENGTH_SHORT).show();
            return;
        }
        OkHttpUtils.post().url(myUrl + "/RtmpStop?")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        EventBus.getDefault().post(Event.EVENT_OPEN_SCREEN);
                        Toast.makeText(App2.get(), "结束投屏", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (response != null) {//地址
                            EventBus.getDefault().post(Event.EVENT_OPEN_SCREEN);
                            Toast.makeText(App2.get(), "结束投屏", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


}
