package com.example.app5libbase.login.presenter;


import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.login.activity.ForgetPswActivity;
import com.example.app5libbase.base.BasePresenter;
import com.example.app5libbase.login.view.ForgetPswNumView;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.VerifyUserNameBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * ForgetPswNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ForgetPswNumPresenter extends BasePresenter<ForgetPswNumView, ForgetPswActivity> {

    public void verityUserName(final String cardId) {
        if (!StringUtils.isMobile(cardId) && cardId.length() != 8) {
            ToastUtil.showShortlToast("请输入正确的账号或手机号");
            return;
        }
        Network.createService(NetWorkService.ForgetPswService.class)
                .verityUserName(cardId)
                .map(new StatusFunc<VerifyUserNameBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<VerifyUserNameBean>(new SubscriberListener<VerifyUserNameBean>() {
                    @Override
                    public void onNext(VerifyUserNameBean o) {
                        mView.verifySuccess(o.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getStatus().getCode() == 21100) {
                                ToastUtil.showShortlToast("您输入的账号不存在，请重新输入");
                                return;
                            }

                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
                        }
                        ToastUtil.showShortlToast("验证失败");
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

}
