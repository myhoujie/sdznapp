package com.example.app5libbase.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * GraphView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class GraphView extends ViewGroup {

    private static final String correctStr = "正确率";
    private static final String completionStr = "完成率";

    private String[] flagValue = {"100", "80", "60", "40", "20", "0"};

    private final static int COLOR_BASE_LINE = 0xFF323C47;
    private final static int COLOR_BASE_TEXT = 0xFF323C47;
    private final static int COLOR_CHAT_LINE = 0xFFEDEDED;
    private final static int COLOR_VALUE_TXT = 0xFF808FA3;
    private final static int COLOR_POP_WINDOW_BG = 0xA6000000;
    private final static int COLOR_POP_WINDOW_TEXT = 0xFFFFFFFF;

    private final static int[] COLOR_COLUM_COLORS = {0xFFFFAC4C, 0xFF10D3D2, 0xFF68C426};


    private List<LineViewBean> mDatas = new ArrayList<>();
    private boolean canScroll;

    private GestureDetector mGestureDetector;

    StaticLayout m;

    /**
     * 柱状图的宽度
     */
    private float defColumWidth = 48;

    /**
     * 柱状图选中时候的宽度
     */
    private float defColumBgWidth = 88;

    /**
     * 每条数据所占用的最小宽度
     */
    private float defItemMinWidth = 180;

    /**
     * 每条数据实际占用的宽度
     */
    private float itemWidth;

    /**
     * 最大的滑动距离
     */
    private float mMaxHorizontalScrollDis;

    /**
     * 当前选中的item
     */
    private int curShowPoint;

    private int curShowItem;
    /**
     * View 的宽度,高度
     */
    private float viewWidth, viewHeight;

    /**
     * 除去周边文字的图形 区域
     */
    private RectF chatRectF = new RectF();

    private Paint baseLinePaint, chatLinePaint, columPaint, pointPaint, popwindowBgPaint;

    private TextPaint baseTxtPaint, valuePaint, infoPaint;

    public GraphView(Context context) {
        this(context, null);
    }

    public GraphView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }

    private void init() {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mGestureDetector = new GestureDetector(getContext(), new OnGestureListener());
        initPaint();
    }

    public void setDatas(List<GraphView.LineViewBean> datas, boolean canScroll) {
        mDatas = datas;
        this.canScroll = canScroll;
        initItemMinWidth(canScroll);
        invalidate();
    }

    private void initPaint() {
        baseLinePaint = new Paint();
        baseLinePaint.setAntiAlias(true);
        baseLinePaint.setStyle(Paint.Style.FILL);
        baseLinePaint.setStrokeWidth(UiUtils.dp2px(1));
        baseLinePaint.setColor(COLOR_BASE_LINE);

        chatLinePaint = new Paint();
        chatLinePaint.setAntiAlias(true);
        chatLinePaint.setStyle(Paint.Style.FILL);
        chatLinePaint.setStrokeWidth(UiUtils.dp2px(1));
        chatLinePaint.setColor(COLOR_CHAT_LINE);


        popwindowBgPaint = new Paint();
        popwindowBgPaint.setAntiAlias(true);
        popwindowBgPaint.setStyle(Paint.Style.FILL);
        popwindowBgPaint.setStrokeWidth(UiUtils.dp2px(1));
        popwindowBgPaint.setColor(COLOR_POP_WINDOW_BG);

        baseTxtPaint = new TextPaint();
        baseTxtPaint.setAntiAlias(true);
        baseTxtPaint.setStyle(Paint.Style.FILL);
        baseTxtPaint.setStrokeWidth(UiUtils.dp2px(1));
        baseTxtPaint.setTextSize(UiUtils.sp2px(12));
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        baseTxtPaint.setColor(COLOR_BASE_TEXT);

        valuePaint = new TextPaint();
        valuePaint.setAntiAlias(true);
        valuePaint.setStyle(Paint.Style.FILL);
        valuePaint.setTextSize(UiUtils.sp2px(12));
        valuePaint.setTextAlign(Paint.Align.CENTER);
        valuePaint.setStrokeWidth(UiUtils.dp2px(1));
        valuePaint.setColor(COLOR_VALUE_TXT);

        columPaint = new Paint();
        columPaint.setAntiAlias(true);
        columPaint.setStyle(Paint.Style.STROKE);
        columPaint.setTextSize(UiUtils.sp2px(12));

        pointPaint = new Paint();
        pointPaint.setAntiAlias(true);
        pointPaint.setStyle(Paint.Style.FILL);

        infoPaint = new TextPaint();
        infoPaint.setAntiAlias(true);
        infoPaint.setStyle(Paint.Style.FILL);
        infoPaint.setTextSize(UiUtils.sp2px(12));
        infoPaint.setTextAlign(Paint.Align.LEFT);
        infoPaint.setStrokeWidth(UiUtils.dp2px(1));
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // view宽高
        viewWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        viewHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        // 图标部分的尺寸
        chatRectF.left = UiUtils.dp2px(42);
        chatRectF.top = UiUtils.dp2px(50);
        chatRectF.right = viewWidth - UiUtils.dp2px(20);
        chatRectF.bottom = viewHeight - UiUtils.dp2px(50);

        // 如果数据太多，需要滑动，所以要按最小宽度，但是如果数据太少不够用，需要扩展最小宽度
        initItemMinWidth(canScroll);
    }

    private void initItemMinWidth(boolean canScroll) {
        if (mDatas != null && mDatas.size() > 0) {
            final float defTotalWidth = (mDatas.get(0).getItemVo().size() - 1) * defItemMinWidth;
            if (defTotalWidth > chatRectF.width() && canScroll) {
                itemWidth = defItemMinWidth;
                mMaxHorizontalScrollDis = defTotalWidth - chatRectF.width();
            } else {
                itemWidth = (chatRectF.right - chatRectF.left) / (mDatas.get(0).getItemVo().size() - 1);
                mMaxHorizontalScrollDis = 0;
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            drawBaseLine(canvas);
            drawChatLine(canvas);
            drawFlagValue(canvas);
            drawLines(canvas);
            drawPopWindow(canvas);
            drawPoint(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 图表的横竖两条边线
     *
     * @param canvas
     */
    private void drawBaseLine(Canvas canvas) {
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.left + getScrollX(), chatRectF.bottom, baseLinePaint);
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX(), chatRectF.bottom, baseLinePaint);
    }

    /**
     * 图表中的横线
     *
     * @param canvas
     */
    private void drawChatLine(Canvas canvas) {
        final float chatH = chatRectF.bottom - chatRectF.top;
        final float itemHeight = chatH / 5;
        final float baseHeight = chatRectF.top;
        for (int i = 0; i < 5; i++) {
            canvas.drawLine(chatRectF.left + getScrollX(), baseHeight + itemHeight * i, chatRectF.right + getScrollX(), baseHeight + itemHeight * i, chatLinePaint);
        }
    }

    /**
     * 绘制图表周边的文字
     *
     * @param canvas
     */
    private void drawFlagValue(Canvas canvas) {
        baseTxtPaint.setColor(COLOR_BASE_TEXT);
        // 五行标数的间隔
        final float h = (chatRectF.bottom - chatRectF.top) / 5;

        baseTxtPaint.setTextAlign(Paint.Align.LEFT);
        //文本的高度
        Rect rect = new Rect();
        baseTxtPaint.getTextBounds(correctStr + "%", 0, (correctStr + "%").length(), rect);
        int height = rect.height();

        canvas.drawText(correctStr + "%", 0, (correctStr + "%").length(), getScrollX() + chatRectF.left - UiUtils.dp2px(26), UiUtils.dp2px(12) + height, baseTxtPaint);

        // 绘制五行标数
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        for (int i = 0; i < flagValue.length; i++) {
            canvas.drawText(flagValue[i], 0, flagValue[i].length(), getScrollX() + chatRectF.left - UiUtils.dp2px(10), chatRectF.top + h * i + height / 2, baseTxtPaint);
        }

        canvas.save();
        // 防止文字显示的太靠边了
        canvas.clipRect(new RectF(chatRectF.left - UiUtils.dp2px(30) + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX() + UiUtils.dp2px(20), viewHeight));

        baseTxtPaint.setTextAlign(Paint.Align.CENTER);
        // 绘制底部文字
        if (mDatas != null && mDatas.size() > 0) {
            for (int i = 0; i < mDatas.get(0).getItemVo().size(); i++) {
                String name = mDatas.get(0).getItemVo().get(i).getName();
                canvas.drawText(name, 0, name.length(), chatRectF.left + itemWidth * i, chatRectF.bottom + UiUtils.dp2px(10) + height, baseTxtPaint);
                String unit = mDatas.get(0).getItemVo().get(i).getUnit();
                if (unit != null) {
                    canvas.drawText(unit, 0, unit.length(), chatRectF.left + itemWidth * i, chatRectF.bottom + UiUtils.dp2px(15) + height * 2, baseTxtPaint);
                }
            }
        }
        canvas.restore();

        // 绘制示例
        baseTxtPaint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(completionStr, 0, completionStr.length(), viewWidth + getScrollX() - UiUtils.dp2px(133), UiUtils.dp2px(12) + height, baseTxtPaint);
        canvas.drawText(correctStr, 0, correctStr.length(), viewWidth + getScrollX() - UiUtils.dp2px(59), UiUtils.dp2px(12) + height, baseTxtPaint);

        baseTxtPaint.setColor(getResources().getColor(R.color.graphview_completion));
        RectF rectFCompletion = new RectF(viewWidth + getScrollX() - UiUtils.dp2px(148), UiUtils.dp2px(16), viewWidth + getScrollX() - UiUtils.dp2px(140), UiUtils.dp2px(24));
        canvas.drawOval(rectFCompletion, baseTxtPaint);
        baseTxtPaint.setColor(getResources().getColor(R.color.graphview_correct));
        RectF rectFCorrect = new RectF(viewWidth + getScrollX() - UiUtils.dp2px(74), UiUtils.dp2px(16), viewWidth + getScrollX() - UiUtils.dp2px(66), UiUtils.dp2px(24));
        canvas.drawOval(rectFCorrect, baseTxtPaint);
    }

    /**
     * 画曲线
     *
     * @param canvas
     */
    private void drawLines(Canvas canvas) {
        measurePath();
        for (int i = 0; i < mDatas.size(); i++) {
            columPaint.setColor(mDatas.get(i).getColor());
            Path path = mDatas.get(i).getPath();
            PathMeasure mPathMeasure = new PathMeasure(path, false);
            Path dst = new Path();
            dst.rLineTo(0, 0);
            float distance = mPathMeasure.getLength();
            if (mPathMeasure.getSegment(0, distance, dst, true)) {
                //绘制线
                canvas.save();
                canvas.clipRect(chatRectF.left + getScrollX(), 0, chatRectF.right + getScrollX(), viewHeight);
                canvas.drawPath(dst, columPaint);
                float[] pos = new float[2];
                mPathMeasure.getPosTan(distance, pos, null);
                //绘制阴影
                dst.lineTo(pos[0], chatRectF.bottom);
                dst.lineTo(chatRectF.left, chatRectF.bottom);
                dst.close();
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);

                int topY = (int) chatRectF.bottom;
                int bottomY = 0;
                List<ItemInfoVo> itemVos = mDatas.get(i).getItemVo();
                for (ItemInfoVo itemInfoVo : itemVos) {
                    if (itemInfoVo.y < topY) {
                        topY = itemInfoVo.y;
                    }
                    if (itemInfoVo.y > bottomY) {
                        bottomY = itemInfoVo.y;
                    }
                }

                Shader mShader = new LinearGradient(0, topY, 0, bottomY, mDatas.get(i).getShadowColor(), null, Shader.TileMode.REPEAT);
                paint.setShader(mShader);
                canvas.drawPath(dst, paint);
                canvas.restore();
            }
        }
    }

    /**
     * 绘制线上的点
     *
     * @param canvas
     */
    private void drawPoint(Canvas canvas) {
        canvas.save();
        canvas.clipRect(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom);
        pointPaint.setStyle(Paint.Style.STROKE);
        pointPaint.setStrokeWidth(UiUtils.dp2px(1));
        pointPaint.setColor(0xFF000000);
        pointPaint.setPathEffect(new DashPathEffect(new float[]{5, 5}, 0));
        if (mDatas != null && mDatas.size() > 0) {
            canvas.drawLine(mDatas.get(0).getItemVo().get(curShowPoint).x, 0, mDatas.get(0).getItemVo().get(curShowPoint).x, viewHeight, pointPaint);
            for (int i = 0; i < mDatas.size(); i++) {
                final ItemInfoVo itemInfoVo = mDatas.get(i).getItemVo().get(curShowPoint);
                pointPaint.setColor(0xDDFFFFFF);
                pointPaint.setPathEffect(null);
                pointPaint.setStrokeWidth(UiUtils.dp2px(2));
                canvas.drawCircle(itemInfoVo.x, itemInfoVo.y, UiUtils.dp2px(4), pointPaint);
                pointPaint.setStyle(Paint.Style.FILL);
                pointPaint.setColor(mDatas.get(curShowItem).getColor());
                canvas.drawCircle(itemInfoVo.x, itemInfoVo.y, UiUtils.dp2px(3), pointPaint);
            }
        }
        canvas.restore();
    }

    /**
     * 计算贝塞尔曲线
     */
    private void measurePath() {
        dealData();

        for (int i = 0; i < mDatas.size(); i++) {
            Path mPath = new Path();
            float prePreviousPointX = Float.NaN;
            float prePreviousPointY = Float.NaN;
            float previousPointX = Float.NaN;
            float previousPointY = Float.NaN;
            float currentPointX = Float.NaN;
            float currentPointY = Float.NaN;
            float nextPointX;
            float nextPointY;

            List<ItemInfoVo> mPointList = mDatas.get(i).getItemVo();
            final int lineSize = mPointList.size();
            for (int valueIndex = 0; valueIndex < lineSize; ++valueIndex) {
                if (Float.isNaN(currentPointX)) {
                    ItemInfoVo point = mPointList.get(valueIndex);
                    currentPointX = point.x;
                    currentPointY = point.y;
                }
                if (Float.isNaN(previousPointX)) {
                    //是否是第一个点
                    if (valueIndex > 0) {
                        ItemInfoVo point = mPointList.get(valueIndex - 1);
                        previousPointX = point.x;
                        previousPointY = point.y;
                    } else {
                        //是的话就用当前点表示上一个点
                        previousPointX = currentPointX;
                        previousPointY = currentPointY;
                    }
                }

                if (Float.isNaN(prePreviousPointX)) {
                    //是否是前两个点
                    if (valueIndex > 1) {
                        ItemInfoVo point = mPointList.get(valueIndex - 2);
                        prePreviousPointX = point.x;
                        prePreviousPointY = point.y;
                    } else {
                        //是的话就用当前点表示上上个点
                        prePreviousPointX = previousPointX;
                        prePreviousPointY = previousPointY;
                    }
                }

                // 判断是不是最后一个点了
                if (valueIndex < lineSize - 1) {
                    ItemInfoVo point = mPointList.get(valueIndex + 1);
                    nextPointX = point.x;
                    nextPointY = point.y;
                } else {
                    //是的话就用当前点表示下一个点
                    nextPointX = currentPointX;
                    nextPointY = currentPointY;
                }

                if (valueIndex == 0) {
                    // 将Path移动到开始点
                    mPath.moveTo(currentPointX, currentPointY);
                } else {
                    // 求出控制点坐标
                    final float firstDiffX = (currentPointX - prePreviousPointX);
                    final float firstDiffY = (currentPointY - prePreviousPointY);
                    final float secondDiffX = (nextPointX - previousPointX);
                    final float secondDiffY = (nextPointY - previousPointY);
                    final float firstControlPointX = previousPointX + (0.16f * firstDiffX);
                    float firstControlPointY = previousPointY + (0.16f * firstDiffY);
                    final float secondControlPointX = currentPointX - (0.16f * secondDiffX);
                    float secondControlPointY = currentPointY - (0.16f * secondDiffY);

                    if (firstControlPointY < chatRectF.top) {
                        firstControlPointY = chatRectF.top;
                    }
                    if (firstControlPointY > chatRectF.bottom) {
                        firstControlPointY = chatRectF.bottom;
                    }
                    if (secondControlPointY < chatRectF.top) {
                        secondControlPointY = chatRectF.top;
                    }
                    if (secondControlPointY > chatRectF.bottom) {
                        secondControlPointY = chatRectF.bottom;
                    }

                    //画出曲线
                    mPath.cubicTo(firstControlPointX, firstControlPointY, secondControlPointX, secondControlPointY,
                            currentPointX, currentPointY);
                }

                // 更新值,
                prePreviousPointX = previousPointX;
                prePreviousPointY = previousPointY;
                previousPointX = currentPointX;
                previousPointY = currentPointY;
                currentPointX = nextPointX;
                currentPointY = nextPointY;
            }

            mDatas.get(i).setPath(mPath);
        }
    }

    /**
     * 计算出每个点的坐标
     */
    private void dealData() {
        for (int i = 0; i < mDatas.size(); i++) {
            for (int j = 0; j < mDatas.get(i).getItemVo().size(); j++) {
                final ItemInfoVo itemInfoVo = mDatas.get(i).getItemVo().get(j);
                itemInfoVo.x = (int) (j * itemWidth + chatRectF.left);
                itemInfoVo.y = (int) ((1 - itemInfoVo.getValue()) * chatRectF.height() + chatRectF.top);
            }
        }
    }

    /**
     * 绘制弹窗
     *
     * @param canvas
     */
    private void drawPopWindow(Canvas canvas) {
        if (mDatas != null && mDatas.size() > 0) {
            ItemInfoVo itemInfoVoCorrect = mDatas.get(0).getItemVo().get(curShowPoint);
            ItemInfoVo itemInfoVoCompletion = mDatas.get(1).getItemVo().get(curShowPoint);

            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(itemInfoVoCorrect.getTime())
                    .append("\n")
                    .append("完成率        ")
                    .append((int) (itemInfoVoCompletion.getValue() * 100))
                    .append("%")
                    .append("\n")
                    .append("正确率        ")
                    .append((int) (itemInfoVoCorrect.getValue() * 100))
                    .append("%");

            RectF tempRect = new RectF();

            StaticLayout staticLayout = new StaticLayout(
                    stringBuffer.toString(),
                    infoPaint,
                    UiUtils.dp2px(90),
                    Layout.Alignment.ALIGN_NORMAL,
                    1.2f,
                    0.4f,
                    true);

//            float padding = 20;
            float paddingColum = 10;

            final int popWidth = staticLayout.getWidth();
            final int popHeight = staticLayout.getHeight();

            Point point = new Point(itemInfoVoCorrect.x, Math.abs(itemInfoVoCorrect.y - itemInfoVoCompletion.y) / 2 + (itemInfoVoCorrect.y > itemInfoVoCompletion.y ? itemInfoVoCompletion.y : itemInfoVoCorrect.y));

            if (popWidth + point.x + paddingColum * 2 - getScrollX() > chatRectF.right - UiUtils.dp2px(8)) {
                tempRect.right = point.x - UiUtils.dp2px(8);
                tempRect.left = tempRect.right - popWidth - paddingColum * 4;
            } else {
                tempRect.left = point.x + UiUtils.dp2px(8);
                tempRect.right = tempRect.left + popWidth + paddingColum * 4;
            }

            if (point.y + popHeight / 2 + paddingColum > chatRectF.bottom) {
                tempRect.bottom = chatRectF.bottom;
                tempRect.top = tempRect.bottom - popHeight - paddingColum * 2;
            } else if (point.y - popHeight / 2 < chatRectF.top) {
                tempRect.top = chatRectF.top;
                tempRect.bottom = chatRectF.top + popHeight + paddingColum * 2;
            } else {
                tempRect.top = point.y - popHeight / 2 - paddingColum;
                tempRect.bottom = point.y + popHeight / 2 + paddingColum;
            }

            canvas.save();
            canvas.clipRect(new RectF(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom));
            canvas.drawRoundRect(tempRect, UiUtils.dp2px(8), UiUtils.dp2px(8), popwindowBgPaint);
            canvas.restore();

            canvas.save();
            canvas.translate(tempRect.left + paddingColum * 2, tempRect.top + paddingColum);
            staticLayout.draw(canvas);
        }
        canvas.restore();
    }

    /**********************
     * 手势区域分割线
     ********************/

    private void setShowItem(int item, int pointPos) {
        curShowItem = item;
        curShowPoint = pointPos;
        if (itemClickListener != null) {
            if (item < mDatas.size()) {
                itemClickListener.onItemChanged(mDatas.get(item).getItemVo().get(pointPos));
            }
        }
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // 判断点击位置离哪个点最近
            float x = e.getX() + getScrollX() - chatRectF.left;
            curShowPoint = (int) ((x + itemWidth / 2) / itemWidth);
            invalidate();
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            if (Math.abs(distanceX) < Math.abs(distanceY)) {
                return false;
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);//屏蔽父控件拦截onTouch事件
            }
            float dx = distanceX;
            int x = getScrollX();

            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (mMaxHorizontalScrollDis < 0) {
                dx = 0;
            } else if (x + dx > mMaxHorizontalScrollDis) {
                dx = mMaxHorizontalScrollDis - x;
            }

            scrollBy((int) dx, 0);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    public static class LineViewBean {
        private int id;
        private String name;
        private int color;
        private int[] shadowColor;
        private List<ItemInfoVo> itemVo;
        private List<Point> points;
        private Path path;

        public List<ItemInfoVo> getItemVo() {
            return itemVo;
        }

        public void setItemVo(List<ItemInfoVo> itemVo) {
            this.itemVo = itemVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public int[] getShadowColor() {
            return shadowColor;
        }

        public void setShadowColor(int[] shadowColor) {
            this.shadowColor = shadowColor;
        }

        public List<Point> getPoints() {
            return points;
        }

        public void setPoints(List<Point> points) {
            this.points = points;
        }

        public Path getPath() {
            return path;
        }

        public void setPath(Path path) {
            this.path = path;
        }
    }

    public static class ItemInfoVo {
        private String name;
        private String unit;
        private float value;
        private String id;
        private String time;
        public int x;
        public int y;


        public ItemInfoVo() {
        }

        public ItemInfoVo(String id, String name, int value, String time, String unit) {
            this.name = name;
            this.unit = unit;
            this.value = value;
            this.id = id;
            this.time = time;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    private ItemChangedListener itemClickListener;

    public void setItemClickListener(ItemChangedListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemChangedListener {
        void onItemChanged(ItemInfoVo pos);
    }
}
