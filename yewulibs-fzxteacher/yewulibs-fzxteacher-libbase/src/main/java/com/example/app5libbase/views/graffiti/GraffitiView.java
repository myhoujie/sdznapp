package com.example.app5libbase.views.graffiti;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.example.app5libbase.views.graffiti.core.IGraffiti;
import com.example.app5libbase.views.graffiti.core.IGraffitiColor;
import com.example.app5libbase.views.graffiti.core.IGraffitiItem;
import com.example.app5libbase.views.graffiti.core.IGraffitiPen;
import com.example.app5libbase.views.graffiti.core.IGraffitiShape;
import com.example.app5libbase.views.graffiti.core.IGraffitiTouchDetector;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.example.app5libbase.views.graffiti.DrawUtil.rotatePoint;
import static org.xutils.image.ImageDecoder.rotate;


/**
 * 涂鸦框架
 * Created by huangziwei on 2016/9/3.
 */
public class GraffitiView extends FrameLayout implements IGraffiti {

    // ACTION
    public final static int ACTION_ROTATION = 1; // 旋转
    public final static int ACTION_UNDO = 3; // 撤销

    public final static float MAX_SCALE = 4f; // 最大缩放倍数
    public final static float MIN_SCALE = 0.25f; // 最小缩放倍数
    public final static int DEFAULT_SIZE = 2; // 默认画笔大小


    private IGraffitiListener mGraffitiListener;

    private Bitmap mBitmap; // 当前涂鸦的原图（旋转后）
    private Bitmap mGraffitiBitmap; // 绘制涂鸦的图片
    private Canvas mBitmapCanvas;

    private float mCenterScale; // 图片适应屏幕时的缩放倍数
    private int mCenterHeight, mCenterWidth;// 图片适应屏幕时的大小（View窗口坐标系上的大小）
    private float mCentreTranX, mCentreTranY;// 图片在适应屏幕时，位于居中位置的偏移（View窗口坐标系上的偏移）

    private float mRotateScale = 1;  // 在旋转后适应屏幕时的缩放倍数
    private float mRotateTranX, mRotateTranY; // 旋转后适应屏幕居中时的偏移

    private float mScale = 1; // 在适应屏幕时的缩放基础上的缩放倍数 （ 图片真实的缩放倍数为 mCenterScale*mScale ）
    private float mTransX = 0, mTransY = 0; // 图片在适应屏幕且处于居中位置的基础上的偏移量（ 图片真实偏移量为mCentreTranX + mTransX，View窗口坐标系上的偏移）
    private float mMinScale = MIN_SCALE; // 最小缩放倍数
    private float mMaxScale = MAX_SCALE; // 最大缩放倍数

    private float mSize;
    private IGraffitiColor mColor; // 画笔底色

    private boolean mIsDrawableOutside = false; // 触摸时，图片区域外是否绘制涂鸦轨迹
    private boolean mReady = false;
    private boolean isJustDrawOriginal = false;


    // 保存涂鸦操作，便于撤销
    private CopyOnWriteArrayList<IGraffitiItem> mItemStack = new CopyOnWriteArrayList<>();

    private IGraffitiPen mPen;
    private IGraffitiShape mShape;

    private float mGraffitiSizeUnit = 1; // 长度单位，不同大小的图片的长度单位不一样。该单位的意义同dp的作用类似，独立于图片之外的单位长度
    private int mGraffitiRotateDegree = 0; // 相对于初始图片旋转的角度

    private List<WeakReference<IGraffitiViewListener>> mListenerList = new CopyOnWriteArrayList<>();

    // 手势相关
    private IGraffitiTouchDetector mDefaultTouchDetector;

    private GraffitiViewInner mInner;
    private RectF mGraffitiBound = new RectF();
    private PointF mTempPoint = new PointF();

    public GraffitiView(Context context, Bitmap bitmap, IGraffitiListener listener) {
        super(context);

        // 关闭硬件加速，因为bitmap的Canvas不支持硬件加速
        setLayerType(LAYER_TYPE_SOFTWARE, null);

        mBitmap = bitmap;
        mGraffitiListener = listener;

        mScale = 1f;
        mColor = new GraffitiColor(Color.RED);

        mPen = GraffitiPen.HAND;
        mShape = GraffitiShape.HAND_WRITE;

        mDefaultTouchDetector = null;

        mInner = new GraffitiViewInner(context);
        addView(mInner);
        setClipChildren(false);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        initCanvas();
        initGraffitiBitmap();
        if (!mReady) {
            mGraffitiListener.onReady();
            mReady = true;
        }
    }

    private Matrix mTouchEventMatrix = new Matrix();

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (mGraffitiRotateDegree == 0 || mGraffitiRotateDegree == 180) {
            return super.dispatchTouchEvent(event);
        } else {
            // 把事件转发给innerView，避免在区域外不可点击
            MotionEvent transformedEvent = MotionEvent.obtain(event);
            mTouchEventMatrix.reset();
            mTouchEventMatrix.setRotate(-mGraffitiRotateDegree, mInner.getWidth() * 0.5f, mInner.getHeight() * 0.5f);
            transformedEvent.transform(mTouchEventMatrix);
            boolean handled = mInner.dispatchTouchEvent(transformedEvent);
            transformedEvent.recycle();

            return handled;
        }
    }

    private void initGraffitiBitmap() {// 不用resize preview
        int w = mBitmap.getWidth();
        int h = mBitmap.getHeight();
        float nw = w * 1f / getWidth();
        float nh = h * 1f / getHeight();
        if (nw > nh) {
            mCenterScale = 1 / nw;
            mCenterWidth = getWidth();
            mCenterHeight = (int) (h * mCenterScale);
        } else {
            mCenterScale = 1 / nh;
            mCenterWidth = (int) (w * mCenterScale);
            mCenterHeight = getHeight();
        }
        // 使图片居中
        mCentreTranX = (getWidth() - mCenterWidth) / 2f;
        mCentreTranY = (getHeight() - mCenterHeight) / 2f;

        float amplifierRadius = Math.min(getWidth(), getHeight()) / 4;
        Path amplifierPath = new Path();
        amplifierPath.addCircle(amplifierRadius, amplifierRadius, amplifierRadius, Path.Direction.CCW);

        mGraffitiSizeUnit = dp2px(getContext(), 1) / mCenterScale;

        if (!mReady) { // 只有初始化时才需要设置画笔大小
            mSize = DEFAULT_SIZE * mGraffitiSizeUnit;
        }
        // 居中适应屏幕
        mTransX = mTransY = 0;
        mScale = 1;

        invalidate();
    }

    private static int dp2px(Context context, float dp) {
        return (int) (context.getResources().getDisplayMetrics().density * dp + 0.5f);
    }

    /**
     * 获取当前图片在View坐标系中的巨型区域
     */
    public RectF getGraffitiBound() {
        float width = mCenterWidth * mRotateScale * mScale;
        float height = mCenterHeight * mRotateScale * mScale;
        if (mGraffitiRotateDegree % 90 == 0) { // 对0,90,180，270度旋转做简化计算
            if (mGraffitiRotateDegree == 0) {
                mTempPoint.x = toTouchX(0);
                mTempPoint.y = toTouchY(0);
            } else if (mGraffitiRotateDegree == 90) {
                mTempPoint.x = toTouchX(0);
                mTempPoint.y = toTouchY(mBitmap.getHeight());
                float t = width;
                width = height;
                height = t;
            } else if (mGraffitiRotateDegree == 180) {
                mTempPoint.x = toTouchX(mBitmap.getWidth());
                mTempPoint.y = toTouchY(mBitmap.getHeight());
            } else if (mGraffitiRotateDegree == 270) {
                mTempPoint.x = toTouchX(mBitmap.getWidth());
                mTempPoint.y = toTouchY(0);
                float t = width;
                width = height;
                height = t;
            }
            rotatePoint(mTempPoint, mGraffitiRotateDegree, mTempPoint.x, mTempPoint.y, mInner.getWidth() / 2, mInner.getHeight() / 2);
            mGraffitiBound.set(mTempPoint.x, mTempPoint.y, mTempPoint.x + width, mTempPoint.y + height);
        } else {
            // 转换成屏幕坐标
            // 左上
            float ltX = toTouchX(0);
            float ltY = toTouchY(0);
            //右下
            float rbX = toTouchX(mBitmap.getWidth());
            float rbY = toTouchY(mBitmap.getHeight());
            // 左下
            float lbX = toTouchX(0);
            float lbY = toTouchY(mBitmap.getHeight());
            //右上
            float rtX = toTouchX(mBitmap.getWidth());
            float rtY = toTouchY(0);

            //转换到View坐标系
            rotatePoint(mTempPoint, mGraffitiRotateDegree, ltX, ltY, mInner.getWidth() / 2, mInner.getHeight() / 2);
            ltX = mTempPoint.x;
            ltY = mTempPoint.y;
            rotatePoint(mTempPoint, mGraffitiRotateDegree, rbX, rbY, mInner.getWidth() / 2, mInner.getHeight() / 2);
            rbX = mTempPoint.x;
            rbY = mTempPoint.y;
            rotatePoint(mTempPoint, mGraffitiRotateDegree, lbX, lbY, mInner.getWidth() / 2, mInner.getHeight() / 2);
            lbX = mTempPoint.x;
            lbY = mTempPoint.y;
            rotatePoint(mTempPoint, mGraffitiRotateDegree, rtX, rtY, mInner.getWidth() / 2, mInner.getHeight() / 2);
            rtX = mTempPoint.x;
            rtY = mTempPoint.y;

            mGraffitiBound.left = Math.min(Math.min(ltX, rbX), Math.min(lbX, rtX));
            mGraffitiBound.top = Math.min(Math.min(ltY, rbY), Math.min(lbY, rtY));
            mGraffitiBound.right = Math.max(Math.max(ltX, rbX), Math.max(lbX, rtX));
            mGraffitiBound.bottom = Math.max(Math.max(ltY, rbY), Math.max(lbY, rtY));
        }
        return mGraffitiBound;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (mBitmap.isRecycled() || mGraffitiBitmap.isRecycled()) {
            return;
        }

        super.dispatchDraw(canvas);

    }

    private void doDraw(Canvas canvas) {
        float left = getAllTranX();
        float top = getAllTranY();

        // 画布和图片共用一个坐标系，只需要处理屏幕坐标系到图片（画布）坐标系的映射关系
        canvas.translate(left, top); // 偏移画布
        float scale = getAllScale();
        canvas.scale(scale, scale); // 缩放画布

        if (isJustDrawOriginal) {
            canvas.drawBitmap(mBitmap,0,0,null);
            return;
        }

        // 绘制涂鸦后的图片
        canvas.drawBitmap(mGraffitiBitmap, 0, 0, null);//android 8.0 too long time

        boolean canvasClipped = false;
        canvas.save(); // 1
        if (!mIsDrawableOutside) { // 裁剪绘制区域为图片区域
            canvasClipped = true;
            canvas.clipRect(0, 0, mBitmap.getWidth(), mBitmap.getHeight());
        }
        for (IGraffitiItem item : mItemStack) {
            if (item instanceof GraffitiItemBase) {
                if (!((GraffitiItemBase) item).isDrawOptimize()) { //画在view的画布上
                    if (!item.isNeedClipOutside()) { // 1.不需要裁剪
                        if (canvasClipped) {
                            canvas.restore();
                        }

                        ((GraffitiItemBase) item).drawBefore(canvas);
                        item.draw(canvas);
                        ((GraffitiItemBase) item).drawAfter(canvas);

                        if (canvasClipped) { // 2.恢复裁剪
                            canvas.save();
                            canvas.clipRect(0, 0, mBitmap.getWidth(), mBitmap.getHeight());
                        }
                    } else {
                        ((GraffitiItemBase) item).drawBefore(canvas);
                        item.draw(canvas);
                        ((GraffitiItemBase) item).drawAfter(canvas);
                    }
                }
            } else {
                item.draw(canvas);
            }
        }
        canvas.restore();
    }

    public float getAllScale() {
        return mCenterScale * mRotateScale * mScale;
    }

    public float getAllTranX() {
        return mCentreTranX + mRotateTranX + mTransX;
    }

    public float getAllTranY() {
        return mCentreTranY + mRotateTranY + mTransY;
    }

    /**
     * 将屏幕触摸坐标x转换成在图片中的坐标
     */
    public final float toX(float touchX) {
        return (touchX - getAllTranX()) / getAllScale();
    }

    /**
     * 将屏幕触摸坐标y转换成在图片中的坐标
     */
    public final float toY(float touchY) {
        return (touchY - getAllTranY()) / getAllScale();
    }

    /**
     * 将图片坐标x转换成屏幕触摸坐标
     */
    public final float toTouchX(float x) {
        return x * getAllScale() + getAllTranX();
    }

    /**
     * 将图片坐标y转换成屏幕触摸坐标
     */
    public final float toTouchY(float y) {
        return y * getAllScale() + getAllTranY();
    }

    /**
     * 坐标换算
     * （公式由toX()中的公式推算出）
     *
     * @param touchX    触摸坐标
     * @param graffitiX 在涂鸦图片中的坐标
     * @return 偏移量
     */
    public final float toTransX(float touchX, float graffitiX) {
        return -graffitiX * getAllScale() + touchX - mCentreTranX - mRotateTranX;
    }

    public final float toTransY(float touchY, float graffitiY) {
        return -graffitiY * getAllScale() + touchY - mCentreTranY - mRotateTranY;
    }


    private void initCanvas() {
        if (mGraffitiBitmap != null) {
            mGraffitiBitmap.recycle();
        }
        mGraffitiBitmap = mBitmap.copy(Bitmap.Config.RGB_565, true);
        mBitmapCanvas = new Canvas(mGraffitiBitmap);
    }

    /**
     * 设置默认手势识别器
     */
    public void setDefaultTouchDetector(IGraffitiTouchDetector touchGestureDetector) {
        mDefaultTouchDetector = touchGestureDetector;
    }

    // ========================= api ================================

    /**
     * 强制刷新，包括重新刷新涂鸦图片
     */
    public void invalidateForce() {
        initCanvas();
        // 重新绘制到图片上
        for (IGraffitiItem item : mItemStack) {
            if (item instanceof GraffitiItemBase) {
                if (((GraffitiItemBase) item).isDrawOptimize()) { // 优化绘制
                    item.draw(mBitmapCanvas);
                }
            } else {
                item.draw(mBitmapCanvas);
            }
        }
        invalidate();
    }

    @Override
    public int getGraffitiRotation() {
        return mGraffitiRotateDegree;
    }

    /**
     * 相对于初始图片旋转的角度
     */

    @Override
    public void setGraffitiRotation(int degree) {
        mGraffitiRotateDegree = degree;
        mGraffitiRotateDegree = mGraffitiRotateDegree % 360;
        mInner.setPivotX(mInner.getWidth() / 2);
        mInner.setPivotY(mInner.getHeight() / 2);
        mInner.setRotation(mGraffitiRotateDegree);

        // 居中
        RectF rectF = getGraffitiBound();
        int w = (int) (rectF.width() / getAllScale());
        int h = (int) (rectF.height() / getAllScale());
        float nw = w * 1f / getWidth();
        float nh = h * 1f / getHeight();
        float scale;
        float tx, ty;
        if (nw > nh) {
            scale = 1 / nw;
        } else {
            scale = 1 / nh;
        }

        int pivotX = mBitmap.getWidth() / 2;
        int pivotY = mBitmap.getHeight() / 2;

        mTransX = mTransY = 0;
        mRotateTranX = mRotateTranY = 0;
        this.mScale = 1;
        mRotateScale = 1;
        float touchX = toTouchX(pivotX);
        float touchY = toTouchY(pivotY);
        mRotateScale = scale / mCenterScale;

        // 缩放后，偏移图片，以产生围绕某个点缩放的效果
        tx = toTransX(touchX, pivotX);
        ty = toTransY(touchY, pivotY);

        mRotateTranX = tx;
        mRotateTranY = ty;

        notifyActionOccur(ACTION_ROTATION, null);

        invalidate();
    }

    @Override
    public boolean undo(int step) {

        if (mItemStack.size() > 0) {
            step = Math.min(mItemStack.size(), step);
            IGraffitiItem item = mItemStack.get(mItemStack.size() - step);
            removeItem(item);
            notifyActionOccur(ACTION_UNDO, item);
            mGraffitiListener.isUndo(mItemStack.size() > 0);
            return true;
        }
        return false;
    }

    @Override
    public boolean isUndo() {
        return mItemStack.size() > 0;
    }

    /**
     * 撤销
     */
    @Override
    public boolean undo() {
        return undo(1);
    }

    /**
     * 设置画笔底色
     */
    @Override
    public void setColor(IGraffitiColor color) {
        mColor = color;
        invalidate();
    }

    @Override
    public IGraffitiColor getColor() {
        return mColor;
    }

    /**
     * 围绕某个点缩放
     * 图片真实的缩放倍数为 mCenterScale*mScale
     *
     * @param pivotX 缩放的中心点
     */
    @Override
    public void setGraffitiScale(float scale, float pivotX, float pivotY) {
        if (scale < mMinScale) {
            scale = mMinScale;
        } else if (scale > mMaxScale) {
            scale = mMaxScale;
        }

        float touchX = toTouchX(pivotX);
        float touchY = toTouchY(pivotY);
        this.mScale = scale;

        // 缩放后，偏移图片，以产生围绕某个点缩放的效果
        mTransX = toTransX(touchX, pivotX);
        mTransY = toTransY(touchY, pivotY);

        invalidate();
    }

    @Override
    public float getGraffitiScale() {
        return mScale;
    }

    @Override
    public IGraffitiPen getPen() {
        return mPen;
    }

    /**
     * 设置画笔形状
     */
    @Override
    public void setShape(IGraffitiShape shape) {
        if (shape == null) {
            throw new RuntimeException("Shape can't be null");
        }
        mShape = shape;
        invalidate();
    }

    @Override
    public IGraffitiShape getShape() {
        return mShape;
    }

    @Override
    public void setGraffitiTranslation(float transX, float transY) {
        mTransX = transX;
        mTransY = transY;
        invalidate();
    }

    /**
     * 设置图片G偏移
     */
    @Override
    public void setGraffitiTranslationX(float transX) {
        this.mTransX = transX;
        invalidate();
    }

    @Override
    public float getGraffitiTranslationX() {
        return mTransX;
    }

    @Override
    public void setGraffitiTranslationY(float transY) {
        this.mTransY = transY;
        invalidate();
    }

    @Override
    public float getGraffitiTranslationY() {
        return mTransY;
    }


    @Override
    public void setSize(float paintSize) {
        mSize = paintSize;
        invalidate();
    }

    @Override
    public float getSize() {
        return mSize;
    }

    /**
     * 触摸时，图片区域外是否绘制涂鸦轨迹
     */
    @Override
    public void setIsDrawableOutside(boolean isDrawableOutside) {
        mIsDrawableOutside = isDrawableOutside;
    }

    /**
     * 是否只绘制原图
     */
    @Override
    public void setIsJustDrawOriginal(boolean isJustDrawOriginal) {
        this.isJustDrawOriginal = isJustDrawOriginal;
    }

    @Override
    public void setGraffitiMinScale(float minScale) {
        mMinScale = minScale;
        setGraffitiScale(mScale, 0, 0);
    }

    @Override
    public float getGraffitiMinScale() {
        return mMinScale;
    }

    @Override
    public void setGraffitiMaxScale(float maxScale) {
        mMaxScale = maxScale;
        setGraffitiScale(mScale, 0, 0);
    }

    @Override
    public float getGraffitiMaxScale() {
        return mMaxScale;
    }

    @Override
    public float getSizeUnit() {
        return mGraffitiSizeUnit;
    }

    @Override
    public void addItem(IGraffitiItem graffitiItem) {
        if (this != graffitiItem.getGraffiti()) {
            throw new RuntimeException("the object Graffiti is illegal");
        }
        if (mItemStack.contains(graffitiItem)) {
            throw new RuntimeException("the item has been added");
        }
        mItemStack.add(graffitiItem);
        mGraffitiListener.isUndo(true);
        graffitiItem.onAdd();

        if (((GraffitiItemBase) graffitiItem).isDrawOptimize()) { // // 优化绘制
            graffitiItem.draw(mBitmapCanvas); // 提前保存到图片中
        }
        invalidate();
    }

    @Override
    public void removeItem(IGraffitiItem graffitiItem) {
        if (!mItemStack.remove(graffitiItem)) {
            return;
        }
        graffitiItem.onRemove();

        if (graffitiItem instanceof GraffitiItemBase &&
                ((GraffitiItemBase) graffitiItem).isDrawOptimize()) { // 由于优化绘制，需要重新绘制抹掉图片上的痕迹
            invalidateForce();
        }
        invalidate();
    }

    @Override
    public List<IGraffitiItem> getAllItem() {
        return mItemStack;
    }

    @Override
    public Bitmap getBitmap() {
        return mBitmap;
    }

    @Override
    public Bitmap getGraffitiBitmap() {
        return mGraffitiBitmap;
    }

    @Override
    public Bitmap save() {
        for (IGraffitiItem item : mItemStack) {
            if (item instanceof GraffitiItemBase) {
                if (!((GraffitiItemBase) item).isDrawOptimize()) { // 最终保存到图片上
                    item.draw(mBitmapCanvas);
                }
            }
        }
        mGraffitiBitmap = rotate(mGraffitiBitmap, mGraffitiRotateDegree, true);
        return mGraffitiBitmap;
    }

    @Override
    public void clear() {
        mItemStack.clear();
        mGraffitiRotateDegree = 0;
    }

    /**
     * 添加回调
     */
    public void addGraffitiViewListener(IGraffitiViewListener listener) {
        if (listener == null || mListenerList.contains(listener)) {
            return;
        }
        mListenerList.add(new WeakReference<>(listener));
    }

    /**
     * 移除回调
     */
    public void removeGraffitiViewListener(IGraffitiViewListener listener) {
        IGraffitiViewListener callBack;
        for (WeakReference<IGraffitiViewListener> ref : mListenerList) {
            callBack = ref.get();
            if (callBack == null || callBack == listener) {
                mListenerList.remove(ref);
            }
        }
    }


    public int getCenterWidth() {
        return mCenterWidth;
    }

    public int getCenterHeight() {
        return mCenterHeight;
    }

    public float getRotateScale() {
        return mRotateScale;
    }

    private void notifyActionOccur(int action, Object obj) {
        IGraffitiViewListener callBack;
        for (WeakReference<IGraffitiViewListener> ref : mListenerList) {
            callBack = ref.get();
            if (callBack != null) {
                callBack.onActionOccur(action, obj);
            } else { // 刪除无用的引用
                mListenerList.remove(ref);
            }
        }
    }

    /**
     * 监听涂鸦中的事件
     */
    public interface IGraffitiViewListener {

        /**
         * 操作发生后回调
         *
         * @param obj 动作相关的对象信息
         */
        void onActionOccur(int action, Object obj);
    }

    private class GraffitiViewInner extends View {

        public GraffitiViewInner(Context context) {
            super(context);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            return super.dispatchTouchEvent(event);
        }


        @Override
        public boolean onTouchEvent(MotionEvent event) {
            // 默认识别器
            if (mDefaultTouchDetector != null) {
                return mDefaultTouchDetector.onTouchEvent(event);
            }
            return super.onTouchEvent(event);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.save();
            doDraw(canvas);
            canvas.restore();
        }
    }
}
