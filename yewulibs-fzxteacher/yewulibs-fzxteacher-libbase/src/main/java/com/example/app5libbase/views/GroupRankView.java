package com.example.app5libbase.views;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app5libbase.R;

public class GroupRankView extends RelativeLayout {

    private TextView tvLogo1;
    private TextView tvGroup1;
    private TextView tvLogo2;
    private TextView tvGroup2;
    private TextView tvLogo3;
    private TextView tvGroup3;
    private TextView tvTitle;

    public GroupRankView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.view_group_rank_view, this, true);
        tvLogo1 = findViewById(R.id.tvLogo1);
        tvGroup1 = findViewById(R.id.tvGroup1);
        tvLogo2 = findViewById(R.id.tvLogo2);
        tvGroup2 = findViewById(R.id.tvGroup2);
        tvLogo3 = findViewById(R.id.tvLogo3);
        tvGroup3 = findViewById(R.id.tvGroup3);
        tvTitle = findViewById(R.id.tvTitle);
    }

    public void setGroup1(String group) {
        if (!TextUtils.isEmpty(group)) {
            tvLogo1.setText(group.substring(0, 1));
            tvGroup1.setText(group);
        } else {
            tvLogo1.setText("无");
            tvGroup1.setText("暂无");
        }
    }

    public void setGroup2(String group) {
        if (!TextUtils.isEmpty(group)) {
            tvLogo2.setText(group.substring(0, 1));
            tvGroup2.setText(group);
        } else {
            tvLogo2.setText("无");
            tvGroup2.setText("暂无");
        }
    }

    public void setGroup3(String group) {
        if (!TextUtils.isEmpty(group)) {
            tvLogo3.setText(group.substring(0, 1));
            tvGroup3.setText(group);
        } else {
            tvLogo3.setText("无");
            tvGroup3.setText("暂无");
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
    }
}
