package com.example.app5libbase.views;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.example.app5libbase.R;
import com.example.app5libbase.listener.OnCalenderSelectListener;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.tubb.calendarselector.library.CalendarSelector;
import com.tubb.calendarselector.library.FullDay;
import com.tubb.calendarselector.library.MonthView;
import com.tubb.calendarselector.library.SCDateUtils;
import com.tubb.calendarselector.library.SCMonth;
import com.tubb.calendarselector.library.SegmentSelectListener;

import org.xutils.x;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CalenderDialog {

    private RecyclerView rvCalendar;

    private CustomDialog dialog;
    private LinearLayoutManager linearLayoutManager;
    private List<SCMonth> data;

    private CalendarSelector selector;

    private FullDay selStartDay;
    private FullDay selEndDay;

    public CalenderDialog(Activity activity, OnCalenderSelectListener onCalenderSelectListener) {
        View contentView = initCalendarView(activity);
        initDialog(activity, contentView, onCalenderSelectListener);
    }

    private void initDialog(Activity activity, View contentView, final OnCalenderSelectListener onCalenderSelectListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setTitle("操作日期");
        builder.setContentView(contentView);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selStartDay == null && selEndDay == null) {
                    ToastUtil.showShortlToast("请选择有效的日期");
                    return;
                }
                Calendar startCalendar = Calendar.getInstance(Locale.CHINA);
                startCalendar.set(selStartDay.getYear(), selStartDay.getMonth() - 1, selStartDay.getDay());
                Calendar endCalendar = Calendar.getInstance(Locale.CHINA);
                endCalendar.set(selEndDay.getYear(), selEndDay.getMonth() - 1, selEndDay.getDay());
                onCalenderSelectListener.onCalenderSelect(startCalendar, endCalendar);
                dialog.dismiss();
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
    }

    private View initCalendarView(Activity activity) {
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_calender, null);
        rvCalendar = contentView.findViewById(R.id.rvCalendar);
//        x.view().inject(this, contentView);
        linearLayoutManager = new LinearLayoutManager(activity);
        rvCalendar.setLayoutManager(linearLayoutManager);
        ((SimpleItemAnimator) rvCalendar.getItemAnimator()).setSupportsChangeAnimations(false);
        data = getData();
        segmentMode();
        return contentView;
    }

    public List<SCMonth> getData() {
        List<SCMonth> months = SCDateUtils.generateMonths(1970, 2020, SCMonth.SUNDAY_OF_WEEK);
        int currentMonth = Calendar.getInstance(Locale.CHINA).get(Calendar.MONTH) + 1;
        List<SCMonth> removeMonths = new ArrayList<>();
        for (int i = 0; i < 12 - currentMonth; i++) {
            removeMonths.add(months.get(months.size() - 1 - i));
        }
        months.removeAll(removeMonths);
        return months;
    }

    /**
     * segment mode
     */
    private void segmentMode() {
        data = getData();

        selector = new CalendarSelector(data, CalendarSelector.SEGMENT);
        selector.setSegmentSelectListener(new SegmentSelectListener() {
            @Override
            public void onSegmentSelect(FullDay startDay, FullDay endDay) {
                selStartDay = startDay;
                selEndDay = endDay;
            }

            @Override
            public boolean onInterceptSelect(FullDay selectingDay) { // one day intercept
                if (selStartDay != null && selEndDay != null) {
                    selStartDay = null;
                    selEndDay = null;
                }
                return super.onInterceptSelect(selectingDay);
            }

            @Override
            public boolean onInterceptSelect(FullDay startDay, FullDay endDay) { // segment days intercept
                return super.onInterceptSelect(startDay, endDay);
            }

            @Override
            public void selectedSameDay(FullDay sameDay) { // selected the same day
                super.selectedSameDay(sameDay);
            }
        });
        rvCalendar.setAdapter(new CalendarAdpater(data));
        moveToPosition(linearLayoutManager, data.size() - 1);
    }

    /**
     * RecyclerView 移动到当前位置，
     *
     * @param manager 设置RecyclerView对应的manager
     * @param n       要跳转的位置
     */
    public void moveToPosition(LinearLayoutManager manager, int n) {
        manager.scrollToPositionWithOffset(n, 0);
        manager.setStackFromEnd(true);
    }

    class CalendarAdpater extends RecyclerView.Adapter<CalendarViewHolder> {

        List<SCMonth> months;

        public CalendarAdpater(List<SCMonth> months) {
            this.months = months;
        }

        @Override
        public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CalendarViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar, parent, false));
        }

        @Override
        public void onBindViewHolder(CalendarViewHolder holder, int position) {
            SCMonth scMonth = months.get(position);
            holder.tvMonthTitle.setText(String.format("%d年 %d月", scMonth.getYear(), scMonth.getMonth()));
            holder.monthView.setSCMonth(scMonth);
            selector.bind(rvCalendar, holder.monthView, position);
        }

        @Override
        public int getItemCount() {
            return months.size();
        }
    }

    class CalendarViewHolder extends RecyclerView.ViewHolder {

        TextView tvMonthTitle;
        MonthView monthView;

        public CalendarViewHolder(View itemView) {
            super(itemView);
            tvMonthTitle = itemView.findViewById(R.id.tvMonthTitle);
            monthView = itemView.findViewById(R.id.ssMv);
        }
    }

    public void show() {
        if (dialog != null) {
            dialog.show();
        }
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
