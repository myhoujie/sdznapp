package com.example.app5libbase.util;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;

import java.io.File;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：鲍圣祥
 * 创建时间：2016/12/2
 */
public class OpenFileUtil {

    public static int openAppNum(Intent intent) {
        PackageManager pManager =  App2.get().getPackageManager();
        List list = pManager.queryIntentActivities(intent, 0);
        return list.size();
    }


    public static Intent getAttachmentIntent(File file, String filetype) {
        switch (filetype.toLowerCase()) {
            case "doc":
            case "docx":
                return getWordFileIntent(file);
            case "pdf":
                return getPdfFileIntent(file);
            case "image":
            case "jpg":
            case "gif":
            case "png":
            case "jpeg":
            case "bmp":
                return getImageFileIntent(file);
            case "txt":
                return getTextFileIntent(file);
            case "xls":
            case "xlsx":
                return getExcelFileIntent(file);
            case "ppt":
            case "pptx":
                return getPptFileIntent(file);
            case "html":
            case "htm":
                return getHtmlFileIntent(file);
            default:
                return getDefaultFileIntent(file);
        }
    }

    private static Intent getWordFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = getUriForFile(file);
        intent.setDataAndType(uri, "application/msword");
        return intent;
    }

    private static Intent getHtmlFileIntent(String content) {
        Uri uri = Uri.parse(content).buildUpon().encodedAuthority("com.android.htmlfileprovider").scheme("content").encodedPath(content).build();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "text/html");
        return intent;
    }

    private static Intent getTextFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = getUriForFile(file);
        intent.setDataAndType(uri, "text/plain");
        return intent;
    }

    private static Intent getImageFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = getUriForFile(file);
        intent.setDataAndType(uri, "image/*");
        return intent;
    }

    private static Intent getPdfFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Uri uri = getUriForFile(file);
        intent.setDataAndType(uri, "application/pdf");
        return intent;
    }

    //Android获取一个用于打开Excel文件的intent
    private static Intent getExcelFileIntent(File file) {
        Uri uri = getUriForFile(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, "application/vnd.ms-excel");
        return intent;
    }

    //Android获取一个用于打开PPT文件的intent
    private static Intent getPptFileIntent(File file) {
        Uri uri = getUriForFile(file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        return intent;
    }

    private static Intent getHtmlFileIntent(File file) {
        Uri uri = Uri.parse(file.toString()).buildUpon().encodedAuthority("com.android.htmlfileprovider").scheme("content").encodedPath(file.toString()).build();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "text/html");
        return intent;
    }

    private static Intent getDefaultFileIntent(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = getUriForFile(file);
        intent.setDataAndType(uri, "");
        return intent;
    }

    public static Uri getUriForFile(File file) {
        Uri uri;
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile( App2.get(), BuildConfig3.APPLICATION_ID + ".fileProvider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        return uri;
    }
}
