package com.example.app5libbase.views.whiteboard;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.example.app5libbase.R;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/6/13
 * 修改单号：
 * 修改内容:
 */
public class PopView {
    static onColorClick onColorClick = null;
    static onPenClick onPenClick = null;
    static onTuXingClick onTuXingClick= null;
    static onXiangPiClick onXiangPiClick= null;

    public static void setOnColorClick(onColorClick onClick) {
        onColorClick = onClick;
    }
    public static void setOnPenClick(onPenClick onPen) {
        onPenClick = onPen;
    }
    public static void setonTuXingClick(onTuXingClick onTuXing) {
        onTuXingClick = onTuXing;
    }
    public static void setonXinagPiClick(onXiangPiClick onTuXing) {
        onXiangPiClick = onTuXing;
    }
    public static PopupWindow showColorPopupWindow(View view, final Activity activity) {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.popup_color, null);

        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        contentView.findViewById(R.id.rbColorWhite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClick.onClickColor(1);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorRed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClick.onClickColor(2);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorBlack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClick.onClickColor(3);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorYellow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClick.onClickColor(4);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorGreen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClick.onClickColor(5);
                popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
        // popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return popupWindow;
    }

    public interface onColorClick {
        void onClickColor(int color);
    }

    public static PopupWindow showPenPopupWindow(View view, final Activity activity) {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.popup_pen, null);

        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        contentView.findViewById(R.id.rbColorWhite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(6);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorRed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(8);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorBlack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(10);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorYellow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(12);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorGreen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(14);
               popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
        // popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return popupWindow;
    }
    public interface onPenClick {
        void onClickPen(int size);
    }

    public static PopupWindow showtuxingPopupWindow(View view, final Activity activity) {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.popup_tuxing, null);

        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        contentView.findViewById(R.id.rbColorWhite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTuXingClick.onClickTuXing(1);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorRed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTuXingClick.onClickTuXing(2);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorBlack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTuXingClick.onClickTuXing(3);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorYellow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTuXingClick.onClickTuXing(4);
                popupWindow.dismiss();
            }
        });

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
        // popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return popupWindow;
    }
    public interface onTuXingClick {
        void onClickTuXing(int size);
    }

    public static PopupWindow showXinagpiPopupWindow(View view, final Activity activity) {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.popup_xiangpi, null);

        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        contentView.findViewById(R.id.rbSize1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(6);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbSize2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(8);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbSize3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(10);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbSize4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(12);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbSize5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(50);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbSize5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onXiangPiClick.onClickTuXing(100);
                popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
        // popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return popupWindow;
    }
    public interface onXiangPiClick {
        void onClickTuXing(int size);
    }
}
