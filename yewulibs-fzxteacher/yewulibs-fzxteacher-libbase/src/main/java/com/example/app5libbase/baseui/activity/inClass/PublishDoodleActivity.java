package com.example.app5libbase.baseui.activity.inClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.chatroom.ImageUploadInfoBean;
import com.sdzn.fzx.teacher.vo.chatroom.UploadPicVo;
import com.example.app5libbase.controller.UserController;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.example.app5libbase.util.ProgressDialogManager;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.SpinerVersionWindow;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.sdzn.fzx.teacher.vo.NodeBean;
import com.sdzn.fzx.teacher.vo.QiniuUptoken;
import com.sdzn.fzx.teacher.vo.VersionBean;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import rain.coder.photopicker.bean.Photo;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 保存板书
 */
public class PublishDoodleActivity extends Activity implements View.OnClickListener {

    VersionBean dealVersionBean;
    List<NodeBean.DataBeanXX> data;
    TextView tvClass1;


    RelativeLayout rlTvClass1;
    TextView tvClass;
    RelativeLayout rlTvClass;
    EditText publishObject;
    LinearLayout cancel;
    LinearLayout confirm;
    private SpinerVersionWindow classChoosePop, classChooseTwo;
    VersionBean.DataBean.VolumeListBean volumeListBean;
    private String pathStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_doodle);
        tvClass1 = findViewById(R.id.tvClass1);
        rlTvClass1 = findViewById(R.id.rl_tvClass1);
        tvClass = findViewById(R.id.tvClass);
        rlTvClass = findViewById(R.id.rl_tvClass);
        publishObject = findViewById(R.id.publish_object);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        rlTvClass1.setOnClickListener(this);
        rlTvClass.setOnClickListener(this);
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);
        pathStr = getIntent().getStringExtra("path");
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PublishDoodleActivity成功");
                }
            }
        }
    }

    private String baseVersionName, baseVersionId, baseGradeName, baseGradeId, baseVolumeName, baseVolumeId, chapterNamePath, chapterId, chapterName, chapterIdPath;


    private void initView() {
        classChoosePop = new SpinerVersionWindow(PublishDoodleActivity.this);
        classChoosePop.setItemListener(new SpinerVersionWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (dealVersionBean == null) {
                    return;
                }
                onVersionChoose(dealVersionBean.getData().get(0).getVolumeList().get(pos));
                // volumeListBean=dealVersionBean.getData()
                tvClass1.setText(dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVersionName() + dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseGradeName() + dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVolumeName());
                baseVersionName = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVersionName();
                baseVersionId = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVersionId();
                baseGradeName = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseGradeName();
                baseGradeId = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseGradeId();
                baseVolumeName = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVolumeName();
                baseVolumeId = dealVersionBean.getData().get(0).getVolumeList().get(pos).getBaseVolumeId();
            }
        });


        classChooseTwo = new SpinerVersionWindow(PublishDoodleActivity.this);
        classChooseTwo.setItemListener(new SpinerVersionWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                if (data == null) {
                    return;
                }

                // volumeListBean=dealVersionBean.getData()
                tvClass.setText(data.get(pos).getData().getName());
                chapterNamePath = data.get(pos).getData().getNodeNamePath();//nodenamepath
                chapterName = data.get(pos).getData().getName();
                chapterIdPath = data.get(pos).getData().getNodeIdPath();
                chapterId = String.valueOf(data.get(pos).getData().getId());
            }
        });

    }

    private void initData() {
        uploadSubjectivePhoto(pathStr);
        getVersionList();
    }

    public void getVersionList() {
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getVersionList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<VersionBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<VersionBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(VersionBean versionBean) {
                        dealVersionBean = versionBean;
                        VersionBean.DataBean.VolumeListBean volumeListBean = new VersionBean.DataBean.VolumeListBean();
                        volumeListBean.setBaseVersionName("全部册别");
                        volumeListBean.setBaseGradeName("");
                        volumeListBean.setBaseVolumeName("");
                        versionBean.getData().get(0).getVolumeList().add(0, volumeListBean);

                        ArrayList<VersionBean.DataBean.VolumeListBean> retList = new ArrayList<>();

                        for (int i = 0; i < versionBean.getData().size(); i++) {
                            retList.addAll(versionBean.getData().get(i).getVolumeList());
                        }

                        List<String> list = new ArrayList<>();
                        for (int i = 0; i < retList.size(); i++) {
                            list.add(retList.get(i).getBaseVersionName() + retList.get(i).getBaseGradeName() + retList.get(i).getBaseVolumeName());
                        }
                        classChoosePop.setPopDatas(list);
                    }
                });

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_tvClass1) {
            classChoosePop.showAsDropDown(rlTvClass1);
        } else if (id == R.id.rl_tvClass) {
            classChooseTwo.showAsDropDown(rlTvClass);
        } else if (id == R.id.cancel) {
            finish();
        } else if (id == R.id.confirm) {
            saveData();
        }
    }


    private void onVersionChoose(VersionBean.DataBean.VolumeListBean bean) {
        getNodeList(bean.getBaseVersionId(), bean.getBaseGradeId(), bean.getBaseVolumeId());
    }

    private List<NodeBean.DataBeanXX> nodeList;

    public void getNodeList(final String baseVersion, String BaseGradeId, String BaseVolumeId) {
        String subject = SubjectSPUtils.getCurrentSubject().getSubjectId() + "";
        final String baseLevelId = SPUtils.getLoginBean().getData().getUser().getBaseLevelId() + "";
        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getNodeList(subject, baseLevelId, BaseGradeId, baseVersion, BaseVolumeId)
                .map(new StatusFunc<NodeBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<NodeBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(NodeBean o) {
                        data = o.getData();

                        NodeBean.DataBeanXX dataBeanXX = new NodeBean.DataBeanXX();
                        NodeBean.DataBeanXX.DataBean dataBean = new NodeBean.DataBeanXX.DataBean();
                        dataBean.setName("全部章节");
                        dataBean.setNodeNamePath("全部章节");
                        dataBean.setLeaf(true);
                        dataBeanXX.setData(dataBean);
                        data.add(0, dataBeanXX);

                        List<String> list = new ArrayList<>();
                        for (int i = 0; i < data.size(); i++) {
                            list.add(data.get(i).getData().getName());
                        }
                        classChooseTwo.setPopDatas(list);
                        tvClass.setText(list.get(0));
                        chapterId = "";
                    }
                });


    }

    private void saveData() {
        if (baseVersionId == null || baseVersionId.isEmpty()) {
            ToastUtil.showShortlToast("请选择册别");
        } else if (chapterId == null || chapterId.isEmpty()) {
            ToastUtil.showShortlToast("请选择章节");
        } else if (publishObject.getText().toString().isEmpty()) {
            ToastUtil.showShortlToast("请填写名称");
        } else if (key.isEmpty() || "".equals(key)) {
            ToastUtil.showShortlToast("截屏图片上传失败");
        } else {
            Map<String, Object> param = new HashMap<>();
            param.put("resourceType", "4");
            param.put("baseSubjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
            param.put("baseSubjectName", SubjectSPUtils.getCurrentSubject().getSubjectName());
            param.put("subjectStructureId", "2");
            param.put("subjectStructureName", "新授课学案");
            param.put("subjectStructureIdPath", "");
            param.put("scene", "1");
            param.put("chapterIdPath", chapterIdPath);//---nodeidpath
            param.put("baseEducationId", "1");
            param.put("baseEducationName", "六三制");
            param.put("chapterName", chapterName);//---getname

            param.put("chapterId", chapterId);//---
            param.put("chapterNamePath", chapterNamePath);//---
            param.put("baseGradeId", baseGradeId);
            param.put("baseGradeName", baseGradeName);
            param.put("baseVersionId", baseVersionId);
            param.put("baseVersionName", baseVersionName);
            param.put("baseVolumeId", baseVolumeId);
            param.put("baseVolumeName", baseVolumeName);
            param.put("resourceName", publishObject.getText().toString() + ".jpg");
            param.put("key", key);

            Network.createTokenService(NetWorkService.SaveDoodleService.class)
                    .publishDoodle(param)
                    .map(new StatusFunc<Object>())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof ApiException) {
                                ApiException apiException = (ApiException) e;
                                StatusVo status = apiException.getStatus();
                                if (status != null && status.getMsg() != null) {
                                    ToastUtil.showLonglToast(status.getMsg());
                                } else {

                                }
                            } else {

                            }
                        }

                        @Override
                        public void onNext(Object o) {
                            ToastUtil.showShortlToast("保存成功");
                            PublishDoodleActivity.this.finish();
                        }
                    }, this, true, false));
        }
    }

    private String urlStr = "";

    private void getUrlStr(UploadPicVo mUploadPicVo) {
        if (mUploadPicVo.getData().size() > 0) {
            urlStr = mUploadPicVo.getData().get(0).getOriginalPath();
        }

    }

    private String key = "";

    /**
     * 上传七牛 获取地址
     */
    public void uploadSubjectivePhoto(String path) {
        final List<Photo> photoLists = new ArrayList<>();
        Photo photo = new Photo();
        photo.setPath(path);
        photoLists.add(photo);
        final ProgressDialogManager pdm = new ProgressDialogManager(this);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("");//压缩中...
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
                key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
                        throw new RuntimeException("");//上传失败: 获取upToken失败
                    }
                    QiniuUptoken.ResultBean bean = uptoken.getResult();
                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        ToastUtil.showShortlToast(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
//                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create(bean.getDomain(), key, bean.getImageStyle());

                                    uploadPicVoList.add(dataBean);

                                    if (uploadPicVoList.size() == photoLists.size()) {
                                        pdm.cancelWaiteDialog();
                                        UploadPicVo mUploadPicVo = new UploadPicVo();
                                        mUploadPicVo.setData(uploadPicVoList);
                                        getUrlStr(mUploadPicVo);
                                    }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    ToastUtil.showShortlToast(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     *
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


}
