package com.example.app5libbase.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.example.app5libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Timer〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class Timer {

    private Context context;

    public static final int TAG = 1;
    private int delayTime;

    public Timer(Context context) {
        this.context = context;
    }

    public void start(int time) {
        delayTime = time;
        timerHandler.removeMessages(TAG);
        timerHandler.sendEmptyMessageDelayed(TAG, delayTime);
    }

    public void stop() {
        timerHandler.removeMessages(TAG);
    }

    private Handler timerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final boolean netConnectOk = AndroidUtil.isNetConnectOk(context);
            EventBus.getDefault().post(new NetworkReceiver.NetworkEvent(netConnectOk));
            this.sendEmptyMessageDelayed(TAG, delayTime);
        }
    };


}
