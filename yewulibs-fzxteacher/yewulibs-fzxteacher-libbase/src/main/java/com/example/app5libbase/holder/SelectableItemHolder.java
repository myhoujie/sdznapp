package com.example.app5libbase.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.unnamed.b.atv.model.TreeNode;


/**
 * Created by Bogdan Melnychuk on 2/15/15.
 */
public class SelectableItemHolder extends TreeNode.BaseNodeViewHolder<String> {
    private TextView tvValue;
    private CheckBox nodeSelector;
    private TextView TextView, WrongCountTv, ScoreTv, AccuracyTv;

    private String QuestionCount;
    private String WrongCount;
    private String Score;
    private String Accuracy;

    public SelectableItemHolder(Context context) {
        super(context);
    }

    public SelectableItemHolder(Context context, String pointWrongCount, String pointScore, String pointAccuracy, String pointQuestionCount) {
        super(context);
        QuestionCount = pointQuestionCount;
        WrongCount = pointWrongCount;
        Score = pointScore;
        Accuracy = pointAccuracy;
    }

    @Override
    public View createNodeView(final TreeNode node, String value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_selectable_item, null, false);

        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value);

        TextView = (TextView) view.findViewById(R.id.error_num);
        TextView.setText(QuestionCount);


        WrongCountTv = (TextView) view.findViewById(R.id.WrongCount);
        WrongCountTv.setText(WrongCount);


        ScoreTv = (TextView) view.findViewById(R.id.Score);
        ScoreTv.setText(Score);

        AccuracyTv = (TextView) view.findViewById(R.id.Accuracy);
        AccuracyTv.setText(Accuracy);


        nodeSelector = (CheckBox) view.findViewById(R.id.node_selector);
        nodeSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                node.setSelected(isChecked);
            }
        });
        nodeSelector.setChecked(node.isSelected());

        if (node.isLastChild()) {
            view.findViewById(R.id.bot_line).setVisibility(View.INVISIBLE);
        }

        return view;
    }


    @Override
    public void toggleSelectionMode(boolean editModeEnabled) {
        nodeSelector.setVisibility(editModeEnabled ? View.GONE : View.GONE);
        nodeSelector.setChecked(mNode.isSelected());
    }
}
