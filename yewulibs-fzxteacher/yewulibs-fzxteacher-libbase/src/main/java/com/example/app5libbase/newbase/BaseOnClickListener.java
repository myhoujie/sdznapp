package com.example.app5libbase.newbase;

public interface BaseOnClickListener {
    //个人中心
    void Titlegrzx();

    //时间
    void Titleshijian();

    //展开时间
    void Titlezankaishijian();

    /*删除*/
    void TitleRemove();

    /*搜索*/
    void Titlesousuo();

    /*提交*/
    void Titletijiao();

    /*下拉加载*/
    void TitleDropdown();

    /*返回*/
    void TitleBack();

    /*添加*/
    void TitleAdd();

    /*更多*/
    void TitleMore();

    /*重新批改*/
    void TitleChongxinpg();

    /*发布批改*/
    void TitleFabupg();
}
