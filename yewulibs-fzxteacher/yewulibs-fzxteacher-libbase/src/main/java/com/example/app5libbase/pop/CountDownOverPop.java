package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import com.example.app5libbase.R;
import com.example.app5libpublic.constant.Constant;

import java.lang.reflect.Method;

public class CountDownOverPop extends PopupWindow implements View.OnClickListener {

    private Activity activity;
    private Button btnClose;

    public CountDownOverPop(final Activity activity) {
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_tools_countdownover, null);
        btnClose = (Button) contentView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setOutsideTouchable(false);
        this.setClippingEnabled(false);
        this.setPopupWindowTouchModal(this, false);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btnClose) {// 关闭倒计时
            dismissPop();
            Constant.setShowCountDown(false);
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(final View parent) {
        if (!this.isShowing()) {
            this.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public void dismissPop() {
        if (this.isShowing()) {
            this.dismiss();
        }
    }

    private void setPopupWindowTouchModal(PopupWindow popupWindow, boolean touchModal) {
        if (null == popupWindow) {
            return;
        }
        try {
            Method method = PopupWindow.class.getDeclaredMethod("setTouchModal",
                    boolean.class);
            method.setAccessible(true);
            method.invoke(popupWindow, touchModal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
