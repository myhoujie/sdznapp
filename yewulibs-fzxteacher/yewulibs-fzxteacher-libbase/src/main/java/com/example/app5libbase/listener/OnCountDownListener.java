package com.example.app5libbase.listener;

import com.example.app5libbase.views.datepick.CountDownPickWheelDialog;

public interface OnCountDownListener {
    void onClick(CountDownPickWheelDialog dialog);
}
