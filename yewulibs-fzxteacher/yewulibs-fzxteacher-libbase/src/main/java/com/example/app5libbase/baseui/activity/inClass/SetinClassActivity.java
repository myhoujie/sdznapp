package com.example.app5libbase.baseui.activity.inClass;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libpublic.event.LessonDetailsGoClassEvent;
import com.example.app5libbase.listener.OnItemGoClassIdlistener;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.datefive.CustomGoClassDatePicker;
import com.example.app5libbase.views.NoScrollGridView;
import com.example.app5libpublic.constant.Constant;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SetinClassActivity extends Activity implements OnItemGoClassIdlistener, View.OnClickListener {

    NoScrollGridView publishObject;
    TextView dateChooseTxt;
    TextView tvDiffer;
    LinearLayout cancel;
    LinearLayout confirm;


    private String endTime = "";
    private String dateTime = "";
    private CustomGoClassDatePicker customDatePicker2;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

    private String setClassId = "";
    private int differTime = 0;
    private String classTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setin_class);
        publishObject = findViewById(R.id.publish_object);
        dateChooseTxt = findViewById(R.id.date_choose_txt);
        tvDiffer = findViewById(R.id.tv_differ);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        dateChooseTxt.setOnClickListener(this);
        initData();
        TextView tv_cancel = findViewById(R.id.tv_cancel);
        TextView tv_confirm = findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (differTime == 0) {
                    ToastUtil.showShortlToast("请选择下课时间");
                } else if (differTime < 0) {
                    ToastUtil.showLonglToast("设置时间请大于当前时间");
                } else if (setClassId.isEmpty() || "".equals(setClassId)) {
                    ToastUtil.showShortlToast("请选择上课班级");
                } else {
                    getBehavior(1);

                }
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        publishObject.setAdapter(myAdapter);
        publishObject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                departmentSelectedPosition = i;
                myAdapter.notifyDataSetChanged();
                /*
                赋值的数据
                 */
                setClassId = mList.get(departmentSelectedPosition).getClassId();
            }
        });
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入SetinClassActivity成功");
                }
            }
        }
    }


    private void show() {
//        CountDownPickWheelDialog countDownPickWheelDialog = new CountDownPickWheelDialog.Builder(this)
//                .setPositiveButton(new OnCountDownListener() {
//                    @Override
//                    public void onClick(CountDownPickWheelDialog dialog) {
//                        if (dialog.getTime() != 0) {
//                            BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
//                            if (foregroundActivity != null) {
//                                dateChooseTxt.setText(DateUtil.millsecondsToStr(dialog.getTime() * 1000));
//                                endTime = dialog.getTime();
//                            }
//                            dialog.dismiss();
//                        } else {
//                            ToastUtil.showShortlToast(getString(R.string.time_is_zero));
//                        }
//                    }
//                })
//                .setNegativeButton(new OnCountDownListener() {
//                    @Override
//                    public void onClick(CountDownPickWheelDialog dialog) {
//                        dialog.dismiss();
//                    }
//                }).create();
//        countDownPickWheelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        countDownPickWheelDialog.setCanceledOnTouchOutside(false);
//
//        countDownPickWheelDialog.show();
//
//        WindowManager m = getWindowManager();
//        Display d = m.getDefaultDisplay();
//
//        Window window = countDownPickWheelDialog.getWindow();
//        WindowManager.LayoutParams lp = window.getAttributes();
//        lp.width = d.getWidth();
//        lp.height = UiUtils.dp2px(256);
//        window.setGravity(Gravity.BOTTOM);
//        window.setAttributes(lp);
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        }
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        endTime = sdf.format(date);

        dateTime = sdfDate.format(date);

        customDatePicker2 = new CustomGoClassDatePicker(this, new CustomGoClassDatePicker.ResultHandler() {
            @Override
            public void handle(String time) { // 回调接口，获得选中的时间
                endTime = sdf.format(new Date());
                dateChooseTxt.setText(time + "");
//                sdfDate.format(new Date());
                String checkDate;
                checkDate = dateTime + " " + time;
                if (DateUtil.getTimeMillisByStr(checkDate, "yyyy-MM-dd HH:mm") > DateUtil.getTimeMillisByStr(endTime, "yyyy-MM-dd HH:mm")) {
                    long timeDiffer;
                    timeDiffer = DateUtil.getTimeMillisByStr(checkDate, "yyyy-MM-dd HH:mm") - DateUtil.getTimeMillisByStr(endTime, "yyyy-MM-dd HH:mm");
                    differTime = (int) (timeDiffer / 1000);
                    if (differTime / 60 > 1) {
                        tvDiffer.setText("(" + (differTime / 60) + "分)");
                    } else if (differTime / 60 < 1 && differTime / 60 > 0) {
                        tvDiffer.setText("(1分)");
                    } else {
                        tvDiffer.setText("(--分)");
                    }
                    classTime = checkDate + ":59";
                } else {
                    ToastUtil.showLonglToast("设置时间请大于当前时间");
                }
            }
        }, endTime, "2028-12-31 00:00:00"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker2.showSpecificTime(true); // 显示时和分
        customDatePicker2.setIsLoop(true); // 允许循环滚动

    }

    protected void initData() {
        show();

        Network.createTokenService(NetWorkService.TeachActivityService.class)
                .getClassList(SubjectSPUtils.getCurrentSubject().getSubjectId() + "")
                .map(new StatusFunc<SyncClassVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SyncClassVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SyncClassVo o) {
                        if (o != null && o.getData().size() > 0) {
                            mList.addAll(o.getData());
                            if (myAdapter != null) {
                                myAdapter.notifyDataSetChanged();
                            }
                        }

                    }
                });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.date_choose_txt) {
            customDatePicker2.show(endTime, true);
//                if (dateChooseTxt.getText().toString().isEmpty()) {
//                    customDatePicker2.show(sdf.format(new Date()),true);
//                } else {
//                    customDatePicker2.show(dateChooseTxt.getText().toString().trim(),false);
//                }
        }
    }


    @Override
    public void onClick(String classId) {
        setClassId = classId;
    }


    private List<SyncClassVo.DataBean> mList = new ArrayList<>();
    private MyAdapter myAdapter = new MyAdapter();


    private int selectedPosition = -1;// 选中的位置
    private int departmentSelectedPosition = -1;//记录已点击的位置 设置为-1,则无初始选中效果


    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mList == null ? 0 : mList.size();
        }

        @Override
        public Object getItem(int i) {
            return mList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {

            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(SetinClassActivity.this, R.layout.go_class_pub_gridview_item, null);
                holder = new ViewHolder();
                holder.tv = convertView.findViewById(R.id.viewName);
                holder.iv = convertView.findViewById(R.id.check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tv.setText(mList.get(position).getBaseGradeName() + mList.get(position).getClassName());

            if (selectedPosition == position) {
                holder.iv.setEnabled(true);
                convertView.setSelected(true);
                convertView.setPressed(true);
//                holder.iv.setVisibility(View.VISIBLE);
            } else {
                holder.iv.setEnabled(false);
                convertView.setSelected(false);
                convertView.setPressed(false);
//                holder.iv.setVisibility(View.INVISIBLE);
            }
            if (position == departmentSelectedPosition) {
                holder.iv.setEnabled(true);
                convertView.setSelected(true);
                convertView.setPressed(true);
//                holder.iv.setVisibility(View.VISIBLE);
            }

            return convertView;
        }

        class ViewHolder {
            TextView tv;
            ImageView iv;
        }
    }

    private void getBehavior(final int type) {
        if (setClassId.isEmpty() || TextUtils.equals(setClassId, "")) {
            ToastUtil.showShortlToast("请重新选择上课班级");
            return;
        }
        /*
         1 上课 2 下课 3 锁屏 4 解锁
         */
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getClassBehavior(setClassId, String.valueOf(type), classTime)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                        EventBus.getDefault().post(new LessonDetailsGoClassEvent());
                    }

                    @Override
                    public void onNext(Object o) {
                        switch (type) {
                            case 1:
                                ToastUtil.showShortlToast("上课");
                                Constant.setShowHeadCountDown(true);
                                BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
                                //时间 s
                                TimeBaseUtil.getInstance().showHeadCountView(differTime);
                                SPUtils.put(SetinClassActivity.this, SPUtils.CLASS_ID, setClassId);
                                finish();
                                break;
                            case 2:
                                ToastUtil.showShortlToast("下课");
                                break;
                            case 3:
                                ToastUtil.showShortlToast("学生端锁屏");
                                break;
                            case 4:
                                ToastUtil.showShortlToast("学生端解锁");
                                break;
                            default:
                                break;
                        }

                    }
                });
    }
}

