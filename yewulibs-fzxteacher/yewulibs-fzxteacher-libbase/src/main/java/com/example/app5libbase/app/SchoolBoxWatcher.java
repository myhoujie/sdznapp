package com.example.app5libbase.app;

import android.text.TextUtils;

import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.example.app5libbase.base.BaseProgressCallBack;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.BosUrlBean;
import com.sdzn.fzx.teacher.vo.BoxInfoBean;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.QiniuConfig;
import com.example.app5libpublic.constant.Constant;

import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * SchoolBoxWatcher〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SchoolBoxWatcher {
    private static final int PERIOD_TIME = 2 * 60 * 1000;

    public static String FILE_IP = "";

    private long lastTime;
    private Subscription subscribe;

    public SchoolBoxWatcher() {

    }


    public void startWatch() {
        subscribe = rx.Observable.interval(PERIOD_TIME, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (System.currentTimeMillis() - lastTime > PERIOD_TIME) {
                            lastTime = System.currentTimeMillis();
                            checkNetwork();
                        }
                    }
                });

    }

    public void stopWatch() {
        if (subscribe != null && subscribe.isUnsubscribed())
            subscribe.unsubscribe();
    }


    public void requestBoxUrl() {
        final LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean == null)
            return;
        final String customerSchoolId = loginBean.getData().getUser().getCustomerSchoolId() + "";

        Network.createTokenService(NetWorkService.LoginService.class)
                .getBoxUrl(customerSchoolId)
                .map(new StatusFunc<BoxInfoBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BoxInfoBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BoxInfoBean o) {
                        try {
                            Constant.BOX_URL = "http://" + o.getData().getIp();
                            checkNetwork();
                        } catch (Exception e) {
                            Log.e("盒子地址获取失败");
                        }
                    }
                });
    }

    OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(10 * 1000, TimeUnit.SECONDS)
            .writeTimeout(10 * 1000, TimeUnit.SECONDS)
            .connectTimeout(10 * 1000, TimeUnit.SECONDS).build();

    private void checkNetwork() {

        if (TextUtils.isEmpty(Constant.BOX_URL)) {
            requestBoxUrl();
            return;
        }
        if (TextUtils.isEmpty(Constant.QINIU_CONFIG)) {
            getQiniuConfig();
            return;
        }
       // new Thread(runnable).start();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (TextUtils.isEmpty(Constant.BOX_URL)) {
                requestBoxUrl();
            } else {
                Request request = new Request.Builder()
                        .url(Constant.BOX_URL)
                        .build();
                try {
                    final Response execute = client.newCall(request).execute();
                    if (execute.isSuccessful()) {
                        FILE_IP = Constant.BOX_URL + "/schoolbox/resource/access";
                    } else {
                        FILE_IP = "";
                        //失败后立即重新检查盒子地址
                        Constant.BOX_URL = null;
                        requestBoxUrl();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    FILE_IP = "";
                    //失败后立即重新检查盒子地址
                    Constant.BOX_URL = null;
                    requestBoxUrl();
                } finally {
                    Log.e("盒子路径是：" + FILE_IP);
                }
            }
        }
    };

 /*   public static String getFileAddress(String resourceId, String address) {
        if (TextUtils.isEmpty(FILE_IP)) {
            return address;
        } else {
            return FILE_IP + "?resourceId=" + resourceId + "&convertMediaPath=" + address;
        }
    }*/

    static SchoolBosUrl schoolBosUrl;

    public static void getFileUrlAddress(final String resourceId, final String address, SchoolBosUrl bosUrl) {
        schoolBosUrl = bosUrl;
        if (TextUtils.isEmpty(FILE_IP)) {
            schoolBosUrl.bosUrl(address);
        } else {
            final LoginBean loginBean = SPUtils.getLoginBean();
            if (loginBean == null)
                return;
            final String customerSchoolId = loginBean.getData().getUser().getCustomerSchoolId() + "";

            Network.createTokenService(NetWorkService.LoginService.class)
                    .getBoxUrl(customerSchoolId)
                    .map(new StatusFunc<BoxInfoBean>())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<BoxInfoBean>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            schoolBosUrl.bosUrl(address);
                        }

                        @Override
                        public void onNext(BoxInfoBean o) {
                            if (TextUtils.isEmpty(o.getData().getIp())) {
                                schoolBosUrl.bosUrl(address);
                            } else {
                                try {
                                    String ip = "http://" + o.getData().getIp() + "/schoolbox/resource/access";
                                    RequestParams params = new RequestParams(ip + "?resourceId=" + resourceId + "&convertMediaPath=" + address);
                                    x.http().get(params, new BaseProgressCallBack<String>() {
                                        @Override
                                        public void onSuccess(String result) {
                                            BosUrlBean bosUrlBean = GsonUtil.fromJson(result, BosUrlBean.class);
                                            schoolBosUrl.bosUrl(bosUrlBean.getResult().getUrl());
                                        }

                                        @Override
                                        public void onError(Throwable ex, boolean isOnCallback) {
                                            schoolBosUrl.bosUrl(address);
                                        }
                                    });
                                } catch (Exception e) {
                                    Log.e("盒子地址获取失败");
                                    schoolBosUrl.bosUrl(address);
                                }
                            }

                        }
                    });


            // return FILE_IP + "?resourceId=" + resourceId + "&convertMediaPath=" + address;

        }
    }

    public interface SchoolBosUrl {

        public void bosUrl(String url);

    }

    public void getQiniuConfig() {

        Network.createTokenService(NetWorkService.QiniuControllerService.class)
                .getQiniuConfig()
                .map(new StatusFunc<QiniuConfig>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<QiniuConfig>() {
                    @Override
                    public void onNext(QiniuConfig o) {

                        Constant.BOX_URL = o.getData().getDomian();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("", "");
                    }

                    @Override
                    public void onCompleted() {

                    }
                });

    }
}
