package com.example.app5libbase.ai.pop.bean;

import com.sdzn.fzx.teacher.bean.GrzxRecActBean1;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean2;

import java.io.Serializable;

public class GRZXBaseRecActBean implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int style1 = 1;
    public static final int style2 = 2;
    public static final int style3 = 3;

    public int type;
    private GrzxRecActBean1 grzxRecActBean1;
    private GrzxRecActBean2 grzxRecActBean2;

    public GRZXBaseRecActBean(int type) {
        this.type = type;
    }

    public GRZXBaseRecActBean(int type, GrzxRecActBean1 mBean) {
        this.type = type;
        this.grzxRecActBean1 = mBean;
    }

    public GRZXBaseRecActBean(int type, GrzxRecActBean2 mBean) {
        this.type = type;
        this.grzxRecActBean2 = mBean;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public GrzxRecActBean1 getGrzxRecActBean1() {
        return grzxRecActBean1;
    }

    public void setGrzxRecActBean1(GrzxRecActBean1 grzxRecActBean1) {
        this.grzxRecActBean1 = grzxRecActBean1;
    }

    public GrzxRecActBean2 getGrzxRecActBean2() {
        return grzxRecActBean2;
    }

    public void setGrzxRecActBean2(GrzxRecActBean2 grzxRecActBean2) {
        this.grzxRecActBean2 = grzxRecActBean2;
    }

}
