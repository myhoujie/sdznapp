package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.MyAdapter;
import com.example.app5libbase.ai.pop.bean.TextModel;
import com.lxj.xpopup.core.CenterPopupView;

import java.util.ArrayList;
import java.util.List;

/**
 * 移动小组(分组)
 */
public class MoveGroupPopupView extends CenterPopupView {
    private TextView tvTitle;
    private RecyclerView rlPopup;
    private TextView tvCancel;
    private TextView tvConfirm;

    private Context mContext;
    MyAdapter myAdapter;
    private List<TextModel> mTextModels;

    public MoveGroupPopupView(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_center_yewu_fenzu;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Log.e("tag", "CustomDrawerPopupView onCreate");
        tvTitle = (TextView) findViewById(R.id.tv_title);
        rlPopup = (RecyclerView) findViewById(R.id.rl_popup);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);

        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.showShort(typeIndex);
                dismiss();

            }
        });
        initRv();

    }

    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }


    private String typeIndex = "";//根据传入的参数判断唯一性

    private void initRv() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        rlPopup.setLayoutManager(gridLayoutManager);
        mTextModels = new ArrayList<>();
        myAdapter = new MyAdapter(mTextModels);
        rlPopup.setAdapter(myAdapter);

        initData();
        myAdapter.setNewData(mTextModels);

        myAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                typeIndex = mTextModels.get(position).getText();
                myAdapter.setClickPosition(typeIndex);
                myAdapter.notifyDataSetChanged();
//                ToastUtils.showShort(typeIndex);
            }
        });



    }

    private void initData() {
        mTextModels = new ArrayList<TextModel>();
        mTextModels.add(new TextModel("全部", false, true));
        mTextModels.add(new TextModel("早餐前", false, true));
        mTextModels.add(new TextModel("午餐前", false, true));
        mTextModels.add(new TextModel("晚餐前", false, true));
        mTextModels.add(new TextModel("早餐后", false, true));
        mTextModels.add(new TextModel("午餐后", false, true));
        mTextModels.add(new TextModel("晚餐后", false, true));
        mTextModels.add(new TextModel("睡前", false, true));
    }
}
