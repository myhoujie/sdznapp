package com.example.app5libbase.views.statistic_chat;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.sdzn.fzx.student.libutils.util.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * GraphView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class GraphView extends ViewGroup {

    private static final String sss = "分值";

    private String[] flagValue = {"100", "80", "60", "40", "20", "0"};

    private final static int COLOR_BASE_LINE = 0xFF323C47;
    private final static int COLOR_BASE_TEXT = 0xFF323C47;
    private final static int COLOR_SELECTED_BG = 0xFFEDF5FF;
    private final static int COLOR_CHAT_LINE = 0xFFEDEDED;
    private final static int COLOR_VALUE_TXT = 0xFF808FA3;
    private final static int COLOR_POP_WINDOW_BG = 0x66000000;
    private final static int COLOR_POP_WINDOW_TEXT = 0xFFFFFFFF;

    private final static int[] COLOR_COLUM_COLORS = {0xFFFFAC4C, 0xFF10D3D2, 0xFF68C426};


    private List<LineViewBean> mDatas = new ArrayList<>();


    private GestureDetector mGestureDetector;

    StaticLayout m;

    /**
     * 柱状图的宽度
     */
    private float defColumWidth = 48;

    /**
     * 柱状图选中时候的宽度
     */
    private float defColumBgWidth = 88;

    /**
     * 每条数据所占用的最小宽度
     */
    private float defItemMinWidth = 180;

    /**
     * 每条数据实际占用的宽度
     */
    private float itemWidth;

    /**
     * 最大的滑动距离
     */
    private float mMaxHorizontalScrollDis;

    /**
     * 当前选中的item
     */
    private int curShwoPoint;

    private int curShowItem;
    /**
     * View 的宽度,高度
     */
    private float viewWidth, viewHeight;

    /**
     * 除去周边文字的图形 区域
     */
    private RectF chatRectF = new RectF();

    private Paint baseLinePaint, selectBgPaint, chatLinePaint, columPaint, pointPaint, popwindowBgPaint;

    private TextPaint baseTxtPaint, valuePaint, infoPaint;

    public GraphView(Context context) {
        this(context, null);
    }

    public GraphView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }

    private void init() {
        mGestureDetector = new GestureDetector(getContext(), new OnGestureListener());
        initPaint();
        initDatas();
    }

    private void initDatas() {
        for (int i = 0; i < 3; i++) {
            final LineViewBean lineViewBean = new LineViewBean();
            ArrayList<ItemInfoVo> list = new ArrayList<>();
            for (int j = 0; j < 20; j++) {
                ItemInfoVo vo = new ItemInfoVo();
                vo.setValue((float) Math.random());
                vo.setName("today");
                list.add(vo);
            }
            lineViewBean.setItemVo(list);
            lineViewBean.setColor(COLOR_COLUM_COLORS[i]);
            mDatas.add(lineViewBean);
        }

    }

    private void initPaint() {
        baseLinePaint = new Paint();
        baseLinePaint.setAntiAlias(true);
        baseLinePaint.setStyle(Paint.Style.FILL);
        baseLinePaint.setStrokeWidth(UiUtils.sp2px( 1));
        baseLinePaint.setColor(COLOR_BASE_LINE);

        selectBgPaint = new Paint();
        selectBgPaint.setAntiAlias(true);
        selectBgPaint.setStyle(Paint.Style.FILL);
        selectBgPaint.setStrokeWidth(UiUtils.sp2px( 1));
        selectBgPaint.setColor(COLOR_SELECTED_BG);

        chatLinePaint = new Paint();
        chatLinePaint.setAntiAlias(true);
        chatLinePaint.setStyle(Paint.Style.FILL);
        chatLinePaint.setStrokeWidth(UiUtils.sp2px( 1));
        chatLinePaint.setColor(COLOR_CHAT_LINE);


        popwindowBgPaint = new Paint();
        popwindowBgPaint.setAntiAlias(true);
        popwindowBgPaint.setStyle(Paint.Style.FILL);
        popwindowBgPaint.setStrokeWidth(UiUtils.sp2px( 1));
        popwindowBgPaint.setColor(COLOR_POP_WINDOW_BG);

        baseTxtPaint = new TextPaint();
        baseTxtPaint.setAntiAlias(true);
        baseTxtPaint.setStyle(Paint.Style.FILL);
        baseTxtPaint.setStrokeWidth(UiUtils.sp2px( 1));
        baseTxtPaint.setTextSize(UiUtils.sp2px(12));
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        baseTxtPaint.setColor(COLOR_BASE_TEXT);

        valuePaint = new TextPaint();
        valuePaint.setAntiAlias(true);
        valuePaint.setStyle(Paint.Style.FILL);
        valuePaint.setTextSize(UiUtils.sp2px(12));
        valuePaint.setTextAlign(Paint.Align.CENTER);
        valuePaint.setStrokeWidth(UiUtils.sp2px( 1));
        valuePaint.setColor(COLOR_VALUE_TXT);

        columPaint = new Paint();
        columPaint.setAntiAlias(true);
        columPaint.setStyle(Paint.Style.STROKE);
        columPaint.setTextSize(UiUtils.sp2px(12));

        pointPaint = new Paint();
        pointPaint.setAntiAlias(true);
        pointPaint.setStyle(Paint.Style.FILL);

        infoPaint = new TextPaint();
        infoPaint.setAntiAlias(true);
        infoPaint.setStyle(Paint.Style.FILL);
        infoPaint.setTextSize(UiUtils.sp2px(12));
        infoPaint.setTextAlign(Paint.Align.LEFT);
        infoPaint.setStrokeWidth(UiUtils.dp2px( 1));
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        viewWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        viewHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();

        chatRectF.left = 112;
        chatRectF.top = 80;
        chatRectF.right = viewWidth - 20;
        chatRectF.bottom = viewHeight - 92;

        final float defTotalWidth = mDatas.get(0).getItemVo().size() * defItemMinWidth;

        if (defTotalWidth > chatRectF.width()) {
            itemWidth = defItemMinWidth;
            mMaxHorizontalScrollDis = defTotalWidth - chatRectF.width();
        } else {
            itemWidth = viewWidth / mDatas.get(0).getItemVo().size();
            mMaxHorizontalScrollDis = 0;
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBaseLine(canvas);
        drawChatLine(canvas);
        drawFlagValue(canvas);
        drawLines(canvas);
        drawPopWindow(canvas);
        drawPoint(canvas);
    }

    private void drawBaseLine(Canvas canvas) {
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.left + getScrollX(), chatRectF.bottom, baseLinePaint);
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX(), chatRectF.bottom, baseLinePaint);
    }

    private void drawChatLine(Canvas c) {
        final float chatH = chatRectF.bottom - chatRectF.top;
        final float itemHeight = chatH / 5;
        final float baseHeight = chatRectF.top;
        for (int i = 0; i < 5; i++) {
            c.drawLine(chatRectF.left + getScrollX(), baseHeight + itemHeight * i, chatRectF.right + getScrollX(), baseHeight + itemHeight * i, chatLinePaint);
        }
    }

    private void drawFlagValue(Canvas canvas) {
        final float h = (chatRectF.bottom - chatRectF.top) / 5;
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(sss, 0, sss.length(), getScrollX() + chatRectF.left - 10, 40, baseTxtPaint);
        for (int i = 0; i < flagValue.length; i++) {
            canvas.drawText(flagValue[i], 0, flagValue[i].length(), getScrollX() + chatRectF.left - 20, chatRectF.top + h * i, baseTxtPaint);
        }

        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left - 30 + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX(), viewHeight));

        baseTxtPaint.setTextAlign(Paint.Align.CENTER);
        for (int i = 0; i < mDatas.get(0).getItemVo().size(); i++) {
            final String name = mDatas.get(0).getItemVo().get(i).getName();
            canvas.drawText(name, 0, name.length(), chatRectF.left + itemWidth * i, chatRectF.bottom + 40, baseTxtPaint);

        }
        canvas.restore();
    }

    private void drawLines(Canvas canvas) {
        measurePath();
        for (int i = 0; i < mDatas.size(); i++) {
            columPaint.setColor(mDatas.get(i).getColor());
            Path path = mDatas.get(i).getPath();
            PathMeasure mPathMeasure = new PathMeasure(path, false);
            Path dst = new Path();
            dst.rLineTo(0, 0);
            float distance = mPathMeasure.getLength();
            if (mPathMeasure.getSegment(0, distance, dst, true)) {
                //绘制线
                canvas.save();
                canvas.clipRect(chatRectF.left + getScrollX(), 0, chatRectF.right + getScrollX(), viewHeight);
                canvas.drawPath(dst, columPaint);
                float[] pos = new float[2];
                mPathMeasure.getPosTan(distance, pos, null);
                //绘制阴影
                dst.lineTo(pos[0], chatRectF.bottom);
                dst.lineTo(chatRectF.left, chatRectF.bottom);
                dst.close();
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                Shader mShader = new LinearGradient(0, 0, 0, chatRectF.bottom, new int[]{mDatas.get(i).getColor(), 0x00C2E9FB}, null, Shader.TileMode.REPEAT);
                paint.setShader(mShader);
                canvas.drawPath(dst, paint);
                canvas.restore();
            }
        }
    }


    private void drawPoint(Canvas canvas) {
        canvas.save();
        canvas.clipRect(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom);
        for (int i = 0; i < mDatas.size(); i++) {
            pointPaint.setColor(mDatas.get(i).getColor());
            for (int j = 0; j < mDatas.get(i).getItemVo().size(); j++) {
                final ItemInfoVo itemInfoVo = mDatas.get(i).getItemVo().get(j);
                canvas.drawCircle(itemInfoVo.x, itemInfoVo.y, 4, pointPaint);
            }
        }
        canvas.restore();
    }

    /**
     * 计算贝塞尔曲线
     */
    private void measurePath() {
        dealData();

        for (int i = 0; i < mDatas.size(); i++) {
            Path mPath = new Path();
            float prePreviousPointX = Float.NaN;
            float prePreviousPointY = Float.NaN;
            float previousPointX = Float.NaN;
            float previousPointY = Float.NaN;
            float currentPointX = Float.NaN;
            float currentPointY = Float.NaN;
            float nextPointX;
            float nextPointY;

            List<ItemInfoVo> mPointList = mDatas.get(i).getItemVo();
            final int lineSize = mPointList.size();
            for (int valueIndex = 0; valueIndex < lineSize; ++valueIndex) {
                if (Float.isNaN(currentPointX)) {
                    ItemInfoVo point = mPointList.get(valueIndex);
                    currentPointX = point.x;
                    currentPointY = point.y;
                }
                if (Float.isNaN(previousPointX)) {
                    //是否是第一个点
                    if (valueIndex > 0) {
                        ItemInfoVo point = mPointList.get(valueIndex - 1);
                        previousPointX = point.x;
                        previousPointY = point.y;
                    } else {
                        //是的话就用当前点表示上一个点
                        previousPointX = currentPointX;
                        previousPointY = currentPointY;
                    }
                }

                if (Float.isNaN(prePreviousPointX)) {
                    //是否是前两个点
                    if (valueIndex > 1) {
                        ItemInfoVo point = mPointList.get(valueIndex - 2);
                        prePreviousPointX = point.x;
                        prePreviousPointY = point.y;
                    } else {
                        //是的话就用当前点表示上上个点
                        prePreviousPointX = previousPointX;
                        prePreviousPointY = previousPointY;
                    }
                }

                // 判断是不是最后一个点了
                if (valueIndex < lineSize - 1) {
                    ItemInfoVo point = mPointList.get(valueIndex + 1);
                    nextPointX = point.x;
                    nextPointY = point.y;
                } else {
                    //是的话就用当前点表示下一个点
                    nextPointX = currentPointX;
                    nextPointY = currentPointY;
                }

                if (valueIndex == 0) {
                    // 将Path移动到开始点
                    mPath.moveTo(currentPointX, currentPointY);
                } else {
                    // 求出控制点坐标
                    final float firstDiffX = (currentPointX - prePreviousPointX);
                    final float firstDiffY = (currentPointY - prePreviousPointY);
                    final float secondDiffX = (nextPointX - previousPointX);
                    final float secondDiffY = (nextPointY - previousPointY);
                    final float firstControlPointX = previousPointX + (0.16f * firstDiffX);
                    float firstControlPointY = previousPointY + (0.16f * firstDiffY);
                    final float secondControlPointX = currentPointX - (0.16f * secondDiffX);
                    float secondControlPointY = currentPointY - (0.16f * secondDiffY);

                    if (firstControlPointY < chatRectF.top) {
                        firstControlPointY = chatRectF.top;
                    }
                    if (firstControlPointY > chatRectF.bottom) {
                        firstControlPointY = chatRectF.bottom;
                    }
                    if (secondControlPointY < chatRectF.top) {
                        secondControlPointY = chatRectF.top;
                    }
                    if (secondControlPointY > chatRectF.bottom) {
                        secondControlPointY = chatRectF.bottom;
                    }

                    //画出曲线
                    mPath.cubicTo(firstControlPointX, firstControlPointY, secondControlPointX, secondControlPointY,
                            currentPointX, currentPointY);
                }

                // 更新值,
                prePreviousPointX = previousPointX;
                prePreviousPointY = previousPointY;
                previousPointX = currentPointX;
                previousPointY = currentPointY;
                currentPointX = nextPointX;
                currentPointY = nextPointY;
            }

            mDatas.get(i).setPath(mPath);
        }


    }

    private void dealData() {
        for (int i = 0; i < mDatas.size(); i++) {
            for (int j = 0; j < mDatas.get(i).getItemVo().size(); j++) {
                final ItemInfoVo itemInfoVo = mDatas.get(i).getItemVo().get(j);
                itemInfoVo.x = (int) (j * itemWidth + chatRectF.left);
                itemInfoVo.y = (int) ((1 - itemInfoVo.getValue()) * chatRectF.height() + chatRectF.top);
            }
        }
    }


    private void drawPopWindow(Canvas canvas) {

        final ItemInfoVo itemInfoVo = mDatas.get(curShowItem).getItemVo().get(curShwoPoint);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("正确率" + (int) (itemInfoVo.getValue() * 100) + "%");
        stringBuffer.append("\n");
        stringBuffer.append("任务数" + "3个");

        RectF tempRect = new RectF();

        StaticLayout staticLayout = new StaticLayout(
                stringBuffer.toString(),
                infoPaint,
                UiUtils.sp2px( 100),
                Layout.Alignment.ALIGN_NORMAL,
                1.2f,
                0.2f,
                true);

        float padding = 20;
        float paddingColum = 20;

        final int popWidth = staticLayout.getWidth();
        final int popHeight = staticLayout.getHeight();

        Point point = new Point(itemInfoVo.x, itemInfoVo.y);

        if (popWidth + point.x - getScrollX() > chatRectF.right) {
            tempRect.right = point.x - 10;
            tempRect.left = tempRect.right - popWidth;
        } else {
            tempRect.left = point.x;
            tempRect.right = point.x + popWidth;
        }

        if (point.y + padding + popHeight / 2 > chatRectF.bottom) {
            tempRect.bottom = chatRectF.bottom;
            tempRect.top = tempRect.bottom - popHeight;
        } else if (point.y - padding - popHeight / 2 < chatRectF.top) {
            tempRect.top = chatRectF.top;
            tempRect.bottom = chatRectF.top + popHeight;
        } else {
            tempRect.top = point.y - popHeight / 2;
            tempRect.bottom = point.y + popHeight / 2;
        }


        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom));
        canvas.drawRoundRect(tempRect, 8, 8, popwindowBgPaint);
        canvas.restore();

        canvas.save();
        canvas.translate(tempRect.left + paddingColum, tempRect.top);
        staticLayout.draw(canvas);

        canvas.restore();
    }


    /**********************
     * 手势区域分割线
     ********************/

    private void setShowItem(int item, int pointPos) {
        curShowItem = item;
        curShwoPoint = pointPos;
        if (itemClickListener != null) {
            if (item < mDatas.size()) {
                itemClickListener.onItemChanged(mDatas.get(item).getItemVo().get(pointPos));
            }
        }
        invalidate();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            float x = e.getX() + getScrollX() - chatRectF.left;
            int item = (int) ((x + itemWidth / 2) / itemWidth);

            final float abs = Math.abs(item * itemWidth - x);
            if (abs > 30) {//说明点击的x位置是有效区
                return true;
            }

            float y = e.getY();
            for (int i = 0; i < mDatas.size(); i++) {

                final float absY = Math.abs(mDatas.get(i).getItemVo().get(item).y - y);
                if (absY < 30) {
                    setShowItem(i, item);
                }
            }


            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            if (Math.abs(distanceX) < Math.abs(distanceY)) {
                return false;
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);//屏蔽父控件拦截onTouch事件
            }
            float dx = distanceX;
            int x = getScrollX();

            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (mMaxHorizontalScrollDis < 0) {
                dx = 0;
            } else if (x + dx > mMaxHorizontalScrollDis) {
                dx = mMaxHorizontalScrollDis - x;
            }

            scrollBy((int) dx, 0);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {

            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    public static class LineViewBean {
        private int id;
        private String name;
        private int color;
        private List<ItemInfoVo> itemVo;
        private List<Point> points;
        private Path path;

        public List<ItemInfoVo> getItemVo() {
            return itemVo;
        }

        public void setItemVo(List<ItemInfoVo> itemVo) {
            this.itemVo = itemVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }


        public List<Point> getPoints() {
            return points;
        }

        public void setPoints(List<Point> points) {
            this.points = points;
        }

        public Path getPath() {
            return path;
        }

        public void setPath(Path path) {
            this.path = path;
        }
    }

    public static class ItemInfoVo {
        private String name;
        private float value;
        private String id;
        public int x;
        public int y;


        public ItemInfoVo() {
        }

        public ItemInfoVo(String id, String name, int value) {
            this.name = name;
            this.value = value;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    private ItemChangedListener itemClickListener;

    public void setItemClickListener(ItemChangedListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemChangedListener {
        void onItemChanged(ItemInfoVo pos);
    }
}
