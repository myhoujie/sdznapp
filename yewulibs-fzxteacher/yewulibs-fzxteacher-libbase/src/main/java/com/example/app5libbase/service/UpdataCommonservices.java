package com.example.app5libbase.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSON;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libutils.util.MmkvUtils;
import com.sdzn.fzx.teacher.bean.ShouyeBean;
import com.sdzn.fzx.teacher.presenter.ShouyePresenter;
import com.sdzn.fzx.teacher.view.ShouyeViews;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class UpdataCommonservices extends Service implements ShouyeViews {

    public static final String HUIBEN_READINGTIME_ACTION = "HUIBEN_READINGTIME_ACTION";
    ShouyePresenter shouyePresenter;
    // 通过静态方法创建ScheduledExecutorService的实例
    private ScheduledExecutorService mScheduledExecutorService = Executors.newScheduledThreadPool(2);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MsgBinder();
    }

    @Override
    public String getIdentifier() {
        return System.currentTimeMillis() + "";
    }

    @Override
    public void onIndexSuccess(ShouyeBean shouyeBean) {
        MmkvUtils.getInstance().set_common_json("ceshi", JSON.toJSONString(shouyeBean), ShouyeBean.class);
    }

    @Override
    public void onIndexNodata(String msg) {

    }

    @Override
    public void onIndexFail(String msg) {

    }

    public class MsgBinder extends Binder {
        public UpdataCommonservices getService() {
            return UpdataCommonservices.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    //启动service
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            String action = intent.getAction();
            if (action.equals(HUIBEN_READINGTIME_ACTION)) {
                //定时服务
                mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("onIndexSuccess", "定时刷新接口");
                        Log.e("testaaaatoken", "onStartCommand: " + SPUtils.getInstance().getString("token", ""));
                        shouyePresenter.queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""));
                    }
                }, 1, 30, TimeUnit.SECONDS);
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        shouyePresenter.onDestory();
        super.onDestroy();
    }
}
