package com.example.app5libbase.listener;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public interface OnSearchClickListener {
    void onSearch(String searchStr);

    void onTextChanged(String searchStr);
}
