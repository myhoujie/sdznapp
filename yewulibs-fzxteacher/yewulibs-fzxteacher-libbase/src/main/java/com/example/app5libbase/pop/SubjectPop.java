package com.example.app5libbase.pop;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libpublic.cons.Subject;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.listener.OnItemTouchListener;
import com.example.app5libbase.listener.OnSubjectClickListener;
import com.sdzn.fzx.teacher.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 学科弹窗
 *
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class SubjectPop extends PopupWindow {
    private View viewBg;
    private LinearLayout llSubject;
    private RecyclerView rvSubject;

    private Activity activity;

    private List<LoginBean.DataBean.SubjectListBean> subjectVos = new ArrayList<>();

    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter subjectAdapter;

    private OnSubjectClickListener onSubjectClickListener;

    public SubjectPop(Activity activity, OnSubjectClickListener onSubjectClickListener) {
        this.onSubjectClickListener = onSubjectClickListener;
        this.activity = activity;

        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_subject, null);
        viewBg = (View) contentView.findViewById(R.id.viewBg);
        llSubject = (LinearLayout) contentView.findViewById(R.id.llSubject);
        rvSubject = (RecyclerView) contentView.findViewById(R.id.rvSubject);

        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setClippingEnabled(false);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        setSubjects();
        viewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        llSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private void setSubjects() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvSubject.setLayoutManager(linearLayoutManager);
        subjectAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvSubject.setAdapter(subjectAdapter);
        rvSubject.addOnItemTouchListener(new OnItemTouchListener(rvSubject) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                LoginBean.DataBean.SubjectListBean subjectListBean = subjectVos.get(position);
                SubjectSPUtils.saveCurrentSubject(subjectListBean);
                SubjectPop.this.onSubjectClickListener.onSubjectClick(subjectListBean);
                dismiss();
            }
        });
    }

    private void addItems() {
        itemEntityList.clear();
        itemEntityList.addItems(R.layout.item_main_subject, subjectVos)
                .addOnBind(R.layout.item_main_subject, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        LoginBean.DataBean.SubjectListBean subjectVo = (LoginBean.DataBean.SubjectListBean) itemData;
                        Subject subject = Subject.subjects.get(subjectVo.getSubjectId());
                        holder.getChildView(R.id.ivSubject).setBackground(activity.getResources().getDrawable(subject.getDrawableId()));
                        holder.setText(R.id.tvSubject, subjectVo.getSubjectName());
                        if (position == subjectVos.size() - 1) {
                            holder.getChildView(R.id.line).setVisibility(View.GONE);
                        } else {
                            holder.getChildView(R.id.line).setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent, List<LoginBean.DataBean.SubjectListBean> subjectVos) {
        if (subjectVos != null && subjectVos.size() > 0) {
            this.subjectVos.clear();
            this.subjectVos.addAll(subjectVos);
            addItems();
            subjectAdapter.notifyDataSetChanged();
        }
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 0);
        } else {
            this.dismiss();
        }
    }
}
