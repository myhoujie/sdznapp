package com.example.app5libbase.views.calendar;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app5libbase.R;
import com.example.app5libbase.views.calendar.entity.DayTimeEntity;
import com.example.app5libbase.views.calendar.entity.MonthTimeEntity;
import com.example.app5libbase.views.calendar.entity.UpdataCalendar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * CalendarDialog〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CalendarDialog extends Dialog implements View.OnClickListener {

    private RecyclerView reycycler;
    private Context context;


    public static DayTimeEntity startDay;
    public static DayTimeEntity stopDay;

    private MonthTimeAdapter adapter;
    private ArrayList<MonthTimeEntity> datas;

    public CalendarDialog(@NonNull Context context) {
        super(context, R.style.MyDialog);
        this.context = context;
    }

    public CalendarDialog(@NonNull Context context, int themeResId) {
        this(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.caclendar_dialog_layout);
        initView();
        initData();
    }

    private void initView() {

        reycycler = (RecyclerView) findViewById(R.id.plan_time_calender);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(context,   // 上下文
                        LinearLayout.HORIZONTAL,  //垂直布局,
                        false);

        reycycler.setLayoutManager(layoutManager);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(reycycler);
    }

    private void initData() {
        startDay = new DayTimeEntity(0, 0, 0, 0);
        stopDay = new DayTimeEntity(-1, -1, -1, -1);
        datas = new ArrayList<>();

        int year = 1990;
        int month = 1;
        Calendar c = Calendar.getInstance();
        final int curYear = c.get(Calendar.YEAR);
        final int curMonth = c.get(Calendar.MONTH) + 1;
        final int totalMouth = (curYear - year) * 12 + curMonth;
        c.set(year, month, 0);

        for (int i = 0; i < totalMouth; i++) {
            int nextYear = c.get(Calendar.YEAR);
            int nextMonth = c.get(Calendar.MONTH) + 1;
            datas.add(new MonthTimeEntity(nextYear, nextMonth));
            c.add(Calendar.MONTH, 1);
        }
        adapter = new MonthTimeAdapter(datas, context);
        reycycler.setAdapter(adapter);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        EventBus.getDefault().register(this);

        reycycler.scrollToPosition(datas.size() - 1);

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(UpdataCalendar event) {
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {

    }
}
