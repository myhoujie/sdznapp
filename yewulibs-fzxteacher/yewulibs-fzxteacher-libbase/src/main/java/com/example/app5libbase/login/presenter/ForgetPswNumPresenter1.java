package com.example.app5libbase.login.presenter;


import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.login.view.ForgetPswNumViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.teacher.api.Api;
import com.sdzn.fzx.teacher.vo.VerifyUserMobile;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ForgetPswNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ForgetPswNumPresenter1 extends Presenter<ForgetPswNumViews> {

    public void verityUserName(final String cardId) {
        if (!StringUtils.isMobile(cardId) && cardId.length() != 8) {
            ToastUtils.showShort("请输入正确的账号或手机号");
            return;
        }
//        Network.createService(NetWorkService.ForgetPswService.class)
//                .verityUserName(cardId)
//                .map(new StatusFunc<VerifyUserNameBean>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<VerifyUserNameBean>(new SubscriberListener<VerifyUserNameBean>() {
//                    @Override
//                    public void onNext(VerifyUserNameBean o) {
//                        getView().verifySuccess(o.getData());
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getStatus().getCode() == 21100) {
//                                ToastUtil.showShortlToast("您输入的账号不存在，请重新输入");
//                                return;
//                            }
//
//                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
//                        }
//                        ToastUtil.showShortlToast("验证失败");
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity));

        com.alibaba.fastjson.JSONObject requestData = new com.alibaba.fastjson.JSONObject();
        requestData.put("account", cardId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .teacherFindMobile(requestBody)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<VerifyUserMobile>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<VerifyUserMobile>> call, Response<ResponseSlbBean1<VerifyUserMobile>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            ToastUtils.showShort("验证失败");
                            return;
                        }
                        getView().verifySuccess(response.body().getResult().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<VerifyUserMobile>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        ToastUtils.showShort("验证失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });

    }

}
