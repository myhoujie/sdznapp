package com.example.app5libbase.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.example.app5libbase.R;

/**
 * 描述：加载图片的options
 * <p>
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ImageLoaderOptions {

    public final static DisplayImageOptions default_options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.mipmap.transparent)
            .cacheInMemory(true)// 是否在内存缓存该图片
            .cacheOnDisk(true)//是否在硬盘缓存该图片
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .considerExifParams(true)//会识别图片的方向信息
            .displayer(new FadeInBitmapDisplayer(800))//设置渐渐显示的动画效果
            .build();

    /**
     * 一般网络图片显示
     *
     * @param url
     * @param imageView
     */
    public static void displayImage(String url, ImageView imageView) {
        displayImage(url, imageView, default_options);
    }

    /**
     * 一般网络图片显示
     *
     * @param url
     * @param imageView
     * @param options
     */
    public static void displayImage(String url, ImageView imageView, DisplayImageOptions options) {
        if (url == null) {
            url = "";
        }
        ImageLoader.getInstance().displayImage(url, imageView, options);
    }

    /**
     * 头像显示
     *
     * @param url
     * @param imageView
     * @param options
     */
    public static void displayAvatarImage(String url, ImageView imageView, DisplayImageOptions options) {
        displayImage(url, imageView, options);
    }

    public static Bitmap getBitmap(String url) {
        return ImageLoader.getInstance().loadImageSync(url);
    }

    public static Bitmap getBitmapFromSDCard(String uri) {
        return ImageLoader.getInstance().loadImageSync("file://" + uri);
    }

    /**
     * 从内存卡中异步加载本地图片
     *
     * @param uri
     * @param imageView
     */
    public static void displayFromSDCard(String uri, ImageView imageView) {
        // String imageUri = "file:///mnt/sdcard/image.png"; // from SD card
        ImageLoader.getInstance().displayImage("file://" + uri, imageView);
    }

    /**
     * 从assets文件夹中异步加载图片
     *
     * @param imageName 图片名称，带后缀的，例如：1.png
     * @param imageView
     */
    public static void dispalyFromAssets(String imageName, ImageView imageView) {
        // String imageUri = "assets://image.png"; // from assets
        ImageLoader.getInstance().displayImage("assets://" + imageName,
                imageView);
    }

    /**
     * 从drawable中异步加载本地图片
     *
     * @param imageId
     * @param imageView
     */
    public static void displayFromDrawable(int imageId, ImageView imageView) {
        // String imageUri = "drawable://" + R.drawable.image; // from drawables
        // (only images, non-9patch)
        ImageLoader.getInstance().displayImage("drawable://" + imageId,
                imageView);
    }

    /**
     * 从内容提提供者中抓取图片
     */
    public static void displayFromContent(String uri, ImageView imageView) {
        // String imageUri = "content://media/external/audio/albumart/13"; //
        // from content provider
        ImageLoader.getInstance().displayImage("content://" + uri, imageView);
    }

}
