package com.example.app5libbase.views.graffiti.core;

import android.graphics.Canvas;
import android.graphics.PointF;

/**
 * Created on 27/06/2018.
 */

public interface IGraffitiItem {

    public void setGraffiti(IGraffiti graffiti);

    public IGraffiti getGraffiti();

    /**
     * 获取画笔
     */
    public IGraffitiPen getPen();

    /**
     * 设置画笔
     */
    public void setPen(IGraffitiPen pen);

    /**
     * 获取画笔形状
     */
    public IGraffitiShape getShape();

    /**
     * 设置画笔形状
     */
    public void setShape(IGraffitiShape shape);

    /**
     * 获取大小
     */
    public float getSize();

    /**
     * 设置大小
     */
    public void setSize(float size);

    /**
     * 获取颜色
     */
    public IGraffitiColor getColor();

    /**
     * 设置颜色
     */
    public void setColor(IGraffitiColor color);

    /**
     * 绘制item
     */
    public void draw(Canvas canvas);

    /**
     * 设置在当前涂鸦中的左上角位置
     */
    public void setLocation(float x, float y);

    /**
     * 获取当前涂鸦中的起始坐标
     */
    public PointF getLocation();

    /**
     * 设置item的旋转值
     */
    public void setItemRotate(float degree);

    /**
     * 获取item的旋转值
     */
    public float getItemRotate();

    /**
     * 是否需要裁剪图片区域外的部分
     */
    public boolean isNeedClipOutside();

    /**
     * 设置是否需要裁剪图片区域外的部分
     */
    public void setNeedClipOutside(boolean clip);

    /**
     * 添加进涂鸦时回调
     */
    public void onAdd();

    /**
     * 移除涂鸦时回调
     */
    public void onRemove();
}
