package com.example.app5libbase.baseui.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app5libpublic.y_recycleradapter.Y_ItemEntityList;
import com.example.app5libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app5libpublic.y_recycleradapter.Y_OnBind;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.listener.OnClassClickListener;
import com.example.app5libbase.pop.ClassPop;
import com.example.app5libbase.baseui.presenter.ScorePresenter;
import com.example.app5libbase.baseui.view.ScoreView;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.GroupVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

/**
 * base 里面  左下角弹框进入的
 * 小组评分
 */

public class ScoreActivity extends MBaseActivity<ScorePresenter> implements ScoreView, View.OnClickListener {
    private TextView tvTask;
    private TextView tvLesson;
    private TextView tvClass;
    private RecyclerView rvScore;
    private ImageView ivClose;
    private Y_MultiRecyclerAdapter scoreAdapter;
    private Y_ItemEntityList itemEntityListScore = new Y_ItemEntityList();

    private ClassPop classPop;

    private SyncClassVo syncClassVo;

    private String type = "2";

    private Drawable bottomDrawable;
    private Drawable bottomDrawableBlank;

    @Override
    public void initPresenter() {
        mPresenter = new ScorePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReturnPop(false);
        setContentView(R.layout.popup_tools_score);
        rvScore = findViewById(R.id.rvScore);
        tvClass = findViewById(R.id.tvClass);
        tvLesson = findViewById(R.id.tvLesson);
        tvTask = findViewById(R.id.tvTask);
        ivClose = findViewById(R.id.ivClose);

        tvTask.setOnClickListener(this);
        tvLesson.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        tvClass.setOnClickListener(this);

        initView();
        initData();

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入ScoreActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        rvScore.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        scoreAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityListScore);
        rvScore.setAdapter(scoreAdapter);
    }

    @Override
    protected void initData() {
        mPresenter.getGroupList(type);
        bottomDrawable = getResources().getDrawable(R.mipmap.dibu_bg);
        bottomDrawable.setBounds(0, 0, bottomDrawable.getMinimumWidth(), bottomDrawable.getMinimumHeight());
        bottomDrawableBlank = getResources().getDrawable(R.mipmap.dibu_bg_blank);
        bottomDrawableBlank.setBounds(0, 0, bottomDrawableBlank.getMinimumWidth(), bottomDrawableBlank.getMinimumHeight());
    }

    private void getGroupList() {
        itemEntityListScore.clear();
        scoreAdapter.notifyDataSetChanged();
        mPresenter.getGroupListWithClassId(type);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvLesson) {
            itemEntityListScore.clear();
            scoreAdapter.notifyDataSetChanged();
            type = "2";
            getGroupList();
            tvTask.setTextColor(getResources().getColor(R.color.activity_main_text1));
            tvLesson.setTextColor(getResources().getColor(R.color.group_tab_blue));
            tvTask.setCompoundDrawables(null, null, null, bottomDrawableBlank);
            tvLesson.setCompoundDrawables(null, null, null, bottomDrawable);
        } else if (id == R.id.tvTask) {
            itemEntityListScore.clear();
            scoreAdapter.notifyDataSetChanged();
            type = "1";
            getGroupList();
            tvTask.setTextColor(getResources().getColor(R.color.group_tab_blue));
            tvLesson.setTextColor(getResources().getColor(R.color.activity_main_text1));
            tvTask.setCompoundDrawables(null, null, null, bottomDrawable);
            tvLesson.setCompoundDrawables(null, null, null, bottomDrawableBlank);
        } else if (id == R.id.ivClose) {
            finish();
        } else if (id == R.id.tvClass) {
            showPop(tvClass, new OnClassClickListener() {
                @Override
                public void onClassClick(SyncClassVo.DataBean dataBean) {
                    tvClass.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
                    mPresenter.setClassId(dataBean.getClassId());
                    getGroupList();
                }
            });
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void getGroupListSuccess(GroupVo groupVo) {
        if (groupVo != null && groupVo.getData() != null && groupVo.getData().size() > 0) {
            itemEntityListScore.clear();
            itemEntityListScore.addItems(R.layout.item_score, groupVo.getData())
                    .addOnBind(R.layout.item_score, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindScoreHolder(holder, (GroupVo.DataBean) itemData);
                        }
                    });
            scoreAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void modifyPointSuccess() {
        ToastUtil.showShortlToast("打分成功");
        getGroupList();
    }

    @Override
    public void getClassVoSuccess(SyncClassVo syncClassVo) {
        if (syncClassVo != null && syncClassVo.getData() != null && syncClassVo.getData().size() > 0) {
            this.syncClassVo = syncClassVo;
            SyncClassVo.DataBean dataBean = syncClassVo.getData().get(0);
            if (dataBean != null) {
                tvClass.setText(dataBean.getBaseGradeName() + dataBean.getClassName());
            }
        }
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    private void showPop(View view, OnClassClickListener onClassClickListener) {
        if (classPop == null) {
            classPop = new ClassPop(activity, onClassClickListener);
        } else {
            classPop.setOnClassClickListener(onClassClickListener);
        }
        if (syncClassVo != null) {
            classPop.setClassVos(syncClassVo.getData());
        }
        classPop.showPopupWindow(view);
    }
}
