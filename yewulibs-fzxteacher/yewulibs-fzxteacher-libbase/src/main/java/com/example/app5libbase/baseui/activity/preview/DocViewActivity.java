package com.example.app5libbase.baseui.activity.preview;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseActivity;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.example.app5libbase.util.OpenFileUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.wps.Wps;
import com.example.app5libpublic.constant.Constant;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class DocViewActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, View.OnClickListener {

    ProgressBar pbLoading;
    private String path;
    private String target;
    private boolean isLoading = true;
    private Callback.Cancelable cancelable;
    private String type;
    RelativeLayout activityPptView;

    private static final int REQUECT_CODE_SDCARD = 1000;
    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppt_view);
        dialog = new Dialog(DocViewActivity.this, R.style.notice_dialog);
        pbLoading = findViewById(R.id.pb_loading);
        activityPptView = findViewById(R.id.activity_ppt_view);
        activityPptView.setOnClickListener(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入DocViewActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        if (target == null) {
            ToastUtil.showShortlToast("未找到目标文件");
            finish();
            return;
        }
        if (target.startsWith("http")) {
            String[] split = target.split(Constant.QINIU_CONFIG);
            String fileName = split[split.length - 1];
            String path;
            try {
                path = FileUtil.getResPath();
            } catch (Exception e) {
                e.printStackTrace();
                path = FileUtil.getAppFilesDir();
            }
            File file = new File(path, fileName);
            target = file.getAbsolutePath();
        }
        File file = new File(target);
        if (!file.exists()) {
            // 下载文件
            isLoading = true;
            pbLoading.setVisibility(View.VISIBLE);
            requestPermissiontest();
        } else {
            showDoc(file, type);
        }
    }

    @Override
    protected void initData() {
        path = getIntent().getStringExtra("path");
        type = getIntent().getStringExtra("type");
        try {
//            target = FileUtil.getResPath() + getIntent().getStringExtra("name") + "." + type;
            target = path;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDoc(File file, String type) {
        Wps wps = new Wps(activity);
        String wpsVersion = wps.checkVersion();
        if (TextUtils.isEmpty(wpsVersion)) {
            Intent pptIntent = OpenFileUtil.getAttachmentIntent(file, type);
            if (OpenFileUtil.openAppNum(pptIntent) > 0) {
                activity.startActivity(pptIntent);
            } else {
                ToastUtil.showShortlToast(activity.getString(R.string.uninstalled_hint));
            }
        } else {
            wps.open(file);
        }
        this.finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                download();
                dialog.dismiss();
            } else {
                requestPermissiontest();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissiontest() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            download();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }

//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

   /* @PermissionGrant(REQUECT_CODE_SDCARD)
    public void requestSdcardSuccess() {
        download();
    }

    @PermissionDenied(REQUECT_CODE_SDCARD)
    public void requestSdcardFailed() {
        pbLoading.setVisibility(View.GONE);
        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
    }*/

    private void download() {
        RequestParams params = new RequestParams(path);
        //设置断点续传
        params.setAutoResume(true);
        params.setAutoRename(true);
        params.setSaveFilePath(target);
        cancelable = x.http().get(params, new Callback.ProgressCallback<File>() {

            @Override
            public void onSuccess(File result) {
                pbLoading.setVisibility(View.GONE);
                isLoading = false;
                showDoc(new File(target), type);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onFinished() {
            }

            @Override
            public void onWaiting() {
            }

            @Override
            public void onStarted() {
            }

            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
            }
        });
    }


    @Override
    protected void onDestroy() {
        if (cancelable != null && !cancelable.isCancelled() && isLoading) {
            cancelable.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        if (cancelable != null && !cancelable.isCancelled() && isLoading) {
            cancelable.cancel();
        }
        this.finish();
    }
}