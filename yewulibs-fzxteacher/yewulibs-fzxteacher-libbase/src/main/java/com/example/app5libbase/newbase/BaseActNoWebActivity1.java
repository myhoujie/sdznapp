package com.example.app5libbase.newbase;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.CustomDrawerPopupView;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.views.CalenderClearEditText;
import com.example.app5libbase.views.ClearableEditText;
import com.example.app5libbase.views.XRecyclerView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;

import java.util.Calendar;

import me.jessyan.autosize.AutoSizeCompat;

public abstract class BaseActNoWebActivity1 extends AppCompatActivity implements NetconListener2 {
    public TextView tvBack;//返回
    public TextView tvTitleName;//标题名称
    public LinearLayout llDownTitle;//下拉标题控制
    public TextView tvDownTitle;//下拉选择
    public XRecyclerView recyclerViewTitle;//RecyclerView滑动选择
    public TextView tvTijiaoTitle;//提交按钮
    public TextView tvSousuoTitle;//搜索按钮
    public TextView tvShijianTitle;//时间按钮
    public TextView tvWeizhi;//未知按钮
    public CalenderClearEditText tvZankaiShijian;//时间选择器Edtext
    private TextView tvClassRemove;//我的班级移除解绑
    public TextView tvMyAdd;//合作添加
    private TextView tvChongxinpg;//重新批改
    private TextView tvFabupg;//发布批改
    public TextView tvMyTitle;//个人中心按钮
    private TextView tvMore;//更多


    protected EmptyViewNew1 emptyview1;//网络监听
    private long mCurrentMs = System.currentTimeMillis();
    public BaseActNoWebActivity1 activity;
    protected NetState netState;

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityManager.addLiveActivity(this);
        activity = this;
        setContentView(getLayoutId());
        setup(savedInstanceState);
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, this);
        findiview();
        onclickview();
        donetwork();
    }

    /*具体业务内容*/
    protected void donetwork() {

    }

    /*加载布局*/
    protected abstract int getLayoutId();

    /**/
    protected void setup(@Nullable Bundle savedInstanceState) {
        tvBack = (TextView) findViewById(R.id.tv_back);//返回
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);//标题名称
        llDownTitle = (LinearLayout) findViewById(R.id.ll_down_title);
        tvDownTitle = (TextView) findViewById(R.id.tv_down_title);//下拉列表名称
        recyclerViewTitle = (XRecyclerView) findViewById(R.id.recycler_view_title);//recyclerVie滑动
        tvTijiaoTitle = (TextView) findViewById(R.id.tv_tijiao_title);//提交按钮
        tvSousuoTitle = (TextView) findViewById(R.id.tv_sousuo_title);//搜索按钮
        tvShijianTitle = (TextView) findViewById(R.id.tv_shijian_title);//时间按钮
        tvWeizhi = (TextView) findViewById(R.id.tv_weizhi);//未知按钮
        tvZankaiShijian = (CalenderClearEditText) findViewById(R.id.tv_zankai_shijian);//时间展示Editetext
        tvClassRemove = (TextView) findViewById(R.id.tv_class_remove);//我的班级移除解绑
        tvMyAdd = (TextView) findViewById(R.id.tv_my_add);//添加按钮
        tvChongxinpg = (TextView) findViewById(R.id.tv_chongxinpg);//重新批改按钮
        tvFabupg = (TextView) findViewById(R.id.tv_fabupg);//发布批改按钮
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        tvMore = (TextView) findViewById(R.id.tv_more);//更多按钮
        emptyview1 = findViewById(R.id.emptyview2_order);//网络状态
        clickListener();
    }

    protected void findiview() {
    }


    protected void onclickview() {
    }


    private BaseOnClickListener mListener;

    public void setBaseOnClickListener(BaseOnClickListener listener) {
        mListener = listener;
    }

    //个人中心
    public void Titlegrzx() {
        CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");

            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }

    //时间
    public void Titleshijian() {
        showCalendarDialog();
    }

    //展开时间
    public void Titlezankaishijian() {
    }

    /*搜索*/
    public void Titlesousuo() {
    }

    /*我的班级移除解绑*/
    public void TitleRemove() {
    }

    /*提交*/
    public void Titletijiao() {
    }

    /*下拉加载*/
    public void TitleDropdown() {
    }

    /*返回*/
    public void TitleBack() {
        finish();
//        onBackPressed();
//        Intent msgIntent = new Intent();
//        msgIntent.setAction("BaseActFragmentBroadcast");
//        LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
    }

    /*重新批改*/
    public void TitleChongxinpg() {
    }

    /*发布批改*/
    public void TitleFabupg() {
    }


    /*添加*/
    public void TitleAdd() {
    }

    /*更多点击事件*/
    public void TitleMore() {
    }

    /*时间选择器-------------------------------------开始*/
    private CalenderDialog calendarDialog;//日历dialog

    public String startTime;//开始时间
    public String endTime;//结束时间

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(this, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvZankaiShijian.setVisibility(View.VISIBLE);
                    tvShijianTitle.setVisibility(View.GONE);
                    tvZankaiShijian.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                    listener.returnRefresh(startTime, endTime);
                }
            });
        }
        calendarDialog.show();

    }

    //回调时间数据
    private static refreshOnDisplayListener listener;

    public interface refreshOnDisplayListener {
        void returnRefresh(String startTime, String endTime);
    }

    public static void setOnDisplayRefreshListener(refreshOnDisplayListener myListener) {
        listener = myListener;
    }

    /*时间选择器-------------------------------------结束*/


    /*点击事件添加回调*/
    protected void clickListener() {
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    tvShijianTitle.setVisibility(View.VISIBLE);
                    tvZankaiShijian.setVisibility(View.GONE);
                }
            });
        }
        /*返回点击*/
        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleBack();
                    }
                }
            });
        }

        /*下拉点击事件*/
        if (llDownTitle != null) {
            llDownTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleDropdown();
                    }
                }
            });
        }
        /*提交点击事件*/
        if (tvTijiaoTitle != null) {
            tvTijiaoTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titletijiao();
                    }
                }
            });
        }
        /*搜索点击事件*/
        if (tvSousuoTitle != null) {
            tvSousuoTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlesousuo();
                    }
                }
            });
        }
        /*时间点击时间*/
        if (tvShijianTitle != null) {
            tvShijianTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titleshijian();
                    }
                }
            });
        }
        /*展开时间点击事件*/
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlezankaishijian();
                    }
                }
            });
        }
        /*我的班级移除解绑点击事件*/
        if (tvClassRemove != null) {
            tvClassRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleRemove();
                    }
                }
            });
        }

        /*个人中心点击事件*/
        if (tvMyTitle != null) {
            tvMyTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlegrzx();
                    }
                }
            });
        }
        /*添加点击事件*/
        if (tvMyAdd != null) {
            tvMyAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleAdd();
                    }
                }
            });
        }
        /*更多点击事件*/
        if (tvMore != null) {
            tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleMore();
                    }
                }
            });
        }
        /*重新批改*/
        if (tvChongxinpg != null) {
            tvChongxinpg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleChongxinpg();
                    }
                }
            });
        }
        /*发布批改*/
        if (tvFabupg != null) {
            tvFabupg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleFabupg();
                    }
                }
            });
        }
    }

    /**
     * @param LayoutStyle 返回判断
     */
    protected void TitleShowHideState(int LayoutStyle) {
        if (LayoutStyle == 1) { /*显示搜索，时间，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvSousuoTitle.setVisibility(View.VISIBLE);
//            tvShijianTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 2) { /*显示提交，时间，个人中心，recuclerView*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvTijiaoTitle.setVisibility(View.VISIBLE);
//            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 3) { /*显示个人中心，recyclerView*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 4) {/*显示时间，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 5) { /*显示个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 6) {   /*显示搜索，时间，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvSousuoTitle.setVisibility(View.VISIBLE);
//            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 7) {   /*显示搜索，时间，个人中心，recyclerView*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvSousuoTitle.setVisibility(View.VISIBLE);
//            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 8) {   /*显示recyclerView,提交,个人中心，*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
//            tvTijiaoTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 9) {/*显示recyclerView,添加,个人中心，*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvMyAdd.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
        } else if (LayoutStyle == 10) {/*显示recyclerView,发布批改，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
//            tvFabupg.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
        } else if (LayoutStyle == 11) {/*显示recyclerView,重新批改,发布批改，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
//            tvChongxinpg.setVisibility(View.VISIBLE);
//            tvFabupg.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onDestroy() {
        if (netState != null) {
            netState.unregisterReceiver();
        }
        super.onDestroy();
    }


    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}
