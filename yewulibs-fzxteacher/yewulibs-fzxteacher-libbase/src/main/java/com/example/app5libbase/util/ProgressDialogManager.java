package com.example.app5libbase.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.View;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.example.app5libbase.views.MProgressDialog;

/**
 * 描述：进度条管理类
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ProgressDialogManager {
    private MProgressDialog dialog;
    private Activity activity;

    public ProgressDialogManager(Activity activity) {
        this.activity = activity;
        if (dialog == null) {
            dialog = MProgressDialog.newInstance(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }
    }

    public void setCancelable(boolean isCancelable) {
        if (dialog != null) {
            dialog.setCancelable(isCancelable);
            dialog.setCanceledOnTouchOutside(isCancelable);
        }
    }

    private void setMessage(CharSequence message) {
        if (dialog != null) {
            if (message == null || message.length() == 0) {
                dialog.findViewById(R.id.message).setVisibility(View.GONE);
            } else {
                TextView txt = (TextView) dialog.findViewById(R.id.message);
                txt.setText(message);
            }
        }
    }

    private void setOnCancelListener(DialogInterface.OnCancelListener cancelListener) {
        if (dialog != null) {
            // 监听返回键处理
            dialog.setOnCancelListener(cancelListener);
        }
    }

    public void cancelWaiteDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void showWaiteDialog(CharSequence message, boolean isCancelable, DialogInterface.OnCancelListener cancelListener) {
        setMessage(message);
        setOnCancelListener(cancelListener);
        if (dialog != null && !dialog.isShowing() && !activity.isFinishing()) {
            dialog.show();
        }
    }

    public void showWaiteDialog(CharSequence message, boolean isCancelable) {
        setMessage(message);
        setCancelable(isCancelable);
        if (dialog != null && !dialog.isShowing() && !activity.isFinishing()) {
            dialog.show();
        }
    }

    public void showWaiteDialog(CharSequence message) {
        setMessage(message);
        if (dialog != null && !dialog.isShowing() && !activity.isFinishing()) {
            dialog.show();
        }
    }

    public void showWaiteDialog() {
        showWaiteDialog("正在加载中，请稍后...");
    }

    public boolean isShow() {
        return dialog.isShowing();
    }

    public MProgressDialog getProgressDialog() {
        return dialog;
    }
}
