package com.example.app5libbase.baseui.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.GroupVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public interface ScoreView extends BaseView {
    void getGroupListSuccess(GroupVo groupVo);

    void modifyPointSuccess();

    void networkError(String msg);

    void getClassVoSuccess(SyncClassVo syncClassVo);
}
