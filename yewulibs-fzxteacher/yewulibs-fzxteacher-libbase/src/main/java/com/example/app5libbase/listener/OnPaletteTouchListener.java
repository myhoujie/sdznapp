package com.example.app5libbase.listener;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/2/7
 */
public interface OnPaletteTouchListener {
    void hide();

    void show();
}
