package com.example.app5libbase.util.chatroom;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.app5libbase.R;


/**
 * zhaosen
 */

public final class GlideHelper {
    public static void load(Context context, Object url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .error(R.mipmap.error_image)
                .placeholder(R.mipmap.img_placeholder);

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public static void load(Activity activity, Object url, ImageView imageView) {
        load(activity, url, imageView, false);
    }

    public static void load(final Activity activity, Object url, ImageView imageView, boolean isClickFinish) {
        RequestOptions options = new RequestOptions()
                .error(R.mipmap.error_image)
                .placeholder(R.mipmap.img_placeholder);
        Glide.with(activity)
                .load(url)
                .apply(options)
                .into(imageView);

        if (isClickFinish) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
        }
    }
}
