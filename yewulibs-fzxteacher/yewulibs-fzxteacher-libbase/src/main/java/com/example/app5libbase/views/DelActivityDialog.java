package com.example.app5libbase.views;

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.app5libbase.R;

/**
 * DelActivityDialog〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class DelActivityDialog {
    private AlertDialog alertDialog;
    OptionListener listener;
    private TextView confirm;
    private TextView content;
    private TextView cancel;

    public DelActivityDialog creatDialog(Activity context) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_del_activity, null);
        alertDialog = new AlertDialog.Builder(context, R.style.exit_dialog).setView(view).create();
        WindowManager m = context.getWindowManager();
        Display d = m.getDefaultDisplay();  //为获取屏幕宽、高
        android.view.WindowManager.LayoutParams p = alertDialog.getWindow().getAttributes();  //获取对话框当前的参数值
        p.height = (int) (d.getHeight() * 0.4);   //高度设置为屏幕的0.3
        p.width = (int) (d.getWidth() * 0.3);    //宽度设置为屏幕的0.5
        alertDialog.getWindow().setAttributes(p);     //设置生效

        content = view.findViewById(R.id.tip_content_txt);
        confirm = view.findViewById(R.id.sure_btn);
        cancel = view.findViewById(R.id.cancel_btn);
        alertDialog.setCancelable(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (listener != null)
                    listener.onConfirmed();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (listener != null) {
                    listener.onCancel();
                }
            }
        });
        return this;
    }

    public DelActivityDialog buildText(String contentTxt) {
        content.setText(contentTxt);
        return this;
    }

    public DelActivityDialog buildConfirmText(String confirmText) {
        confirm.setText(confirmText);
        return this;
    }

    public DelActivityDialog buildCancelText(String text) {
        cancel.setText(text);
        return this;
    }

    public DelActivityDialog buildListener(OptionListener listener) {
        this.listener = listener;
        return this;
    }

    public void show() {
        alertDialog.show();
    }

    public interface OptionListener {

        void onConfirmed();

        void onCancel();

    }
}
