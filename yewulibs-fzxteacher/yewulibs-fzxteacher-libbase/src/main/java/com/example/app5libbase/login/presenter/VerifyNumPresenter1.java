package com.example.app5libbase.login.presenter;

import android.os.Handler;
import android.os.Message;

import com.example.app5libbase.login.view.VerifyNumViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.teacher.api.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * VerifyNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyNumPresenter1 extends Presenter<VerifyNumViews> {
    private long endTime;

    public void sendVerityCode(String tel) {
        endTime = System.currentTimeMillis() + 60 * 1000;
        mHandler.sendEmptyMessageDelayed(0, 100);
        RetrofitNetNew.build(Api.class, getIdentifier())
                .sendYZM(tel)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        stopCountDown();
                        if (!hasView()) {
                            return;
                        }
                        getView().onFailed("验证码发送失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
//        Network.createService(NetWorkService.ForgetPswService.class)
//                .sendVerityCode(tel, "3")
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<Object>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        stopCountDown();
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getCode() == 21202) {
//                                new DialogUtils().createTipDialog(mActivity).buildText("今天获取验证码次数已达上限，\n" +
//                                        "请明天重试。").show();
//                                return;
//                            }
//                        }
//                        ToastUtil.showShortlToast("验证码发送失败");
//                    }
//
//                    @Override
//                    public void onNext(Object o) {
//
//                    }
//                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .checkYZM(phoneNum, code)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        if (String.valueOf(response.body().getResult()).equals("true")) {
                            getView().verifySuccess(phoneNum, code);
                        }else {
                            getView().onFailed("验证码错误，请重新输入");
                        }

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        getView().onFailed("验证码错误，请重新输入");
                        t.printStackTrace();
                        call.cancel();
                    }
                });

//        Network.createService(NetWorkService.ForgetPswService.class)
//                .checkVerityCode(phoneNum, code)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onNext(Object o) {
//                        mView.verifySuccess(phoneNum, code);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getCode() == 21204) {
//                                stopCountDown();
//                                ToastUtil.showShortlToast("验证码已过期，请重新获取");
//                                return;
//                            }
//                        }
//                        ToastUtil.showShortlToast("验证码错误，请重新输入");
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity));

    }


    private void stopCountDown() {
        endTime = System.currentTimeMillis();
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("时间差：" + (endTime - System.currentTimeMillis()));
            final int lessTime = (int) ((endTime - System.currentTimeMillis()) / 1000);

            getView().onCountDownChanged(lessTime);
            if (lessTime < 0) {
                mHandler.removeMessages(0);
            } else {
                mHandler.sendEmptyMessageDelayed(0, 100);
            }
        }
    };

//    @Override
//    public void detachView() {
//        super.detachView();
//
//    }

    @Override
    public void onDestory() {
        super.onDestory();
        mHandler.removeMessages(0);
    }
}
