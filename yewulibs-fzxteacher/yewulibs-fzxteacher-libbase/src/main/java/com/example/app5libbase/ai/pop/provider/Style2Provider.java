package com.example.app5libbase.ai.pop.provider;

import android.widget.ImageView;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActBean;


public class Style2Provider extends BaseItemProvider<GRZXBaseRecActBean, BaseViewHolder> {

    @Override
    public int viewType() {
        return GRZXBaseRecActAdapter.STYLE_TWO;
    }

    @Override
    public int layout() {
        return R.layout.popup_right_recycleview_item2;
    }

    @Override
    public void convert(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {

        helper.addOnClickListener(R.id.iv_item);
        LogUtils.e(data.getGrzxRecActBean2().getPicture());
        Glide.with(mContext).load(data.getGrzxRecActBean2().getPicture()).into((ImageView) helper.getView(R.id.iv_item));
    }

//    @Override
//    public void onClick(BaseViewHolder helper, BaseRecActDemo42Bean data, int position) {
//        if (helper.getItemId() == helper.getView(R.id.tv).getId()) {
//            Toasty.normal(BaseApp.get(), position + "item click=" + data.getmBean().getUserName()).show();
//        } else if (helper.getItemId() == helper.getView(R.id.tv1).getId()) {
//            Toasty.normal(BaseApp.get(), position + "item click=" + data.getmBean().getText()).show();
//        } else {
//        }
//
//    }

    @Override
    public boolean onLongClick(BaseViewHolder helper, GRZXBaseRecActBean data, int position) {
        Toast.makeText(mContext, "longClick", Toast.LENGTH_SHORT).show();
        return true;
    }
}
