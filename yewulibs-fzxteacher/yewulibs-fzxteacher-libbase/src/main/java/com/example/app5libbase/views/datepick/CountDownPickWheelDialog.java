package com.example.app5libbase.views.datepick;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.app5libbase.R;
import com.example.app5libbase.listener.OnCountDownListener;
import com.sdzn.fzx.student.libutils.util.UiUtils;

import org.xutils.x;

public class CountDownPickWheelDialog extends Dialog {

    private WheelView wvHour;
    private WheelView wvMinute;
    private WheelView wvSecond;
    private Button btnCancel;
    private Button btnConfirm;

    private Context mContext;

    private OnCountDownListener positiveClickListener;
    private OnCountDownListener negativeClickListener;

    private int hour;
    private int minute;
    private int second;

    private CountDownPickWheelDialog(Context context) {
        super(context, R.style.dialog);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.select_time_dialog, null);
        wvHour = contentView.findViewById(R.id.wvHour);
        wvMinute = contentView.findViewById(R.id.wvMinute);
        wvSecond = contentView.findViewById(R.id.wvSecond);
        btnCancel = contentView.findViewById(R.id.btnCancel);
        btnConfirm = contentView.findViewById(R.id.btnConfirm);
        this.setContentView(contentView);
        x.view().inject(this, contentView);
        adjustView();
        setListener();
    }

    private void adjustView() {
        wvHour.TEXT_SIZE = UiUtils.dp2px(20);
        wvMinute.TEXT_SIZE = UiUtils.dp2px(20);
        wvSecond.TEXT_SIZE = UiUtils.dp2px(20);

        wvHour.setLabel("时");
        wvMinute.setLabel("分");
        wvSecond.setLabel("秒");

        wvHour.setAdapter(new NumericWheelAdapter(0, 12));
        wvMinute.setAdapter(new NumericWheelAdapter(0, 59));
        wvSecond.setAdapter(new NumericWheelAdapter(0, 59));

        wvHour.setCurrentItem(0);
        wvMinute.setCurrentItem(0);
        wvSecond.setCurrentItem(0);
    }

    private void setListener() {
        wvHour.addChangingListener(wheelListener_hour);
        wvMinute.addChangingListener(wheelListener_minute);
        wvSecond.addChangingListener(wheelListener_second);

        if (negativeClickListener != null) {
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    negativeClickListener.onClick(CountDownPickWheelDialog.this);
                }
            });
        } else {
            btnCancel.setOnClickListener(dismissListener);
        }
        if (positiveClickListener != null) {
            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    positiveClickListener.onClick(CountDownPickWheelDialog.this);
                }
            });
        } else {
            btnConfirm.setOnClickListener(dismissListener);
        }
    }

    private final View.OnClickListener dismissListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    // 添加"时"监听
    private final OnWheelChangedListener wheelListener_hour = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            hour = newValue + 1;
        }
    };

    // 添加"分"监听
    private final OnWheelChangedListener wheelListener_minute = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            minute = newValue + 1;
        }
    };

    // 添加"秒"监听
    private final OnWheelChangedListener wheelListener_second = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            second = newValue + 1;
        }
    };

    public void setPositiveButton(OnCountDownListener positiveClickListener) {
        this.positiveClickListener = positiveClickListener;
    }

    public void setNegativeButton(OnCountDownListener negativeClickListener) {
        this.negativeClickListener = negativeClickListener;
    }

    public static class Builder {
        private final CountDownPickWheelDialog.CountDownPickParams P;

        public Builder(Context context) {
            P = new CountDownPickWheelDialog.CountDownPickParams(context);
        }

        public CountDownPickWheelDialog.Builder setPositiveButton(OnCountDownListener listener) {
            P.mPositiveButtonListener = listener;
            return this;
        }

        public CountDownPickWheelDialog.Builder setNegativeButton(OnCountDownListener listener) {
            P.mNegativeButtonListener = listener;
            return this;
        }

        public CountDownPickWheelDialog create() {
            final CountDownPickWheelDialog dialog = new CountDownPickWheelDialog(
                    P.mContext);
            P.apply(dialog);
            return dialog;
        }
    }

    public static class CountDownPickParams {
        public OnCountDownListener mPositiveButtonListener;
        public final Context mContext;
        private OnCountDownListener mNegativeButtonListener;

        public CountDownPickParams(Context context) {
            mContext = context;
        }

        public void apply(CountDownPickWheelDialog dialog) {
            dialog.setPositiveButton(mPositiveButtonListener);
            dialog.setNegativeButton(mNegativeButtonListener);
        }
    }

    public int getTime() {
        return wvHour.getCurrentItem() * 60 * 60 + wvMinute.getCurrentItem() * 60 + wvSecond.getCurrentItem();
    }
}