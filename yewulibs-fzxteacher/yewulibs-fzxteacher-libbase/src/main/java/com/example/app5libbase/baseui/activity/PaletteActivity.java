package com.example.app5libbase.baseui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.baseui.fragment.WhiteboardFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：白板界面
 * -
 * 创建人：wangchunxiao
 * 创建时间：2017/3/17
 */
public class PaletteActivity extends BaseActivity {

    private List<Fragment> fragments = new ArrayList<>();
    private Fragment currFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReturnPop(false);
        setContentView(R.layout.activity_palette);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PaletteActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        Bundle bundle = new Bundle();
        bundle.putString("type", getIntent().getStringExtra("type"));
        fragments.add(WhiteboardFragment.newInstance(bundle));

        showFragment(0);
    }

    @Override
    protected void initData() {
    }

    private void showFragment(int fragmentIndex) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        if (currFragment != null) {
            if (currFragment != fragment) {
                if (!fragment.isAdded()) {
                    ft.hide(currFragment).add(R.id.framelayout, fragment, fragment.getClass().getName());
                } else {
                    ft.hide(currFragment).show(fragment);
                }
            }
        } else {
            ft.add(R.id.framelayout, fragment, fragment.getClass().getName());
        }
        currFragment = fragment;
        ft.commit();
    }
}