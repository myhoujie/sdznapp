package com.example.app5libbase.base;

import android.os.Bundle;

/**
 * 描述：所有Activity的基类，任何Activity必须继承它
 * -
 * 创建人：zhangchao
 * 创建时间：17/3/20
 */
public abstract class MBaseActivity<P extends BasePresenter> extends BaseActivity {

    public P mPresenter;

    public abstract void initPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }
}