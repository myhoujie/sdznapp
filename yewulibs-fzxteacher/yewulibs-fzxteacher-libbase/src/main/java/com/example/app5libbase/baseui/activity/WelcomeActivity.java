package com.example.app5libbase.baseui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.app.NetworkReceiver;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.baseui.adapter.GuidanceAdapter;
import com.example.app5libbase.baseui.fragment.GuidanceFragment;
import com.example.app5libbase.controller.UserController;
import com.example.app5libbase.login.presenter.LoginPresenter;
import com.example.app5libbase.login.view.LoginView;
import com.example.app5libbase.msg.MqttService;
import com.example.app5libbase.util.SubjectSPUtils;
import com.example.app5libbase.util.base.TimeBaseUtil;
import com.example.app5libbase.views.DelActivityDialog;
import com.example.app5libpublic.constant.Constant;
import com.example.app5libpublic.event.Event;
import com.google.gson.Gson;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.dao.UserSubject;
import com.sdzn.fzx.teacher.presenter.LoginNewPresenter;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.view.LoginViews;
import com.sdzn.fzx.teacher.vo.InClassStatus;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.vo.VersionCode;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class WelcomeActivity extends BaseActivity implements LoginViews {

    private LoginNewPresenter mPresenter;
    private ViewPager guidanceViewpager;
    private RelativeLayout netErrorRy;
    private ImageView ivWelcome;
    private ImageView ivBottomWelcome;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    private List<Fragment> views;
    private GuidanceAdapter vpAdapter;
    private boolean isFromBrought;

    private VersionCode versionCode;
    private DelActivityDialog delActivityDialog;



    public void initPresenter() {
        mPresenter = new LoginNewPresenter();
        mPresenter.onCreate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
//        StatusBarUtil.setLightMode(this);
        setReturnPop(false);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            isFromBrought = true;
            finish();
            return;
        }

        setContentView(R.layout.activity_welcome);
        guidanceViewpager = findViewById(R.id.guidance_viewpager);
        netErrorRy = findViewById(R.id.net_error_ry);
        ivWelcome = findViewById(R.id.iv_welcome);
        ivBottomWelcome = findViewById(R.id.iv_bottom_welcome);

        initView();
        initData();
        EventBus.getDefault().register(this);
        getVersionCode();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入WelcomeActivity成功");
                }
            }
        }
    }


    @Override
    protected void initView() {
        vpAdapter = new GuidanceAdapter(getSupportFragmentManager());
        guidanceViewpager.setAdapter(vpAdapter);
    }

    @Override
    protected void initData() {

    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final boolean isGuidance = (boolean) SPUtils.get(WelcomeActivity.this, SPUtils.GUIDANCE_TAG, false);
            if (isGuidance) {
                LoginBean data = SPUtils.getLoginBean();
                String pwd = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_PAS_CHECK, ""));
                if (data != null && data.getData() != null && !TextUtils.isEmpty(data.getData().getAccessToken()) && !pwd.isEmpty()) {
                    String num = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_USER_NUM, ""));
                    if (!mPresenter.vertifyNum(num, pwd)) {
                        return;
                    }
                    mPresenter.login(num, pwd,WelcomeActivity.this);
                } else {
                    jump();
                }

            } else {
                guidanceViewpager.setVisibility(View.VISIBLE);
                ivWelcome.setVisibility(View.GONE);
                ivBottomWelcome.setVisibility(View.GONE);
                initGuidance();
            }
            SPUtils.put(WelcomeActivity.this, SPUtils.GUIDANCE_TAG, true);

//            jump();
        }
    };

    private void initGuidance() {
        views = new ArrayList<>();
        for (int i = 0; i < GuidanceFragment.pics.length; i++) {
            Fragment fragment = new GuidanceFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(GuidanceFragment.TAG, i);
            fragment.setArguments(bundle);
            views.add(fragment);
        }
        vpAdapter.setData(views);
    }

    @Override
    protected void onDestroy() {
        if (!isFromBrought) {
            handler.removeCallbacksAndMessages(null);
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    public synchronized void jump() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    void getVersionCode() {
        RequestParams params = new RequestParams(Network.BASE_DOWN_URL);
        params.setConnectTimeout(60 * 1000);
        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                versionCode = gson.fromJson(result, VersionCode.class);
                String versionStr = versionCode.getTeacherandroid().getVersion();
                int version = 0;
                try {
                    version = Integer.parseInt(versionStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int localVersion = getVersionName();
                if (version <= localVersion) {
                    handler.sendEmptyMessageDelayed(1, 1000);
                } else {
                    ActivityGvAdapter(versionCode.getTeacherandroid().getDownUrl(), version).show();
                }
            }

            //请求异常后的回调方法
            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                ToastUtil.showLonglToast("App升级网络地址不可用");
                Observable.interval(3, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                finish();
                            }
                        });
            }

            //主动调用取消请求的回调方法
            @Override
            public void onCancelled(CancelledException cex) {
            }

            @Override
            public void onFinished() {
            }
        });
    }



    private int getVersionName() {
        //获取packagemanager的实例
        PackageManager packageManager = getPackageManager();
        //getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.e("TAG", "版本号" + packInfo.versionCode);
        Log.e("TAG", "版本名" + packInfo.versionName);
        return packInfo.versionCode;
    }

    public DelActivityDialog ActivityGvAdapter(final String url, int version) {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(WelcomeActivity.this)
                .buildText("发现新版本 辅助线 " + version)
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                    /*    Intent intent = new Intent(WelcomeActivity.this, DownLoadActivity.class);
                        intent.putExtra("url", url);
                        startActivity(intent);*/
                    }

                    @Override
                    public void onCancel() {
                    }
                });
        return delActivityDialog;
    }

    @Override
    public void loginSuccess(LoginBean.DataBean vo) {
//        final Editable userNumText = userNumEdit.getText();
//        final Editable pswText = pswEdit.getText();
//        SPUtils.put(this, SPUtils.LOGIN_USER_NUM, userNumText);
//        if (check.isChecked()) {
//            SPUtils.put(this, SPUtils.LOGIN_PAS_CHECK, pswText);
//        } else {
//            SPUtils.put(this, SPUtils.LOGIN_PAS_CHECK, "");
//        }
        LoginBean loginBean = new LoginBean();
        loginBean.setData(vo);
        SPUtils.saveLoginBean(loginBean);
//        if (vo != null && vo.getData() != null && vo.getData().getSubjectList() != null && vo.getData().getSubjectList().size() > 0) {
//            UserSubject userSubject = UserController.queryByUserId();
//            LoginBean.DataBean.SubjectListBean subjectListBean = null;
//            if (userSubject != null) {
//                for (LoginBean.DataBean.SubjectListBean bean : vo.getData().getSubjectList()) {
//                    if (bean.getSubjectId() == userSubject.getSubjectId()) {
//                        subjectListBean = bean;
//                    }
//                }
//            }
//            if (subjectListBean == null) {
//                subjectListBean = vo.getData().getSubjectList().get(0);
//            }
//            SubjectSPUtils.saveCurrentSubject(subjectListBean);
//        }
        com.blankj.utilcode.util.SPUtils.getInstance().put("token", vo.getAccessToken());
        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);
        startMqttService();
//        mPresenter.getInClassStatus();

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1");
        startActivity(intent);

        finish();
    }


    private void startMqttService() {
        Intent intent = new Intent(this, MqttService.class);
        startService(intent);

    }


    @Override
    public void loginFailed(String msg) {
        jump();
    }

    @Override
    public void inClassStatusResult(InClassStatus classStatus) {
        if (classStatus.getData() != null && classStatus.getData().getStatus() == 1) {
            Date date = new Date();
            final String now = sdf.format(date);
            int inClassDate = (int) DateUtil.getTimeMillisByStr(classStatus.getData().getClassTime(), "yyyy-MM-dd HH:mm") / 1000;
            int nowDate = (int) DateUtil.getTimeMillisByStr(now, "yyyy-MM-dd HH:mm:ss") / 1000;
            if (inClassDate > nowDate) {
                int differTime = inClassDate - nowDate;
                Constant.setShowHeadCountDown(true);
                BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
                TimeBaseUtil.getInstance().showHeadCountView(differTime);
                SPUtils.put(WelcomeActivity.this, SPUtils.CLASS_ID, String.valueOf(classStatus.getData().getClassId()));
            }
        }
    }
}
