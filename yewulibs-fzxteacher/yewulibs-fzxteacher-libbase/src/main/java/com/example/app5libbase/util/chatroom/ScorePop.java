package com.example.app5libbase.util.chatroom;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RadioGroup;

import com.example.app5libbase.R;
import com.example.app5libbase.listener.chatroom.OnGroupScoreListener;

import org.xutils.x;

/**
 * 弹窗
 */
public class ScorePop extends PopupWindow {


    private Activity activity;

    private RadioGroup radioGroup;

    private OnGroupScoreListener onGroupScoreListener;

    public ScorePop(Activity activity, OnGroupScoreListener onGroupScoreListener) {
        this.onGroupScoreListener = onGroupScoreListener;
        this.activity = activity;
        View contentView = LayoutInflater.from(activity).inflate(R.layout.popup_score, null);
        x.view().inject(this, contentView);
        radioGroup = contentView.findViewById(R.id.radioGroup);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.update();
        this.setAnimationStyle(R.style.AnimationAlpha);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        setSubjects();
    }

    private void setSubjects() {

//        rvClass.addOnItemTouchListener(new OnItemTouchListener(rvClass) {
//            @Override
//            public void onItemClick(RecyclerView.ViewHolder viewHolder) {
//                int position = viewHolder.getAdapterPosition();
//                SyncClassVo.DataBean dataBean = classVos.get(position);
//                ScorePop.this.onClassClickListener.onClassClick(dataBean);
//                dismiss();
//            }
//        });
        radioGroup.setOnCheckedChangeListener(new MyOnCheckedChangeListener());
    }

    class MyOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (i == R.id.rb_0) {
                ScorePop.this.onGroupScoreListener.onScoreClick("0");
            } else if (i == R.id.rb_2) {
                ScorePop.this.onGroupScoreListener.onScoreClick("2");
            } else if (i == R.id.rb_4) {
                ScorePop.this.onGroupScoreListener.onScoreClick("4");
            } else if (i == R.id.rb_6) {
                ScorePop.this.onGroupScoreListener.onScoreClick("6");
            } else if (i == R.id.rb_8) {
                ScorePop.this.onGroupScoreListener.onScoreClick("8");
            } else if (i == R.id.rb_10) {
                ScorePop.this.onGroupScoreListener.onScoreClick("10");
            }
        }
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }

}
