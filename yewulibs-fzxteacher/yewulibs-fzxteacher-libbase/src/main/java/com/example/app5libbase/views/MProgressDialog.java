package com.example.app5libbase.views;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.app5libbase.R;


/**
 * 描述：
 * -
 * 创建人：wangchunxiao
 * 创建时间：2017/3/6
 */
public class MProgressDialog extends Dialog {

    private MProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    /**
     * 当窗口焦点改变时调用
     */
    public void onWindowFocusChanged(boolean hasFocus) {
        ImageView imageView = (ImageView) findViewById(R.id.spinnerImageView);
        final RotateAnimation animation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(3000);//设置动画持续时间
        animation.setRepeatCount(Animation.INFINITE);//设置重复次数
        animation.setFillAfter(false);//动画执行完后是否停留在执行完的状态
        animation.setStartOffset(0);//执行前的等待时间
        LinearInterpolator lir = new LinearInterpolator();
        animation.setInterpolator(lir);
        imageView.setAnimation(animation);
        animation.startNow();
    }

    /**
     * 弹出自定义ProgressDialog
     *
     * @param context 上下文
     * @return
     */
    public static MProgressDialog newInstance(Context context) {

        MProgressDialog dialog = new MProgressDialog(context, R.style.ProgressDialogStyle);
        dialog.setTitle("");
        dialog.setContentView(R.layout.layout_progress_dialog);
        // 按返回键是否取消
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        // 设置居中
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        // 设置背景层透明度
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        // dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        return dialog;
    }
}