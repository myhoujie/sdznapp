package com.example.app5libbase.listener;

import java.util.Calendar;

public interface OnCalenderSelectListener {
    void onCalenderSelect(Calendar startCalendar, Calendar endCalendar);
}
