package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.StudentListTiWen;

/**
 * Created by admin on 2019/8/16.
 */

public class TiWenGridAdapter extends BaseAdapter {
    private Context context;
    StudentListTiWen list;

    public TiWenGridAdapter(Context context) {
        this.context = context;
    }

    public TiWenGridAdapter(Context context,StudentListTiWen list) {
        this.context = context;
        this.list = list;
    }
    public void setListData(StudentListTiWen list){
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int i) {
        return list.getData().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null){
            view = View.inflate(context, R.layout.item_tiwen_grid,null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);

        }
        else {
            viewHolder= (ViewHolder) view.getTag();
        }
        viewHolder.tvName.setText(list.getData().get(i).getRealName());

        return view;
    }
    class ViewHolder {
        protected ImageView ivHead;
        protected TextView tvName;

        public ViewHolder(View convertView) {
            ivHead = (ImageView) convertView.findViewById(R.id.iv_head);
            tvName = (TextView) convertView.findViewById(R.id.tv_name);
        }
    }


}
