package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.StudentList;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/29
 * 修改单号：
 * 修改内容:
 */
public class statisticeAdapter extends BaseAdapter {

    private List<StudentList.DataBean> mList;
    private Context mContext;

    public statisticeAdapter(List<StudentList.DataBean> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.student_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.photo.setVisibility(View.VISIBLE);
        holder.num.setTextColor(mContext.getResources().getColor(R.color.c_808FA3));
        holder.name.setTextColor(mContext.getResources().getColor(R.color.c_323C47));
        holder.state.setTextColor(mContext.getResources().getColor(R.color.c_323C47));
        holder.correct_state.setTextColor(mContext.getResources().getColor(R.color.c_323C47));
        holder.score.setTextColor(mContext.getResources().getColor(R.color.c_323C47));
        holder.time.setTextColor(mContext.getResources().getColor(R.color.c_323C47));

        holder.num.setText((position + 1) + "");

        holder.name.setText(mList.get(position).getUserStudentName() + "");


        if (mList.get(position).getStatus() == 2) {
            holder.state.setText("已完成");
        } else if (mList.get(position).getStatus() == 1) {
            holder.state.setText("作答中");
        } else if (mList.get(position).getStatus() == 3) {
            holder.state.setText("补交");
        } else if (mList.get(position).getStatus() == 0) {
            holder.state.setText("未作答");
        }

        /*老师批改状态*/
        if (mList.get(position).getIsCorrect() == 1) {
            holder.correct_state.setText("已批改");
        } else {
            holder.correct_state.setText("未批改");
        }
        /*学生批改状态*/
        if (mList.get(position).getStudentCorrectStatus() == 1) {
            holder.student_correct_state.setText("未批改");
        } else {
            holder.student_correct_state.setText("已批改");
        }
        /*学生批改人名称*/
        /*if (mList.get(position).getStudentCorrectName().equals("") && mList.get(position).getStudentCorrectName().equals(null)) {
            holder.student_person.setText("---      ");
        } else {
            holder.student_person.setText(mList.get(position).getStudentCorrectName());
        }*/
        /*主观题得分*/
        if (mList.get(position).getScoreSubjective() < 0) {
            holder.subjective_score.setText("---      ");
        } else {
            holder.subjective_score.setText(mList.get(position).getScoreSubjective() + "");
        }
        /*客观题得分*/
        if (mList.get(position).getScoreObjective() < 0) {
            holder.objective_score.setText("---      ");
        } else {
            holder.objective_score.setText(mList.get(position).getScoreObjective() + "");
        }


        if (TextUtils.equals(mList.get(position).getScoreTotal().trim(), "-1")) {
            holder.score.setText("---      ");
        } else {
            if (TextUtils.equals(mList.get(position).getScoreTotal().trim(), "0.0") ||
                    TextUtils.equals(mList.get(position).getScoreTotal().trim(), "0")) {
                holder.score.setText("0");
            } else {
                holder.score.setText(mList.get(position).getScoreTotal() + "");
            }
        }


        if (mList.get(position).getUseTime() % (24 * 60 * 60) / (60 * 60) == 0) {
            holder.time.setText(mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) / 60 + "分"
                    + mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        } else {
            holder.time.setText(mList.get(position).getUseTime() % (24 * 60 * 60) / (60 * 60) + "小时"
                    + mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) / 60 + "分"
                    + mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        }


        if (mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) / 60 == 0) {
            holder.time.setText(mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        } else {
            holder.time.setText(mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) / 60 + "分" + mList.get(position).getUseTime() % (24 * 60 * 60) % (60 * 60) % 60 + "秒");
        }
        return convertView;
    }


    class ViewHolder {
        //序号
        TextView num;
        //头像
        ImageView photo;
        //名称
        TextView name;
        //作答情况
        TextView state;
        //老师批改情况
        TextView correct_state;
        //学生批改情况
        TextView student_correct_state;
        //批改人
        TextView student_person;
        //主观题得分
        TextView subjective_score;
        //客观题得分
        TextView objective_score;
        //总分
        TextView score;
        //用时
        TextView time;


        ViewHolder(View view) {
            num = view.findViewById(R.id.num);
            photo = view.findViewById(R.id.photo);
            name = view.findViewById(R.id.name);
            state = view.findViewById(R.id.state);
            correct_state = view.findViewById(R.id.correct_state);
            student_correct_state = view.findViewById(R.id.student_correct_state);
            student_person = view.findViewById(R.id.student_person);
            subjective_score = view.findViewById(R.id.subjective_score);
            objective_score = view.findViewById(R.id.objective_score);
            score = view.findViewById(R.id.score);
            time = view.findViewById(R.id.time);
        }
    }
}
