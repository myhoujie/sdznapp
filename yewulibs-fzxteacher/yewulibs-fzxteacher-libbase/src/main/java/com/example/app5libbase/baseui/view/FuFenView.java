package com.example.app5libbase.baseui.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.FuFenBean;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/17.
 */
public interface FuFenView extends BaseView {
    void getClassGroupDataSuccess(FuFenBean classGroupingVo);

    void onError(String msg);

    void getAddSuccess();
}
