package com.example.app5libbase.baseui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * TeachActivityAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class TeachActivityAdapter extends FragmentPagerAdapter {

    private List<Fragment> mDatas;

    public void setDatas(List<Fragment> datas) {
        this.mDatas = datas;
        notifyDataSetChanged();
    }

    public TeachActivityAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public int getCount() {
        return mDatas == null ? 0 : mDatas.size();
    }
}
