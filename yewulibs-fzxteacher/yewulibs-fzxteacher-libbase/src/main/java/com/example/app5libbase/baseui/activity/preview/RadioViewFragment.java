package com.example.app5libbase.baseui.activity.preview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.VerticalSeekBar;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 描述：视频播放
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/17
 */
public class RadioViewFragment extends PlayerFragment implements View.OnClickListener {

    RelativeLayout rlVideoTitle;
    RelativeLayout rlRadioArea;
    RelativeLayout rlController;
    ImageView ivPlay;
    TextView tvPlayTime;
    SeekBar seekbar;
    VerticalSeekBar volumeSeekBar;
    LinearLayout llVolumeSeekBar;
    TextView tvTotalTime;
    ImageView ivVolume;
    TextView tvVideoTitle;
    private TextView tvClose;
    private LinearLayout llProgress;
    private RelativeLayout rlTitle;

    private MediaPlayer mediaPlayer;
    private String path;

    private Timer timer = new Timer();
    private TimerTask timerTask;

    private boolean isDisplay = false;

    private static final int MSG_CONTROLLER = 0;
    private static final int HIDE_TIME = 3000;

    private int width;
    private int height;
    private String title;

    private AudioManager mAudioManager;

    private MyVolumeReceiver mVolumeReceiver;

    private boolean isTracking = false;

    private boolean isPlaying;

    public static RadioViewFragment newInstance(Bundle bundle) {
        RadioViewFragment fragment = new RadioViewFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radio_view, null);
        rlVideoTitle = view.findViewById(R.id.rl_title);
        rlRadioArea = view.findViewById(R.id.rlRadioArea);
        rlController = view.findViewById(R.id.rlController);
        ivPlay = view.findViewById(R.id.ivPlay);
        tvPlayTime = view.findViewById(R.id.tvPlayTime);
        seekbar = view.findViewById(R.id.seekbar);
        volumeSeekBar = view.findViewById(R.id.volumeSeekBar);
        llVolumeSeekBar = view.findViewById(R.id.llVolumeSeekBar);
        tvTotalTime = view.findViewById(R.id.tvTotalTime);
        ivVolume = view.findViewById(R.id.ivVolume);
        tvVideoTitle = view.findViewById(R.id.tv_video_title);
        tvClose = view.findViewById(R.id.tv_close);
        llProgress = view.findViewById(R.id.llProgress);
        rlTitle = view.findViewById(R.id.rl_title);
        ivPlay.setOnClickListener(this);
        tvClose.setOnClickListener(this);
        llProgress.setOnClickListener(this);
        rlTitle.setOnClickListener(this);
        llVolumeSeekBar.setOnClickListener(this);

        initView();
        playFunction(view);
        return view;
    }

    private void initView() {
        tvVideoTitle.setText(title);

        // 解决thumb透明问题
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekbar.setSplitTrack(false);
            volumeSeekBar.setSplitTrack(false);
        }

        rlRadioArea.setOnTouchListener(touchListener);
        rlController.setVisibility(View.VISIBLE);
//        if (setScreenSize) {
//            ivFullScreen.setVisibility(View.VISIBLE);
//        } else {
//            ivFullScreen.setVisibility(View.GONE);
//        }
        seekbar.setMax(100);

        mAudioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeSeekBar.setMax(max);
        volumeSeekBar.setProgress(current);

        setVolumeImage(max, current);

        mVolumeReceiver = new MyVolumeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        activity.registerReceiver(mVolumeReceiver, filter);

        setListener();
    }

    private void setListener() {
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        volumeSeekBar.setOnSeekBarChangeListener(onVolumeSeekBarChangeListener);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivPlay) {
            hideControllerDelayed();
            if (mediaPlayer.isPlaying()) {
                isPlaying = false;
                mediaPlayer.pause();
                ivPlay.setImageResource(R.mipmap.bofang_icon);
                updateProgress();
            } else {
                isPlaying = true;
                mediaPlayer.start();
                ivPlay.setImageResource(R.mipmap.zanting_icon);
                updateProgress();
            }
        } else if (id == R.id.ivVolume) {
            int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volumeSeekBar.setProgress(current);
            llVolumeSeekBar.setVisibility(View.VISIBLE);
            // 设置全屏
//                if (mVideoView.isPlaying()) {
//                    hideControllerDelayed();
//                }
//                if (!isFullScreen) {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.FULL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.play_normal);
//                    isFullScreen = true;
//                } else {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.NORMAL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.fullscreen);
//                    isFullScreen = false;
//                }
        } else if (id == R.id.tv_close) {
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
        }
    }

    /**
     * 处理音量变化时的界面显示
     *
     * @author long
     */
    private class MyVolumeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //如果音量发生变化则更改seekbar的位置
            int currVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 当前的媒体音量
            volumeSeekBar.setProgress(currVolume);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public void onDestroy() {
        if (mVolumeReceiver != null) {
            activity.unregisterReceiver(mVolumeReceiver);
        }
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        if (playerHandler != null) {
            playerHandler.removeCallbacksAndMessages(null);
            playerHandler = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroy();
    }

    protected void initData() {
        path = getArguments().getString("path");
        title = getArguments().getString("title");
        width = getArguments().getInt("width");
        height = getArguments().getInt("height");
    }

    private void playFunction(View view) {
        if (TextUtils.isEmpty(path)) {
            ToastUtil.showShortlToast(getString(R.string.fragment_task_content_text12));
        } else {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnBufferingUpdateListener(onBufferingUpdateListener);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    setScreenSizeParams(width, height);
                    long duration = mediaPlayer.getDuration();
                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    tvTotalTime.setText(DateUtil.millsecondsToStr(duration));

                    mediaPlayer.start();
                    hideControllerDelayed();
                    timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            playerHandler.sendEmptyMessage(0);
                        }
                    };
                    timer.schedule(timerTask, 0, 1000);
                    isPlaying = true;

                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
                        }
                    });
                }
            });

            try {
                mediaPlayer.setDataSource(path);
                mediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            seekbar.setSecondaryProgress((int) (((float) percent / (float) mp.getDuration()) * seekbar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mediaPlayer.seekTo(seekBar.getProgress() * mediaPlayer.getDuration() / seekBar.getMax());
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
            isTracking = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
            isTracking = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            tvPlayTime.setText(DateUtil.millsecondsToStr(progress * mediaPlayer.getDuration() / seekBar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onVolumeSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, AudioManager.FLAG_VIBRATE);
            setVolumeImage(seekBar.getMax(), progress);
        }
    };

    public void setVolumeImage(int max, int progress) {
        if (progress > max / 2) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangda_icon));
        } else if (progress == 0) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangwu_icon));
        } else {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangxiao_icon));
        }
    }


    Handler playerHandler = new Handler() {
        public void handleMessage(Message msg) {
            // 更新播放进度
            if (isPlaying) {
                updateProgress();
            }
        }
    };

    private void updateProgress() {
        long position = mediaPlayer.getCurrentPosition();
        long duration = mediaPlayer.getDuration();

        if (duration > 0) {
            long pos = seekbar.getMax() * position / duration;
            tvPlayTime.setText(DateUtil.millsecondsToStr(position));
            if (!isTracking) {
                seekbar.setProgress((int) pos);
            }
        }
    }

    /**
     * 控制播放器面板显示
     */
    private View.OnTouchListener touchListener = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                controllerHandler.removeMessages(MSG_CONTROLLER);
                if (isDisplay) {
                    setLayoutVisibility(View.GONE, false);
                } else {
                    setLayoutVisibility(View.VISIBLE, true);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                hideControllerDelayed();
            }
            return true;
        }
    };

    private void hideControllerDelayed() {
        controllerHandler.removeMessages(MSG_CONTROLLER);
        controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
    }

    private void setLayoutVisibility(int visibility, boolean isDisplay) {
        this.isDisplay = isDisplay;
        rlController.setVisibility(visibility);
        rlVideoTitle.setVisibility(visibility);
        llVolumeSeekBar.setVisibility(View.GONE);
    }

    Handler controllerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setLayoutVisibility(View.GONE, false);
        }
    };

    @Override
    public void setScreenSizeParams(int width, int height) {
    }

    @Override
    public void onResume() {
        if (isPlaying) {
            mediaPlayer.start();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
        super.onPause();
    }
}