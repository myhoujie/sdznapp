package com.example.app5libbase.listener.chatroom;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2019/8/21
 */
public interface OnPageChangeListener {
    void onPageChange(int pos);
}
