package com.example.app5libbase.baseui.activity.preview;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.artifex.mupdflib.FilePicker;
import com.artifex.mupdflib.MuPDFCore;
import com.artifex.mupdflib.MuPDFPageAdapter;
import com.artifex.mupdflib.ReaderView;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * 描述：显示pdf
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/17
 */
public class PDFFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks{

    private String path;
    private long size;
    private String target;
    private MuPDFCore mCore;
    private ReaderView mDocView;
    private ProgressBar loadingPb;

    private static final int REQUECT_CODE_SDCARD = 1000;
    private View rootView;
    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Dialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pdf, null);
        dialog = new Dialog(getActivity(), R.style.notice_dialog);
        loadingPb = (ProgressBar) rootView.findViewById(R.id.pb_loading);
        requestPermissiontest();
        return rootView;
    }

    private void initView(View view) {
        File file = new File(target);
        if (!file.exists()) {
            // 下载文件
            loadingPb.setVisibility(View.VISIBLE);
            download(view);
        } else {
            showPdf(view);
        }
    }

    private void download(final View view) {
        RequestParams params = new RequestParams(path);
        //设置断点续传
        params.setAutoResume(true);
        params.setAutoRename(true);
        params.setSaveFilePath(target);
        x.http().get(params, new Callback.ProgressCallback<File>() {

            @Override
            public void onSuccess(File result) {
                loadingPb.setVisibility(View.GONE);
                showPdf(view);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                loadingPb.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                loadingPb.setVisibility(View.GONE);
            }

            @Override
            public void onFinished() {
            }

            @Override
            public void onWaiting() {
            }

            @Override
            public void onStarted() {
            }

            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
            }
        });
    }

    private void showPdf(View view) {
        FileInputStream fis = null;
        try {
            File file = new File(target);
            fis = new FileInputStream(file);
            if (size != 0 && fis.available() != size) {
                if (file.delete()) {
                    Log.i("msg", "删除成功");
                }
                ToastUtil.showShortlToast("资源不可用");
            } else {
                LinearLayout llPdf = (LinearLayout) view.findViewById(R.id.llPdf);
                llPdf.addView(initPdfView());
                openPdf();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private View initPdfView() {
        RelativeLayout layout = new RelativeLayout(activity);
        mDocView = new ReaderView(activity);
        layout.setBackgroundColor(Color.WHITE);
        layout.addView(mDocView);
        return layout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    protected void initData() {
        Bundle bundle = getArguments();
        path = bundle.getString("path");
        target = bundle.getString("target");
        size = bundle.getLong("size");
    }

    private void openPdf() {
        try {
            mCore = new MuPDFCore(activity, target);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mCore != null && mCore.countPages() == 0) {
            mCore = null;
        }
        if (null == mCore) {
            ToastUtil.showShortlToast("打开失败");
            File file = new File(target);
            if (file.exists()) {
                file.delete();
            }
            return;
        }
        MuPDFPageAdapter mAdapter;
        mAdapter = new MuPDFPageAdapter(activity, new FilePicker.FilePickerSupport() {
            @Override
            public void performPickFor(FilePicker picker) {

            }
        }, mCore);
        mDocView.setAdapter(mAdapter);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            //从设置页面返回，判断权限是否申请。
            if (EasyPermissions.hasPermissions(getActivity(), ALL_PERMISSIONS)) {
//                Toast.makeText(this, "权限申请成功!", Toast.LENGTH_SHORT).show();
                initView(rootView);
                dialog.dismiss();
            } else {
                requestPermissiontest();
//                Toast.makeText(this, "权限申请失败!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissiontest() {
        if (EasyPermissions.hasPermissions(getActivity(), ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            initView(rootView);
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }
//        MPermissions.requestPermissions(this, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
                    startActivityForResult(intent, 16061);
                }
            });
        }
    }

   /* @PermissionGrant(REQUECT_CODE_SDCARD)
    public void requestSdcardSuccess() {
        initView(rootView);
    }

    @PermissionDenied(REQUECT_CODE_SDCARD)
    public void requestSdcardFailed() {
        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
    }*/
}