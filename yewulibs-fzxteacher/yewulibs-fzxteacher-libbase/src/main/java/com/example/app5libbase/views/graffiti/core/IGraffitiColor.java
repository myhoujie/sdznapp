package com.example.app5libbase.views.graffiti.core;

import android.graphics.Paint;

public interface IGraffitiColor {

    /**
     * 配置画笔
     */
    public void config(IGraffitiItem graffitiItem, Paint paint);

    /**
     * 深度拷贝
     * @return
     */
    public IGraffitiColor copy();
}
