package com.example.app5libbase.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import com.example.app5libbase.R;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.BuildConfig3;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;

/**
 * Created by wangc on 2018/6/19 0019.
 */

public class DownService extends Service {
    public static final String BOX_PATH = "boxPath";
    public static final String APP_PATH = "appPath";

    private OnProgressListener onProgressLintener;
    private String boxPath;
    private String appPath;

    public static final int DOWNLOAD_BOX = 1001;
    public static final int DOWNLOAD_APP = 1002;
    public static final int DOWNLOAD_FAILED = 1010;

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DOWNLOAD_BOX:
                    if (TextUtils.isEmpty(boxPath)) {
                        downloadApk(appPath);
                        return;
                    }
                    downloadApk(boxPath);
                    break;
                case DOWNLOAD_APP:
                    downloadApk(appPath);
                    break;
                case DOWNLOAD_FAILED:
                    if (onProgressLintener != null) {
                        onProgressLintener.onFailed((Throwable) msg.obj);
                    }
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new DownBinder();
    }

    public void setOnProgressLintener(OnProgressListener onProgressLintener) {
        this.onProgressLintener = onProgressLintener;
    }

    public class DownBinder extends Binder {
        public DownService getService() {
            return DownService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            boxPath = intent.getStringExtra(BOX_PATH);
            appPath = intent.getStringExtra(APP_PATH);
            mHandler.sendEmptyMessage(DOWNLOAD_BOX);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private final static int NO_1 = 1000;
    private static final String DOWNLOAD_CHANNEL_ID = "com.sdzn.fzx.student.NotifyDownload";
    private static final String DOWNLOAD_CHANNEL_NAME = "com.sdzn.fzx.student.FZXNotifyDownload";

    private void downloadApk(final String url) {
        try {
            File file = new File(FileUtil.getAppFilesDir() + "/apk/update.apk");
            if (file.exists()) {
                FileUtil.delete(file);
            }
            RequestParams params = new RequestParams(url);
            //设置断点续传
            params.setAutoResume(true);
            params.setAutoRename(true);
            params.setConnectTimeout(TextUtils.equals(appPath, url) ? 15000 : 5000);
            params.setSaveFilePath(FileUtil.getAppFilesDir() + "/apk/update.apk");

            final Notification.Builder builder = new Notification.Builder( App2.get());
            builder.setSmallIcon(R.mipmap.logo_notif);
            builder.setLargeIcon(BitmapFactory.decodeResource(
                     App2.get().getResources(), R.mipmap.logo));
            builder.setContentTitle("下载");
            builder.setContentText("正在下载");
            builder.setOnlyAlertOnce(true);
            final NotificationManager manager = (NotificationManager)  App2.get().getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(DOWNLOAD_CHANNEL_ID);
                NotificationChannel channel = new NotificationChannel(DOWNLOAD_CHANNEL_ID,
                        DOWNLOAD_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
                if (manager != null) {
                    manager.createNotificationChannel(channel);
                }
            }
            builder.setProgress(100, 0, false);
            manager.notify(NO_1, builder.build());

            x.http().get(params, new Callback.ProgressCallback<File>() {

                @Override
                public void onSuccess(File result) {
                    builder.setProgress(100, 100, false);
                    builder.setContentText("下载完成");
                    manager.notify(NO_1, builder.build());

                    Uri data;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        // UpdateConfig.FILE_PROVIDER_AUTH 即是在清单文件中配置的authorities
                        data = FileProvider.getUriForFile( App2.get(), BuildConfig3.APPLICATION_ID + ".fileProvider", result);
                        // 给目标应用一个临时授权
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            boolean hasInstallPermission =  App2.get().getPackageManager().canRequestPackageInstalls();
                            if (!hasInstallPermission) {
                                ToastUtil.showShortlToast("安装应用需要用户授权");
                                if (onProgressLintener != null) {
                                    onProgressLintener.requestPermission(data);
                                } else {
                                    startInstallPermissionSettingActivity();
                                }
                                return;
                            }
                        }
                    } else {
                        data = Uri.fromFile(result);
                    }
                    intent.setDataAndType(data, "application/vnd.android.package-archive");
                     App2.get().startActivity(intent);

                    //跳转活动
                    PendingIntent pi = PendingIntent.getActivities( App2.get(), 0, new Intent[]{intent}, PendingIntent.FLAG_CANCEL_CURRENT);
                    builder.setContentIntent(pi);
                    manager.notify(NO_1, builder.build());
                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {
                    ex.printStackTrace();
                    Log.d("下载失败");
                    ToastUtil.showShortlToast("下载失败");
                    builder.setContentText("下载失败");
                    manager.notify(NO_1, builder.build());
                    Message msg = Message.obtain();
                    msg.what = TextUtils.equals(appPath, url) ? DOWNLOAD_FAILED : DOWNLOAD_APP;
                    msg.obj = ex;
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {
                }

                @Override
                public void onWaiting() {
                }

                @Override
                public void onStarted() {
                }

                @Override
                public void onLoading(long total, long current, boolean isDownloading) {
                    Log.i("下载中" + current);
                    builder.setProgress(100, (int) (current * 100 / total), false);
                    builder.setContentText("已下载" + (int) (current * 100 / total) + "%");
                    manager.notify(NO_1, builder.build());
                    if (onProgressLintener != null) {
                        onProgressLintener.onProgressChanged((int) (current * 100 / total));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessage(TextUtils.equals(appPath, url) ? DOWNLOAD_FAILED : DOWNLOAD_APP);
        }
    }

    /**
     * 跳转到设置-允许安装未知来源-页面
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {//注意这个是8.0新API
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         App2.get().startActivity(intent);
    }

    public interface OnProgressListener {
        void onProgressChanged(int progress);

        @TargetApi(26)
        void requestPermission(Uri data);

        void onFailed(Throwable throwable);
    }
}
