package com.example.app5libbase.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.app5libbase.R;

/**
 * 自定义主界面小标题
 *
 * @author wangchunxiao
 * @date 2018/1/8
 */
public class MainFragmentTitleView extends LinearLayout {

    private TextView tvTitle;
    private TextView tvNum;

    public MainFragmentTitleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.view_main_fragment_title, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        tvNum = findViewById(R.id.tvNum);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.MainFragmentTitleView);
        if (attributes != null) {
            String title = attributes.getString(R.styleable.MainFragmentTitleView_title);
            if (!TextUtils.isEmpty(title)) {
                tvTitle.setText(title);
            }
            int num = attributes.getInteger(R.styleable.MainFragmentTitleView_num, 0);
            int numVisibiliy = attributes.getInteger(R.styleable.MainFragmentTitleView_numVisibiliy, View.VISIBLE);
            tvNum.setVisibility(numVisibiliy);
            tvNum.setText(String.valueOf(num));
            attributes.recycle();
        }
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
    }

    public void setNum(int num) {
        tvNum.setText(String.valueOf(num));
    }

    public void setNumVisibiliy(int visibiliy) {
        tvNum.setVisibility(visibiliy);
    }
}
