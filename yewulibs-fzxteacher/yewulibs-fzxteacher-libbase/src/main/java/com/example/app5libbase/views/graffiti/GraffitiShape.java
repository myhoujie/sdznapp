package com.example.app5libbase.views.graffiti;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.app5libbase.views.graffiti.core.IGraffitiItem;
import com.example.app5libbase.views.graffiti.core.IGraffitiShape;


/**
 * 常用图形
 */
public enum GraffitiShape implements IGraffitiShape {
    HAND_WRITE;// 手绘

    @Override
    public void draw(Canvas canvas, IGraffitiItem graffitiItem, Paint paint) {
        GraffitiPath graffitiPath = (GraffitiPath) graffitiItem;
        canvas.drawPath(graffitiPath.getPath(), paint);
    }

    @Override
    public IGraffitiShape copy() {
        return this;
    }
}
