package com.example.app5libbase.util.dao;

import android.database.sqlite.SQLiteDatabase;

import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.dao.DaoMaster;
import com.sdzn.fzx.teacher.dao.DaoSession;

public class DaoInitutil {

    private static DaoInitutil daoInitutil = null;

    public DaoInitutil() {
        initDao();
    }


    public static DaoInitutil getInstance() {
        synchronized (DaoInitutil.class) {
            if (daoInitutil == null) {
                daoInitutil = new DaoInitutil();
            }
        }
        return daoInitutil;
    }

    private static DaoSession daoSession;

    private void initDao() {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(App2.get(), "basemvp.db");
        SQLiteDatabase writableDatabase = devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(writableDatabase);
        daoSession = daoMaster.newSession();
    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }
}
