package com.example.app5libbase.baseui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.baseui.adapter.FuFenAdapter;
import com.example.app5libbase.baseui.adapter.FuFenStudentAdapter;
import com.example.app5libbase.base.MBaseActivity;
import com.example.app5libbase.baseui.presenter.FuFenPresenter;
import com.example.app5libbase.baseui.view.FuFenView;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.example.app5libbase.util.SubjectSPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.teacher.vo.FuFenBean;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVo;
import com.sdzn.fzx.teacher.vo.me.ClassGroupingVoListVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/17.
 */
public class FuFenActivity extends MBaseActivity<FuFenPresenter> implements FuFenView, View.OnClickListener {

    private RelativeLayout head;
    private TextView tvBack;
    private LinearLayout llBotom;
    private TextView tvL1;
    private TextView tvL2;
    private TextView tvL3;
    private TextView tvL4;
    private TextView tvL5;
    private TextView tvR1;
    private TextView tvR2;
    private TextView tvR3;
    private TextView tvR4;
    private TextView tvR5;
    private RecyclerView rv;


    FuFenAdapter mAdapter;
    List<ClassGroupingVoListVo> list = new ArrayList<>();


    @Override
    public void initPresenter() {
        mPresenter = new FuFenPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fufen);
        head = (RelativeLayout) findViewById(R.id.head);
        tvBack = (TextView) findViewById(R.id.tv_back);
        llBotom = (LinearLayout) findViewById(R.id.ll_botom);
        tvL1 = (TextView) findViewById(R.id.tv_l1);
        tvL2 = (TextView) findViewById(R.id.tv_l2);
        tvL3 = (TextView) findViewById(R.id.tv_l3);
        tvL4 = (TextView) findViewById(R.id.tv_l4);
        tvL5 = (TextView) findViewById(R.id.tv_l5);
        tvR1 = (TextView) findViewById(R.id.tv_r1);
        tvR2 = (TextView) findViewById(R.id.tv_r2);
        tvR3 = (TextView) findViewById(R.id.tv_r3);
        tvR4 = (TextView) findViewById(R.id.tv_r4);
        tvR5 = (TextView) findViewById(R.id.tv_r5);
        rv = (RecyclerView) findViewById(R.id.rv);
        tvL1.setOnClickListener(this);
        tvL2.setOnClickListener(this);
        tvL3.setOnClickListener(this);
        tvL4.setOnClickListener(this);
        tvL5.setOnClickListener(this);
        tvR1.setOnClickListener(this);
        tvR2.setOnClickListener(this);
        tvR3.setOnClickListener(this);
        tvR4.setOnClickListener(this);
        tvR5.setOnClickListener(this);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入FuFenActivity成功");
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void initView() {
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FuFenActivity.this.finish();
            }
        });
    }

    private String mClassId = "604";

    @Override
    protected void initData() {
        // TODO: 2019/6/19 id从何获取?
        mClassId = SPUtils.get(FuFenActivity.this, SPUtils.CLASS_ID, "").toString();
        mPresenter.getClassGroupList(mClassId);

    }

    @Override
    public void getClassGroupDataSuccess(FuFenBean fenBean) {
        if (fenBean == null || fenBean.getData().isEmpty()) {
            return;
        }
        list.clear();
        for (int group = 0; group < fenBean.getData().size(); group++) {
            List<ClassGroupingVo.DataBean> mList = new ArrayList<>();
            for (int stu = 0; stu < fenBean.getData().get(group).getStudentList().size(); stu++) {
                mList.add(new ClassGroupingVo.DataBean(fenBean.getData().get(group).getStudentList().get(stu).getClassId(),
                        fenBean.getData().get(group).getStudentList().get(stu).getClassName(), fenBean.getData().get(group).getStudentList().get(stu).getStudentId()
                        , fenBean.getData().get(group).getStudentList().get(stu).getStudentName(), fenBean.getData().get(group).getStudentList().get(stu).getClassGroupId(),
                        fenBean.getData().get(group).getStudentList().get(stu).getClassGroupName(), false));
            }
            list.add(new ClassGroupingVoListVo(fenBean.getData().get(group).getGroupName(), String.valueOf(fenBean.getData().get(group).getPoints()), mList));
        }


        mAdapter = new FuFenAdapter(this, list, new FuFenStudentAdapter.OnStudentClickListener() {

            @Override
            public void onStudentClick(ClassGroupingVo.DataBean bean) {
                if (lastBean != null) {
                    lastBean.setChecked(false);
                }
                lastBean = bean;
                lastBean.setChecked(true);
                mAdapter.notifyDataSetChanged();
            }
        });
        rv.setAdapter(mAdapter);
    }

    private ClassGroupingVo.DataBean lastBean = null;

    @Override
    public void onError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void getAddSuccess() {
        fuFenScoure = 0;
        lastBean = null;
        mPresenter.getClassGroupList(mClassId);
    }

    private int fuFenScoure = 0;

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_l1) {
            fuFenScoure = 1;
        } else if (id == R.id.tv_l2) {
            fuFenScoure = 2;
        } else if (id == R.id.tv_l3) {
            fuFenScoure = 3;
        } else if (id == R.id.tv_l4) {
            fuFenScoure = 4;
        } else if (id == R.id.tv_l5) {
            fuFenScoure = 5;
        } else if (id == R.id.tv_r1) {
            fuFenScoure = -1;
        } else if (id == R.id.tv_r2) {
            fuFenScoure = -2;
        } else if (id == R.id.tv_r3) {
            fuFenScoure = -3;
        } else if (id == R.id.tv_r4) {
            fuFenScoure = -4;
        } else if (id == R.id.tv_r5) {
            fuFenScoure = -5;
        }
        if (fuFenScoure != 0 && lastBean != null) {
            Map<String, Object> parms = new HashMap<>();
            parms.put("points", fuFenScoure);
            parms.put("subjectId", SubjectSPUtils.getCurrentSubject().getSubjectId());
            parms.put("subjectName", SubjectSPUtils.getCurrentSubject().getSubjectName());
            parms.put("classId", lastBean.getClassId());
            parms.put("className", lastBean.getClassName());
            parms.put("groupId", lastBean.getClassGroupId());
            parms.put("groupName", lastBean.getClassGroupName());
            parms.put("studentId", lastBean.getStudentId());
            parms.put("studentName", lastBean.getStudentName());
            parms.put("type", "2");
            mPresenter.addFuFen(parms);
        } else if (lastBean == null) {
            ToastUtil.showLonglToast("请选择评分学生");
        } else {

        }
    }
}
