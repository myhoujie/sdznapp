package com.example.app5libbase.login.fragment;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libbase.login.activity.ForgetPswActivity;
import com.example.app5libbase.base.MBaseFragment;
import com.example.app5libbase.login.presenter.ResetPswPresenter;
import com.example.app5libbase.login.presenter.ResetPswPresenter1;
import com.example.app5libbase.login.view.ResetPswView;
import com.example.app5libbase.login.view.ResetPswViews;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPswFragment extends BaseFragment implements ResetPswViews, TextWatcher, View.OnClickListener {
    ResetPswPresenter1 mPresenter;
    public static final String USER_PHONE = "user_phone";
    public static final String VERIFT_CODE = "verify_code";
    private RelativeLayout titleContainerRy;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleNameTxt;
    private EditText newPswEdit;
    private ImageView newEditDelImg;
    private EditText surePswEdit;
    private ImageView sureEditDelImg;
    private Button nextBtn;
    private String phoneNum;
    private String verifyCode;

    public ResetPswFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_psw, container, false);
        titleContainerRy = (RelativeLayout) view.findViewById(R.id.title_container_ry);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleNameTxt = (TextView) view.findViewById(R.id.title_name_txt);
        newPswEdit = (EditText) view.findViewById(R.id.new_psw_edit);
        newEditDelImg = (ImageView) view.findViewById(R.id.new_edit_del_img);
        surePswEdit = (EditText) view.findViewById(R.id.sure_psw_edit);
        sureEditDelImg = (ImageView) view.findViewById(R.id.sure_edit_del_img);
        nextBtn = (Button) view.findViewById(R.id.next_btn);

        titleBackLy.setOnClickListener(this);
        newEditDelImg.setOnClickListener(this);
        sureEditDelImg.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        initPresenter();
        initView();
        return view;
    }


    public void initPresenter() {
        mPresenter = new ResetPswPresenter1();
        mPresenter.onCreate(this);
    }

    private void initView() {
        titleNameTxt.setText("重置密码");
        newPswEdit.addTextChangedListener(this);
        surePswEdit.addTextChangedListener(this);

        final Bundle arguments = getArguments();
        phoneNum = arguments.getString(USER_PHONE);
        verifyCode = arguments.getString(VERIFT_CODE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.title_back_ly) {
            getActivity().onBackPressed();
        } else if (id == R.id.new_edit_del_img) {
            newPswEdit.setText("");
            newPswEdit.requestFocus();
        } else if (id == R.id.sure_edit_del_img) {
            surePswEdit.setText("");
            surePswEdit.requestFocus();
        } else if (id == R.id.next_btn) {
            mPresenter.changePsw(phoneNum, verifyCode, newPswEdit.getText().toString(), surePswEdit.getText().toString());
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        changeLoginStatus();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void changeLoginStatus() {
        final Editable userNumText = newPswEdit.getText();
        final Editable pswText = surePswEdit.getText();
        /**
         * 思路：默认为 不可登录（false）状态，用户名有文字时，设为true，没有文字设置为false，
         * 接着判断密码，如果没文字，设置为false，如果有文字，则不改变之前的状态
         */

        boolean canLogin = !TextUtils.isEmpty(userNumText);

        if (TextUtils.isEmpty(pswText)) {
            canLogin = false;
        }

        nextBtn.setEnabled(canLogin);

    }

    @Override
    public void changePswSucced() {
        ToastUtil.showShortlToast("重置成功");
        getActivity().finish();
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
