package com.example.app5libbase.ai.pop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActBean;
import com.example.app5libbase.ai.pop.bean.GRZXBaseRecActChildBean;
import com.lxj.xpopup.core.DrawerPopupView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.teacher.bean.GrzxRecActBean;
import com.sdzn.fzx.teacher.utils.SPUtils;

import java.util.ArrayList;
import java.util.List;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * Description: 自定义抽屉弹窗
 * <p>
 * //通过设置topMargin，可以让Drawer弹窗进行局部阴影展示
 * //        ViewGroup.MarginLayoutParams params = (MarginLayoutParams) getPopupContentView().getLayoutParams();
 * //        params.topMargin = 450;
 */
public class CustomDrawerPopupView1 extends DrawerPopupView {
    private RecyclerView mRecyclerView;
    private List<GRZXBaseRecActBean> mList;
    private GRZXBaseRecActAdapter mAdapter;
    GrzxNextCallBack grzxNextCallBack;


    public CustomDrawerPopupView1(@NonNull Context context) {
        super(context);
    }

    public CustomDrawerPopupView1(@NonNull Context context, GrzxNextCallBack mGrzxNextCallBack) {
        super(context);
        this.grzxNextCallBack = mGrzxNextCallBack;
    }


    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_right_drawer;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findview();
        donetwork();
//        onclicklistener();
    }


    private void findview() {
        mRecyclerView = findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));

    }

    private void donetwork() {
        mList = new ArrayList<>();
//        mList = getMultipleItemData(2);
//        mAdapter = new GRZXBaseRecActAdapter(mList);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        mAdapter.setNotDoAnimationCount(3);// mFirstPageItemCount

        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int type = mList.get(position).type;
                if (type == GRZXBaseRecActBean.style1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }


//    private void onclicklistener() {
//        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                GRZXBaseRecActBean addressBean = mList.get(position);
//                int i = view.getId();
//                if (i == R.id.iv_item) {
////                    ToastUtils.showShort(position + " click=" + addressBean.getmBean().getUserName());
//                    if (!addressBean.getmBean().getCreatedAt().isEmpty()) {
//                        if ("com.sdzn.pkt.teacher.hd".equals(addressBean.getmBean().getCreatedAt())) {
//                            if (AppUtils.isAppInstalled("com.sdzn.pkt.teacher.hd")) {
//                                String name = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_USER_NUM, ""));
//                                String pwd = String.valueOf(SPUtils.get(App2.get(), SPUtils.LOGIN_PAS_CHECK, ""));
//                                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
//                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.teacher.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd));
//                                getContext().startActivity(intent);
//                            } else {
//
//                                ToastUtils.showLong("请先安装智囊学堂老师端APP");
//                            }
//                        } else if (".hs.act.WebBaiduEncyclopediaActivity".equals(addressBean.getmBean().getCreatedAt())) {
//                            Intent intent = new Intent(AppUtils.getAppPackageName() + addressBean.getmBean().getCreatedAt());
//                            if (!addressBean.getmBean().getAddress().isEmpty()) {
//                                intent.putExtra("url_key", "https://baike.baidu.com/vbaike#");
//                            }
//                            getContext().startActivity(intent);
//                        } else {
//                            Intent intent = new Intent(AppUtils.getAppPackageName() + addressBean.getmBean().getCreatedAt());
//                            if (!addressBean.getmBean().getAddress().isEmpty()) {
//                                intent.putExtra("url_key", com.blankj.utilcode.util.SPUtils.getInstance().getString("url", "https://www.baidu.com/") + addressBean.getmBean().getAddress());
//                            }
//                            intent.putExtra("show", "1");
//                            getContext().startActivity(intent);
//                        }
//                    }
//                    dismiss();
//                }
//            }
//        });
//
//        findViewById(R.id.btn).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
//        findViewById(R.id.rl_data).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (grzxNextCallBack != null) {
////                    grzxNextCallBack.toGrzxNextClick();
////                }
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//                intent.putExtra("show", 0);
//                startActivity(intent);
//            }
//        });
//    }


//    public static List<GRZXBaseRecActBean> getMultipleItemData(int lenth) {
//        List<GRZXBaseRecActBean> list = new ArrayList<>();
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("课堂流程")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_1, ".hs.act.WdkcActivity", "indexPad")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_2, ".hs.act.KtjxShouyeActivity", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_3, ".hs.act.ZzxxShouyeActivity", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_4, ".hs.act.ZyJiLuActivity", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("教学功能")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_5, ".hs.act.KaoShiActivity", "examination/index/sectionTopics")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_6, ".hs.act.TeacherpgActivity", "taskCorrecting/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_7, ".hs.act.XqfxActivity", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_8, ".hs.act.HeZuoActivity", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_9, ".hs.act.WdbjActivity", "myClass/index")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, new GRZXBaseRecActChildBean("学习提升")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_10, "com.sdzn.pkt.teacher.hd", "")));
//        list.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, new GRZXBaseRecActChildBean(R.mipmap.grzx_11, ".hs.act.WebBaiduEncyclopediaActivity", "https://baike.baidu.com/vbaike#")));
//        return list;
//    }

    private List<GrzxRecActBean> grzxRecActBeanList;


    public interface GrzxNextCallBack {
        void toGrzxNextClick();
    }


    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}