package com.example.app5libbase.ai.pop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app5libbase.R;
import com.example.app5libbase.ai.pop.bean.TextModel;


import java.util.List;

public class MyAdapter extends BaseQuickAdapter<TextModel, BaseViewHolder> {
    private String clickId = "";;
    public MyAdapter(List<TextModel> list) {
        super(R.layout.recycleview_movegroup_item);
    }

    //选中传过来的ID对应的item     也可以设置默认
    public void setClickPosition(String clickId) {
        this.clickId = clickId;
    }

    @Override
    protected void convert(BaseViewHolder helper, TextModel item) {
        helper.setText(R.id.tv1, String.valueOf(item.getText()));
        helper.addOnClickListener(R.id.tv1);


       if (clickId.equals(item.getText())){
           helper.setTextColor(R.id.tv1, Color.parseColor("#D9000000"));
           helper.setChecked(R.id.tv1, true);
       }else {
           helper.setTextColor(R.id.tv1, Color.parseColor("#D9000000"));
           helper.setChecked(R.id.tv1, false);
       }

    }

    @Override
    public void remove(int position) {
        super.remove(position);

    }
}
