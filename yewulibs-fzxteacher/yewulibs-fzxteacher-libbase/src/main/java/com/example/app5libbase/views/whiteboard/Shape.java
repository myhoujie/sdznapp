package com.example.app5libbase.views.whiteboard;

/**
 * 图形
 */
public enum Shape {
    HAND_WRITE, //
    ARROW, // 箭头
    LINE, // 直线
    DASHED,//虚线
    OVAL,//椭圆
    FILL_CIRCLE, // 实心圆
    HOLLOW_CIRCLE, // 空心圆
    FILL_RECT, // 实心矩形
    HOLLOW_RECT, // 空心矩形
    SQUARE,//正方形
    TRIANGLE//三角
}