package com.example.app5libbase.msg;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.ActivityManager;
import com.example.app5libbase.base.BaseActivity;
import com.sdzn.fzx.teacher.utils.SPUtils;


public class OtherLoginTipActivity extends BaseActivity implements View.OnClickListener {

    public static final String EXTRA_OTHER_LOGIN_MSG = "other_login_msg";
    private RelativeLayout activityOtherLoginTip;
    private TextView msgContentTxt;
    private TextView reLoad;
    private TextView exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_login_tip);
        activityOtherLoginTip = (RelativeLayout) findViewById(R.id.activity_other_login_tip);
        msgContentTxt = (TextView) findViewById(R.id.msg_content_txt);
        reLoad = (TextView) findViewById(R.id.re_load);
        exit = (TextView) findViewById(R.id.exit);
        reLoad.setOnClickListener(this);
        exit.setOnClickListener(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入OtherLoginTipActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        Intent intent = new Intent(this, MqttService.class);
        stopService(intent);
    }

    @Override
    protected void initData() {
        final Intent intent = getIntent();
        String msg = intent.getStringExtra(EXTRA_OTHER_LOGIN_MSG);
        if (TextUtils.isEmpty(msg))
            msg = "您的账号在其他上登陆，如不是本人操作，请重新登陆并及时修改密码";
        msgContentTxt.setText(msg);
    }

    @Override
    public void onClick(View view) {
//        SPUtils.put(OtherLoginTipActivity.this, SPUtils.LOGIN_PAS_CHECK, "");
//        SPUtils.put(OtherLoginTipActivity.this, SPUtils.LOGIN_CHECK_STATUS, false);
        int id = view.getId();
        if (id == R.id.re_load) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            ActivityManager.finishAll();
        } else if (id == R.id.exit) {
            ActivityManager.finishAll();
            System.exit(0);
        }
    }
}
