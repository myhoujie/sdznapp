package com.example.app5libbase.login.view;


import com.example.app5libbase.base.BaseView;

/**
 * VerifyNumView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public interface VerifyNumView extends BaseView {

    public void onCountDownChanged(int str);

    void verifySuccess(final String phone, final String code);
}
