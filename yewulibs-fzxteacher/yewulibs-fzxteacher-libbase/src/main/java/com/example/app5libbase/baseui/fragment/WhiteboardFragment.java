package com.example.app5libbase.baseui.fragment;


import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseFragment;
import com.example.app5libpublic.event.BaiBanScreenEvent;
import com.example.app5libbase.util.CustomClicklistener;
import com.example.app5libbase.views.whiteboard.PopView;
import com.example.app5libbase.views.whiteboard.GraffitiView;
import com.example.app5libbase.views.whiteboard.Pen;
import com.example.app5libbase.views.whiteboard.Shape;

import org.greenrobot.eventbus.EventBus;

/**
 * 电子白板
 * 张超
 */
public class WhiteboardFragment extends BaseFragment implements View.OnClickListener {
    private GraffitiView graffitiview;
    private RadioGroup radioGroup;
    private RadioButton rbExit;
    private RadioButton rbqingping;
    private RadioButton rbchexiao;
    private RadioButton rbhuifu;
    private RadioButton rbyanse;
    private RadioButton rbhuabi;
    private RadioButton rbtuxing;
    private RadioButton rbxiangpi;
    private RadioButton rbwenzi;
    private RadioButton rbbeijing;
    private RadioButton rbtupian;
    private RadioButton rbfasong;

    private ObjectAnimator rlToolsHideAnimator;
    private ObjectAnimator rlToolsShowAnimator;

    private final static int NONE = -1;
    private final static int SHAPE = 0;
    private final static int ERASER = 1;
    private final static int PEN = 2;
    private int tool = NONE;

    private Shape shape;
    private int shapeColor;
    private int penColor;
    private int shapeSize;
    private int eraserSize;
    private int penSize;
    private boolean flag = true;
    PopupWindow popupWindow;

    public WhiteboardFragment() {
        // Required empty public constructor
    }

    public static WhiteboardFragment newInstance(Bundle bundle) {
        WhiteboardFragment whiteboardFragment = new WhiteboardFragment();
        whiteboardFragment.setArguments(bundle);
        return whiteboardFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_whiteboard, container, false);
        graffitiview = (GraffitiView) view.findViewById(R.id.graffitiview);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        rbExit = (RadioButton) view.findViewById(R.id.rbExit);
        rbqingping = (RadioButton) view.findViewById(R.id.rbqingping);
        rbchexiao = (RadioButton) view.findViewById(R.id.rbchexiao);
        rbhuifu = (RadioButton) view.findViewById(R.id.rbhuifu);
        rbyanse = (RadioButton) view.findViewById(R.id.rbyanse);
        rbhuabi = (RadioButton) view.findViewById(R.id.rbhuabi);
        rbtuxing = (RadioButton) view.findViewById(R.id.rbtuxing);
        rbxiangpi = (RadioButton) view.findViewById(R.id.rbxiangpi);
        rbwenzi = (RadioButton) view.findViewById(R.id.rbwenzi);
        rbbeijing = (RadioButton) view.findViewById(R.id.rbbeijing);
        rbtupian = (RadioButton) view.findViewById(R.id.rbtupian);
        rbfasong = (RadioButton) view.findViewById(R.id.rbfasong);

        rbExit.setOnClickListener(this);
        rbqingping.setOnClickListener(this);
        rbchexiao.setOnClickListener(this);
        rbhuifu.setOnClickListener(this);
        rbyanse.setOnClickListener(this);
        rbhuabi.setOnClickListener(this);
        rbtuxing.setOnClickListener(this);
        rbxiangpi.setOnClickListener(this);
        rbwenzi.setOnClickListener(this);
        rbbeijing.setOnClickListener(this);
        rbtupian.setOnClickListener(this);
        initView();
        return view;
    }

    private void initView() {
        rbfasong.setOnClickListener(new CustomClicklistener() {
            @Override
            protected void onSingleClick() {
                getScreen();
            }

            @Override
            protected void onFastClick() {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rbExit) {
            activity.finish();
        } else if (id == R.id.rbqingping) {
            graffitiview.clear();
        } else if (id == R.id.rbchexiao) {
            graffitiview.undo();
        } else if (id == R.id.rbhuifu) {
            graffitiview.redo();
        } else if (id == R.id.rbyanse) {
            popupWindow = PopView.showColorPopupWindow(rbyanse, activity);
            popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
            PopView.setOnColorClick(new PopView.onColorClick() {
                @Override
                public void onClickColor(int color) {
                    switch (color) {
                        case 1:
                            graffitiview.setColor(getResources().getColor(R.color.white));
                            break;
                        case 2:
                            graffitiview.setColor(getResources().getColor(R.color.palette_red));
                            break;
                        case 3:
                            graffitiview.setColor(getResources().getColor(R.color.black));
                            break;
                        case 4:
                            graffitiview.setColor(getResources().getColor(R.color.palette_yellow));
                            break;
                        case 5:
                            graffitiview.setColor(getResources().getColor(R.color.palette_green));
                            break;
                    }
                }
            });
        } else if (id == R.id.rbhuabi) {
            PopView.showPenPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
            PopView.setOnPenClick(new PopView.onPenClick() {
                @Override
                public void onClickPen(int size) {
                    graffitiview.setPen(Pen.HAND);
                    graffitiview.setPaintSize(size);
                }
            });
        } else if (id == R.id.rbtuxing) {
            PopView.showtuxingPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
            PopView.setonTuXingClick(new PopView.onTuXingClick() {
                @Override
                public void onClickTuXing(int type) {
                    switch (type) {
                        case 1:
                            graffitiview.setShape(Shape.LINE);
                            break;
                        case 2:
                            graffitiview.setShape(Shape.HOLLOW_RECT);
                            break;
                        case 3:
                            graffitiview.setShape(Shape.HOLLOW_CIRCLE);
                            break;
                        case 4:
                            graffitiview.setShape(Shape.TRIANGLE);
                            break;
                    }
                }
            });
        } else if (id == R.id.rbxiangpi) {
            PopView.showXinagpiPopupWindow(rbyanse, activity).showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
            PopView.setonXinagPiClick(new PopView.onXiangPiClick() {
                @Override
                public void onClickTuXing(int size) {
                    graffitiview.setPen(Pen.ERASER);
                    graffitiview.setShape(Shape.HAND_WRITE);
                    graffitiview.setEraserSize(size);
                }
            });
        } else if (id == R.id.rbwenzi) {
        } else if (id == R.id.rbbeijing) {
            try {
                if (flag) {
                    graffitiview.setColorBg(Color.parseColor("#24604C"));
                    flag = false;
                } else {
                    graffitiview.setColorBg(Color.parseColor("#ffffff"));
                    flag = true;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.rbtupian) {
        }
    }


    /**
     * 截屏
     */
    public void getScreen() {
        EventBus.getDefault().post(new BaiBanScreenEvent());

    }

}
