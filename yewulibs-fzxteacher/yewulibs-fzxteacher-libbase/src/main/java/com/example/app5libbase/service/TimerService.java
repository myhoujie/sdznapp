package com.example.app5libbase.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 描述：倒计时服务
 * -
 * 创建人：wangchunxiao
 * 创建时间：2016/9/20
 */
public class TimerService extends Service {

    private Timer timer;
    private Handler handler;

    private int recLen;

    public static final int MSG_TIMER_RUNNING = 0;
    public static final int MSG_TIMER_FINISHED = 1;

    @Override
    public IBinder onBind(Intent intent) {
        return new TimerBinder();
    }

    public class TimerBinder extends Binder {
        public TimerService getService() {
            return TimerService.this;
        }
    }

    public void startTimer() {
        // 如果已经存在计时任务，需要先将之前的任务取消，否则会造成多任务同时运行的情况
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new Timer();
        timer.schedule(new MyTimerTask(), 0, 1000);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (recLen < 0) {
                // 倒计时结束时做的动作
                timer.cancel();
                handler.sendEmptyMessage(MSG_TIMER_FINISHED);
            } else {
                // 倒计时正在进行时做的动作
                Message msg = Message.obtain();
                msg.obj = recLen;
                msg.what = MSG_TIMER_RUNNING;
                handler.sendMessage(msg);
            }
            recLen--;
        }
    }

    /**
     * 往正在倒计时的任务中添加时间
     *
     * @param rec
     */
    public void addTime(int rec) {
        this.recLen += rec;
    }

    public void setTime(int rec) {
        this.recLen = rec;
    }

    @Override
    public void onDestroy() {
        if (timer != null) {
            timer.cancel();
        }
        super.onDestroy();
    }
}
