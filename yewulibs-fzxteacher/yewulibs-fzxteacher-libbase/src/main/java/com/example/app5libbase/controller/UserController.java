package com.example.app5libbase.controller;

import com.example.app5libbase.util.dao.DaoInitutil;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.teacher.vo.LoginBean;
import com.sdzn.fzx.teacher.dao.UserSubject;
import com.sdzn.fzx.teacher.dao.UserSubjectDao;

import java.util.List;

public class UserController {
    public static int getUserId() {
//        LoginBean loginBean = SPUtils.getLoginBean();
//        if (loginBean != null) {
//            return loginBean.getData().getUser().getId();
//        }
        return -1;
    }

    public static String getUserName() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getRealName());
        }
        return "";
    }

    public static String getPhoto() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getPhoto());
        }
        return "";
    }

    public static String getAccessToken() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null) {
            return loginBean.getData().getAccessToken();
        }
        return null;
    }

    public static String getRefreshToken() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null) {
            return loginBean.getData().getRefreshToken();
        }
        return null;
    }

    public static String getAccountId() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getAccountId());
        }
        return null;
    }

    public static String getBaseLevelId() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return String.valueOf(loginBean.getData().getUser().getBaseLevelId());
        }
        return null;
    }

    public static LoginBean getLoginBean() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return loginBean;
        }
        return null;
    }

    public static List<LoginBean.DataBean.SubjectListBean> getSubjectList() {
        LoginBean loginBean = SPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getSubjectList() != null) {
            return loginBean.getData().getSubjectList();
        }
        return null;
    }

    public static boolean isManager() {
        LoginBean loginBean = SPUtils.getLoginBean();
        return loginBean != null && loginBean.getData() != null && loginBean.getData().getIsManager() == 1;
    }

    public static UserSubject queryByUserId() {
        return DaoInitutil.getInstance().getDaoSession().getUserSubjectDao().queryBuilder()
                .where(UserSubjectDao.Properties.UserId.eq(String.valueOf(getUserId())))
                .build().unique();
    }

    public static void saveSubject(int subjectId) {
        UserSubject userSubject = queryByUserId();
        if (userSubject != null) {
            DaoInitutil.getInstance().getDaoSession().getUserSubjectDao().delete(userSubject);
        }

        UserSubject us = new UserSubject();
        us.setUserId(getUserId());
        us.setSubjectId(subjectId);
        DaoInitutil.getInstance().getDaoSession().getUserSubjectDao().insert(us);
    }
}
