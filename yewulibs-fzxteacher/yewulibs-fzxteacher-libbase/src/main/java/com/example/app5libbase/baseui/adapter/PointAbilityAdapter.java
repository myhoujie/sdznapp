package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.PointVo;

import java.util.List;

/**
 *
 */
public class PointAbilityAdapter extends BaseAdapter {
    private String ErrorRate = "0.0%";//正确率
    private List<PointVo.AbilityBean> mList;
    private Context mContext;

    public PointAbilityAdapter(List<PointVo.AbilityBean> mList, Context mContext, String ErrorRate) {
        this.mList = mList;
        this.mContext = mContext;
        this.ErrorRate = ErrorRate;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_point, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(mList.get(position).getAbilityName());
        holder.srcOver.setText(ErrorRate);
        holder.score.setText(mList.get(position).getAbilityAccuracy() + "%");
        return convertView;
    }

    static class ViewHolder {
        TextView name;
        TextView srcOver;
        TextView score;

        ViewHolder(View view) {
            name = view.findViewById(R.id.name);
            srcOver = view.findViewById(R.id.src_over);
            score = view.findViewById(R.id.score);
        }
    }
}
