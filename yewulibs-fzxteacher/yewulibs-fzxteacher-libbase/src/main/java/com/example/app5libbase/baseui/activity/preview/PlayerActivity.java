package com.example.app5libbase.baseui.activity.preview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.example.app5libbase.base.BaseActivity;
import com.sdzn.fzx.student.libutils.util.UiUtils;

public class PlayerActivity extends BaseActivity {

    FrameLayout flContent;

    private PlayerFragment playerFragment;
    private int screenHeight;
    private int screenWidth;
    private int FL_HEIGHT;
    public static final int FULL_SCREEN = 0;
    public static final int NORMAL_SCREEN = 1;
    private String videoUrl;
    private String radioUrl;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setReturnPop(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        flContent = findViewById(R.id.flContent);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入PlayerActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        initVideo(title);
    }

    @Override
    protected void initData() {
        FL_HEIGHT = UiUtils.dp2px(400);
        videoUrl = getIntent().getStringExtra("videoUrl");
        radioUrl = getIntent().getStringExtra("radioUrl");
        title = getIntent().getStringExtra("title");
    }

    private void initVideo(String title) {
        getScreenSize();
        setVideoLayoutParams();

        Bundle videoBundle = new Bundle();

        if (!TextUtils.isEmpty(videoUrl)) {
            playerFragment = new VideoViewFragment();
            videoBundle.putString("path", videoUrl);
        } else {
            playerFragment = new RadioViewFragment();
            videoBundle.putString("path", radioUrl);
        }

        videoBundle.putString("title", title);
        videoBundle.putBoolean("setScreenSize", true);
        videoBundle.putInt("width", screenWidth);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            videoBundle.putInt("height", screenHeight);
        } else {
            videoBundle.putInt("height", FL_HEIGHT);
        }
        playerFragment.setArguments(videoBundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, playerFragment).commit();
    }

    private void setVideoLayoutParams(int width, int height) {
        setFlContent(width, height);
        if (playerFragment != null) {
            playerFragment.setScreenSizeParams(width, height);
        }
    }

    private void setFlContent(int width, int height) {
        flContent.getLayoutParams().height = height;
        flContent.getLayoutParams().width = width;
    }

    private void setVideoLayoutParams() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setVideoLayoutParams(screenWidth, screenHeight);
        } else {
            setVideoLayoutParams(screenWidth, FL_HEIGHT);
        }
    }

    /**
     * 获取屏幕宽高
     */
    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        screenHeight = display.getHeight();
        screenWidth = display.getWidth();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getScreenSize();
        setVideoLayoutParams();
    }

    /**
     * 切换全屏并横屏
     *
     * @param screen
     */
    @SuppressLint("SourceLockedOrientationActivity")
    public void setScreen(int screen) {
        if (screen == FULL_SCREEN) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else if (screen == NORMAL_SCREEN) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            back();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back() {
        setResult(Activity.RESULT_OK);
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }
}
