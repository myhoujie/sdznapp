package com.example.app5libbase.baseui.view;

import com.example.app5libbase.base.BaseView;
import com.sdzn.fzx.teacher.vo.CorrectVo;
import com.sdzn.fzx.teacher.vo.LessonListVo;
import com.sdzn.fzx.teacher.vo.RankVo;
import com.sdzn.fzx.teacher.vo.SyncClassVo;
import com.sdzn.fzx.teacher.vo.TaskListVo;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public interface MainView extends BaseView {
    void getTaskListSuccess(TaskListVo taskListVo);

    void getTaskListFailure();

    void getLessonListSuccess(LessonListVo taskListVo);

    void getLessonListFailure();

    void getClassVoSuccess(SyncClassVo syncClassVo);

    void getRankSuccess(RankVo rankVo);

    void getCorrectSuccess(CorrectVo correctVo);

    void networkError(String msg);

    void onCompleted();
}
