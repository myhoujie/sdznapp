package com.example.app5libbase.baseui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.api.func.ApiException;
import com.sdzn.fzx.teacher.api.func.StatusFunc;
import com.sdzn.fzx.teacher.api.module.StatusVo;
import com.sdzn.fzx.teacher.api.network.NetWorkService;
import com.sdzn.fzx.teacher.api.network.Network;
import com.sdzn.fzx.teacher.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.teacher.api.subscriber.SubscriberListener;
import com.example.app5libbase.views.CustomGridView;
import com.example.app5libbase.baseui.adapter.TiWenGridAdapter;
import com.example.app5libbase.base.BaseActivity;
import com.example.app5libbase.controller.UserController;
import com.example.app5libpublic.event.TiWenEvent;
import com.sdzn.fzx.teacher.utils.SPUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.example.app5libbase.views.RoundAngleImageView;
import com.sdzn.fzx.teacher.vo.StudentListTiWen;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/6/25.
 */
public class TiWenActivity extends BaseActivity implements View.OnClickListener {
    private Animation animation;    //动画初始化

    private RelativeLayout head;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private TextView tvConfirm;
    private CustomGridView gridView;
    private RelativeLayout rl;
    private RoundAngleImageView ivHead;
    private ImageView ivDefault;
    private ImageView ivCheck;
    private TextView tvName;
    private ImageView ivQiangDa;
    private ImageView ivTiWen;
    private ImageView ivClose;


    TiWenGridAdapter gridAdapter;
    private int numStudent;
    private List<StudentListTiWen.DataBean> studentLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiwen);
        head = (RelativeLayout) findViewById(R.id.head);
        titleBackLy = (LinearLayout) findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) findViewById(R.id.title_back_txt);
        title = (TextView) findViewById(R.id.title);
        tvConfirm = (TextView) findViewById(R.id.tv_confirm);
        gridView = (CustomGridView) findViewById(R.id.gridView);
        rl = (RelativeLayout) findViewById(R.id.rl);
        ivHead = (RoundAngleImageView) findViewById(R.id.iv_head);
        ivDefault = (ImageView) findViewById(R.id.iv_default);
        ivCheck = (ImageView) findViewById(R.id.iv_check);
        tvName = (TextView) findViewById(R.id.tv_name);
        ivQiangDa = (ImageView) findViewById(R.id.iv_qiangda);
        ivTiWen = (ImageView) findViewById(R.id.iv_tiwen);
        ivClose = (ImageView) findViewById(R.id.iv_close);

        EventBus.getDefault().register(this);
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入TiWenActivity成功");
                }
            }
        }
    }

    @Override
    protected void initView() {
        ivClose.setOnClickListener(this);
        ivQiangDa.setOnClickListener(this);
        ivTiWen.setOnClickListener(this);
        //转动
//        animation = AnimationUtils.loadAnimation(this, R.anim.tip);
//        animation.setInterpolator(new LinearInterpolator());
//        ivHead.setAnimation(animation);

    }

    @Override
    protected void initData() {
        getStudent();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTiWenEvent(TiWenEvent event) {
        if (studentLists.size() > 0) {
            for (int stu = 0; stu < studentLists.size(); stu++) {
                if (event.getStudentId() == studentLists.get(stu).getId()) {
                    tvName.setText(studentLists.get(stu).getRealName());
                    ivDefault.setVisibility(View.GONE);
                    ivCheck.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_close) {
            finish();
            return;
        } else if (id == R.id.iv_qiangda) {// TODO: 2019/7/10 让学生抢答
            tvName.setText("");
            getResponder(1, "0");
        } else if (id == R.id.iv_tiwen) {// TODO: 2019/7/10 随机选一个学生点名
            tvName.setText("");
            if (numStudent > 0) {
                Random random = new Random();
                int num = random.nextInt(1000);
                tvName.setText(studentLists.get(num % numStudent).getRealName());
                ivDefault.setVisibility(View.GONE);
                ivCheck.setVisibility(View.VISIBLE);
                getResponder(2, String.valueOf(studentLists.get(num % numStudent).getId()));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (animation != null) animation.cancel();

        EventBus.getDefault().unregister(this);
    }

    private void setAdapter(StudentListTiWen list) {
        studentLists.clear();
        studentLists.addAll(list.getData());
        numStudent = list.getData().size();
        gridAdapter = new TiWenGridAdapter(this, list);
        gridView.setAdapter(gridAdapter);
    }

    /**
     * 发起抢答
     * 1  抢答
     * 2 随机
     */
    private void getResponder(final int type, String studentId) {
//        int questionId = (int) (Math.random() * (1000 - 1) + 4);
        UUID uuid = UUID.randomUUID();
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getClassResponder(SPUtils.get(TiWenActivity.this, SPUtils.CLASS_ID, "").toString(), String.valueOf(uuid), String.valueOf(type), studentId, String.valueOf(UserController.getUserId()))
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(Object o) {
                        switch (type) {
                            case 1:
                                ToastUtil.showLonglToast("已发起抢答");
                                break;
                            case 2:
                                ToastUtil.showLonglToast("已发起随机提问");
                                break;
                            default:
                                break;
                        }

                    }
                }, activity, true, false));
    }

    private void getStudent() {

        Network.createTokenService(NetWorkService.ToolsService.class)
//                .getClassStudent(SPUtils.get(TiWenActivity.this, SPUtils.CLASS_ID, "").toString())
                .getClassStudent(SPUtils.get(TiWenActivity.this, SPUtils.CLASS_ID, "").toString())
                .map(new StatusFunc<StudentListTiWen>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentListTiWen>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                ToastUtil.showLonglToast(status.getMsg());
                            } else {

                            }
                        } else {

                        }
                    }

                    @Override
                    public void onNext(StudentListTiWen o) {
                        if (o.getData().size() > 0) {
                            setAdapter(o);
                        }
                    }
                });
    }
}
