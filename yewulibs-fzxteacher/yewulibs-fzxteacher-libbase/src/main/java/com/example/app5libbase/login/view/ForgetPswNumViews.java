package com.example.app5libbase.login.view;


import com.example.app5libbase.base.BaseView;
import com.haier.cellarette.libmvp.mvp.IView;

/**
 */
public interface ForgetPswNumViews extends IView {

    void verifySuccess(String tel);

}
