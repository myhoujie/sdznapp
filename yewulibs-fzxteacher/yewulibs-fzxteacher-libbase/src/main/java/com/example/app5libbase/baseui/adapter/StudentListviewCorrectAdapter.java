package com.example.app5libbase.baseui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app5libbase.R;
import com.sdzn.fzx.teacher.vo.StudentListVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/1/30
 * 修改单号：
 * 修改内容:
 */
public class StudentListviewCorrectAdapter extends BaseAdapter {
    private List<StudentListVo.DataBean> mList;
    private Context mContext;

    public StudentListviewCorrectAdapter(List<StudentListVo.DataBean> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.student_correct_item, null);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mList.get(position).getIsCorrect() == 0) {
            holder.state.setVisibility(View.VISIBLE);
        } else {
            holder.state.setVisibility(View.GONE);
        }

        holder.examName.setText(mList.get(position).getUserStudentName());
        return convertView;
    }

    static class ViewHolder {
        TextView examName;
        ImageView state;

        ViewHolder(View view) {
            examName = view.findViewById(R.id.name);
            state = view.findViewById(R.id.state);
        }
    }
}
