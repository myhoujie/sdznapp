var learningGuide;//作答对象
var todayError;  //今日错题对象
var subjectError; //学科错题对象
var steamAll;
var clozeOptionList;  //完型填空选项集合

/**
 *  作答页面
 */


function getUptoken(params) {
    getAllParams(params);
    LearningGuideHandler.getStudentUptoken(params, this, function (flag, data) {
        if (flag) {
            console.log(data.result);
            localStorage.setItem("upToken", data.result.imageStyle);
        }
    });
}

//加载任务
function getMissionDetail(params) {
    console.log("参数" + JSON.stringify(params));
    getAllParams(params);
    showLoading();
    LearningGuideHandler.getLMissionDetail(params, this, function (flag, data) {
        hideLoading();
        if (flag) {

            if (data.result.data && data.result.data.length > 0) {
                learningGuide = new AnswerExam(data.result.data);
                // $('#submit').on('click', function () {
                //   submitAnswer();
                // });
            } else {
                $('.answer-exam').addClass('dn');
                $('.empty-view').removeClass('dn');
                html5SendSystemInfoToNative('未获取到任务数据，请稍后再试！');
            }
        } else {
            $('.answer-exam').addClass('dn');
            $('.empty-view').removeClass('dn');
            if (data && data.msg) {
                html5SendSystemInfoToNative(data.msg);
            }

        }
    });
}

//加载试题题干
function getReadExam(params) {
    // getAllParams(params);
    // LearningGuideHandler.readExam(params,this,function(code,result){
    //     if(code){
    //         var rExam = new ReadExam(result.result.data);
    //     }
    // })
    var rExam = new ReadExam(params);
}

/**
 *  作答结果页面  提交
 */

//提交答案条件
function submitStudentAnswer() {
    if (learningGuide.submitFun()) {
        //可提交
        // html5SendSystemInfoToNative('提交后将无法作答，确定提交吗？');
        interfaceName.submitToast('提交后将无法作答，确定提交吗？');
    } else {
        //存在未作答题目
        // html5SendSystemInfoToNative('存在未作答试题，提交后将无法作答，确定提交吗？');
        interfaceName.submitToast('存在未作答试题，提交后将无法作答，确定提交吗？');
    }
}

//确定提交
function submitAnswer() {
    var param = {};
    param.lessonTaskId = lessonTaskId;
    param.lessonTaskStudentId = lessonTaskStudentId;
    param.userStudentId = userStudentId;
    param.userStudentName = userStudentName;
    param.answerTime = answerTime;
    param.endTime = endTime;
    param.replyExamJSON = JSON.stringify(learningGuide.getValue());
    console.log(JSON.stringify(learningGuide.getValue()));
    showLoading();
    LearningGuideHandler.submitLessonGuideDetail(param, this, function (flag, data) {
        hideLoading();
        if (flag || data.code == 22000) {
            html5SendSystemInfoToNative('提交成功！');
            interfaceName.submitAnswerSuccess();
        } else {
            if (data && data.msg) {
                html5SendSystemInfoToNative(data.msg);
                if (data.code == 21710) {
                    interfaceName.submitAnswerOneMinDelete(data.msg);
                } else {
                    interfaceName.submitAnswerFailure(data.msg);
                }
            }
        }
    });
}

//正常退出保存
function submitAnswerOneMin() {
    if (learningGuide && learningGuide.getValue() && learningGuide.getValue().length > 0) {
        var param = {};
        param.lessonTaskId = lessonTaskId;
        param.lessonTaskStudentId = lessonTaskStudentId;
        param.userStudentId = userStudentId;
        param.userStudentName = userStudentName;
        param.answerTime = answerTime;
        param.endTime = endTime;
        param.replyExamJSON = JSON.stringify(learningGuide.getValue());
        showLoading();
        LearningGuideHandler.getStudentLessonGuideDetail(param, this, function (flag, data) {
            hideLoading();
            if (flag) {
                interfaceName.submitAnswerExitSuccess();
                learningGuide.resetTime();
            } else {
                if (data && data.msg) {
                    if (data.code == 21710) {
                        interfaceName.submitAnswerOneMinDelete(data.msg);
                    } else {
                        interfaceName.submitAnswerExitFailed(data.msg);
                    }
                } else {
                    interfaceName.submitAnswerExitFailed("网络错误");
                }
            }
        });
    } else {
        interfaceName.submitAnswerExitFailed("网络错误");
        return false;
    }
}

//每分钟自动提交
function autoSubmitAnswerOneMin() {
    if (learningGuide && learningGuide.getValue() && learningGuide.getValue().length > 0) {
        var param = {};
        param.lessonTaskId = lessonTaskId;
        param.lessonTaskStudentId = lessonTaskStudentId;
        param.userStudentId = userStudentId;
        param.userStudentName = userStudentName;
        param.answerTime = answerTime;
        param.endTime = endTime;
        param.replyExamJSON = JSON.stringify(learningGuide.getValue());
        LearningGuideHandler.getStudentLessonGuideDetail(param, this, function (flag, data) {
            if (flag) {
                interfaceName.submitAnswerOneMinSuccess();
            } else {
                if (data && data.msg) {
                    if (data.code == 21710) {
                        interfaceName.submitAnswerOneMinDelete(data.msg);
                    } else {
                        interfaceName.submitAnswerOneMinFailure(data.msg);
                    }
                } else {
                    interfaceName.submitAnswerOneMinFailure("网络错误");

                }

            }
        });
    } else {
        interfaceName.submitAnswerOneMinFailure("网络错误");
        return false;
    }
}

/**
 *  作答结果页面 相机功能
 */


//调用原生相机或者相册选择本地图片
function html5GetNativePic(id, numViewId, examId, curLen) {
    // nativeInsertPicToHtml5('',id, numViewId,examId);
    interfaceName.addPic(JSON.stringify(id), JSON.stringify(numViewId), JSON.stringify(examId), JSON.stringify(curLen));
}

//原生相机选取图片后插入图片到HTML5页面
// function nativeInsertPicToHtml5(thumbnailSrc, src, id, numViewId, examId) {
// //    src = 'http://192.168.0.215:8886/resource/image/2018/03/b0013218f6ad487c8195d69ef0c4455d.png';
// //    thumbnailSrc= 'http://192.168.0.215:8886/exam/2018/04/5e1d6c570f424184972b4fa835f4fc82.jpg';
//     if (src && thumbnailSrc) {
//         var obj = document.getElementById(id);
//         var curLen = $(obj).find('.imgUP').length;
//         if (curLen == 4) {
//             html5MorePic();
//         } else {
//             var img = $('<img class="imgUP" sid="' + src + '" src="' + thumbnailSrc + '">');
//             $(obj).append(img);
//             img.on("click", function () {
//
//                 html5SendPicToNative($(this).attr('sid'), 1, examId, $(this).index());
//             });
//             if (numViewId) {
//                 var numObj = document.getElementById(numViewId);
//                 $(numObj).addClass('all');
//                 learningGuide.updateShortFinish();
//             } else {
//                 updateComStatus();
//             }
//         }
//     }
// }

function nativeInsertPicToHtml5(src, id, numViewId, examId) {
  if (src) {
        var obj = document.getElementById(id);
        var curLen = $(obj).find('.imgUP').length;
        var curLenImg = curLen-1;
        if (curLen == 4) {
            html5MorePic();
        } else {
            var imageStyle = localStorage.getItem("upToken");
            var srcThn = src + "?" + imageStyle;
            var img = $('<img class="imgUP1" sid="' + src + '" src="' + srcThn +'">');
            var imgContain = $('<div class="imgUP" eid="imgUP'+curLenImg+'" sid="' + src + '" ></div>');
            imgContain.append(img);
            $(obj).append(imgContain);
            imgContain.on("click", function () {

                html5SendPicToNative($(this).attr('sid'), 1, examId, $(this).index());
            });
            //imgContain.css("background-image","url("+src+")");
            //imgContain.css("background-position","center");
            //imgContain.css("background-repeat","no-repeat");
            //imgContain.css("background-size","contain");
            if (numViewId) {
                var numObj = document.getElementById(numViewId);
                $(numObj).addClass('all');
                learningGuide.updateShortFinish();
            } else {
                updateComStatus();
            }
        }
    }
}


/**
 *  作答结果页面 手写板功能
 */
// http://192.168.0.100:8099/img/empty-pad.png
//调用原生手写板  appId(任务Id和试题id的拼接) index: 1 或13  AppNum（试题序号 4 或 4（1））
function html5GetNativeWrite(id, numViewId, appId, appNum, index, examId) {
    interfaceName.addWritePic(JSON.stringify(id), JSON.stringify(numViewId), JSON.stringify(appId), appNum, JSON.stringify(index), examId);
}

//原生手写板 ---- 返回功能
function html5GetNativeWriteEdit(src, id, numViewId, appId, appNum, index) {
    var obj = document.getElementById(id);
    // $(obj).empty().append($(ShortExam.writeIngTemplate));
    $(obj).empty();
    // learningGuide.navData.data.thumSrcArr = [];
    // learningGuide.navData.data.thumSrcArr.push(thumbnailSrc);
    learningGuide.navData.data.srcArr.push(src);
    learningGuide.navData.data.id = id;//编辑图片使用id
    learningGuide.navData.data.numViewId = numViewId;
    learningGuide.navData.data.appId = appId;
    learningGuide.navData.data.appNum = appNum;
    learningGuide.navData.data.index = index;
    learningGuide.updateNavFun();
    learningGuide.navCon.addClass('dn');
    learningGuide.SaveNavData(learningGuide);
}

//将原生手写板图片添加到页面  srcArr为图片地址数组             (页面中的保存按钮 也调该函数)
// function nativeInsertWriteToHtml5(thumbnailSrc, src, id, numViewId, appId, appNum, index) {
//     $('#' + learningGuide.navData.data.id).empty();
//     var obj = document.getElementById(id);
//     // var img = $('<img class="imgUP" src="' + src + '">');
//     var img = $('<img class="imgUP" sid="' + src + '" src="' + thumbnailSrc + '">');
//     img.on("click", function () {
//         for (var j = 0; j < learningGuide.examArr.length; j++) {
//             var aa = "w" + learningGuide.examArr[j].allData.id;
//             var mm = learningGuide.examArr[j].allData.examList;
//             // 综合题
//             if (learningGuide.examArr[j].allData.examTemplateId == 16) {
//                 var bigStem = JSON.parse(learningGuide.examArr[j].allData.examText).examStem;
//                 for (var k = 0; k < mm.length; k++) {
//                     var cc = "w" + mm[k].id;
//                     if (cc == id) {
//                         var smallStem = JSON.parse(mm[k].examText).examStem;
//                         steamAll = bigStem + smallStem;
//                     }
//                 }
//
//             }
//             //简答题
//             if (learningGuide.examArr[j].allData.examTemplateId == 4) {
//                 if (aa == id) {
//                     steamAll = JSON.parse(learningGuide.examArr[j].allData.examText).examStem;
//                 }
//             }
//         }
//         html5EditWrite(src, id, numViewId, appId, appNum, index, steamAll);
//     });
//     $(obj).append(img);
//     if (numViewId) {
//         var numObj = document.getElementById(numViewId);
//         $(numObj).addClass('all');
//         learningGuide.updateShortFinish();
//     } else {
//         updateComStatus();
//     }
//
// };

function nativeInsertWriteToHtml5(src, id, numViewId, appId, appNum, index) {
    $('#' + learningGuide.navData.data.id).empty();
    var obj = document.getElementById(id);
    var curLen = $(obj).find('.imgUP').length;
    var curLenImg = curLen-1;
    var imageStyle = localStorage.getItem("upToken");
    var srcThn = src + "?" + imageStyle;
    var img = $('<img class="imgUP1" sid="' + src + '" src="' + srcThn +'">');
    var imgContain = $('<div class="imgUP" eid="imgUP'+curLenImg+'" sid="' + src + '" ></div>');
    imgContain.append(img);
    imgContain.on("click", function () {
        for (var j = 0; j < learningGuide.examArr.length; j++) {
            var aa = "w" + learningGuide.examArr[j].allData.id;
            var mm = learningGuide.examArr[j].allData.examList;
            // 综合题
            if (learningGuide.examArr[j].allData.examTemplateId == 16) {
                var bigStem = JSON.parse(learningGuide.examArr[j].allData.examText).examStem;
                for (var k = 0; k < mm.length; k++) {
                    var cc = "w" + mm[k].id;
                    if (cc == id) {
                        var smallStem = JSON.parse(mm[k].examText).examStem;
                        steamAll = bigStem + smallStem;
                    }
                }

            }
            //简答题
            if (learningGuide.examArr[j].allData.examTemplateId == 4) {
                if (aa == id) {
                    steamAll = JSON.parse(learningGuide.examArr[j].allData.examText).examStem;
                }
            }
        }
        html5EditWrite(src, id, numViewId, appId, appNum, index, steamAll);
    });
    $(obj).append(imgContain);
    //imgContain.css("background-image","url("+src+")");
    //imgContain.css("background-position","center");
    //imgContain.css("background-repeat","no-repeat");
    //imgContain.css("background-size","contain");
    if (numViewId) {
        var numObj = document.getElementById(numViewId);
        $(numObj).addClass('all');
        learningGuide.updateShortFinish();
    } else {
        updateComStatus();
    }

};


//原生手写板 ---- 继续编辑手写板功能
function html5EditWrite(src, id, numViewId, appId, appNum, index, examId) {
    interfaceName.addCompileWrite(src, JSON.stringify(id), JSON.stringify(numViewId), JSON.stringify(appId), appNum, JSON.stringify(index), examId);
};

//原生手写板 ---- 保存前先清除之前的图片
function clearImg(id) {
    var objBox = document.getElementById(id);
    learningGuide.navData.data.srcArr = [];
    $(objBox).empty();
};

/**
 *  作答结果页面
 */
function getAnswerResult(params) {
    console.log("作答2" + JSON.stringify(params));
    getAllParams(params);
    new AnswerResult();
};


/**
 *  加载今日错题  今日收藏
 */
function getTodayError(params) {
    // console.log("今日错题 今日收藏" + JSON.stringify(params));
    getAllParams(params);
    todayError = new TodayError();
};


/**
 *  加载学科错题  学科收藏
 */

function getSubjectError(params) {
    // console.log("学科错题 学科收藏" + JSON.stringify(params));
    getAllParams(params);
    subjectError = new SubjectError();
};

/**
 *  删除 错题 收藏 提示信息
 */
function deleInfo(msg) {
    interfaceName.showMessage(msg);
}

/**
 *  共用
 */
//发送系统消息
function html5SendSystemInfoToNative(msg) {
    interfaceName.showMessage(msg);
};

//超过四张图片
function html5MorePic() {
    interfaceName.showMessage('最多上传四张图片');
};

//点击页面上的图片调用原生放大查看 type:1-存在删除按钮 0-不存在删除按钮 examId:试题id index:小标
function html5SendPicToNative(src, type, examId, index) {
    interfaceName.showImage(src, type, examId, index);
};

//预览资源 data:资源数据
function html5ScanResource(data) {
    interfaceName.showResource(JSON.stringify(data));
};

//取消预览资源 resId :资源id  time:时长
function html5CancelScanResource(resId, time) {
    learningGuide.updateResTime(resId, time);
};

//试题收藏成功
function addCollectSuccess() {
    interfaceName.showMessage("收藏成功");
};

//试题取消收藏成功
function deleteCollectSuccess() {
    interfaceName.showMessage("取消收藏成功");
};

//收藏失败、取消收藏失败
function collectError(msg) {
    interfaceName.showMessage(msg);
};

//简答题中的是否确定删除 examId 试题id  type: 1-相册  2-手写板
function delExamPop(examId, type, appid, appNum) {
    interfaceName.delExamPop(examId, type, "删除后需要重新作答,确定删除吗？", appid, appNum);
};

//简答题中的确定删除 点击确定后的回调
function delExamOk(examId, type) {
    if (type == 1) {
        for (var i = 0; i < learningGuide.examArr.length; i++) {
            if (learningGuide.examArr[i].allData.examTemplateId == 4 && learningGuide.examArr[i].allData.id == examId) {
                learningGuide.examArr[i].photoDel();
                return;
            } else if (learningGuide.examArr[i].allData.examTemplateId == 16) {
                for (var j = 0; j < learningGuide.examArr[i].smallExamArr.length; j++) {
                    if (learningGuide.examArr[i].smallExamArr[j].allData.examTemplateId == 4 && learningGuide.examArr[i].smallExamArr[j].allData.id == examId) {
                        learningGuide.examArr[i].smallExamArr[j].photoDel();
                        return;
                    } else {
                    }
                }
            } else {
            }
        }
        alert('未找到要删除图片的试题' + examId);
    } else if (type == 2) {
        for (var i = 0; i < learningGuide.examArr.length; i++) {
            if (learningGuide.examArr[i].allData.examTemplateId == 4 && learningGuide.examArr[i].allData.id == examId) {
                learningGuide.examArr[i].writeDel();
                return;
            } else if (learningGuide.examArr[i].allData.examTemplateId == 16) {
                for (var j = 0; j < learningGuide.examArr[i].smallExamArr.length; j++) {
                    if (learningGuide.examArr[i].smallExamArr[j].allData.examTemplateId == 4 && learningGuide.examArr[i].smallExamArr[j].allData.id == examId) {
                        learningGuide.examArr[i].smallExamArr[j].writeDel();
                        return;
                    } else {
                    }
                }
            } else {
            }
        }
        alert('未找到要删除图片的试题' + examId);
    } else {
        alert('删除类型不明' + type);
    }
};

//删除单张图片
function delOnePic(examId, index) {
    for (var i = 0; i < learningGuide.examArr.length; i++) {
        if (learningGuide.examArr[i].allData.examTemplateId == 4 && learningGuide.examArr[i].allData.id == examId) {
            $(learningGuide.examArr[i].photoView.find('.imgUP')[index - 1]).remove();
            if (learningGuide.examArr[i].photoView.find('.imgUP').length == 0) {
                learningGuide.examArr[i].photoDel();
            } else {
            }
            return;
        } else if (learningGuide.examArr[i].allData.examTemplateId == 16) {
            for (var j = 0; j < learningGuide.examArr[i].smallExamArr.length; j++) {
                if (learningGuide.examArr[i].smallExamArr[j].allData.examTemplateId == 4 && learningGuide.examArr[i].smallExamArr[j].allData.id == examId) {
                    $(learningGuide.examArr[i].smallExamArr[j].photoView.find('.imgUP')[index - 1]).remove();
                    if (learningGuide.examArr[i].smallExamArr[j].photoView.find('.imgUP').length == 0) {
                        learningGuide.examArr[i].smallExamArr[j].photoDel();
                    } else {
                    }
                    return;
                } else {
                }
            }
        } else {
        }
    }
};

// 删除错题 收藏
function delCollExam(model, studentBookId) {
    interfaceName.onDelExam(model, studentBookId);
}

var collExamId = 0;


function delCollExamOk(model, studentBookId) {
    collExamId = studentBookId;
    if (model == 1) {
        ErrorHandler.errorDelete({'studentBookId': studentBookId});
    } else {
        ErrorHandler.collectDelete({'studentBookId': studentBookId});
    }
}

// 今日错题 今日收藏 删除
function delCallBackFun(flag, data) {
    if (flag) {
        $('.today-error').find('[sid=t-test' + collExamId + ']').remove();
        //TODO 删除后试题的序号需要改变
        var examview = $('.today-error').find('[class=testNum]');
        if (examview.length != 0) {
            for (var j = 0; j < examview.length; j++) {
                $(examview[j]).text(j + 1 + ".");
            }

        } else {
            $('.today-error').html('<div class="empty-view"><div class="empty-text">暂无内容！</div></div>');
        }

    } else {
        alert("删除失败" + data.msg);
    }
}

function refreshTokenFun(token, refreshToken) {
    interfaceName.refreshTokenFun(token, refreshToken);
}


//调取移动端刷新token的方法
function mobileRefreshTokenFun(token, key) {
    interfaceName.mobileRefreshTokenFun(token, key);
}

//移动端刷新完成后的回调 flag：true:成功！ false：失败！
function refreshH5TokenFun(flag, token, key) {
    var mOptions = null;
    for (var i = 0; i < requestArr.length; i++) {
        if (requestArr[i].key == key) {
            mOptions = requestArr[i].value;
        }
    }
    if (flag) {
        access_token = token;
        mOptions.data.access_token = token;
        $.ajax(mOptions);
    } else {
        //alert('移动端刷新token失败!');
    }
}

//显示loading
function showLoading() {
    // alert("显示loading");
     interfaceName.showLoading();
}

//隐藏loading
function hideLoading() {
    // alert("取消loading");
     interfaceName.hideLoading();
}


// ---------------------------------------------------------------2018.11.15-----------------------------------------------------------
//完型填空作答，与移动端交互

//点击完形填空试题第一个空时，把所有选项封装成list集合，传给移动端
function clozeAnswer(examId, examSeq, optionList) {
    var strOptionList = JSON.stringify(optionList);
    interfaceName.clozeAnswer(examId, examSeq, strOptionList);
    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + (examSeq) + ']').removeClass("over_ing").addClass("answer_ing");
}

//移动端把对应空的答案传给我
function myClozeAnswer(examId, examSeq, optionList) {
    var str = JSON.stringify(optionList);
    for (var i = 0; i < learningGuide.examArr.length; i++) {
        if (learningGuide.examArr[i].allData.examTemplateId == 14 && learningGuide.examArr[i].allData.id == examId) {
            for (var m = 0; m < optionList.length; m++) {
                if (optionList[m].myContent) {
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').find('[sid=cContainer]').html(optionList[examSeq - 1].myContent);
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').attr('cid', optionList[examSeq - 1].myContent);
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').attr('fid', optionList[examSeq - 1].list[optionList[examSeq - 1].mySeq - 1].right);
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').attr('seqId', optionList[examSeq - 1].mySeq);
                    $('.answer-exam').find('[id=' + examId + ']').attr('oid', str);
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').attr('mid', examSeq);
                    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').removeClass("answer_ing over_ing").addClass("over_ing");
                    learningGuide.examArr[i].answerCallBack();
                }
            }
        }
    }
}


//移动端自动跳到下一题时调用此方法，告诉我是哪个空，上一个空preSeq，下一个空nextSeq
function jumpOption(examId, preSeq, nextSeq) {
    // console.log("移动端自动跳"+examId+"        "+preSeq+"          "+nextSeq);
    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + preSeq + ']').removeClass("answer_ing over_ing").addClass("over_ing");
    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + nextSeq + ']').removeClass("answer_ing over_ing").addClass("answer_ing");
}

// ---------------------------------------------------------------2018.11.23-----------------------------------------------------------

//关掉选项时
function closeOption(examId, examSeq) {
    // console.log("关掉选项"+examId+"        "+examSeq);
    $('.answer-exam').find('[id=' + examId + ']').find('[sid=' + examSeq + ']').removeClass("answer_ing over_ing").addClass("over_ing");

}