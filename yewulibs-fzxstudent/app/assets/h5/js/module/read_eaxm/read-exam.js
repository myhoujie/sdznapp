
var ReadExam = function (data) {
    this.view = $('.answer-exam');
    this.examCon = this.view.find('[sid=exam]');
    this.data = data;
    this.init();
};

ReadExam.prototype.init = function () {
    this.examCon.html(this.data.examStem);
};

ReadExam.prototype.checkStr = function (obj, str) {

};

ReadExam.prototype.updateShortFinish = function () {

};

ReadExam.prototype.answerCallBack = function () {
};

ReadExam.prototype.getValue = function () {
    if (!this.navCon.hasClass('dn')) {
        this.navSaveCon.trigger('click');
    }
    var arr = [];
    for (var i = 0; i < this.examArr.length; i++) {
        var t = this.examArr[i].getValue();
        if (JSON.parse(this.examArr[i].allData.examText).examTypeId == 16) {
            for (var j = 0; j < t.length; j++) {
                t[j].type = 1;
                arr.push(t[j])
            }
        } else {
            t.type = 1;
            arr.push(t)
        }
    }
    for (var k = 0; k < this.resArr.length; k++) {
        var t = this.resArr[k].getValue();
        if (t) {
            arr.push(t);
        } else {
        }
    }
    arr.push(this.getTextValue());
    if (arr && arr.length > 0) {
        return arr;
    }
    return false;
};

ReadExam.prototype.updateResTime = function (resId, time) {
    for (var j = 0; j < this.resArr.length; j++) {
        if (this.resArr[j].data.id == resId) {
            this.resArr[j].useTime = time;
            this.resArr[j].isRead = 1;
        }
    }
};

ReadExam.prototype.submitFun = function () {
    var flag = true;
    for (var i = 0; i < this.examArr.length; i++) {
        if (!this.examArr[i].finishFlag) {
            flag = false;
        }
    }
    return flag;
};

ReadExam.prototype.initExamType = function (tempData) {
    switch (JSON.parse(tempData.examText).examTypeId) {
        case 1:
        case 2:
        case 3:
            return new SelectExam(this, tempData);
            break;
        case 6:
            return new ComExam(this, tempData);
            break;
        case 4:
            return new ShortExam(this, tempData);
            break;
        case 16:
            return new Comprehensive(this, tempData);
            break;
        default :
            console.log('未找到匹配的选项类型，无法加载该选项');
    }
};

ReadExam.prototype.updateNavFun = function () {
    this.navTextCon.text(this.navData.name);
};

ReadExam.prototype.SaveNavData = function (e) {
    var self = e.data;
    $('#' + self.navData.data.id).empty();
    for (var i = 0; i < self.navData.data.srcArr.length; i++) {
        nativeInsertWriteToHtml5(self.navData.data.srcArr[i], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index);
    }
};

ReadExam.prototype.returnNavData = function (e) {
    var self = e.data;
    html5EditWrite(self.navData.data.srcArr[0], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index, self.navData.id);
};

ReadExam.prototype.updateNavData = function (id, numViewId, appId, appNum, index) {
    if (!this.navCon.hasClass('dn')) {
        this.navSaveCon.trigger('click');
        this.navCon.addClass('dn');
    }
    this.navData.name = '第' + appNum + '题';
    this.navData.data.srcArr = [];
    this.navData.data.id = id;
    this.navData.data.numViewId = numViewId;
    this.navData.data.appId = appId;
    this.navData.data.appNum = appNum;
    this.navData.data.index = index;
};

ReadExam.prototype.getTextValue = function () {
    var param = {};
    param.type = 3;
    param.lessonReadExamId = null;
    param.lessonTaskDetailsId = null;
    param.lessonReadExamParentId = null;
    param.templateId = null;
    param.emptyCount = null;
    param.answerList = null;
    return param;
};

ReadExam.prototype.resetTime = function () {
    for (var k = 0; k < this.resArr.length; k++) {
        this.resArr[k].useTime = 0;
    }
};



