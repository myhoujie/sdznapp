/**
 * Created by zhangxia on 2018/2/27.
 */
var AnswerExam = function (data) {
    this.view = $('.answer-exam');
    this.numView = this.view.find('[sid=ans-e-num]');  //顶部题目序号
    this.examCon = this.view.find('[sid=ans-e-con]');  //题目内容
    this.navCon = this.view.find('[sid=nav]');         //保存div
    this.navTextCon = this.navCon.find('[sid=exam-text]');
    this.navSaveCon = this.navCon.find('[sid=save]');
    this.navData = {
        'name': '',
        'data': {
            "srcArr": [],
            // "thumSrcArr":[],
            "id": '',
            "numViewId": '',
            "appId": '',
            "appNum": '',
            "index": ''
        }
    };
    this.data = data ? data : [];
    this.navTextCon.on('click', this, this.returnNavData);
    this.navSaveCon.on('click', this, this.SaveNavData);
    this.init();
};

AnswerExam.prototype.init = function () {
    this.examArr = [];
    this.resArr = [];
    this.examCon.empty();
    this.numView.find('[sid=num-box]').empty();
    var examCount = 1;
    for (var i = 0; i < this.data.length; i++) {
        switch (this.data[i].type) {
            case 1:
                var temp = this.initExamType(this.data[i]);
                temp.addListener(this, this.answerCallBack);
                this.examArr.push(temp);
                this.examCon.append(temp.view);
                examCount++;
                break;
            case 2:
                var tempRes = new ResItem(this, this.data[i]);
                this.examCon.append(tempRes.view);
                this.resArr.push(tempRes);
                break;
            case 3:
                this.checkStr(this.examCon, this.data[i].resourceText);
                break;
            default:
                console.log('不能判断数据类型');
        }
    }
    for (var j = 0; j < this.examArr.length; j++) {
        this.numView.find('[sid=num-box]').append(this.examArr[j].numView);
    }
    this.numView.slider({inEndEffect: "none", steps: 3, auto: false, duration: 300, delay: 5000});

};

AnswerExam.prototype.checkStr = function (obj, str) {
    if (str == '<p></p>' || str == '<p><br/></p>' || str == '<br/>' || str == '<p><br/></p><p><br/></p>') {

    } else {
        obj.append($('<div>' + str + '</div>'));
    }
};

AnswerExam.prototype.updateShortFinish = function () {

    for (var i = 0; i < this.examArr.length; i++) {
        if ((JSON.parse(this.examArr[i].allData.examText).examTypeId == 4) && this.examArr[i].numView.hasClass('all')) {
            this.examArr[i].finishFlag = true;
        }
    }
};

AnswerExam.prototype.answerCallBack = function () {
};

AnswerExam.prototype.getValue = function () {
    if (!this.navCon.hasClass('dn')) {
        this.navSaveCon.trigger('click');
    }
    var arr = [];
    for (var i = 0; i < this.examArr.length; i++) {
        var t = this.examArr[i].getValue();
        if (JSON.parse(this.examArr[i].allData.examText).examTypeId == 16) {
            for (var j = 0; j < t.length; j++) {
                t[j].type = 1;
                arr.push(t[j])
            }
        } else {
            t.type = 1;
            arr.push(t)
        }
    }
    for (var k = 0; k < this.resArr.length; k++) {
        var t = this.resArr[k].getValue();
        if (t) {
            arr.push(t);
        } else {
        }
    }
    arr.push(this.getTextValue());
    if (arr && arr.length > 0) {
        return arr;
    }
    return false;
};

AnswerExam.prototype.updateResTime = function (resId, time) {
    for (var j = 0; j < this.resArr.length; j++) {
        if (this.resArr[j].data.id == resId) {
            this.resArr[j].useTime += time;
            this.resArr[j].isRead = 1;
        }
    }
};

AnswerExam.prototype.submitFun = function () {
    var flag = true;
    for (var i = 0; i < this.examArr.length; i++) {
        if (!this.examArr[i].finishFlag) {
            flag = false;
        }
    }
    return flag;
};

AnswerExam.prototype.initExamType = function (tempData) {
    switch (JSON.parse(tempData.examText).examTypeId) {
        case 1:
        case 2:
        case 3:
            return new SelectExam(this, tempData);
            break;
        case 6:
            return new ComExam(this, tempData);
            break;
        case 4:
            return new ShortExam(this, tempData);
            break;
        case 16:
            return new Comprehensive(this, tempData);
            break;
        case 14://完形填空
            return new ClozeTest(this, tempData);
            break;
        default :
            console.log('未找到匹配的选项类型，无法加载该选项');
    }
};

AnswerExam.prototype.updateNavFun = function () {
    this.navTextCon.text(this.navData.name);
};

AnswerExam.prototype.SaveNavData = function (e) {
    var self = e;
    // var self = e.data;
    $('#' + self.navData.data.id).empty();
    for (var i = 0; i < self.navData.data.srcArr.length; i++) {
        nativeInsertWriteToHtml5(self.navData.data.srcArr[i], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index);
        // nativeInsertWriteToHtml5(self.navData.data.srcArr[i], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index);

    }

};

AnswerExam.prototype.returnNavData = function (e) {
    var self = e.data;
    // html5EditWrite(self.navData.data.thumSrcArr[0], self.navData.data.srcArr[0], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index, self.navData.id);
    html5EditWrite(self.navData.data.srcArr[0], self.navData.data.id, self.navData.data.numViewId, self.navData.data.appId, self.navData.data.appNum, self.navData.data.index, self.navData.id);

};

AnswerExam.prototype.updateNavData = function (id, numViewId, appId, appNum, index) {
    if (!this.navCon.hasClass('dn')) {
        this.navSaveCon.trigger('click');
        this.navCon.addClass('dn');
    }
    this.navData.name = '第' + appNum + '题';
    this.navData.data.srcArr = [];
    // this.navData.data.thumSrcArr = [];
    this.navData.data.id = id;
    this.navData.data.numViewId = numViewId;
    this.navData.data.appId = appId;
    this.navData.data.appNum = appNum;
    this.navData.data.index = index;
};

AnswerExam.prototype.getTextValue = function () {
    var param = {};
    param.type = 3;
    param.lessonAnswerExamId = null;
    param.lessonTaskDetailsId = null;
    param.lessonAnswerExamParentId = null;
    param.templateId = null;
    param.emptyCount = null;
    param.answerList = null;
    return param;
};

AnswerExam.prototype.resetTime = function () {
    for (var k = 0; k < this.resArr.length; k++) {
        this.resArr[k].useTime = 0;
    }
};



