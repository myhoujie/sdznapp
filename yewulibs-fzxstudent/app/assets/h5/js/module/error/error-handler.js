/**
 * Created by wjh on 2018/3/1.
 */

var TODAY_ERROR = 'teach/teachStudentBook/today/subject/list';            // 今日错题  今日收藏
var SUBJECT_ERROR = 'teach/teachStudentBook/subject/list';                //学科错题   学科收藏

var ERROR_DELETE = 'teach/student/exam/book/error/delete';          // 删除错题
var COLLECT_DELETE = 'teach/student/exam/book/collect/delete';      // 取消收藏



var ErrorHandler = function () {

};

ErrorHandler.getTodayError = function (data, listener, listenerFun) {

    doAjax({
        url: TODAY_ERROR,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("TODAY_ERROR");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};





ErrorHandler.getSubjectError = function (data, listener, listenerFun) {

    doAjax({
        url: SUBJECT_ERROR,
        data: data,
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log("SUBJECT_ERROR");
            console.log(data);
            if (data.code === 0) {
                listenerFun.call(listener, true, data);
            } else {
                listenerFun.call(listener, false, data);
            }
        },
        error: function (data) {
            listenerFun.call(listener, false, data);
        }
    });
};


ErrorHandler.errorDelete = function (data) {
    doAjax({
        url: ERROR_DELETE,
        data: data,
        type: "post",
        traditional: true,
        success: function (data) {
            console.log(data);
            if (data.code === 0) {
              delCallBackFun(true,data);
            } else {
              delCallBackFun(false,data);
            }
        },
        error: function (data) {
          delCallBackFun(false, data);
        }
    });
};


ErrorHandler.collectDelete = function (data) {
    doAjax({
        url: COLLECT_DELETE,
        data: data,
        type: "post",
        traditional: true,
      success: function (data) {
        if (data.code == 0) {
          delCallBackFun(true, data);
        } else {
          delCallBackFun(false, data);
        }
      },
      error: function (data) {
        delCallBackFun(false, data);
      }
    });
};