/**
 * Created by wjh on 2018/3/1.
 */

var SubjectError = function () {
  this.view = $('.subject');
  this.getData();
};


SubjectError.prototype.getData = function () {
  var param = {};
  param.userStudentId = userStudentId;
  param.model = model;
  param.baseSubjectId = baseSubjectId;
  param.examTemplateStyleId = examTemplateStyleId;
  param.timeType = timeType;
  param.count = count;
  param.chapterName = chapterName;
  ErrorHandler.getSubjectError(param, this, this.getDataCallBack);
};

SubjectError.prototype.getDataCallBack = function (flag, data) {
  if (flag) {
    this.data = data.result.data;
    this.initData();
  }
};

SubjectError.prototype.initData = function () {
  if (this.data.length != 0) {
    this.view.empty();
    for (var j = 0; j < this.data.length; j++) {

      this.testInsAllView = $(SubjectError.testInsAllTempalte).clone();
      this.testInsView = $(SubjectError.testInsTempalte).clone();
      this.testInsAllView.append(this.testInsView);
      this.view.append(this.testInsAllView);

      // 序号
      this.testInsView.find('[sid=testNum]').attr('class', 'testNum');
      this.testInsView.find('[sid=testNum]').attr('sid', 'testNum' + this.data[j].id);
      this.testInsView.find('[sid=testNum' + this.data[j].id + ']').text(j + 1 + ".");

      // 试题类型
      this.testInsView.find('[sid=testType]').attr('sid', 'testType' + this.data[j].id);
      this.testInsView.find('[sid=testType' + this.data[j].id + ']').html(this.data[j].examTemplateStyleName + "&nbsp;&nbsp;&nbsp;&nbsp;");

      // 错误次数
      this.testInsView.find('[sid=errorcount]').attr('sid', 'errorcount' + this.data[j].id);
      this.testInsView.find('[sid=errorcount' + this.data[j].id + ']').html(this.data[j].count + "次&nbsp;&nbsp;&nbsp;&nbsp;");

      // 难易程度
      this.testInsView.find('[sid=testDifficulty]').attr('sid', 'testDifficulty' + this.data[j].id);
      if (this.data[j].examDifficulty === 1) {
        this.testInsView.find('[sid=testDifficulty' + this.data[j].id + ']').html("简单" + "&nbsp;&nbsp;&nbsp;&nbsp;");
      } else if (this.data[j].examDifficulty === 2) {
        this.testInsView.find('[sid=testDifficulty' + this.data[j].id + ']').html("中等" + "&nbsp;&nbsp;&nbsp;&nbsp;");
      } else if (this.data[j].examDifficulty === 3) {
        this.testInsView.find('[sid=testDifficulty' + this.data[j].id + ']').html("较难" + "&nbsp;&nbsp;&nbsp;&nbsp;");
      } else {
        this.testInsView.find('[sid=testDifficulty' + this.data[j].id + ']').html("难" + "&nbsp;&nbsp;&nbsp;&nbsp;");
      }

      // 章节
      this.testInsView.find('[sid=chapter]').attr('sid', 'chapter' + this.data[j].id);
      if(this.data[j].chapterNodeNamePath != null){
        this.testInsView.find('[sid=chapter' + this.data[j].id + ']').html(this.data[j].chapterNodeNamePath + "&nbsp;&nbsp;&nbsp;&nbsp;");
      }


      // 修改时间
      var updateTime = formatDate(this.data[j].timeUpdate, 'yyyy-MM-dd');
      var weekDay = this.getWeeks(updateTime);
      this.testInsView.find('[sid=testUpdate]').attr('sid', 'testUpdate' + this.data[j].id);
      this.testInsView.find('[sid=testUpdate' + this.data[j].id + ']').html(updateTime + "&nbsp;&nbsp;" + weekDay + "&nbsp;&nbsp;&nbsp;&nbsp;");
      // 删除错题
      this.testInsView.find('[sid=test-delete]').attr('sid', 'test-delete' + this.data[j].id);
      this.testInsView.find('[sid=test-delete]').attr('id', this.data[j].id);
      this.testInsView.find('[sid=test-delete' + this.data[j].id + ']').on("click", {
        'obj': this,
        'id': this.data[j].id
      }, this.deleteClick);

      //加载题型
      var temp = new ExamPreview(JSON.parse(this.data[j].examText), 1);
      this.testInsAllView.append(temp.view);

      // 答案显示隐藏
      this.view.find('[sid=t-test]').attr('sid', 't-test' + this.data[j].id);
      this.testInsView.find('[sid=rough-took]').attr('sid', 'rough-took' + this.data[j].id);
      this.showAnswerView = this.testInsView.find('[sid=rough-took' + this.data[j].id + ']');
      this.answerSelectObj = new SwitchSelect(this.showAnswerView);
      this.answerSelectObj.addListener(this, this.changeAnswerStatus);

      this.testInsAllView.find('.exam-scan-h').addClass('dn');

      // 展开 收起
      this.imgSwitchView = $(SubjectError.imgSwitchTempalte).clone();
      this.testInsAllView.append(this.imgSwitchView);
      if (this.testInsAllView.height() > 201) {
        this.imgSwitchView.append('<img src="../img/error/zhankia_icon@2x.png" style="width: 42px;height: 16px">');
        var aa = this.testInsAllView.find('.exam-scan').html();
        this.aa = aa;
        this.testInsAllView.find('[sid=imgSwith]').attr('sid', 'imgSwith' + this.data[j].id);
        this.contentSelectView = this.testInsAllView.find('[sid=imgSwith' + this.data[j].id + ']');
        this.contentSelectObj = new SwitchContent(this, this.data[j].id, this.aa, this.contentSelectView);
      }


    }
  }else{
    this.view.empty().append('<div class="empty-view"><div class="empty-text">暂无内容！</div></div>');
  }

};


SubjectError.prototype.changeAnswerStatus = function (status, obj) {
  if (status == 1) {
    $(obj).parent().parent().parent().parent().find('.exam-scan-h').removeClass('dn');
  } else {
    $(obj).parent().parent().parent().parent().find('.exam-scan-h').addClass('dn');
  }
};


SubjectError.prototype.getWeeks = function (days) {
  var m = new Date(days);
  var weeks;
  if (m.getDay() === 0) {
    weeks = "星期天";
  }
  if (m.getDay() === 1) {
    weeks = "星期一";
  }
  if (m.getDay() === 2) {
    weeks = "星期二";
  }
  if (m.getDay() === 3) {
    weeks = "星期三";
  }
  if (m.getDay() === 4) {
    weeks = "星期四";
  }
  if (m.getDay() === 5) {
    weeks = "星期五";
  }
  if (m.getDay() === 6) {
    weeks = "星期六";
  }
  return weeks;
};

// SubjectError.subjectAllTempalte = '<div class="subjectAll" sid="t-subjectAll">' +
//     '</div>';
//
// // 学科名 模板
// SubjectError.subjectNameTempalte = '<div class="t-subject clearfix" sid="t-subject">' +
//     '<div class="l">' +
//     '<i class="t-subject-icon" sid="t-subject-icon"></i>' +
//     '<span class="font-black" sid="t-subject-name"></span>' +
//     '</div>' +
//     '<div class="r">' +
//     '<span class="font-grey">共</span>' +
//     '<span class="font-black2" sid="t-subject-totalNum">14</span>' +
//     '<span  class="font-grey">道</span>' +
//     '</div>' +
//     '</div>';

//题目介绍
SubjectError.testInsAllTempalte = '<div class="t-test clearfix" sid="t-test">' +
  '</div>';

SubjectError.testInsTempalte = '<div class="test-ins font-grey bac-grey clearfix" sid="test-ins">' +
  '<div class="l" style="line-height: 38px">' +
  '<span class="" sid="testNum"></span>' +
  '<span class="" sid="testType"></span>' +
  '<span class="font-red" sid="errorcount"></span>' +
  '<span class="" sid="testDifficulty"></span>' +
  '<span class="" sid="chapter"></span>' +
  '</div>' +
  '<div class="r">' +

  '<div class="test-delete" sid="test-delete" style="float: right;margin-top: 4px"></div>' +

  '<div class="ans-r-info-btn pi-jp-item">' +
  '<div class="pi-jp-switch" sid="rough-took">' +
  '<span class="switch-y">显</span> ' +
  '<span class="switch-n">隐</span>' +
  '<i class="switch-icon"></i>' +
  '</div>' +
  '<span class="pi-jp-label">答案</span>' +
  '</div>' +

  '<div class="" sid="testUpdate" style="float: right;line-height: 38px"></div>' +
  '</div>' +
  '</div>';

// 展开 收起
SubjectError.imgSwitchTempalte = '<div class="imgSwith" sid="imgSwith" style="text-align: center;border-top: 1px solid #EDEDED;margin-bottom: 10px;">' +

  '</div>';

/**
 * 删除今日错题事件
 * @param e
 */


// SubjectError.prototype.deleteClick = function (e) {
//     var self = e.data.obj;
//     var studentBookId = e.data.id;
//     if (model === 1) {
//         ErrorHandler.errorDelete({'studentBookId': studentBookId}, self, self.deleteCallBack);
//     } else {
//         ErrorHandler.collectDelete({'studentBookId': studentBookId}, self, self.deleteCallBack);
//     }
//
// };

SubjectError.prototype.deleteClick = function (e) {

  var self = e.data.obj;
  var studentBookId = e.data.id;
  delCollExam(model, studentBookId);
};


// SubjectError.prototype.deleteCallBack = function (flag, data) {
//     if (flag) {
//         this.getData();
//         console.log("删除成功");
//         deleInfo(data.msg);
//     } else {
//         console.log("删除失败");
//         deleInfo(data.msg);
//     }
// };






