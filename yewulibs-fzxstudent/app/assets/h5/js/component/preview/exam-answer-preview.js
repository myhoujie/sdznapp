/**
 * Created by zhangxia on 2018/2/11.
 */

var ExamAnswerPreview = function (data, index) {
    this.data = data;
    this.index = index;
    this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    this.serialNum3 = ["正确", "错误"];
    this.imageStyle = localStorage.getItem("upToken");
    this.initType();
};

ExamAnswerPreview.prototype.initType = function () {
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    var examText = JSON.parse(this.data.examText);
    switch (examText.examTypeId) {
        case 1:
            this.scanSingleExam(this.view, this.data, this.index);
            break;
        case 2:
            this.scanSingleExam(this.view, this.data, this.index);
            break;
        case 6:
            this.scanComExam(this.view, this.data, this.index);
            break;
        case 3:
            this.scanJudgeExam(this.view, this.data, this.index);
            break;
        case 4:
            this.scanShortExam(this.view, this.data, this.index);
            break;
        case 16:
            this.scanAllExam(this.view, this.data, this.index);
            break;
        case 14:
            this.scanClozeExam(this.view, this.data, this.index);
            break;
        default:
            console.log('未找到对应题型：' + this.data.examTypeId);
    }
    this.view.find('.exam-scan-h').addClass('dn');
};

ExamAnswerPreview.prototype.initData = function (indexFlag, data, type, score, scores, value) {
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
    viewArr.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    viewArr.optionConResultView = $(ExamPreViewTem.optionConResultTemplate).clone();
    viewArr.answerView = $(ExamPreViewTem.amswerTemplate).clone();
    viewArr.analyzeView = $(ExamPreViewTem.analyzeTemplate).clone();
    viewArr.shortAnswerView = $(ExamPreViewTem.amswerTemplate1).clone();

    if (indexFlag) {
        viewArr.introView.find('[sid=index]').text(indexFlag);
    }
    if (type) {
        viewArr.introView.find('[sid=type]').text(this.stage(data.examTypeId));
    } else {
        viewArr.introView.find('[sid=type]').text(data.templateStyleName);
    }

    if (score >= 0) {
        viewArr.introView.find('[cid=exam_score]').show();
        viewArr.introView.find('[cid=exam_score]').html("(本题" + score + "分)");
        if (value.isCollect) {
            viewArr.shortAnswerView.find('[cid=xscore]').show();
            viewArr.shortAnswerView.find('[cid=xscore]').html(scores + '分');
        }
    } else {
        viewArr.shortAnswerView.find('[cid=xscore]').hide();
        viewArr.introView.find('[cid=exam_score]').hide();

    }
    this.radio = viewArr.shortAnswerView.find("[cid=radio]");
    this.sSelect = new SwitchSelect(this.radio, 1);
    this.sSelect.setStatus(1);
    this.sSelect.addListener(this, this.setCorrect);
    if (data.examTypeId == 6) {
        var tempStem = $('<div>' + data.examStem + '</div>');
        console.log(data.examStem);
        for (var i = 0; i < data.examOptions.length; i++) {
            $(tempStem.find('.cus-com')[i]).val('');
            if (data.examOptions[i].isRight) {
                $(tempStem.find('.cus-com')[i]).addClass('false-answer');
            } else {
                $(tempStem.find('.cus-com')[i]).addClass('true-answer');
            }
        }
        viewArr.stemView.append(tempStem);
    } else {
        viewArr.stemView.html(data.examStem);
    }
    viewArr.analyzeView.find('[sid=exam-scan-analyse]').html(data.examAnalysis ? data.examAnalysis : '略');
    return viewArr;
};

//预览单选,多选
ExamAnswerPreview.prototype.scanSingleExam = function (parentView, data, indexFlag, type) {
    var examInfo = JSON.parse(data.examText);
    var viewArr = this.initData(indexFlag, examInfo, type, data.score, data.scores, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConView);
    for (var i = 0; i < examInfo.examOptions.length; i++) {
        var option = $(ExamPreViewTem.optionTemplate).clone();
        option.find('[sid=index]').text(this.serialNum1[i]);
        option.find('[sid=stem]').html(examInfo.examOptions[i].content);
        viewArr.optionConView.append(option);
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
    this.setAnswerView(parentView, data);
};

//预览判断
ExamAnswerPreview.prototype.scanJudgeExam = function (parentView, data, indexFlag, type) {
    var examInfo = JSON.parse(data.examText);
    var viewArr = this.initData(indexFlag, examInfo, type, data.score, data.scores, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append($(ExamPreViewTem.optionJudgeConTem).clone());
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
    this.setAnswerView(parentView, data);
};

//预览填空
ExamAnswerPreview.prototype.scanComExam = function (parentView, data, indexFlag, type) {


    var allScore = data.score * data.examEmptyCount;
    var examInfo = JSON.parse(data.examText);
    var viewArr = this.initData(indexFlag, examInfo, type, allScore, data.scores, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
    this.setComAnswerView(parentView, data);
};

//预览简答
ExamAnswerPreview.prototype.scanShortExam = function (parentView, data, indexFlag, type) {
    var examInfo = JSON.parse(data.examText);
    var viewArr = this.initData(indexFlag, examInfo, type, data.score, data.scores, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    viewArr.answerView.find('[sid=exam-scan-answer]').html(trimStr(examInfo.examAnswer) ? trimStr(examInfo.examAnswer) : '略');
    parentView.append(viewArr.shortAnswerView);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
    this.setShortAnswerView(parentView, data);
};

//预览综合
ExamAnswerPreview.prototype.scanAllExam = function (parentView, data, indexFlag) {
    var viewArr = this.initShortData(indexFlag, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.smallExamView);
    for (var i = 0; i < data.examList.length; i++) {
        var tempView = $(ExamPreViewTem.boxTemplate).clone();
        switch (data.examList[i].examTemplateId) {
            case 1:
                this.scanSingleExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 2:
                this.scanSingleExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 6:
                this.scanComExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 3:
                this.scanJudgeExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            case 4:
                this.scanShortExam(tempView, data.examList[i], '(' + parseInt(i + 1) + ')', 1);
                break;
            default:
                console.log('未找到对应的小题型：' + data.examTemplateId);
        }
        viewArr.smallExamView.append(tempView);
    }
};

//预览完型填空
ExamAnswerPreview.prototype.scanClozeExam = function (parentView, data, indexFlag, type) {
    var allScore = data.score * data.examEmptyCount;
    var examInfo = JSON.parse(data.examText);
    var viewArr = this.initData(indexFlag, examInfo, type, allScore, data.scores, data);
    parentView.append(viewArr.introView);
    parentView.append(viewArr.stemView);
    parentView.append(viewArr.optionConResultView);
    for (var i = 0; i < examInfo.examOptions.length; i++) {
        var smallOption = examInfo.examOptions[i].list;
        var clozeOption = $(ExamPreViewTem.clozeAnswerResultTemplate).clone();
        clozeOption.find('[sid=flag]').text("（" + (i + 1) + "）");
        for (var j = 0; j < smallOption.length; j++) {
            var option = $(ExamPreViewTem.clozeOptionItemResultTemplate).clone();
            option.find(".scan-cloze-content").html(smallOption[j].content);
            if (smallOption[j].right == true) {
                option.find('[sid=index]').text(this.serialNum1[j]);
                option.find('[sid=index]').addClass("true-answer");
            } else {
                option.find('[sid=index]').text(this.serialNum1[j] + ".");
            }
            clozeOption.find(".scan-cloze-option").append(option)
        }
        viewArr.optionConResultView.append(clozeOption);
    }
    viewArr.answerView.find('[sid=exam-scan-answer]').append(viewArr.optionConResultView);
    parentView.append(viewArr.answerView);
    parentView.append(viewArr.analyzeView);
    this.setClozeAnswerView(parentView, data);
};


//显示答案和解析
ExamAnswerPreview.prototype.showAnswer = function () {
    this.view.find('.exam-scan-h').removeClass('dn');
};

ExamAnswerPreview.prototype.hideAnswer = function () {
    this.view.find('.exam-scan-h').addClass('dn');
};

ExamAnswerPreview.prototype.stage = function (num) {
    switch (num) {
        case 1:
            return '单选题';
            break;
        case 2:
            return '多选题';
            break;
        case 3:
            return '判断题';
            break;
        case 4:
            return '简答题';
            break;
        case 6:
            return '填空题';
            break;
        default:
            return '未知';
            break;
    }
};

ExamAnswerPreview.prototype.initShortData = function (indexFlag, data) {
    var shortExamInfo = JSON.parse(data.examText);
    var viewArr = {};
    viewArr.view = $(ExamPreViewTem.boxTemplate).clone();
    viewArr.introView = $(ExamPreViewTem.introTemplate).clone();
    viewArr.stemView = $(ExamPreViewTem.stemTemplate).clone();
    viewArr.smallExamView = $(ExamPreViewTem.optionAllConTemp).clone();
    if (indexFlag) {
        viewArr.introView.find('[sid=index]').text(indexFlag);
    }

    if (data.fullScore && data.fullScore > 0) {
        viewArr.introView.find('[cid=exam_score]').show();
        viewArr.introView.find('[cid=score_text]').text(data.fullScore);
    } else {
        viewArr.introView.find('[cid=exam_score]').hide();
    }
    viewArr.introView.find('[sid=type]').text(shortExamInfo.templateStyleName);
    viewArr.stemView.html(shortExamInfo.examStem);
    return viewArr;
};
//批改痕迹控制
ExamAnswerPreview.prototype.setCorrect = function (data) {
    //0:关闭  1：开启
    if (data === 0) {
        this.view.find("[sid=exam-scan-answer-h]").show();
        this.view.find("[sid=exam-scan-answer-s]").hide();
    } else {
        this.view.find("[sid=exam-scan-answer-s]").show();
        this.view.find("[sid=exam-scan-answer-h]").hide();

    }
}
//简答题的答案
// ExamAnswerPreview.prototype.setShortAnswerView = function (parentView, data) {
//     var status = this.getRightStatus(data.isRight);
//     if (data.isAnswer) {
//         // if (data.examOptionList[0].seq == 2) {
//             var srcArr1 = data.examOptionList[0].myAnswer.split(';')[0].split(',');  //大图
//             var srcArr = data.examOptionList[0].myAnswer.split(';')[1].split(',');   //缩略图
//             for (var i = 0; i < srcArr.length; i++) {
//                 var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
//                 var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');
//                 imgContain.on("click", function () {
//                     html5SendPicToNative($(this).attr('sid'), 0, 0, 0);
//                 });
//                 imgContain.css("background-image","url("+srcArr[i]+")");
//                 imgContain.css("background-position","center");
//                 imgContain.css("background-repeat","no-repeat");
//                 imgContain.css("background-size","contain");
//                 imgContain.append(img);
//                 parentView.find('[sid=exam-scan-answer-s]').append(imgContain);
//              }
//             srcArr1 = data.examOptionList[0].myOldAnswer.split(';')[0].split(',');
//             srcArr = data.examOptionList[0].myOldAnswer.split(';')[1].split(',');
//             for (var i = 0; i < srcArr1.length; i++) {
//
//                 var img = $('<img class="imgUP1" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '" style="display: none">');
//                 var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr1[i] + '" src="' + srcArr[i] + '"></div>');
//                 imgContain.on("click", function () {
//                     html5SendPicToNative($(this).attr('sid'), 0, 0, 0);
//                 });
//                 imgContain.css("background-image","url("+srcArr[i]+")");
//                 imgContain.css("background-position","center");
//                 imgContain.css("background-repeat","no-repeat");
//                 imgContain.css("background-size","contain");
//                 imgContain.append(img);
//                 parentView.find('[sid=exam-scan-answer-h]').append(imgContain);
//             }
//         // }
//         // else {
//         //     var srcArr = data.examOptionList[0].myOldAnswer.split(';')[0].split(',');
//         //     for (var i = 0; i < srcArr.length; i++) {
//         //         var img = $('<img class="imgUP" src="' + srcArr[i] + '">');
//         //         img.on("click", function () {
//         //             html5SendPicToNative($(this).attr('src'), 0, 0, 0);
//         //         });
//         //         parentView.find('[sid=exam-scan-answer-s]').append(img);
//         //         parentView.find('[sid=exam-scan-answer-h]').append(img.clone(true));
//         //
//         //     }
//         // }
//     } else {
//         parentView.find('[sid=exam-scan-answer-s]').append($('<div class="short-no-answer">您未提交该题答案！</div>'));
//         // status = "false-answer";
//     }
//     parentView.find('[sid=exam-scan-answer-s]').find('[sid=flag]').addClass(status);
// };
ExamAnswerPreview.prototype.setShortAnswerView = function (parentView, data) {
    var status = this.getRightStatus(data.isRight);
    if (data.isAnswer) {
        var srcArr = data.examOptionList[0].myAnswer.split(',');
        for (var i = 0; i < srcArr.length; i++) {
            var srcThn = srcArr[i] + "?" + this.imageStyle;
            var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn +'">');
            var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
            img.on("click", function () {
                html5SendPicToNative($(this).attr('sid'), 0, 0, 0);
            });
            imgContain.append(img);
            parentView.find('[sid=exam-scan-answer-s]').append(imgContain);
        }

        srcArr = data.examOptionList[0].myOldAnswer.split(',');
        for (var i = 0; i < srcArr.length; i++) {
            var srcThn = srcArr[i] + "?" + this.imageStyle;
            var img = $('<img class="imgUP1" sid="' + srcArr[i] + '" src="' + srcThn + '">');
            var imgContain = $('<div class="imgUP" eid="imgUP'+i+'" sid="' + srcArr[i] + '" ></div>');
            img.on("click", function () {
                html5SendPicToNative($(this).attr('sid'), 0, 0, 0);
            });
            imgContain.append(img);
            parentView.find('[sid=exam-scan-answer-h]').append(imgContain);
        }

    } else {
        parentView.find('[sid=exam-scan-answer-s]').append($('<div class="short-no-answer">您未提交该题答案！</div>'));
    }
    parentView.find('[sid=exam-scan-answer-s]').find('[sid=flag]').addClass(status);
};

//填空题的答案
ExamAnswerPreview.prototype.setComAnswerView = function (parentView, data) {
    parentView.find("input.cus-com").replaceWith($('<span class="black_filling"></span>'));
    if (data.examOptionList && data.examOptionList.length > 0) {
        for (var i = 0; i < data.examOptionList.length; i++) {
            var status = this.getComRightStatus(data.examOptionList[i].isRight);
            $(parentView.find('[sid=exam-scan-stem]').find('.black_filling')[data.examOptionList[i].seq - 1]).removeClass('true-answer false-answer').addClass(status).text(data.examOptionList[i].myAnswer);
        }
    }
};

//完型填空的答案
ExamAnswerPreview.prototype.setClozeAnswerView = function (parentView, data) {
    parentView.find("input.cus-com").replaceWith($('<span class="black_filling"><span sid="clozeIndex"></span><span sid="clozeDetail"></span></span>'));
    if (data.examEmptyCount) {
        for (var i = 0; i < data.examEmptyCount; i++) {
            $(parentView.find('[sid=exam-scan-stem]').find('[sid=clozeIndex]')[i]).text(i + 1);
        }
    }
    if (data.examOptionList && data.examOptionList.length > 0) {
        for (var i = 0; i < data.examOptionList.length; i++) {
            var status = this.getComRightStatus(data.examOptionList[i].isRight);
            $(parentView.find('[sid=exam-scan-stem]').find('.black_filling')[data.examOptionList[i].seq - 1]).find('[sid=clozeDetail]').removeClass('true-answer false-answer').addClass(status).html("&nbsp;" + data.examOptionList[i].myAnswer);
        }
    }
};


//选择题判断题的答案
ExamAnswerPreview.prototype.setAnswerView = function (parentView, data) {
    var status = this.getRightStatus(data.isRight);
    if (data.examOptionList && data.examOptionList.length > 0) {
        for (var i = 0; i < data.examOptionList.length; i++) {
            $(parentView.find('[sid=exam-scan-option]').find('[sid=index]')[data.examOptionList[i].seq - 1]).addClass(status);

        }
    }
};

//正确 错误 半对 状态
ExamAnswerPreview.prototype.getRightStatus = function (flag) {
    var classArr = ["true-answer", "false-answer", "half-true"];
    var className = '';
    switch (flag) {
        case 1:
            className = classArr[0];
            break;
        case 2:
            className = classArr[1];
            break;
        case 3:
            className = classArr[2];
            break;
        default:
            className = '';
    }
    return className;
};

//正确 错误  状态 填空
ExamAnswerPreview.prototype.getComRightStatus = function (flag) {
    var classArr = ["true-answer", "false-answer", "half-true"];
    var className = '';
    switch (flag) {
        case 1:
            className = classArr[0];
            break;
        case 0:
            className = classArr[1];
            break;
        default:
            className = '';
    }
    console.log(className);
    return className;
};
