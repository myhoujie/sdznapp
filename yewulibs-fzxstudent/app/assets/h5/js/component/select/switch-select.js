var SwitchSelect = function (view, type) {

    this.view = view;
    this.status = 0; //0:关闭  1：开启
    this.type = type ? type : 0;
    this.init();
};


SwitchSelect.prototype.init = function () {
    this.view.off("click").on('click',this, this.handleClick);
};

SwitchSelect.prototype.handleClick = function (evt) {
    var self  = evt.data;
    if (self.status === 1) {
        self.setStatus(0);
    } else if (self.status === 0) {
        self.setStatus(1);
    }
    if (self.listener) {
        self.listenerFun.call(self.listener, self.status);
    }
};

SwitchSelect.prototype.setStatus = function (status) {
    this.status = parseInt(status);
    if (parseInt(status) === 1) {
        this.view.addClass('active')
    } else if (parseInt(status) === 0) {
        this.view.removeClass('active')
    }
};

SwitchSelect.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};