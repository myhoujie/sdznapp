/**
 * Created by wjh on 2018/3/7.
 */
var SwitchContent = function (allView,testID,examView,view) {
    this.allView = allView;
    this.testID = testID;
    this.examView = examView;
    this.view = view;
    this.status = 1; //0:关闭  1：开启
    this.init();
};


SwitchContent.prototype.init = function () {

    this.view.on('click', this,this.handleClick);
};

SwitchContent.prototype.handleClick = function (e) {
    var self = e.data;
    if (self.status === 1) {
        self.setStatus(0);
    } else if (self.status === 0) {
        self.setStatus(1);
    }
    if (self.listener) {
        self.listenerFun.call(self.listener, self.status ,this);
    }
};

SwitchContent.prototype.setStatus = function (status) {
    this.status = parseInt(status);
    if (parseInt(status) === 1) {
        this.allView.view.find('[sid=t-test' + this.testID + ']').children("div.exam-scan").html(this.examView);
        this.view.empty().append('<img src="../img/error/zhankia_icon@2x.png" style="width: 42px;height: 16px">');
    } else if (parseInt(status) === 0) {
        this.allView.view.find('[sid=t-test' + this.testID + ']').children("div.exam-scan").html(this.examView.substring(0, 790) + "...");
        this.view.empty().append('<img src="../img/error/shouqi_icon@2x.png" style="width: 42px;height: 16px">');
    }

    var hasTrue = this.allView.view.find('[sid=t-test' + this.testID + ']').children("div.test-ins").find('[sid=rough-took' + this.testID + ']').hasClass("active");
    var hasAnswer = this.allView.view.find('[sid=t-test' + this.testID + ']').find('.exam-scan-h');
   if(hasTrue){
      hasAnswer.removeClass('dn');
    }else{
      hasAnswer.addClass('dn');
    }

};

SwitchContent.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};