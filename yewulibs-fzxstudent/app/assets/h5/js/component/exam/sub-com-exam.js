/**
 * Created by zhangxia on 2018/2/27.
 * 小填空
 */

var SubComExam = function (answerObj, data, index) {
    this.answerObj = answerObj;
    this.allData = data;
    this.answerData = data.examOptionList;
    this.data = JSON.parse(data.examText);
    this.index = index;
    this.answered = false;
    this.score = this.allData.score;
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    this.init();
};

SubComExam.prototype.init = function () {
    this.introView = $(ExamPreViewTem.introTemplate).clone();
    this.stemView = $(ExamPreViewTem.stemTemplate).clone();
    this.introView.find('[sid=type]').text(this.allData.tiXingName);
    this.initStem();
    if (this.index) {
        this.introView.find('[sid=index]').text(this.index);
    }
    if (!this.score || this.score <= 0) {
        this.introView.find('[cid=exam_score]').hide();
    } else {
        this.introView.find('[cid=exam_score]').show();
        this.introView.find('[cid=score_text]').text(this.score);
    }
    this.view.append(this.introView);
    this.view.append(this.stemView);
};

SubComExam.prototype.initStem = function () {
    var stem = $(this.data.examStem);
    stem.find("input.cus-com").replaceWith($(CompletionChoiceItem.template));
    var ItemsView = stem.find(".black_filling");
    if (ItemsView.length !== 0) {
        this.choiceItems = new Array(ItemsView.length);
        this.value = new Array(ItemsView.length);
        for (var i = 0, len = ItemsView.length; i < len; i++) {
            if (this.data.examOptions[i]) {
                this.choiceItems[i] = new CompletionChoiceItem(ItemsView.eq(i), this.data.examOptions[i]);
                this.choiceItems[i].addListener(this, this.answerCallBack);
            }
        }
    }
    this.stemView.append(stem);
    // this.initAnswerView();
};

SubComExam.prototype.answerCallBack = function () {
    var num = 0;
    for (var i = 0, len = this.choiceItems.length; i < len; i++) {
        if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
            num++;
        }
    }
    if (num == 0) {
        this.answered = false;
    } else if (num > 0 && num < this.choiceItems.length) {
        this.answered = 'half';
    } else {
        this.answered = true;
    }
    this.answerObj.answerCallBack();
};

SubComExam.prototype.initAnswerView = function () {
    if (this.answerData && this.answerData.length > 0) {
        for (var i = 0; i < this.answerData.length; i++) {
            this.choiceItems[this.answerData[i].seq - 1].view.text(this.answerData[i].myAnswer);
            this.choiceItems[this.answerData[i].seq - 1].value = this.answerData[i].myAnswer;
        }
    }
    this.answerCallBack();
};

SubComExam.prototype.getValue = function () {
    var param = {};
    param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
    param.lessonTaskDetailsId = this.allData.id;
    param.lessonAnswerExamParentId = this.allData.lessonAnswerExamParentId;
    param.templateId = this.allData.examTemplateId;
    param.emptyCount = this.allData.examEmptyCount;
    param.score = this.allData.score;
    if (!this.answered) {
        param.answerList = null;
        return param;
    }
    param.answerList = [];
    for (var i = 0; i < this.choiceItems.length; i++) {
        if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
            var t = {};
            t.seq = parseInt(i + 1);
            t.myAnswer = this.choiceItems[i].getValue().detailContent.trim();
            t.myAnswerHtml = '';
            t.isAnswer = this.choiceItems[i].data.right;
            t.rightAnswer = this.choiceItems[i].data.content;
            param.answerList.push(t);
        }
    }
    return param;
};

SubComExam.template = '<span class="black_filling" contenteditable></span>';

SubComExam.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};

/**
 * Created by zhangxia on 2018/2/27.
 * 填空 选项
 */
var CompletionChoiceItem = function (view, data) {

    this.view = view;
    this.data = data;
    this.rightAnswer = data.content;

    this.value = null;

    this.init();
};

CompletionChoiceItem.template = '<span class="black_filling" contenteditable></span>';

CompletionChoiceItem.prototype = {

    init: function () {

        this.answer = this.data.content;
        this.unbindEvent().bindEvent();
    },

    bindEvent: function () {

        this.view.on("click", this, this.itemClick);
        this.view.on("keypress", this, this.itemKeypress);
        this.view.on("input", this, this.itemInput);
        this.view.on("focus", this, this.itemFocus);
        this.view.on("blur", this, this.itemBlur);

        return this;
    },

    unbindEvent: function () {

        this.view.off("click", this.itemClick);
        this.view.off("keypress", this.itemKeypress);
        this.view.off("input", this.itemInput);
        this.view.off("focus", this.itemFocus);
        this.view.off("blur", this.itemBlur);

        return this;
    },

    setContenteditable: function (flag) {

        if (typeof flag === "boolean") {
            this.view.attr("contenteditable", flag);
        }
    },

    itemClick: function (e) {

        var self = e.data;
        self.view.focus();
    },

    itemKeypress: function (e) {

        if (e.keyCode === 13) {
            e.preventDefault();
            e.returnValue = false;
            return false
        }
    },

    itemInput: function (e) {

        var self = e.data;
        if (40 - self.view.width() > 0) {
            self.view.css("padding-left", (40 - self.view.width()) / 2);
            self.view.css("padding-right", (40 - self.view.width()) / 2);
        } else {
            self.view.css("padding", 0);
        }

        self.value = self.view.text();
        if (self.listener) {
            self.listenerFun.call(self.listener, null);
        }
    },

    itemFocus: function (e) {

        var self = e.data;
        console.log(self);
    },

    itemBlur: function (e) {

        var self = e.data;
        console.log(self);
    },

    getValue: function () {

        var params = {};
        params.detailContent = this.value;
        return params;
    },

    addListener: function (listener, listenerFun) {

        this.listener = listener;
        this.listenerFun = listenerFun;
    },

    getRightAnswer: function () {

        return this.rightAnswer;
    },

    setAnswer: function (data) {

        this.view.append(data);
    },

    showRightAnswer: function () {

        this.view.html(this.rightAnswer);
    },

    setError: function () {
        this.view.addClass("error");
    },

    setSuccess: function () {
        this.view.addClass("success");
    },

    checkMyAnswer: function () {

        if (this.getValue() === this.answer) {
            this.view.text(this.value).removeClass("error");
            return true;
        } else {
            this.view.text(this.value).addClass("error");
            return false;
        }
    }
};