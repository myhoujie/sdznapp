/**
 * Created by wjh on 2018/11/13.
 * 综合题
 */
var ClozeTest = function (answerObj, data) {
  this.answerObj = answerObj;
  this.allData = data;
  this.data = JSON.parse(data.examText);
  this.answerData = data.examOptionList;
//  console.log(JSON.stringify(data.examOptionList));
  this.optionList = this.data.examOptions;
  this.index = data.examSeq;
  this.collFlag = this.allData.isCollect;
  this.finishFlag = false;
  this.score = this.allData.score;
  // this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-coll" sid="ans-e-a-coll"></div><div class="ans-e-a-r" sid="exam"></div></div>');
  this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-r" sid="exam"></div></div>');
  this.numView = $('<span class="ans-e-num-item" id="#' + this.allData.id + '">' + this.index + '</span>');
  this.serialNum1 = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "I", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  this.init();
};

ClozeTest.prototype.jumpExam = function () {
  var hr = $(this).attr("id");
  var anh = $(hr).offset().top + $(".ans-e-con").scrollTop() - 50;
  $(".ans-e-con").stop().animate({scrollTop: anh}, 200);
};

ClozeTest.prototype.init = function () {
  // this.collView = this.view.find('[sid=ans-e-a-coll]');
  // this.collView.off('click').on('click', this, this.collectFun);
  this.boxView = $(ExamPreViewTem.boxTemplate).clone();
  this.introView = $(ExamPreViewTem.introTemplate).clone();
  this.stemView = $(ExamPreViewTem.stemTemplate).clone();
  this.introView.find('[sid=type]').text(this.data.templateStyleName);
  this.initStem();
  if (this.index) {
    this.introView.find('[sid=index]').text(this.index);
  }
  this.boxView.append(this.stemView);
  this.numView.on('click', this, this.jumpExam);
  this.view.find('[sid=exam]').append(this.boxView);
};

ClozeTest.prototype.initStem = function () {
  this.examOptionArr = [];
  var stem = $('<div style="word-wrap:break-word;">' + this.data.examStem + '</div>');
  stem.find("input.cus-com").replaceWith($(ClozeChoiceItem.template));
  // var ItemsView = stem.find(".black_filling");
  var ItemsView = stem.find(".default_filling");
  if (ItemsView.length !== 0) {
    if (!this.score || this.score <= 0) {
      this.introView.find('[cid=exam_score]').hide();
    }
    else {
      this.introView.find('[cid=exam_score]').show();
      this.introView.find('[cid=score_text]').text(this.score * ItemsView.length);
    }
    this.boxView.append(this.introView);


    this.choiceItems = new Array(ItemsView.length);
    this.value = new Array(ItemsView.length);
    for (var i = 0, len = ItemsView.length; i < len; i++) {
      if (this.data.examOptions[i]) {
        this.choiceItems[i] = new ClozeChoiceItem(ItemsView.eq(i), this.data.examOptions[i], this.allData.id, this.optionList, i + 1, this.view);
        this.examOptionArr.push(this.choiceItems[i]);
        this.choiceItems[i].addListener(this, this.answerCallBack);
      }
    }
  }
  this.stemView.append(stem);
  // this.updateCollect();
  this.initAnswerView();
};

ClozeTest.prototype.answerCallBack = function () {
  var num = 0;
  if (this.answerData && this.answerData.length > 0) {
    for (var j = 0; j < this.answerData.length; j++) {
      num++;
    }
    for (var i = 0, len = this.choiceItems.length; i < len; i++) {
      if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
        num++;
      }
    }
  } else {
    for (var i = 0, len = this.choiceItems.length; i < len; i++) {
      if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
        num++;
      }
    }
  }
  // for (var i = 0, len = this.choiceItems.length; i < len; i++) {
  //   if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
  //     num++;
  //   }
  // }
  if (num == 0) {
    this.numView.removeClass('all');
    this.numView.removeClass('half');
    this.finishFlag = false;
  } else if (num > 0 && num < this.choiceItems.length) {
    this.numView.removeClass('all');
    this.numView.addClass('half');
    this.finishFlag = false;
  } else if (num == this.choiceItems.length) {
    this.numView.removeClass('half');
    this.numView.addClass('all');
    this.finishFlag = true;
  }
};

ClozeTest.prototype.collectFun = function (e) {
  var self = e.data;
  showLoading();
  if (!self.collFlag) {
    LearningGuideHandler.examCollectAdd({'id': self.allData.id}, self, self.collectCallBack);
  } else {
    LearningGuideHandler.examCollectDelete({'studentBookId': self.allData.studentBookId}, self, self.collectCallBack);
  }
};

ClozeTest.prototype.collectCallBack = function (flag, data) {
  hideLoading();
  if (flag) {
    this.collFlag = !this.collFlag;
    this.updateCollect();
    if (this.collFlag) {
      this.allData.studentBookId = data.result.data;
      addCollectSuccess();
    } else {
      deleteCollectSuccess();
    }
  } else {
    collectError(data.msg);
  }
};

ClozeTest.prototype.updateCollect = function () {
  if (this.collFlag) {
    this.collView.addClass('active');
  } else {
    this.collView.removeClass('active');
  }
};

ClozeTest.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};

ClozeTest.prototype.initAnswerView = function () {
  if (this.answerData && this.answerData.length > 0) {
    for (var i = 0; i < this.answerData.length; i++) {
      this.choiceItems[this.answerData[i].seq - 1].view.find('[sid=seqC]').html(this.answerData[i].seq + "&nbsp;");
      this.choiceItems[this.answerData[i].seq - 1].view.find('[sid=cContainer]').html(this.answerData[i].myAnswer);
      this.choiceItems[this.answerData[i].seq - 1].view.attr('cid', this.answerData[i].myAnswer);
      if (this.answerData[i].isRight == 0) {
        this.choiceItems[this.answerData[i].seq - 1].view.attr('fid', false);
      } else {
        this.choiceItems[this.answerData[i].seq - 1].view.attr('fid', true);
      }
      this.choiceItems[this.answerData[i].seq - 1].view.attr('seqId', this.answerData[i].clozeSeq);
      this.choiceItems[this.answerData[i].seq - 1].value = this.answerData[i].myAnswer;
    }
  }
  this.answerCallBack();
};

ClozeTest.prototype.getValue = function () {
  var param = {};
  param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
  param.lessonTaskDetailsId = this.allData.id;
  param.lessonAnswerExamParentId = this.allData.parentId;
  param.templateId = this.allData.examTemplateId;
  param.emptyCount = this.allData.examEmptyCount;
  param.score = this.allData.score;
  if ((!this.numView.hasClass('half')) && (!this.numView.hasClass('all'))) {
    param.answerList = null;
    return param;
  }
  param.answerList = [];
  for (var i = 0; i < this.choiceItems.length; i++) {
    if (this.choiceItems[i].view.find('[sid=cContainer]').html() != "") {
      var t = {};
      t.seq = parseInt(i + 1);
      t.myAnswer = this.choiceItems[i].view.attr("cid");        //当前选项 如A
      t.clozeSeq = this.choiceItems[i].view.attr("seqId");      //当前选项 如1
      t.myAnswerHtml = '';
      t.isAnswer = this.choiceItems[i].view.attr("fid");        //当前选项的right   是否正确
      for (var m = 0; m < this.choiceItems[i].data.list.length; m++) {
        if (this.choiceItems[i].data.list[m].right == true) {
          t.rightAnswer = this.serialNum1[this.choiceItems[i].data.list[m].seq - 1];      //正确选项 如B
        }
      }
      param.answerList.push(t);
    }
  }
  return param;
};


/**
 * Created by zhangxia on 2018/2/27.
 * 填空 选项
 */
var ClozeChoiceItem = function (view, data, examId, allOptionData, examSeq, allView) {

  this.view = view;
  this.data = data;
  this.examId = examId;
  this.allOptionData = allOptionData;
  this.examSeq = examSeq;
  this.value = null;
  this.allView = allView;

  this.init();

};

// ClozeChoiceItem.template = '<span class="black_filling"></span>';
ClozeChoiceItem.template = '<span class="default_filling over_ing"><span sid="seqC"></span><span sid="cContainer"></span></span>';

ClozeChoiceItem.prototype = {

  init: function () {

    //封装正确答案 开始
    for (var i = 0; i < this.data.list.length; i++) {
      if (this.data.list[i].right == true) {
        this.rightAnswer = this.data.list[i].content;
        this.answer = this.data.list[i].content;
      }
    }
    //封装正确答案  结束


    this.unbindEvent().bindEvent();

    //初始化空里的序号
    this.view.find('[sid=seqC]').html(this.examSeq + "&nbsp;");
    this.view.attr('sid', this.examSeq);


  },

  bindEvent: function () {

    this.view.on("click", this, this.itemClick);
    this.view.on("keypress", this, this.itemKeypress);
    this.view.on("input", this, this.itemInput);
    this.view.on("focus", this, this.itemFocus);
    this.view.on("blur", this, this.itemBlur);

    return this;
  },

  unbindEvent: function () {

    this.view.off("click", this.itemClick);
    this.view.off("keypress", this.itemKeypress);
    this.view.off("input", this.itemInput);
    this.view.off("focus", this.itemFocus);
    this.view.off("blur", this.itemBlur);

    return this;
  },

  setContenteditable: function (flag) {

    if (typeof flag === "boolean") {
      this.view.attr("contenteditable", flag);
    }
  },

  itemClick: function (e) {
    console.log("点击空，把试题id,当前点击的空，当前点击的空的所有选项 传给移动端");
    var self = e.data;
    self.view.focus();
    // 点击空，把试题id,当前点击的空，所有选项 传给移动端
    var allOptionData = self.allOptionData;
    clozeOptionList = allOptionData;
    if (self.allView.attr("oid")) {
      clozeOptionList = JSON.parse(self.allView.attr("oid"));
    }
    clozeAnswer(self.examId, self.examSeq, clozeOptionList);
  },

  itemKeypress: function (e) {

    if (e.keyCode === 13) {
      e.preventDefault();
      e.returnValue = false;
      return false
    }
  },

  itemInput: function (e) {
    console.log(e);
    var self = e.data;
    if (40 - self.view.width() > 0) {
      self.view.css("padding-left", (40 - self.view.width()) / 2);
      self.view.css("padding-right", (40 - self.view.width()) / 2);
    } else {
      self.view.css("padding", 0);
    }

    self.value = self.view.find('[sid=cContainer]').text();
    if (self.listener) {
      self.listenerFun.call(self.listener, null);
    }
  },

  itemFocus: function (e) {

    var self = e.data;
    // console.log(self);
  },

  itemBlur: function (e) {

    var self = e.data;
    // console.log(self);
  },

  getValue: function () {

    var params = {};
//    console.log(this.view.text());
//    console.log(1 + "                 " + this.value);
    params.detailContent = this.view.attr("mid");
    params.mySeq = "";
    return params;
  },

  addListener: function (listener, listenerFun) {

    this.listener = listener;
    this.listenerFun = listenerFun;
  },

  getRightAnswer: function () {

    return this.rightAnswer;
  },

  setAnswer: function (data) {

    this.view.append(data);
  },

  showRightAnswer: function () {

    this.view.html(this.rightAnswer);
  },

  setError: function () {
    this.view.addClass("error");
  },

  setSuccess: function () {
    this.view.addClass("success");
  },

  checkMyAnswer: function () {

    if (this.getValue() === this.answer) {
      this.view.text(this.value).removeClass("error");
      return true;
    } else {
      this.view.text(this.value).addClass("error");
      return false;
    }
  }
};