/**
 * Created by zhangxia on  2018/3/1.
 * 单选 多选 判断
 */

var SubSelectExam = function (answerObj, data, index) {
    this.answerObj = answerObj;
    this.allData = data;
    this.answerData = data.examOptionList;
    this.data = JSON.parse(data.examText);
    this.index = index;
    this.answered = false;
    this.score = this.allData.score;
    this.view = $(ExamPreViewTem.boxTemplate).clone();
    this.numView = $('<span class="ans-e-num-item">' + parseInt(this.index + 1) + '</span>');
    this.init();
};

SubSelectExam.prototype.init = function () {
    this.introView = $(ExamPreViewTem.introTemplate).clone();
    this.stemView = $(ExamPreViewTem.stemTemplate).clone();
    this.introView.find('[sid=index]').text(this.index);
    this.introView.find('[sid=type]').text(this.allData.tiXingName);
    this.stemView.html(this.data.examStem);
    if (!this.score || this.score <= 0) {
        this.introView.find('[cid=exam_score]').hide();
    } else {
        this.introView.find('[cid=exam_score]').show();
        this.introView.find('[cid=score_text]').text(this.score);
    }
    this.optionConView = $(ExamPreViewTem.optionConTemplate).clone();
    this.view.append(this.introView);
    this.view.append(this.stemView);
    this.view.append(this.optionConView);
    this.initOption();
};

SubSelectExam.prototype.initOption = function () {
    this.optionArr = [];
    for (var i = 0; i < this.data.examOptions.length; i++) {
        var tempData = this.data.examOptions[i];
        tempData.examTypeId = this.data.examTypeId;
        var option = new SelectOption(this, this.data.examOptions[i], i);
        if (this.data.examTypeId == 2) {
            //多选
            option.addListener(this, this.clickOptionCallBack2);
        } else {
            //单选，判断
            option.addListener(this, this.clickOptionCallBack1);
        }
        this.optionArr.push(option);
        this.optionConView.append(option.view);
    }
    // this.initAnswerView();
};

//单选
SubSelectExam.prototype.clickOptionCallBack1 = function (obj, type) {
    for (var i = 0; i < this.optionArr.length; i++) {
        this.optionArr[i].clickFlag = false;
        this.optionArr[i].view.find('[sid=index]').removeClass('active');
    }
    if (type) {
        obj.clickFlag = true;
        obj.view.find('[sid=index]').addClass('active');
        this.answered = true;
    } else {
        this.answered = false;
    }
    if (this.listener) {
        this.listenerFun.call(this.listener);
    }
};

//多选
SubSelectExam.prototype.clickOptionCallBack2 = function (obj, type) {
    var len = 0;
    if (type) {
        obj.clickFlag = true;
        obj.view.find('[sid=index]').addClass('active');
    } else {
        obj.clickFlag = false;
        obj.view.find('[sid=index]').removeClass('active');
    }
    for (var i = 0; i < this.optionArr.length; i++) {
        if (this.optionArr[i].clickFlag) {
            len++;
        }
    }
    if (len > 0) {
        this.answered = true;
    } else {
        this.answered = false;
    }
    if (this.listener) {
        this.listenerFun.call(this.listener);
    }
};

SubSelectExam.prototype.initAnswerView = function () {
    if (this.answerData && this.answerData.length > 0) {
        for (var i = 0; i < this.answerData.length; i++) {
            this.optionArr[this.answerData[i].seq - 1].view.find('[sid=index]').trigger('click');
        }
    }
};

SubSelectExam.prototype.getValue = function () {
    var param = {};
    param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
    param.lessonTaskDetailsId = this.allData.id;
    param.lessonAnswerExamParentId = this.allData.lessonAnswerExamParentId;
    param.templateId = this.allData.examTemplateId;
    param.emptyCount = this.allData.examEmptyCount;
    param.score = this.allData.score;
    if (!this.answered) {
        param.answerList = null;
        return param;
    }
    param.answerList = [];
    for (var i = 0; i < this.optionArr.length; i++) {
        if (this.optionArr[i].clickFlag) {
            var t = {};
            t.seq = parseInt(i + 1);
            t.myAnswer = this.optionArr[i].indexFlag;
            t.myAnswerHtml = '';
            t.isAnswer = this.optionArr[i].data.right;
            t.rightAnswer = '';
            param.answerList.push(t);
        }
    }
    return param;
};

SubSelectExam.prototype.addListener = function (listener, listenerFun) {
    this.listener = listener;
    this.listenerFun = listenerFun;
};