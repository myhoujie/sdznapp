/**
 * Created by zhangxia on 2018/2/27.
 * 填空
 */

var ComExam = function (answerObj, data) {
  this.answerObj = answerObj;
  this.allData = data;
  this.data = JSON.parse(data.examText);
  this.answerData = data.examOptionList;
  this.index = data.examSeq;
  this.collFlag = this.allData.isCollect;
  this.finishFlag = false;
  this.score = this.allData.score;
  // this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-coll" sid="ans-e-a-coll"></div><div class="ans-e-a-r" sid="exam"></div></div>');
  this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-r" sid="exam"></div></div>');
  this.numView = $('<span class="ans-e-num-item" id="#' + this.allData.id + '">' + this.index + '</span>');
  this.init();
};

ComExam.prototype.jumpExam = function () {
  var hr = $(this).attr("id");
  // console.log($(hr).offset().top+$(".ans-e-con").scrollTop());
  var anh = $(hr).offset().top + $(".ans-e-con").scrollTop() - 50;
  $(".ans-e-con").stop().animate({scrollTop: anh}, 200);
};

ComExam.prototype.init = function () {
  // this.collView = this.view.find('[sid=ans-e-a-coll]');
  // this.collView.off('click').on('click', this, this.collectFun);
  this.boxView = $(ExamPreViewTem.boxTemplate).clone();
  this.introView = $(ExamPreViewTem.introTemplate).clone();
  this.stemView = $(ExamPreViewTem.stemTemplate).clone();
  this.introView.find('[sid=type]').text(this.data.templateStyleName);
  this.initStem();
  if (this.index) {
    this.introView.find('[sid=index]').text(this.index + '.');
  }
  // if (!this.score || this.score <= 0) {
  //     this.introView.find('[cid=exam_score]').hide();
  // }
  // else {
  //     this.introView.find('[cid=exam_score]').show();
  //     this.introView.find('[cid=score_text]').text(this.score);
  // }
  // this.boxView.append(this.introView);
  this.boxView.append(this.stemView);
  this.numView.on('click', this, this.jumpExam);
  this.view.find('[sid=exam]').append(this.boxView);
};

ComExam.prototype.initStem = function () {
  var stem = $('<div>' + this.data.examStem + '</div>');
  stem.find("input.cus-com").replaceWith($(CompletionChoiceItem.template));
  var ItemsView = stem.find(".black_filling");
  if (ItemsView.length !== 0) {
    if (!this.score || this.score <= 0) {
      this.introView.find('[cid=exam_score]').hide();
    }
    else {
      this.introView.find('[cid=exam_score]').show();
      this.introView.find('[cid=score_text]').text(this.score * ItemsView.length);
    }
    this.boxView.append(this.introView);
    this.choiceItems = new Array(ItemsView.length);
    this.value = new Array(ItemsView.length);
    for (var i = 0, len = ItemsView.length; i < len; i++) {
      if (this.data.examOptions[i]) {
        this.choiceItems[i] = new CompletionChoiceItem(ItemsView.eq(i), this.data.examOptions[i]);
        this.choiceItems[i].addListener(this, this.answerCallBack);
      }
    }
  }
  this.stemView.append(stem);
  // this.updateCollect();
  this.initAnswerView();
};

ComExam.prototype.answerCallBack = function () {
  var num = 0;
  for (var i = 0, len = this.choiceItems.length; i < len; i++) {
    if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
      num++;
    }
  }
  if (num == 0) {
    this.numView.removeClass('all');
    this.numView.removeClass('half');
    this.finishFlag = false;
  } else if (num > 0 && num < this.choiceItems.length) {
    this.numView.removeClass('all');
    this.numView.addClass('half');
    this.finishFlag = false;
  } else if (num == this.choiceItems.length) {
    this.numView.removeClass('half');
    this.numView.addClass('all');
    this.finishFlag = true;
  }
};

ComExam.prototype.collectFun = function (e) {
  var self = e.data;
  showLoading();
  if (!self.collFlag) {
    LearningGuideHandler.examCollectAdd({'id': self.allData.id}, self, self.collectCallBack);
  } else {
    LearningGuideHandler.examCollectDelete({'studentBookId': self.allData.studentBookId}, self, self.collectCallBack);
  }
};

ComExam.prototype.collectCallBack = function (flag, data) {
  hideLoading();
  if (flag) {
    this.collFlag = !this.collFlag;
    this.updateCollect();
    if (this.collFlag) {
      this.allData.studentBookId = data.result.data;
      addCollectSuccess();
    } else {
      deleteCollectSuccess();
    }
  } else {
    collectError(data.msg);
  }
};

ComExam.prototype.updateCollect = function () {
  if (this.collFlag) {
    this.collView.addClass('active');
  } else {
    this.collView.removeClass('active');
  }
};

ComExam.template = '<span class="black_filling" contenteditable></span>';

ComExam.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};

ComExam.prototype.initAnswerView = function () {
  if (this.answerData && this.answerData.length > 0) {
    for (var i = 0; i < this.answerData.length; i++) {
      this.choiceItems[this.answerData[i].seq - 1].view.text(this.answerData[i].myAnswer);
      this.choiceItems[this.answerData[i].seq - 1].value = this.answerData[i].myAnswer;
    }
  }
  this.answerCallBack();
};

ComExam.prototype.getValue = function () {
  var param = {};
  param.lessonAnswerExamId = this.allData.lessonAnswerExamId;
  param.lessonTaskDetailsId = this.allData.id;
  param.lessonAnswerExamParentId = this.allData.parentId;
  param.templateId = this.allData.examTemplateId;
  param.emptyCount = this.allData.examEmptyCount;
  param.score = this.allData.score;
  if ((!this.numView.hasClass('half')) && (!this.numView.hasClass('all'))) {
    param.answerList = null;
    return param;
  }
  param.answerList = [];
  for (var i = 0; i < this.choiceItems.length; i++) {
    if (this.choiceItems[i] && this.choiceItems[i].getValue().detailContent) {
      var t = {};
      t.seq = parseInt(i + 1);
      t.myAnswer = this.choiceItems[i].getValue().detailContent.trim();
      t.myAnswerHtml = '';
      t.isAnswer = this.choiceItems[i].data.right;
      t.rightAnswer = this.choiceItems[i].data.content;
      param.answerList.push(t);
    }
  }
  return param;
};


/**
 * Created by zhangxia on 2018/2/27.
 * 填空 选项
 */
var CompletionChoiceItem = function (view, data) {

  this.view = view;
  this.data = data;
  this.rightAnswer = data.content;

  this.value = null;

  this.init();
};

CompletionChoiceItem.template = '<span class="black_filling" contenteditable></span>';

CompletionChoiceItem.prototype = {

  init: function () {

    this.answer = this.data.content;
    this.unbindEvent().bindEvent();
  },

  bindEvent: function () {

    this.view.on("click", this, this.itemClick);
    this.view.on("keypress", this, this.itemKeypress);
    this.view.on("input", this, this.itemInput);
    this.view.on("focus", this, this.itemFocus);
    this.view.on("blur", this, this.itemBlur);

    return this;
  },

  unbindEvent: function () {

    this.view.off("click", this.itemClick);
    this.view.off("keypress", this.itemKeypress);
    this.view.off("input", this.itemInput);
    this.view.off("focus", this.itemFocus);
    this.view.off("blur", this.itemBlur);

    return this;
  },

  setContenteditable: function (flag) {

    if (typeof flag === "boolean") {
      this.view.attr("contenteditable", flag);
    }
  },

  itemClick: function (e) {

    var self = e.data;
    self.view.focus();
  },

  itemKeypress: function (e) {

    if (e.keyCode === 13) {
      e.preventDefault();
      e.returnValue = false;
      return false
    }
  },

  itemInput: function (e) {

    var self = e.data;
    if (40 - self.view.width() > 0) {
      self.view.css("padding-left", (40 - self.view.width()) / 2);
      self.view.css("padding-right", (40 - self.view.width()) / 2);
    } else {
      self.view.css("padding", 0);
    }

    self.value = self.view.text();
    if (self.listener) {
      self.listenerFun.call(self.listener, null);
    }
  },

  itemFocus: function (e) {

    var self = e.data;
    console.log(self);
  },

  itemBlur: function (e) {

    var self = e.data;
    console.log(self);
  },

  getValue: function () {

    var params = {};
    params.detailContent = this.value;
    return params;
  },

  addListener: function (listener, listenerFun) {

    this.listener = listener;
    this.listenerFun = listenerFun;
  },

  getRightAnswer: function () {

    return this.rightAnswer;
  },

  setAnswer: function (data) {

    this.view.append(data);
  },

  showRightAnswer: function () {

    this.view.html(this.rightAnswer);
  },

  setError: function () {
    this.view.addClass("error");
  },

  setSuccess: function () {
    this.view.addClass("success");
  },

  checkMyAnswer: function () {

    if (this.getValue() === this.answer) {
      this.view.text(this.value).removeClass("error");
      return true;
    } else {
      this.view.text(this.value).addClass("error");
      return false;
    }
  }
};