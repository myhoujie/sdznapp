/**
 * Created by zhangxia on 2018/2/28.
 * 综合题
 */

var Comprehensive = function (answerObj, data) {
//  console.log(data)
  this.answerObj = answerObj;
  this.allData = data;
  this.data = JSON.parse(data.examText);
  this.answerData = data.examOptionList;
  this.smallData = data.examList;
  this.index = data.examSeq;
  this.collFlag = this.allData.isCollect;
  this.finishFlag = false;
  this.score = this.allData.score;
  // this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-coll" sid="ans-e-a-coll"></div><div class="ans-e-a-r" sid="exam"></div></div>');
  this.view = $('<div class="ans-e-exam box-s" id="' + this.allData.id + '"><div class="ans-e-a-r" sid="exam"></div></div>');
  this.numView = $('<span class="ans-e-num-item" id="#' + this.allData.id + '">' + this.index + '</span>');
  this.view.addClass(this.allData.id);
  this.init();
};

Comprehensive.prototype.jumpExam = function () {
  var hr = $(this).attr("id");
  // console.log($(hr).offset().top+$(".ans-e-con").scrollTop());
  var anh = $(hr).offset().top + $(".ans-e-con").scrollTop() - 50;
  $(".ans-e-con").stop().animate({scrollTop: anh}, 200);
};

Comprehensive.prototype.init = function () {
  // this.collView = this.view.find('[sid=ans-e-a-coll]');
  // this.collView.off('click').on('click', this, this.collectFun);
  this.boxView = $(ExamPreViewTem.boxTemplate).clone();
  this.introView = $(ExamPreViewTem.introTemplate).clone();
  this.stemView = $(ExamPreViewTem.stemTemplate).clone();
  this.smallView = $(ExamPreViewTem.optionAllConTemp).clone();
  this.introView.find('[sid=type]').text(this.data.templateStyleName);
  this.stemView.html(this.data.examStem);
  if (this.index) {
    this.introView.find('[sid=index]').text(this.index + '.');
  }
  if (!this.score || this.score <= 0) {
    this.introView.find('[cid=exam_score]').hide();
  } else {
    this.introView.find('[cid=exam_score]').show();
    this.introView.find('[cid=score_text]').text(this.score);
  }
  this.boxView.append(this.introView);
  this.boxView.append(this.stemView);
  // this.updateCollect();
  this.boxView.append(this.smallView);
  this.view.find('[sid=exam]').append(this.boxView);
  this.numView.on('click', this, this.jumpExam);
  this.initSmallExam();
};

//加载小题
Comprehensive.prototype.initSmallExam = function () {
  this.smallExamArr = [];
  for (var i = 0; i < this.smallData.length; i++) {
    this.smallData[i].tiXingName = this.getName(this.smallData[i].examTemplateId);
    switch (this.smallData[i].examTemplateId) {
      case 1:
        this.initSubSingleExam(this.smallData[i], i);
        break;
      case 2:
        this.initSubSingleExam(this.smallData[i], i);
        break;
      case 3:
        this.initSubSingleExam(this.smallData[i], i);
        break;
      case 6:
        this.initSubComExam(this.smallData[i], i);
        break;
      case 4:
        this.initSubShortAnswerExam(this.smallData[i], i);
        break;
      default:
        console.log('未识别出题目类型，无法加载');
    }
  }
  this.initAnswerView();
};

//加载答案
Comprehensive.prototype.initAnswerView = function () {
  for (var i = 0; i < this.smallExamArr.length; i++) {
    this.smallExamArr[i].initAnswerView();
  }
};

//单选
Comprehensive.prototype.initSubSingleExam = function (data, index) {
  var tempExam = new SubSelectExam(this, data, "(" + parseInt(index + 1) + ")");
  tempExam.addListener(this, this.answerCallBack);
  this.smallExamArr.push(tempExam);
  this.smallView.append(tempExam.view);
};

//填空
Comprehensive.prototype.initSubComExam = function (data, index) {
  var tempExam = new SubComExam(this, data, "(" + parseInt(index + 1) + ")");
  tempExam.addListener(this, this.answerCallBack);
  this.smallExamArr.push(tempExam);
  this.smallView.append(tempExam.view);
};

//简答
Comprehensive.prototype.initSubShortAnswerExam = function (data, index) {
//  console.log(data);
  var tempExam = new SubShortExam(this, data, parseInt(index + 1));
  tempExam.addListener(this, this.answerCallBack);
  this.smallExamArr.push(tempExam);
  this.smallView.append(tempExam.view);
};

Comprehensive.prototype.answerCallBack = function () {
  var num = false;
  var comNum = 0;
  for (var i = 0, len = this.smallExamArr.length; i < len; i++) {
    if (this.smallExamArr[i].allData.examTemplateId == 4) {
      if (this.smallExamArr[i].writeView.find('.imgUP').length > 0 || this.smallExamArr[i].photoView.find('.imgUP').length > 0) {
        this.smallExamArr[i].answered = true;
        num++;
      } else {
      }
    } else if (this.smallExamArr[i].allData.examTemplateId == 6) {
      if (this.smallExamArr[i] && this.smallExamArr[i].answered == true) {
        num++;
      } else if (this.smallExamArr[i] && this.smallExamArr[i].answered == 'half') {
        comNum++;
      } else {
      }
    } else {
      if (this.smallExamArr[i] && this.smallExamArr[i].answered) {
        num++;
      } else {
      }
    }
  }
  if (num == 0 && comNum > 0) {
    this.numView.removeClass('all');
    this.numView.addClass('half');
    this.finishFlag = false;
  } else if (num == 0) {
    this.numView.removeClass('all');
    this.numView.removeClass('half');
    this.finishFlag = false;
  } else if (num > 0 && num < this.smallExamArr.length) {
    this.numView.removeClass('all');
    this.numView.addClass('half');
    this.finishFlag = false;
  } else if (num == this.smallExamArr.length) {
    this.numView.removeClass('half');
    this.numView.addClass('all');
    this.finishFlag = true;
  }
};

Comprehensive.prototype.collectFun = function (e) {
  var self = e.data;
  showLoading();
  if (!self.collFlag) {
    LearningGuideHandler.examCollectAdd({'id': self.allData.id}, self, self.collectCallBack);
  } else {
    LearningGuideHandler.examCollectDelete({'studentBookId': self.allData.studentBookId}, self, self.collectCallBack);
  }
};

Comprehensive.prototype.collectCallBack = function (flag, data) {
  hideLoading();
  if (flag) {
    this.collFlag = !this.collFlag;
    this.updateCollect();
    if (this.collFlag) {
      this.allData.studentBookId = data.result.data;
      addCollectSuccess();
    } else {
      deleteCollectSuccess();
    }
  } else {
    collectError(data.msg);
  }
};

Comprehensive.prototype.updateCollect = function () {
  if (this.collFlag) {
    this.collView.addClass('active');
  } else {
    this.collView.removeClass('active');
  }
};

Comprehensive.prototype.getName = function (type) {
  switch (type) {
    case 1:
      return '单选题';
      break;
    case 2:
      return '多选题';
      break;
    case 3:
      return '判断题';
      break;
    case 4:
      return '简答题';
      break;
    case 6:
      return '填空题';
      break;
    default:
      return '未知';
      break;
  }
};

Comprehensive.prototype.getValue = function () {
  var arr = [];
  for (var i = 0; i < this.smallExamArr.length; i++) {
    if (this.smallExamArr[i].getValue()) {
      arr.push(this.smallExamArr[i].getValue());
    }
  }
  return arr;
};

Comprehensive.template = '<span class="black_filling" contenteditable></span>';

Comprehensive.prototype.addListener = function (listener, listenerFun) {
  this.listener = listener;
  this.listenerFun = listenerFun;
};