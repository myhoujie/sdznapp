//package com.sdzn.fzx.student.app;
//
//import android.app.Application;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageInfo;
//import android.content.res.Configuration;
//import android.content.res.Resources;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.net.Uri;
//import android.os.Handler;
//import android.os.Looper;
//import android.support.multidex.MultiDex;
//import android.support.multidex.MultiDexApplication;
//import android.text.TextUtils;
//import android.util.DisplayMetrics;
//
//import com.liulishuo.filedownloader.FileDownloader;
//import com.netease.nimlib.sdk.NIMClient;
//import com.netease.nimlib.sdk.SDKOptions;
//import com.netease.nimlib.sdk.auth.LoginInfo;
//import com.netease.nimlib.sdk.util.NIMUtil;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
//import com.sdzn.fzx.student.BuildConfig;
//import com.sdzn.fzx.student.chatroom.NimSDKOptionConfig;
//import com.sdzn.fzx.student.dao.DaoMaster;
//import com.sdzn.fzx.student.dao.DaoSession;
//import com.example.app1home.ui.activity.LoginActivity;
//import com.example.app1home.ui.base.ActivityManager;
//import com.example.app1home.ui.event.Event;
//import com.example.app1home.ui.listener.MyTinkerLoadReporter;
//import com.example.app1home.ui.listener.MyTinkerPatchReporter;
//import com.sdzn.fzx.student.util.AndroidUtil;
//import com.sdzn.fzx.student.util.Log;
//import com.sdzn.fzx.student.util.Timer;
//import com.sdzn.fzx.student.vo.SSOInfoBean;
//import com.tencent.bugly.crashreport.CrashReport;
//import com.tencent.tinker.lib.listener.DefaultPatchListener;
//import com.tencent.tinker.lib.patch.UpgradePatch;
//import com.tencent.tinker.lib.service.DefaultTinkerResultService;
//import com.tencent.tinker.lib.tinker.TinkerInstaller;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//import org.xutils.x;
//
//import java.security.SecureRandom;
//import java.security.cert.X509Certificate;
//
//import javax.crypto.Cipher;
//import javax.crypto.spec.SecretKeySpec;
//import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSession;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//import rain.coder.photopicker.PhotoPick;
//
///**
// * 自定义ApplicationLike类.
// * <p>
// * 注意：这个类是Application的代理类，以前所有在Application的实现必须要全部拷贝到这里<br/>
// *
// * @author wenjiewu
// * @since 2016/11/7
// */
//public class MyApplication extends MultiDexApplication {
//
//    public static final String TAG = "Tinker.SampleApplicationLike";
//    public static final String DIR_PROJECT = "/alty/app/";
//    public static final String DIR_CACHE = DIR_PROJECT + "cache/"; // 网页缓存路径
//    public static final String IMG_CACHE = DIR_PROJECT + "image/"; // image缓存路径
//    public static final String VIDEO_CACHE = DIR_PROJECT + "video/"; // video缓存路径
//    public static final String MUSIC_CACHE = DIR_PROJECT + "music/"; // music缓存路径
//    private int mFinalCount;
//
//
//    public static Application mContext;
//    public static AppLike mAppLike;
//    private static DaoSession daoSession;
//    public static String BOX_URL = null;
//    public static int flag = 0;//开关状态
//    private NetworkReceiver networkReceiver;
//    public BLEServiceManger mBLEServiceManger;
//    private Handler handler;
//
//    public static String qiniuDomain;
//    public static boolean isReceiveRbMq = false;
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        // tinker
//        handleSSLHandshake();
//        handler = new Handler(Looper.myLooper());
//        Resources resources = getResources();
//        if (resources != null) {
//            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
//            resources.updateConfiguration(resources.getConfiguration(), displayMetrics);
//            Configuration configuration = resources.getConfiguration();
//            configuration.fontScale = 1.0f;
//            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
//        }
//
//        if (!AndroidUtil.isApkInDebug(this)) {
//            initCrashHandler();
//            Log.setEnabled(false);
//        } else {
//            Log.setEnabled(true);
//        }
//        MultiDex.install(this);
//        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
//        strategy.setAppChannel(BuildConfig.BUGLY_CHANNEL);
//        CrashReport.initCrashReport(this, "dc5dce9f96", true, strategy);
//        CrashReport.setIsDevelopmentDevice(this, BuildConfig.VERSION_NAME.endsWith("3"));
//        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "basemvp.db");
//        SQLiteDatabase writableDatabase = devOpenHelper.getWritableDatabase();
//        DaoMaster daoMaster = new DaoMaster(writableDatabase);
//        daoSession = daoMaster.newSession();
//        initImageLoader();
//        x.Ext.init(this);
//        PhotoPick.init(this);
//
//        checkNetwork();
//
//        EventBus.getDefault().register(this);
//        //注册fileDownloader
//        FileDownloader.setup(this);
//
//        initUIKit();
//    }
//
//    private void initCrashHandler() {
////        CrashHandler crashHandler = CrashHandler.getInstance();
////        crashHandler.init(getApplicationContext());
//    }
//
//    /**
//     * 初始化图片加载器
//     */
//    private void initImageLoader() {
//        ImageLoaderConfiguration configuration = ImageLoaderConfiguration
//                .createDefault(this);
//        ImageLoader.getInstance().init(configuration);
//    }
//
//    public DaoSession getDaoSession() {
//        return daoSession;
//    }
//
//    private void checkNetwork() {
//        new Timer(this).start(30 * 1000);
//
//        IntentFilter filter = new IntentFilter();
//        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
//        networkReceiver = new NetworkReceiver();
//        registerReceiver(networkReceiver, filter);
//
//    }
//
////    @Override
////    protected void attachBaseContext(Context base) {
////        super.attachBaseContext(base);
////        MultiDex.install(this);
////    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onLoginSuccess(String msg) {
//        if (TextUtils.equals(Event.ONLOGIN_SUCCESS, msg)) {
//            if (handler == null) {
//                handler = new Handler(Looper.getMainLooper());
//            }
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    SchoolBoxWatcher.getInstance().getBoxUrl();
//                    SchoolBoxWatcher.getInstance().stopWatch();
//                    SchoolBoxWatcher.getInstance().startWatch();
//                }
//            }, 5000);
//
//        } else if (TextUtils.equals(Event.EVENT_TOKEN_MISS, msg)) {
//            Intent intent = new Intent(this, LoginActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            ActivityManager.finishAll();
//        }
//
//    }
//
//    @Override
//    public Resources getResources(Resources resources) {
//        if (resources != null) {
//            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
//            Configuration conf = resources.getConfiguration();
//            if (resources.getConfiguration().fontScale != 1.0f) {
//                conf.fontScale = 1.0f;
//            }
//            resources.updateConfiguration(conf, displayMetrics);
//        }
//        return resources;
//    }
//
//    @Override
//    public void onTerminate() {
//        EventBus.getDefault().unregister(this);
//        unregisterReceiver(networkReceiver);
//        super.onTerminate();
//    }
//
//
//    private static final String Name = "Name";
//    private static final String Pwd = "Pwd";
//    private static final String AutoLogin = "autoLogin";
//    private static final String SEED = "com.sdzn.fzx";
//
//    public SSOInfoBean getSSOInfo() {
//        SSOInfoBean bean = null;
//        Uri uri = Uri.parse("content://com.linspirer.android.fzxsso.provider.UserProvider/user/query");
//        Cursor cursor = getContentResolver().query(uri, new String[]{Name, Pwd, AutoLogin}, null, null, null);
//        if (cursor == null) {
//            return bean;
//        }
//        if (cursor.getCount() > 0 && cursor.moveToFirst()) {
//            String name = cursor.getString(cursor.getColumnIndex(Name));
//            String pwd = cursor.getString(cursor.getColumnIndex(Pwd));
//            boolean autoLogin = cursor.getString(cursor.getColumnIndex(AutoLogin)).equals("1");
//            try {
//                name = decrypt(name);
//                pwd = decrypt(pwd);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            bean = new SSOInfoBean(name, pwd, autoLogin);
//        }
//        cursor.close();
//        return bean;
//    }
//
//    public void logoutSSO() {
//        // TODO: 2018-11-12 注销时将对应数据autoLogin字段改false
//    }
//
//    /**
//     * 字符串解密
//     */
//    private String decrypt(String content) throws Exception {
//        // 创建AES秘钥
//        SecretKeySpec key = new SecretKeySpec(SEED.getBytes(), "AES/CBC/PKCS5PADDING");
//        // 创建密码器
//        Cipher cipher = Cipher.getInstance("AES");
//        // 初始化解密器
//        cipher.init(Cipher.DECRYPT_MODE, key);
//        // 解密
//        byte[] decrypt = cipher.doFinal(content.getBytes("UTF-8"));
//        return new String(decrypt, "UTF-8");
//    }
//
//    /**
//     * 字符串加密
//     */
//    private String encrypt(String content) throws Exception {
//        // 创建AES秘钥
//        SecretKeySpec key = new SecretKeySpec(SEED.getBytes(), "AES/CBC/PKCS5PADDING");
//        // 创建密码器
//        Cipher cipher = Cipher.getInstance("AES");
//        // 初始化加密器
//        cipher.init(Cipher.ENCRYPT_MODE, key);
//        // 加密
//        byte[] bytes = cipher.doFinal(content.getBytes("UTF-8"));
//        return new String(bytes, "UTF-8");
//    }
//
//    /*================ 手写笔字段 ================*/
////    public static String mPenName = "";
////    public static String mFirmWare = "";
////    public static String mMCUFirmWare = "";
////    public static String mCustomerID = "0000";
//    public static String mBTMac = "";
//    public static int mBattery = 0;
//    public static boolean mPenIsCharging = false;
////    public static int mUsedMem = 0;
////    public static boolean mBeep = true;
////    public static boolean mPowerOnMode = true;
////    public static int mPowerOffTime = 20;
////    public static long mTimer = 1262275200; // 2010-01-01 00:00:00
////    public static int mPenSens = 0;
////    public static String tmp_mPenName;
////    public static boolean tmp_mBeep = true;
////    public static boolean tmp_mPowerOnMode = true;
////    public static int tmp_mPowerOffTime;
////    public static int tmp_mPenSens;
////    public static long tmp_mTimer;
//
//    /*================ 领创对接 ================*/
//
//    //判断当前rom是否为领创定制版
//    public boolean isLingChuangPad() {
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.android.launcher3", 0);
//            String appName = info.applicationInfo.loadLabel(getPackageManager()).toString();
//            Log.e("领创平板管理", "名字" + appName);
//            return "领创平板管理".equals(appName);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    /**
//     * 登录接口
//     */
//    public void login(String userId, String userName, String schoolId, String className, String txUrl, String token) {
//        Intent intent = new Intent("com.linspirer.edu.loginapkfinish");
//        intent.putExtra("userid", userId);
//        intent.putExtra("username", userName);
//        intent.putExtra("schoolid", schoolId);
//        intent.putExtra("classname", className);
//        intent.putExtra("txurl", txUrl);
//        intent.putExtra("token", token);
//        sendBroadcast(intent);
//    }
//
//    /**
//     * 注销接口
//     */
//    public void logout() {
//        sendBroadcast(new Intent("com.linspirer.edu.logout"));
//    }
//
//    /**
//     * 锁定按键 包括 Home键、 Back键、 Recent键、音量键、电源键、短按截屏组合键
//     *
//     * @param classLast todo 不知道什么意思
//     * @param time      锁定时间, 单位分钟
//     */
//    public void lockKey(String classLast, int time) {
//        Intent intent = new Intent("com.linspirer.edu.class.begin");
//        intent.putExtra(classLast, time);
//        sendBroadcast(intent);
//    }
//
//    /**
//     * 解锁按键
//     */
//    public void unLockKey() {
//        sendBroadcast(new Intent("com.linspirer.edu.class.over"));
//    }
//
//    /**
//     * 回到桌面
//     */
//    public void returnLauncher() {
//        sendBroadcast(new Intent("com.linspirer.edu.returnlauncher"));
//    }
//
//    /**
//     * 禁用back键
//     */
//    public void disableBack() {
//        sendBroadcast(new Intent("com.linspirer.edu.disableback"));
//    }
//
//    /**
//     * 启用back键
//     */
//    public void enableBack() {
//        sendBroadcast(new Intent("com.linspirer.edu.enableback"));
//    }
//
//    /**
//     * 禁用home
//     */
//    public void disableHome() {
//        sendBroadcast(new Intent("com.linspirer.edu.disablehome"));
//    }
//
//    /**
//     * 启用home
//     */
//    public void enableHome() {
//        sendBroadcast(new Intent("com.linspirer.edu.enablehome"));
//    }
//
//    /**
//     * 禁用recent键
//     */
//    public void disableRecent() {
//        sendBroadcast(new Intent("com.linspirer.edu.disablerecent"));
//    }
//
//    /**
//     * 启用recent键
//     */
//    public void enableRecent() {
//        sendBroadcast(new Intent("com.linspirer.edu.enablerecent"));
//    }
//
//    /**
//     * 呼叫管理员界面
//     */
//    public void openAdmin() {
//        sendBroadcast(new Intent("com.android.launcher3.mdm.OPEN_ADMIN"));
//    }
//
//    /**
//     * 检查更新
//     */
//    public void checkUpdate() {
//        sendBroadcast(new Intent("com.android.laucher3.mdm.CHECK_UPDATE"));
//    }
//
//    private void initUIKit() {
//        //先初始化
//        NIMClient.init(mContext, getLoginInfo(), NimSDKOptionConfig.getSDKOptions(mContext));
//        // crash handler
////        AppCrashHandler.getInstance(this);
//        // init pinyin
//
//        if (NIMUtil.isMainProcess(mContext)) {
//
//
//        }
//    }
//
//    private LoginInfo getLoginInfo() {
////        String account = "18366166528";
////        String token =  "7eafaa28e9aaa152d0c3dacd572340f9";
////
////        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(token)) {
////
////            return new LoginInfo(account, token);
////        } else {
////            return null;
////        }
//        return null;
//    }
//
//    private SDKOptions buildUIKitOptions() {
//        SDKOptions options = new SDKOptions();
//        // 设置app图片/音频/日志等缓存目录
//        options.sdkStorageRootPath = NimSDKOptionConfig.getAppCacheDir(mContext) + "/app";
//        return options;
//    }
//
//    /**
//     * 忽略https的证书校验
//     * 避免Glide加载https图片报错：
//     * javax.net.ssl.SSLHandshakeException: java.security.cert.CertPathValidatorException: Trust anchor for certification path not found.
//     */
//    public static void handleSSLHandshake() {
//        try {
//            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }
//
//                @Override
//                public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                }
//
//                @Override
//                public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                }
//            }};
//
//            SSLContext sc = SSLContext.getInstance("TLS");
//            // trustAllCerts信任所有的证书
//            sc.init(null, trustAllCerts, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });
//        } catch (Exception ignored) {
//        }
//    }
//
//    @Override
//    protected void attachBaseContext(Context base) {
//        //保存系统选择语言
//        super.attachBaseContext(base);
//        MultiDex.install(base);
//        // TODO: 安装tinker
//        //or you can put com.tencent.tinker.** to main dex
//        TinkerInstaller.install(this,
//                new MyTinkerLoadReporter(this),
//                new MyTinkerPatchReporter(this),
//                new DefaultPatchListener(this),
//                DefaultTinkerResultService.class,
//                new UpgradePatch());
//    }
//
//}
