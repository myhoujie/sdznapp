package com.sdzn.fzx.student.app;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import com.example.app1xxrw.fragment.TaskFragment;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.liulishuo.filedownloader.FileDownloader;
import com.meituan.android.walle.WalleChannelReader;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.SDKOptions;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.util.NIMUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sdzn.fzx.student.libbase.app.CrashHandler;
import com.sdzn.fzx.student.libbase.chatroom.NimSDKOptionConfig;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.interfaces.BetaPatchListener;
import com.tencent.tinker.anno.DefaultLifeCycle;
import com.tencent.tinker.entry.DefaultApplicationLike;
import com.tencent.tinker.loader.shareutil.ShareConstants;

import org.xutils.x;

import java.util.Locale;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.external.ExternalAdaptManager;
import me.jessyan.autosize.unit.Subunits;
import rain.coder.photopicker.PhotoPick;

/**
 * AppLike〈一句话功能简述〉
 * 〈功能详细描述〉
 * <p>
 * 接入tinker
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
@SuppressWarnings("unused")
@DefaultLifeCycle(application = "com.sdzn.fzx.student.app.App",
        flags = ShareConstants.TINKER_ENABLE_ALL,
        loadVerifyFlag = false)
public class AppLike extends DefaultApplicationLike {

    public AppLike(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag,
                   long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {

        super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime, tinkerResultIntent);
    }

    /**
     * install multiDex before install tinker
     * so we don't need to put the tinker lib classes in the main dex
     *
     * @param base
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onBaseContextAttached(Context base) {
        super.onBaseContextAttached(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        // 安装tinker
        Beta.installTinker(this);
        //you must install multiDex whatever tinker is installed!
        //MultiDex.install(base);
        //installTinker after load multiDex
        //or you can put com.tencent.tinker.** to main dex
//        TinkerInstaller.install(this,
//                new MyTinkerLoadReporter(getApplication()),
//                new MyTinkerPatchReporter(getApplication()),
//                new DefaultPatchListener(getApplication()),
//                DefaultTinkerResultService.class,
//                new UpgradePatch());
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void registerActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks callback) {
        getApplication().registerActivityLifecycleCallbacks(callback);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        mAppLike = this;
       /* Resources resources = getApplication().getResources();
        if (resources != null) {
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            resources.updateConfiguration(resources.getConfiguration(), displayMetrics);
            Configuration configuration = resources.getConfiguration();
            configuration.fontScale = 1.0f;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }*/
        configRetrofitNet();
        configShipei();
        if (!AndroidUtil.isApkInDebug(getApplication())) {
            initCrashHandler();
            Log.setEnabled(false);
        } else {
            Log.setEnabled(true);
        }
        // customAdaptForExternal();
        //MultiDex.install(getApplication());
        /*
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(getApplication());
        strategy.setAppChannel(BuildConfig2.BUGLY_CHANNEL);
        CrashReport.initCrashReport(getApplication(), "0a0dfa4972", true, strategy);
        Bugly.init(getApplication(), "0a0dfa4972", true);
        CrashReport.setIsDevelopmentDevice(getApplication(), BuildConfig2.VERSION_NAME.endsWith("2"));*/

        // 这里实现SDK初始化，appId替换成你的在Bugly平台申请的appId,调试时将第三个参数设置为true
        String channel = WalleChannelReader.getChannel(getApplication());

        Bugly.setAppChannel(getApplication(), channel);
        // 这里实现SDK初始化，appId替换成你的在Bugly平台申请的appId
        Bugly.init(getApplication(), "20244d9499", true);


        initImageLoader();
        x.Ext.init(getApplication());
        PhotoPick.init(getApplication());

        //注册fileDownloader
        FileDownloader.setup(getApplication());

        initUIKit();
        setStrictMode();
        // 设置是否开启热更新能力，默认为true
        Beta.enableHotfix = true;
        // 设置是否自动下载补丁
        Beta.canAutoDownloadPatch = true;
        // 设置是否提示用户重启
        Beta.canNotifyUserRestart = true;
        // 设置是否自动合成补丁
        Beta.canAutoPatch = true;

        /**
         *  全量升级状态回调
         */
       /* Beta.upgradeStateListener = new UpgradeStateListener() {
            @Override
            public void onUpgradeFailed(boolean b) {

            }

            @Override
            public void onUpgradeSuccess(boolean b) {

            }

            @Override
            public void onUpgradeNoVersion(boolean b) {
                Toast.makeText(getApplication(), "最新版本", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpgrading(boolean b) {
                Toast.makeText(getApplication(), "onUpgrading", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDownloadCompleted(boolean b) {

            }
        };*/

        /**
         * 补丁回调接口，可以监听补丁接收、下载、合成的回调
         */
        Beta.betaPatchListener = new BetaPatchListener() {
            /*补丁下载地址*/
            @Override
            public void onPatchReceived(String patchFileUrl) {
                Toast.makeText(getApplication(), patchFileUrl, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDownloadReceived(long savedLength, long totalLength) {
                Toast.makeText(getApplication(), String.format(Locale.getDefault(),
                        "%s %d%%",
                        Beta.strNotificationDownloading,
                        (int) (totalLength == 0 ? 0 : savedLength * 100 / totalLength)), Toast.LENGTH_SHORT).show();
            }

            /*补丁下载成功*/
            @Override
            public void onDownloadSuccess(String patchFilePath) {
                Toast.makeText(getApplication(), patchFilePath, Toast.LENGTH_SHORT).show();
                Beta.applyDownloadedPatch();
            }

            /*补丁下载失败*/
            @Override
            public void onDownloadFailure(String msg) {
                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁应用成功*/
            @Override
            public void onApplySuccess(String msg) {
                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁应用失败*/
            @Override
            public void onApplyFailure(String msg) {
                Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show();
            }

            /*补丁回滚*/
            @Override
            public void onPatchRollback() {
                Toast.makeText(getApplication(), "onPatchRollback", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }


    private void configShipei() {
        AutoSizeConfig.getInstance().setCustomFragment(true);
        AutoSizeConfig.getInstance().getUnitsManager()
                .setSupportDP(false)
                .setSupportSP(false)
                .setSupportSubunits(Subunits.MM);
        AutoSize.initCompatMultiProcess(getApplication());
    }

    @TargetApi(9)
    protected void setStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
    }

    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplication());
    }

    /**
     * 初始化图片加载器
     */
    private void initImageLoader() {
        ImageLoaderConfiguration configuration = ImageLoaderConfiguration
                .createDefault(getApplication());
        ImageLoader.getInstance().init(configuration);
    }

    private void customAdaptForExternal() {
        /**
         * {@link ExternalAdaptManager} 是一个管理外部三方库的适配信息和状态的管理类, 详细介绍请看 {@link ExternalAdaptManager} 的类注释
         */
        AutoSizeConfig.getInstance().getExternalAdaptManager()

                //加入的 Activity 将会放弃屏幕适配, 一般用于三方库的 Activity, 详情请看方法注释
                //如果不想放弃三方库页面的适配, 请用 addExternalAdaptInfoOfActivity 方法, 建议对三方库页面进行适配, 让自己的 App 更完美一点
//                .addCancelAdaptOfActivity(DefaultErrorActivity.class)

                //为指定的 Activity 提供自定义适配参数, AndroidAutoSize 将会按照提供的适配参数进行适配, 详情请看方法注释
                //一般用于三方库的 Activity, 因为三方库的设计图尺寸可能和项目自身的设计图尺寸不一致, 所以要想完美适配三方库的页面
                //就需要提供三方库的设计图尺寸, 以及适配的方向 (以宽为基准还是高为基准?)
                //三方库页面的设计图尺寸可能无法获知, 所以如果想让三方库的适配效果达到最好, 只有靠不断的尝试
                //由于 AndroidAutoSize 可以让布局在所有设备上都等比例缩放, 所以只要您在一个设备上测试出了一个最完美的设计图尺寸
                //那这个三方库页面在其他设备上也会呈现出同样的适配效果, 等比例缩放, 所以也就完成了三方库页面的屏幕适配
                //即使在不改三方库源码的情况下也可以完美适配三方库的页面, 这就是 AndroidAutoSize 的优势
                //但前提是三方库页面的布局使用的是 dp 和 sp, 如果布局全部使用的 px, 那 AndroidAutoSize 也将无能为力
                //经过测试 DefaultErrorActivity 的设计图宽度在 380dp - 400dp 显示效果都是比较舒服的
                .addCancelAdaptOfActivity(TaskFragment.class);
    }
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

//    @Override
//    public Resources getResources(Resources resources) {
//        if (resources != null) {
//            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
//            Configuration conf = resources.getConfiguration();
//            if (resources.getConfiguration().fontScale != 1.0f) {
//                conf.fontScale = 1.0f;
//            }
//            resources.updateConfiguration(conf, displayMetrics);
//        }
//        return resources;
//    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private void initUIKit() {
        //先初始化


        NIMClient.init(getApplication(), getLoginInfo(), NimSDKOptionConfig.getSDKOptions(getApplication()));
        // crash handler
//        AppCrashHandler.getInstance(this);
        // init pinyin

        if (NIMUtil.isMainProcess(getApplication())) {


        }
    }

    private LoginInfo getLoginInfo() {
//        String account = "18366166528";
//        String token =  "7eafaa28e9aaa152d0c3dacd572340f9";
//
//        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(token)) {
//
//            return new LoginInfo(account, token);
//        } else {
//            return null;
//        }
        return null;
    }

    private SDKOptions buildUIKitOptions() {
        SDKOptions options = new SDKOptions();
        // 设置app图片/音频/日志等缓存目录
        options.sdkStorageRootPath = NimSDKOptionConfig.getAppCacheDir(getApplication()) + "/app";
        return options;
    }

}
