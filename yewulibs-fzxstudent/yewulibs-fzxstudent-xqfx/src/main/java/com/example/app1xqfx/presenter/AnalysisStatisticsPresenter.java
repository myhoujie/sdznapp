package com.example.app1xqfx.presenter;

import com.example.app1xqfx.fragment.StatisticsFragment;
import com.example.app1xqfx.view.AnalysisStatisticsView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.StatisticsGraphView;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.StringBean;
import com.sdzn.fzx.student.vo.TeachStatisticsVo;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/8
 * 修改单号：
 * 修改内容:
 */
public class AnalysisStatisticsPresenter extends BasePresenter<AnalysisStatisticsView, BaseActivity> {
    private List<StatisticsGraphView.ItemInfoVo> itemInfoVoList = new ArrayList<>();

    public void showStudentAssessementHistory(int courseId, int type) {
        int id = StudentSPUtils.getLoginBean().getData().getUser().getId();
        Network.createTokenService(NetWorkService.RuanYun.class)
                .showStudentAssessementHistory(courseId, null, null,
                        id, type)
//              .showStudentAssessementHistory(Config.ruanYunDebug ?1:courseId, null, null,
//                        Config.ruanYunDebug ?1:id, Config.ruanYunDebug ?1:type)
                .map(new StatusFunc<StringBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<StringBean>(new SubscriberListener<StringBean>() {
                    @Override
                    public void onNext(StringBean o) {
                        TeachStatisticsVo teachStatisticsVo = GsonUtil.fromJson(o.getData(), TeachStatisticsVo.class);
                        mView.setTeachStatisticsVo(teachStatisticsVo);
                        List<StatisticsGraphView.LineViewBean> lineViewBeans = dealRightRateList(setRate(teachStatisticsVo));
                        mView.setLineViewBean(lineViewBeans);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
//                                mView.networkError("数据获取失败");
                            }
                        } else {
//                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }

    private TeachStatisticsVo setRate(TeachStatisticsVo teachStatisticsVo) {
        for (int i = 0; i < teachStatisticsVo.getLineChart().size(); i++) {
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("A")) {
                teachStatisticsVo.getLineChart().get(i).setRate(1);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("B")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.8f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("C")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.6f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("D")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.4f);
            }
            if (teachStatisticsVo.getLineChart().get(i).getScore().equals("E")) {
                teachStatisticsVo.getLineChart().get(i).setRate(0.2f);
            }
        }
        return teachStatisticsVo;
    }


    private List<StatisticsGraphView.LineViewBean> dealRightRateList(TeachStatisticsVo bean) {
        if (bean == null)
            return null;

        final List<TeachStatisticsVo.LineChartBean> rightDataList = bean.getLineChart();

        List<StatisticsGraphView.LineViewBean> retList = new ArrayList<>();

        ArrayList<StatisticsGraphView.ItemInfoVo> mylist = new ArrayList<>();

        StatisticsGraphView.LineViewBean myLineBean = new StatisticsGraphView.LineViewBean();

        for (int j = 0; j < rightDataList.size(); j++) {
            StatisticsGraphView.ItemInfoVo myVo = new StatisticsGraphView.ItemInfoVo();
            String day;
            String score;
            String timeId = "0";
            if (timeId.equals(StatisticsFragment.timeIds[0])) {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            } else if (timeId.equals(StatisticsFragment.timeIds[1])) {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            } else {
                day = rightDataList.get(j).getDate();
                score = rightDataList.get(j).getScore();
            }

            myVo.setValue(rightDataList.get(j).getRate());
            myVo.setName(day);
            myVo.setScore(score);
            mylist.add(myVo);
        }
        myLineBean.setItemVo(mylist);
        myLineBean.setColor(StatisticsGraphView.COLOR_COLUM_COLORS[0]);
        myLineBean.setName("我的");

        retList.add(myLineBean);
        return retList;
    }
}
