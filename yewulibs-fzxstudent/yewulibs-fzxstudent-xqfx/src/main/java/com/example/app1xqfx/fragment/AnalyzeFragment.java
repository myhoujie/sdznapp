package com.example.app1xqfx.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app1xqfx.R;
import com.example.app1xqfx.listener.OnAnalyzeListener;
import com.example.app1xqfx.presenter.AnalyzePresenter;
import com.example.app1xqfx.view.AnalyzeView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libpublic.views.SpinerPopWindow;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.ColumStatisticsChat;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.DoubleColumView;
import com.sdzn.fzx.student.libpublic.views.statistic_chat.GraphView;
import com.sdzn.fzx.student.vo.GroupAnalyzeBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lixinbin on 2018/1/8.
 */

public class AnalyzeFragment extends MBaseFragment<AnalyzePresenter> implements AnalyzeView, OnAnalyzeListener, View.OnClickListener {
    private static final String[] time_filter = {"全部时间", "近一周", "近一个月"};
    public static final String[] timeIds = {"3", "1", "2"};

    private RoundAngleImageView photoIcon;
    private TextView userName;
    private TextView groupIcon;
    private TextView groupName;
    private TextView className;
    private TextView spinnerTxt;
    private SmartRefreshLayout refreshLayout;
    private ColumStatisticsChat subjectAnalyzeView;
    private GraphView timeAnalyzeView;
    private LinearLayout totalRankLy;
    private TextView totalRankTxt;
    private LinearLayout taskRankLy;
    private TextView taskRankTxt;
    private LinearLayout classRankLy;
    private TextView classRankTxt;
    private DoubleColumView groupAnalyzeView;
    private SpinerPopWindow window;

    public static AnalyzeFragment newInstance(Bundle bundle) {
        AnalyzeFragment fragment = new AnalyzeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new AnalyzePresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_analyze, container, false);
        photoIcon = (RoundAngleImageView) rootView.findViewById(R.id.photo_icon);
        userName = (TextView) rootView.findViewById(R.id.user_name);
        groupIcon = (TextView) rootView.findViewById(R.id.group_icon);
        groupName = (TextView) rootView.findViewById(R.id.group_name);
        className = (TextView) rootView.findViewById(R.id.class_name);
        spinnerTxt = (TextView) rootView.findViewById(R.id.spinner_txt);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        subjectAnalyzeView = (ColumStatisticsChat) rootView.findViewById(R.id.subject_analyze_view);
        timeAnalyzeView = (GraphView) rootView.findViewById(R.id.time_analyze_view);
        totalRankLy = (LinearLayout) rootView.findViewById(R.id.total_rank_ly);
        totalRankTxt = (TextView) rootView.findViewById(R.id.total_rank_txt);
        taskRankLy = (LinearLayout) rootView.findViewById(R.id.task_rank_ly);
        taskRankTxt = (TextView) rootView.findViewById(R.id.task_rank_txt);
        classRankLy = (LinearLayout) rootView.findViewById(R.id.class_rank_ly);
        classRankTxt = (TextView) rootView.findViewById(R.id.class_rank_txt);
        groupAnalyzeView = (DoubleColumView) rootView.findViewById(R.id.group_analyze_view);

        spinnerTxt.setOnClickListener(this);
        initView();
        initData();
        return rootView;
    }

    private void initView() {
        window = new SpinerPopWindow(getActivity());
        window.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                spinnerTxt.setText(time_filter[pos]);
                onChangeTime(pos);
            }
        });
        window.setPopDatas(time_filter);
        subjectAnalyzeView.setItemClickListener(new ColumStatisticsChat.ItemChangedListener() {
            @Override
            public void onItemChanged(ColumStatisticsChat.ItemInfoVo pos) {
                mPresenter.getGroupAnalyzeData(pos.getId());
                mPresenter.getRightRateStatictis(pos.getId());
            }
        });

        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                mPresenter.getSubjectStatictis();
            }
        });
    }

    private void initData() {
        final LoginBean.DataBean.UserBean user = UserController.getLoginBean().getData().getUser();
        Glide.with(getActivity())
                .load(user.getPhoto())
                .bitmapTransform(new CircleTransform(getContext()))
                .into(photoIcon);

        if (!TextUtils.isEmpty(user.getRealName()))
            userName.setText(user.getRealName());
        if (!TextUtils.isEmpty(user.getClassGroupName())) {
            groupName.setText(user.getClassGroupName());
            groupIcon.setText(user.getClassGroupName().substring(0, 1));
        }

        if (!TextUtils.isEmpty(user.getBaseGradeName()) && !TextUtils.isEmpty(user.getClassName()))
            className.setText(user.getBaseGradeName() + user.getClassName());

        onChangeTime(0);
    }

    private void onChangeTime(int pos) {
        mPresenter.setTimeId(timeIds[pos]);
        mPresenter.getSubjectStatictis();
    }

    @Override
    public void onResume() {
        final LoginBean.DataBean.UserBean user = UserController.getLoginBean().getData().getUser();
        Glide.with(getActivity())
                .load(user.getPhoto())
                .bitmapTransform(new CircleTransform(getContext()))
                .into(photoIcon);
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.spinner_txt) {
            window.showAsDropDown(spinnerTxt);
        }
    }
    /*@OnClick(R.id.spinner_txt)
    public void onViewClicked() {
        window.showAsDropDown(spinnerTxt);
    }*/

    @Override
    public void onSubjectSuccess(List<ColumStatisticsChat.ItemInfoVo> retList) {
        refreshLayout.finishRefresh(true);
        subjectAnalyzeView.setDatas(retList);
    }

    @Override
    public void onSubjectFailed() {
        refreshLayout.finishRefresh(false);
    }

    @Override
    public void onGroupSuccess(List<DoubleColumView.ItemInfoVo> list, GroupAnalyzeBean.MyGroupInfo myGroupInfo) {
        groupAnalyzeView.setData(list);
        if (myGroupInfo == null) {
            totalRankLy.setVisibility(View.GONE);
            taskRankLy.setVisibility(View.GONE);
            classRankLy.setVisibility(View.GONE);
        } else {
            totalRankLy.setVisibility(View.VISIBLE);
            taskRankLy.setVisibility(View.VISIBLE);
            classRankLy.setVisibility(View.VISIBLE);
            totalRankTxt.setText(myGroupInfo.getTotalRank());
            taskRankTxt.setText(myGroupInfo.getTaskRank());
            classRankTxt.setText(myGroupInfo.getClassRank());
        }


    }

    @Override
    public void onGroupFail() {
        groupAnalyzeView.setData(new ArrayList<DoubleColumView.ItemInfoVo>());
        totalRankLy.setVisibility(View.GONE);
        taskRankLy.setVisibility(View.GONE);
        classRankLy.setVisibility(View.GONE);
    }

    @Override
    public void onRightRateSuccess(List<GraphView.LineViewBean> list) {
        if (list != null) {
            timeAnalyzeView.setData(list);
        }
    }

    @Override
    public void onRightRateFailed() {
        timeAnalyzeView.setData(new ArrayList<GraphView.LineViewBean>());
    }

    @Override
    public void onSubjectItemClick(int position) {

    }

    @Override
    public void onSearch(String searchStr) {

    }

    @Override
    public void onStatusChanged(int status) {

    }

    @Override
    public void onSubjectGet(List<SubjectVo.DataBean> subjects) {

    }

    @Override
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {

    }
}
