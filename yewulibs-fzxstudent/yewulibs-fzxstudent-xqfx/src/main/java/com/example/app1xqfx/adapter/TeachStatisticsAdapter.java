package com.example.app1xqfx.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app1xqfx.R;

import java.util.List;


/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/11
 * 修改单号：
 * 修改内容:
 */
public class TeachStatisticsAdapter extends BaseAdapter {

    private List<String> mlist;
    private Context context;
    private static boolean flag = true;

    public TeachStatisticsAdapter(List<String> mlist, Context context) {
        this.mlist = mlist;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_teach_statistics, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // 奇偶行显示不同颜色
        if (flag) {
            flag = (position % 2) == 0;
            holder.name.setBackgroundResource(R.color.color_f3f5f8);
        } else {
            flag = (position % 2) == 1;
            holder.name.setBackgroundResource(R.color.white);
        }
        holder.name.setText("(" + (position + 1) + ")   " + mlist.get(position));
        return convertView;
    }

    static class ViewHolder {
       /* @BindView(R.id.name)
        TextView name;
        @BindView(R.id.ll_answer)
        LinearLayout llAnswer;
        @BindView(R.id.rl_teach)
        RelativeLayout rlteach;*/

        private LinearLayout llAnswer;
        private RelativeLayout rlTeach;
        private TextView name;

        ViewHolder(View view) {
            llAnswer = (LinearLayout) view.findViewById(R.id.ll_answer);
            rlTeach = (RelativeLayout) view.findViewById(R.id.rl_teach);
            name = (TextView) view.findViewById(R.id.name);
        }
    }
}
