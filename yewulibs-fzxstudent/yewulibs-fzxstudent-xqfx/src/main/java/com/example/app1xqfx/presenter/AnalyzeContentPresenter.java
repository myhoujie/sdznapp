package com.example.app1xqfx.presenter;

import com.example.app1xqfx.view.AnalyzeContentView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class AnalyzeContentPresenter extends BasePresenter<AnalyzeContentView, BaseActivity> {


    public void getSubjects() {
        Map<String, String> params = new HashMap<>();
        params.put("baseGradeId", UserController.getBaseGradeId());
        params.put("flag", "true");
        Subscription subscribe = Network.createTokenService(NetWorkService.TaskService.class)
                .getSubjects(params)
                .map(new StatusFunc<SubjectVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<SubjectVo>(new SubscriberListener<SubjectVo>() {
                    @Override
                    public void onNext(SubjectVo subjectVo) {
                        mView.onSubjectSuccess(subjectVo.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity));
        subscriptions.add(subscribe);
    }
}
