package com.example.app1xztl.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.app1xztl.R;
import com.example.app1xztl.activity.FullImageActivity;
import com.sdzn.fzx.student.libpublic.utils.chatroom.GlideHelper;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

import java.util.List;

/**
 *
 */

public class EllGridViewAdapter extends BaseAdapter {
    private Context context;
    private List<GroupListBean.DataBean.TeachGroupChatResultPicsBean> list;
    LayoutInflater layoutInflater;


    public EllGridViewAdapter(Context context, List<GroupListBean.DataBean.TeachGroupChatResultPicsBean> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {//注意此处
        return list.size() ;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.item_discuss_ell_grid, null);
        ImageView imageView = convertView.findViewById(R.id.image);
        GlideHelper.load(context,list.get(position).getPicUrl(),imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentImage = new Intent(context, FullImageActivity.class);
                intentImage.putExtra("photoUrl", list.get(position).getPicUrl());
                context.startActivity(intentImage);
            }
        });

        return convertView;
    }

}
