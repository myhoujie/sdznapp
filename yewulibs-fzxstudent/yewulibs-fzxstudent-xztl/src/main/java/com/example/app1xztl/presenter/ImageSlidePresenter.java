package com.example.app1xztl.presenter;


import com.example.app1xztl.view.ImageSlideView;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/15 .
 */

public class ImageSlidePresenter extends BasePresenter<ImageSlideView, BaseActivity> {
    /**
     * 网络请求  删除myself的作答结果
     */
    public void getDelete(int id){

            Network.createTokenService(NetWorkService.ChatService.class)
                    .getDeleteResult(String.valueOf(id))
                    .map(new StatusFunc<Object>())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                        @Override
                        public void onCompleted() {

                            mActivity.finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.networkError(e.toString());
                        }

                        @Override
                        public void onNext(Object o) {
                            mView.getDeleteResult();

                        }
                    },mActivity,true,false,false,""));


        //成功
//        mView.getDeleteResult(true);

    }


}
