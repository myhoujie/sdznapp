package com.example.app1xztl.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1xztl.R;
import com.sdzn.fzx.student.vo.chatroombean.Home;

import java.util.List;

/**
 * Created by admin on 2019/8/16.
 */

public class OtherGridAdapter extends BaseAdapter {
    private Context context;
    List<Home> list;

    public OtherGridAdapter(Context context) {
        this.context = context;
    }

    public OtherGridAdapter(Context context, List<Home> list) {
        this.context = context;
        this.list = list;
    }
    public void setListData(List<Home> list){
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null){
            view = View.inflate(context, R.layout.item_discuss_grid,null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }
        else {
            viewHolder= (ViewHolder) view.getTag();
        }
        viewHolder.tvName.setText(list.get(i).getName());
//        GlideHelper.load(true,context,list.get(i).getUrl(),viewHolder.ivHead);

        return view;
    }
    class ViewHolder {
        protected ImageView ivHead;
        protected TextView tvName;

        public ViewHolder(View convertView) {
            ivHead = (ImageView) convertView.findViewById(R.id.iv_head);
            tvName = (TextView) convertView.findViewById(R.id.tv_name);
        }
    }


}
