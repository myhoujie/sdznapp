package com.example.app1xztl.adapter.holder;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.ChatAdapter;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.sdzn.fzx.student.libpublic.utils.chatroom.ImageUrlUtil;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libpublic.views.chatroom_widget.BubbleImageView;


/**
 * 作者：Rance on 2016/11/29 10:47
 * 邮箱：rance935@163.com
 */
public class ChatAcceptViewHolder extends BaseViewHolder<ChatRoomMessage> {

   /* @BindView(R.id.tv)
    TextView textView;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.chat_item_content_image)
    ImageView chatItemContentImage;
    @BindView(R.id.ll_tv)
    LinearLayout llTv;*/

    private TextView chatItemDate;
    private RoundAngleImageView chatItemHeader;
    private TextView tvName;
    private LinearLayout llTv;
    private TextView textView;
    private BubbleImageView chatItemContentImage;


    private ChatAdapter.onItemClickListener onItemClickListener;
    private Handler handler;
    private String path = "";

    public ChatAcceptViewHolder(ViewGroup parent, ChatAdapter.onItemClickListener onItemClickListener, Handler handler) {
        super(parent, R.layout.item_chat_accept);
        chatItemDate = (TextView) itemView.findViewById(R.id.chat_item_date);
        chatItemHeader = (RoundAngleImageView) itemView.findViewById(R.id.chat_item_header);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        llTv = (LinearLayout) itemView.findViewById(R.id.ll_tv);
        textView = (TextView) itemView.findViewById(R.id.tv);
        chatItemContentImage = (BubbleImageView) itemView.findViewById(R.id.chat_item_content_image);

        this.onItemClickListener = onItemClickListener;
        this.handler = handler;
    }

    @Override
    public void setData(ChatRoomMessage data) {
        if (data.getMsgType() == MsgTypeEnum.text) {
            tvName.setText(data.getRemoteExtension().get("chat_name").toString());
            textView.setText(data.getContent());
            textView.setVisibility(View.VISIBLE);
            llTv.setVisibility(View.VISIBLE);
            chatItemContentImage.setVisibility(View.GONE);
        } else if (data.getMsgType() == MsgTypeEnum.image) {
            tvName.setText(data.getRemoteExtension().get("chat_name").toString());
            textView.setVisibility(View.GONE);
            llTv.setVisibility(View.GONE);
            chatItemContentImage.setVisibility(View.VISIBLE);
            path = ImageUrlUtil.getImagetUrl(data.getAttachStr());

            Glide.with(getContext()).load(path).override(240, 300).into(chatItemContentImage);
            chatItemContentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onImageClick(chatItemContentImage, getDataPosition());
                }
            });
        }
    }
}
