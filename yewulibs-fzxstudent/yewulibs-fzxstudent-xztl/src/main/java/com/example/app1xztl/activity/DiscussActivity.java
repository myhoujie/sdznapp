package com.example.app1xztl.activity;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.EllGridViewAdapter;
import com.example.app1xztl.fragment.MyChatRoomMessageFragment;
import com.example.app1xztl.listener.OnDiscussListener;
import com.example.app1xztl.presenter.DiscussPresenter;
import com.example.app1xztl.view.DiscussView;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomStatusChangeData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.utils.GlideImageLoader;
import com.sdzn.fzx.student.libpublic.utils.chatroom.ExpandableLinearLayout;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.views.NoScrollGridView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.SoftKeyBoardListener;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.chatroom.SpacesItemVeiticalDecoration;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;
import com.sdzn.fzx.student.vo.chatroombean.ImageSlideBean;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;


/**
 * 小组讨论界面
 */
public class DiscussActivity extends MBaseActivity<DiscussPresenter> implements DiscussView, View.OnClickListener, OnDiscussListener, OnRefreshListener {
    private RelativeLayout relContent;
    private RelativeLayout rlTop;
    private TextView mTvBack;
    private ImageView imMember;
    private TextView tvTitle;
    private View line;
    private RelativeLayout rlResult;
    private RelativeLayout llDiscussResult;
    private LinearLayout llResult;
    private TextView tvResult;
    private TextView tvResultSend;
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rv;
    private LinearLayout llEmpty;
    private TextView tvEmpty;
    private View viewLine;
    private LinearLayout llChat;
    private LinearLayout llHead;
    private ExpandableLinearLayout ellTop;
    private ImageView ivArrow;
    private FrameLayout chatRoomMessageFragment;
    private LinearLayout llArrowTwo;
    private ImageView ivArrowTwo;

    private boolean isResultSend = false;//右下角发送
    private boolean isResult = false;//false  能发送   true 跳转到其他小组
    //右侧 列表刷新
    private Y_MultiRecyclerAdapter adapter;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private static int DISCUSS_OLD = 1;
    private static int DISCUSS_NOW = 0;
    public static final int REQ_CODE_CAMERA = 1003;
    /**
     * 子页面
     */
    private MyChatRoomMessageFragment messageFragment;
    private AbortableFuture<EnterChatRoomResultData> enterRequest;

    private boolean hasEnterSuccess = false; // 是否已经成功登录聊天室
    private String roomId = "117795375";
    private EllGridViewAdapter ellGridViewAdapter;

    @Override
    public void initPresenter() {
        mPresenter = new DiscussPresenter();
        mPresenter.attachView(this, this);
    }

    private Bundle bundle;
    private String title, content, endTime, createTime = "2019-09-11 13:50:09";
    private int groupChatTaskId, id, state, isRead;
    private String picJsonStr = "";
    private float score;
    private ArrayList<GroupListBean.DataBean.TeachGroupChatResultPicsBean> pics = new ArrayList<>();
    private static final int REQUECT_CODE_CAMERA = 1100;
    private static final int IS_READ = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_discuss);
        relContent = (RelativeLayout) findViewById(R.id.rel_content);
        rlTop = (RelativeLayout) findViewById(R.id.rl_top);
        mTvBack = (TextView) findViewById(R.id.tvBack);
        imMember = (ImageView) findViewById(R.id.imMember);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        line = (View) findViewById(R.id.line);
        rlResult = (RelativeLayout) findViewById(R.id.rl_result);
        llDiscussResult = (RelativeLayout) findViewById(R.id.ll_discuss_result);
        llResult = (LinearLayout) findViewById(R.id.llResult);
        tvResult = (TextView) findViewById(R.id.tvResult);
        tvResultSend = (TextView) findViewById(R.id.tv_result_send);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refreshLayout);
        rv = (EmptyRecyclerView) findViewById(R.id.rv);
        llEmpty = (LinearLayout) findViewById(R.id.llEmpty);
        tvEmpty = (TextView) findViewById(R.id.tvEmpty);
        viewLine = (View) findViewById(R.id.view_line);
        llChat = (LinearLayout) findViewById(R.id.ll_chat);
        llHead = (LinearLayout) findViewById(R.id.ll_head);
        ellTop = (ExpandableLinearLayout) findViewById(R.id.ell_product);
        ivArrow = (ImageView) findViewById(R.id.iv_discuss_arrow);
        chatRoomMessageFragment = (FrameLayout) findViewById(R.id.chat_room_message_fragment);
        llArrowTwo = (LinearLayout) findViewById(R.id.ll_arrow_two);
        ivArrowTwo = (ImageView) findViewById(R.id.iv_discuss_arrow_two);

        bundle = getIntent().getExtras();
        roomId = bundle.getString("roomId");
        id = bundle.getInt("id");
        isRead = bundle.getInt("isRead");
        state = bundle.getInt("state");
        groupChatTaskId = bundle.getInt("groupChatTaskId");
        title = bundle.getString("title");
        content = bundle.getString("content");
        endTime = bundle.getString("endTime");
        createTime = bundle.getString("createTime");
        score = bundle.getFloat("score");
        pics = bundle.getParcelableArrayList("pic");
        if (state == DISCUSS_NOW) {
            isResult = false;
        } else if (state == DISCUSS_OLD) {
            isResult = true;
            llResult.setVisibility(View.VISIBLE);
            tvResult.setText(String.valueOf((int) score));

            tvResultSend.setText("讨论结果");
            tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send_no));
        }
        if (isRead == IS_READ) {
            mPresenter.getChatIsRead(String.valueOf(id));
        }

        // 注册监听
        registerObservers(true);
        initView();
        if (!isResult) {
            login();
        }
        initEllView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(DiscussActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getChatPicData();
    }

    @Override
    protected void initView() {
        mTvBack.setOnClickListener(this);
        tvResultSend.setOnClickListener(this);
        imMember.setOnClickListener(this);
        //右侧 列表刷新
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        rv.setEmptyView(llEmpty);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(App2.get());
        rv.setLayoutManager(linearLayoutManager);
        adapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        rv.setAdapter(adapter);
        rv.addItemDecoration(new SpacesItemVeiticalDecoration(10));
        rv.addOnItemTouchListener(new OnItemTouchListener(rv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
            }
        });

        adapter.notifyDataSetChanged();


        SoftKeyBoardListener.setListener(DiscussActivity.this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {

            @Override
            public void onKeyBoardShow(int height) {
                ellImageHeight = height;
//                Toast.makeText(DiscussActivity.this, "键盘显示 高度" + height, Toast.LENGTH_SHORT).show();
                isBoardShow = true;
                if (false == isEllTop) {
                    //找到一个布局
//                    ImageView iv_discuss_arrow_two = findViewById(R.id.iv_discuss_arrow_two);
//                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    lp.setMargins(0, height - (44 * 2), 0, 0);
//                    iv_discuss_arrow_two.setLayoutParams(lp);
//                    llArrowTwo.setVisibility(View.VISIBLE);
                    setEllImage(ellImageHeight);
                } else {
                    ellTop.toggle();


                    setEllImage(ellImageHeight);
//                    setEllImage(0);
                }
            }

            @Override
            public void onKeyBoardHide(int height) {
                isBoardShow = false;
                setEllImage(0);
            }
        });

    }

    private void setEllImage(int height) {
        ImageView iv_discuss_arrow = findViewById(R.id.iv_discuss_arrow);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (height > 0) {
            lp.setMargins(0, height - (44 + 48), 0, 0);
        } else {
            lp.setMargins(0, 0, 0, 0);
        }
        iv_discuss_arrow.setLayoutParams(lp);
    }

    /**
     * 隐藏键盘
     */
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

    }

    private boolean isBoardShow = false;
    private int ellImageHeight = 0;

    @Override
    protected void initData() {

    }

    /**
     * 头部展示view  可收起
     */

    private void initEllView() {
        ellTop.removeAllViews();//清除所有的子View（避免重新刷新数据时重复添加）
        View view = View.inflate(this, R.layout.view_discuss_ell_top, null);
        TextView tvTitleEll = view.findViewById(R.id.tv_title);
        TextView tvContentEll = view.findViewById(R.id.tv_content);
        TextView tvDateEll = view.findViewById(R.id.tv_date);
        TextView tvStatus = view.findViewById(R.id.tvStatus);
        NoScrollGridView gridView = view.findViewById(R.id.gridView);
        if (pics.size() > 0) {
            ellGridViewAdapter = new EllGridViewAdapter(DiscussActivity.this, pics);
            gridView.setAdapter(ellGridViewAdapter);

        }
        if (isResult) {
            tvStatus.setVisibility(View.GONE);
        } else {
            tvStatus.setVisibility(View.VISIBLE);
        }
        tvTitleEll.setText(title);
        tvContentEll.setText(content);
        tvDateEll.setText("截止时间：" + endTime);

        ellTop.addItem(view);
        ellTop.toggle();//初始 展开
        ivArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBoardShow == true && isEllTop == false) {
                    hideInput();
                }
                ellTop.toggle();
            }
        });
        ellTop.setOnStateChangeListener(new ExpandableLinearLayout.OnStateChangeListener() {
            @Override
            public void onStateChanged(boolean isExpanded) {
                isEllTop = isExpanded;
                if (isExpanded == false && isBoardShow == true) {
                    setEllImage(ellImageHeight);
                } else {
                    setEllImage(0);
                }
            }
        });

        /*独立出来的*/
        ivArrowTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ellTop.toggle();
            }
        });
    }

    private boolean isEllTop = true;

    private void initMessageFragment() {
//        messageFragment = (ChatRoomMessageFragment) getSupportFragmentManager().findFragmentById(R.id.chat_room_message_fragment);
        messageFragment = new MyChatRoomMessageFragment();
        if (messageFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.chat_room_message_fragment, messageFragment);
            transaction.commit();
            messageFragment.init(roomId, DateUtil.getTimeMillisByStr(createTime, "yyyy-MM-dd HH:mm:ss"));
        } else {
            // 如果Fragment还未Create完成，延迟初始化
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    initMessageFragment();
                }
            }, 50);
        }
    }

    private void enterRoom() {
        hasEnterSuccess = false;
        EnterChatRoomData data = new EnterChatRoomData(roomId);
        enterRequest = NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 4);
        enterRequest.setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
//                NimUIKit.enterChatRoomSuccess(result, false);
                initMessageFragment();
                ToastUtil.showShortlToast("进入聊天室");
            }

            @Override
            public void onFailed(int code) {

                if (code == ResponseCode.RES_CHATROOM_BLACKLIST) {
                    ToastUtil.showShortlToast("你已被拉入黑名单，不能再进入");
                } else if (code == ResponseCode.RES_ENONEXIST) {
                    ToastUtil.showShortlToast("聊天室不存在");
                } else if (code == ResponseCode.RES_EXCEPTION) {
                    if (hasEnterSuccess) {
                        hasEnterSuccess = false;
                        enterRoom();
                    } else {
                        ToastUtil.showShortlToast("聊天室登录失败");
                    }
                } else {
                    ToastUtil.showShortlToast("聊天室登录失败, " + code);
                }
                initMessageFragment();

            }

            @Override
            public void onException(Throwable exception) {
                ToastUtil.showShortlToast("聊天室登录失败, e=" + exception.getMessage());
                initMessageFragment();
            }
        });
    }


    private void registerObservers(boolean register) {
        NIMClient.getService(ChatRoomServiceObserver.class).observeOnlineStatus(onlineStatus, register);
        NIMClient.getService(ChatRoomServiceObserver.class).observeKickOutEvent(kickOutObserver, register);
    }

    private void logoutChatRoom() {
        NIMClient.getService(ChatRoomService.class).exitChatRoom(roomId);
        onExitedChatRoom();
    }

    public void onExitedChatRoom() {
        NIMClient.getService(AuthService.class).logout();
    }

    Observer<ChatRoomStatusChangeData> onlineStatus = new Observer<ChatRoomStatusChangeData>() {
        @Override
        public void onEvent(ChatRoomStatusChangeData chatRoomStatusChangeData) {
            if (!chatRoomStatusChangeData.roomId.equals(roomId)) {
                return;
            }
            if (chatRoomStatusChangeData.status == StatusCode.UNLOGIN) {
                int errorCode = NIMClient.getService(ChatRoomService.class).getEnterErrorCode(roomId);
                // 如果遇到错误码13001，13002，13003，403，404，414，表示无法进入聊天室，此时应该调用离开聊天室接口。
                if (errorCode != 13001 || errorCode != 13002 || errorCode != 403 || errorCode != 404 || errorCode != 414) {
                    enterRoom();
                }
            }


        }
    };

    Observer<ChatRoomKickOutEvent> kickOutObserver = new Observer<ChatRoomKickOutEvent>() {
        @Override
        public void onEvent(ChatRoomKickOutEvent chatRoomKickOutEvent) {

            onExitedChatRoom();
        }
    };

    /**
     * 获取成员列表 成员
     */
    private void getMember() {
        // 以游客为例，从最新时间开始，查询30条
        NIMClient.getService(ChatRoomService.class).fetchRoomMembers(roomId, MemberQueryType.ONLINE_NORMAL, 0, 30).setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int code, List<ChatRoomMember> result, Throwable exception) {

            }
        });
    }


    private void onLoginDone() {
        enterRequest = null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        logoutChatRoom();
    }

    private void login() {
        LoginInfo info = new LoginInfo(UserController.getLoginBean().getData().getUser().getAccountName()
                , UserController.getLoginBean().getData().getUser().getAccountName());//MD5.md5("");
        RequestCallback<LoginInfo> callback =
                new RequestCallback<LoginInfo>() {
                    @Override
                    public void onSuccess(LoginInfo loginInfo) {
                        enterRoom();
                        hasEnterSuccess = true;
                    }

                    @Override
                    public void onFailed(int i) {
                        if (i == 302 || i == 404) {
                            ToastUtil.showShortlToast(getResources().getString(R.string.login_failed));
                        }
                        if (i == ResponseCode.RES_EXCEPTION) {
                            if (!hasEnterSuccess) {
                                login();
                                hasEnterSuccess = true;
                            }
                        } else if (i == ResponseCode.RES_EEXIST) {
                            onExitedChatRoom();
                            if (!hasEnterSuccess) {
                                login();
                                hasEnterSuccess = true;
                            }
                        }
                        ToastUtil.showShortlToast("登录聊天室失败");
                        initMessageFragment();
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        ToastUtil.showShortlToast(getResources().getString(R.string.login_exception));
                        initMessageFragment();
                    }
                    // 可以在此保存LoginInfo到本地，下次启动APP做自动登录用
                };
        NIMClient.getService(AuthService.class).login(info)
                .setCallback(callback);
    }

    @Override
    public void onBackPressed() {
        if (messageFragment == null) {
            super.onBackPressed();
        }
        logoutChatRoom();
        DiscussActivity.this.finish();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            logoutChatRoom();
            DiscussActivity.this.finish();

        } else if (i == R.id.tv_result_send) {
            if (isResult) {//跳转
                Intent intent = new Intent(DiscussActivity.this, DiscussOtherActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("groupChatTaskId", groupChatTaskId);
                intent.putExtras(bundle);
                startActivity(intent);
                startActivity(intent);

            } else {//send
                isResultSend = true;
                if (isIntentPop) {
                    mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
                        @Override
                        public void selectAlbum() {
                            new PhotoPickConfig.Builder(mPresenter.mActivity)
                                    .imageLoader(new GlideImageLoader())
                                    .showCamera(false)
                                    .maxPickSize(selectAlbumNum)
                                    .spanCount(8)
                                    .clipPhoto(false)
                                    .mustClip(false)
                                    .build();
                        }

                        @Override
                        public void selectCamera() {
                            requestPermissiontest();
                        }
                    });
                }
            }

        } else if (i == R.id.imMember) {//                getMember();
            Intent intent = new Intent(this, MemberActivity.class);
            startActivity(intent);

        } else {
        }
    }

    private void resultSend(boolean isResult, List<Photo> paths) {
        if (isResult) {//true
            isResultSend = false;
            mPresenter.uploadSubjectivePhoto(paths);
        }
    }

    @Override
    protected void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (isResultSend) {
            if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {//相册返回图片  图片选择器
                if (resultCode == Activity.RESULT_OK) {
                    if (data.getBooleanExtra("isClip", false)) {
                        String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                        List<Photo> paths = new ArrayList<>();
                        Photo photo = new Photo();
                        photo.setPath(path);
                        paths.add(photo);
                        resultSend(isResultSend, paths);
                    } else {
                        ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                        if (photoLists != null && !photoLists.isEmpty()) {
                            resultSend(isResultSend, photoLists);
                        }
                    }

                }
            } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
                if (resultCode == Activity.RESULT_OK) {
                    mPresenter.startClipPic(false);
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (resultCode == Activity.RESULT_OK) {

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(UCrop.getOutput(data));
                    paths.add(photo);
                    resultSend(isResultSend, paths);
                }
            }
        } else {
            if (messageFragment != null) {
                messageFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
/* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        保证fragment 能调用 onActivityResult
        if (isResultSend) {
            if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {//相册返回图片  图片选择器
                if (resultCode == Activity.RESULT_OK) {
                    if (data.getBooleanExtra("isClip", false)) {
                        String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                        List<Photo> paths = new ArrayList<>();
                        Photo photo = new Photo();
                        photo.setPath(path);
                        paths.add(photo);
                        resultSend(isResultSend, paths);
                    } else {
                        ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                        if (photoLists != null && !photoLists.isEmpty()) {
                            resultSend(isResultSend, photoLists);
                        }
                    }

                }
            } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
                if (resultCode == Activity.RESULT_OK) {
                    mPresenter.startClipPic(false);
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (resultCode == Activity.RESULT_OK) {

                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(UCrop.getOutput(data));
                    paths.add(photo);
                    resultSend(isResultSend, paths);
                }
            }
        } else {
            if (messageFragment != null) {
                messageFragment.onActivityResult(requestCode, resultCode, data);
            }
        }

    }*/

    /**
     * myChatFragment--->发过来
     * 涉及图片地址处理
     */
    @Override
    public void setResultSendStatus() {
        isResultSend = false;

    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getChatPicData();
        cancilLoadState();
    }


    /**
     * 讨论结果上传图片 返回结果
     */
    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);

    }

    /**
     * 此处刷新 或 重新加载接口
     */
    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) { //pic
        if (uploadVos.getData().size() > 0) {
            List<String> insertList = new ArrayList<>();
            for (int i = 0; i < uploadVos.getData().size(); i++) {
                insertList.add(uploadVos.getData().get(i).getOriginalPath());
            }
            picJsonStr = buildChatPic(insertList).toString();
            insertPic();
        }
    }

    private void insertPic() {
        Map<String, Object> parm = new HashMap<>();
        parm.put("groupChatTaskPicJson", picJsonStr);
        parm.put("groupChatTaskId", groupChatTaskId);
        parm.put("groupChatResultId", id);
        parm.put("groupId", UserController.getLoginBean().getData().getUser().getClassGroupId());
        parm.put("groupName", UserController.getLoginBean().getData().getUser().getClassGroupName());
        mPresenter.insertChatPic(parm);
    }

    private JSONArray buildChatPic(List<String> mDatas) {
        JSONArray jsonArray = new JSONArray();

        for (String url : mDatas) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("pic", url);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    /**
     * 插入图片  成功
     */
    @Override
    public void returnSuccess() {
        refreshLayout.autoRefresh();
    }

    private void getChatPicData() {
        Map<String, Object> parm = new HashMap<>();
        parm.put("groupChatTaskId", groupChatTaskId);
        parm.put("groupChatResultId", id);
        parm.put("groupId", UserController.getLoginBean().getData().getUser().getClassGroupId());
        mPresenter.getChatPic(parm);
    }

    private int selectAlbumNum = 2;
    private boolean isIntentPop = true;

    @Override
    public void getChatPic(final DiscussionPicList picList) {
        itemEntityList.clear();
        if (picList.getData().size() > 0) {
            int myCount = 0;
            final ArrayList<ImageSlideBean> picsBeanList = new ArrayList<>();
            for (int i = 0; i < picList.getData().size(); i++) {
                picsBeanList.add(new ImageSlideBean(picList.getData().get(i).getId(),
                        picList.getData().get(i).getPicUrl(), picList.getData().get(i).getGroupId(), String.valueOf(picList.getData().get(i).getUserStudentId())));
                if (picList.getData().get(i).getGroupId().equals(UserController.getLoginBean().getData().getUser().getClassGroupId())
                        && picList.getData().get(i).getUserStudentId() == UserController.getLoginBean().getData().getUser().getId()) {
                    myCount++;
                }
            }
            if (state == DISCUSS_NOW) {
                if (myCount > 1) {//当多余两张时不能在发送
                    isIntentPop = false;
                    tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send_no));
                } else {
                    isIntentPop = true;
                    if (myCount > 0) {
                        selectAlbumNum = 1;
                    } else {
                        selectAlbumNum = 2;
                    }
                    tvResultSend.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_discuss_result_send));
                }
            }
            itemEntityList.addItems(R.layout.item_discuss_result_recycler, picList.getData())
                    .addOnBind(R.layout.item_discuss_result_recycler, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindHolder(rv.getHeight(), holder, (DiscussionPicList.DataBean) itemData, picsBeanList, isResult);
                        }
                    });
//        refreshLayout.setEnableLoadMore(false);
            adapter.notifyDataSetChanged();
            cancilLoadState();

        } else {
            rv.setEmptyView(llEmpty);
            isIntentPop = true;
            selectAlbumNum = 2;

        }
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    /**
     * 相机权限
     */
    public void requestPermissiontest() {
//        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
//        EasyPermissions.requestPermissions(this, "请设置相机权限",
//                REQUECT_CODE_CAMERA, perms);
        methodRequiresTwoPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @AfterPermissionGranted(REQUECT_CODE_CAMERA)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            mPresenter.toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置相机权限",
                    REQUECT_CODE_CAMERA, perms);
        }
    }
//
//    @PermissionGrant(REQUECT_CODE_CAMERA)
//    public void requestSdcardSuccess() {
//        mPresenter.toSystemCamera();
//    }
//
//    @PermissionDenied(REQUECT_CODE_CAMERA)
//    public void requestSdcardFailed() {
//        ToastUtil.showShortlToast("请设置相机权限");
//    }


}
