package com.example.app1xztl.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1xztl.R;
import com.example.app1xztl.activity.ImageSlideActivity;
import com.sdzn.fzx.student.libpublic.views.chatroom_widget.CustomGridView;
import com.sdzn.fzx.student.libutils.util.chatroom.SpacesItemThreeDecoration;
import com.sdzn.fzx.student.vo.chatroombean.ChatOtherBean;
import com.sdzn.fzx.student.vo.chatroombean.Home;
import com.sdzn.fzx.student.vo.chatroombean.ImageSlideBean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class OtherRecyclerViewAdapter extends RecyclerView.Adapter<OtherRecyclerViewAdapter.ItemViewHolder> {

    private Context mContext;
    private List<ChatOtherBean.DataBean> mDatas;
    private ItemViewHolder holder;
    private OtherItemRecyclerViewAdapter mOtherItemRecyclerViewAdapter;

    public OtherRecyclerViewAdapter(Context pContext, List<ChatOtherBean.DataBean> data) {
        this.mContext = pContext;
        this.mDatas = data;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        RecyclerView mRecyclerView;
        CustomGridView customGridView;
        LinearLayout llResult, llResultZero;
        TextView tvResult;

        ItemViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.tv_name);
            mRecyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            customGridView = (CustomGridView) itemView.findViewById(R.id.gridView);
            llResult = itemView.findViewById(R.id.llResult);
            llResultZero = itemView.findViewById(R.id.llResultZero);
            tvResult = itemView.findViewById(R.id.tvResult);
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //LayoutInflater.from指定写法
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler, parent, false);
        //holder = new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discuss_other_recycler, parent, false));
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (mDatas.get(position).getTeachGroupChatResultPics().size() > 0) {
            holder.mTextView.setText(mDatas.get(position).getTeachGroupChatResultPics().get(0).getGroupName() + "");
            holder.mRecyclerView.setHasFixedSize(true);
//        holder.mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 4,
//                GridLayoutManager.VERTICAL, false));
            RecyclerView.ItemDecoration itemDecoration = ((ItemViewHolder) holder).mRecyclerView.getItemDecorationAt(0);
            if (itemDecoration == null) {
                ((ItemViewHolder) holder).mRecyclerView.addItemDecoration(new SpacesItemThreeDecoration(8));
            }
            holder.mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mOtherItemRecyclerViewAdapter = new OtherItemRecyclerViewAdapter(position, mDatas.get(position).getTeachGroupChatResultPics());
            holder.mRecyclerView.setAdapter(mOtherItemRecyclerViewAdapter);
            drawRecyclerView();
            //
            OtherGridAdapter gridAdapter = new OtherGridAdapter(mContext);
            List<Home> list = new ArrayList<>();
            for (int i = 0; i < mDatas.get(position).getTeachGroupChatResultPics().size(); i++) {
                list.add(new Home(mDatas.get(position).getTeachGroupChatResultPics().get(i).getUserStudentName().trim(), ""));
            }
            Set<Home> ts = new HashSet<Home>();
            ts.addAll(list);
            list.clear();
            for (Home home : ts) {
                list.add(home);
            }
            gridAdapter.setListData(list);
            holder.customGridView.setNumColumns(15);
            holder.customGridView.setAdapter(gridAdapter);
            if (mDatas.get(position).getScore() > 0) {
                holder.llResult.setVisibility(View.VISIBLE);
                holder.llResultZero.setVisibility(View.GONE);
                holder.tvResult.setText((int) mDatas.get(position).getScore() + "");
            } else {
                holder.llResult.setVisibility(View.GONE);
                holder.llResultZero.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    /**
     * RecyclerView 内层点击事件方法
     */
    private void drawRecyclerView() {
        //RecyclerView点击事件
        mOtherItemRecyclerViewAdapter.setOnItemClickListener(mContext, new OtherItemRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, List<ChatOtherBean.DataBean.TeachGroupChatResultPicsBean> picList) {
//                Toast.makeText(mContext, "" + position + "str--" + str, Toast.LENGTH_SHORT).show();
                final ArrayList<ImageSlideBean> picsBeanList = new ArrayList<>();
                for (int i = 0; i < picList.size(); i++) {
                    picsBeanList.add(new ImageSlideBean(picList.get(i).getId(),
                            picList.get(i).getPicUrl(), "0","0"));//picList.get(i).getUserStudentId()
                }

                Intent intent = new Intent(mContext, ImageSlideActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putParcelableArrayList("pics", picsBeanList);
                bundle.putBoolean("isResult",false);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }

        });
    }


}
