package com.example.app1xztl.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.chatroombean.ChatOtherBean;

/**
 * Created by Administrator on 2019/8/16 0011.
 */

public interface DiscussOtherView extends BaseView {
    void getDiscussResult(ChatOtherBean otherBean);

    void netError(String s);
}
