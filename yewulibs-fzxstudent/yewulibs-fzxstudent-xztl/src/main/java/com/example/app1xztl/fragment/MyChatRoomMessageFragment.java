package com.example.app1xztl.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.activity.FullImageActivity;
import com.example.app1xztl.adapter.ChatAdapter;
import com.example.app1xztl.listener.OnDiscussListener;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessageExtension;
import com.netease.nimlib.sdk.chatroom.model.CustomChatRoomMessageConfig;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.AttachStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.MemberPushOption;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libpublic.utils.GlideImageLoader;
import com.sdzn.fzx.student.libpublic.utils.chatroom.ImageUrlUtil;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;
import rain.coder.photopicker.utils.ImageUtils;
import rain.coder.photopicker.utils.UCropUtils;

/**
 * Created by admin on 2019/8/8.
 */

public class MyChatRoomMessageFragment extends BaseFragment implements View.OnClickListener, TextWatcher, OnRefreshListener {
    View rootView;

    /* @BindView(R.id.chat_list)
     EasyRecyclerView chatList;
     @BindView(R.id.edit_text)
     EditText editText;
     @BindView(R.id.im_select)
     ImageView imageSelect;
     @BindView(R.id.im_add)
     ImageView imageAdd;
     @BindView(R.id.bt_send)
     Button btSend;
     @BindView(R.id.refreshLayout)
     SmartRefreshLayout refreshLayout;*/
    private EditText editText;
    private ImageView imageSelect;
    private ImageView imageAdd;
    private Button btSend;
    private SmartRefreshLayout refreshLayout;
    private EasyRecyclerView chatList;


    private ChatAdapter chatAdapter;
    private LinearLayoutManager layoutManager;
    private List<ChatRoomMessage> messageInfos;

    //    private Y_MultiRecyclerAdapter disAdapter;
//    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private static int CHAT_TYPE_TEXT = 1;
    public static int CHAT_TYPE_IMAGE = 2;
    public static final int REQUECT_CODE_CAMERA = 1100;
    public static final int REQ_CODE_CAMERA = 1003;
    private String roomId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat_room, container, false);
        editText = (EditText) rootView.findViewById(R.id.edit_text);
        imageSelect = (ImageView) rootView.findViewById(R.id.im_select);
        imageAdd = (ImageView) rootView.findViewById(R.id.im_add);
        btSend = (Button) rootView.findViewById(R.id.bt_send);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        chatList = (EasyRecyclerView) rootView.findViewById(R.id.chat_list);

        initWidget();
        startTime = Calendar.getInstance().getTimeInMillis();
        LoadData();
        return rootView;
    }

    public void init(String roomId, long endTime) {
        this.roomId = roomId;
        this.endTime = endTime;
        registerObservers(true);
    }


    private void initWidget() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);

        chatAdapter = new ChatAdapter(getContext());
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatList.setLayoutManager(layoutManager);
        chatAdapter.setHasStableIds(true);
        chatList.setAdapter(chatAdapter);


        chatList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
//                        chatAdapter.handler.removeCallbacksAndMessages(null);
//                        chatAdapter.notifyDataSetChanged();
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
//                        chatAdapter.handler.removeCallbacksAndMessages(null);
//                        mDetector.hideEmotionLayout(false);
//                        mDetector.hideSoftInput();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        chatAdapter.addItemClickListener(itemClickListener);


        messageInfos = new ArrayList<>();

        btSend.setOnClickListener(this);
        editText.addTextChangedListener(this);
        imageAdd.setOnClickListener(this);
        imageSelect.setOnClickListener(this);
    }

    private void LoadData() {
        if (messageInfos != null) {
            messageInfos.clear();
        }
        if (chatAdapter != null) {
            chatAdapter.clear();
        }
        loadHistory(10);//第一次加载一些历史
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    /**
     * 历史消息
     */
    private long startTime, endTime;

    private boolean isVisiTime;

    @Override
    public void onRefresh(final RefreshLayout refreshLayout) {
        if (chatAdapter != null) {
            loadHistory(15);
            cancilLoadState();
        }
    }

    private void loadHistory(final int num) {
        if (startTime > endTime) {
            MsgTypeEnum typeEnums[] = {MsgTypeEnum.text, MsgTypeEnum.image};
// 从现在开始，查询20条消息
            NIMClient.getService(ChatRoomService.class).pullMessageHistoryExType(roomId, startTime, num, QueryDirectionEnum.QUERY_OLD, typeEnums).setCallback(new RequestCallback<List<ChatRoomMessage>>() {
                @Override
                public void onSuccess(List<ChatRoomMessage> chatRoomMessages) {
                    isVisiTime = false;
                    if (chatRoomMessages.size() > 0) {

                        for (int i = 0; i < chatRoomMessages.size(); i++) {
                            if (chatRoomMessages.get(i).getTime() > endTime) {
                                chatAdapter.insert(chatRoomMessages.get(i), 0);
                                isVisiTime = true;
                            }
                        }

                        if (chatAdapter.getAllData().size() > 0) {
                            startTime = chatAdapter.getAllData().get(0).getTime();
                            if (isVisiTime) {
                                addCenterTime(1);
                            }
                        }
                        messageInfos.clear();
                        messageInfos.addAll(chatAdapter.getAllData());

                        chatList.scrollToPosition(0);
                        chatAdapter.notifyDataSetChanged();

                        if (chatRoomMessages.size() < num) {
                            refreshLayout.setEnableRefresh(false);
                            addCenterTime(0);
                        }
                    } else {
//                        ToastUtil.showShortlToast("已经没有历史记录啦");
                        refreshLayout.setEnableRefresh(false);
                        addCenterTime(0);
                    }
                }

                @Override
                public void onFailed(int i) {
//                    ToastUtil.showShortlToast("历史记录加载失败");
                }

                @Override
                public void onException(Throwable throwable) {
//                    ToastUtil.showShortlToast("历史记录加载失败" + throwable);
                }
            });

        } else {
            refreshLayout.setEnableRefresh(false);
        }
    }


    /**
     * 接受消息
     */
    private void registerObservers(boolean register) {
        NIMClient.getService(ChatRoomServiceObserver.class).observeReceiveMessage(incomingChatRoomMsg, register);
    }

    Observer<List<ChatRoomMessage>> incomingChatRoomMsg = new Observer<List<ChatRoomMessage>>() {
        @Override
        public void onEvent(List<ChatRoomMessage> messages) {
            if (messages == null || messages.isEmpty()) {
                return;
            }

            for (ChatRoomMessage message : messages) {
                if (message.getMsgType() == MsgTypeEnum.text || message.getMsgType() == MsgTypeEnum.image) {
                    chatAdapter.add(message);
                    chatList.scrollToPosition(chatAdapter.getCount() - 1);
                }

            }
            if (chatAdapter != null) {
                chatAdapter.notifyDataSetChanged();
            }

//            messageListPanel.onIncomingMessage(messages);
        }
    };

    /*================ 列表回调接口 ================*/

    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
        if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {
            /*相册返回图片  图片选择器*/
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);
                    sendImMes(path, CHAT_TYPE_IMAGE);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        sendImMes(photoLists.get(0).getPath(), CHAT_TYPE_IMAGE);
                    }
                }

            }
        } else if (requestCode == REQ_CODE_CAMERA) {
            /*拍照返回*/
            if (resultCode == Activity.RESULT_OK) {
                startClipPic(false);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {
                sendImMes(UCrop.getOutput(data), CHAT_TYPE_IMAGE);
            }
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE && null != data) {
            *//*相册返回图片  图片选择器*//*
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);
                    sendImMes(path, CHAT_TYPE_IMAGE);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        sendImMes(photoLists.get(0).getPath(), CHAT_TYPE_IMAGE);
                    }
                }

            }
        } else if (requestCode == REQ_CODE_CAMERA) {
            *//*拍照返回*//*
            if (resultCode == Activity.RESULT_OK) {
                startClipPic(false);
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {
                sendImMes(UCrop.getOutput(data), CHAT_TYPE_IMAGE);
            }
        }


    }*/


    /**
     * item点击事件
     */
    private ChatAdapter.onItemClickListener itemClickListener = new ChatAdapter.onItemClickListener() {
        @Override
        public void onHeaderClick(int position) {
//            Toast.makeText(getContext(), "onHeaderClick", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onImageClick(View view, int position) {
            Intent intentImage = new Intent(getActivity(), FullImageActivity.class);
            intentImage.putExtra("photoUrl", ImageUrlUtil.getImagetUrl(messageInfos.get(position - 1).getAttachStr()));
            startActivity(intentImage);

        }

        @Override
        public void onVoiceClick(final ImageView imageView, final int position) {

        }
    };

    /**
     * 发送消息（图片）
     *
     * @param
     */
    public void sendImMes(String text, int tag) {
        //createChatRoomImageMessage(String roomId, File file, String displayName)
        if (tag == CHAT_TYPE_TEXT) {//text
            // 创建聊天室文本消息
            final ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomTextMessage(roomId, text);
            Map map = new HashMap();
            map.put("chat_name", UserController.getUserName());
            message.setRemoteExtension(map);
            NIMClient.getService(ChatRoomService.class).sendMessage(message, false)
                    .setCallback(new RequestCallback<Void>() {
                        @Override
                        public void onSuccess(Void param) {
                            messageInfos.add(message);
                            chatAdapter.add(message);
                            chatList.scrollToPosition(chatAdapter.getCount() - 1);
                            chatAdapter.notifyDataSetChanged();
                            editText.setText("");
                        }

                        @Override
                        public void onFailed(int code) {
                            ToastUtil.showShortlToast("消息发送失败" + code);
                        }

                        @Override
                        public void onException(Throwable exception) {
                            ToastUtil.showShortlToast("消息发送失败: exception");
                        }
                    });
        } else if (tag == CHAT_TYPE_IMAGE) {//image
            // 创建聊天室文本消息
            File file = new File(text);
            String name = text.substring(text.lastIndexOf("\\") + 1);
            final ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomImageMessage(roomId, file, name);
            Map map = new HashMap();
            map.put("chat_name", UserController.getUserName());
            message.setRemoteExtension(map);
            NIMClient.getService(ChatRoomService.class).sendMessage(message, false)
                    .setCallback(new RequestCallback<Void>() {
                        @Override
                        public void onSuccess(Void param) {
                            messageInfos.add(message);
                            chatAdapter.add(message);
                            chatList.scrollToPosition(chatAdapter.getCount() - 1);
                            chatAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onFailed(int code) {
                            ToastUtil.showShortlToast("消息发送失败" + code);
                        }

                        @Override
                        public void onException(Throwable exception) {
                            ToastUtil.showShortlToast("消息发送失败: exception");
                        }
                    });

        }

    }

    /**
     * 添加显示时间
     */
    private void addCenterTime(final int noHis) {
        ChatRoomMessage addChatTime = new ChatRoomMessage() {
            @Override
            public ChatRoomMessageExtension getChatRoomMessageExtension() {
                return null;
            }

            @Override
            public CustomChatRoomMessageConfig getChatRoomConfig() {
                return null;
            }

            @Override
            public void setChatRoomConfig(CustomChatRoomMessageConfig customChatRoomMessageConfig) {

            }

            @Override
            public boolean isHighPriorityMessage() {
                return false;
            }

            @Override
            public String getUuid() {
                return null;
            }

            @Override
            public boolean isTheSame(IMMessage imMessage) {
                return false;
            }

            @Override
            public String getSessionId() {
                return null;
            }

            @Override
            public SessionTypeEnum getSessionType() {
                return null;
            }

            @Override
            public String getFromNick() {
                return null;
            }

            @Override
            public MsgTypeEnum getMsgType() {
                return MsgTypeEnum.custom;
            }

            @Override
            public MsgStatusEnum getStatus() {
                return null;
            }

            @Override
            public void setStatus(MsgStatusEnum msgStatusEnum) {

            }

            @Override
            public void setDirect(MsgDirectionEnum msgDirectionEnum) {

            }

            @Override
            public MsgDirectionEnum getDirect() {
                return null;
            }

            @Override
            public void setContent(String s) {

            }

            @Override
            public String getContent() {
                return String.valueOf(noHis);
            }

            @Override
            public long getTime() {
                return startTime;
            }

            @Override
            public void setFromAccount(String s) {

            }

            @Override
            public String getFromAccount() {
                return null;
            }

            @Override
            public void setAttachment(MsgAttachment msgAttachment) {

            }

            @Override
            public MsgAttachment getAttachment() {
                return null;
            }

            @Override
            public String getAttachStr() {
                return null;
            }

            @Override
            public AttachStatusEnum getAttachStatus() {
                return null;
            }

            @Override
            public void setAttachStatus(AttachStatusEnum attachStatusEnum) {

            }

            @Override
            public CustomMessageConfig getConfig() {
                return null;
            }

            @Override
            public void setConfig(CustomMessageConfig customMessageConfig) {

            }

            @Override
            public Map<String, Object> getRemoteExtension() {
                return null;
            }

            @Override
            public void setRemoteExtension(Map<String, Object> map) {

            }

            @Override
            public Map<String, Object> getLocalExtension() {
                return null;
            }

            @Override
            public void setLocalExtension(Map<String, Object> map) {

            }

            @Override
            public String getPushContent() {
                return null;
            }

            @Override
            public void setPushContent(String s) {

            }

            @Override
            public Map<String, Object> getPushPayload() {
                return null;
            }

            @Override
            public void setPushPayload(Map<String, Object> map) {

            }

            @Override
            public MemberPushOption getMemberPushOption() {
                return null;
            }

            @Override
            public void setMemberPushOption(MemberPushOption memberPushOption) {

            }

            @Override
            public boolean isRemoteRead() {
                return false;
            }

            @Override
            public boolean needMsgAck() {
                return false;
            }

            @Override
            public void setMsgAck() {

            }

            @Override
            public boolean hasSendAck() {
                return false;
            }

            @Override
            public int getTeamMsgAckCount() {
                return 0;
            }

            @Override
            public int getTeamMsgUnAckCount() {
                return 0;
            }

            @Override
            public int getFromClientType() {
                return 0;
            }

            @Override
            public NIMAntiSpamOption getNIMAntiSpamOption() {
                return null;
            }

            @Override
            public void setNIMAntiSpamOption(NIMAntiSpamOption nimAntiSpamOption) {

            }

            @Override
            public void setClientAntiSpam(boolean b) {

            }

            @Override
            public void setForceUploadFile(boolean b) {

            }

            @Override
            public boolean isInBlackList() {
                return false;
            }
        };
        chatAdapter.insert(addChatTime, 0);

    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.bt_send) {
            if (editText.getText().length() > 0 && !editText.getText().toString().trim().isEmpty()) {
                sendImMes(editText.getText().toString(), CHAT_TYPE_TEXT);
            } else {
                ToastUtil.showShortlToast("输入内容不能为空");
            }

        } else if (i == R.id.im_add) {
            ((OnDiscussListener) getContext()).setResultSendStatus();
            requestPermissiontest();

        } else if (i == R.id.im_select) {
            ((OnDiscussListener) getContext()).setResultSendStatus();
            new PhotoPickConfig.Builder(this)
                    .imageLoader(new GlideImageLoader())
                    .showCamera(false)
                    .maxPickSize(1)
                    .spanCount(8)
                    .clipPhoto(false)
                    .mustClip(false)
                    .build();
        }
    }

    private static final int REQUECT_CODE_S = 1000;
    private static final int REQUECT_CODE_C = 1001;


    /**
     * 输入键盘
     */

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 0) {
            btSend.setVisibility(View.VISIBLE);
            imageAdd.setVisibility(View.GONE);
        } else {
            btSend.setVisibility(View.GONE);
            imageAdd.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            if (FileUtil.existsSdcard()) {
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            } else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(getActivity(), AppUtils.getAppPackageName() + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {
            GalleryFinal.init(getActivity());
            GalleryFinal.openCrop(takeImageFile.getAbsolutePath(), new GalleryFinal.OnHanlderResultCallback() {
                @Override
                public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                    if (resultList == null || resultList.isEmpty()) {
                        onHanlderFailure(reqeustCode, "裁剪失败");
                        return;
                    }
//                   clipSuccess(resultList.get(0).getPhotoPath());
                    ToastUtil.showShortlToast("CAMERA-----" + resultList.get(0).getPhotoPath());
                }

                @Override
                public void onHanlderFailure(int requestCode, String errorMsg) {
//                    mView.clipFailed(errorMsg);
                }
            });
        } else {
            String imagePath = ImageUtils.getImagePath(getActivity(), "/Crop/");
            File corpFile = new File(imagePath + ImageUtils.createFile());
            UCropUtils.start(getActivity(), new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    private File takeImageFile;

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    /**
     * 相机权限
     */

    public void requestPermissiontest() {
//        MPermissions.requestPermissions(this, REQUECT_CODE_CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
//        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
//        EasyPermissions.requestPermissions(activity, "请设置权限",
//                REQUECT_CODE_CAMERA, perms);
        methodRequiresTwoPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @AfterPermissionGranted(REQUECT_CODE_CAMERA)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(activity, perms)) {
            // Already have permission, do the thing
            // ...
            toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(activity, "请设置相机权限",
                    REQUECT_CODE_CAMERA, perms);
        }
    }

//    @PermissionGrant(REQUECT_CODE_CAMERA)
//    public void requestSdcardSuccess() {
//        toSystemCamera();
//    }
//
//    @PermissionDenied(REQUECT_CODE_CAMERA)
//    public void requestSdcardFailed() {
//        ToastUtil.showShortlToast("请设置相机权限");
//    }

}
