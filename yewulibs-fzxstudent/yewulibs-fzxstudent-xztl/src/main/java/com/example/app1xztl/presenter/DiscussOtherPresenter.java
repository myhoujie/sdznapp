package com.example.app1xztl.presenter;

import com.example.app1xztl.view.DiscussOtherView;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.vo.chatroombean.ChatOtherBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Administrator on 2019/8/16 0012.
 */

public class DiscussOtherPresenter extends BasePresenter<DiscussOtherView, BaseActivity> {


    public void getDiscussResult(String groupChatTaskId){
        Network.createTokenService(NetWorkService.ChatService.class)
                .getGroupChatTask(groupChatTaskId)
                .map(new StatusFunc<ChatOtherBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ChatOtherBean>(new SubscriberListener<ChatOtherBean>(){

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable!=null) {
                            mView.netError(throwable.toString());
                        }
                    }

                    @Override
                    public void onNext(ChatOtherBean chatOtherBean) {
                        mView.getDiscussResult(chatOtherBean);
                    }
                },mActivity,true,false,false,""));

    }



}
