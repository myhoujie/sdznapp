package com.example.app1xztl.adapter.holder;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.ChatAdapter;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.sdzn.fzx.student.libpublic.utils.chatroom.ImageUrlUtil;

/**
 * 作者：Rance on 2016/11/29 10:47
 * 邮箱：rance935@163.com
 */
public class ChatSendViewHolder extends BaseViewHolder<ChatRoomMessage> {
    private TextView textView;
    private ImageView chatItemContentImage;
    private LinearLayout llTv;

    private ChatAdapter.onItemClickListener onItemClickListener;
    private Handler handler;
    private String path = "";

    public ChatSendViewHolder(ViewGroup parent, ChatAdapter.onItemClickListener onItemClickListener, Handler handler) {
        super(parent, R.layout.item_chat_send);
        textView = itemView.findViewById(R.id.tv_chat_send);
        chatItemContentImage = itemView.findViewById(R.id.chat_item_content_image);
        llTv = itemView.findViewById(R.id.ll_tv);

        this.onItemClickListener = onItemClickListener;
        this.handler = handler;
    }


    @Override
    public void setData(ChatRoomMessage data) {
        if (data.getMsgType() == MsgTypeEnum.text) {
            if (!data.getContent().equals(null)) {
                textView.setText(data.getContent() + "");
            }
            textView.setVisibility(View.VISIBLE);
            llTv.setVisibility(View.VISIBLE);
            chatItemContentImage.setVisibility(View.GONE);
        } else if (data.getMsgType() == MsgTypeEnum.image) {
            textView.setVisibility(View.GONE);
            llTv.setVisibility(View.GONE);
            chatItemContentImage.setVisibility(View.VISIBLE);

            path = ImageUrlUtil.getImagetUrl(data.getAttachStr());
            Glide.with(getContext()).load(path).override(240, 300).into(chatItemContentImage);
            chatItemContentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onImageClick(chatItemContentImage, getDataPosition());
                }
            });
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(data.getMsgType() + "");
        }
    }
}
