package com.example.app1xztl.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.OtherGridAdapter;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libpublic.views.chatroom_widget.CustomGridView;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.chatroombean.ClassGroupingVo;
import com.sdzn.fzx.student.vo.chatroombean.Home;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author zs
 */
public class MemberActivity extends BaseActivity implements View.OnClickListener {
    private TextView tvName;
    private CustomGridView gridView;
    private View viewPerch;
    private TextView tvClose;
    OtherGridAdapter gridAdapter;
    private String classId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
        tvName = (TextView) findViewById(R.id.tv_name);
        gridView = (CustomGridView) findViewById(R.id.gridView);
        viewPerch = (View) findViewById(R.id.view_perch);
        tvClose = (TextView) findViewById(R.id.tv_close);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(MemberActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        tvClose.setOnClickListener(this);

    }

    @Override
    protected void initData() {
        getClassGroupList(UserController.getClassId());
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tv_close) {
            MemberActivity.this.finish();
        }
    }

    private String groupName = "";

    private void getClassGroupData(ClassGroupingVo classGroupingVo) {
        if (classGroupingVo.getData().size() > 0) {
            viewPerch.setVisibility(View.GONE);


            List<Home> list = new ArrayList<>();
            for (int i = 0; i < classGroupingVo.getData().size(); i++) {
                if (UserController.getLoginBean().getData().getUser().getClassGroupId().equals(String.valueOf(classGroupingVo.getData().get(i).getClassGroupId()))) {
                    list.add(new Home(classGroupingVo.getData().get(i).getStudentName(), ""));
                    groupName = classGroupingVo.getData().get(i).getClassGroupName();
                }
            }
            tvName.setText(groupName);
            gridAdapter = new OtherGridAdapter(this, list);
            gridView.setAdapter(gridAdapter);
        } else {
            viewPerch.setVisibility(View.VISIBLE);
        }

    }

    private void getClassGroupList(String classId) {
        Network.createTokenService(NetWorkService.ChatService.class)
                .GetClassGrouping(classId)
                .map(new StatusFunc<ClassGroupingVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ClassGroupingVo>(new SubscriberListener<ClassGroupingVo>() {
                    @Override
                    public void onNext(ClassGroupingVo classGroupingVo) {
                        getClassGroupData(classGroupingVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ToastUtil.showShortlToast(e.toString());
                        }

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, this));
    }
}
