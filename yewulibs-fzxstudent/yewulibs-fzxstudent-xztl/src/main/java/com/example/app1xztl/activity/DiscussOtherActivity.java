package com.example.app1xztl.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.adapter.OtherRecyclerViewAdapter;
import com.example.app1xztl.presenter.DiscussOtherPresenter;
import com.example.app1xztl.view.DiscussOtherView;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.chatroombean.ChatOtherBean;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

import java.util.ArrayList;
import java.util.List;


/**
 * 其他小组结果
 */

public class DiscussOtherActivity extends MBaseActivity<DiscussOtherPresenter> implements DiscussOtherView, View.OnClickListener {
    OtherRecyclerViewAdapter mOtherRecyclerViewAdapter;
    private TextView tvBack;
    private RecyclerView mTvRecycler;

    @Override
    public void initPresenter() {
        mPresenter = new DiscussOtherPresenter();
        mPresenter.attachView(this, this);
    }

    private Bundle bundle;
    private String title, content, endTime;
    private int groupChatTaskId, id, state;
    private ArrayList<GroupListBean.DataBean.TeachGroupChatResultPicsBean> pics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discuss_other);
        tvBack = (TextView) findViewById(R.id.tvBack);
        mTvRecycler = (RecyclerView) findViewById(R.id.my_recycler);


        bundle = getIntent().getExtras();
        groupChatTaskId = bundle.getInt("groupChatTaskId");
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(DiscussOtherActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getDiscussResult(String.valueOf(groupChatTaskId));
    }

    @Override
    protected void initView() {
        tvBack.setOnClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mTvRecycler.setLayoutManager(layoutManager);

    }

    @Override
    protected void initData() {

    }

    @Override
    public void getDiscussResult(ChatOtherBean otherBean) {
        if (otherBean.getData().size() > 0) {
            List<ChatOtherBean.DataBean> mDatas = new ArrayList<>();
            for (int a = 0; a < otherBean.getData().size(); a++) {
                if (otherBean.getData().get(a).getTeachGroupChatResultPics().size() > 0) {
                    mDatas.add(otherBean.getData().get(a));
                }
            }
            mOtherRecyclerViewAdapter = new OtherRecyclerViewAdapter(DiscussOtherActivity.this, mDatas);
            //给RecyclerView设置适配器
            mTvRecycler.setAdapter(mOtherRecyclerViewAdapter);
        }
    }

    @Override
    public void netError(String s) {

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            DiscussOtherActivity.this.finish();
        }
    }
}
