package com.example.app1xztl.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app1xztl.R;
import com.example.app1xztl.activity.DiscussActivity;
import com.example.app1xztl.presenter.GroupListPresenter;
import com.example.app1xztl.view.GroupListView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.chatroom.SpacesItemAllDecoration;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 小组讨论-- recyclerview展示界面
 */

public class GroupListFragment extends MBaseFragment<GroupListPresenter> implements GroupListView, OnRefreshListener, OnLoadMoreListener {
    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rv;
    private LinearLayout llEmpty;
    private ImageView ivEmpty;
    private TextView tvEmpty;


    private int itemCount = 0;//标签  历史位置
    private int subjectId;
    private int currPage = 0;
    private int pageSize = 20;

    private Y_MultiRecyclerAdapter disAdapter;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private String searchStr;//搜索

    public static GroupListFragment newInstance(Bundle bundle) {
        GroupListFragment fragment = new GroupListFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new GroupListPresenter();
        mPresenter.attachView(this, activity);
    }

    /**
     * 获取数据
     */
    private void getGroupDis() {
        Map<String, Object> params = new HashMap<>();
        if (subjectId > 0) {
            params.put("subjectId", String.valueOf(subjectId));
        } else {
            params.put("subjectId", "");
        }
        if (searchStr != null) {
            params.put("keyWord", searchStr);
        } else {
            params.put("keyWord", "");
        }
        params.put("groupId", UserController.getLoginBean().getData().getUser().getClassGroupId());
        params.put("classId", UserController.getLoginBean().getData().getUser().getClassId());
        params.put("page", String.valueOf(currPage));
        params.put("rows", String.valueOf(pageSize));
        mPresenter.getDiscussData(params);
    }

    /**
     * 搜索   GroupDiscussionFragment-->传入
     */
    public void search(String mSearchStr) {
        currPage = 1;
        itemEntityList.clear();
        this.searchStr = mSearchStr;
        getGroupDis(); //获取网络数据
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_discuss, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        rv = (EmptyRecyclerView) rootView.findViewById(R.id.rv);
        llEmpty = (LinearLayout) rootView.findViewById(R.id.llEmpty);
        ivEmpty = (ImageView) rootView.findViewById(R.id.ivEmpty);
        tvEmpty = (TextView) rootView.findViewById(R.id.tvEmpty);

        subjectId = getArguments().getInt("subjectId");
        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        currPage = 1;
        itemEntityList.clear();
        getGroupDis();
        if (disAdapter != null) {
            disAdapter.notifyDataSetChanged();
        }
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));
//        ivEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.unf_empty));
//        tvEmpty.setText(getResources().getString(R.string.fragment_main_text16));
        rv.setEmptyView(llEmpty);
        rv.addItemDecoration(new SpacesItemAllDecoration(10));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(App2.get(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int count = 1;
                if (position == 0 || position == itemCount) {
                    return 2;
                }
                return count;
            }
        });
        rv.setLayoutManager(gridLayoutManager);
        disAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityList);
        rv.setAdapter(disAdapter);
        rv.addOnItemTouchListener(new OnItemTouchListener(rv) {
            @Override
            public void onItemClick(final RecyclerView.ViewHolder vh) {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        int position = vh.getAdapterPosition();
                        if (position < 0 || position >= itemEntityList.getItemCount()) {
                            return;
                        }
                        Intent intent = new Intent(getActivity(), DiscussActivity.class);

                        Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
                        if (itemData instanceof GroupListBean.DataBean) {
                            //正在讨论抓过你太  且roomId为null或空
                            if (((GroupListBean.DataBean) itemData).getState() == 0 && ((GroupListBean.DataBean) itemData).getRoomId() == null) {
                                ToastUtil.showShortlToast("聊天室创建失败");
                                return;
                            } else if (((GroupListBean.DataBean) itemData).getState() == 0 && ((GroupListBean.DataBean) itemData).getRoomId().isEmpty()) {
                                ToastUtil.showShortlToast("聊天室创建失败");
                                return;
                            }
                            Bundle bundle = new Bundle();
                            bundle.putString("roomId", ((GroupListBean.DataBean) itemData).getRoomId());
                            bundle.putString("title", ((GroupListBean.DataBean) itemData).getChatTitle());
                            bundle.putString("endTime", ((GroupListBean.DataBean) itemData).getEndTime());
                            bundle.putString("createTime", ((GroupListBean.DataBean) itemData).getCreateTime());
                            bundle.putString("content", ((GroupListBean.DataBean) itemData).getChatContent());
                            bundle.putInt("groupChatTaskId", ((GroupListBean.DataBean) itemData).getGroupChatTaskId());
                            bundle.putInt("state", ((GroupListBean.DataBean) itemData).getState());
                            bundle.putInt("id", ((GroupListBean.DataBean) itemData).getId());
                            bundle.putInt("isRead", ((GroupListBean.DataBean) itemData).getIsRead());
                            bundle.putFloat("score", ((GroupListBean.DataBean) itemData).getScore());
                            ArrayList<GroupListBean.DataBean.TeachGroupChatResultPicsBean> pic = (ArrayList<GroupListBean.DataBean.TeachGroupChatResultPicsBean>) ((GroupListBean.DataBean) itemData).getTeachGroupChatResultPics();
                            bundle.putParcelableArrayList("pic", pic);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }
                });
            }
        });

    }


    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        itemEntityList.clear();
        getGroupDis();
        cancilLoadState();
    }

    /**
     * 加载更多  此处进行页数处理    getGroupDis(); currPage++;
     */
    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getGroupDis();
    }

    private List<GroupListBean.DataBean> nowData = new ArrayList<>();
    private List<GroupListBean.DataBean> oldData = new ArrayList<>();

    private void setDis(List<GroupListBean.DataBean> dis) {
        for (int a = 0; a < dis.size(); a++) {
            if (dis.get(a).getState() == 0 && !itemEntityList.getItems().contains("当前讨论")) {
                itemEntityList.addItem(R.layout.item_fragment_discuss_top, "当前讨论")
                        .addOnBind(R.layout.item_fragment_discuss_top, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                holder.setText(R.id.tvName, (String) itemData);
                            }
                        });
            } else if (dis.get(a).getState() == 1 && !itemEntityList.getItems().contains("历史讨论")) {
//                itemCount = itemEntityList.getItemCount();
                itemEntityList.addItem(R.layout.item_fragment_discuss_top, "历史讨论")
                        .addOnBind(R.layout.item_fragment_discuss_top, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                holder.setText(R.id.tvName, (String) itemData);
                            }
                        });
            }
            itemEntityList.addItem(R.layout.item_fragment_discuss_n, dis.get(a))
                    .addOnBind(R.layout.item_fragment_discuss_n, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            mPresenter.bindHolder(rv.getHeight(), holder, (GroupListBean.DataBean) itemData, subjectId);
                        }
                    });


        }
    }

    @Override
    public void getDiscussSuccess(GroupListBean discussBeanList) {
        cancilLoadState();
        // 第一页，需要先清空
        if (currPage == 1) {
            itemEntityList.clear();
            itemCount = 0;
        }
        nowData.clear();
        oldData.clear();

        if (discussBeanList.getData().size() > 0) { //此处数据判断
            rv.setEmptyView(llEmpty);//尝试刷新

            for (int i = 0; i < discussBeanList.getData().size(); i++) {
                if (discussBeanList.getData().get(i).getState() == 0) {
                    nowData.add(discussBeanList.getData().get(i));
                } else {
                    oldData.add(discussBeanList.getData().get(i));
                }
            }
            if (currPage == 1) {
                if (nowData.size() > 0) {
                    itemCount = nowData.size() + 1;
                } else {
                    itemCount = 0;
                }
            }
            setDis(nowData);
            setDis(oldData);

            disAdapter.notifyDataSetChanged();
            if (discussBeanList.getData().size() < pageSize) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }

        } else {
            itemEntityList.clear();
            disAdapter.notifyDataSetChanged();
            rv.setEmptyView(llEmpty);
            refreshLayout.setEnableLoadMore(false);
        }
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }


}
