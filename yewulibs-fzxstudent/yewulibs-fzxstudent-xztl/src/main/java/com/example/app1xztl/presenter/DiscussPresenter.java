package com.example.app1xztl.presenter;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.AppUtils;
import com.example.app1xztl.R;
import com.example.app1xztl.activity.ImageSlideActivity;
import com.example.app1xztl.view.DiscussView;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libpublic.utils.chatroom.GlideHelper;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.ImageUploadInfoBean;
import com.sdzn.fzx.student.vo.QiniuUptoken;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.ImageSlideBean;
import com.tencent.bugly.crashreport.CrashReport;

import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.utils.ImageUtils;
import rain.coder.photopicker.utils.UCropUtils;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2019/8/14 0012.
 */

public class DiscussPresenter extends BasePresenter<DiscussView, BaseActivity> {
    public static final int REQ_CODE_CAMERA = 1003;

    public void getChatIsRead(String id) {
        Network.createTokenService(NetWorkService.ChatService.class)
                .getChatIsRead(id)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {


                    }
                });
    }

    public void insertChatPic(Map<String, Object> parms) {
        Network.createTokenService(NetWorkService.ChatService.class)
                .insertChatTaskPic(parms)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        mView.returnSuccess();

                    }
                }, mActivity, true, false, false, ""));
    }

    public void getChatPic(Map<String, Object> parms) {
        Network.createTokenService(NetWorkService.ChatService.class)
                .getChatTaskPic(parms)
                .map(new StatusFunc<DiscussionPicList>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<DiscussionPicList>(new SubscriberListener<DiscussionPicList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(DiscussionPicList o) {
                        mView.getChatPic(o);

                    }
                }, mActivity, true, false, false, ""));
    }


    public void bindHolder(int rvHeight, final GeneralRecyclerViewHolder holder, DiscussionPicList.DataBean oldList, final ArrayList<ImageSlideBean> picsBeanList, final boolean isResult) {
//        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlItem).getLayoutParams();
//        layoutParams.height = rvHeight / 3;
//        holder.getChildView(R.id.rlItem).setLayoutParams(layoutParams);
        holder.getChildView(R.id.tv_name).setVisibility(View.VISIBLE);
        holder.setText(R.id.tv_name, oldList.getUserStudentName());

        GlideHelper.load(mActivity, oldList.getPicUrl(), (ImageView) holder.getChildView(R.id.iv_image));

        ((RelativeLayout) holder.getChildView(R.id.rl_image)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, ImageSlideActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", holder.getAdapterPosition());
                bundle.putParcelableArrayList("pics", picsBeanList);
                if (isResult) {
                    bundle.putBoolean("isResult", true);
                } else {
                    bundle.putBoolean("isResult", false);
                }
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
            }
        });

    }


    public void uploadSubjectivePhoto(final List<Photo> photoLists) {
        final ProgressDialogManager pdm = new ProgressDialogManager(mActivity);
        final List<UploadPicVo.DataBean> uploadPicVoList = new ArrayList<>();
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("压缩中...");
        final UploadManager manager = new UploadManager();
        Observable.from(photoLists)
                .flatMap(new Func1<Photo, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Photo photo) {//压缩
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(photo.getPath(), options);
                        Bitmap compressBitmap = qiNiuCompress(bitmap);
                        bitmap.recycle();
                        return Observable.just(compressBitmap);
                    }
                }).flatMap(new Func1<Bitmap, Observable<ImageUploadInfoBean>>() {
            @Override
            public Observable<ImageUploadInfoBean> call(Bitmap bitmap) {//获取uptoken
                RequestParams params = new RequestParams(Network.BASE_GET_UPTOKEN);
                String key = getKey();
                params.addBodyParameter("key", key);
                params.addBodyParameter("access_token", UserController.getAccessToken());
                try {
                    String result = x.http().getSync(params, String.class);
                    QiniuUptoken uptoken = GsonUtil.fromJson(result, QiniuUptoken.class);
                    if (uptoken == null || uptoken.getResult() == null || TextUtils.isEmpty(uptoken.getResult().getUpToken())) {
                        throw new RuntimeException("上传失败: 获取upToken失败");
                    }
                    QiniuUptoken.ResultBean bean = uptoken.getResult();
                    return Observable.just(new ImageUploadInfoBean(key, bean.getUpToken(), bean.getDomain(), bean.getImageStyle(), bitmap));
                } catch (Throwable e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageUploadInfoBean>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        pdm.cancelWaiteDialog();
                        mView.networkError(e.getMessage());
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }

                    @Override
                    public void onNext(final ImageUploadInfoBean bean) {
                        pdm.showWaiteDialog("正在上传...");
                        manager.put(bitmap2Bytes(bean.getBitmap()), bean.getKey(), bean.getUpToken(), new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info, JSONObject response) {
                                if (info.isOK()) {
                                    Log.e("上传成功", "上传成功" + key);
                                    //imageView2/2/w/100/h/100/q/75|imageslim
                                    UploadPicVo.DataBean dataBean = UploadPicVo.DataBean
                                            .create(bean.getDomain(), key, bean.getImageStyle());

                                    uploadPicVoList.add(dataBean);

                                    if (uploadPicVoList.size() == photoLists.size()) {
                                        pdm.cancelWaiteDialog();
                                        UploadPicVo mUploadPicVo = new UploadPicVo();
                                        mUploadPicVo.setData(uploadPicVoList);
                                        mView.onUploadPicSuccess(mUploadPicVo);
                                    }
                                } else {
                                    pdm.cancelWaiteDialog();
                                    mView.networkError(info.error);
                                    CrashReport.postCatchedException(new RuntimeException(info.error));
                                }
                            }
                        }, null);
                    }
                });
    }

    /**
     * 上传七牛前图片压缩
     *
     * @param image 图片bitmap
     * @return 压缩后的图片
     */
    private Bitmap qiNiuCompress(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        Bitmap bitmap = BitmapFactory.decodeStream(bais);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        float scale;
        if (bitmapWidth > bitmapHeight) {//横向图片
            if (bitmapWidth < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapWidth;
            }
        } else {//纵向图片
            if (bitmapHeight < 1088) {
                // 2019-01-28 小图不压
                return bitmap;
            } else {
                scale = 1088f / bitmapHeight;
            }
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
        bitmap.recycle();
        return bitmap2;
    }

    private String getKey() {
        return UUID.randomUUID().toString() + ".jpg";
    }

    /**
     * bitmap转byte数组
     */
    private byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    private File takeImageFile;

    /**
     * 调用相机
     */
    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard()) {
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            } else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            Uri uri;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                uri = Uri.fromFile(takeImageFile);
            } else {
                uri = FileProvider.getUriForFile(mActivity, AppUtils.getAppPackageName() + ".fileProvider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities
                        (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mActivity.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        mActivity.startActivityForResult(takePictureIntent, REQ_CODE_CAMERA);
    }

    /**
     * 裁剪图片
     */
    public void startClipPic(boolean isFillBlank) {
        if (isFillBlank) {
            GalleryFinal.init(mActivity);
            GalleryFinal.openCrop(takeImageFile.getAbsolutePath(), new GalleryFinal.OnHanlderResultCallback() {
                @Override
                public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                    if (resultList == null || resultList.isEmpty()) {
                        onHanlderFailure(reqeustCode, "裁剪失败");
                        return;
                    }
//                   clipSuccess(resultList.get(0).getPhotoPath());
                    ToastUtil.showShortlToast("CAMERA-----" + resultList.get(0).getPhotoPath());
                }

                @Override
                public void onHanlderFailure(int requestCode, String errorMsg) {
//                    mView.clipFailed(errorMsg);
                }
            });
        } else {
            String imagePath = ImageUtils.getImagePath(mActivity, "/Crop/");
            File corpFile = new File(imagePath + ImageUtils.createFile());
            UCropUtils.start(mActivity, new File(takeImageFile.getAbsolutePath()), corpFile, false);
        }
    }

    /**
     * 发送 图片
     */
    public void showSelectImgDialog(final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(mActivity, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

}
