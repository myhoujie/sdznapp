package com.example.app1grzx.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app1grzx.R;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.views.DelActivityDialog;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Administrator on 2018/1/8.
 * 个人中心
 */

public class MeFragment extends BaseFragment implements View.OnClickListener {
    private RoundAngleImageView meHead;
    private TextView meName;
    private TextView meClass;
    private RelativeLayout basicsData;
    private ImageView basicsImg;
    private RelativeLayout schoolData;
    private ImageView schoolImg;
    private RelativeLayout writeData;
    private ImageView writeImg;
    private RelativeLayout clearData;
    private ImageView clearImg;
    private RelativeLayout aboutData;
    private ImageView aboutImg;
    private RelativeLayout gooutRl;
    private TextView gooutRlTv;
    private FrameLayout framelayoutMe;
    private List<Fragment> fragments;
    private int containerId = R.id.framelayoutMe;
    private Fragment currFragment;
    private DelActivityDialog delActivityDialog;

    public static MeFragment newInstance(Bundle bundle) {
        MeFragment fragment = new MeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_me, container, false);
        meHead = (RoundAngleImageView) rootView.findViewById(R.id.me_head);
        meName = (TextView) rootView.findViewById(R.id.me_name);
        meClass = (TextView) rootView.findViewById(R.id.me_class);
        basicsData = (RelativeLayout) rootView.findViewById(R.id.basics_data);
        basicsImg = (ImageView) rootView.findViewById(R.id.basics_img);
        schoolData = (RelativeLayout) rootView.findViewById(R.id.school_data);
        schoolImg = (ImageView) rootView.findViewById(R.id.school_img);
        writeData = (RelativeLayout) rootView.findViewById(R.id.write_data);
        writeImg = (ImageView) rootView.findViewById(R.id.write_img);
        clearData = (RelativeLayout) rootView.findViewById(R.id.clear_data);
        clearImg = (ImageView) rootView.findViewById(R.id.clear_img);
        aboutData = (RelativeLayout) rootView.findViewById(R.id.about_data);
        aboutImg = (ImageView) rootView.findViewById(R.id.about_img);
        gooutRl = (RelativeLayout) rootView.findViewById(R.id.goout_rl);
        gooutRlTv = (TextView) rootView.findViewById(R.id.goout_rl_tv);
        framelayoutMe = (FrameLayout) rootView.findViewById(R.id.framelayoutMe);
        initView();
        setOnClick();

        initFragment();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        LoginBean.DataBean data = UserController.getLoginBean().getData();
        if (!TextUtils.isEmpty(data.getUser().getRealName())) {
            meName.setText(data.getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getUser().getPhoto())) {
            Glide.with(getActivity())
                    .load(data.getUser().getPhoto())
                    .error(R.mipmap.mrtx_img)
                    .transform(new CircleTransform(getActivity())).into(meHead);
        }
        if (!TextUtils.isEmpty(data.getUser().getClassName()) && !TextUtils.isEmpty(data.getUser().getBaseGradeName())) {
            meClass.setText(data.getUser().getBaseGradeName() + data.getUser().getClassName());
        }
    }

    private void initFragment() {

        fragments = new ArrayList<>();
        fragments.add(BasicDataFragment.newInstance(null));
        fragments.add(SchoolNewsFragment.newInstance(null));
        fragments.add(WriteFragment.newInstance(null));
        fragments.add(ClearDataFragment.newInstance(null));
        fragments.add(AboutFragment.newInstance(null));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.replace(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
        ft.replace(containerId, fragment);

        ft.commit();
    }

    private void setOnClick() {
        gooutRl.setOnClickListener(this);
        aboutData.setOnClickListener(this);
        clearData.setOnClickListener(this);
        writeData.setOnClickListener(this);
        schoolData.setOnClickListener(this);
        basicsData.setOnClickListener(this);
    }

    private void initView() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    public DelActivityDialog ActivityGvAdapter() {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(getActivity())
                .buildText("确定退出登录？")
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        BLEServiceManger.getInstance().destroyManger(activity);//关掉手写笔

                        Intent intent2 = new Intent(App2.get(), MqttService.class);
                        getActivity().stopService(intent2);// 关闭服务


                        StudentSPUtils.clear(getContext());
                        CommonUtils.isReceiveRbMq = false;
                        //判断当前rom是否为领创定制版
                        if (CommonAppUtils.isLingChuangPad()) {
                            //登录状态
                            StudentSPUtils.setAutoLogin(false);
                            //sp密码清空
                            StudentSPUtils.saveLoginUserPwd("");
                            //sp账号清空
                            StudentSPUtils.saveLoginUserNum("");

                            CommonAppUtils.logout();
                            //关闭MqttService服务
                            getActivity().stopService(new Intent(App2.get(), MqttService.class));
                            //清楚堆栈
                            ActivityManager.exit();
                            //关闭项目
                            System.exit(0);
                        } else {
                            getActivity().stopService(new Intent(App2.get(), MqttService.class));
                            ActivityManager.exit();
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                });
        return delActivityDialog;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.goout_rl) {
            ActivityGvAdapter().show();
        } else if (i == R.id.basics_data) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    showAssignedFragment(0);
                    basicsData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                    schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));

                }
            });
        } else if (i == R.id.school_data) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    showAssignedFragment(1);
                    basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    schoolData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                    clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
            });
        } else if (i == R.id.clear_data) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    clearData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                    writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    showAssignedFragment(3);
                }
            });
        } else if (i == R.id.write_data) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    writeData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                    aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    showAssignedFragment(2);
                }
            });
        } else if (i == R.id.about_data) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    schoolData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    aboutData.setBackgroundColor(Color.parseColor("#EDF5FF"));
                    showAssignedFragment(4);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(Event event) {
        if (event instanceof UpdataPhoto) {
            initData();
        }
    }
}
