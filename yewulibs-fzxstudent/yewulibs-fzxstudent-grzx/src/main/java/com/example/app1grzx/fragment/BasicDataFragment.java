package com.example.app1grzx.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.example.app1grzx.R;
import com.sdzn.fzx.student.presenter.BasicDataPresenter1;
import com.sdzn.fzx.student.view.BasicDataViews;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.utils.GlideImageLoader;
import com.sdzn.fzx.student.libpublic.utils.PopupwindowUtils;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.UserBean;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;

import static android.app.Activity.RESULT_OK;
import static rain.coder.photopicker.utils.UCropUtils.start;

/**
 * @date
 */
public class BasicDataFragment extends BaseFragment implements BasicDataViews, View.OnClickListener {
    BasicDataPresenter1 mPresenter;
    private RelativeLayout headRl;
    private RoundAngleImageView headImg;

    private RelativeLayout nameRl;
    private TextView nameTx;
    private RelativeLayout sexRl;
    private TextView sexTx;
    private RelativeLayout schoolRl;
    private TextView schoolTx;
    private TextView levelTx;
    private TextView classTx;
    private TextView teacherTx;
    private LoginBean loginBean;

    private UserBean.DataBean loginUserBean;
    private int containerId = R.id.framelayoutMe;
    private FragmentManager fmanager;
    private FragmentTransaction ftransaction;
    private ChangePhoneFragment changePhoneFragment;
    private ChangepasswordFragment changepasswordFragment;

    private File takeImageFile;
    private static final String TAG = "BasicDataFragment";

    private static final int REQUECT_CODE_SDCARD = 1000;
    private static final int REQUECT_CODE_CAMERA = 1001;
    protected static final int CHOOSE_PICTURE = 0;
    protected static final int TAKE_PICTURE = 1;
    private static final int CROP_SMALL_PICTURE = 2;
    protected static Uri tempUri;
    private PopupWindow popupWindow;

    public BasicDataFragment() {
    }

    public static BasicDataFragment newInstance(Bundle bundle) {
        BasicDataFragment fragment = new BasicDataFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_basic_data, container, false);
        headRl = (RelativeLayout) view.findViewById(R.id.head_rl);
        headImg = (RoundAngleImageView) view.findViewById(R.id.head_img);
        nameRl = (RelativeLayout) view.findViewById(R.id.name_rl);
        nameTx = (TextView) view.findViewById(R.id.name_tx);
        sexRl = (RelativeLayout) view.findViewById(R.id.sex_rl);
        sexTx = (TextView) view.findViewById(R.id.sex_tx);
        schoolRl = (RelativeLayout) view.findViewById(R.id.school_rl);
        schoolTx = (TextView) view.findViewById(R.id.school_tx);
        levelTx = (TextView) view.findViewById(R.id.level_tx);
        classTx = (TextView) view.findViewById(R.id.class_tx);
        teacherTx = (TextView) view.findViewById(R.id.teacher_tx);
        initPresenter();
        headRl.setOnClickListener(this);
        setSpuDate(StudentSPUtils.getLoginBean().getData().getUser());
        return view;
    }

    private void setSpuDate(LoginBean.DataBean.UserBean userBean) {
        if (userBean == null) {
            return;
        }
        if (!TextUtils.isEmpty(userBean.getPhoto())) {
            Glide.with(getActivity()).load(userBean.getPhoto()).error(R.mipmap.mrtx_img).transform(new CircleTransform(getActivity())).into(headImg);
        }
        if (!TextUtils.isEmpty(userBean.getRealName())) {
            nameTx.setText(userBean.getRealName());
        }
    }

    private void setDate(UserBean userBean) {
        loginUserBean = userBean.getData();
        if (userBean == null || loginUserBean == null) {
            return;
        }
        if (!TextUtils.isEmpty(loginUserBean.getPhoto())) {
            Glide.with(getActivity()).load(loginUserBean.getPhoto()).error(R.mipmap.mrtx_img).transform(new CircleTransform(getActivity())).into(headImg);
        }

        if (!TextUtils.isEmpty(loginUserBean.getRealName())) {
            nameTx.setText(loginUserBean.getRealName());
        }

        if (!TextUtils.isEmpty(loginUserBean.getGenderName())) {
            sexTx.setText(loginUserBean.getGenderName());
        }
        setSchoolData();
    }

    private void setSchoolData() {
//        loginBean = UserController.getLoginBean();
        if (loginUserBean == null) {
            return;
        }
        if (!TextUtils.isEmpty(loginUserBean.getSchoolName())) {
            schoolTx.setText(loginUserBean.getSchoolName());
        }
        if (!TextUtils.isEmpty(loginUserBean.getLevelName())) {
            levelTx.setText(loginUserBean.getLevelName());
        }
        if (!TextUtils.isEmpty(loginUserBean.getGradeName())) {
            classTx.setText(loginUserBean.getGradeName());
        }
        if (!TextUtils.isEmpty(loginUserBean.getTeacherName())) {
            teacherTx.setText(loginUserBean.getTeacherName());
        }
    }


    public void initPresenter() {
        mPresenter = new BasicDataPresenter1();
        mPresenter.onCreate(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE:
//                    startPhotoZoom(tempUri);
                    startClipPic();
                    break;
                case PhotoPickConfig.PICK_REQUEST_CODE:
                    // 裁切的路径
                    if (data.getBooleanExtra("isClip", false)) {
                        // 裁切的路径
                        String output = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);

                        Glide.with(getActivity()).load(output).error(R.mipmap.mrtx_img).transform(new CircleTransform(getActivity())).into(headImg);
                        mPresenter.saveHead(new File(output));
                    } else {
                        ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                        if (photoLists != null && !photoLists.isEmpty()) {
                            // 路径列表
                            Photo photo = photoLists.get(0);
                            String output = photo.getPath();
                            Glide.with(getActivity()).load(output).error(R.mipmap.mrtx_img).transform(new CircleTransform(getActivity())).into(headImg);
                            mPresenter.saveHead(new File(output));
                        }
                    }
                    break;
                case UCrop.REQUEST_CROP:
//                    if (data != null) {
                    setImageToView(data);
//                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        /*if (i == R.id.head_rl) {
            showChoosePicDialog(view);

        } else */

    }


    public void showChoosePicDialog(View v) {

        popupWindow = PopupwindowUtils.showHeadPopupWindow(getActivity(), getActivity());
        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

        PopupwindowUtils.setonClick(new PopupwindowUtils.onClick() {
            @Override
            public void onClickPhoto() {
//                MPermissions.requestPermissions(BasicDataFragment.this, REQUECT_CODE_SDCARD,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
//                String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
//                EasyPermissions.requestPermissions(getActivity(), "请设置权限",
//                        REQUECT_CODE_SDCARD, perms);
                methodRequiresTwoPermission();
                popupWindow.dismiss();
            }

            @Override
            public void onClickCamera() {
//                MPermissions.requestPermissions(BasicDataFragment.this, REQUECT_CODE_CAMERA,
//                        Manifest.permission.CAMERA);
//                String[] perms = {Manifest.permission.CAMERA};
//                EasyPermissions.requestPermissions(getActivity(), "请设置权限",
//                        REQUECT_CODE_CAMERA, perms);
                methodRequiresTwoPermission2();
                popupWindow.dismiss();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(BasicDataFragment.this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }


    @AfterPermissionGranted(REQUECT_CODE_SDCARD)
    private void methodRequiresTwoPermission() {
        String[] perms = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // Already have permission, do the thing
            // ...
            new PhotoPickConfig.Builder(BasicDataFragment.this)
                    .imageLoader(new GlideImageLoader())
                    .showCamera(false)
                    .maxPickSize(1)
                    .spanCount(8)
                    .clipPhoto(false)
                    .build();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(getActivity(), "请设置权限",
                    REQUECT_CODE_SDCARD, perms);
        }
    }

    @AfterPermissionGranted(REQUECT_CODE_CAMERA)
    private void methodRequiresTwoPermission2() {
        String[] perms = {Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // Already have permission, do the thing
            // ...
            toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(getActivity(), "请设置权限",
                    REQUECT_CODE_CAMERA, perms);
        }
    }

//    @PermissionGrant(REQUECT_CODE_SDCARD)
//    public void requestSdcardSuccess() {
//        new PhotoPickConfig.Builder(BasicDataFragment.this)
//                .imageLoader(new GlideImageLoader())
//                .showCamera(false)
//                .maxPickSize(1)
//                .spanCount(8)
//                .clipPhoto(false)
//                .build();
//    }

//    @PermissionDenied(REQUECT_CODE_SDCARD)
//    public void requestSdcardFailed() {
//        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
//    }
//
//    @PermissionGrant(REQUECT_CODE_CAMERA)
//    public void requestCameraSuccess() {
//        toSystemCamera();
//    }
//
//    @PermissionDenied(REQUECT_CODE_CAMERA)
//    public void requestCamerAFailed() {
//        ToastUtil.showShortlToast("访问相机权限被拒绝");
//    }

    public void toSystemCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            if (FileUtil.existsSdcard())
                takeImageFile = new File(Environment.getExternalStorageDirectory(), "/DCIM/camera/");
            else {
                takeImageFile = Environment.getDataDirectory();
            }
            takeImageFile = FileUtil.createFile(takeImageFile, "IMG_", ".jpg");
            if (takeImageFile != null) {
                // 默认情况下，即不需要指定intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                // 照相机有自己默认的存储路径，拍摄的照片将返回一个缩略图。如果想访问原始图片，
                // 可以通过dat extra能够得到原始图片位置。即，如果指定了目标uri，data就没有数据，
                // 如果没有指定uri，则data就返回有数据！
                Uri uri;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    uri = Uri.fromFile(takeImageFile);
                } else {
                    /**
                     * 7.0 调用系统相机拍照不再允许使用Uri方式，应该替换为FileProvider
                     * 并且这样可以解决MIUI系统上拍照返回size为0的情况
                     */
                    uri = FileProvider.getUriForFile(activity, AppUtils.getAppPackageName() + ".fileProvider", takeImageFile);
                    //加入uri权限 要不三星手机不能拍照
                    List<ResolveInfo> resInfoList = activity.getPackageManager().queryIntentActivities
                            (takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        activity.grantUriPermission(packageName, uri, Intent
                                .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            }
        }
        this.startActivityForResult(takePictureIntent, TAKE_PICTURE);
    }


    /**
     * TODO 裁剪图片
     */
    public void startClipPic() {
        String imagePath = rain.coder.photopicker.utils.ImageUtils.getImagePath(activity, "/Crop/");
        File corpFile = new File(imagePath + rain.coder.photopicker.utils.ImageUtils.createFile());
        start(this, new File(takeImageFile.getAbsolutePath()), corpFile, false);
    }


    private void takePicture() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= 23) {

            int check = ContextCompat.checkSelfPermission(getActivity(), permissions[0]);
            if (check != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }

        }
        Intent openCameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory(), "image.jpg");

        if (Build.VERSION.SDK_INT >= 24) {
            openCameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            tempUri = FileProvider.getUriForFile(getActivity(), "com.sdzn.fzx.student.fileProvider", file);
        } else {
            tempUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "image.jpg"));
        }
        openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
        startActivityForResult(openCameraIntent, TAKE_PICTURE);
    }

    public void startPhotoZoom(Uri uri) {
        if (uri == null) {
            return;
        }
        tempUri = uri;
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, CROP_SMALL_PICTURE);
    }

    public void setImageToView(Intent data) {
        String output = UCrop.getOutput(data);
        Glide.with(getActivity()).load(output).error(R.mipmap.mrtx_img).transform(new CircleTransform(getActivity())).into(headImg);
        mPresenter.saveHead(new File(output));
    }

    @Override
    public void getUserInfo(UserBean userBean) {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        loginBean.getData().getUser().setPhoto(userBean.getData().getPhoto());
        StudentSPUtils.saveLoginBean(loginBean);
        setDate(userBean);
        EventBus.getDefault().post(new UpdataPhoto());
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getUserInfo("Bearer " + (String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""));
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
