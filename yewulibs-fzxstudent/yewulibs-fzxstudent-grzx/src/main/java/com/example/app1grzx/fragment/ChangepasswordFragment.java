package com.example.app1grzx.fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1grzx.R;
import com.sdzn.fzx.student.presenter.ChangepasswordPresenter1;
import com.sdzn.fzx.student.view.ChangepasswordViews;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libpublic.views.ClearableEditText;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangepasswordFragment extends BaseFragment implements ChangepasswordViews,View.OnClickListener {


    /*@BindView(R.id.title_back_txt)
    TextView titleBackTxt;
    @BindView(R.id.title_back_ly)
    LinearLayout titleBackLy;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.confirm_ly)
    LinearLayout confirmLy;
    @BindView(R.id.password_old)
    TextView passwordOld;
    @BindView(R.id.password_old_edit)
    ClearableEditText passwordOldEdit;
    @BindView(R.id.new_password_tx)
    TextView newPasswordTx;
    @BindView(R.id.new_password_edit)
    ClearableEditText newPasswordEdit;
    @BindView(R.id.confirm_password_tx)
    TextView confirmPasswordTx;
    @BindView(R.id.confirm_password_edit)
    ClearableEditText confirmPasswordEdit;*/
    ChangepasswordPresenter1 mPresenter;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView title;
    private LinearLayout confirmLy;
    private TextView passwordOld;
    private ClearableEditText passwordOldEdit;
    private TextView newPasswordTx;
    private ClearableEditText newPasswordEdit;
    private TextView confirmPasswordTx;
    private ClearableEditText confirmPasswordEdit;

    private String oldPassword;
    private String newPassword;
    private String confirmPassword;


    public ChangepasswordFragment() {
        // Required empty public constructor
    }

    public static ChangepasswordFragment newInstance(Bundle bundle) {
        ChangepasswordFragment fragment = new ChangepasswordFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_changepassword, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        title = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        passwordOld = (TextView) view.findViewById(R.id.password_old);
        passwordOldEdit = (ClearableEditText) view.findViewById(R.id.password_old_edit);
        newPasswordTx = (TextView) view.findViewById(R.id.new_password_tx);
        newPasswordEdit = (ClearableEditText) view.findViewById(R.id.new_password_edit);
        confirmPasswordTx = (TextView) view.findViewById(R.id.confirm_password_tx);
        confirmPasswordEdit = (ClearableEditText) view.findViewById(R.id.confirm_password_edit);
        initPresenter();
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        return view;
    }

    public void initPresenter() {
        mPresenter = new ChangepasswordPresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (i == R.id.confirm_ly) {
            changePassword();
        }
    }
    private void changePassword() {
        oldPassword = passwordOldEdit.getText().toString();
        newPassword = newPasswordEdit.getText().toString();
        confirmPassword = confirmPasswordEdit.getText().toString();
        if (!StringUtils.vertifyPsw(newPassword)) {

            ToastUtil.showLonglToast("请输入6-16位数字、字母密码");
            return;
        }
        if (!confirmPassword.equals(newPassword)) {

            ToastUtil.showLonglToast("两次密码输入不一致");
            return;
        }
        mPresenter.changePassword("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
                String.valueOf(UserController.getLoginBean().getData().getUser().getId()),oldPassword, newPassword);
    }

    @Override
    public void changePassswordSuccess() {
        reLogin();
    }

    @Override
    public void onFailed(String msg) {

    }

    private void reLogin() {
        Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
        getActivity().startActivity(loginIntent);
        getActivity().finish();
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
