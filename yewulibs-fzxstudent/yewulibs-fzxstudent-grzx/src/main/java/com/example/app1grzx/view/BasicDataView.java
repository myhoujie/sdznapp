package com.example.app1grzx.view;

import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.UserBean;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface BasicDataView extends BaseView {

    void getUserInfo(UserBean loginBean);
}
