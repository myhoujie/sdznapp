package com.example.app1grzx.fragment;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app1grzx.R;
import com.example.app1grzx.presenter.AboutPresenter;
import com.example.app1grzx.view.AboutView;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends BaseFragment implements AboutView, View.OnClickListener {

    private TextView versions;
    private TextView versions1;
    private RelativeLayout functionRl;
    RelativeLayout yinsiRl;
    RelativeLayout yonghuRl;


    public AboutFragment() {
        // Required empty public constructor
    }

    public static AboutFragment newInstance(Bundle bundle) {
        AboutFragment fragment = new AboutFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        versions = (TextView) view.findViewById(R.id.versions);
        versions1 = (TextView) view.findViewById(R.id.versions1);
        functionRl = (RelativeLayout) view.findViewById(R.id.function_rl);
        yinsiRl = view.findViewById(R.id.yinsi_rl);
        yonghuRl = view.findViewById(R.id.yonghu_rl);
        functionRl.setOnClickListener(this);
        yinsiRl.setOnClickListener(this);
        yonghuRl.setOnClickListener(this);
        initData();
        return view;
    }

    private void initData() {
        try {
            versions.setText("当前版本： " + getVersionName());
            versions1.setText("" + getVersionName());
//            mPresenter.saveStudentVersion(getVersionName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.function_rl) {
        } else if (i == R.id.yonghu_rl) {

            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuContract.html");
            startActivity(intent);
        } else if (i == R.id.yinsi_rl) {
//            HiosHelper.resolveAd(getActivity(), XieYiActivity.class, "https://www.baidu.com");

            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuPrivacyContract.html");
            startActivity(intent);

        }
    }

    private String getVersionName() throws Exception {
        // 获取packagemanager的实例
        PackageManager packageManager = getActivity().getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = packageManager.getPackageInfo(getActivity().getPackageName(), 0);
        String version = packInfo.versionName;
        return version;
    }
}
