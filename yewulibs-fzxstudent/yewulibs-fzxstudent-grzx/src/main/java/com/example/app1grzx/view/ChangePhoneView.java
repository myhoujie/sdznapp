package com.example.app1grzx.view;


import com.sdzn.fzx.student.libbase.base.BaseView;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface ChangePhoneView extends BaseView {
    public void onCountDownChanged(int str);

    void verifySuccess(final String phone, final String code);

    void savePhone();
}
