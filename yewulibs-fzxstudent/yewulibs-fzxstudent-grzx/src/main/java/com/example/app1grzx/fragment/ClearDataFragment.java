package com.example.app1grzx.fragment;


import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.app1grzx.R;
import com.example.app1grzx.presenter.ClearDataPresenter;
import com.example.app1grzx.view.ClearDataView;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libutils.util.FileSizeUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.view.MProgressDialog;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClearDataFragment extends BaseFragment implements ClearDataView, View.OnClickListener {
    private RelativeLayout cacheRl;
    private TextView cacheDataTx;
    private String storePath;
    private Handler handler;

    public ClearDataFragment() {
        // Required empty public constructor
    }

    public static ClearDataFragment newInstance(Bundle bundle) {
        ClearDataFragment fragment = new ClearDataFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clear_data, container, false);
        cacheRl = (RelativeLayout) view.findViewById(R.id.cache_rl);
        cacheDataTx = (TextView) view.findViewById(R.id.cache_data_tx);
        cacheRl.setOnClickListener(this);

        storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "dearxy";
        initData();
        return view;
    }

    private void initData() {

        double fileOrFilesSize = 0;
        try {
            fileOrFilesSize = FileSizeUtil.getFolderSize(new File(storePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        cacheDataTx.setText(fileOrFilesSize + "M");
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ToastUtil.showShortlToast("清除缓存成功");
            }
        };
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        int i1 = view.getId();
        if (i1 == R.id.cache_rl) {
            final MProgressDialog mProgressDialog = MProgressDialog.newInstance(activity);
            TextView txt = (TextView) mProgressDialog.findViewById(R.id.message);
            txt.setText("正在清除缓存...");
            mProgressDialog.show();
            try {
                int i = FileSizeUtil.deleteFolderFile(storePath, true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    /**
                     *要执行的操作
                     */
                    mProgressDialog.cancel();
                    handler.sendEmptyMessage(1);
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, 3000);
        }
    }
}
