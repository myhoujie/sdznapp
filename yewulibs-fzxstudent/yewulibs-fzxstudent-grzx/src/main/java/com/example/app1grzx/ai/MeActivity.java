package com.example.app1grzx.ai;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.example.app1grzx.R;
import com.example.app1grzx.fragment.AboutFragment;
import com.example.app1grzx.fragment.BasicDataFragment;
import com.example.app1grzx.fragment.ClearDataFragment;
import com.example.app1grzx.fragment.DataSafetyFragment;
import com.example.app1grzx.fragment.MeFragment;
import com.example.app1grzx.fragment.WriteFragment;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.event.UpdataPhoto;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.views.DelActivityDialog;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MeActivity extends BaseActivity implements View.OnClickListener {
    private RoundAngleImageView meHead;
    private TextView meName;
    private TextView meClass;
    private RelativeLayout basicsData;
    private ImageView basicsImg;
    private RelativeLayout safeData;
    private ImageView safeImg;
    private RelativeLayout writeData;
    private ImageView writeImg;
    private RelativeLayout clearData;
    private ImageView clearImg;
    private RelativeLayout aboutData;
    private ImageView aboutImg;
    private RelativeLayout gooutRl;
    private TextView gooutRlTv;
    private TextView basicsTv;
    private TextView safeTv;
    private TextView writeTv;
    private TextView clearTv;
    private TextView aboutTv;
    private FrameLayout framelayoutMe;
    private List<Fragment> fragments;
    private int containerId = R.id.framelayoutMe;
    private Fragment currFragment;
    private DelActivityDialog delActivityDialog;
    private TextView tvMyTitle;
    private TextView tvBack;
    private TextView tvTitleName;

    public static MeFragment newInstance(Bundle bundle) {
        MeFragment fragment = new MeFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        setContentView(R.layout.activity_me);

        initView();
        setOnClick();
        initFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    protected void initData() {
        LoginBean.DataBean data = UserController.getLoginBean().getData();
        if (!TextUtils.isEmpty(data.getUser().getRealName())) {
            meName.setText(data.getUser().getRealName());
        }
        if (!TextUtils.isEmpty(data.getUser().getPhoto())) {
            Glide.with(App2.get())
                    .load(data.getUser().getPhoto())
                    .error(R.mipmap.mrtx_img)
                    .transform(new CircleTransform(App2.get())).into(meHead);
        }
        if (!TextUtils.isEmpty(data.getUser().getClassName()) && !TextUtils.isEmpty(data.getUser().getBaseGradeName())) {
            meClass.setText(data.getUser().getBaseGradeName() + data.getUser().getClassName());
        }
    }

    private void initFragment() {

        fragments = new ArrayList<>();
        fragments.add(BasicDataFragment.newInstance(null));
        fragments.add(DataSafetyFragment.newInstance(null));
        fragments.add(WriteFragment.newInstance(null));
        fragments.add(ClearDataFragment.newInstance(null));
        fragments.add(AboutFragment.newInstance(null));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.replace(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
        ft.replace(containerId, fragment);
        Log.d("grzx", "111" + currFragment.toString());
        ft.commit();
    }

    private void setOnClick() {
        gooutRl.setOnClickListener(this);
        aboutData.setOnClickListener(this);
        clearData.setOnClickListener(this);
        writeData.setOnClickListener(this);
        safeData.setOnClickListener(this);
        basicsData.setOnClickListener(this);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvMyTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(MeActivity.this, new CustomDrawerPopupView.GrzxNextCallBack() {
                    @Override
                    public void toGrzxNextClick() {
                        ToastUtils.showShort("个人");

                    }
                });
                new XPopup.Builder(MeActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(customDrawerPopupView)
                        .show();
            }
        });
    }

    protected void initView() {
        meHead = (RoundAngleImageView) findViewById(R.id.me_head);
        meName = (TextView) findViewById(R.id.me_name);
        meClass = (TextView) findViewById(R.id.me_class);
        basicsData = (RelativeLayout) findViewById(R.id.basics_data_s);
        basicsImg = (ImageView) findViewById(R.id.basics_img_s);
        basicsTv = (TextView) findViewById(R.id.basics_tv_s);
        safeData = (RelativeLayout) findViewById(R.id.safe_data_s);
        safeImg = (ImageView) findViewById(R.id.safe_img_s);
        safeTv = (TextView) findViewById(R.id.safe_tv_s);
        writeData = (RelativeLayout) findViewById(R.id.write_data_s);
        writeImg = (ImageView) findViewById(R.id.write_img_s);
        writeTv = (TextView) findViewById(R.id.write_tv_s);
        clearData = (RelativeLayout) findViewById(R.id.clear_data_s);
        clearImg = (ImageView) findViewById(R.id.clear_img_s);
        clearTv = (TextView) findViewById(R.id.clear_tv_s);
        aboutData = (RelativeLayout) findViewById(R.id.about_data_s);
        aboutImg = (ImageView) findViewById(R.id.about_img_s);
        aboutTv = (TextView) findViewById(R.id.about_tv_s);
        gooutRl = (RelativeLayout) findViewById(R.id.goout_rl);
        gooutRlTv = (TextView) findViewById(R.id.goout_rl_tv);
        framelayoutMe = (FrameLayout) findViewById(R.id.framelayoutMe);
        /*

         */
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        tvBack = (TextView) findViewById(R.id.tv_back);//返回
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);//标题名称
        tvTitleName.setText("个人中心");
        tvMyTitle.setVisibility(View.VISIBLE);
        tvBack.setVisibility(View.VISIBLE);
        tvTitleName.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public DelActivityDialog ActivityGvAdapter() {
        delActivityDialog = new DelActivityDialog()
                .creatDialog(MeActivity.this)
                .buildText("确定退出登录？")
                .buildListener(new DelActivityDialog.OptionListener() {
                    @Override
                    public void onConfirmed() {
                        BLEServiceManger.getInstance().destroyManger(MeActivity.this);//关掉手写笔

                        Intent intent2 = new Intent(App2.get(), MqttService.class);
                        stopService(intent2);// 关闭服务


                        StudentSPUtils.clear(App2.get());
                        CommonUtils.isReceiveRbMq = false;
                        //判断当前rom是否为领创定制版
                        if (CommonAppUtils.isLingChuangPad()) {
                            //登录状态
                            StudentSPUtils.setAutoLogin(false);
//                            //sp密码清空
//                            StudentSPUtils.saveLoginUserPwd("");
//                            //sp账号清空
//                            StudentSPUtils.saveLoginUserNum("");

                            CommonAppUtils.logout();
                            //关闭MqttService服务
                            stopService(new Intent(App2.get(), MqttService.class));
                            //清楚堆栈
                            ActivityManager.exit();
                            //关闭项目
                            System.exit(0);
                        } else {
                            stopService(new Intent(App2.get(), MqttService.class));
                            ActivityManager.exit();
                            Intent intent = new Intent(App2.get(), LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                });
        return delActivityDialog;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.goout_rl) {
            ActivityGvAdapter().show();
        } else if (i == R.id.basics_data_s) {
//            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
//                @Override
//                public void run() {
//
//
//                }
//            });
            showAssignedFragment(0);
            initBgAndIc();
            basicsData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            basicsTv.setTextColor(Color.parseColor("#FA541C"));
            basicsImg.setImageResource(R.mipmap.grzx_ziliao);
        } else if (i == R.id.safe_data_s) {
//            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
//                @Override
//                public void run() {
//
//
//
//                }
//            });
            showAssignedFragment(1);
            initBgAndIc();
            safeData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            safeTv.setTextColor(Color.parseColor("#FA541C"));
            safeImg.setImageResource(R.mipmap.grzx_zhaq);

        } else if (i == R.id.write_data_s) {
//            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
//                @Override
//                public void run() {
//                }
//            });
            showAssignedFragment(2);
            initBgAndIc();
            writeData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            writeTv.setTextColor(Color.parseColor("#FA541C"));
            writeImg.setImageResource(R.mipmap.grzx_write);
        } else if (i == R.id.clear_data_s) {
//            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
//                @Override
//                public void run() {
//                }
//            });
            showAssignedFragment(3);
            initBgAndIc();
            clearData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            clearTv.setTextColor(Color.parseColor("#FA541C"));
            clearImg.setImageResource(R.mipmap.grzx_qingchu);
        } else if (i == R.id.about_data_s) {
//            SlbLoginUtil2.get().loginTowhere(this, new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            });
            showAssignedFragment(4);
            initBgAndIc();
            aboutData.setBackgroundResource(R.drawable.bg_grzx_xuan_zhong);
            aboutTv.setTextColor(Color.parseColor("#FA541C"));
            aboutImg.setImageResource(R.mipmap.grzx_about);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(Event event) {
        if (event instanceof UpdataPhoto) {
            initData();
        }
    }

    private void initBgAndIc() {


        basicsData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        safeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        clearData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        writeData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        aboutData.setBackgroundColor(Color.parseColor("#FFFFFF"));
        gooutRl.setBackgroundColor(Color.parseColor("#FFFFFF"));


        basicsImg.setImageResource(R.mipmap.grzx_ziliao_normal);
        safeImg.setImageResource(R.mipmap.grzx_zhaq_normal);
        writeImg.setImageResource(R.mipmap.grzx_write_normal);
        clearImg.setImageResource(R.mipmap.grzx_qingchu_normal);
        aboutImg.setImageResource(R.mipmap.grzx_about_normal);

        basicsTv.setTextColor(Color.parseColor("#323C47"));
        safeTv.setTextColor(Color.parseColor("#323C47"));
        writeTv.setTextColor(Color.parseColor("#323C47"));
        clearTv.setTextColor(Color.parseColor("#323C47"));
        aboutTv.setTextColor(Color.parseColor("#323C47"));

    }
}
