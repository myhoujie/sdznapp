package com.example.app1grzx.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app1grzx.R;
import com.sdzn.fzx.student.presenter.ChangePhonePresenter1;
import com.sdzn.fzx.student.view.ChangePhoneViews;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libpublic.views.ClearableEditText;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

public class ChangePhoneFragment extends BaseFragment implements ChangePhoneViews, View.OnClickListener {
    ChangePhonePresenter1 mPresenter;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleTv;
    private LinearLayout confirmLy;
    private TextView phoneTx;
    private ClearableEditText userNumEdit;
    private TextView codeTx;
    private ClearableEditText userCodeEdit;
    private TextView countDownBtn;


    private String userPhone;
    private String title;
    private int flag;

    public ChangePhoneFragment() {
        // Required empty public constructor
    }

    public static ChangePhoneFragment newInstance(Bundle bundle) {
        ChangePhoneFragment fragment = new ChangePhoneFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_phone, container, false);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleTv = (TextView) view.findViewById(R.id.title);
        confirmLy = (LinearLayout) view.findViewById(R.id.confirm_ly);
        phoneTx = (TextView) view.findViewById(R.id.phone_tx);
        userNumEdit = (ClearableEditText) view.findViewById(R.id.user_num_edit);
        codeTx = (TextView) view.findViewById(R.id.code_tx);
        userCodeEdit = (ClearableEditText) view.findViewById(R.id.user_code_edit);
        countDownBtn = (TextView) view.findViewById(R.id.count_down_btn);
        titleBackLy.setOnClickListener(this);
        confirmLy.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        initPresenter();
        initData();
        return view;
    }

    private void initData() {
        if (getArguments() != null) {
            title = getArguments().getString("title");
            flag = getArguments().getInt("flag");
            titleTv.setText(title);
        }

    }


    public void initPresenter() {
        mPresenter = new ChangePhonePresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {
            getFragmentManager().popBackStack();
        } else if (i == R.id.confirm_ly) {
            confirm();
        } else if (i == R.id.count_down_btn) {
            getCode();
        }
    }

    private void confirm() {
        mPresenter.checkVerityNum(userPhone, userCodeEdit.getText().toString());
    }

    private void getCode() {
        userPhone = userNumEdit.getText().toString();
        if (StringUtils.isMobile(userPhone)) {
            mPresenter.sendVerityCode("Bearer "+(String) com.blankj.utilcode.util.SPUtils.getInstance().getString("token", ""),
                    String.valueOf(UserController.getLoginBean().getData().getUser().getId()),userPhone);
        } else {
            ToastUtil.showShortlToast("请输入正确的手机号格式");
        }

    }

    @Override
    public void onCountDownChanged(int lessTime) {
        if (countDownBtn == null) {
            return;
        }
        if (lessTime < 0) {
            String str = "<font color='#FFFFFF'>重新获取</font>";
            countDownBtn.setEnabled(true);
            countDownBtn.setText(Html.fromHtml(str));
        } else {
            String str = "<font color='#4291FF'>" + (lessTime + 1) + "</font>"
                    + "<font color='#808FA3'>秒后重发</font>";
            countDownBtn.setEnabled(false);
            countDownBtn.setText(Html.fromHtml(str));
        }
    }

    @Override
    public void verifySuccess(String phone, String code) {
        getFragmentManager().popBackStack();
    }

    @Override
    public void savePhone() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
