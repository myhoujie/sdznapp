package com.example.app1grzx.fragment;


import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.example.app1grzx.R;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.connect.LeDeviceListAdapter;
import com.sdzn.fzx.student.libbase.event.BLEServiceEvent;
import com.sdzn.fzx.student.libbase.service.BluetoothLEService;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.tqltech.tqlpencomm.BLEException;
import com.tqltech.tqlpencomm.BLEScanner;
import com.tqltech.tqlpencomm.Dot;
import com.tqltech.tqlpencomm.PenCommAgent;
import com.tqltech.tqlpencomm.PenStatus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteFragment extends BaseFragment implements BluetoothLEService.OnDataReceiveListener, View.OnClickListener {

    private ImageView onOff;
    private TextView bluetoothTv;
    private RelativeLayout rlOne;
    private TextView idNumber;
    private ImageView battery;
    private TextView batteryTv;
    private TextView tvIsConnected;
    private RelativeLayout search;
    private TextView searchTv;
    private LinearLayout ll;
    private TextView rlFour;
    private TextView tvClean;
    private RelativeLayout rlTwo;
    private ListView listview;

    private boolean isOpen;
    private boolean isSearching;

    private LeDeviceListAdapter mLeDeviceListAdapter;
    private final String[] requiredPermissons = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    /**
     * 上次配对信息
     */
    public static final String SP_LAST_PAIRED = "last_paired_device";
    /**
     * 记录配对信息
     */
    public static final String SP_PAIRED_DEVICE = "sp_paird";

    private static final int SELECT_ITEM = 2000;
    private static final int SERVICE_BINDED = 2001;
    private boolean needConnect;
    private String lastDevice;
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SELECT_ITEM:
                    lastDevice = (String) msg.obj;
                    BluetoothLEService service = BLEServiceManger.getInstance().getService();
                    if (service != null) {
                        connectPen();
                        needConnect = false;
                    } else {
                        needConnect = true;
                    }
                    break;
                case SERVICE_BINDED:
                    if (needConnect && BLEServiceManger.getInstance().getService() != null) {
                        connectPen();
                        needConnect = false;
                    }
                    break;
            }
        }
    };
    private PenCommAgent bleManager;

    public WriteFragment() {
    }

    public static WriteFragment newInstance(Bundle bundle) {
        WriteFragment fragment = new WriteFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    /**
     * 连接手写笔
     */
    private void connectPen() {
        BluetoothLEService service = BLEServiceManger.getInstance().getService();
        if (service != null) {
            boolean b = service.connect(lastDevice);
            if (!b) {
                ToastUtil.showShortlToast("连接失败");
            }
        } else {
            ToastUtil.showShortlToast("蓝牙服务未开启");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_write, container, false);
        onOff = (ImageView) view.findViewById(R.id.on_off);
        bluetoothTv = (TextView) view.findViewById(R.id.bluetooth_tv);
        rlOne = (RelativeLayout) view.findViewById(R.id.rl_one);
        idNumber = (TextView) view.findViewById(R.id.id_number);
        battery = (ImageView) view.findViewById(R.id.battery);
        batteryTv = (TextView) view.findViewById(R.id.battery_tv);
        tvIsConnected = (TextView) view.findViewById(R.id.is_connected);
        search = (RelativeLayout) view.findViewById(R.id.search);
        searchTv = (TextView) view.findViewById(R.id.search_tv);
        ll = (LinearLayout) view.findViewById(R.id.ll);
        rlFour = (TextView) view.findViewById(R.id.rl_four);
        tvClean = (TextView) view.findViewById(R.id.tv_clean);
        rlTwo = (RelativeLayout) view.findViewById(R.id.rl_two);
        listview = (ListView) view.findViewById(R.id.listview);

        onOff.setOnClickListener(this);
        search.setOnClickListener(this);
        tvClean.setOnClickListener(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        isOpen = (boolean) StudentSPUtils.get(App2.get(), "TQLPen", false);
        if (isOpen) {
            checkAndOpenBlueTooth();
        }

        mLeDeviceListAdapter = new LeDeviceListAdapter(getActivity());

        listview.setAdapter(mLeDeviceListAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                final BluetoothDevice device = mLeDeviceListAdapter.getItem(index);
                if (device == null) {
                    return;
                }
                if (BLEServiceManger.getInstance().getService() == null) {
                    BLEServiceManger.getInstance().bindService(getActivity());
                }
                if (BLEServiceManger.getInstance().getService() == null) {
                    ToastUtil.showShortlToast("手写笔服务未启动");
                    return;
                }
                if (BLEServiceManger.getInstance().getService().isPenConnected()) {
                    CustomDialog.Builder builder = new CustomDialog.Builder(WriteFragment.this.getActivity());
                    builder.setMessage("更改后，之前连接的手写笔将不可用，确定更改吗？");
                    builder.setPositive("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            connectPen(device.getAddress());
                        }
                    });
                    builder.setNegative("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    CustomDialog dialog = builder.create();
                    dialog.show();
                    return;
                }
                connectPen(device.getAddress());
            }
        });
        if (BLEServiceManger.getInstance().getService() != null) {
            BLEServiceManger.getInstance().getService().addOnDataReceiveListener(this);
        }
        changeUI();
        return view;
    }

    private void connectPen(String addr) {
        //停止搜索
        stopScan();
        bindPenService();
        Message message = Message.obtain();
        message.what = SELECT_ITEM;
        message.obj = addr;
        handler.sendMessage(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
        stopScan();
        if (BLEServiceManger.getInstance().getService() != null) {
            BLEServiceManger.getInstance().getService().removeOnDataReceiveListener(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceConnect(BLEServiceEvent event) {
        BluetoothLEService service = event.getService();
        if (service == null) {//解绑
            return;
        }
        if (!service.initialize()) {
            BLEServiceManger.getInstance().destroyManger(getActivity());
            ActivityManager.finishAll();
            System.exit(0);
            return;
        }
        service.addOnDataReceiveListener(this);
        changeUI();
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
        if (!service.isPenConnected()) {
            battery.setVisibility(View.GONE);
            batteryTv.setVisibility(View.GONE);
            idNumber.setText("设备ID:");
        } else {
            buildBattery();
            idNumber.setText("设备ID:" + CommonUtils.mBTMac);
        }
//        CommonUtils.flag = 1;
        handler.sendEmptyMessage(SERVICE_BINDED);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), requiredPermissons, 0);
        } else {
            bindPenService();
        }
    }

    private void changeUI() {
        if (isOpen) {
            onOff.setImageResource(R.mipmap.kai_icon);
            rlOne.setVisibility(View.VISIBLE);
            rlFour.setVisibility(View.VISIBLE);
            rlTwo.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            batteryTv.setVisibility(View.VISIBLE);
            battery.setVisibility(View.VISIBLE);
            tvClean.setVisibility(View.VISIBLE);
            bluetoothTv.setText("已配对的设备");
            BluetoothLEService service = BLEServiceManger.getInstance().getService();
            if (service != null && service.isPenConnected()) {
                tvIsConnected.setText("已连接");
                idNumber.setText("设备ID:" + CommonUtils.mBTMac);
                battery.setVisibility(View.VISIBLE);
                batteryTv.setVisibility(View.VISIBLE);
                buildBattery();
            } else {
                tvIsConnected.setText("未连接");
                CommonUtils.mBTMac = "";
                CommonUtils.mBattery = 0;
                battery.setVisibility(View.GONE);
                batteryTv.setVisibility(View.GONE);
                idNumber.setText("设备ID:");
            }
//            StudentSPUtils.put(App2.get(), "TQLPen", true);
            SPUtils.getInstance().put("TQLPen", true);
        } else {
            onOff.setImageResource(R.mipmap.guan_icon);
            tvClean.setVisibility(View.GONE);
            searchTv.setText("搜索设备");
            rlOne.setVisibility(View.GONE);
            rlFour.setVisibility(View.GONE);
            rlTwo.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            idNumber.setText("设备ID:");
            batteryTv.setVisibility(View.GONE);
            battery.setVisibility(View.GONE);
            tvIsConnected.setText("未连接");
//            StudentSPUtils.put(App2.get(), "TQLPen", false);
            SPUtils.getInstance().put("TQLPen", false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0
                && grantResults.length > 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            bindPenService();
        } else {
//            Toast.makeText(getActivity(), "Require SD read/write permisson!", Toast.LENGTH_SHORT).show();
        }
    }

    public void bindPenService() {
        BLEServiceManger.getInstance().bindService(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopScan();
        if (BLEServiceManger.getInstance().getService() != null) {
            BLEServiceManger.getInstance().getService().removeOnDataReceiveListener(this);
        }
        BLEServiceManger.getInstance().unbindService(getActivity());
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.search) {
            if (isSearching) {
                rlFour.setVisibility(View.GONE);
                tvClean.setVisibility(View.GONE);
                rlTwo.setVisibility(View.GONE);
                isSearching = false;
                //取消搜索
                if (bleManager == null) {
                    bleManager = PenCommAgent.GetInstance(App2.get());
                }
                bleManager.stopFindAllDevices();
                if (searchTv != null) {
                    searchTv.setText("搜索设备");
                }
                mLeDeviceListAdapter.notifyDataSetChanged();
            } else {
                rlFour.setVisibility(View.VISIBLE);
                tvClean.setVisibility(View.VISIBLE);
                rlTwo.setVisibility(View.VISIBLE);
                checkPermission();
            }

        } else if (i == R.id.on_off) {
            isOpen = !isOpen;
            mLeDeviceListAdapter.clear();
            mLeDeviceListAdapter.notifyDataSetChanged();
            changeUI();
            if (isOpen) {
                BLEServiceManger.getInstance().getService();
                checkAndOpenBlueTooth();
//                    if (BLEServiceManger.getInstance().getService() != null && checkAndOpenBlueTooth()) {
//                        startScan();
//                    }
            } else {
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                if (adapter == null || !adapter.isEnabled()) {
                    return;
                }
                if (!checkAndOpenBlueTooth()) {
                    return;
                }
                if (CommonUtils.flag == 0) {
                    return;
                }
                CommonUtils.flag = 0;
                if (bleManager == null) {
                    bleManager = PenCommAgent.GetInstance(App2.get());
                }
                bleManager.stopFindAllDevices();
                if (BLEServiceManger.getInstance().getService() != null) {
                    BLEServiceManger.getInstance().getService().disconnect();
                }
            }

            rlFour.setVisibility(View.GONE);
            tvClean.setVisibility(View.GONE);
            rlTwo.setVisibility(View.GONE);
            mLeDeviceListAdapter.clear();
            mLeDeviceListAdapter.notifyDataSetChanged();

        } else if (i == R.id.tv_clean) {
            rlFour.setVisibility(View.GONE);
            tvClean.setVisibility(View.GONE);
            rlTwo.setVisibility(View.GONE);
            mLeDeviceListAdapter.clear();
            mLeDeviceListAdapter.notifyDataSetChanged();

        }
    }
    /*@OnClick({R.id.search, R.id.on_off, R.id.tv_clean})
    void OnClick(View view) {
        switch (view.getId()) {
            case R.id.search:
                if (isSearching) {
                    rlFour.setVisibility(View.GONE);
                    tvClean.setVisibility(View.GONE);
                    rlTwo.setVisibility(View.GONE);
                    isSearching = false;
                    //取消搜索
                    if (bleManager == null) {
                        bleManager = PenCommAgent.GetInstance(App2.get());
                    }
                    bleManager.stopFindAllDevices();
                    if (searchTv != null) {
                        searchTv.setText("搜索设备");
                    }
                    mLeDeviceListAdapter.notifyDataSetChanged();
                } else {
                    rlFour.setVisibility(View.VISIBLE);
                    tvClean.setVisibility(View.VISIBLE);
                    rlTwo.setVisibility(View.VISIBLE);
                    checkPermission();
                }
                break;
            case R.id.on_off:
                isOpen = !isOpen;
                mLeDeviceListAdapter.clear();
                mLeDeviceListAdapter.notifyDataSetChanged();
                changeUI();
                if (isOpen) {
                    BLEServiceManger.getInstance().getService();
                    checkAndOpenBlueTooth();
//                    if (BLEServiceManger.getInstance().getService() != null && checkAndOpenBlueTooth()) {
//                        startScan();
//                    }
                } else {
                    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                    if (adapter == null || !adapter.isEnabled()) {
                        return;
                    }
                    if (!checkAndOpenBlueTooth()) {
                        return;
                    }
                    if (CommonUtils.flag == 0) {
                        return;
                    }
                    CommonUtils.flag = 0;
                    if (bleManager == null) {
                        bleManager = PenCommAgent.GetInstance(App2.get());
                    }
                    bleManager.stopFindAllDevices();
                    if (BLEServiceManger.getInstance().getService() != null) {
                        BLEServiceManger.getInstance().getService().disconnect();
                    }
                }
            case R.id.tv_clean:
                rlFour.setVisibility(View.GONE);
                tvClean.setVisibility(View.GONE);
                rlTwo.setVisibility(View.GONE);
                mLeDeviceListAdapter.clear();
                mLeDeviceListAdapter.notifyDataSetChanged();
                break;
        }
    }*/

    /**
     * 蓝牙未开启请求
     */
    @Override
    public void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 0xb) {
            bleManager.init();
        }
    }

    /*--------------
     * 设备连接部分
     -----------------*/

    /**
     * 校验蓝牙是否打开
     * 6.0以上使用蓝牙的相关权限是否具备
     * ACCESS_COARSE_LOCATION 必须校验
     */
    public void checkPermission() {
        boolean openBlueTooth = checkAndOpenBlueTooth();
        if (!openBlueTooth) {
            return;
        }
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLeDeviceListAdapter.clear();
            mLeDeviceListAdapter.notifyDataSetChanged();
            startScan();

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }
    }

    private boolean checkAndOpenBlueTooth() {
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
        int init = bleManager.init();
        switch (init) {
            case 30001:
                showMessageAndExit("对不起，您的设备不支持蓝牙,即将退出");
                return false;
            case 30002:
                showMessageAndExit("对不起，您的设备不支持BLE ,即将退出");
                return false;
            default:
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                if (adapter == null) {
                    showMessageAndExit("对不起，您的设备不支持蓝牙,即将退出");
                    return false;
                }
                if (!adapter.isEnabled()) {
                    Toast.makeText(getActivity(), "平板蓝牙未打开，无法连接手写笔", Toast.LENGTH_SHORT).show();
                    return false;
                }
                return true;
        }
    }

    private void showMessageAndExit(String message) {
        ToastUtil.showShortlToast(message);
        getActivity().finish();
    }

    /**
     * 开始扫描Ble设备--带过滤
     */
    public void startScan() {
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
        isSearching = true;
        searchTv.setText("取消搜索");
        try {//这里try防止搜索中关闭蓝牙导致应用崩溃
            bleManager.FindAllDevices(new BLEScanner.OnBLEScanListener() {
                @Override
                public void onScanResult(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
                    Log.i("搜索设备==> " + bluetoothDevice.getAddress());
                    mLeDeviceListAdapter.addDevice(bluetoothDevice);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }

                @Override
                public void onScanFailed(BLEException e) {
                    new RuntimeException(e.getMessage()).printStackTrace();
                    if (searchTv != null) {
                        searchTv.setText("搜索设备");
                    }
                    isSearching = false;
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (searchTv != null) {
                searchTv.setText("搜索设备");
            }
            isSearching = false;
            mLeDeviceListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 停止扫描Ble设备
     */
    public void stopScan() {
        bleManager.stopFindAllDevices();
    }

    private void buildBattery() {
        int bat = CommonUtils.mBattery;
        battery.setVisibility(View.VISIBLE);
        batteryTv.setVisibility(View.VISIBLE);
        if (bat <= 10) {
            battery.setImageResource(R.mipmap.zero);
        } else if (bat <= 20) {
            battery.setImageResource(R.mipmap.twenty);
        } else if (bat <= 40) {
            battery.setImageResource(R.mipmap.forty);
        } else if (bat <= 60) {
            battery.setImageResource(R.mipmap.sixty);
        } else if (bat <= 80) {
            battery.setImageResource(R.mipmap.eighty);
        } else {
            battery.setImageResource(R.mipmap.one);
        }
        batteryTv.setText(bat + "%");
    }

    @Override
    public void onConnected() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showShortlToast("连接成功");
                bluetoothTv.setText("已配对的设备");
            }
        });
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    bleManager.getPenMac();
//                    Thread.sleep(100);
//                    bleManager.getPenPowerStatus();
////                    bleManager.getPenAllStatus();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, 5000);
    }

    @Override
    public void onDisconnected() {
        if (activity == null) {
            BLEServiceManger.getInstance().getService().removeOnDataReceiveListener(this);
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showShortlToast("断开连接");
                changeUI();
            }
        });
//        bluetoothTv.setVisibility(View.GONE);
//        tvIsConnected.setText("未连接");
//        battery.setVisibility(View.GONE);
//        batteryTv.setVisibility(View.GONE);
    }

    @Override
    public void onDataReceive(Dot dot) {
    }

    @Override
    public void onOfflineDataReceive(Dot dot) {
    }

    @Override
    public void onFinishedOfflineDown(boolean success) {
    }

    @Override
    public void onOfflineDataNum(int num) {
    }

    @Override
    public void onReceiveOIDSize(int OIDSize) {
    }

    @Override
    public void onReceiveOfflineProgress(int i) {
    }

    @Override
    public void onDownloadOfflineProgress(int i) {
    }

    @Override
    public void onReceivePenLED(byte color) {
    }

    @Override
    public void onOfflineDataNumCmdResult(boolean success) {
    }

    @Override
    public void onDownOfflineDataCmdResult(boolean success) {
    }

    @Override
    public void onWriteCmdResult(int code) {
    }

    @Override
    public void onReceivePenAllStatus(PenStatus status) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                buildBattery();
                idNumber.setText("设备ID:" + CommonUtils.mBTMac);
                tvIsConnected.setText("已连接");
            }
        });
    }
}
