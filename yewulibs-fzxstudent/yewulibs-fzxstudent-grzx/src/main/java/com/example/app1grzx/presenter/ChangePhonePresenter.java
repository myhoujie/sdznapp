package com.example.app1grzx.presenter;


import android.os.Handler;
import android.os.Message;

import com.example.app1grzx.view.ChangePhoneView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.utils.DialogUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/16
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class ChangePhonePresenter extends BasePresenter<ChangePhoneView, BaseActivity> {
    private long endTime;

    public void sendVerityCode(String tel) {
        endTime = System.currentTimeMillis() + 60 * 1000;
        mHandler.sendEmptyMessageDelayed(0, 100);
        Network.createTokenService(NetWorkService.SavePhoneService.class)
                .sendVerityCode(tel)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        stopCountDown();
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getCode() == 21202) {
                                new DialogUtils().createTipDialog(mActivity, "今天获取验证码次数已达上限，\n" +
                                        "请明天重试。").show();
                                return;
                            }
                        }
                        ToastUtil.showShortlToast("验证码发送失败");
                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }


    public void checkVerityNum(final String phoneNum, final String code) {

        Network.createService(NetWorkService.ForgetPswService.class)
                .checkVerityCode(phoneNum, code)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        mView.verifySuccess(phoneNum, code);
                       savePhone(phoneNum);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e). getCode() == 21204) {
                                stopCountDown();
                                ToastUtil.showShortlToast("验证码已过期，请重新获取");
                                return;
                            }
                        }
                        ToastUtil.showShortlToast("验证码错误，请重新输入");
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    public void savePhone(final String phoneNum) {
        Network.createTokenService(NetWorkService.SavePhoneService.class)
                .SavePhone(phoneNum)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        ToastUtil.showShortlToast("成功绑定手机号");
                        mView.savePhone();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getCode() == 21206) {
                                ToastUtil.showShortlToast("该手机号已被绑定！");
                            }
                        }

                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));

    }

    private void stopCountDown() {
        endTime = System.currentTimeMillis();
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            final int lessTime = (int) ((endTime - System.currentTimeMillis()) / 1000);

            mView.onCountDownChanged(lessTime);
            if (lessTime < 0) {
                mHandler.removeMessages(0);
            } else {
                mHandler.sendEmptyMessageDelayed(0, 100);
            }
        }
    };

    @Override
    public void detachView() {
        super.detachView();
        mHandler.removeMessages(0);
    }
}
