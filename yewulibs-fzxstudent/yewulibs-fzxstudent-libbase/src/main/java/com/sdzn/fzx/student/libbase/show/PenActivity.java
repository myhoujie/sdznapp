package com.sdzn.fzx.student.libbase.show;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.base.BaseActivity;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by 张超
 * 功能介绍：权限管理
 * 修改内容：新增
 * 修改时间：2018/4/13
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */


public abstract class PenActivity extends BaseActivity {
    private final String[] requiredPermissons = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, requiredPermissons, 0);
        } else {
            bindPenService();
        }
    }

    private void bindPenService() {
        BLEServiceManger.getInstance().startService();
        BLEServiceManger.getInstance().bindService(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0
                && grantResults.length > 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            bindPenService();
        } else {
//            Toast.makeText(this, "Require SD read/write permisson!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        BLEServiceManger.getInstance().unbindService(this);
        EventBus.getDefault().unregister(this);
    }
}
