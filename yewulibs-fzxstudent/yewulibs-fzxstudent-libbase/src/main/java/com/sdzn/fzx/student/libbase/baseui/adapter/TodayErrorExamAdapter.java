package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.vo.TodayNativeVo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/13
 * 修改单号：
 * 修改内容:
 * on 2018/12/13
 */
public class TodayErrorExamAdapter extends BaseAdapter {
    private Activity mActivity = null;
    private List<TodayNativeVo.DataBean> mlist;
    private Context mContext;

    public TodayErrorExamAdapter(List mlist, Context mContext, Activity activity) {
        this.mlist = mlist;
        this.mContext = mContext;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int i) {
        return mlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_today_subject_title, viewGroup, false);
            holder = new ViewHolder();

            holder.subjectImg = (ImageView) convertView.findViewById(R.id.subject_icon);
            holder.subjectTv = (TextView) convertView.findViewById(R.id.subject_text);
            holder.recyclerView = (RecyclerView) convertView.findViewById(R.id.recyclerView);
            holder.subjectCount = (TextView) convertView.findViewById(R.id.subject_count);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.subjectTv.setText(mlist.get(i).getBaseSubjectName());
        ToadyExamAdapter toadyExamAdapter = new ToadyExamAdapter(mContext, mlist.get(i).getExamList(), mActivity);
        holder.subjectTv.setText(mlist.get(i).getBaseSubjectName());
        holder.subjectCount.setText("共" + mlist.get(i).getExamList().size() + "道");
        Subject subject = Subject.subjects.get(mlist.get(i).getBaseSubjectId());
        if (subject != null) {
            holder.subjectImg.setBackground(mContext.getResources().getDrawable(subject.getDrawableId()));
        }

        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        holder.recyclerView.setAdapter(toadyExamAdapter);
        return convertView;
    }

    static class ViewHolder {

        ImageView subjectImg;
        TextView subjectTv;
        TextView subjectCount;
        RecyclerView recyclerView;
    }
}
