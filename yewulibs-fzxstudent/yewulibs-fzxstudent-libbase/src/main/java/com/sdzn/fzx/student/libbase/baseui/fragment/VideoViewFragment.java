package com.sdzn.fzx.student.libbase.baseui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.baseui.activity.PlayerActivity;
import com.sdzn.fzx.student.libpublic.views.VerticalSeekBar;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;

import java.util.Timer;
import java.util.TimerTask;


/**
 * 描述：视频播放
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/17
 */
public class VideoViewFragment extends PlayerFragment implements View.OnClickListener {

    /*@ViewInject(R.id.rl_title)
    RelativeLayout rlVideoTitle;
    @ViewInject(R.id.rlVideoArea)
    RelativeLayout rlVideoArea;
    @ViewInject(R.id.surface_view)
    VideoView mVideoView;
    @ViewInject(R.id.pb_loading)
    ProgressBar loadingPb;
    @ViewInject(R.id.rlController)
    RelativeLayout rlController;
    @ViewInject(R.id.ivPlay)
    ImageView ivPlay;
    @ViewInject(R.id.tvPlayTime)
    TextView tvPlayTime;
    @ViewInject(R.id.seekbar)
    SeekBar seekbar;
    @ViewInject(R.id.volumeSeekBar)
    VerticalSeekBar volumeSeekBar;
    @ViewInject(R.id.llVolumeSeekBar)
    LinearLayout llVolumeSeekBar;
    @ViewInject(R.id.tvTotalTime)
    TextView tvTotalTime;
    @ViewInject(R.id.ivVolume)
    ImageView ivVolume;
    @ViewInject(R.id.tv_video_title)
    TextView tvVideoTitle;*/

    private RelativeLayout rlVideo;
    private RelativeLayout rlVideoArea;
    private VideoView mVideoView;
    private RelativeLayout rlVideoTitle;
    private TextView tvVideoTitle;
    private TextView tvClose;
    private ProgressBar loadingPb;
    private RelativeLayout rlController;
    private ImageView ivVolume;
    private LinearLayout llVolumeSeekBar;
    private VerticalSeekBar volumeSeekBar;
    private LinearLayout llProgress;
    private ImageView ivPlay;
    private TextView tvPlayTime;
    private SeekBar seekbar;
    private TextView tvTotalTime;

    private String path;
    private Timer timer = new Timer();
    private TimerTask timerTask;
    private boolean isDisplay = false;
    private static final int MSG_CONTROLLER = 0;
    private static final int HIDE_TIME = 3000;
    private int width;
    private int height;
    private String title;
    private AudioManager mAudioManager;
    private MyVolumeReceiver mVolumeReceiver;
    private boolean isTracking = false;
    private boolean isPlaying = false;
    private long currentPos;
    private MediaPlayer mMediaPlayer;

    public static VideoViewFragment newInstance(Bundle bundle) {
        VideoViewFragment fragment = new VideoViewFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_view, null);
//        Vitamio.isInitialized(activity);
        rlVideo = (RelativeLayout) view.findViewById(R.id.rl_video);
        rlVideoArea = (RelativeLayout) view.findViewById(R.id.rlVideoArea);
        mVideoView = (VideoView) view.findViewById(R.id.surface_view);
        rlVideoTitle = (RelativeLayout) view.findViewById(R.id.rl_title);
        tvVideoTitle = (TextView) view.findViewById(R.id.tv_video_title);
        tvClose = (TextView) view.findViewById(R.id.tv_close);
        loadingPb = (ProgressBar) view.findViewById(R.id.pb_loading);
        rlController = (RelativeLayout) view.findViewById(R.id.rlController);
        ivVolume = (ImageView) view.findViewById(R.id.ivVolume);
        llVolumeSeekBar = (LinearLayout) view.findViewById(R.id.llVolumeSeekBar);
        volumeSeekBar = (VerticalSeekBar) view.findViewById(R.id.volumeSeekBar);
        llProgress = (LinearLayout) view.findViewById(R.id.llProgress);
        ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
        tvPlayTime = (TextView) view.findViewById(R.id.tvPlayTime);
        seekbar = (SeekBar) view.findViewById(R.id.seekbar);
        tvTotalTime = (TextView) view.findViewById(R.id.tvTotalTime);
        //x.view().inject(this, view);
        initView();
        playFunction(view);
        return view;
    }

    private void initView() {
        ivPlay.setOnClickListener(this);
        ivVolume.setOnClickListener(this);
        tvClose.setOnClickListener(this);

        tvVideoTitle.setText(title);
        // 解决thumb透明问题
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekbar.setSplitTrack(false);
            volumeSeekBar.setSplitTrack(false);
        }

        rlVideoArea.setOnTouchListener(touchListener);
        SurfaceHolder holder = mVideoView.getHolder();
        holder.setFormat(PixelFormat.RGBX_8888);
        rlController.setVisibility(View.VISIBLE);
//        if (setScreenSize) {
//            ivFullScreen.setVisibility(View.VISIBLE);
//        } else {
//            ivFullScreen.setVisibility(View.GONE);
//        }
        seekbar.setMax(100);

        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        int max = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeSeekBar.setMax(max);
        volumeSeekBar.setProgress(current);

        setVolumeImage(max, current);

        mVolumeReceiver = new MyVolumeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        getActivity().registerReceiver(mVolumeReceiver, filter);

        setListener();
    }

    private void setListener() {
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        volumeSeekBar.setOnSeekBarChangeListener(onVolumeSeekBarChangeListener);
    }

    /**
     * 处理音量变化时的界面显示
     *
     * @author long
     */
    private class MyVolumeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //如果音量发生变化则更改seekbar的位置
            int currVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);// 当前的媒体音量
            volumeSeekBar.setProgress(currVolume);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public void onDestroy() {
        if (mVolumeReceiver != null) {
            getActivity().unregisterReceiver(mVolumeReceiver);
        }
        if (playerHandler != null) {
            playerHandler.removeCallbacksAndMessages(null);
            playerHandler = null;
        }
        if (controllerHandler != null) {
            controllerHandler.removeCallbacksAndMessages(null);
            controllerHandler = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroy();
    }

    protected void initData() {
        path = getArguments().getString("path");
        title = getArguments().getString("title");
        width = getArguments().getInt("width");
        height = getArguments().getInt("height");
    }

    private void playFunction(View view) {
        if (TextUtils.isEmpty(path)) {
            ToastUtil.showShortlToast(getString(R.string.fragment_task_content_text12));
        } else {
            mVideoView.setVideoPath(path);
//            mVideoView.setVideoURI(Uri.parse(path));
            mVideoView.requestFocus();

//            mVideoView.setOnBufferingUpdateListener(onBufferingUpdateListener);
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer = mp;
                    mMediaPlayer.setOnBufferingUpdateListener(onBufferingUpdateListener);
                    mMediaPlayer.start();
                    setScreenSizeParams(width, height);
//                    mediaPlayer.setPlaybackSpeed(1.0f);
                    long duration = mVideoView.getDuration();
                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    tvTotalTime.setText(DateUtil.millsecondsToStr(duration));
                    isPlaying = true;
                    hideControllerDelayed();
                    loadingPb.setVisibility(View.GONE);
//                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    timerTask = new TimerTask() {
                        @Override
                        public void run() {
                            playerHandler.sendEmptyMessage(0);
                        }
                    };
                    timer.schedule(timerTask, 0, 1000);
//                    controllerHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            mVideoView.setBackgroundColor(Color.TRANSPARENT);
//                        }
//                    }, 100);
//                    isPlaying = true;
                }
            });
//            mVideoView.setOnStartListener(new VideoView.OnStartListener() {
//                @Override
//                public void onStart(MediaPlayer mediaPlayer) {
//
//                }
//            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    ((PlayerActivity) getActivity()).back();
                }
            });
        }
    }

    MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            seekbar.setSecondaryProgress((int) (((float) percent / (float) mp.getDuration()) * seekbar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            long time = seekBar.getProgress() * mVideoView.getDuration() / seekBar.getMax();
            if (time == 0) {
                time += 1000;
            }
            mVideoView.seekTo((int) time);
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
            isTracking = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
            isTracking = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            tvPlayTime.setText(DateUtil.millsecondsToStr(progress * mVideoView.getDuration() / seekBar.getMax()));
        }
    };

    SeekBar.OnSeekBarChangeListener onVolumeSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            controllerHandler.removeMessages(MSG_CONTROLLER);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, AudioManager.FLAG_VIBRATE);
            setVolumeImage(seekBar.getMax(), progress);
        }
    };

    public void setVolumeImage(int max, int progress) {
        if (progress > max / 2) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangda_icon));
        } else if (progress == 0) {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangwu_icon));
        } else {
            ivVolume.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.yinliangxiao_icon));
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.ivPlay) {
            hideControllerDelayed();
            if (mVideoView.isPlaying()) {
                mVideoView.pause();
                ivPlay.setImageResource(R.mipmap.bofang_icon);
                isPlaying = false;
                updateProgress();
            } else {
                mVideoView.start();
                ivPlay.setImageResource(R.mipmap.zanting_icon);
                isPlaying = true;
                updateProgress();
            }

        } else if (i == R.id.ivVolume) {
            int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            volumeSeekBar.setProgress(current);
            llVolumeSeekBar.setVisibility(View.VISIBLE);
            // 设置全屏
//                if (mVideoView.isPlaying()) {
//                    hideControllerDelayed();
//                }
//                if (!isFullScreen) {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.FULL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.play_normal);
//                    isFullScreen = true;
//                } else {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.NORMAL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.fullscreen);
//                    isFullScreen = false;
//                }

        } else if (i == R.id.tv_close) {
            ((PlayerActivity) getActivity()).back();

        } else {
        }
    }
/*
    @Event(value = {R.id.ivPlay, R.id.ivVolume, R.id.tv_close, R.id.llProgress, R.id.rl_title, R.id.llVolumeSeekBar}, type = View.OnClickListener.class)
    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivPlay:
                hideControllerDelayed();
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    ivPlay.setImageResource(R.mipmap.bofang_icon);
                    isPlaying = false;
                    updateProgress();
                } else {
                    mVideoView.start();
                    ivPlay.setImageResource(R.mipmap.zanting_icon);
                    isPlaying = true;
                    updateProgress();
                }
                break;
            case R.id.ivVolume:
                int current = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                volumeSeekBar.setProgress(current);
                llVolumeSeekBar.setVisibility(View.VISIBLE);
                // 设置全屏
//                if (mVideoView.isPlaying()) {
//                    hideControllerDelayed();
//                }
//                if (!isFullScreen) {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.FULL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.play_normal);
//                    isFullScreen = true;
//                } else {
//                    ((PlayerActivity) activity).setScreen(PlayerActivity.NORMAL_SCREEN);
//                    ivFullScreen.setImageResource(R.drawable.fullscreen);
//                    isFullScreen = false;
//                }
                break;
            case R.id.tv_close:
                ((PlayerActivity) activity).back();
                break;
            default:
                break;
        }
    }*/

    Handler playerHandler = new Handler() {
        public void handleMessage(Message msg) {
            // 更新播放进度
            if (isPlaying) {
                updateProgress();
            }
        }
    };

    private void updateProgress() {
        long position = mVideoView.getCurrentPosition();
        long duration = mVideoView.getDuration();
        Log.d("position:::" + position);
        if (duration > 0) {
            long pos = seekbar.getMax() * position / duration;
            tvPlayTime.setText(DateUtil.millsecondsToStr(position));
            if (!isTracking) {
                seekbar.setProgress((int) pos);
            }
        }
    }

    /**
     * 控制播放器面板显示
     */
    private View.OnTouchListener touchListener = new View.OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                controllerHandler.removeMessages(MSG_CONTROLLER);
                if (isDisplay) {
                    setLayoutVisibility(View.GONE, false);
                } else {
                    setLayoutVisibility(View.VISIBLE, true);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                hideControllerDelayed();
            }
            return true;
        }
    };

    private void hideControllerDelayed() {
        controllerHandler.removeMessages(MSG_CONTROLLER);
        controllerHandler.sendEmptyMessageDelayed(MSG_CONTROLLER, HIDE_TIME);
    }

    private void setLayoutVisibility(int visibility, boolean isDisplay) {
        this.isDisplay = isDisplay;
        rlController.setVisibility(visibility);
        rlVideoTitle.setVisibility(visibility);
        llVolumeSeekBar.setVisibility(View.GONE);
    }

    Handler controllerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CONTROLLER:
                    setLayoutVisibility(View.GONE, false);
                    break;
            }
        }
    };

    @Override
    public void setScreenSizeParams(int width, int height) {
        int vWidth;
        int vHeight;
        if (mMediaPlayer == null) {
            vWidth = 0;
            vHeight = 0;
        } else {
            vWidth = mMediaPlayer.getVideoWidth();
            vHeight = mMediaPlayer.getVideoHeight();
        }

        if (vWidth > width || vHeight > height) {
            float wRatio = (float) vWidth / (float) width;
            float hRatio = (float) vHeight / (float) height;
            float ratio = Math.max(wRatio, hRatio);

            width = (int) Math.ceil((float) vWidth / ratio);
            height = (int) Math.ceil((float) vHeight / ratio);
        } else {
            float wRatio = (float) width / (float) vWidth;
            float hRatio = (float) height / (float) vHeight;
            float ratio = Math.min(wRatio, hRatio);

            width = (int) Math.ceil((float) vWidth * ratio);
            height = (int) Math.ceil((float) vHeight * ratio);
        }

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlVideoArea.getLayoutParams();
        params.width = width;
        params.height = height;
        rlVideoArea.setLayoutParams(params);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mVideoView.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        mVideoView.setLayoutParams(layoutParams);
        // 调用这个方法可以让视频大小适应父控件大小
//        mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);
    }

    @Override
    public void onResume() {
        if (mVideoView != null) {
            mVideoView.seekTo((int) currentPos);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mVideoView != null) {
            currentPos = mVideoView.getCurrentPosition();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (mVideoView != null) {
            mVideoView.stopPlayback();
        }
        super.onDestroyView();
    }
}