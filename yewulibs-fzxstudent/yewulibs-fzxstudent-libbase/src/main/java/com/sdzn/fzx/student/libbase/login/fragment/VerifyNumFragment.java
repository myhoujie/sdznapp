package com.sdzn.fzx.student.libbase.login.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.login.activity.ForgetPswActivity;
import com.sdzn.fzx.student.libbase.login.presenter.VerifyNumPresenter;
import com.sdzn.fzx.student.libbase.login.presenter.VerifyNumPresenter1;
import com.sdzn.fzx.student.libbase.login.view.VerifyNumView;
import com.sdzn.fzx.student.libbase.login.view.VerifyNumViews;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libbase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyNumFragment extends BaseFragment implements VerifyNumViews, View.OnClickListener {
    VerifyNumPresenter1 mPresenter;
    public static final String USER_PHONE = "user_phone";
    private RelativeLayout titleContainerRy;
    private LinearLayout titleBackLy;
    private TextView titleBackTxt;
    private TextView titleNameTxt;
    private TextView tipTxt;
    private Button verityNumIcon;
    private EditText verityNumEdit;
    private TextView countDownBtn;
    private Button nextBtn;


    private String phoneNum;

    public VerifyNumFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verify_num, container, false);
        titleContainerRy = (RelativeLayout) view.findViewById(R.id.title_container_ry);
        titleBackLy = (LinearLayout) view.findViewById(R.id.title_back_ly);
        titleBackTxt = (TextView) view.findViewById(R.id.title_back_txt);
        titleNameTxt = (TextView) view.findViewById(R.id.title_name_txt);
        tipTxt = (TextView) view.findViewById(R.id.tip_txt);
        verityNumIcon = (Button) view.findViewById(R.id.verity_num_icon);
        verityNumEdit = (EditText) view.findViewById(R.id.verity_num_edit);
        countDownBtn = (TextView) view.findViewById(R.id.count_down_btn);
        nextBtn = (Button) view.findViewById(R.id.next_btn);
        initPresenter();
        initView();
        return view;
    }


    public void initPresenter() {
        mPresenter = new VerifyNumPresenter1();
        mPresenter.onCreate(this);//, (ForgetPswActivity) getActivity()
    }

    private void initView() {
        titleBackLy.setOnClickListener(this);
        countDownBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);


        titleNameTxt.setText("找回密码");
        phoneNum = getArguments().getString(USER_PHONE);
        final String userPhone = StringUtils.formatPhoneNum(phoneNum);
        final String tipStr = "我们已发送 <b>验证码</b> 短信到您的手机：<b>" + userPhone + "</b>";
        tipTxt.setText(Html.fromHtml(tipStr));

        verityNumEdit.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(verityNumEdit.getText())) {

                    mPresenter.sendVerityCode(phoneNum);
                }
            }
        }, 100);


        verityNumEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                verityNumIcon.setEnabled(!TextUtils.isEmpty(charSequence));
                nextBtn.setEnabled(!TextUtils.isEmpty(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.title_back_ly) {
            getActivity().onBackPressed();
        } else if (i == R.id.count_down_btn) {
            mPresenter.sendVerityCode(phoneNum);
        } else if (i == R.id.next_btn) {
            mPresenter.checkVerityNum(phoneNum, verityNumEdit.getText().toString());
        }
    }

    @Override
    public void onCountDownChanged(int lessTime) {
        if (countDownBtn == null)
            return;

        if (lessTime < 0) {
            String str = "<font color='#FFFFFF'>重新获取</font>";
            countDownBtn.setEnabled(true);
            countDownBtn.setText(Html.fromHtml(str));
        } else {
            String str = "<font color='#4291FF'>" + (lessTime + 1) + "</font>"
                    + "<font color='#808FA3'>秒后重发</font>";
            countDownBtn.setEnabled(false);
            countDownBtn.setText(Html.fromHtml(str));
        }
    }

    @Override
    public void verifySuccess(final String phone, final String code) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        ResetPswFragment fragment = new ResetPswFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ResetPswFragment.USER_PHONE, phone);
        bundle.putString(ResetPswFragment.VERIFT_CODE, code);
        fragment.setArguments(bundle);


        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
        transaction.addToBackStack("tag");
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
