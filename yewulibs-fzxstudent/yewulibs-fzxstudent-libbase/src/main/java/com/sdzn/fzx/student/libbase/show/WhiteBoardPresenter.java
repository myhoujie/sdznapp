package com.sdzn.fzx.student.libbase.show;

import android.graphics.RectF;

import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.dao.DotsDao;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.CommonDaoUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * @author Reisen at 2018-08-06
 */
public class WhiteBoardPresenter extends BasePresenter<WhiteBoardView, WhiteBoardWithMethodActivity> {

    /**
     * 读数据库加载本地数据
     */
    public void loadLocalData(final String appNum, final String appId) {
        Subscription subscribe = CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .rx()
                .loadAll()
                .compose(new Observable.Transformer<List<Dots>, List<Dots>>() {
                    @Override
                    public Observable<List<Dots>> call(Observable<List<Dots>> listObservable) {
                        return listObservable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).map(new Func1<List<Dots>, List<Dots>>() {
                    @Override
                    public List<Dots> call(List<Dots> list) {
                        if (appNum == null) {
                            throw new RuntimeException("appNum is NULL");
                        }
                        if (appId == null) {
                            throw new RuntimeException("appId is NULL");
                        }
                        if (list == null) {
                            return new ArrayList<>();
                        }
                        ArrayList<Dots> dots1 = new ArrayList<>();
                        for (Dots dots : list) {
                            if (appNum.equals(dots.appNum) && appId.equals(dots.appId)) {
                                dots1.add(dots);
                            }
                        }
                        return dots1;
                    }
                }).subscribe(new ProgressSubscriber<>(new SubscriberListener<List<Dots>>() {
                    @Override
                    public void onNext(List<Dots> list) {
                        mView.loadLocalDataSuccess(list);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.showShortlToast(e.getMessage());
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity, true, false, false, ""));
    }

    public void deleteDots(final String appNum, final String appId) {
        CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .queryBuilder()
                .where(DotsDao.Properties.AppNum.eq(appNum), DotsDao.Properties.AppId.eq(appId))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }

    public void saveDots(Collection<Dots> list, final int flag, final RectF rectF) {
        if (list == null) {
            return;
        }
        CommonDaoUtils.getInstance()
                .getDaoSession()
                .getDotsDao()
                .rx()
                .saveInTx(list)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener() {
                    @Override
                    public void onNext(Object o) {
                        mView.saveDotsSuccess(flag, rectF);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.saveDotsFailed(e.getMessage(), flag);
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity, true, false, false, ""));
    }
}
