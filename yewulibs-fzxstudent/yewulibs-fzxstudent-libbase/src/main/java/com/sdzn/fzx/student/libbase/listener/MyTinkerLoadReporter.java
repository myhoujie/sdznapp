package com.sdzn.fzx.student.libbase.listener;

import android.content.Context;

import com.sdzn.fzx.student.libpublic.event.TinkerResult;
import com.tencent.tinker.lib.reporter.DefaultLoadReporter;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/29.
 */
public class MyTinkerLoadReporter extends DefaultLoadReporter {
    public MyTinkerLoadReporter(Context context) {
        super(context);
    }

    @Override
    public void onLoadPatchListenerReceiveFail(File patchFile, int errorCode) {
        super.onLoadPatchListenerReceiveFail(patchFile, errorCode);
        EventBus.getDefault().post(new TinkerResult(false,0));
    }
}
