package com.sdzn.fzx.student.libbase.baseui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.baseui.adapter.ErrorSubjectNativeAdapter;
import com.sdzn.fzx.student.libbase.baseui.presenter.ErrorNativePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.ErrorNativeView;
import com.sdzn.fzx.student.libbase.listener.OnSearchClickListener;
import com.sdzn.fzx.student.libbase.pop.TaskSearchPop;
import com.sdzn.fzx.student.libpublic.event.DeleteEvent;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.views.ImageHintEditText;
import com.sdzn.fzx.student.libpublic.views.SpinerPopWindow;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.ExamTypeVo;
import com.sdzn.fzx.student.vo.SubjectErrorBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/12/11
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */
public class ErrorNativeActivity extends MBaseActivity<ErrorNativePresenter> implements ErrorNativeView, View.OnClickListener {

    public static final String ERROR_TAG = "save_error";
    public static final String COLLECT_TAG = "save_collect";
    public static final String BASESUBJECT_ID_TAG = "baseSubjectId";
    public static final String BASESUBJECT_NAME_TAG = "baseSubjectName";
    public static final String BASESUBJECT_COUNT = "subject_count";
    public static final String IS_ERROR = "is_error";

    private String baseSubjectId;
    private String baseSubjectName;
    private String baseSubjectCount;
    private String flag;
    private String saveName = ERROR_TAG;

    private List<String> examTypeNameList = new ArrayList<>();
    private List<ExamTypeVo.DataBean> examVoList = new ArrayList<>();
    private TaskSearchPop taskSearchPop;
    private SpinerPopWindow examTypeSpiner;
    private SpinerPopWindow timeSpiner;

    private static final String[] time_filter = {"全部时间", "近一周", "近一个月"};
    private static final String[] timeIds = {"3", "1", "2"};

    private ErrorSubjectNativeAdapter errorSubjectNativeAdapter;
    private List<SubjectErrorBean.DataBean> mList = new ArrayList<>();

    /*@BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvCommit)
    TextView tvCommit;
    @BindView(R.id.btnSearch)
    ImageHintEditText btnSearch;
    @BindView(R.id.net_error_ry)
    RelativeLayout netErrorRy;
    @BindView(R.id.exam_type_txt)
    TextView examTypeTxt;
    @BindView(R.id.time_txt)
    TextView timeTxt;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.llTaskEmpty)
    LinearLayout llTaskEmpty;*/


    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvTitle;
    private TextView tvCommit;
    private TextView examTypeTxt;
    private TextView timeTxt;
    private ImageHintEditText btnSearch;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private RecyclerView mRecyclerView;


    @Override
    public void initPresenter() {
        mPresenter = new ErrorNativePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_native);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvCommit = (TextView) findViewById(R.id.tvCommit);
        examTypeTxt = (TextView) findViewById(R.id.exam_type_txt);
        timeTxt = (TextView) findViewById(R.id.time_txt);
        btnSearch = (ImageHintEditText) findViewById(R.id.btnSearch);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        examTypeTxt.setOnClickListener(this);
        timeTxt.setOnClickListener(this);
        tvBack.setOnClickListener(this);
        btnSearch.setOnClickListener(this);


        errorSubjectNativeAdapter = new ErrorSubjectNativeAdapter(App2.get(), mList, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        mRecyclerView.setAdapter(errorSubjectNativeAdapter);
        initData();
        initView();
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(ErrorNativeActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        if (TextUtils.equals("1", flag)) {
            tvTitle.setText(baseSubjectName + "错题本");
            saveName = ERROR_TAG;
            timeTxt.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setText(baseSubjectName + "收藏试题");
            saveName = COLLECT_TAG;
            timeTxt.setVisibility(View.GONE);
        }
        tvCommit.setText(baseSubjectCount);
        //  setXWalkView();

        examTypeSpiner = new SpinerPopWindow(this);
        examTypeSpiner.setPopDatas(examTypeNameList);

        timeSpiner = new SpinerPopWindow(this);
        timeSpiner.setPopDatas(time_filter);

        examTypeSpiner.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                mPresenter.setExamTemplateStyleId(examVoList.get(pos).getId());
                examTypeTxt.setText(examTypeNameList.get(pos));
                getData();
            }
        });
        timeSpiner.setItemListener(new SpinerPopWindow.IOnItemSelectListener() {
            @Override
            public void onItemClick(int pos) {
                mPresenter.setTimeType(timeIds[pos]);
                timeTxt.setText(time_filter[pos]);
                getData();
            }
        });
    }


    @Override
    protected void initData() {

        baseSubjectId = getIntent().getStringExtra(BASESUBJECT_ID_TAG);
        baseSubjectName = getIntent().getStringExtra(BASESUBJECT_NAME_TAG);
        baseSubjectCount = getIntent().getStringExtra(BASESUBJECT_COUNT);
        flag = getIntent().getStringExtra(IS_ERROR);
        mPresenter.setBaseSubjectId(baseSubjectId);
        mPresenter.setModel(flag);
        examTypeNameList.clear();
        examTypeNameList.add("全部题型");
        examVoList.clear();
        final ExamTypeVo.DataBean dataBean = new ExamTypeVo.DataBean();
        examVoList.add(dataBean);
        mPresenter.getExamTypeList();
        getData();
    }

    void getData() {
        mPresenter.getSubjectErrorList(mPresenter.getParams());
    }

    @Override
    public void onExamTypeSuccess(List<ExamTypeVo.DataBean> list, List<String> strings) {
        examVoList.addAll(list);
        examTypeNameList.addAll(strings);
        examTypeSpiner.setPopDatas(examTypeNameList);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.exam_type_txt) {
            examTypeSpiner.showAsDropDown(examTypeTxt);
        } else if (i == R.id.time_txt) {
            timeSpiner.showAsDropDown(timeTxt);
        } else if (i == R.id.tvBack) {
            finish();
        } else if (i == R.id.btnSearch) {
            showPop();
        }
    }
/*
    @OnClick({R.id.exam_type_txt, R.id.time_txt, R.id.tvBack, R.id.btnSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exam_type_txt:
                examTypeSpiner.showAsDropDown(examTypeTxt);
                break;
            case R.id.time_txt:
                timeSpiner.showAsDropDown(timeTxt);
                break;
            case R.id.tvBack:
                finish();
                break;
            case R.id.btnSearch:
                showPop();
                break;
        }
    }
*/

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(activity, new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {

                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    mPresenter.setChapterName(searchStr);
                    getData();
                }
            });
        }

        taskSearchPop.showPopupWindow(saveName, btnSearch, Integer.parseInt(baseSubjectId));
    }

    @Override
    public void setSubjectListSuccess(SubjectErrorBean subjectErrorBean) {

        if (subjectErrorBean.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {
            llTaskEmpty.setVisibility(View.GONE);
            mList.clear();
            mList.addAll(subjectErrorBean.getData());
            refreshAdapter();
            //errorSubjectNativeAdapter.refreshAdapter();
        }

    }

    private void refreshAdapter() {
        errorSubjectNativeAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(Event event) {
        if (event instanceof DeleteEvent) {
            getData();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
