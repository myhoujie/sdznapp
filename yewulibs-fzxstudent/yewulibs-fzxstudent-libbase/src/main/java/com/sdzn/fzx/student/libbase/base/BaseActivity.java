package com.sdzn.fzx.student.libbase.base;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.baselibrary.base.BaseAppManager;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.utils.Timer;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeCompat;


/**
 * 描述：所有Activity的基类，任何Activity必须继承它
 * －
 * import android.app.Activity;
 * import android.os.Build;
 * import android.os.Bundle;
 * <p>
 * <p>
 * import android.support.v4.app.FragmentActivity;
 * import android.view.WindowManager;
 * <p>
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.app.AppContext;
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.ui.activity.ActivityManager;
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.utils.ProgressDialogManager;
 * 创建人：zhangchao
 * 创建时间：17/3/20
 */
public abstract class BaseActivity extends AppCompatActivity {

    //    protected Application appContext;
    protected BaseActivity activity;
    protected float density;
    protected ProgressDialogManager mManager;
    private NetworkReceiver networkReceiver;
    //    public BLEServiceManger mBLEServiceManger;
    private Handler handler;
    private long mCurrentMs = System.currentTimeMillis();

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources());//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity(super.getResources(), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        BaseAppManager.getInstance().add(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            //透明状态栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        StatusBarUtil.setLightMode(this);
//        appContext = getApplication();
        activity = this;
        density = getResources().getDisplayMetrics().density;
        mManager = new ProgressDialogManager(this);
        ActivityManager.addLiveActivity(this);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        handler = new Handler(Looper.myLooper());
        checkNetwork();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //监听横竖屏变化导致
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d("width", "横屏");
            AutoSize.autoConvertDensityOfGlobal(this); //如果没有自定义需求用这个方法
        } else {
            Log.d("width", "竖屏");
        }
    }

    private void checkNetwork() {
        new Timer(getApplication()).start(30 * 1000);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkReceiver = new NetworkReceiver();
        getApplication().registerReceiver(networkReceiver, filter);

    }

    public abstract class OnMultiClickListener implements View.OnClickListener {
        // 两次点击按钮之间的点击间隔不能少于1000毫秒
        private final int MIN_CLICK_DELAY_TIME = 1500;
        private long lastClickTime;

        public abstract void onMultiClick(View v);

        @Override
        public void onClick(View v) {
            long curClickTime = System.currentTimeMillis();
            if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
                // 超过点击间隔后再将lastClickTime重置为当前点击时间
                lastClickTime = curClickTime;
                onMultiClick(v);
            }
        }
    }

    /**
     * 初始化组件
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    @Override
    protected void onStart() {
        super.onStart();
        ActivityManager.addVisibleActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityManager.removeVisibleActivity(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityManager.addForegroundActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ActivityManager.removeForegroundActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.removeLiveActivity(this);
        EventBus.getDefault().unregister(this);
        getApplication().unregisterReceiver(networkReceiver);
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginSuccess(String msg) {
        if (TextUtils.equals(Event.ONLOGIN_SUCCESS, msg)) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    SchoolBoxWatcher.getInstance().getBoxUrl();
                    SchoolBoxWatcher.getInstance().stopWatch();
                    SchoolBoxWatcher.getInstance().startWatch();
                }
            }, 5000);
        } else if (TextUtils.equals(Event.EVENT_TOKEN_MISS, msg)) {
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplication().startActivity(intent);
            ActivityManager.finishAll();
        }
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}