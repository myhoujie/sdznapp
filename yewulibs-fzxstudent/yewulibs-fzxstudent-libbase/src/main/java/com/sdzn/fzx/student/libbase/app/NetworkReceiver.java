package com.sdzn.fzx.student.libbase.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sdzn.fzx.student.libutils.util.AndroidUtil;

import org.greenrobot.eventbus.EventBus;

public class NetworkReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final boolean netConnectOk = AndroidUtil.isNetConnectOk(context);
        EventBus.getDefault().post(new NetworkEvent(netConnectOk));
        if (netConnectOk) {
            SchoolBoxWatcher.getInstance().stopWatch();
            SchoolBoxWatcher.getInstance().startWatch();
        }
    }

    public static class NetworkEvent {
        public boolean isWork;

        public NetworkEvent(boolean isWork) {
            this.isWork = isWork;
        }
    }
}
