package com.sdzn.fzx.student.libbase.app;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.RelativeLayout;

import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libpublic.views.DrawImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.tqltech.tqlpencomm.Dot;
import com.tqltech.tqlpencomm.PenCommAgent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 手写笔属性类
 * @author Created by Reisen on 2019/2/28.
 */
public class HandWriteManager {

    // A5
    private double A5_WIDTH = 182.86;//横坐标相对坐标原点的偏移量
    private double A5_HEIGHT = 256.0;//纵坐标相对坐标原点的偏移量
    public int BG_REAL_WIDTH = 1075;
    public int BG_REAL_HEIGHT = 1512;
    public int BG_WIDTH;//控件宽
    public int BG_HEIGHT;//控件高
    public int A5_X_OFFSET;
    public int A5_Y_OFFSET;
    private float mov_x;//声明起点坐标
    private float mov_y;//声明起点坐标
    public static float mWidth;//屏幕宽
    public static float mHeight;//屏幕高
    public int gCurPageID = -1;
    private int gCurBookID = -1;

    private static final double XDIST_PERUNIT = 1.524;
    private static final double YDIST_PERUNIT = 1.524;

    private ArrayList<Dots> dot_number = new ArrayList<>();
    private ArrayList<Dots> dot_number1 = new ArrayList<>();
    private ArrayList<Dots> dot_number2 = new ArrayList<>();
    private ArrayList<Dots> dot_number4 = new ArrayList<>();

    private float fromX, toX;
    private float fromY, toY;

    private boolean gbSetNormal = false;


    private byte[] bookID = new byte[]{0, 1, 100};
    private Boolean bBookIDIsValide;
    private PenCommAgent bleManager;
    private DrawImageView ivDraw;
    private boolean bIsReply;

    //这里y偏移用于落笔时重新定位笔迹, 切页时归零
    private int yOffset;//y偏移

    private boolean isTouch;//是否有书写

    //=====================  =====================
    private HandWriteCallBack mCallBack;
    private ProgressDialogManager dialogManager;

    public HandWriteManager(Activity activity, HandWriteCallBack callBack, DrawImageView drawImageView) {
        dialogManager = new ProgressDialogManager(activity);
        mCallBack = callBack;
        ivDraw = drawImageView;
        if (bleManager == null) {
            bleManager = PenCommAgent.GetInstance(App2.get());
        }
    }

    //=====================  =====================

    public void drawInit() {
        if (gCurPageID == -1 && gCurBookID == -1) {
            ivDraw.initDraw(ivDraw.getContext());
        }
        ivDraw.paint.setStrokeCap(Paint.Cap.ROUND);
        ivDraw.paint.setStyle(Paint.Style.FILL);
        ivDraw.paint.setAntiAlias(true);
        ivDraw.invalidate();
    }

    /**
     * 加载本地数据成功回调
     */
    public void loadLocalDataSuccess(List<Dots> list) {
        if (bIsReply) {
            return;
        }
        bIsReply = true;
        dialogManager.showWaiteDialog("正在加载中，请稍后...",false);
        cleanDots();
        for (Dots dots : list) {
            switch (dots.bookID) {
                case 100:
                    gCurBookID = 100;
                    gCurPageID = dots.pageID;
                    dot_number.add(dots);
                    break;
                case 0:
                    gCurBookID = 0;
                    gCurPageID = dots.pageID;
                    dot_number1.add(dots);
                    break;
                case 1:
                    gCurBookID = 1;
                    gCurPageID = dots.pageID;
                    dot_number2.add(dots);
                    break;
            }
        }
        replay();
    }

    /**
     * 重绘内容
     */
    private void replay() {
        drawInit();
        if (gCurBookID == 100) {//这里用book100
            dot_number4 = dot_number;
        } else if (gCurBookID == 0) {
            dot_number4 = dot_number1;
        } else if (gCurBookID == 1) {
            dot_number4 = dot_number2;
        }

        if (dot_number4.isEmpty()) {
            bIsReply = false;
            dialogManager.cancelWaiteDialog();
            return;
        }

        Dots lastDot = dot_number4.get(dot_number4.size() - 1);
        int lastPage = lastDot.getPageID();
        setBackgroundImage(lastDot.getBookID(), lastDot.getPageID());
        for (Dots dot : dot_number4) {
            if (dot.getPageID() == lastPage) {
                gbSetNormal = false;
                bBookIDIsValide = false;
                if (gCurBookID != dot.bookID || gCurPageID != dot.pageID) {
                    gCurPageID = dot.pageID;
                    gCurBookID = dot.bookID;
                }
                drawInit();
                drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
            }
        }
        dialogManager.cancelWaiteDialog();
        bIsReply = false;
        if (mCallBack != null) {
            mCallBack.onReplayFinish();
        }
    }

    private void setBackgroundImage(int BookID, int PageID) {
        if (!gbSetNormal) {
            RelativeLayout.LayoutParams para = (RelativeLayout.LayoutParams) ivDraw.getLayoutParams();
            ivDraw.setLayoutParams(para);
            gbSetNormal = true;
        }

        int resId;
        if (BookID == 100) {
            resId = App2.get().getResources().getIdentifier("p" + PageID, "drawable", App2.get().getPackageName());
            if (resId == 0) {
                return;
            }
            ivDraw.setImageResource(resId);
        } else if (BookID == 0) {
            resId = App2.get().getResources().getIdentifier("blank" + PageID, "drawable", App2.get().getPackageName());
            if (resId == 0) {
                return;
            }
            ivDraw.setImageResource(resId);
        } else if (BookID == 1) {
            resId = App2.get().getResources().getIdentifier("zhen" + PageID, "drawable", App2.get().getPackageName());
            if (resId == 0) {
                return;
            }
            ivDraw.setImageResource(resId);
        }
    }

    /**
     * 检查y坐标并重新指定y值
     */
    private void checkOffset(Dot dot) {
        //只有换本或换页时才重新检查y偏移
        if (gCurPageID != dot.PageID || gCurBookID != dot.BookID) {
            if (dot.y > 16) {
                yOffset = dot.y - 16;
            }
        }
        dot.y = dot.y - yOffset;
    }

    /**
     * 绘制笔迹
     */
    public void processDots(Dot dot) {
        int pointZ = dot.force;

        //点压力值<0, 未接触纸面
        if (pointZ < 0) {
            return;
        }

        isTouch = true;
        checkOffset(dot);

        //计算接触点横坐标
        int tmpx = dot.x;
        float pointX = dot.fx;
        pointX /= 100.0;
        pointX += tmpx;

        //计算接触点纵坐标
        int tmpy = dot.y;
        float pointY = dot.fy;
        pointY /= 100.0;
        pointY += tmpy;

        //通过书号判断横纵坐标相对坐标原点的偏移量和实际尺寸
        if (dot.BookID == 100) {//这里用的是book100
            A5_WIDTH = 182.86;
            A5_HEIGHT = 256.0;
            BG_REAL_WIDTH = 1075;
            BG_REAL_HEIGHT = 1512;
        } else if (dot.BookID == 0) {
            A5_WIDTH = 210.29;
            A5_HEIGHT = 297.15;
            BG_REAL_WIDTH = 992;
            BG_REAL_HEIGHT = 1403;
        } else if (dot.BookID == 1) {
            A5_WIDTH = 210.68;
            A5_HEIGHT = 285.67;
            BG_REAL_WIDTH = 2489;
            BG_REAL_HEIGHT = 3375;
        }

        getRedio();
        pointX *= (BG_WIDTH);
        float ax = (float) (A5_WIDTH / XDIST_PERUNIT) * 1.9f;//--
        pointX /= ax;

        pointY *= (BG_HEIGHT);
        float ay = (float) (A5_HEIGHT / YDIST_PERUNIT) * 1.88f;//--
        pointY /= ay;

        pointX += A5_X_OFFSET;//--
        pointY += A5_Y_OFFSET + 50;

        int gWidth = 1;
        if (pointZ > 0) {
            if (dot.type == Dot.DotType.PEN_DOWN) {
                int PageID, BookID;
                PageID = dot.PageID;
                BookID = dot.BookID;
                if (PageID < 0 || BookID < 0 || PageID > 64) {
                    // 谨防笔连接不切页的情况
                    return;
                }

                for (byte aBookID : bookID) {
                    if (BookID == aBookID) {
                        bBookIDIsValide = true;
                    }
                }

                if (!bBookIDIsValide) {
                    return;
                }

                if (PageID != gCurPageID || BookID != gCurBookID) {
                    gbSetNormal = false;
                    bBookIDIsValide = false;
                    setBackgroundImage(dot.BookID, PageID);
                    gCurPageID = PageID;
                    gCurBookID = BookID;
                    drawInit();
                    drawExistingStroke(gCurBookID, gCurPageID);
                }

                drawSubFountainPen2(gWidth, pointX, pointY, 0);

                // 保存屏幕坐标，原始坐标会使比例缩小
                saveData(pointX, pointY, pointZ,0 ,gWidth, dot);
                mov_x = pointX;
                mov_y = pointY;
                return;
            }

            if (dot.type == Dot.DotType.PEN_MOVE) {
                // Pen Move
                mov_x = pointX;
                mov_y = pointY;

                drawSubFountainPen2(gWidth, pointX, pointY, 1);
                // 保存屏幕坐标，原始坐标会使比例缩小
                saveData(pointX, pointY, pointZ, 1, gWidth,dot);
            }
        } else if (dot.type == Dot.DotType.PEN_UP) {
            // Pen Up
            if (dot.x == 0 || dot.y == 0) {
                pointX = mov_x;
                pointY = mov_y;
            }

            drawSubFountainPen2(gWidth, pointX, pointY, 2);

            // 保存屏幕坐标，原始坐标会使比例缩小
            saveData(pointX, pointY, pointZ, 2, gWidth,dot);
        }
    }

    private void saveData(float pointX, float pointY, int pointZ,int type, int gWidth, Dot dot) {
        if (mCallBack != null) {
            String id = mCallBack.getAppId();//任务id
            String num = mCallBack.getAppNum();//题号
            int position = mCallBack.getAppPosition();//填空第几个空
            Dots dots = new Dots(id, num, position, gCurBookID, gCurPageID, pointX, pointY, pointZ, type, gWidth, Color.BLACK, dot.Counter, dot.angle);
            addDots(gCurBookID,dots);
            mCallBack.onSaveData();
        }
    }

    private void drawExistingStroke(int BookID, int PageID) {
        if (BookID == 100) {
            dot_number4 = dot_number;
        } else if (BookID == 0) {
            dot_number4 = dot_number1;
        } else if (BookID == 1) {
            dot_number4 = dot_number2;
        }

        if (dot_number4.isEmpty()) {
            return;
        }

        for (Dots dot : dot_number4) {
            if (dot.getPageID() == PageID) {
                drawSubFountainPen1(dot.penWidth, dot.pointX, dot.pointY, dot.ntype);
            }
        }
    }

    private void drawSubFountainPen1(int penWidth, float x, float y, int ntype) {
        if (ntype == 0) {
            fromX = x;
            fromY = y;
            toX = x;
            toY = y;
        }

        if (ntype == 2) {
            toX = x;
            toY = y;
        } else {
            toX = x;
            toY = y;
        }

        ivDraw.paint.setStrokeWidth(penWidth);
        ivDraw.drawLine(fromX, fromY, toX, toY);
        fromX = toX;
        fromY = toY;
    }

    private void drawSubFountainPen2(int penWidth, float x, float y, int ntype) {
        switch (ntype) {
            case 0:
                fromX = x;
                fromY = y;
                toX = x;
                toY = y;
                break;
            case 1:
                toX = x;
                toY = y;
                break;
            case 2:
                toX = x;
                toY = y;
                break;
        }
        ivDraw.paint.setStrokeWidth(penWidth);
        ivDraw.drawLine(fromX, fromY, toX, toY);
        fromX =toX;
        fromY =toY;
    }

    /**
     * 获取dotlist
     */
    public Collection<Dots> getDotsList() {
        switch (gCurBookID) {
            case 0:
                return dot_number1;
            case 1:
                return dot_number2;
            default://默认book100
                return dot_number;
        }
    }

    /**
     * 添加dot
     */
    private void addDots(int bookID, Dots dot) {
        if (bookID == 100) {
            dot_number.add(dot);
        } else if (bookID == 0) {
            dot_number1.add(dot);
        } else if (bookID == 1) {
            dot_number2.add(dot);
        }
    }

    /**
     * 计算缩放比例
     */
    private void getRedio() {
        float ratio;
        if (mWidth == 1536 && mHeight == 2048) { // 适应小米PAD,4:3
            ratio = (mWidth * 63 / 72) / BG_REAL_WIDTH;
        } else if (mWidth == 1536 && mHeight == 1952) { // 非标准4：3，适用iFive
            ratio = (mWidth * 60 / 72) / BG_REAL_WIDTH;
        } else if (mWidth == 1600 && mHeight == 2452) {
            ratio = (mWidth * 67 / 72) / BG_REAL_WIDTH;
        } else if (mWidth == 1920 && mHeight == 1128) {//联想pad
            ratio = (mWidth * 80 / 72) / BG_REAL_WIDTH;
        } else {
            ratio = (mWidth * 70 / 72) / BG_REAL_WIDTH;
        }

        BG_WIDTH = (int) (BG_REAL_WIDTH * ratio);
        BG_HEIGHT = (int) (BG_REAL_HEIGHT * ratio);

        A5_X_OFFSET = 0;
        A5_Y_OFFSET = -50;
    }

    /**
     * 点击重做
     */
    public void redo() {
        cleanDots();
        if (mCallBack != null) {
            mCallBack.onRedoCallBack();
        }
    }

    /**
     * 清除缓存的点 并初始化界面
     */
    public void cleanDots() {
        gCurPageID = -1;
        gCurBookID = -1;
        drawInit();
        setBackgroundImage(gCurBookID, gCurPageID);
        if (dot_number != null) {
            dot_number.clear();
        }

        if (dot_number1 != null) {
            dot_number1.clear();
        }

        if (dot_number2 != null) {
            dot_number2.clear();
        }

        if (dot_number4 != null) {
            dot_number4.clear();
        }
        yOffset = 0;
        isTouch = false;
    }

    public boolean isTouch() {
        return isTouch;
    }

    public void setTouch(boolean touch) {
        isTouch = touch;
    }

    public interface HandWriteCallBack {
        void onReplayFinish();

        void onSaveData();

        String getAppId();//任务id
        String getAppNum();//题号
        int getAppPosition();//填空第几个空

        void onRedoCallBack();//重做回调
    }
}