package com.sdzn.fzx.student.libbase.msg;


import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.libpublic.utils.ArrayUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SaveLogUtils;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import java.util.ArrayList;
import java.util.List;

/**
 * 管理mqtt的连接,发布,订阅,断开连接, 断开重连等操作
 *
 * @author LichFaker on 16/3/24.
 * @Email lichfaker@gmail.com
 */
public class MqttManager {

    // 单例
    private static MqttManager mInstance = null;
    // 回调
    private MqttCallback mCallback;
    private MqttClient client;
    private MqttConnectOptions conOpt;

    private static String host = Network.BASE_MQTT_URL;
    private static String userName = Network.BASE_MQTT_USENAME;
    private static String password = Network.BASE_MQTT_PASSWORD;

    private static List<String> topics = new ArrayList<>();
    private static List<Integer> qas = new ArrayList<>();


    private MqttManager(MqttCallback callback) {
        mCallback = callback;
    }

    public static MqttManager getInstance(MqttCallback callback) {
        if (null == mInstance) {
            mInstance = new MqttManager(callback);
        }
        return mInstance;
    }

    /**
     * 释放单例, 及其所引用的资源
     */
    public static void release() {
        try {
            if (mInstance != null) {
                mInstance.disConnect();
                mInstance = null;
            }
        } catch (Exception e) {

        }
    }

    /**
     * 创建Mqtt 连接
     *
     * @param brokerUrl Mqtt服务器地址(tcp://xxxx:1863)
     * @return
     */
    public boolean createConnect(String brokerUrl) {

        final String deviceID = AndroidUtil.getDeviceID(App2.get());

        if (client != null && client.isConnected()) {

            return true;
        }
        boolean flag = false;
        String tmpDir = System.getProperty("java.io.tmpdir");
        MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir);

        try {
            // Construct the connection options object that contains connection parameters
            // such as cleanSession and LWT
            conOpt = new MqttConnectOptions();
            conOpt.setCleanSession(true);
            conOpt.setKeepAliveInterval(20);
            if (password != null) {
                conOpt.setPassword(password.toCharArray());
            }
            if (userName != null) {
                conOpt.setUserName(userName);
            }

            client = new MqttClient(brokerUrl, deviceID, dataStore);
            // Set this wrapper as the callback handler
            client.setCallback(mCallback);
            flag = doConnect();
        } catch (MqttException e) {
            Log.e("mq : " + e.getMessage());
        }

        return flag;
    }

    /**
     * 建立连接
     *
     * @return
     */
    private boolean doConnect() {
        boolean flag = false;
        if (client != null) {
            try {
                client.connect(conOpt);
                Log.e("mq  to " + client.getServerURI() + " with client ID " + client.getClientId());
                flag = true;
            } catch (Exception e) {
                Log.e("mq " + e.toString());
                e.printStackTrace();
            }
        }
        return flag;
    }

    /**
     * Publish / send a message to an MQTT server
     *
     * @param topicName the name of the topic to publish to
     * @param qos       the quality of service to delivery the message at (0,1,2)
     * @param payload   the set of bytes to send to the MQTT server
     * @return boolean
     */
    public boolean publish(String topicName, int qos, byte[] payload) {

        boolean flag = false;
        try {
            if (client == null) {
                createConnect(host);
            }

            if (!client.isConnected()) {
                this.reconnect();
                SaveLogUtils.saveInfo("publish:" + "断线重连");
            }

            Log.d("Publishing to topic \"" + topicName + "\" qas " + qos);

            // Create and configure a message
            MqttMessage message = new MqttMessage(payload);
            message.setQos(qos);

            client.publish(topicName, message);
            flag = true;

        } catch (MqttException e) {
            Log.e("mq", e.toString());
        }
        return flag;
    }

    /**
     * Subscribe to a topic on an MQTT server
     * Once subscribed this method waits for the messages to arrive from the server
     * that match the subscription. It continues listening for messages until the enter key is
     * pressed.
     *
     * @param topicName to subscribe to (can be wild carded)
     * @param qos       the maximum quality of service to receive messages at for this subscription
     * @return boolean
     */
    public boolean subscribe(String topicName, int qos) {

        boolean flag = false;
        addTopicItem(topicName, qos);
        if (client != null && client.isConnected()) {
            // Subscribe to the requested topic
            // The QoS specified is the maximum level that messages will be sent to the client at.
            // For instance if QoS 1 is specified, any messages originally published at QoS 2 will
            // be downgraded to 1 when delivering to the client but messages published at 1 and 0
            // will be received at the same level they were published at.
            Log.d("Subscribing to topic \"" + topicName + "\" qas " + qos);
            try {
                client.subscribe(topicName, qos);
                flag = true;
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

        return flag;
    }

    public boolean subscribe(String[] topicName, int qos[]) {

        boolean flag = false;

        addTopicItem(topicName, qos);

        if (client != null && client.isConnected()) {

            try {
                client.subscribe(topicName, qos);
                flag = true;
            } catch (MqttException e) {
                SaveLogUtils.saveInfo("mq subscribe error " + e.toString());
            }
        } else {

            SaveLogUtils.saveInfo("mq subscribe error connect fail ");
        }

        return flag;
    }

    public boolean unSubscribe(String topicName) {

        boolean flag = false;

        removeTopicItem(topicName);

        if (client != null && client.isConnected()) {
            try {
                client.unsubscribe(topicName);
                flag = true;
            } catch (MqttException e) {

            }
        }

        return flag;
    }

    public boolean unSubscribe(String[] topicName) {
        boolean flag = false;
        removeTopicItem(topicName);

        if (client != null && client.isConnected()) {
            try {
                client.unsubscribe(topicName);
                flag = true;
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }


    /**
     * 取消连接
     *
     * @throws MqttException
     */
    public void disConnect() throws MqttException {
        if (client != null && client.isConnected()) {
            try {
                client.disconnect();
            } catch (MqttException e) {
                Log.e("mq " + e.toString());
                e.printStackTrace();
            }
        }
    }

    public void reconnect() {

        if (client != null && !client.isConnected()) {

            SaveLogUtils.saveInfo("mq reconnect");
            try {
                client.setCallback(mCallback);
                client.connect(conOpt);
                client.subscribe(topics.toArray(new String[]{}), ArrayUtils.toIntArray(qas));
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    private void addTopicItem(String[] topicName, int qos[]) {
        for (int i = 0; i < topicName.length; i++) {
            if (!topics.contains(topicName[i])) {
                topics.add(topicName[i]);
                try {
                    qas.add(qos[i]);
                } catch (Exception e) {
                    qas.add(1);
                }
            }
        }
    }

    private void addTopicItem(String topicName, int qos) {
        if (!topics.contains(topicName)) {
            topics.add(topicName);
            qas.add(qos);
        }
    }

    private void removeTopicItem(String topicName) {

        if (topics.contains(topicName)) {
            final int index = topics.indexOf(topicName);
            topics.remove(index);
            try {
                qas.remove(index);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void removeTopicItem(String[] topicName) {
        for (int i = 0; i < topicName.length; i++) {
            if (topics.contains(topicName[i])) {
                final int index = topics.indexOf(topicName[i]);
                topics.remove(index);
                try {
                    qas.remove(index);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
