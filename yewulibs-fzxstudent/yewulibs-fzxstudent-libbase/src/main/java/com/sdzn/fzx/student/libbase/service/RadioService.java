package com.sdzn.fzx.student.libbase.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;

import com.sdzn.fzx.student.libutils.util.Log;

import java.io.IOException;
import java.util.HashSet;


public class RadioService extends Service {
    private HashSet<MediaPlayerCallBack> mCallBackList = new HashSet<>();

    private MediaPlayer mediaPlayer;

    public RadioService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final RadioServiceBinder mBinder = new RadioServiceBinder();

    public class RadioServiceBinder extends Binder {
        public RadioService getService() {
            return RadioService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initMediaPlayer();
        Log.i("mediaService Create");
    }

    @Override
    public void onDestroy() {
        destroyMedia();
        super.onDestroy();
        Log.i("mediaService Destroy");
    }

    public void destroyMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            currentState = END;
            mediaPlayer = null;
        }
    }

    private void initMediaPlayer() {
        synchronized (RadioService.class) {
            if (mediaPlayer == null) {
                synchronized (RadioService.class) {
                    currentState = 0;
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnBufferingUpdateListener(onBufferingUpdateListener);
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            currentState = PREPARED;
                            SystemClock.sleep(500);
                            synchronized (RadioService.class) {
                                for (MediaPlayerCallBack callBack : mCallBackList) {
                                    callBack.onPrepared(RadioService.this);
                                }
                            }
                            mediaPlayer.start();
                        }
                    });
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            currentState = PLAYBACK_COMPLETED;
                            seekTo(0);
                            synchronized (RadioService.class) {
                                for (MediaPlayerCallBack callBack : mCallBackList) {
                                    callBack.onCompletion(RadioService.this);
                                }
                            }
                        }
                    });
                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            currentState = ERROR;
                            synchronized (RadioService.class) {
                                for (MediaPlayerCallBack callBack : mCallBackList) {
                                    callBack.onError(RadioService.this, what, extra);
                                }
                            }
                            if (what == -38 && extra == 0) {
                                try {
                                    Log.e("-38,0  sleep100ms");
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                            return false;
                        }
                    });
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            Log.e("what = " + what + ", extra = " + extra);
                            return false;
                        }
                    });
                }
            }
        }
    }

    MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            synchronized (RadioService.class) {
                for (MediaPlayerCallBack callBack : mCallBackList) {
                    callBack.onBufferingUpdate(RadioService.this, percent);
                }
            }
        }
    };

    private String playPath;
    private String title;

    public String getPlayPath() {
        return playPath;
    }

    public String getTitle() {
        return title;
    }

    @Deprecated
    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public interface MediaPlayerCallBack {
        /**
         * 播放进度变化回调
         */
        void onBufferingUpdate(RadioService service, int percent);

        /**
         * 准备完成回调
         */
        void onPrepared(RadioService service);

        /**
         * 播放完成回调
         */
        void onCompletion(RadioService service);

        /**
         * 播放error回调
         */
        void onError(RadioService service, int what, int extra);
    }

    public void addMediaPlayCallBack(MediaPlayerCallBack callBack) {
        if (callBack == null) {
            return;
        }
        synchronized (RadioService.class) {
            mCallBackList.add(callBack);
        }
    }

    public void removeMediaPalyCallBack(MediaPlayerCallBack callBack) {
        if (callBack == null) {
            return;
        }
        synchronized (RadioService.class) {
            mCallBackList.remove(callBack);
        }
    }

    /*================  ================*/
    public void reset() {
        if (mediaPlayer == null) {
            Log.e("mediaService reInitPlayer");
            initMediaPlayer();
        }
        mediaPlayer.reset();
        currentState = IDLE;
    }

    public void cleanAndReset() {
        playPath = null;
        title = null;
        reset();
    }

    public void setDataSource(String path, String title) throws IOException {
        if (currentState != IDLE) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        if (TextUtils.isEmpty(path)) {
            Log.e("filePath = null");
            throw new IllegalArgumentException("filePath == null");
        }
        playPath = path;
        this.title = title;
        mediaPlayer.setDataSource(path);
        currentState = INITLAIZED;
    }

    public void prepareAsync() {
        if (currentState != INITLAIZED && currentState != STOPED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        mediaPlayer.prepareAsync();
        currentState = PREPARING;
    }

    public void start() {
        if (currentState != PAUSE &&
                currentState != PREPARED &&
                currentState != PLAYBACK_COMPLETED &&
                currentState != STARTED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        mediaPlayer.start();
        currentState = STARTED;
    }

    public void pause() {
        if (currentState != STARTED && currentState != PAUSE) {
            Log.e("currentState = "+ currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        mediaPlayer.pause();
        currentState = PAUSE;
    }

    public void stop() {
        if (currentState != STARTED &&
                currentState != PAUSE &&
                currentState != PLAYBACK_COMPLETED&&
                currentState != STOPED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        mediaPlayer.stop();
        currentState = STOPED;
    }

    public void reInitMediaPlayerAndPlay(String path, String title) {
        initMediaPlayer();
        reset();
        try {
            setDataSource(path,title);
            prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void seekTo(int msec) {
        if (currentState != PREPARED &&
                currentState != STARTED &&
                currentState != PAUSE &&
                currentState != STOPED &&
                currentState != PLAYBACK_COMPLETED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return;
        }
        mediaPlayer.seekTo(msec);
    }

    public int getDuration() {
        if (currentState != PREPARED &&
                currentState != STARTED&&
                currentState != PAUSE&&
                currentState != STOPED&&
                currentState != PLAYBACK_COMPLETED) {
            return 0;
        }
        return mediaPlayer.getDuration();
    }

    public int getCurrentPosition() {
        if (currentState != IDLE &&
                currentState != INITLAIZED &&
                currentState != PREPARED &&
                currentState != STARTED&&
                currentState != PAUSE&&
                currentState != STOPED&&
                currentState != PLAYBACK_COMPLETED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return 0;
        }
        return mediaPlayer.getCurrentPosition();
    }

    public boolean isPlaying() {
        if (currentState != IDLE &&
                currentState != INITLAIZED &&
                currentState != PREPARED &&
                currentState != STARTED&&
                currentState != PAUSE&&
                currentState != STOPED&&
                currentState != PLAYBACK_COMPLETED) {
            Log.e("currentState = " + currentState);
            new RuntimeException().printStackTrace();
            return false;
        }
        if (mediaPlayer.isPlaying()) {
            currentState = STARTED;
        }
        return currentState == STARTED;
    }

    /*================  ================*/

    public int getCurrentState() {
        return currentState;
    }

    private int currentState;

    public static final int IDLE = 1;
    public static final int INITLAIZED = 2;
    public static final int PREPARING = 3;
    public static final int PREPARED = 4;
    public static final int STARTED = 5;
    public static final int PAUSE = 6;
    public static final int STOPED = 7;
    public static final int PLAYBACK_COMPLETED = 8;
    public static final int END = 9;
    public static final int ERROR = -1;
}
