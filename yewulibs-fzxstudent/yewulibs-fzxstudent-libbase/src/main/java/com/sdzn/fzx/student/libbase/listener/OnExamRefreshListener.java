package com.sdzn.fzx.student.libbase.listener;

/**
 * @author Reisen at 2018-11-16
 */
public interface OnExamRefreshListener {
    void requestRefresh();

    void requestRefreshExam(int examType);

    void refreshTop();
    void refreshList();
}
