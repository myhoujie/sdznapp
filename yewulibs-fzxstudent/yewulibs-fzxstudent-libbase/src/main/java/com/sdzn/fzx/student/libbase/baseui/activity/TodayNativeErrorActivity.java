package com.sdzn.fzx.student.libbase.baseui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.baseui.adapter.TodayErrorExamAdapter;
import com.sdzn.fzx.student.libbase.baseui.presenter.TodayNativeErrorPresenter;
import com.sdzn.fzx.student.libbase.baseui.view.TodayNativeErrorView;
import com.sdzn.fzx.student.libpublic.event.DeleteEvent;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.views.exam.ExamListView;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.TodayNativeVo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TodayNativeErrorActivity extends MBaseActivity<TodayNativeErrorPresenter> implements TodayNativeErrorView, View.OnClickListener {
    private String isError;
    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvTitle;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;
    private ExamListView todayListView;

    private TodayErrorExamAdapter todayErrorExamAdapter;
    private List<TodayNativeVo.DataBean> mList = new ArrayList<>();

    @Override
    public void initPresenter() {
        mPresenter = new TodayNativeErrorPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_native_error);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initData();
        initView();
        todayErrorExamAdapter = new TodayErrorExamAdapter(mList, this, this);
        todayListView.setAdapter(todayErrorExamAdapter);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(TodayNativeErrorActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        llTaskEmpty = (LinearLayout) findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) findViewById(R.id.tvTaskEmpty);
        todayListView = (ExamListView) findViewById(R.id.today_listView);
        tvBack.setOnClickListener(this);

        if (TextUtils.equals("1", isError)) {
            tvTitle.setText("今日新增错题");
        } else {
            tvTitle.setText("今日收藏试题");
        }
    }

    @Override
    protected void initData() {
        final Intent intent = getIntent();
        isError = intent.getStringExtra(ErrorNativeActivity.IS_ERROR);
        mPresenter.getToadySubjectList(loadParams());
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(Event event) {
        if (event instanceof DeleteEvent) {
            mPresenter.getToadySubjectList(loadParams());
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private Map<String, String> loadParams() {
        Map<String, String> params = new HashMap<>();
        params.put("userStudentId", UserController.getUserId());
        params.put("model", isError);
        return params;
    }

    @Override
    public void setTodayListSuccess(TodayNativeVo todayNativeVo) {
        if (todayNativeVo.getData().size() == 0) {
            llTaskEmpty.setVisibility(View.VISIBLE);
        } else {
            mList.clear();
            mList.addAll(todayNativeVo.getData());
            refreshAdapter();
        }

    }

    private void refreshAdapter() {
        todayErrorExamAdapter.notifyDataSetChanged();
    }

}
