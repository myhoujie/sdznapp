package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.libbase.base.OnExamClickListener;
import com.sdzn.fzx.student.libbase.listener.BlankTextChangeImp;
import com.sdzn.fzx.student.libbase.listener.OnExamRefreshListener;
import com.sdzn.fzx.student.libpublic.utils.CustomClickListener;
import com.sdzn.fzx.student.libpublic.views.exam.ClozeTestTextView;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankEditText;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.libutils.annotations.InputMethod;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 试题列表adapter
 *
 * @author Reisen at 2018-08-29
 */
public class AnswerListAdapter extends BaseRcvAdapter<AnswerListBean.AnswerDataBean> {
    private OnExamClickListener mListener;
    private OnExamRefreshListener mRefreshListener;
    private int mFillBlankPosition = -1;
    private InputMethodManager mInputMethodManager;

    public AnswerListAdapter(Context context, List<AnswerListBean.AnswerDataBean> mList) {
        super(context, mList);
        mInputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void setListener(OnExamClickListener listener, OnExamRefreshListener refreshListener) {
        mListener = listener;
        mRefreshListener = refreshListener;
    }

    @Override
    public int getItemViewType(int position) {
        AnswerListBean.AnswerDataBean bean = mList.get(position);
        return bean.getType() * 10000 + (bean.getParentId() == 0 ? 0 : 1000) + bean.getExamTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 20000://资源
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_resource);
            case 30000://文本
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_text);
            case 10001://单选
            case 10002://多选
            case 10003://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_select);
            case 10004://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_short_answer);
            case 10006://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_fill_blank);
            case 10014://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_cloze_test);
            case 10016://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_synthesize);
            case 11001://综合-单选
            case 11002://综合-多选
            case 11003://综合-判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_syn_select);
            case 11004://综合-简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_syn_short_answer);
            case 11006://综合-填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_syn_fill_blank);
            case 11014://综合-完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_syn_cloze);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_exam_other_type);
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        holder.getView(R.id.convert_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.closeInput(null);
                }
            }
        });
        if (bean.getType() == 2) {//资源
            bindResource(holder, position, bean);
            return;
        }
        if (bean.getType() == 3) {//文字
            bindText(holder, position, bean);
            return;
        }
        switch (bean.getExamTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindClozeTest(holder, position, bean);//完型填空
                break;
            case 16:
                bindSynthesize(holder, position, bean);//综合
                break;
            default:
                bindOtherType(holder, bean);//其他类型
                break;
        }
    }

    /**
     * 完型填空
     */
    private void bindClozeTest(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        ClozeTestTextView tv = holder.getView(R.id.tv_exam);

        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        SparseArray<String> arr = new SparseArray<>();
        if (list != null) {
            for (AnswerListBean.ExamOptionBean optionBean : list) {
                arr.append(optionBean.getSeq(), optionBean.getMyAnswer());
            }
        }

        if (examTextBean == null) {
            tv.setHtmlBody("", arr);
        } else {
            tv.setHtmlBody(examTextBean.getExamStem(), arr);
        }
        tv.setClozeTestChangeListener(new ClozeTestTextView.ClozeTestChangeListener() {

            @Override
            public void clickCloze(ClozeTestTextView view, SparseArray<ClozeAnswerBean> map,
                                   int position) {
                if (mListener != null) {
                    mListener.showClozeTestTextWindow(view, bean, map, position);
                }
            }
        });
    }

    /**
     * 综合
     */
    private void bindSynthesize(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(examTextBean == null ? "" : examTextBean.getExamStem());
//        ListView lv = holder.getView(R.id.lv);
//        SynthesizeListAdapter adapter = bean.getSynthesizeListAdapter();
//        if (adapter == null) {
//            adapter = new SynthesizeListAdapter(bean, context, mListener, mRefreshListener);
//            bean.setSynthesizeListAdapter(adapter);
//        }
//        lv.setAdapter(adapter);
//        setListHeight(lv);
//        adapter.notifyDataSetChanged();
    }

    //设置listview高度
    private void setListHeight(ListView lv) {
        ListAdapter la = lv.getAdapter();
        if (null == la) {
            return;
        }
        // calculate height of all items.
        int h = 0;
        final int cnt = la.getCount();
        for (int i = 0; i < cnt; i++) {
            View item = la.getView(i, null, lv);
            item.measure(0, 0);
            h += item.getMeasuredHeight();
        }
        // reset ListView height
        ViewGroup.LayoutParams lp = lv.getLayoutParams();
        lp.height = h + (lv.getDividerHeight() * (cnt - 1));
        lv.setLayoutParams(lp);
    }

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);
        AnswerListBean.ExamTextBean examTextBean = bean.getExamBean();
        HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(examTextBean == null ? "" : examTextBean.getExamStem());

        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<AnswerListBean.ExamOptions> options = examTextBean == null ? null : examTextBean.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            final List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextBean.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examList);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (AnswerListBean.ExamOptionBean optionBean : examList) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getMyAnswer())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
            child.setOnClickListener(new SelectorOnClick(bean, examTextBean.getExamTypeId()) {
                @Override
                public void onClick(View view, AnswerListBean.AnswerDataBean bean, int examType) {
                    radioGroupClick(view, bean, examType);
                    if (mRefreshListener != null) {
                        mRefreshListener.requestRefresh();
                    }
                }
            });
        }
    }

    /**
     * 选择选项点击事件处理
     */
    private void radioGroupClick(View view, AnswerListBean.AnswerDataBean bean, int examType) {
        String trim = ((TextView) ((ViewGroup) view).getChildAt(0)).getText().toString().trim();
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null) {
            list = new ArrayList<>();
            bean.setExamOptionList(list);
        }
        if (examType == 1 || examType == 3) {//单选
            if (list.isEmpty()) {
                AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
                optionBean.setMyAnswer(trim);
                optionBean.setSeq(trim.charAt(0) - 64);
                list.add(optionBean);
                bean.setIsAnswer(1);
            } else if (TextUtils.equals(list.get(0).getMyAnswer(), trim)) {
                list.remove(0);
                bean.setIsAnswer(0);
            } else {
                list.get(0).setMyAnswer(trim);
                list.get(0).setSeq(trim.charAt(0) - 64);
                bean.setIsAnswer(1);
            }
            return;
        }
        Iterator<AnswerListBean.ExamOptionBean> iterator = list.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            AnswerListBean.ExamOptionBean next = iterator.next();
            if (TextUtils.equals(trim, next.getMyAnswer())) {
                iterator.remove();
                b = true;
                break;
            }
        }
        if (!b) {
            AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
            optionBean.setMyAnswer(trim);
            optionBean.setSeq(trim.charAt(0) - 64);
            list.add(optionBean);
        }
        bean.setIsAnswer(list.isEmpty() ? 0 : 1);
    }

    @Override
    public void onViewDetachedFromWindow(BaseViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        View view = holder.getView(R.id.tv_exam);
        if (!(view instanceof FillBlankEditText)) {
            return;
        }
        if (mFillBlankPosition == holder.getAdapterPosition()) {
            mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 填空
     */
    @SuppressLint("WrongConstant")
    private void bindFillBlank(BaseViewHolder holder, final int position, AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);

        AnswerListBean.ExamTextBean examBean = bean.getExamBean();

        FillBlankEditText tv = holder.getView(R.id.tv_exam);
        tv.setInputMethod(bean.getStatus() == 0 ? InputMethod.Picture : InputMethod.KeyBoard);
        tv.setAnswerDataBean(bean);
        tv.setBlankTextChangeListener(new BlankTextChangeImp(bean) {
            @Override
            public void onBlankTextChange(SparseArray<ClozeAnswerBean> blankTextList, @Nullable ClozeAnswerBean bean) {
                List<AnswerListBean.ExamOptionBean> list = mAnswerDataBean.getExamOptionList();
                list.clear();
                for (int i = 0; i < blankTextList.size(); i++) {
                    if (TextUtils.isEmpty(blankTextList.valueAt(i).getAnswer())) {
                        continue;
                    }
                    AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
                    optionBean.setSeq(blankTextList.keyAt(i));
                    optionBean.setMyAnswer(blankTextList.valueAt(i).getAnswer() == null ? "" : blankTextList.valueAt(i).getAnswer());
                    optionBean.setAnswerType(blankTextList.valueAt(i).getInputMethod());
                    list.add(optionBean);
                }
                if (list.isEmpty()) {
                    mAnswerDataBean.setIsAnswer(0);
                } else {
                    mAnswerDataBean.setIsAnswer(1);
                }

                if (mRefreshListener != null) {
                    mRefreshListener.refreshTop();
                }

                if (mListener != null) {
                    mListener.onBlankTextChange(bean);
                }
            }

            @Override
            public void closeInput(FillBlankEditText et) {
                if (mListener != null) {
                    mListener.closeInput(et);
                }
            }

            @Override
            public void onBlankClick(FillBlankEditText et, ClozeAnswerBean bean, AnswerListBean.AnswerDataBean answerDataBean) {
                mFillBlankPosition = mList.indexOf(answerDataBean);
                if (mListener != null) {
                    mListener.onBlankClick(et, bean, position);
                }
            }
        });
//        ArrayList<String> list = new ArrayList<>();
//        for (AnswerListBean.ExamOptionBean optionBean : bean.getExamOptionList()) {
//            list.add(optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer());
//        }
        SparseArray<ClozeAnswerBean> arr = new SparseArray<>();
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                AnswerListBean.ExamOptionBean optionBean = list.get(i);
                ClozeAnswerBean clozeAnswerBean;
                switch (optionBean.getAnswerType()) {
                    case 1://键盘作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.KeyBoard);
                        break;
                    case 2://手写笔作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.PenWrite);
                        break;
                    case 3://拍照作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.Picture);
                        break;
                    case 0://未作答
                    default:
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(), false, InputMethod.KeyBoard);
                        break;

                }
                arr.append(optionBean.getSeq(), clozeAnswerBean);
            }
//            for (AnswerListBean.ExamOptionBean optionBean : list) {
//                arr.append(optionBean.getSeq(), new ClozeAnswerBean(optionBean.getSeq(),""));
//            }
        }
        if (examBean == null) {
            tv.setHtmlBody("");
        } else {
            tv.setHtmlBody(examBean.getExamStem(), arr);
        }
    }

    /**
     * 文本
     */
    private void bindText(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean) {
        HtmlTextView tv = holder.getView(R.id.tv);
        tv.setHtmlText(bean.getResourceText());
    }

    /**
     * 资源
     */
    private void bindResource(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
//        ImageView iv_collect = holder.getView(R.id.iv_collect);
//        iv_collect.setVisibility(View.INVISIBLE);
//        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        holder.setText(R.id.tv_name, bean.getResourceName());
        ImageView iv_type = holder.getView(R.id.iv_type);
//        final ResourceVo.ResourceTextVo resourceBean = bean.getResourceVoBean();
        //1文档类2演示稿3视频类4图片类5音频类
        switch (bean.getResourceType()) {
            case 1:
                if (bean.getResourceVoBean() == null) {
                    iv_type.setImageResource(R.drawable.word);
                } else if (".txt".equalsIgnoreCase(bean.getResourceVoBean().getResourceSuffix())) {
                    iv_type.setImageResource(R.drawable.txt);
                } else {
                    iv_type.setImageResource(R.drawable.word);
                }
                break;
            case 2:
                iv_type.setImageResource(R.drawable.ppt);
                break;
            case 3:
                iv_type.setImageResource(R.drawable.video);
                break;
            case 4:
                iv_type.setImageResource(R.drawable.image);
                break;
            case 5:
                iv_type.setImageResource(R.drawable.music);
                break;
            default:
                iv_type.setImageResource(R.drawable.pdf);
                break;
        }
        holder.getView(R.id.ll).setOnClickListener(new CustomClickListener() {
            @Override
            protected void onSingleClick() {
                if (mListener != null) {
                    mListener.showResource(bean);
                }
            }

            @Override
            protected void onFastClick() {
                ToastUtil.showLonglToast("请勿重复点击");
            }
        });
    /*    holder.getView(R.id.ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.showResource(bean);
                }
            }
        });*/
    }

    /**
     * 简答
     */
    private void bindShortAnswer(final BaseViewHolder holder, final int position, final AnswerListBean.AnswerDataBean bean) {
        bindExamTitle(holder, position, bean);

        String str = bean.getExamText();
        if (str == null || str.length() == 0) {
            holder.getView(R.id.tv_exam).setVisibility(View.GONE);
            holder.getView(R.id.ll_un_exam).setVisibility(View.GONE);
            holder.getView(R.id.ll_photo).setVisibility(View.GONE);
        } else if (bean.getIsAnswer() == 0) {
            holder.getView(R.id.tv_exam).setVisibility(View.VISIBLE);
            holder.getView(R.id.ll_un_exam).setVisibility(View.VISIBLE);
            holder.getView(R.id.ll_photo).setVisibility(View.GONE);
            holder.getView(R.id.iv_shouxie).setVisibility(CommonUtils.flag == 0 ? View.GONE : View.VISIBLE);
        } else {
            holder.getView(R.id.tv_exam).setVisibility(View.VISIBLE);
            holder.getView(R.id.ll_un_exam).setVisibility(View.GONE);
            holder.getView(R.id.ll_photo).setVisibility(View.VISIBLE);
        }

        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        holder.getView(R.id.tv_exam).setVisibility(View.VISIBLE);
        HtmlTextView tv = holder.getView(R.id.tv_exam);
        tv.setHtmlText(examBean == null ? "" : examBean.getExamStem());
        RecyclerView rv = holder.getView(R.id.rv_add_pic);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(manager);
//        rv.addItemDecoration(new SpacesItemDecoration(10));
        ShortAnswerAdapter adapter = (ShortAnswerAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerAdapter(context, true);
            rv.setAdapter(adapter);
        }
        adapter.setListener(new ShortAnswerAdapter.OnClickListener() {
            @Override
            public void clickAdd(AnswerListBean.AnswerDataBean dataBean, int picLen) {
                if (mListener != null) {
                    mListener.addPic(dataBean, picLen);
                }
            }

            @Override
            public void clickImage(int index, String url) {
                if (mListener != null) {
                    mListener.openImg(index, url, true, mList.get(position));
                }
            }

            @Override
            public void clickWriteImage(int index) {
                if (mListener != null) {
                    mListener.addWritePic(index, mList.get(position));
                }
            }
        });
        adapter.setDataBean(bean);
        List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
        if (examList != null && !examList.isEmpty()) {
            if (examList.get(0).getSeq() == 1) {
                adapter.setSeq(1);
                holder.setText(R.id.tv_exam_type, "当前为手写笔作答");
            } else {
                adapter.setSeq(2);
                holder.setText(R.id.tv_exam_type, "当前为拍照作答");
            }
            for (AnswerListBean.ExamOptionBean optionBean : examList) {

                String str2 = optionBean.getMyAnswer();
                if (str2 == null) {
                    break;
                }
                String[] arr = str2.split(";");
                if (arr.length == 1) {
                    adapter.setList(arr[0].split(","), null);
                } else {
                    adapter.setList(arr[1].split(","), arr[0].split(","));
                }
            }
        } else {
            adapter.setList(new String[]{}, null);
        }

        holder.getView(R.id.iv_shouxie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.addWritePic(position, bean);
                }
            }
        });
        holder.getView(R.id.iv_xiangji).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.getView(R.id.ll_un_exam).setVisibility(View.GONE);
                holder.getView(R.id.ll_photo).setVisibility(View.VISIBLE);
                if (mListener != null) {
                    mListener.addPic(bean, 0);
                }
            }
        });
        holder.getView(R.id.del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.cleanImg(holder, position, bean);
                }
            }
        });
    }

    /**
     * 其他类型
     */
    private void bindOtherType(BaseViewHolder holder, AnswerListBean.AnswerDataBean bean) {
        String name = bean.getExamTemplateStyleName();
        if (name == null) {
            name = "题型: null";
        } else if (name.length() == 0) {
            name = "题型: 其他题型";
        }
        holder.setText(R.id.tv_type, "题型: " + name);
    }

    /**
     * 试题标题
     */
    private void bindExamTitle(BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
//        ImageView iv_collect = holder.getView(R.id.iv_collect);
//        iv_collect.setVisibility(View.INVISIBLE);//试题作答页面不显示收藏按钮
//        iv_collect.setImageResource(bean.isCollect() ? R.drawable.coll_yes : R.drawable.coll_no);
        if (bean.getParentId() == 0) {
            holder.setText(R.id.tv_count, bean.getExamSeq() + ". ");
        } else {//区分综合题展示效果
            holder.setText(R.id.tv_count, "(" + bean.getExamSeq() + ")");
        }
        int score = (int) bean.getScore();
        if (score <= 0) {
            holder.setVisible(R.id.tv_score, false);
        } else {
            holder.setVisible(R.id.tv_score, true);
            if (bean.getExamTemplateId() == 14 || bean.getExamTemplateId() == 6) {
                score *= bean.getExamBean().getExamOptions().size();
            }
            holder.setText(R.id.tv_score, "(本题" + score + "分)");
        }
        holder.setText(R.id.tv_type, bean.getExamTemplateStyleName());
    }

    private static abstract class SelectorOnClick implements View.OnClickListener {
        private AnswerListBean.AnswerDataBean bean;
        private int examType;

        public SelectorOnClick(AnswerListBean.AnswerDataBean bean, int examType) {
            this.bean = bean;
            this.examType = examType;
        }

        @Override
        public void onClick(View v) {
            onClick(v, bean, examType);
        }

        public abstract void onClick(View view, AnswerListBean.AnswerDataBean bean, int examType);
    }
}
