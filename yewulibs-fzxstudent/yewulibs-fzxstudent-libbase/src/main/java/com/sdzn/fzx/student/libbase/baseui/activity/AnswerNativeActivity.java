package com.sdzn.fzx.student.libbase.baseui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.app.HandWriteManager;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.app.RadioServiceManager;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.base.OnExamClickListener;
import com.sdzn.fzx.student.libbase.baseui.adapter.AnswerListAdapter;
import com.sdzn.fzx.student.libbase.baseui.adapter.AnswerTopBarAdapter;
import com.sdzn.fzx.student.libbase.baseui.adapter.ClozePageNativeAdapter;
import com.sdzn.fzx.student.libbase.baseui.adapter.SynthesizeListAdapter;
import com.sdzn.fzx.student.libbase.baseui.presenter.AnswerNativePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.AnswerNativeView;
import com.sdzn.fzx.student.libbase.event.BLEServiceEvent;
import com.sdzn.fzx.student.libbase.listener.AlbumOrCameraListener;
import com.sdzn.fzx.student.libbase.listener.OnExamRefreshListener;
import com.sdzn.fzx.student.libbase.service.BluetoothLEService;
import com.sdzn.fzx.student.libbase.service.RadioService;
import com.sdzn.fzx.student.libbase.show.WhiteBoardWithMethodActivity;
import com.sdzn.fzx.student.libpublic.event.RefreshTaskEvent;
import com.sdzn.fzx.student.libpublic.utils.GlideImageLoader;
import com.sdzn.fzx.student.libpublic.utils.ImageUtils;
import com.sdzn.fzx.student.libpublic.utils.InputTools;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libpublic.views.DrawImageView;
import com.sdzn.fzx.student.libpublic.views.VDRelativeLayout2;
import com.sdzn.fzx.student.libpublic.views.exam.ClozeTestTextView;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankEditText;
import com.sdzn.fzx.student.libpublic.views.exam.InputRadioGroup;
import com.sdzn.fzx.student.libutils.annotations.InputMethod;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.SoftKeyBoardListener;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;
import com.sdzn.fzx.student.vo.ResourceVo;
import com.sdzn.fzx.student.vo.ServerTimeVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.tencent.bugly.crashreport.CrashReport;
import com.tqltech.tqlpencomm.Dot;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.controller.PhotoPickConfig;

import static com.sdzn.fzx.student.libbase.app.HandWriteManager.mHeight;
import static com.sdzn.fzx.student.libbase.app.HandWriteManager.mWidth;

/**
 * 原生答题界面
 *
 * @author Reisen at 2018-08-27
 */
public class AnswerNativeActivity extends MBaseActivity<AnswerNativePresenter>
        implements AnswerNativeView, OnExamClickListener, OnExamRefreshListener,
        RadioService.MediaPlayerCallBack, SoftKeyBoardListener.OnSoftKeyBoardChangeListener,
        HandWriteManager.HandWriteCallBack, View.OnClickListener {
    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvTitle;
    private TextView tvCommit;
    private TextView tvTime;
    private VDRelativeLayout2 mVDRelativeLayout;
    private RelativeLayout rlTop;
    private ImageView ivLeft;
    private ImageView ivRight;
    private RecyclerView rvAnswers;
    private View line;
    private RecyclerView mRecyclerView;
    private LinearLayout ll_mini;
    private ImageView ivRadio;
    private ImageView ivPlay;
    private ImageView ivClose;
    private LinearLayout emptyLayout;
    private TextView tvUnfEmpty;
    private FrameLayout flInput;
    private InputRadioGroup mRadioGroup;
    private RadioButton rbKeyBoard;
    private RadioButton rbWrite;
    private RadioButton rbPicture;
    private FrameLayout flBottom;
    private FrameLayout rlInputKeyboard;
    private RelativeLayout rlInputWrite;
    private DrawImageView ivDraw;
    private TextView tvRedoWrite;
    private RelativeLayout rlInputCamera;
    private ImageView ivAddPic;
    private ImageView ivPic;
    private TextView tvRedoCamera;

    private static final int REQUECT_CODE_SDCARD = 1000;
    private static final int REQUECT_CODE_CAMERA = 1001;
    public static final int REQ_CODE_ALBUM = 1002;
    public static final int REQ_CODE_CAMERA = 1003;
    public static final int WHITE_BOARD_ID = 1004;
    public static final int REQUEST_RES = 1005;
    public static final int REQUEST_IMAGE = 1006;
    public static final int REQUEST_RADIO = 1007;

    public static final int REQUEST_FILL_BLANK_SDCAED = 1010;
    public static final int REQUEST_FILL_BLANK_CAMERA = 1011;

    private long otherResourceId;
    private long radioResourceId;
    private long radioTime;
    private long radioMiniTime;
    private long radioMiniStartTime;

    private TaskListVo.DataBean taskDataBean;
    private MyCountDownTimer myCountDownTimer;
    private long serverTime;
    private int submitOneMinCount = 0;
    private int submitCount = 0;
    private int picLen;
    //    private int curPosition;
    private AnswerListBean.AnswerDataBean answerDataBean;
    private boolean isExit = false;

    private List<AnswerListBean.AnswerDataBean> mList = new ArrayList<>();
    private AnswerListAdapter mAdapter;
    private AnswerTopBarAdapter mTopAdapter;

    private Handler handler;
    private ProgressDialogManager mDialogManager;

    @Override
    public void initPresenter() {
        mPresenter = new AnswerNativePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_native);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvCommit = (TextView) findViewById(R.id.tvCommit);
        tvTime = (TextView) findViewById(R.id.tvTime);
        mVDRelativeLayout = (VDRelativeLayout2) findViewById(R.id.dragLayout);
        rlTop = (RelativeLayout) findViewById(R.id.rl_top);
        ivLeft = (ImageView) findViewById(R.id.iv_left);
        ivRight = (ImageView) findViewById(R.id.iv_right);
        rvAnswers = (RecyclerView) findViewById(R.id.rv_answers);
        line = (View) findViewById(R.id.line);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ll_mini = (LinearLayout) findViewById(R.id.mini_radio);
        ivRadio = (ImageView) findViewById(R.id.iv_radio);
        ivPlay = (ImageView) findViewById(R.id.iv_play);
        ivClose = (ImageView) findViewById(R.id.iv_close);
        emptyLayout = (LinearLayout) findViewById(R.id.ll_empty);
        tvUnfEmpty = (TextView) findViewById(R.id.tvUnfEmpty);
        flInput = (FrameLayout) findViewById(R.id.fl_input);
        mRadioGroup = (InputRadioGroup) findViewById(R.id.rg);
        rbKeyBoard = (RadioButton) findViewById(R.id.rb_keyboard);
        rbWrite = (RadioButton) findViewById(R.id.rb_write);
        rbPicture = (RadioButton) findViewById(R.id.rb_picture);
        flBottom = (FrameLayout) findViewById(R.id.fl_bottom);
        rlInputKeyboard = (FrameLayout) findViewById(R.id.fl_input_keyboard);
        rlInputWrite = (RelativeLayout) findViewById(R.id.rl_input_write);
        ivDraw = (DrawImageView) findViewById(R.id.iv_draw);
        tvRedoWrite = (TextView) findViewById(R.id.redo_write);
        rlInputCamera = (RelativeLayout) findViewById(R.id.rl_input_camera);
        ivAddPic = (ImageView) findViewById(R.id.iv_add_pic);
        ivPic = (ImageView) findViewById(R.id.iv_pic);
        tvRedoCamera = (TextView) findViewById(R.id.redo_camera);

        tvBack.setOnClickListener(this);
        tvCommit.setOnClickListener(this);
        ivRadio.setOnClickListener(this);
        ivPlay.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        //x.view().inject(this);
        initData();
        initView();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        SoftKeyBoardListener.setListener(this, this);
        int height = StudentSPUtils.getKeyBoardHeight();
        if (height != 0) {
            setLayoutHeight(rlInputKeyboard, height);
            setLayoutHeight(rlInputWrite, height);
            setLayoutHeight(rlInputCamera, height);
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(AnswerNativeActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        if (mDialogManager == null) {
            mDialogManager = new ProgressDialogManager(this);
        }
        ivDraw.setPenIsConnected(false);
        mDialogManager.getProgressDialog().setCancelable(false);
        mDialogManager.getProgressDialog().setCanceledOnTouchOutside(false);
        tvTitle.setText(taskDataBean.getName());
        handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    RadioService service = RadioServiceManager.getInstance().getService();
                    if (service == null) {
                        handler.sendEmptyMessageDelayed(0, 100);
                    } else {
                        service.addMediaPlayCallBack(AnswerNativeActivity.this);
                    }
                }
                super.handleMessage(msg);
            }
        };
        handler.sendEmptyMessageDelayed(0, 100);
        mVDRelativeLayout.addDragView(R.id.mini_radio);
        ll_mini.setVisibility(View.GONE);
        initRadioButton();
        initHandWrite();
    }

    @Override
    protected void initData() {
        taskDataBean = getIntent().getParcelableExtra("taskDataBean");
//        mAdapter = new Y_MultiRecyclerAdapter(appContext, mList);
        mAdapter = new AnswerListAdapter(App2.get(), mList);
        mAdapter.setListener(this, this);

        mTopAdapter = new AnswerTopBarAdapter(App2.get());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        rvAnswers.setLayoutManager(new LinearLayoutManager(App2.get(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(mAdapter);
        rvAnswers.setAdapter(mTopAdapter);
        ivLeft.setVisibility(View.GONE);
        ivRight.setVisibility(View.GONE);
        mTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mRecyclerView.scrollToPosition(position);
//                mRecyclerView.smoothScrollToPosition(position);
            }
        });
        mPresenter.updateStatus(String.valueOf(taskDataBean.getLessonTaskStudentId()));
    }

    @Override
    protected void onDestroy() {
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null) {
            service.destroyMedia();
            service.removeMediaPalyCallBack(AnswerNativeActivity.this);
        }
        RadioServiceManager.getInstance().unbindService(this);
        RadioServiceManager.getInstance().stopService(this);
        if (myCountDownTimer != null) {
            myCountDownTimer.cancel();
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        cleanDotsAndRemoveListener();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive() && getCurrentFocus() != null) {
            if (getCurrentFocus().getWindowToken() != null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
        super.onDestroy();
    }

    private void cleanDotsAndRemoveListener() {
        sManager.cleanDots();
        BLEServiceManger manger = BLEServiceManger.getInstance();
        if (manger.getService() != null) {
            manger.getService().removeOnDataReceiveListener(getDataListener());
        }
        manger.unbindService(activity);
    }

    /*================ AnswerNativeViewImp ================*/

    /**
     * 获取服务器端时间
     */
    @Override
    public void getServerTimeSuccess(ServerTimeVo serverTimeVo) {
        if (serverTimeVo == null) {
            return;
        }
        serverTime = serverTimeVo.getData();
        mPresenter.loadAnswer(taskDataBean.getId(), taskDataBean.getLessonTaskStudentId());
        startTimer(serverTimeVo.getData());
        tvCommit.setVisibility(View.VISIBLE);
    }

    /**
     * 加载试题成功
     */
    @Override
    public void getAnswerListSuccess(List<AnswerListBean.AnswerDataBean> list) {
        mList.clear();
        if (list == null) {
            refreshAdapter();
            return;
        }
        mTopAdapter.clear();
        mTopAdapter.add(list);
        mList.addAll(list);
        emptyLayout.setVisibility(list.isEmpty() ? View.VISIBLE : View.GONE);
        refreshAdapter();
    }

    /**
     * 手写上传成功
     */
    @Override
    public void onWhiteUploadPicSuccess(UploadPicVo uploadVos) {
        onPicSuccess(uploadVos, 1);
    }

    /**
     * 拍照/相册图片上传成功
     */
    @Override
    public void onUploadPicSuccess(UploadPicVo uploadVos) {
        onPicSuccess(uploadVos, 2);
    }

    /**
     * 图片上传回调
     *
     * @param seq 1=手写上传, 2=拍照上传
     */
    private void onPicSuccess(UploadPicVo uploadVos, int seq) {
        if (uploadVos == null) {
            return;
        }
        List<UploadPicVo.DataBean> list = uploadVos.getData();
        if (list == null || list.isEmpty()) {
            return;
        }
        if (mTempAnswerBean != null) {
            mTempAnswerBean.setAnswer(uploadVos.getData().get(0).getOriginalPath());
            onFillBlankPicSuccess(mTempAnswerBean, seq, uploadVos.getData().get(0));
            mTempEditText.rebuildText();
            mTempEditText.blankTextChange();
            mTempEditText = null;
            mTempAnswerBean = null;
            return;
        }
        if (answerDataBean == null) {
            return;
        }
        AnswerListBean.AnswerDataBean dataBean = answerDataBean;
        List<AnswerListBean.ExamOptionBean> optionList = dataBean.getExamOptionList();
        if (optionList == null) {
            optionList = new ArrayList<>();
            dataBean.setExamOptionList(optionList);
        }
        if (optionList.isEmpty()) {
            optionList.add(new AnswerListBean.ExamOptionBean());
        }
        StringBuilder strImg = new StringBuilder();
        for (UploadPicVo.DataBean bean : list) {
            if (bean != null) {
                strImg.append(bean.getOriginalPath()).append(",");
            }
        }
        if (strImg.length() != 0) {
            strImg.deleteCharAt(strImg.length() - 1);
        }
        AnswerListBean.ExamOptionBean optionBean = optionList.get(0);
        String myAnswer = optionBean.getMyAnswer();
        if (myAnswer != null) {
            optionBean.setMyAnswer(myAnswer + "," + strImg);
        } else {
            optionBean.setMyAnswer(strImg.toString());
        }
        optionBean.setSeq(seq);
        dataBean.setIsAnswer(1);
        requestRefresh();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }
    /*================ AnswerNativeViewImp ================*/

    private void refreshAdapter() {
        mAdapter.notifyDataSetChanged();
        mTopAdapter.notifyDataSetChanged();
    }

    /**
     * 开始计时
     */
    private void startTimer(long time) {
        myCountDownTimer = new MyCountDownTimer(tvTime, taskDataBean.getEndTime() - time, 1000);
        myCountDownTimer.start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }
    //111111
    /*================ 列表回调接口 ================*/
    @Override
    public void addPic(AnswerListBean.AnswerDataBean bean, int picLen) {
        this.picLen = picLen;
//        curPosition = position;
        answerDataBean = bean;
        mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
            @Override
            public void selectAlbum() {
//                MPermissions.requestPermissions(activity, REQUECT_CODE_SDCARD, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
//                String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
//                EasyPermissions.requestPermissions(activity, "请设置权限",
//                        REQUECT_CODE_SDCARD, perms);
                methodRequiresTwoPermission();
            }

            @Override
            public void selectCamera() {
//                MPermissions.requestPermissions(activity, REQUECT_CODE_CAMERA, Manifest.permission.CAMERA);
//                MPermissions.requestPermissions(activity, REQUECT_CODE_CAMERA, Manifest.permission.CAMERA);
//                String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
//                EasyPermissions.requestPermissions(activity, "请设置权限",
//                        REQUECT_CODE_CAMERA, perms);
                methodRequiresTwoPermission1();
            }
        });
    }

    @Override
    public void addWritePic(int position, AnswerListBean.AnswerDataBean bean) {
//        curPosition = position;
        answerDataBean = bean;
        Intent intentWritePic = new Intent(activity, WhiteBoardWithMethodActivity.class);
        intentWritePic.putExtra("appId", "" + taskDataBean.getId() + bean.getId());//查询索引
        intentWritePic.putExtra("appNum", position);//显示题号, 查询索引
        intentWritePic.putExtra("examStem", bean.getExamBean().getExamStem());//必传, 用来显示题干
        activity.startActivityForResult(intentWritePic, WHITE_BOARD_ID);
    }


    /**
     * 简答题清除图片
     */
    @Override
    public void cleanImg(final BaseViewHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        if (bean.getIsAnswer() == 0 && (bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty())) {
            holder.setVisible(R.id.ll_photo, false);
            holder.setVisible(R.id.ll_un_exam, true);
            return;
        }
        mPresenter.showDialog("删除后需要重新作答,确定删除吗？", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                holder.setVisible(R.id.ll_photo, false);
                holder.setVisible(R.id.ll_un_exam, true);
                bean.setIsAnswer(0);
                bean.setExamOptionList(null);
                refreshAdapter();
            }
        });
    }

    @Override
    public void cleanImg(final SynthesizeListAdapter.SynShortAnswerHolder holder, int position, final AnswerListBean.AnswerDataBean bean) {
        mPresenter.showDialog("删除后需要重新作答,确定删除吗？", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                holder.llPhoto.setVisibility(View.GONE);
                holder.llUnExam.setVisibility(View.VISIBLE);
                bean.setIsAnswer(0);
                bean.setExamOptionList(null);
                refreshAdapter();
            }
        });
    }

    @Override
    public void openImg(int index, String url, boolean showDel, AnswerListBean.AnswerDataBean bean) {
        Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
        intentImage.putExtra("photoUrl", url);
        intentImage.putExtra("showDel", showDel);
        intentImage.putExtra("examId", "" + mList.indexOf(bean));
        intentImage.putExtra("index", index);
        activity.startActivityForResult(intentImage, REQUEST_IMAGE);
    }

    @Override
    public void showResource(final AnswerListBean.AnswerDataBean resourceVo) {
        if (resourceVo == null) {
            ToastUtil.showShortlToast("找不到资源");
            return;
        }
        final ResourceVo.ResourceTextVo resourceTextVo = resourceVo.getResourceVoBean();
        if (resourceTextVo == null) {
            ToastUtil.showShortlToast("找不到资源");
            return;
        }
        // 1文档类2演示稿3视频类4图片类5音频类*/
        switch (resourceTextVo.getResourceType()) {
            case 1:
            case 2:
                otherResourceId = resourceVo.getResourceId();
                showSelectOpenDialog(new OpenListener() {
                    @Override
                    public void openByApp() {
                        Intent intentDoc = new Intent(activity, PDFActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                        intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        activity.startActivityForResult(intentDoc, REQUEST_RES);
                    }

                    @Override
                    public void openByOther() {
                        Intent intentDoc = new Intent(activity, DocViewActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        switch (resourceTextVo.getResourceType()) {
                            case 1:
                                if (".txt".equalsIgnoreCase(resourceTextVo.getResourceSuffix())) {
                                    intentDoc.putExtra("type", "txt");
                                } else {
                                    intentDoc.putExtra("type", "doc");
                                }
                                break;
                            case 2:
                                intentDoc.putExtra("type", "ppt");
                                break;
                        }
                        activity.startActivity(intentDoc);
                    }
                });
                break;
            case 3:
                if (radioResourceId != 0) {//音视频冲突, 这里保存时间
                    radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                    saveAndResetRadioTime();
                    ivPlay.setImageResource(R.mipmap.mini_play);
                    ll_mini.setVisibility(View.GONE);
                }
                otherResourceId = resourceVo.getResourceId();
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentVideo = new Intent(activity, PlayerActivity.class);
                        intentVideo.putExtra("videoUrl", path);
                        intentVideo.putExtra("title", resourceTextVo.getResourceName());
                        activity.startActivityForResult(intentVideo, REQUEST_RES);
                    }
                });
//                Intent intentVideo = new Intent(activity, PlayerActivity.class);
//                intentVideo.putExtra("videoUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                intentVideo.putExtra("title", resourceTextVo.getResourceName());
//                activity.startActivityForResult(intentVideo, REQUEST_RES);
                break;
            case 4:
                otherResourceId = resourceVo.getResourceId();
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
                        intentImage.putExtra("photoUrl", path);
                        activity.startActivityForResult(intentImage, REQUEST_RES);
                    }
                });
//                Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
//                intentImage.putExtra("photoUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                activity.startActivityForResult(intentImage, REQUEST_RES);
                break;
            case 5:
                long id = resourceVo.getResourceId();
                if (radioMiniStartTime != 0) {
                    radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                }
                if (radioResourceId != 0 && id != radioResourceId) {//点击新音频, 统计时间回传h5
                    saveAndResetRadioTime();
                }
                if (radioMiniStartTime == 0) {
                    radioMiniStartTime = System.currentTimeMillis();
                }
                radioResourceId = id;
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentRadio = new Intent(activity, PlayerActivity.class);
                        intentRadio.putExtra("radioUrl", path);
                        intentRadio.putExtra("title", resourceTextVo.getResourceName());
                        intentRadio.putExtra(PlayerActivity.SHOW_MINI_BUTTON, true);
                        activity.startActivityForResult(intentRadio, REQUEST_RADIO);
                    }
                });
//                Intent intentRadio = new Intent(activity, PlayerActivity.class);
//                intentRadio.putExtra("radioUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                intentRadio.putExtra("title", resourceTextVo.getResourceName());
//                intentRadio.putExtra(PlayerActivity.SHOW_MINI_BUTTON, true);
//                activity.startActivityForResult(intentRadio, REQUEST_RADIO);
                break;
            case 6:
                otherResourceId = resourceVo.getResourceId();
                Intent intentDoc = new Intent(activity, PDFActivity.class);
                intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                activity.startActivityForResult(intentDoc, REQUEST_RES);
                break;
        }
    }

    //===================== ↓↓↓填空输入部分↓↓↓ =====================

    @Override
    public void onKeyBoardShow(int height) {
        int h = StudentSPUtils.getKeyBoardHeight();
        if (h != 0 && h == height) {
            return;
        }
        StudentSPUtils.saveKeyBoardHeight(height);
        mRadioGroup.setVisibility(View.VISIBLE);
        setLayoutHeight(rlInputKeyboard, height);
        setLayoutHeight(rlInputWrite, height);
        setLayoutHeight(rlInputCamera, height);
        rlInputKeyboard.setVisibility(View.VISIBLE);
        isShowHandWrite = false;
    }

    @Override
    public void onKeyBoardHide(int height) {
        rlInputKeyboard.setVisibility(View.GONE);
        ClozeAnswerBean bean = mRadioGroup.getBean();
        if (bean == null || InputTools.isKeyboard(bean.getInputMethod())) {
            mRadioGroup.setVisibility(View.GONE);
            isShowHandWrite = false;
        } else {
            mRadioGroup.setVisibility(View.VISIBLE);
            isShowHandWrite = mRadioGroup.getCheckedRadioButtonId() == R.id.rb_write;
        }
    }

    /**
     * 设置下方输入框高度
     */
    private void setLayoutHeight(View view, int height) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (params.height == height) {
            return;
        }
        params.height = height;
        view.setLayoutParams(params);
    }


    /**
     * 填空题图片答题回调
     *
     * @param inputMethod 1=手写,2=拍照
     */
    @SuppressLint("WrongConstant")
    private void onFillBlankPicSuccess(ClozeAnswerBean bean, int inputMethod, UploadPicVo.DataBean dataBean) {
        //这段是回调的处理内容
        bean.setAnswer(dataBean.getOriginalPath());
        if (inputMethod == 1) {
            bean.setInputMethod(InputMethod.PenWrite);
            tvRedoWrite.setEnabled(true);
        } else {
            bean.setInputMethod(InputMethod.Picture);
            ivPic.setVisibility(View.VISIBLE);
            ivAddPic.setVisibility(View.GONE);
            tvRedoCamera.setEnabled(true);
            loadImageIntoPicView(bean.getAnswer());
        }
//        FillBlankEditText editText = mRadioGroup.getEditText();
//        editText.rebuildText();
//        editText.blankTextChange();
    }

    /**
     * 点击填空题空位, 处理调起键盘等逻辑
     */
    @SuppressLint("WrongConstant")
    @Override
    public void onBlankClick(FillBlankEditText et, ClozeAnswerBean bean, int position) {
        ClozeAnswerBean answerBean = mRadioGroup.getBean();
        if (answerBean != null && InputTools.isPenWrite(answerBean.getInputMethod()) &&
                sManager != null && sManager.isTouch()) {
            savePhoto();
        }

        @InputMethod int inputMethod = bean.getInputMethod();
        mRadioGroup.setVisibility(View.VISIBLE);
        isShowHandWrite = mRadioGroup.getCheckedRadioButtonId() == R.id.rb_write;
        mRadioGroup.setEditText(et);
        //区分是否可键盘输入
        if (InputTools.isKeyboard(et.getInputMethod())) {
            rbKeyBoard.setVisibility(View.VISIBLE);
            rbWrite.setVisibility(View.VISIBLE);
            rbPicture.setVisibility(View.VISIBLE);
            rbWrite.setBackgroundResource(R.drawable.fg_input_middle_selector);
        } else {
            rbKeyBoard.setVisibility(View.GONE);
            rbWrite.setVisibility(View.VISIBLE);
            rbPicture.setVisibility(View.VISIBLE);
            rbWrite.setBackgroundResource(R.drawable.fg_input_middle_left_selector);
            if (InputTools.isKeyboard(bean.getInputMethod())) {
                bean.setInputMethod(InputMethod.PenWrite);
                inputMethod = InputMethod.PenWrite;
            }
        }
        mRadioGroup.clearCheck();
        mRadioGroup.setBean(bean);
        mRadioGroup.setExamPosition(position);
        switch (inputMethod) {
            case InputMethod.KeyBoard:
            case InputMethod.NONE:
                changeButtonState(rbKeyBoard, TextUtils.isEmpty(bean.getAnswer()), rbWrite, rbPicture);
                mRadioGroup.check(R.id.rb_keyboard);
                break;
            case InputMethod.PenWrite:
                boolean penPicIsEmpty = TextUtils.isEmpty(bean.getAnswer());
                changeButtonState(rbWrite, penPicIsEmpty, rbKeyBoard, rbPicture);
                mRadioGroup.check(R.id.rb_write);
                ivDraw.initDraw(ivDraw.getContext());
                if (!penPicIsEmpty) {
                    mPresenter.loadLocalData(getAppId(), getAppNum(), getAppPosition());
                }
                tvRedoWrite.setEnabled(!penPicIsEmpty);
                break;
            case InputMethod.Picture:
                boolean picIsEmpty = TextUtils.isEmpty(bean.getAnswer());
                changeButtonState(rbPicture, picIsEmpty, rbKeyBoard, rbWrite);
                mRadioGroup.check(R.id.rb_picture);
                if (picIsEmpty) {
                    ivAddPic.setVisibility(View.VISIBLE);
                    ivPic.setImageDrawable(null);
                    ivPic.setVisibility(View.GONE);
                } else {
                    ivAddPic.setVisibility(View.GONE);
                    loadImageIntoPicView(bean.getAnswer());
                    ivPic.setVisibility(View.VISIBLE);
                }
                tvRedoCamera.setEnabled(!picIsEmpty);
                break;
            case InputMethod.HandWrite:
            default:
                break;
        }
    }

    private void loadImageIntoPicView(String url) {
        Glide.with(AnswerNativeActivity.this).load(url).asBitmap().into(ivPic);
    }

    private void changeButtonState(RadioButton showButton, boolean showOtherButton,
                                   RadioButton rb2, RadioButton rb3) {
        showButton.setEnabled(true);
        if (showOtherButton) {
            rb2.setEnabled(true);
            rb3.setEnabled(true);
        } else {
            rb2.setEnabled(false);
            rb3.setEnabled(false);
        }
    }

    /**
     * 填空内容改变监听, 处理文本修改时逻辑
     */
    @Override
    public void onBlankTextChange(ClozeAnswerBean bean) {
        if (bean == null) {
            return;
        }
        switch (bean.getInputMethod()) {
            case InputMethod.KeyBoard:
            case InputMethod.NONE:
                changeButtonState(rbKeyBoard, TextUtils.isEmpty(bean.getAnswer()), rbWrite, rbPicture);
                mRadioGroup.check(R.id.rb_keyboard);
                break;
            case InputMethod.PenWrite:
                changeButtonState(rbWrite, TextUtils.isEmpty(bean.getAnswer()), rbKeyBoard, rbPicture);
                mRadioGroup.check(R.id.rb_write);
                break;
            case InputMethod.Picture:
                changeButtonState(rbPicture, TextUtils.isEmpty(bean.getAnswer()), rbKeyBoard, rbWrite);
                mRadioGroup.check(R.id.rb_picture);
                break;
            case InputMethod.HandWrite:
            default:
                break;
        }
    }

    /**
     * 关闭输入窗
     */
    @Override
    public void closeInput(FillBlankEditText et) {
        closeInput(et, true);
    }

    private void closeInput(FillBlankEditText et, boolean save) {
        if (save && sManager != null && sManager.isTouch()) {
            savePhoto();
        }
        mRadioGroup.setVisibility(View.GONE);
        if (et == null) {
            et = mRadioGroup.getEditText();
        }
        if (et != null) {
            hideKeyBoard(et);
        }
        mRadioGroup.setBean(null);
        mRadioGroup.setEditText(null);
        mRadioGroup.setExamPosition(-1);
        rbKeyBoard.setVisibility(View.GONE);
        rbWrite.setVisibility(View.GONE);
        rbPicture.setVisibility(View.GONE);
        rlInputKeyboard.setVisibility(View.GONE);
        rlInputCamera.setVisibility(View.GONE);
        rlInputWrite.setVisibility(View.GONE);
    }

    private void showKeyBoard(TextView text) {
        if (text == null) {
            CrashReport.postCatchedException(new NullPointerException("view == null"));
            return;
        }
        if (isSoftShowing()) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(text, InputMethodManager.SHOW_FORCED);
        }
    }

    private void hideKeyBoard(View view) {
        if (view == null) {
            CrashReport.postCatchedException(new NullPointerException("view == null"));
            return;
        }
        if (!isSoftShowing()) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            IBinder token = view.getWindowToken();
            imm.hideSoftInputFromWindow(token, 0);
            if (imm.isActive()) {
                View currentFocus = getWindow().getDecorView();
                if (currentFocus != null) {
                    imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), InputMethodManager
                            .HIDE_NOT_ALWAYS);
                }
            }
        }
    }

    /**
     * 通过屏幕高度判断键盘是否弹起
     */
    private boolean isSoftShowing() {
        int height = getWindow().getDecorView().getHeight();
        Rect rect = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return height - rect.bottom - getSoftButtonsBarHeight() != 0;
    }

    private int getSoftButtonsBarHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        //这个方法获取可能不是真实屏幕的高度
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int usableHeight = metrics.heightPixels;
        //获取当前屏幕的真实高度
        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int realHeight = metrics.heightPixels;
        if (realHeight > usableHeight) {
            return realHeight - usableHeight;
        } else {
            return 0;
        }
    }
    //===================== ↑↑↑填空输入部分↑↑↑ =====================

    /**
     * 完形填空弹起试题作答弹窗
     */
    @Override
    public void showClozeTestTextWindow(final ClozeTestTextView view,
                                        final AnswerListBean.AnswerDataBean bean,
                                        final SparseArray<ClozeAnswerBean> map,
                                        int position) {
        final Dialog dialog = new Dialog(AnswerNativeActivity.this, R.style.cloze_bottom_dialog);
        @SuppressWarnings("inflateParams")
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(AnswerNativeActivity.this)
                .inflate(R.layout.alert_cloze, null);
        TextView tvTotal = layout.findViewById(R.id.tv_total_count);
        final TextView tvCurrent = layout.findViewById(R.id.tv_current_count);
        final ViewPager vp = layout.findViewById(R.id.vp);
        if (map == null || map.size() == 0) {
            tvTotal.setText("/0");
            tvCurrent.setText("0");
            return;
        }
        tvTotal.setText("/" + map.size());
        tvCurrent.setText("" + position);
        final ClozePageNativeAdapter adapter = new ClozePageNativeAdapter(
                AnswerNativeActivity.this, bean, map);
        adapter.setClickCallBack(new ClozePageNativeAdapter.ClozeClickCallBack() {
            @Override
            public void click(SparseArray<ClozeAnswerBean> map, final int position) {
                view.refresh();
                List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
                boolean b = true;
                for (AnswerListBean.ExamOptionBean optionBean : list) {
                    if (optionBean.getSeq() == position + 1) {
                        optionBean.setMyAnswer(map.valueAt(position).getAnswer());
                        b = false;
                        break;
                    }
                }
                if (b) {
                    AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
                    optionBean.setMyAnswer(map.valueAt(position).getAnswer());
                    optionBean.setSeq(position + 1);
                    list.add(optionBean);
                }
                adapter.notifyDataSetChanged();
                mTopAdapter.notifyDataSetChanged();
                if (position < map.size() - 1) {
                    if (TextUtils.isEmpty(map.valueAt(position + 1).getAnswer())) {//下一题未作答才跳到下一题
                        view.select(position + 1);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                vp.setCurrentItem(position + 1);
                            }
                        }, 1000);
                    } else {
                        view.select(-1);
                        dialog.dismiss();
                    }
                } else {
                    view.select(-1);
                    dialog.dismiss();
                }
            }
        });
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                tvCurrent.setText(1 + position + "");
                view.select(position + 1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        vp.setAdapter(adapter);
        //默认指定页
        vp.setCurrentItem(position, false);
        dialog.setContentView(layout);
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels;
        if (dialog.getWindow() != null) {
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                view.select(-1);
            }
        });
        dialog.show();
    }
    /*================ 列表回调接口 ================*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    /*================ 申请权限回调 ================*/
    @AfterPermissionGranted(REQUECT_CODE_SDCARD)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            new PhotoPickConfig.Builder(this)
                    .imageLoader(new GlideImageLoader())
                    .showCamera(false)
                    .maxPickSize(4 - picLen)
                    .spanCount(8)
                    .clipPhoto(false)
                    .mustClip(false)
                    .build();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUECT_CODE_SDCARD, perms);
        }
    }

    /**
     * 访问相机权限被拒绝*/
    @AfterPermissionGranted(REQUECT_CODE_CAMERA)
    private void methodRequiresTwoPermission1() {
        String[] perms = {Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            mPresenter.toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUECT_CODE_CAMERA, perms);
        }
    }

    /**
     * 访问相机权限被拒绝*/
    @AfterPermissionGranted(REQUEST_FILL_BLANK_CAMERA)
    private void methodRequiresTwoPermission4() {
        String[] perms = {Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            mPresenter.toSystemCamera();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUEST_FILL_BLANK_CAMERA, perms);
        }
    }
    /**
     * 简单题图片
     * */
    @AfterPermissionGranted(REQUEST_FILL_BLANK_SDCAED)
    private void methodRequiresTwoPermission2() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            try {
                new PhotoPickConfig.Builder(this)
                        .imageLoader(new GlideImageLoader())
                        .showCamera(false)
                        .maxPickSize(1)
                        .spanCount(8)
                        .clipPhoto(true)
                        .mustClip(true)
                        .build();
            } catch (Exception e) {
                Log.d("111111", "methodRequiresTwoPermission2: " + e.toString());
            }
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUEST_FILL_BLANK_SDCAED, perms);
        }
    }

    @AfterPermissionGranted(REQUEST_FILL_BLANK_SDCAED)
    private void methodRequiresTwoPermission3() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
            new PhotoPickConfig.Builder(this)
                    .imageLoader(new GlideImageLoader())
                    .showCamera(false)
                    .maxPickSize(1)
                    .spanCount(8)
                    .clipPhoto(true)
                    .mustClip(true)
                    .build();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "请设置权限",
                    REQUEST_FILL_BLANK_SDCAED, perms);
        }
    }

//    @PermissionGrant(REQUECT_CODE_SDCARD)
//    public void requestSdcardSuccess() {
//        new PhotoPickConfig.Builder(this)
//                .imageLoader(new GlideImageLoader())
//                .showCamera(false)
//                .maxPickSize(4 - picLen)
//                .spanCount(8)
//                .clipPhoto(false)
//                .mustClip(false)
//                .build();
//    }
//
//    @PermissionDenied(REQUECT_CODE_SDCARD)
//    public void requestSdcardFailed() {
//        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
//    }
//
//    @PermissionGrant(REQUECT_CODE_CAMERA)
//    public void requestCameraSuccess() {
//        mPresenter.toSystemCamera();
//    }
//
//    @PermissionDenied(REQUECT_CODE_CAMERA)
//    public void requestCamerAFailed() {
//        ToastUtil.showShortlToast("访问相机权限被拒绝");
//    }

    //填空题图片
//    @PermissionGrant(REQUEST_FILL_BLANK_SDCAED)
//    public void requestFillSdcardSuccess() {
//        new PhotoPickConfig.Builder(this)
//                .imageLoader(new GlideImageLoader())
//                .showCamera(false)
//                .maxPickSize(1)
//                .spanCount(8)
//                .clipPhoto(true)
//                .mustClip(true)
//                .build();
//    }

//    @PermissionDenied(REQUEST_FILL_BLANK_SDCAED)
//    public void requestFillSdcardFailed() {
//        ToastUtil.showShortlToast("访问SD卡权限被拒绝");
//    }
//
//    @PermissionGrant(REQUEST_FILL_BLANK_CAMERA)
//    public void requestFillCameraSuccess() {
//        mPresenter.toSystemCamera();
//    }
//
//    @PermissionDenied(REQUEST_FILL_BLANK_CAMERA)
//    public void requestFillCamerAFailed() {
//        ToastUtil.showShortlToast("访问相机权限被拒绝");
//    }

    private void saveFillBlankViewAndBean() {
        if (mRadioGroup.getBean() != null) {
            mTempAnswerBean = mRadioGroup.getBean();
        }
        if (mRadioGroup.getEditText() != null) {
            mTempEditText = mRadioGroup.getEditText();
        }
    }

    private void cleanFillBlankViewAndBean() {
        mTempAnswerBean = null;
        mTempEditText = null;
    }

    @Override
    public void clipSuccess(String photoPath) {
        List<Photo> paths = new ArrayList<>();
        Photo photo = new Photo();
        photo.setPath(photoPath);
        paths.add(photo);
        saveFillBlankViewAndBean();
        mPresenter.uploadSubjectivePhoto(paths);
    }

    @Override
    public void clipFailed(String errorMsg) {
        ToastUtil.showShortlToast(errorMsg);
        cleanFillBlankViewAndBean();
    }

    @Override
    protected void onActResult(int requestCode, int resultCode, Intent data) {
        super.onActResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_ALBUM && null != data) {//相册返回图片
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                String path = FileUtil.getUriPath(uri, activity);
                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(path);
                paths.add(photo);
                saveFillBlankViewAndBean();
                mPresenter.uploadSubjectivePhoto(paths);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
            if (resultCode == Activity.RESULT_OK) {
                saveFillBlankViewAndBean();
                mPresenter.startClipPic(mTempAnswerBean != null && mTempEditText != null);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {//裁剪返回
            if (resultCode == Activity.RESULT_OK) {
                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(UCrop.getOutput(data));
                paths.add(photo);
                saveFillBlankViewAndBean();
                mPresenter.uploadSubjectivePhoto(paths);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE) {//图片选择器返回
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    // 裁切的路径
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);
                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(path);
                    paths.add(photo);
                    saveFillBlankViewAndBean();
                    mPresenter.uploadSubjectivePhoto(paths);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        saveFillBlankViewAndBean();
                        // 路径列表
                        mPresenter.uploadSubjectivePhoto(photoLists);
                    }
                }
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == WHITE_BOARD_ID && resultCode == Activity.RESULT_OK) {//简答手写返回
            String webUrl = data.getStringExtra("webUrl");
            if (answerDataBean == null) {
                return;
            }
            answerDataBean.setIsAnswer(0);
            List<AnswerListBean.ExamOptionBean> list = answerDataBean.getExamOptionList();
            if (list == null) {
                list = new ArrayList<>();
                answerDataBean.setExamOptionList(list);
            } else {
                list.clear();
            }
            int flag = data.getIntExtra("flag", 2);
            if (flag == 1) {//直接上传
                mPresenter.WhiteUploadSubjectivePhoto(webUrl);
            }
        } else if (requestCode == REQUEST_RADIO && resultCode == Activity.RESULT_OK) {//播放媒体返回
            long time = data.getLongExtra("time", 0);
            if (radioTime == 0) {
                radioTime = time;
            } else {
                radioTime += time;
            }
            if (data.getBooleanExtra("isMini", false)) {
                radioMiniStartTime = System.currentTimeMillis();
                ll_mini.setVisibility(View.VISIBLE);
                RadioService service = RadioServiceManager.getInstance().getService();
                if (service != null) {
                    ivPlay.setImageResource(service.getCurrentState() == RadioService.STARTED ?
                            R.mipmap.mini_pause : R.mipmap.mini_play);
                }
            } else {
                ll_mini.setVisibility(View.GONE);
                saveAndResetRadioTime();
            }
        } else if (requestCode == REQUEST_RES && resultCode == Activity.RESULT_OK) {//查看其他资源返回
            final long time = data.getLongExtra("time", 0);
            final long id = otherResourceId;
            saveResTime(id, time);
            otherResourceId = 0;
        } else if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {//预览图片返回
            boolean isDel = data.getBooleanExtra("isDel", false);
            int examId = Integer.valueOf(data.getStringExtra("examId"));
            int index = data.getIntExtra("index", 0);
            if (isDel) {
                // 删除图片
                mPresenter.removeImage(mList.get(examId), index);
                refreshAdapter();
            }
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_ALBUM && null != data) {//相册返回图片
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                String path = FileUtil.getUriPath(uri, activity);
                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(path);
                paths.add(photo);
                saveFillBlankViewAndBean();
                mPresenter.uploadSubjectivePhoto(paths);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == REQ_CODE_CAMERA) {//拍照返回
            if (resultCode == Activity.RESULT_OK) {
                saveFillBlankViewAndBean();
                mPresenter.startClipPic(mTempAnswerBean != null && mTempEditText != null);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {//裁剪返回
            if (resultCode == Activity.RESULT_OK) {
                List<Photo> paths = new ArrayList<>();
                Photo photo = new Photo();
                photo.setPath(UCrop.getOutput(data));
                paths.add(photo);
                saveFillBlankViewAndBean();
                mPresenter.uploadSubjectivePhoto(paths);
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == PhotoPickConfig.PICK_REQUEST_CODE) {//图片选择器返回
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("isClip", false)) {
                    // 裁切的路径
                    String path = data.getStringExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO);
                    List<Photo> paths = new ArrayList<>();
                    Photo photo = new Photo();
                    photo.setPath(path);
                    paths.add(photo);
                    saveFillBlankViewAndBean();
                    mPresenter.uploadSubjectivePhoto(paths);
                } else {
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists != null && !photoLists.isEmpty()) {
                        saveFillBlankViewAndBean();
                        // 路径列表
                        mPresenter.uploadSubjectivePhoto(photoLists);
                    }
                }
            } else {
                cleanFillBlankViewAndBean();
            }
        } else if (requestCode == WHITE_BOARD_ID && resultCode == Activity.RESULT_OK) {//简答手写返回
            String webUrl = data.getStringExtra("webUrl");
            if (answerDataBean == null) {
                return;
            }
            answerDataBean.setIsAnswer(0);
            List<AnswerListBean.ExamOptionBean> list = answerDataBean.getExamOptionList();
            if (list == null) {
                list = new ArrayList<>();
                answerDataBean.setExamOptionList(list);
            } else {
                list.clear();
            }
            int flag = data.getIntExtra("flag", 2);
            if (flag == 1) {//直接上传
                mPresenter.WhiteUploadSubjectivePhoto(webUrl);
            }
        } else if (requestCode == REQUEST_RADIO && resultCode == Activity.RESULT_OK) {//播放媒体返回
            long time = data.getLongExtra("time", 0);
            if (radioTime == 0) {
                radioTime = time;
            } else {
                radioTime += time;
            }
            if (data.getBooleanExtra("isMini", false)) {
                radioMiniStartTime = System.currentTimeMillis();
                ll_mini.setVisibility(View.VISIBLE);
                RadioService service = RadioServiceManager.getInstance().getService();
                if (service != null) {
                    ivPlay.setImageResource(service.getCurrentState() == RadioService.STARTED ?
                            R.mipmap.mini_pause : R.mipmap.mini_play);
                }
            } else {
                ll_mini.setVisibility(View.GONE);
                saveAndResetRadioTime();
            }
        } else if (requestCode == REQUEST_RES && resultCode == Activity.RESULT_OK) {//查看其他资源返回
            final long time = data.getLongExtra("time", 0);
            final long id = otherResourceId;
            saveResTime(id, time);
            otherResourceId = 0;
        } else if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {//预览图片返回
            boolean isDel = data.getBooleanExtra("isDel", false);
            int examId = Integer.valueOf(data.getStringExtra("examId"));
            int index = data.getIntExtra("index", 0);
            if (isDel) {
                // 删除图片
                mPresenter.removeImage(mList.get(examId), index);
                refreshAdapter();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/

    private void saveResTime(long id, long time) {
        if (id == 0 || time == 0) {
            return;
        }
        for (AnswerListBean.AnswerDataBean bean : mList) {
            if (bean.getResourceId() == id) {
                bean.setIsRead(1);
                bean.setUseTime(bean.getUseTime() + time);
            }
        }
    }

    public void showSelectOpenDialog(final OpenListener listener) {
        final Dialog dialog = new Dialog(activity, R.style.Dialog);
        @SuppressWarnings("inflateParams")
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_open_res, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
    }

    @Override
    public void requestRefreshExam(int examType) {

    }

    @Override
    public void requestRefresh() {
        refreshAdapter();
    }

    @Override
    public void refreshTop() {
        mTopAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null && (service.getCurrentState() == RadioService.PAUSE || service.getCurrentState() == RadioService.STARTED)) {
            service.start();
            ivPlay.setImageResource(R.mipmap.mini_pause);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        RadioService service = RadioServiceManager.getInstance().getService();
        if (service != null && (service.getCurrentState() == RadioService.STARTED || service.getCurrentState() == RadioService.PAUSE)) {
            service.pause();
            ivPlay.setImageResource(R.mipmap.mini_play);
        }
    }

    /*================  ================*/
    @Override
    public void onBufferingUpdate(RadioService service, int percent) {

    }

    @Override
    public void onPrepared(RadioService service) {
        ivPlay.setImageResource(R.mipmap.mini_pause);
    }

    @Override
    public void onCompletion(RadioService service) {
        ivPlay.setImageResource(R.mipmap.mini_play);
    }

    @Override
    public void onError(RadioService service, int what, int extra) {

    }

    /*================  ================*/

    interface OpenListener {
        void openByApp();//app内打开

        void openByOther();//第三方打开
    }

    @Override
    public void onClick(View view) {
        RadioService service = RadioServiceManager.getInstance().getService();
        int i = view.getId();
        if (i == R.id.tvBack) {
            back();
        } else if (i == R.id.tvCommit) {
            if (mPresenter.examIsAllDo(mList)) {
                submitToast("提交后将无法作答，确定提交吗？");
            } else {
                submitToast("存在未作答试题，提交后将无法作答，确定提交吗？");
            }
        } else if (i == R.id.iv_radio) {
            if (service != null) {
                radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                Intent intentRadio = new Intent(activity, PlayerActivity.class);
                intentRadio.putExtra("radioUrl", service.getPlayPath());
                intentRadio.putExtra("title", service.getTitle());
                intentRadio.putExtra(PlayerActivity.SHOW_MINI_BUTTON, true);
                activity.startActivityForResult(intentRadio, REQUEST_RADIO);
            }
        } else if (i == R.id.iv_play) {
            if (service != null) {
                if (service.getCurrentState() == RadioService.STARTED || service.getCurrentState() == RadioService.PLAYBACK_COMPLETED) {
                    service.pause();
                    ivPlay.setImageResource(R.mipmap.mini_play);
                } else if (service.getCurrentState() == RadioService.PAUSE || service.getCurrentState() == RadioService.PLAYBACK_COMPLETED) {
                    service.start();
                    ivPlay.setImageResource(R.mipmap.mini_pause);
                } else {
                    service.cleanAndReset();
                    ll_mini.setVisibility(View.GONE);
                }
            }

        } else if (i == R.id.iv_close) {
            if (service != null) {
                service.cleanAndReset();
                ll_mini.setVisibility(View.GONE);
            }
            radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
            saveAndResetRadioTime();

        } else {
        }
    }
/*
    @SuppressWarnings("unused")
    @Event(value = {R.id.tvBack, R.id.tvCommit, R.id.iv_radio, R.id.iv_play, R.id.iv_close})
    private void onClick(View view) {
        RadioService service = RadioServiceManager.getInstance().getService();
        switch (view.getId()) {
            case R.id.tvBack:
                back();
                break;
            case R.id.tvCommit:
                if (mPresenter.examIsAllDo(mList)) {
                    submitToast("提交后将无法作答，确定提交吗？");
                } else {
                    submitToast("存在未作答试题，提交后将无法作答，确定提交吗？");
                }
                break;
            case R.id.iv_radio:
                if (service != null) {
                    radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                    Intent intentRadio = new Intent(activity, PlayerActivity.class);
                    intentRadio.putExtra("radioUrl", service.getPlayPath());
                    intentRadio.putExtra("title", service.getTitle());
                    intentRadio.putExtra(PlayerActivity.SHOW_MINI_BUTTON, true);
                    activity.startActivityForResult(intentRadio, REQUEST_RADIO);
                }
                break;
            case R.id.iv_play:
                if (service != null) {
                    if (service.getCurrentState() == RadioService.STARTED || service.getCurrentState() == RadioService.PLAYBACK_COMPLETED) {
                        service.pause();
                        ivPlay.setImageResource(R.mipmap.mini_play);
                    } else if (service.getCurrentState() == RadioService.PAUSE || service.getCurrentState() == RadioService.PLAYBACK_COMPLETED) {
                        service.start();
                        ivPlay.setImageResource(R.mipmap.mini_pause);
                    } else {
                        service.cleanAndReset();
                        ll_mini.setVisibility(View.GONE);
                    }
                }
                break;
            case R.id.iv_close:
                if (service != null) {
                    service.cleanAndReset();
                    ll_mini.setVisibility(View.GONE);
                }
                radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                saveAndResetRadioTime();
                break;
            default:
                break;
        }
    }
*/

    private void submitToast(String s) {
        if (mRadioGroup.getEditText() != null) {
            hideKeyBoard(mRadioGroup.getEditText());
        }
        mPresenter.showDialog(s, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (radioMiniStartTime != 0) {
                    radioMiniTime += (System.currentTimeMillis() - radioMiniStartTime) / 1000;
                    saveAndResetRadioTime();
                }
                submitCount = 0;
                submitAnswer();

            }
        });
    }

    /**
     * 保存并重置播放时间
     */
    private void saveAndResetRadioTime() {
        final long time = radioTime + radioMiniTime;
        final long id = radioResourceId;
        saveResTime(id, time);
        radioResourceId = 0;
        radioTime = 0;
        radioMiniTime = 0;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            back();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back() {
        if (mRadioGroup.getEditText() != null) {
            hideKeyBoard(mRadioGroup.getEditText());
        }
        mPresenter.showDialog("试题还未提交，确定退出吗？", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onExitSave();
            }
        });
    }

    private void onExitSave() {
        isExit = true;
        submitOneMinCount = 0;
        submitAnswerOneMin(true);
    }

    private void submitAnswer() {
        isExit = true;
        mDialogManager.showWaiteDialog("正在提交...");
        mPresenter.submitAnswer(mList, taskDataBean, serverTime);
    }

    private void submitAnswerOneMin(boolean showProgress) {
        mPresenter.submitAnswerOneMin(mList, taskDataBean, serverTime, showProgress);
    }

    public void submitAnswerOneMinSuccess() {
        resetSubmitOneMin();
    }

    @Override
    public void submitAnswerOneMinFailure(String msg, boolean showProgress) {
        submitOneMinCount++;
        if (submitOneMinCount < 3) {
            submitAnswerOneMin(showProgress);
        } else {
            resetSubmitOneMin();
        }
    }

    /**
     * 重置提交次数
     */
    private void resetSubmitOneMin() {
        submitOneMinCount = 0;
        if (isExit) {
            EventBus.getDefault().post(new RefreshTaskEvent());
            finish();
        }
    }

    @Override
    public void submitAnswerFailure(String msg) {
        mDialogManager.cancelWaiteDialog();
        submitCount++;
        if (submitCount < 3) {
            submitAnswer();
        } else {
            submitCount = 0;
            ToastUtil.showShortlToast(msg);
        }
    }

    /**
     * 计时器
     */
    public class MyCountDownTimer extends CountDownTimer {
        TextView textView;
        int count = 0;

        MyCountDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.textView = textView;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (textView != null) {
                textView.setText(DateUtil.millsecondsToStr(millisUntilFinished));
            }
            count++;
            if (count == 60) {
                count = 0;
                // 提交一次
                submitOneMinCount = 0;
                submitAnswerOneMin(false);
            }
        }

        @Override
        public void onFinish() {
            cancel();
            textView.setVisibility(View.GONE);
        }
    }

    private void initRadioButton() {
        //手写重做
        tvRedoWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog.Builder builder = new CustomDialog.Builder(activity);
                builder.setTitle("提示");
                builder.setMessage("是否重新作答此空？");
                builder.setPositive("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sManager.redo();
                    }
                });
                builder.setNegative("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                CustomDialog dialog = builder.create();
                dialog.show();
            }
        });

        //拍照重做
        tvRedoCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog.Builder builder = new CustomDialog.Builder(activity);
                builder.setTitle("提示");
                builder.setMessage("是否重新作答此空？");
                builder.setPositive("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ClozeAnswerBean bean = mRadioGroup.getBean();
                        FillBlankEditText editText = mRadioGroup.getEditText();
                        bean.setAnswer("");
                        editText.rebuildText();
                        for (int i = 0; i < editText.getAnswerMap().size(); i++) {
                            if (editText.getAnswerMap().valueAt(i) == bean) {
                                editText.selectBlank(i);
                                break;
                            }
                        }
                        tvRedoCamera.setEnabled(false);
                        ivPic.setImageDrawable(null);
                        ivPic.setVisibility(View.GONE);
                        ivAddPic.setVisibility(View.VISIBLE);
                    }
                });
                builder.setNegative("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                CustomDialog dialog = builder.create();
                dialog.show();
            }
        });
        //222222
        //拍照添加图片
        ivAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showSelectImgDialog(new AlbumOrCameraListener() {
                    @Override
                    public void selectAlbum() {
                        saveFillBlankViewAndBean();
//                        MPermissions.requestPermissions(activity, REQUEST_FILL_BLANK_SDCAED, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
//                        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
//                        EasyPermissions.requestPermissions(activity, "请设置权限",
//                                REQUEST_FILL_BLANK_SDCAED, perms);
                        methodRequiresTwoPermission2();
                    }

                    @Override
                    public void selectCamera() {
                        saveFillBlankViewAndBean();
//                        MPermissions.requestPermissions(activity, REQUEST_FILL_BLANK_CAMERA, Manifest.permission.CAMERA);
//                        String[] perms = {Manifest.permission.CAMERA};
//                        EasyPermissions.requestPermissions(activity, "请设置权限",
//                                REQUEST_FILL_BLANK_CAMERA, perms);
                        methodRequiresTwoPermission4();
                    }
                });
            }
        });

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean noChecked = true;
                for (int i = 0; i < group.getChildCount(); i++) {
                    RadioButton rb = (RadioButton) group.getChildAt(i);
                    if (rb.isChecked() && rb.getId() == checkedId) {
                        noChecked = false;
                        break;
                    }
                }
                if (noChecked) {
                    return;
                }
                ClozeAnswerBean answerBean = mRadioGroup.getBean();
                if (answerBean != null && InputTools.isPenWrite(answerBean.getInputMethod()) &&
                        sManager != null && sManager.isTouch()) {
                    savePhoto();
                }
                ClozeAnswerBean bean = mRadioGroup.getBean();
                if (checkedId == R.id.rb_keyboard) {
                    if (bean != null) {
                        bean.setInputMethod(InputMethod.KeyBoard);
                    }
                    showKeyBoard(mRadioGroup.getEditText());
                    rlInputKeyboard.setVisibility(View.VISIBLE);
                    rlInputCamera.setVisibility(View.GONE);
                    rlInputWrite.setVisibility(View.GONE);
                    isShowHandWrite = false;

                } else if (checkedId == R.id.rb_write) {
                    if (bean != null) {
                        bean.setInputMethod(InputMethod.PenWrite);
                    }
                    hideKeyBoard(mRadioGroup.getEditText());
                    rlInputKeyboard.setVisibility(View.GONE);
                    rlInputWrite.setVisibility(View.VISIBLE);
                    rlInputCamera.setVisibility(View.GONE);
                    if (mRadioGroup.getBean() != null && !TextUtils.isEmpty(mRadioGroup.getBean().getAnswer())) {
                        mPresenter.loadLocalData(getAppId(), getAppNum(), getAppPosition());
                    }
                    isShowHandWrite = true;

                } else if (checkedId == R.id.rb_picture) {
                    if (bean != null) {
                        bean.setInputMethod(InputMethod.Picture);
                        if (TextUtils.isEmpty(bean.getAnswer())) {
                            ivAddPic.setVisibility(View.VISIBLE);
                            ivPic.setVisibility(View.GONE);
                        } else {
                            ivAddPic.setVisibility(View.GONE);
                            ivPic.setVisibility(View.VISIBLE);
                            loadImageIntoPicView(bean.getAnswer());
                        }
                    }
                    hideKeyBoard(mRadioGroup.getEditText());
                    rlInputKeyboard.setVisibility(View.GONE);
                    rlInputWrite.setVisibility(View.GONE);
                    rlInputCamera.setVisibility(View.VISIBLE);
                    isShowHandWrite = false;

                } else {
                    isShowHandWrite = false;

                }
            }
        });
    }

    @Override
    public void onRedoCallBack() {
        changeUI();
        mPresenter.deleteDots(getAppId(), getAppNum(), getAppPosition());
    }

    //=====================↓↓↓ 填空手写全局变量 ↓↓↓=====================
    private boolean isActive;//该应用在活动
    private boolean isShowHandWrite;//填空手写是否显示

    private static HandWriteManager sManager;

//    private PenCommAgent bleManager;
    //=====================↑↑↑ 填空手写全局变量 ↑↑↑=====================

    //=====================↓↓↓ 填空题手写笔 ↓↓↓=====================

    /**
     * 初始化填空题手写
     */
    private void initHandWrite() {
        sManager = new HandWriteManager(this, this, ivDraw);
        if (BLEServiceManger.getInstance().getService() == null) {
            BLEServiceManger.getInstance().bindService(activity);
        } else {
            addServiceListener();
        }
//
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        mWidth = dm.widthPixels;
        mHeight = dm.heightPixels;

        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        param.width = (int) mWidth;
        param.height = (int) mHeight;
        param.rightMargin = 1;
        param.bottomMargin = 1;

        sManager.drawInit();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivDraw.getLayoutParams();
        float ratio;
        if (mWidth == 1536 && mHeight == 2048) {  // 适应小米PAD,4:3
            ratio = (mWidth * 63 / 72) / sManager.BG_REAL_WIDTH;
        } else if (mWidth == 1536 && mHeight == 1952) { // 非标准4：3，适用iFive
            ratio = (mWidth * 60 / 72) / sManager.BG_REAL_WIDTH;
        } else {
            ratio = (mWidth * 70 / 72) / sManager.BG_REAL_WIDTH;
        }
        sManager.BG_WIDTH = (int) (sManager.BG_REAL_WIDTH * ratio);
        sManager.BG_HEIGHT = (int) (sManager.BG_REAL_HEIGHT * ratio);

        int gcontentLeft = activity.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getLeft();
        //布局左边缘和顶部的位置
        int gcontentTop = activity.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        sManager.A5_X_OFFSET = (int) (mWidth - gcontentLeft - sManager.BG_WIDTH) / 2;
        sManager.A5_Y_OFFSET = (int) (mHeight - gcontentTop - sManager.BG_HEIGHT) / 2;

        ivDraw.setLayoutParams(params);
        changeUI();
    }

    @Override
    public void onReplayFinish() {
        changeUI();
    }

    private void changeUI() {
        //是否书写, 书写后切换填空需要上传图片
        boolean isTouch = sManager.isTouch();
        ClozeAnswerBean bean = mRadioGroup.getBean();
        //有作答内容或已书写
        if ((bean != null && InputTools.isPenWrite(bean.getInputMethod()) && !TextUtils.isEmpty(bean.getAnswer())) || isTouch) {
            tvRedoWrite.setEnabled(isTouch);
        }
        changeButtonState(rbWrite, isTouch, rbKeyBoard, rbPicture);
    }

    private BluetoothLEService.BaseOnDataReceiveListener mServiceListener;

    private BluetoothLEService.BaseOnDataReceiveListener getDataListener() {
        if (mServiceListener == null) {
            mServiceListener = new BluetoothLEService.BaseOnDataReceiveListener() {
                @Override
                public void onConnected() {
                    ivDraw.setPenIsConnected(true);
                    if (!sManager.isTouch()) {
                        ivDraw.initDraw(ivDraw.getContext());
                    }
                }

                @Override
                public void onDisconnected() {
                    ivDraw.setPenIsConnected(false);
                    if (!sManager.isTouch()) {
                        ivDraw.initDraw(ivDraw.getContext());
                    }
                    CustomDialog.Builder builder = new CustomDialog.Builder(activity);
                    builder.setMessage("手写笔连接中断，是否保存当前作答？");
                    builder.setPositive("保存", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            savePhoto();
                        }
                    });
                    builder.setNegative("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (mRadioGroup.getEditText() != null) {
                                closeInput(mRadioGroup.getEditText(), false);
                            }
                        }
                    });
                    CustomDialog dialog = builder.create();
                    dialog.show();
                }

                @Override
                public void onDataReceive(final Dot dot) {
                    if (!isActive || !isShowHandWrite) {
                        return;
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (sManager != null) {
                                sManager.processDots(dot);
                            }
                        }
                    });
                }

                @Override
                public void onReceiveOIDSize(int OIDSize) {
                    sManager.gCurPageID = -1;
                }
            };
        }
        return mServiceListener;
    }

    /**
     * 绑定成功后添加监听
     */
    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceConnected(BLEServiceEvent event) {
        if (event.getService() != null) {
            addServiceListener();
        }
    }

    /**
     * 添加回调监听
     */
    private void addServiceListener() {
        BLEServiceManger.getInstance().getService().addOnDataReceiveListener(getDataListener());
        ivDraw.setPenIsConnected(BLEServiceManger.getInstance().getService().isPenConnected());
        ivDraw.initDraw(ivDraw.getContext());
//        if (!BLEServiceManger.getInstance().getService().isPenConnected()) {
//            ivDraw.setPenIsConnected(false);
//            ivDraw.initDraw(ivDraw.getContext());
//        }
    }

    //重新加载本地数据成功
    @Override
    public void loadLocalDataSuccess(List<Dots> list) {
        sManager.loadLocalDataSuccess(list);
    }

    /**
     * 储绘制点成功
     */
    @Override
    public void saveDotsSuccess(RectF rectF) {
        Bitmap bitmap;
        if (rectF != null) {
            bitmap = Bitmap.createBitmap(ivDraw.backBitmap, ((int) rectF.left), ((int) rectF.top),
                    ((int) (rectF.right - rectF.left)), ((int) (rectF.bottom - rectF.top)));
        } else {
            bitmap = ivDraw.backBitmap;
        }
        sManager.cleanDots();
        String imagePath = ImageUtils.savePhoto(bitmap, Environment.getExternalStorageDirectory().getAbsolutePath(),
                String.valueOf(System.currentTimeMillis()));
        if (imagePath != null) {
            // 拿着imagePath上传了
            mPresenter.whiteUploadFillBlank(imagePath);
        } else {
            ToastUtil.showLonglToast("图片截取失败");
        }
    }

    //保存填空图片时赋值 手写/拍照
    //手写保存/图片上传时赋值, 保存完成后清除
    private FillBlankEditText mTempEditText;
    private ClozeAnswerBean mTempAnswerBean;
//    /**
//     * 填空手写上传成功
//     */
//    @Override
//    public void onFillBlankWhiteUploadSuccess(UploadPicVo.DataBean bean) {//上传成功后更新对应填空bean的answer
//        mTempAnswerBean.setAnswer(bean.getOriginalPath());
//        mTempEditText.rebuildText();
//        mTempEditText = null;
//        mTempAnswerBean = null;
//    }

    /**
     * 填空保存手写图片
     */
    private void savePhoto() {
        if (mRadioGroup.getEditText() == null || mRadioGroup.getBean() == null) {
            String msg = (mRadioGroup.getEditText() == null ? "<EditText = null> " : "")
                    + (mRadioGroup.getBean() == null ? "<bean = null>" : "");
            CrashReport.postCatchedException(new RuntimeException(msg));
            ToastUtil.showShortlToast("保存失败");
            return;
        }
        mTempEditText = mRadioGroup.getEditText();
        mTempAnswerBean = mRadioGroup.getBean();
        sManager.setTouch(false);
        //保存数据库
        Collection<Dots> list = sManager.getDotsList();
        if (list.isEmpty()) {
            closeInput(null, false);
        } else {
            mPresenter.deleteDots(getAppId(), getAppNum(), getAppPosition());
            int size = 8;
            RectF rectF = new RectF(-1, -1, -1, -1);
            for (Dots dots : list) {
                if (rectF.top == -1) {
                    rectF.top = dots.pointY;
                }
                if (rectF.bottom == -1) {
                    rectF.bottom = dots.pointY;
                }
                if (rectF.left == -1) {
                    rectF.left = dots.pointX;
                }
                if (rectF.right == -1) {
                    rectF.right = dots.pointX;
                }
                if (dots.pointX > rectF.right) {
                    rectF.right = dots.pointX;
                } else if (dots.pointX < rectF.left) {
                    rectF.left = dots.pointX;
                }
                if (dots.pointY > rectF.bottom) {
                    rectF.bottom = dots.pointY;
                } else if (dots.pointY < rectF.top) {
                    rectF.top = dots.pointY;
                }
            }
            //x(0~992
            //y(0~1403
            rectF.left -= size;
            rectF.right += size;
            rectF.top -= size;
            rectF.bottom += size;
            if (rectF.left < 0) {
                rectF.left = 0;
            }
            if (rectF.top < 0) {
                rectF.top = 0;
            }
            if (rectF.right > 992) {
                rectF.right = 992;
            }
            if (rectF.bottom > 1403) {
                rectF.bottom = 1403;
            }
            mPresenter.saveDots(list, rectF);
        }
    }

    /**
     * 保存失败
     */
    @Override
    public void saveDotsFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onSaveData() {
        changeUI();
    }

    @Override
    public String getAppId() {
        FillBlankEditText et = mRadioGroup.getEditText();
        return "" + taskDataBean.getId() + et.getAnswerDataBean().getId();//任务id
    }

    @Override
    public String getAppNum() {
        return "" + mRadioGroup.getExamPosition();//题号
    }

    @Override
    public int getAppPosition() {
        FillBlankEditText et = mRadioGroup.getEditText();
        return et.getAnswerMap().indexOfValue(mRadioGroup.getBean());//填空第几个空
    }
    //=====================↑↑↑ 填空题手写笔 ↑↑↑=====================
}
