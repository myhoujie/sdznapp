package com.sdzn.fzx.student.libbase.baseui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;


public class OtherLoginTipActivity extends BaseActivity implements View.OnClickListener {

    public static final String EXTRA_OTHER_LOGIN_MSG = "other_login_msg";

    private TextView msgContentTxt;
    private TextView reLoad;
    private TextView exit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_login_tip);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(OtherLoginTipActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        msgContentTxt = (TextView) findViewById(R.id.msg_content_txt);
        reLoad = (TextView) findViewById(R.id.re_load);
        exit = (TextView) findViewById(R.id.exit);

        reLoad.setOnClickListener(this);
        exit.setOnClickListener(this);
        Intent intent = new Intent(this, MqttService.class);
        stopService(intent);
    }

    @Override
    protected void initData() {
        final Intent intent = getIntent();
        String msg = intent.getStringExtra(EXTRA_OTHER_LOGIN_MSG);
        if (TextUtils.isEmpty(msg))
            msg = "您的账号在其他设备上登录，如不是本人操作，请重新登录并及时修改密码";
        msgContentTxt.setText(msg);

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        StudentSPUtils.put(App2.get(), StudentSPUtils.LOGIN_CHECK_STATUS, false);
//        StudentSPUtils.put(App2.get(), StudentSPUtils.LOGIN_USER_PWD, "");

        if (i == R.id.re_load) {
            if (CommonAppUtils.isLingChuangPad()) {
                CommonAppUtils.logout();
                stopService(new Intent(App2.get(), MqttService.class));
                ActivityManager.exit();
                System.exit(0);
            } else {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                ActivityManager.finishAll();
            }
        } else if (i == R.id.exit) {
            if (CommonAppUtils.isLingChuangPad()) {
                CommonAppUtils.logout();
                stopService(new Intent(App2.get(), MqttService.class));
                ActivityManager.exit();
                System.exit(0);
            } else {
                ActivityManager.finishAll();
                System.exit(0);
            }
        }
    }
}
