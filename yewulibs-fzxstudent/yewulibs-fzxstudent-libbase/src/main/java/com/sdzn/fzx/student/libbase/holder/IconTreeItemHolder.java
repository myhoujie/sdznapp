package com.sdzn.fzx.student.libbase.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.sdzn.fzx.student.libbase.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 *
 */
public class IconTreeItemHolder extends TreeNode.BaseNodeViewHolder<IconTreeItemHolder.IconTreeItem> {
    private PrintView arrowView;

    public IconTreeItemHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_icon_node, null, false);
        TextView tvName = view.findViewById(R.id.tv_name);
        tvName.setText(value.name);
        TextView tvValue = view.findViewById(R.id.tv_degree);
        tvValue.setText(value.degree);

        arrowView = view.findViewById(R.id.arrow_icon);

        return view;
    }

    @Override
    public void toggle(boolean active) {
        arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
    }

    public static class IconTreeItem {
        public String name;
        public String degree;
        boolean last;
        String amount, errors, accuracy;

        public IconTreeItem(String name, String degree, boolean last, String amount, String errors, String accuracy) {
            this.name = name;
            this.degree = degree;
            this.last = last;
            this.amount = amount;
            this.errors = errors;
            this.accuracy = accuracy;
        }
    }
}
