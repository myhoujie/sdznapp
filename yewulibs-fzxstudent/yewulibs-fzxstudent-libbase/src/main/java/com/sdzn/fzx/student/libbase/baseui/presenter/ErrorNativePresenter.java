package com.sdzn.fzx.student.libbase.baseui.presenter;

import android.text.TextUtils;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.activity.ErrorNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.view.ErrorNativeView;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.ExamText;
import com.sdzn.fzx.student.vo.ExamTypeVo;
import com.sdzn.fzx.student.vo.SubjectErrorBean;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/12/11
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */
public class ErrorNativePresenter extends BasePresenter<ErrorNativeView, ErrorNativeActivity> {
    /**
     * 加载试题
     */
    public void getSubjectErrorList(Map<String, String> params) {
        Network.createTokenService(NetWorkService.SubjectErrorList.class)
                .SubjectErrorList(params)
                .map(new StatusFunc<SubjectErrorBean>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<SubjectErrorBean>(new SubscriberListener<SubjectErrorBean>() {
                    @Override
                    public void onNext(SubjectErrorBean subjectErrorBean) {

                        initData(subjectErrorBean);
                        mView.setSubjectListSuccess(subjectErrorBean);

                    }

                    private void initData(SubjectErrorBean beans) {
                        List<SubjectErrorBean.DataBean> list = beans.getData();
                        if (list == null) {
                            return;
                        }
                        for (SubjectErrorBean.DataBean bean : list) {
                            if (bean.getType() == 3) {//纯文本
                                continue;
                            }
                            if (bean.getType() == 2) {//资源
                                String resourceText = bean.getExamText();
                                ExamText examText = GsonUtil.fromJson(resourceText, ExamText.class);
                                if (examText == null) {
                                    throw new RuntimeException();
                                }
                                bean.setExamTextVo(examText);
                                continue;
                            }
                            String resourceText = bean.getExamText();
                            ExamText examText = GsonUtil.fromJson(resourceText, ExamText.class);
                            if (examText == null) {
                                throw new RuntimeException();
                            }
                            bean.setExamTextVo(examText);

                        /*    //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);*/
                            //综合题额外进行一轮解析
                            if (bean.getExamTemplateId() != 16) {
                                continue;
                            }
                            for (SubjectErrorBean.DataBean dataBean : bean.getExamList()) {
                                ExamText json = GsonUtil.fromJson(dataBean.getExamText(), ExamText.class);
                                dataBean.setExamTextVo(json);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                        String msg = "请求失败";
                        if (e instanceof ApiException) {
                            msg = ((ApiException) e).getMsg();
                        }
                        ToastUtil.showShortlToast(msg);
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }


    private String model;
    private String baseSubjectId;
    private String examTemplateStyleId;
    private String timeType;
    private String count;
    private String chapterName;


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(String baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getExamTemplateStyleId() {
        return examTemplateStyleId;
    }

    public void setExamTemplateStyleId(String examTemplateStyleId) {
        this.examTemplateStyleId = examTemplateStyleId;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }


    public Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();

        params.put("userStudentId", UserController.getUserId());
        params.put("model", getModel());
        params.put("baseSubjectId", getBaseSubjectId());
        if (!TextUtils.isEmpty(examTemplateStyleId)) {
            params.put("examTemplateStyleId", getExamTemplateStyleId());
        }
        if (!TextUtils.isEmpty(timeType)) {
            params.put("timeType", getTimeType());
        }
        if (!TextUtils.isEmpty(count)) {
            params.put("count", getCount());
        }
        if (!TextUtils.isEmpty(chapterName)) {
            params.put("chapterName", getChapterName());
        }


        return params;
    }

    public void getExamTypeList() {
        final String baseLevelId = UserController.getBaseLevelId();
        Network.createTokenService(NetWorkService.ExamService.class)
                .getExamList(baseLevelId, baseSubjectId)
                .map(new StatusFunc<ExamTypeVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ExamTypeVo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ExamTypeVo o) {
                        dealExamType(o);
                    }
                });
    }

    private void dealExamType(ExamTypeVo o) {
        final ArrayList<ExamTypeVo.DataBean> dataBeans = new ArrayList<>();
        final ArrayList<String> strings = new ArrayList<>();
        if (o != null && o.getData().size() > 0) {
            dataBeans.addAll(o.getData());
            for (ExamTypeVo.DataBean bean : o.getData()) {
                strings.add(bean.getTemplateStyleName());
            }
        }
        mView.onExamTypeSuccess(dataBeans, strings);

    }
}
