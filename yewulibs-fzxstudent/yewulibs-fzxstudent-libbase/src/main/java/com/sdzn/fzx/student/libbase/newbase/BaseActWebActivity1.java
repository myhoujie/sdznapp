package com.sdzn.fzx.student.libbase.newbase;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.just.agentweb.base.BaseStudentCurrencyAgentWebActivity;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libpublic.views.CalenderClearEditText;
import com.sdzn.fzx.student.libpublic.views.ClearableEditText;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;

import java.util.Calendar;

import me.jessyan.autosize.AutoSizeCompat;

public abstract class BaseActWebActivity1 extends BaseStudentCurrencyAgentWebActivity implements NetconListener2 {
    public TextView tvBack;//返回
    public TextView tvTitleName;//标题名称
    public TextView tvDownTitle;//下拉选择
    public View arrowIv;//下拉
    public XRecyclerView recyclerViewTitle;//RecyclerView滑动选择
    public TextView tvTijiaoTitle;//提交按钮
    public TextView tvSousuoTitle;//搜索按钮
    public TextView tvShijianTitle;//时间按钮
    public TextView tvWeizhi;//未知按钮
    public CalenderClearEditText tvZankaiShijian;//时间选择器Edtext
    public TextView tvClassRemove;//我的班级移除解绑
    public TextView tvMyAdd;//合作添加
    private TextView tvChongxinpg;//重新批改
    private TextView tvFabupg;//发布批改
    public TextView tvMyTitle;//个人中心按钮
    public TextView tvMore;//更多(三点)
    public TextView tvTijiaoanniu;//提交按钮
    public TextView tvShaiXuan;//筛选
    public TextView tvXiaoZu;//合作讨论 小组
    public LinearLayout llDownTitle;//
    protected SmartRefreshLayout refreshLayout1;//刷新


    protected EmptyViewNew1 emptyview1;//网络监听
    private long mCurrentMs = System.currentTimeMillis();
    protected NetState netState;
    public Activity activity;
    public static String URL;

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources());//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity(super.getResources(), 800, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setup(savedInstanceState);
        URL = SPUtils.getInstance().getString("url", "http://49.4.7.45:8090/#/");
        activity = this;
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, this);
        findiview();
        onclickview();
        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                RefreshLoad();
            }
        }, 1000);
    }

    /*加载布局*/
    protected abstract int getLayoutId();


    /**/
    protected void setup(@Nullable Bundle savedInstanceState) {
        tvBack = (TextView) findViewById(R.id.tv_back);//返回
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);//标题名称
        tvDownTitle = (TextView) findViewById(R.id.tv_down_title);//下拉列表名称
        llDownTitle = (LinearLayout) findViewById(R.id.ll_down_title);//下拉状态
        arrowIv = (View) findViewById(R.id.arrowIv);
        recyclerViewTitle = (XRecyclerView) findViewById(R.id.recycler_view_title);//recyclerVie滑动
        tvTijiaoTitle = (TextView) findViewById(R.id.tv_tijiao_title);//提交按钮
        tvSousuoTitle = (TextView) findViewById(R.id.tv_sousuo_title);//搜索按钮
        tvShijianTitle = (TextView) findViewById(R.id.tv_shijian_title);//时间按钮
        tvWeizhi = (TextView) findViewById(R.id.tv_weizhi);//未知按钮
        tvZankaiShijian = (CalenderClearEditText) findViewById(R.id.tv_zankai_shijian);//时间展示Editetext
        tvClassRemove = (TextView) findViewById(R.id.tv_class_remove);//我的班级移除解绑
        tvMyAdd = (TextView) findViewById(R.id.tv_my_add);//添加按钮
        tvChongxinpg = (TextView) findViewById(R.id.tv_chongxinpg);//重新批改按钮
        tvFabupg = (TextView) findViewById(R.id.tv_fabupg);//发布批改按钮
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);//个人中心按钮
        tvMore = (TextView) findViewById(R.id.tv_more);//更多按钮
        tvTijiaoanniu = findViewById(R.id.tv_tijiaoanniu);//提交按钮
        tvShaiXuan = findViewById(R.id.tv_shaixuan);//筛选
        tvXiaoZu = findViewById(R.id.tv_zu);//筛选
        emptyview1 = findViewById(R.id.emptyview2_order);//网络状态
        refreshLayout1 = findViewById(R.id.refreshLayout1_order);//刷新控件
        emptyview1.loading();
        //使上拉加载具有弹性效果
        refreshLayout1.setEnableAutoLoadMore(false);
        //禁止越界拖动（1.0.4以上版本）
        refreshLayout1.setEnableOverScrollDrag(false);
        //关闭越界回弹功能
        refreshLayout1.setEnableOverScrollBounce(false);
        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
            @Override
            public boolean canRefresh(View content) {
                //webview滚动到顶部才可以下拉刷新
                MyLogUtil.e("ssssss", "" + mAgentWeb.getWebCreator().getWebView().getScrollY());
//                return mAgentWeb.getWebCreator().getWebView().getScrollY() <= 0;
                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
            }

            @Override
            public boolean canLoadMore(View content) {
                return false;
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                //刷新内容
                RefreshLoad();
            }
        });

        /*绑定empty方法*/
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                RefreshLoad();
            }
        });

        clickListener();
    }

    /*重新加载*/
    protected void RefreshLoad() {

    }

    @Override
    public void net_con_none() {
        ToastUtils.showLong("网络异常，请检查网络连接！");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
    }

    protected void findiview() {

    }

    /*绑定控件*/
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.ll_base_container);
    }

    /*设置标题*/
    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "");
    }

    protected void setTitleContent(String title, String content) {
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 20) {
                title = title.substring(0, 20).concat("...");
            }
        }
        if (TextUtils.isEmpty(content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
            return;
        }
        if (TextUtils.equals(title, content)) {
            if (tvTitleName != null) {
                tvTitleName.setText(title);
            }
        } else {
            if (tvTitleName != null) {
                tvTitleName.setText(content);
            }
        }
    }

    protected void onclickview() {

    }

    private BaseOnClickListener mListener;

    public void setBaseOnClickListener(BaseOnClickListener listener) {
        mListener = listener;
    }


    //个人中心
    public void Titlegrzx() {
        CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");

            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }

    //时间
    public void Titleshijian() {
        showCalendarDialog();
    }

    //展开时间
    public void Titlezankaishijian() {
    }

    /*搜索*/
    public void Titlesousuo() {

    }

    /*删除*/
    public void TitleRemove() {
    }

    /*提交*/
    public void Titletijiao() {
        ToastUtils.showLong("点击提交");
    }

    /*下拉加载*/
    public void TitleDropdown() {
    }

    /*返回*/
    public void TitleBack() {
        if (mAgentWeb.getWebCreator().getWebView().canGoBack()) {
            mAgentWeb.back();
        } else {
            finish();
        }
    }

    /*重新批改*/
    public void TitleChongxinpg() {
    }

    /*发布批改*/
    public void TitleFabupg() {
    }

    /*添加*/
    public void TitleAdd() {
    }

    /*更多点击事件*/
    public void TitleMore() {
    }

    /*更多点击事件*/
    public void Tijiaoanniu() {

    }

    /*筛选按钮*/
    public void Shaixuananniu() {

    }

    /*合作讨论 小组*/
    public void Xiaozuanniu() {

    }

    /*选择学科*/
    public void TitleXueKeDropdown() {

    }

    /*选择--多场景*/
    public void TitleMultDropdown() {

    }


    /*时间选择器-------------------------------------开始*/
    private CalenderDialog calendarDialog;//日历dialog

    public String startTime;//开始时间
    public String endTime;//结束时间

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(this, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvZankaiShijian.setVisibility(View.VISIBLE);
                    tvShijianTitle.setVisibility(View.GONE);
                    tvZankaiShijian.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                    listener.returnRefresh(startTime, endTime);
                }
            });
        }
        calendarDialog.show();

    }

    //回调时间数据
    private static refreshOnDisplayListener listener;

    public interface refreshOnDisplayListener {
        void returnRefresh(String startTime, String endTime);
    }

    public static void setOnDisplayRefreshListener(refreshOnDisplayListener myListener) {
        listener = myListener;
    }

    /*时间选择器-------------------------------------结束*/

    /**
     * 刷新webView
     */
    public void AgentwebRefresh(String url) {
//        mAgentWeb.getUrlLoader().reload(); // 刷新
        loadWebSite(url); // 刷新
    }


    /**
     * 打开浏览器
     *
     * @param targetUrl 外部浏览器打开的地址
     */
    private void openBrowser(String targetUrl) {
        if (TextUtils.isEmpty(targetUrl) || targetUrl.startsWith("file://")) {
            ToastUtils.showLong(targetUrl + "该链接无法使用浏览器打开");
            return;
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri mUri = Uri.parse(targetUrl);
        intent.setData(mUri);
        startActivity(intent);
    }

    /**
     * 清除 WebView 缓存
     */
    private void toCleanWebCache() {

        if (this.mAgentWeb != null) {
            //清理所有跟WebView相关的缓存 ，数据库， 历史记录 等。
            this.mAgentWeb.clearWebCache();
            ToastUtils.showLong("已清理缓存");
            //清空所有 AgentWeb 硬盘缓存，包括 WebView 的缓存 , AgentWeb 下载的图片 ，视频 ，apk 等文件。
//            AgentWebConfig.clearDiskCache(this.getContext());
        }
    }

    @Override
    public void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();//恢复
        super.onResume();
    }

    @Override
    public void onPause() {
        mAgentWeb.getWebLifeCycle().onPause(); //暂停应用内所有WebView ， 调用mWebView.resumeTimers();/mAgentWeb.getWebLifeCycle().onResume(); 恢复。
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (this.mAgentWeb != null) {
            this.mAgentWeb.clearWebCache();
        }
        if (netState != null) {
            netState.unregisterReceiver();
        }
        mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }


    /*点击事件添加回调*/
    protected void clickListener() {
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setClearTextListener(new ClearableEditText.ClearTextListener() {
                @Override
                public void onTextClear() {
                    tvShijianTitle.setVisibility(View.VISIBLE);
                    tvZankaiShijian.setVisibility(View.GONE);
                }
            });
        }
        /*返回点击*/
        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleBack();
                    }
                }
            });
        }

        /*下拉点击事件*/
        if (llDownTitle != null) {
            llDownTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.TitleDropdown();
                    }
                }
            });
        }
        if (tvTijiaoTitle != null) {
            tvTijiaoTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titletijiao();
                    }
                }
            });
        }
        if (tvSousuoTitle != null) {
            tvSousuoTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlesousuo();
                    }
                }
            });
        }
        if (tvShijianTitle != null) {
            tvShijianTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titleshijian();
                    }
                }
            });
        }
        if (tvZankaiShijian != null) {
            tvZankaiShijian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlezankaishijian();
                    }
                }
            });
        }
        if (tvMyTitle != null) {
            tvMyTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Titlegrzx();
                    }
                }
            });
        }
        if (tvTijiaoanniu != null) {
            tvTijiaoanniu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Tijiaoanniu();
                    }
                }
            });
        }
        if (tvShaiXuan != null) {
            tvShaiXuan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Shaixuananniu();
                    }
                }
            });
        }
        if (tvXiaoZu != null) {
            tvXiaoZu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.Xiaozuanniu();
                    }
                }
            });
        }
    }

    /**
     * @param LayoutStyle 返回判断
     */
    protected void TitleShowHideState(int LayoutStyle) {
        if (LayoutStyle == 1) { /*显示搜索，个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvSousuoTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 2) { /*个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 3) { /*提交,个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvTijiaoanniu.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 4) { /*显示搜索，筛选,个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
//            tvSousuoTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvShaiXuan.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 5) { /*小组,个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvXiaoZu.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 6) { /*筛选,个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
//            tvShaiXuan.setVisibility(View.VISIBLE);
            return;
        }
    }
}
