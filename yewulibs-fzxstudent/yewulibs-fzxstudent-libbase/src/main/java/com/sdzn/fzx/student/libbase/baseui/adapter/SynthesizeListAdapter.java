package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.OnExamClickListener;
import com.sdzn.fzx.student.libbase.listener.BlankTextChangeImp;
import com.sdzn.fzx.student.libbase.listener.OnExamRefreshListener;
import com.sdzn.fzx.student.libpublic.views.exam.ClozeTestTextView;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankEditText;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.libutils.annotations.InputMethod;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 综合题 adapter
 *
 * @author Reisen at 2018-11-16
 */
public class SynthesizeListAdapter extends BaseAdapter {
    private List<AnswerListBean.AnswerDataBean> mList;
    private Context mContext;
    private OnExamClickListener mListener;
    private OnExamRefreshListener mRefreshListener;
    private AnswerListBean.AnswerDataBean mBean;

    SynthesizeListAdapter(AnswerListBean.AnswerDataBean bean, Context context, OnExamClickListener listener1, OnExamRefreshListener listener2) {
        mBean = bean;
        mList = bean.getExamList();
        mContext = context;
        mListener = listener1;
        mRefreshListener = listener2;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getExamTemplateId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = createItemView(getItemViewType(position));
        }
        if (convertView == null) {
            return null;
        }
        SynHolder tag = (SynHolder) convertView.getTag();
        tag.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.closeInput(null);
            }
        });

        if (tag instanceof SynSelectorHolder) {//选择判断
            bindSelectorData(position, convertView, (SynSelectorHolder) tag);
        } else if (tag instanceof SynShortAnswerHolder) {//简答
            bindShortAnswerData(position, convertView, (SynShortAnswerHolder) tag);
        } else if (tag instanceof SynFillBlankHolder) {//填空
            bindFillBlankData(position, convertView, (SynFillBlankHolder) tag);
        } else if (tag instanceof SynClozeTestHolder) {
            bindClozeTestData(position, convertView, (SynClozeTestHolder) tag);
        }
        return convertView;
    }

    /**
     * 创建view并绑定holder
     */
    private View createItemView(int type) {
        View view;
        SynHolder holder;
        switch (type) {
            case 1://单选
            case 2://多选
            case 3://判断
                view = LayoutInflater.from(mContext).inflate(R.layout.item_exam_syn_select, null);
                holder = new SynSelectorHolder(view);
                view.setTag(holder);
                return view;
            case 4://简答
                view = LayoutInflater.from(mContext).inflate(R.layout.item_exam_syn_short_answer, null);
                holder = new SynShortAnswerHolder(view);
                view.setTag(holder);
                return view;
            case 6://填空
                view = LayoutInflater.from(mContext).inflate(R.layout.item_exam_syn_fill_blank, null);
                holder = new SynFillBlankHolder(view);
                view.setTag(holder);
                return view;
            case 14://完型
                view = LayoutInflater.from(mContext).inflate(R.layout.item_exam_syn_cloze, null);
                holder = new SynClozeTestHolder(view);
                view.setTag(holder);
                return view;
            default://综合题不嵌套综合题
                return null;
        }
    }

    //选择
    private void bindSelectorData(int position, View convertView, SynSelectorHolder holder) {
        bindItemTitle(position, holder);
        AnswerListBean.AnswerDataBean bean = mList.get(position);
        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        if (examBean == null) {
            holder.tvExam.setHtmlText("");
            holder.rg.removeAllViews();
            return;
        }
        holder.tvExam.setHtmlText(examBean.getExamStem());
        //无选项情况
        List<AnswerListBean.ExamOptions> options = examBean.getExamOptions();
        if (options == null || options.isEmpty()) {
            holder.rg.removeAllViews();
            return;
        }
        //有选项
        Collections.sort(options);
        int size = options.size();
        int childCount = holder.rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            holder.rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(mContext).inflate(R.layout.item_child_select, holder.rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = holder.rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            final List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
            if (examList == null || examList.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examBean.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examList);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examList.get(0).getMyAnswer()));
            } else {//多选
                Collections.sort(examList);
                for (AnswerListBean.ExamOptionBean optionBean : examList) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getMyAnswer())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
            //选项设置点击事件
            child.setOnClickListener(new SynSelectorOnClick(bean) {
                @Override
                public void onClick(View view, AnswerListBean.AnswerDataBean bean) {
                    radioGroupClick(view, bean);
                    if (mRefreshListener != null) {
                        mRefreshListener.requestRefresh();
                    }
                }
            });
        }
    }

    //简答
    private void bindShortAnswerData(final int position, View convertView, final SynShortAnswerHolder holder) {
        bindItemTitle(position, holder);
        final AnswerListBean.AnswerDataBean bean = mList.get(position);
        String str = bean.getExamText();
        if (str == null || str.isEmpty()) {
            holder.tvExam.setVisibility(View.GONE);
            holder.llUnExam.setVisibility(View.GONE);
            holder.llPhoto.setVisibility(View.GONE);
        } else if (bean.getIsAnswer() == 0) {
            holder.tvExam.setVisibility(View.VISIBLE);
            holder.llUnExam.setVisibility(View.VISIBLE);
            holder.llPhoto.setVisibility(View.GONE);
            holder.ivShouxie.setVisibility(CommonUtils.flag == 0 ? View.GONE : View.VISIBLE);
        } else {
            holder.tvExam.setVisibility(View.VISIBLE);
            holder.llUnExam.setVisibility(View.GONE);
            holder.llPhoto.setVisibility(View.VISIBLE);
        }

        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        if (examBean == null) {
            holder.tvExam.setVisibility(View.GONE);
            holder.llUnExam.setVisibility(View.GONE);
            holder.llPhoto.setVisibility(View.GONE);
            return;
        }
        holder.tvExam.setVisibility(View.VISIBLE);
        HtmlTextView tv = holder.tvExam;
        tv.setHtmlText(examBean.getExamStem());
        RecyclerView rv = holder.rvAddPic;
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(manager);
        ShortAnswerAdapter adapter = (ShortAnswerAdapter) rv.getAdapter();
        if (adapter == null) {
            adapter = new ShortAnswerAdapter(mContext, true);
            rv.setAdapter(adapter);
        }
        adapter.setListener(new ShortAnswerAdapter.OnClickListener() {
            @Override
            public void clickAdd(AnswerListBean.AnswerDataBean dataBean, int picLen) {
                if (mListener != null) {
                    mListener.addPic(dataBean, picLen);
                }
            }

            @Override
            public void clickImage(int index, String url) {
                if (mListener != null) {
                    mListener.openImg(index, url, true, mList.get(position));
                }
            }

            @Override
            public void clickWriteImage(int index) {
                if (mListener != null) {
                    mListener.addWritePic(index, mList.get(position));
                }
            }
        });
        adapter.setDataBean(bean);
        List<AnswerListBean.ExamOptionBean> examList = bean.getExamOptionList();
        if (examList != null && !examList.isEmpty()) {
            if (examList.get(0).getSeq() == 1) {
                adapter.setSeq(1);
                holder.tvExamType.setText("当前为手写笔作答");
            } else {
                adapter.setSeq(2);
                holder.tvExamType.setText("当前为拍照作答");
            }
            for (AnswerListBean.ExamOptionBean optionBean : examList) {

                String str2 = optionBean.getMyAnswer();
                if (str2 == null) {
                    break;
                }
                String[] arr = str2.split(";");
                if (arr.length == 1) {
                    adapter.setList(arr[0].split(","), null);
                } else {
                    adapter.setList(arr[1].split(","), arr[0].split(","));
                }
            }
        } else {
            adapter.setList(new String[]{}, null);
        }

        holder.ivShouxie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.addWritePic(position, bean);
                }
            }
        });
        holder.ivXiangJi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llUnExam.setVisibility(View.GONE);
                holder.llPhoto.setVisibility(View.VISIBLE);
                if (mListener != null) {
                    mListener.addPic(bean, 0);
                }
                if (mRefreshListener != null) {
                    mRefreshListener.refreshList();
                }
            }
        });
        holder.ivDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.cleanImg(holder, position, bean);
                }
                if (mRefreshListener != null) {
                    mRefreshListener.refreshList();
                }
            }
        });
    }

    //填空
    @SuppressLint("WrongConstant")
    private void bindFillBlankData(final int position, View convertView, SynFillBlankHolder holder) {
        bindItemTitle(position, holder);
        final AnswerListBean.AnswerDataBean bean = mList.get(position);
        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        if (examBean == null) {
            holder.tvExam.setHtmlBody("");
            return;
        }
        holder.tvExam.setInputMethod(bean.getStatus() == 0 ? InputMethod.Picture : InputMethod.KeyBoard);
        holder.tvExam.setAnswerDataBean(bean);
        holder.tvExam.setBlankTextChangeListener(new BlankTextChangeImp(bean) {
            @Override
            public void onBlankTextChange(SparseArray<ClozeAnswerBean> blankTextList, @Nullable ClozeAnswerBean bean) {
                List<AnswerListBean.ExamOptionBean> list = mAnswerDataBean.getExamOptionList();
                list.clear();
                for (int i = 0; i < blankTextList.size(); i++) {
                    if (TextUtils.isEmpty(blankTextList.valueAt(i).getAnswer())) {
                        continue;
                    }
                    AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
                    optionBean.setSeq(blankTextList.keyAt(i));
                    optionBean.setMyAnswer(blankTextList.valueAt(i).getAnswer() == null ? "" : blankTextList.valueAt(i).getAnswer());
                    optionBean.setAnswerType(blankTextList.valueAt(i).getInputMethod());
                    list.add(optionBean);
                }
                if (mRefreshListener != null) {
                    mRefreshListener.refreshTop();
                }
                if (mListener != null) {
                    mListener.onBlankTextChange(bean);
                }
            }

            @Override
            public void onBlankClick(FillBlankEditText et, ClozeAnswerBean bean, AnswerListBean.AnswerDataBean answerDataBean) {
                if (mListener != null) {
                    // FIXME: 2019/2/18 这里传的position会不会跟外面list的position冲突?
                    mListener.onBlankClick(et, bean, position);
                }
            }

            @Override
            public void closeInput(FillBlankEditText et) {
                if (mListener != null) {
                    mListener.closeInput(et);
                }
            }
        });
        SparseArray<ClozeAnswerBean> arr = new SparseArray<>();
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list != null) {
//            for (AnswerListBean.ExamOptionBean optionBean : list) {
//                arr.append(optionBean.getSeq(), optionBean.getMyAnswer());
//            }
            for (int i = 0; i < list.size(); i++) {
                AnswerListBean.ExamOptionBean optionBean = list.get(i);
                ClozeAnswerBean clozeAnswerBean;
                switch (optionBean.getAnswerType()) {
                    case 1://键盘作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.KeyBoard);
                        break;
                    case 2://手写笔作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.PenWrite);
                        break;
                    case 3://拍照作答
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer(), false, InputMethod.Picture);
                        break;
                    case 0://未作答
                    default:
                        clozeAnswerBean = new ClozeAnswerBean(i, optionBean.getSeq() + "", optionBean.getMyAnswer() == null ? "" : optionBean.getMyAnswer(), false, InputMethod.KeyBoard);
                        break;

                }
                arr.append(optionBean.getSeq(), clozeAnswerBean);
            }
        }
        holder.tvExam.setHtmlBody(examBean.getExamStem(), arr);
    }

    private void bindClozeTestData(int position, View convertView, SynClozeTestHolder holder) {
        bindItemTitle(position,holder);
        final AnswerListBean.AnswerDataBean bean = mList.get(position);
        AnswerListBean.ExamTextBean examBean = bean.getExamBean();
        if (examBean == null) {
            holder.tvExam.setHtmlBody("");
            return;
        }
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        SparseArray<String> arr = new SparseArray<>();
        if (list != null) {
            for (AnswerListBean.ExamOptionBean optionBean : list) {
                arr.append(optionBean.getSeq(), optionBean.getMyAnswer());
            }
        }

        holder.tvExam.setHtmlBody(examBean.getExamStem(), arr);
        holder.tvExam.setClozeTestChangeListener(new ClozeTestTextView.ClozeTestChangeListener() {
            @Override
            public void clickCloze(ClozeTestTextView view, SparseArray<ClozeAnswerBean> map, int position) {
                if (mListener != null) {
                    mListener.showClozeTestTextWindow(view, bean, map, position);
                }
            }
        });
    }

    //统一设置标题
    private void bindItemTitle(int position, SynHolder holder) {
        holder.tvCount.setText("(" + mList.get(position).getExamSeq() + ")");
        int score = (int) mList.get(position).getScore();
        if (score <= 0) {
            holder.tvScore.setVisibility(View.GONE);
        }else {
            holder.tvScore.setVisibility(View.VISIBLE);
            if (mList.get(position).getExamTemplateId() == 6) {
                score *= mList.get(position).getExamBean().getExamOptions().size();
            }
            holder.tvScore.setText("(本题"+ score +"分)");
        }
        holder.tvType.setText(mList.get(position).getExamTemplateStyleName());
    }

    //选择项点击事件处理
    private void radioGroupClick(View view, AnswerListBean.AnswerDataBean bean) {
        String trim = ((TextView) ((ViewGroup) view).getChildAt(0)).getText().toString().trim();//点击的选项
        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null) {
            list = new ArrayList<>();
            bean.setExamOptionList(list);
        }
        int examType = bean.getExamTemplateId();
        //单选or判断
        if (examType == 1 || examType == 3) {
            if (list.isEmpty()) {
                AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
                optionBean.setMyAnswer(trim);
                optionBean.setSeq(trim.charAt(0) - 64);
                list.add(optionBean);
                bean.setIsAnswer(1);
            } else if (TextUtils.equals(list.get(0).getMyAnswer(), trim)) {
                list.remove(0);
                bean.setIsAnswer(0);
            } else {
                list.get(0).setMyAnswer(trim);
                list.get(0).setSeq(trim.charAt(0) - 64);
                bean.setIsAnswer(1);
            }
            return;
        }
        //多选
        Iterator<AnswerListBean.ExamOptionBean> iterator = list.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            AnswerListBean.ExamOptionBean next = iterator.next();
            if (TextUtils.equals(trim, next.getMyAnswer())) {
                iterator.remove();
                b = true;
                break;
            }
        }
        if (!b) {
            AnswerListBean.ExamOptionBean optionBean = new AnswerListBean.ExamOptionBean();
            optionBean.setMyAnswer(trim);
            list.add(optionBean);
        }
        bean.setIsAnswer(list.isEmpty() ? 0 : 1);
    }

    //单选多选判断
    private static class SynSelectorHolder extends SynHolder {
        HtmlTextView tvExam;//题干
        RadioGroup rg;//选项

        SynSelectorHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            rg = view.findViewById(R.id.rg_answer);
        }
    }

    //简答
    public static class SynShortAnswerHolder extends SynHolder {
        public HtmlTextView tvExam;//题干
        public RelativeLayout llUnExam;//未作答
        public ImageView ivShouxie;//手写
        public ImageView ivXiangJi;//拍照
        public RelativeLayout llPhoto;//已作答
        public TextView tvExamType;//当前作答方式
        public ImageView ivDel;//删除按钮
        public RecyclerView rvAddPic;//作答图片列表

        SynShortAnswerHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
            llUnExam = view.findViewById(R.id.ll_un_exam);
            ivShouxie = view.findViewById(R.id.iv_shouxie);
            ivXiangJi = view.findViewById(R.id.iv_xiangji);
            llPhoto = view.findViewById(R.id.ll_photo);
            tvExamType = view.findViewById(R.id.tv_exam_type);
            ivDel = view.findViewById(R.id.del);
            rvAddPic = view.findViewById(R.id.rv_add_pic);
        }
    }

    //填空
    private static class SynFillBlankHolder extends SynHolder {
        FillBlankEditText tvExam;

        SynFillBlankHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
        }
    }

    private static class SynClozeTestHolder extends SynHolder {
        ClozeTestTextView tvExam;

        SynClozeTestHolder(View view) {
            super(view);
            tvExam = view.findViewById(R.id.tv_exam);
        }
    }

    private static class SynHolder {
        TextView tvCount;
        TextView tvType;
        TextView tvScore;
        ViewGroup convertView;

        SynHolder(View view) {
            tvCount = view.findViewById(R.id.tv_count);
            tvType = view.findViewById(R.id.tv_type);
            tvScore = view.findViewById(R.id.tv_score);
            convertView = view.findViewById(R.id.convert_view);
        }
    }

    //综合选择题点击事件接口
    private static abstract class SynSelectorOnClick implements View.OnClickListener {
        private AnswerListBean.AnswerDataBean bean;

        SynSelectorOnClick(AnswerListBean.AnswerDataBean bean) {
            this.bean = bean;
        }

        @Override
        public void onClick(View v) {
            onClick(v, bean);
        }

        public abstract void onClick(View view, AnswerListBean.AnswerDataBean bean);
    }
}
