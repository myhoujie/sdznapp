package com.sdzn.fzx.student.libbase.baseui.view;

import android.graphics.RectF;

import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ServerTimeVo;
import com.sdzn.fzx.student.vo.UploadPicVo;

import java.util.List;

/**
 * 作答页面
 *
 * @author Reisen at 2018-08-27
 */
public interface AnswerNativeView extends BaseView {

    void getServerTimeSuccess(ServerTimeVo serverTimeVo);

    void networkError(String msg);

    void onUploadPicSuccess(UploadPicVo uploadVos);

    void onWhiteUploadPicSuccess(UploadPicVo uploadVos);

    /*================ h5改原生部分接口 ================*/
    void getAnswerListSuccess(List<AnswerListBean.AnswerDataBean> list);

    void submitAnswerOneMinFailure(String msg, boolean showProgress);

    void submitAnswerOneMinSuccess();

    void submitAnswerFailure(String msg);

    //===================== 填空手写笔 =====================
    void loadLocalDataSuccess(List<Dots> list);

    void saveDotsSuccess(RectF rectF);

    void saveDotsFailed(String msg);

    void clipSuccess(String photoPath);

    void clipFailed(String errorMsg);

//    void onFillBlankWhiteUploadSuccess(UploadPicVo.DataBean bean);
}
