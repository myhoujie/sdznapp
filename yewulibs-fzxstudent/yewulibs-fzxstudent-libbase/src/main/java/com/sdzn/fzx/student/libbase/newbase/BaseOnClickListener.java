package com.sdzn.fzx.student.libbase.newbase;

public interface BaseOnClickListener {
    //个人中心
    void Titlegrzx();

    //时间
    void Titleshijian();

    //展开时间
    void Titlezankaishijian();

    /*删除*/
    void TitleRemove();

    /*搜索*/
    void Titlesousuo();

    /*提交*/
    void Titletijiao();

    /*下拉加载*/
    void TitleDropdown();

    /*返回*/
    void TitleBack();

    /*添加*/
    void TitleAdd();

    /*更多*/
    void TitleMore();

    /*重新批改*/
    void TitleChongxinpg();

    /*发布批改*/
    void TitleFabupg();

    /*提交按钮*/
    void Tijiaoanniu();

    /*筛选按钮*/
    void Shaixuananniu();

    /*合作讨论 小组*/
    void Xiaozuanniu();

    /*选择学科*/
    void TitleXueKeDropdown();

    /*选择多场景*/
    void TitleMultDropdown();

}
