package com.sdzn.fzx.student.libbase.newbase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.internal.CustomAdapt;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class NewBaseFragment extends Fragment implements NetconListener2 {
    protected EmptyViewNew1 emptyview1;//网络监听
    private long mCurrentMs = System.currentTimeMillis();
    protected NetState netState;
    protected SmartRefreshLayout refreshLayout1;//刷新


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AutoSize.autoConvertDensity(getActivity(), 800, false);
        View rootView = inflater.inflate(getLayoutId(), container, false);
        LogUtils.e("BaseActFragment-----onCreateView");
        setup(rootView, savedInstanceState);
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, getActivity());

        donetwork();
        //由于某些原因, 屏幕旋转后 Fragment 的重建, 会导致框架对 Fragment 的自定义适配参数失去效果
        //所以如果您的 Fragment 允许屏幕旋转, 则请在 onCreateView 手动调用一次 AutoSize.autoConvertDensity()
        //如果您的 Fragment 不允许屏幕旋转, 则可以将下面调用 AutoSize.autoConvertDensity() 的代码删除掉
//        return createTextView(inflater, "Fragment-1\nView width = 360dp\nTotal width = 1080dp", 0xffff0000);
//        AutoSize.autoConvertDensity(getActivity(), 800, false);
        return rootView;
    }



    /*具体业务内容*/
    protected void donetwork() {
    }

    protected abstract int getLayoutId();


    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);//刷新控件
        emptyview1.loading();
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                //刷新内容
                RefreshLoad();
            }
        });

        /*绑定empty方法*/
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                RefreshLoad();
            }
        });
    }

    /*重新加载*/
    protected void RefreshLoad() {

    }

    @Override
    public void net_con_none() {
        ToastUtils.showLong("网络异常，请检查网络连接！");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
    }


    public String ids;

    public void call(Object value) {
        LogUtils.e("BaseActFragment-----call");
    }
    boolean mIsPrepare = false;		//视图还没准备好
    boolean mIsVisible= false;		//不可见
    boolean mIsFirstLoad = true;	//第一次加载

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mIsPrepare = true;
        lazyLoad();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            mIsVisible = true;
            lazyLoad();
        } else {
            mIsVisible = false;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //isVisibleToUser这个boolean值表示:该Fragment的UI 用户是否可见
        if (isVisibleToUser) {
            mIsVisible = true;
            lazyLoad();
        } else {
            mIsVisible = false;
        }
    }
    private void lazyLoad() {
        //这里进行三个条件的判断，如果有一个不满足，都将不进行加载
        if (!mIsPrepare || !mIsVisible||!mIsFirstLoad) {
            return;
        }
        loadData();
        //数据加载完毕,恢复标记,防止重复加载
        mIsFirstLoad = false;
    }

    public void loadData() {
        //这里进行网络请求和数据装载
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (netState != null) {
            netState.unregisterReceiver();
        }
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}