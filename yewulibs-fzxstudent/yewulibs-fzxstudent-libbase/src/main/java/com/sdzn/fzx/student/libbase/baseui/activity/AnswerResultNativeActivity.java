package com.sdzn.fzx.student.libbase.baseui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.baseui.adapter.AnswerResultListAdapter;
import com.sdzn.fzx.student.libbase.baseui.presenter.AnswerResultNativePresenter;
import com.sdzn.fzx.student.libbase.baseui.view.AnswerResultNativeView;
import com.sdzn.fzx.student.libbase.listener.OnExamResultClickListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.TimeUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.AnswerResultInfoBean;
import com.sdzn.fzx.student.vo.ResourceVo;
import com.sdzn.fzx.student.vo.StudentTaskVo;
import com.sdzn.fzx.student.vo.TaskListVo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Reisen at 2018-12-11
 */
public class AnswerResultNativeActivity extends MBaseActivity<AnswerResultNativePresenter> implements AnswerResultNativeView, OnExamResultClickListener, View.OnClickListener {
    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvTitle;
    private TextView tvStatistics;
    private ImageView ivCorrectType;
    private TextView tvCorrectType;
    private TextView tvScore;
    private TextView tvTotalScore;
    private TextView tvAveScore;
    private TextView tvTime;
    private CheckBox ivOnlyShowError;
    private CheckBox ivShowAnswer;
    private TextView tvShowAnswer;
    private View line;
    private RecyclerView mRecyclerView;
    private LinearLayout llEmpty;
    private TextView tvUnfEmpty;

    private TaskListVo.DataBean taskDataBean;
    private List<AnswerListBean.AnswerDataBean> mList = new ArrayList<>();
    private AnswerResultListAdapter mAdapter;

    private boolean showError;
    private boolean showAnswerButton;

    private int isRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_result_native);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvStatistics = (TextView) findViewById(R.id.tvStatistics);
        ivCorrectType = (ImageView) findViewById(R.id.iv_correctType);
        tvCorrectType = (TextView) findViewById(R.id.tv_correctType);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvTotalScore = (TextView) findViewById(R.id.tv_total_score);
        tvAveScore = (TextView) findViewById(R.id.tv_ave_score);
        tvTime = (TextView) findViewById(R.id.tv_time);
        ivOnlyShowError = (CheckBox) findViewById(R.id.iv_only_show_error);
        ivShowAnswer = (CheckBox) findViewById(R.id.iv_show_answer);
        tvShowAnswer = (TextView) findViewById(R.id.tv_show_answer);
        line = (View) findViewById(R.id.line);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        llEmpty = (LinearLayout) findViewById(R.id.ll_empty);
        tvUnfEmpty = (TextView) findViewById(R.id.tvUnfEmpty);

        tvBack.setOnClickListener(this);
        tvStatistics.setOnClickListener(this);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(AnswerResultNativeActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    public void initPresenter() {
        mPresenter = new AnswerResultNativePresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void initView() {
        tvTitle.setText(taskDataBean.getName());
        /*
         * 只看错题
         */
        ivOnlyShowError.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isRight = 2;
                } else {
                    isRight = 1;
                }
                getData();
            }
        });
        /*
         * 显示答案
         */
        ivShowAnswer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mAdapter.setShowAnswer(isChecked);
            }
        });
    }

    @Override
    protected void initData() {
        taskDataBean = getIntent().getParcelableExtra("taskDataBean");
        mAdapter = new AnswerResultListAdapter(this, mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(App2.get()));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setListener(this);
        getData();
    }

    @Override
    public void loadExamInfoSuccess(AnswerResultInfoBean.AnswerResultDataBean bean) {
        showAnswerButton = bean.getAnswerViewOpen() == 1;
        StudentTaskVo taskVo = bean.getStudentTaskVo();
        mList.clear();
        if (taskVo != null) {
            if (taskVo.getCorrectType() == 2) {
                ivCorrectType.setVisibility(View.VISIBLE);
                tvCorrectType.setVisibility(View.VISIBLE);
                ivCorrectType.setImageResource(R.mipmap.cucao_img);
                tvCorrectType.setText("粗糙卷面");
            } else if (taskVo.getCorrectType() == 1) {
                ivCorrectType.setVisibility(View.VISIBLE);
                tvCorrectType.setVisibility(View.VISIBLE);
                ivCorrectType.setImageResource(R.mipmap.youxiu_img);
                tvCorrectType.setText("优秀卷面");
            } else {
                ivCorrectType.setVisibility(View.GONE);
                tvCorrectType.setVisibility(View.GONE);
            }
            tvScore.setText(taskVo.getScoreTotal() < 0 ? "--" : taskVo.getScoreTotal() + "分");
            tvTotalScore.setText(taskVo.getScore() < 0 ? "--" : taskVo.getScore() + "分");
            tvAveScore.setText(taskVo.getScoreAvg() < 0 ? "--" : taskVo.getScoreAvg() + "分");
            tvTime.setText(TimeUtil.sec2HMS(taskVo.getUseTime()));

        }
        if (showAnswerButton) {
            ivShowAnswer.setVisibility(View.VISIBLE);
            tvShowAnswer.setVisibility(View.VISIBLE);
        } else {
            ivShowAnswer.setVisibility(View.GONE);
            tvShowAnswer.setVisibility(View.GONE);
        }
        if (bean.getExamAnswerList() != null) {
            mList.addAll(bean.getExamAnswerList());
        }
        mAdapter.changeState(false, showAnswerButton);
        mAdapter.setIsCorrect(taskVo != null && taskVo.getIsCorrect() == 1);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadExamInfoFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    /**
     * 获取数据
     */
    private void getData() {
        Map<String, String> params = new HashMap<>();
        params.put("lessonTaskId", String.valueOf(taskDataBean.getId()));
        params.put("lessonTaskStudentId", String.valueOf(taskDataBean.getLessonTaskStudentId()));
        if (isRight == 2) {
            params.put("isRight", isRight + "");
        }

        mPresenter.loadExamInfo(params);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tvBack) {
            finish();

        } else if (i == R.id.tvStatistics) {
            Intent statisticsIntent = new Intent(activity, StatisticsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("taskDataBean", taskDataBean);
            statisticsIntent.putExtras(bundle);
            activity.startActivity(statisticsIntent);
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void openImg(int index, String url, boolean showDel, AnswerListBean.AnswerDataBean bean) {
        Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
        intentImage.putExtra("photoUrl", url);
        activity.startActivity(intentImage);
    }

    @Override
    public void changeCollect(AnswerListBean.AnswerDataBean bean) {
        if (bean.getType() == 2) {//资源
            if (bean.isCollect()) {
                mPresenter.deleteResCollect(bean);
            } else {
                mPresenter.saveResCollect(bean, taskDataBean.getLessonId(), taskDataBean.getLessonName());
            }
        } else {
            if (bean.isCollect()) {
                mPresenter.deleteCollect(bean);
            } else {
                mPresenter.saveCollect(bean);
            }
        }
    }

    @Override
    public void showResource(AnswerListBean.AnswerDataBean bean) {
        if (bean == null || bean.getResourceVoBean() == null) {
            ToastUtil.showShortlToast("找不到资源");
            return;
        }
        final ResourceVo.ResourceTextVo resourceTextVo = bean.getResourceVoBean();
        if (resourceTextVo == null) {
            ToastUtil.showShortlToast("找不到资源");
            return;
        }
        switch (resourceTextVo.getResourceType()) {
            case 1:
            case 2:
                showSelectOpenDialog(new OpenListener() {
                    @Override
                    public void openByApp() {
                        Intent intentDoc = new Intent(activity, PDFActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                        intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        activity.startActivity(intentDoc);
                    }

                    @Override
                    public void openByOther() {
                        Intent intentDoc = new Intent(activity, DocViewActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        switch (resourceTextVo.getResourceType()) {
                            case 1:
                                if (".txt".equalsIgnoreCase(resourceTextVo.getResourceSuffix())) {
                                    intentDoc.putExtra("type", "txt");
                                } else {
                                    intentDoc.putExtra("type", "doc");
                                }
                                break;
                            case 2:
                                intentDoc.putExtra("type", "ppt");
                                break;
                        }
                        activity.startActivity(intentDoc);
                    }
                });
                break;
            case 3:
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentVideo = new Intent(activity, PlayerActivity.class);
                        intentVideo.putExtra("videoUrl", path);
                        intentVideo.putExtra("title", resourceTextVo.getResourceName());
                        activity.startActivity(intentVideo);
                    }
                });
//                Intent intentVideo = new Intent(activity, PlayerActivity.class);
//                intentVideo.putExtra("videoUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                intentVideo.putExtra("title", resourceTextVo.getResourceName());
//                activity.startActivity(intentVideo);
                break;
            case 4:
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
                        intentImage.putExtra("photoUrl", path);
                        activity.startActivity(intentImage);
                    }
                });
//                Intent intentImage = new Intent(activity, ImageDisplayActivity.class);
//                intentImage.putExtra("photoUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                activity.startActivity(intentImage);
                break;
            case 5:
                SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath(), this, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                    @Override
                    public void onFailed(Throwable e) {
                        ToastUtil.showShortlToast("获取文件地址失败");
                    }

                    @Override
                    public void onSuccess(String path) {
                        Intent intentRadio = new Intent(activity, PlayerActivity.class);
                        intentRadio.putExtra("radioUrl", path);
                        intentRadio.putExtra("title", resourceTextVo.getResourceName());
                        activity.startActivity(intentRadio);
                    }
                });
//                Intent intentRadio = new Intent(activity, PlayerActivity.class);
//                intentRadio.putExtra("radioUrl", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
//                intentRadio.putExtra("title", resourceTextVo.getResourceName());
//                activity.startActivity(intentRadio);
                break;
            case 6:
                Intent intentDoc = new Intent(activity, PDFActivity.class);
                intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                activity.startActivity(intentDoc);
                break;
        }
    }

    public Dialog showSelectOpenDialog(final AnswerResultNativeActivity.OpenListener listener) {
        final Dialog dialog = new Dialog(activity, R.style.Dialog);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_open_res, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    interface OpenListener {
        void openByApp();

        void openByOther();
    }

    @Override
    public void saveCollectSuccess() {
        ToastUtil.showShortlToast("收藏成功");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteCollectSuccess() {
        ToastUtil.showShortlToast("取消收藏成功");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void collectError(String msg) {
        ToastUtil.showShortlToast(msg);
    }
}
