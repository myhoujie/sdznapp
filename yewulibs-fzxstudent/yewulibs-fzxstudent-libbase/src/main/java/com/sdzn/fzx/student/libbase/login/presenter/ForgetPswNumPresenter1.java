package com.sdzn.fzx.student.libbase.login.presenter;

import com.blankj.utilcode.util.ToastUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.libbase.login.view.ForgetPswNumViews;
import com.sdzn.fzx.student.vo.VerifyUserMobile;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * ForgetPswNumPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ForgetPswNumPresenter1 extends Presenter<ForgetPswNumViews> {

    public void verityUserName(final String cardId) {
        com.alibaba.fastjson.JSONObject requestData = new com.alibaba.fastjson.JSONObject();
        requestData.put("account", cardId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .studentFindMobile(requestBody)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<VerifyUserMobile>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<VerifyUserMobile>> call, Response<ResponseSlbBean1<VerifyUserMobile>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            ToastUtils.showShort("验证失败");
                            return;
                        }
                        getView().verifySuccess(response.body().getResult().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<VerifyUserMobile>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        ToastUtils.showShort("验证失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }




}
