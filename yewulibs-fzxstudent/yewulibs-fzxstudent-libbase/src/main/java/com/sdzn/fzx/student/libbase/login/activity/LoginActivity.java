package com.sdzn.fzx.student.libbase.login.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.login.presenter.LoginPresenter1;
import com.sdzn.fzx.student.libbase.login.view.LoginViews;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class LoginActivity extends BaseActivity implements LoginViews, TextWatcher, View.OnClickListener {
    LoginPresenter1 mPresenter;
    private RelativeLayout netErrorRy;
    private Button userNumIcon;
    private EditText userNumEdit;
    private ImageView userNumEditDelImg;
    private Button pswIcon;
    private EditText pswEdit;
    private ImageView pswNumEditDelImg;
    private TextView forgetPsw;
    private Button loginBtn;
    private TextView tvVersion;
    private LinearLayout llPememberPas;
    private CheckBox check;
    private TextView tvYonghu;
    private TextView tvYinsi;

    public void initPresenter() {
        mPresenter = new LoginPresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.color_F5F6FF), 0);
        setContentView(R.layout.activity_login);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initPresenter();
        initView();
        initData();
//        mPresenter.login("54937162", "123456");
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入 LoginActivity");
                }
            }
        }
    }

    @Override
    protected void initView() {
        netErrorRy = findViewById(R.id.net_error_ry);
        userNumIcon = findViewById(R.id.user_num_icon);
        userNumEdit = findViewById(R.id.user_num_edit);
        userNumEditDelImg = findViewById(R.id.user_num_edit_del_img);
        pswIcon = findViewById(R.id.psw_icon);
        pswEdit = findViewById(R.id.psw_edit);
        pswNumEditDelImg = findViewById(R.id.psw_num_edit_del_img);
        forgetPsw = findViewById(R.id.forget_psw);
        loginBtn = findViewById(R.id.login_btn);
        tvVersion = findViewById(R.id.tv_version);
        check = (CheckBox) findViewById(R.id.check);
        llPememberPas = (LinearLayout) findViewById(R.id.ll_remember_pas);
        tvYonghu = (TextView) findViewById(R.id.tv_yonghu);
        tvYinsi = (TextView) findViewById(R.id.tv_yinsi);
        forgetPsw.setOnClickListener(this);
        loginBtn.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                final String userName = userNumEdit.getText().toString().trim();
                final String userPsw = pswEdit.getText().toString().trim();
                if (!mPresenter.vertifyNum(userName, userPsw)) {
                    if (CommonAppUtils.isLingChuangPad()) {
                        CommonAppUtils.logout();
                        App2.get().stopService(new Intent(App2.get(), MqttService.class));
                        ActivityManager.exit();
                    }
                    return;
                }
                mPresenter.login(userName, userPsw,LoginActivity.this);
            }
        });
        userNumEditDelImg.setOnClickListener(this);
        pswNumEditDelImg.setOnClickListener(this);
        userNumEdit.addTextChangedListener(this);
        pswEdit.addTextChangedListener(this);
        llPememberPas.setOnClickListener(this);
        tvYonghu.setOnClickListener(this);
        tvYinsi.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        String versionName = getVersionName();
        if (versionName == null) {
            tvVersion.setVisibility(View.GONE);
        } else {
            tvVersion.setVisibility(View.VISIBLE);
            tvVersion.setText("辅助线 " + versionName);
        }
        final String loginUserNum = StudentSPUtils.getLoginUserNum();
        if (!TextUtils.isEmpty(loginUserNum))
            userNumEdit.setText(loginUserNum);
//        if (SPUtils.isAutoLogin()) {
//            String name = SPUtils.getLoginUserNum();
//            String pwd = SPUtils.getLoginUserPwd();
//            mPresenter.login(name,pwd);
//        }
//        if ((boolean) StudentSPUtils.get(this, StudentSPUtils.LOGIN_CHECK_STATUS, false)) {
//            check.setChecked(true);
//        }
        pswEdit.setText(String.valueOf(StudentSPUtils.get(this, StudentSPUtils.LOGIN_USER_PWD, "")));
    }

    private String getVersionName() {
        try {
            PackageInfo packInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.forget_psw) {
            enterForgetPsw();
        } /*else if (i == R.id.login_btn) {

//            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1"));
//            finish();
        }*/ else if (i == R.id.user_num_edit_del_img) {
            userNumEdit.setText("");
        } else if (i == R.id.psw_num_edit_del_img) {
            pswEdit.setText("");
        } else if (i == R.id.ll_remember_pas) {
            if (check.isChecked()) {
                check.setChecked(false);
                StudentSPUtils.put(this, StudentSPUtils.LOGIN_CHECK_STATUS, false);
            } else {
                check.setChecked(true);
                StudentSPUtils.put(this, StudentSPUtils.LOGIN_CHECK_STATUS, true);
            }
        } else if (i == R.id.tv_yonghu) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuContract.html");
            startActivity(intent);
        } else if (i == R.id.tv_yinsi) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XieYiActivity");
            intent.putExtra("url_key", "http://teach.fuzhuxian.com/stuPrivacyContract.html");
            startActivity(intent);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        changeLoginStatus();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void changeLoginStatus() {
        final Editable userNumText = userNumEdit.getText();
        final Editable pswText = pswEdit.getText();
        boolean canLogin;
        /**
         * 思路：默认为 不可登录（false）状态，用户名有文字时，设为true，没有文字设置为false，
         * 接着判断密码，如果没文字，设置为false，如果有文字，则不改变之前的状态
         */
        if (TextUtils.isEmpty(userNumText)) {
            canLogin = false;
            userNumIcon.setEnabled(false);
//            ToastUtil.showCenterBottomToast("请输入学生号或身份证号");
        } else {
            canLogin = true;
            userNumIcon.setEnabled(true);
        }

        if (TextUtils.isEmpty(pswText)) {
            canLogin = false;
            pswIcon.setEnabled(false);
//            ToastUtil.showCenterBottomToast("请输入密码");
        } else {
            pswIcon.setEnabled(true);
        }

        loginBtn.setEnabled(canLogin);

    }

    @Override
    public void loginSuccess(LoginBean.DataBean vo) {
        setResult(SlbLoginUtil2.LOGIN_RESULT_OK);
        LoginBean loginBean = new LoginBean();
        loginBean.setData(vo);
        StudentSPUtils.saveLoginBean(loginBean);
        com.blankj.utilcode.util.SPUtils.getInstance().put("token", vo.getAccessToken());

        final Editable userNumText = userNumEdit.getText();
        final Editable pswText = pswEdit.getText();
        StudentSPUtils.put(this, StudentSPUtils.LOGIN_USER_NUM, userNumText);
        StudentSPUtils.put(this, StudentSPUtils.LOGIN_USER_PWD, pswText);

        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);
        if (ActivityUtils.getActivityList().size() == 1) {
//            ToastUtils.showLong("准备跳转中。。。。。。。");
//            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity"));
//            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1"));
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1"));
        }
//        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
//        startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.ShouyeActivity"));
        finish();
        startMqttService();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void startMqttService() {
        Intent intent = new Intent(this, MqttService.class);
        startService(intent);

    }

    public void enterForgetPsw() {
        Intent intent = new Intent(this, ForgetPswActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginFailed(String msg) {
        ToastUtils.showShort(msg);
    }
}
