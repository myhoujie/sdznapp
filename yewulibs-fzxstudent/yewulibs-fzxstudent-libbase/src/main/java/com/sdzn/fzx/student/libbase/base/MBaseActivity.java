package com.sdzn.fzx.student.libbase.base;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;


import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;

import me.jessyan.autosize.AutoSize;

/**
 * 描述：所有Activity的基类，任何Activity必须继承它
 * -
 * 创建人：zhangchao
 * 创建时间：17/3/20
 */
public abstract class MBaseActivity<P extends BasePresenter> extends BaseActivity {

    public P mPresenter;

    public static final String REQUEST_CODE = "request_code";


    public abstract void initPresenter();

    /**
     *影响性能，切换的时候出现每次加载都要检测一次竖屏，然后在切换横屏，导致的卡顿
     * @param savedInstanceState
     */
//    @Override
//    public Resources getResources() {
//        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
//        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
//        AutoSizeCompat.autoConvertDensity((super.getResources()), 1280, true);//如果有自定义需求就用这个方法,,,,,宽度设置false,,高度设置true
//        return super.getResources();
//    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
    }

    public void startActivity(String action) {
        startActivity(new Intent(action));
    }

    public void startActivityForResult(Class<? extends Activity> activity, int requestCode) {
        startActivityForResult(new Intent(this, activity), requestCode);
    }

    public void startActivityForResult(String action, int requestCode) {
        startActivityForResult(new Intent(action), requestCode);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (intent.resolveActivity(getPackageManager()) == null) {
            Log.e("Activity", "No Activity found to handle intent " + intent);
            return;
        }

        if (requestCode != -1 && intent.getIntExtra(REQUEST_CODE, -1) == -1) {
            intent.putExtra(REQUEST_CODE, requestCode);
        }

        super.startActivityForResult(intent, requestCode);
    }


    protected void onActResult(int requestCode, int resultCode, Intent data) {

    }
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        //监听横竖屏变化导致
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Log.d("width", "横屏");
//            AutoSize.autoConvertDensityOfGlobal(this); //如果没有自定义需求用这个方法
//        } else {
//           Log.d("width", "竖屏");
//        }
//    }

    @Override
    protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (UserUtils.get().onActivityResult(requestCode, resultCode, data)) {
//            if (UserUtils.get().isUserLogin()) {
//                onUserLogined(UserUtils.get().userId());
//            }
//            return;
//        }
//        if (LoginUtil.get().login_activity_result(requestCode, resultCode, data)) {
////            if (LoginUtil.get().isUserLogin()) {
////                onUserLogined(LoginUtil.get().userId());
////            } else {
////                onUserLoginCanceled();
////            }
//            return;
//        }
        //登录和未登录成功状态
        if (SlbLoginUtil2.get().login_activity_result(requestCode, resultCode, data)) {
//            if (LoginUtil.get().isUserLogin()) {
//                onUserLogined(LoginUtil.get().userId());
//            } else {
//                onUserLoginCanceled();
//            }
            if (isIs_finish_login()) {
                finish();
                setIs_finish_login(false);
            } else {
            }
            return;
        }
        //正常状态
        onActResult(requestCode, resultCode, data);
    }
    public boolean is_finish_login;

    public boolean isIs_finish_login() {
        return is_finish_login;
    }

    public void setIs_finish_login(boolean is_finish_login) {
        this.is_finish_login = is_finish_login;
    }
    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }
}