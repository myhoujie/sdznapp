package com.sdzn.fzx.student.libbase.newbase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.baselibrary.emptyview.EmptyView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.just.agentweb.base.BaseCurrencyAgentWebFragment;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.api.ScrollBoundaryDecider;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseActFragment1 extends BaseCurrencyAgentWebFragment implements NetconListener2 {
    protected EmptyViewNew1 emptyview1;//网络监听
    private long mCurrentMs = System.currentTimeMillis();
    protected NetState netState;
    protected SmartRefreshLayout refreshLayout1;//刷新

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        LogUtils.e("BaseActFragment-----onCreateView");
        setup(rootView, savedInstanceState);
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        getActivity().getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                RefreshLoad();
            }
        }, 1000);
        return rootView;
    }

    protected abstract int getLayoutId();


    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        LogUtils.e("BaseActFragment-----setup");
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
        refreshLayout1 = rootView.findViewById(R.id.refreshLayout1_order);//刷新控件
        emptyview1.loading();
        //使上拉加载具有弹性效果
        refreshLayout1.setEnableAutoLoadMore(false);
        //禁止越界拖动（1.0.4以上版本）
        refreshLayout1.setEnableOverScrollDrag(false);
        //关闭越界回弹功能
        refreshLayout1.setEnableOverScrollBounce(false);
        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
            @Override
            public boolean canRefresh(View content) {
                //webview滚动到顶部才可以下拉刷新
                MyLogUtil.e("ssssss", "" + mAgentWeb.getWebCreator().getWebView().getScrollY());
//                return mAgentWeb.getWebCreator().getWebView().getScrollY() <= 0;
                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
            }

            @Override
            public boolean canLoadMore(View content) {
                return false;
            }
        });
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                //刷新内容
                RefreshLoad();
            }
        });

        /*绑定empty方法*/
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                RefreshLoad();
            }
        });
    }


    /*重新加载*/
    protected void RefreshLoad() {

    }

    @Override
    public void net_con_none() {
        ToastUtils.showLong("网络异常，请检查网络连接！");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
    }


    public String ids;

    public void call(Object value) {
        LogUtils.e("BaseActFragment-----call");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (netState != null) {
            netState.unregisterReceiver();
        }
        super.onDestroy();
    }

    /**
     * 刷新webView
     */
    public void AgentwebRefresh(String url) {
//        mAgentWeb.getUrlLoader().reload(); // 刷新
        loadWebSite(url); // 刷新
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}