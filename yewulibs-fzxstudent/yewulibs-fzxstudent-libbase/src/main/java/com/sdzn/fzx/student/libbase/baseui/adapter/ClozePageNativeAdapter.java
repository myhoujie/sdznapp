package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libpublic.views.radioview.RadioLayout;
import com.sdzn.fzx.student.libpublic.views.radioview.RadioView;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import java.util.List;

/**
 * @author Reisen at 2018-11-19
 */
public class ClozePageNativeAdapter extends PagerAdapter {
    private Context mContext;
    private AnswerListBean.AnswerDataBean bean;
    private SparseArray<ClozeAnswerBean> map;
    private ClozeClickCallBack mCallBack;

    public ClozePageNativeAdapter(Context context, AnswerListBean.AnswerDataBean bean,
                                  SparseArray<ClozeAnswerBean> map) {
        mContext = context;
        this.bean = bean;
        this.map = map;
    }

    @Override
    public int getCount() {
        if (bean == null || bean.getExamBean() == null || bean.getExamBean().getExamOptions() == null) {
            return 0;
        }
        return bean.getExamBean().getExamOptions().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        AnswerListBean.ExamOptions bean = this.bean.getExamBean().getExamOptions().get(position);
        List<AnswerListBean.ExamOptions> itemList = bean.getList();//指定页的选项集
        ClozeAnswerBean answerBean = map.valueAt(position);//该题作答结果

        RadioLayout layout;
        if (itemList.size() == 3) {
            layout = (RadioLayout) LayoutInflater.from(mContext).inflate(R.layout.page_cloze_3, null);
        } else if (itemList.size() == 4) {
            layout = (RadioLayout) LayoutInflater.from(mContext).inflate(R.layout.page_cloze_4, null);
        } else {
            return LayoutInflater.from(mContext).inflate(R.layout.page_cloze_none, null);
        }

        String s0 = itemList.get(0).getContent();
        String s1 = itemList.get(1).getContent();
        String s2 = itemList.get(2).getContent();
        String s3;
        if (itemList.size() == 4) {
            s3 = itemList.get(3).getContent();
        } else {
            s3 = null;
        }

        RadioView rd_A = layout.findViewById(R.id.rd_a);
        TextView tvA = rd_A.findViewById(R.id.tv_a);
        TextView tvAbody = rd_A.findViewById(R.id.tv_a_body);
        tvAbody.setText(s0);

        RadioView rd_B = layout.findViewById(R.id.rd_b);
        TextView tvB = rd_B.findViewById(R.id.tv_b);
        TextView tvBbody = rd_B.findViewById(R.id.tv_b_body);
        tvBbody.setText(s1);

        RadioView rd_C = layout.findViewById(R.id.rd_c);
        TextView tvC = rd_C.findViewById(R.id.tv_c);
        TextView tvCbody = rd_C.findViewById(R.id.tv_c_body);
        tvCbody.setText(s2);

        RadioView rd_D = null;
        TextView tvD = null;
        if (itemList.size() == 4) {
            rd_D = layout.findViewById(R.id.rd_d);
            tvD = rd_D.findViewById(R.id.tv_d);
            TextView tvDbody = rd_D.findViewById(R.id.tv_d_body);
            tvDbody.setText(s3);
        }

        if ("".equals(answerBean.getAnswer())) {//和else相同
            tvA.setText("A.");
            tvB.setText("B.");
            tvC.setText("C.");
            if (tvD != null) {
                tvD.setText("D.");
            }
        } else if (TextUtils.equals(answerBean.getAnswer(), s0)) {
            rd_A.setChecked(true);
            tvA.setText("A");
            tvB.setText("B.");
            tvC.setText("C.");
            if (tvD != null) {
                tvD.setText("D.");
            }
        } else if (TextUtils.equals(answerBean.getAnswer(), s1)) {
            rd_B.setChecked(true);
            tvA.setText("A.");
            tvB.setText("B");
            tvC.setText("C.");
            if (tvD != null) {
                tvD.setText("D.");
            }
        } else if (TextUtils.equals(answerBean.getAnswer(), s2)) {
            rd_C.setChecked(true);
            tvA.setText("A.");
            tvB.setText("B.");
            tvC.setText("C");
            if (tvD != null) {
                tvD.setText("D.");
            }
        } else if (TextUtils.equals(answerBean.getAnswer(), s3)) {
            if (rd_D != null) {
                rd_D.setChecked(true);
            }
            tvA.setText("A.");
            tvB.setText("B.");
            tvC.setText("C.");
            if (tvD != null) {
                tvD.setText("D");
            }
        } else {
            tvA.setText("A.");
            tvB.setText("B.");
            tvC.setText("C.");
            if (tvD != null) {
                tvD.setText("D.");
            }
        }

        layout.setOnCheckedChangeListener(new BaseCheckedChangeListener(
                new RadioHolder(itemList, map, mCallBack,
                        rd_A, rd_B, rd_C, rd_D, tvA, tvB, tvC, tvD,position)));
        container.addView(layout);
        return layout;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;//强制刷新页面
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private static class BaseCheckedChangeListener implements RadioLayout.OnCheckedChangeListener {
        private RadioHolder mHolder;

        BaseCheckedChangeListener(RadioHolder holder) {
            mHolder = holder;
        }

        @Override
        public void checkedChange(int resId) {
            if (mHolder.rd_D == null) {//三个选项
                if (resId == View.NO_ID) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer("");
                } else if (resId == mHolder.rd_A.getId()) {
                    mHolder.tv_A.setText("A");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(0).getContent());
                } else if (resId == mHolder.rd_B.getId()) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B");
                    mHolder.tv_C.setText("C.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(1).getContent());
                } else if (resId == mHolder.rd_C.getId()) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(2).getContent());
                }
            } else {//四个选项
                if (resId == View.NO_ID) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C.");
                    mHolder.tv_D.setText("D.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer("");
                } else if (resId == mHolder.rd_A.getId()) {
                    mHolder.tv_A.setText("A");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C.");
                    mHolder.tv_D.setText("D.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(0).getContent());
                } else if (resId == mHolder.rd_B.getId()) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B");
                    mHolder.tv_C.setText("C.");
                    mHolder.tv_D.setText("D.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(1).getContent());
                } else if (resId == mHolder.rd_C.getId()) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C");
                    mHolder.tv_D.setText("D.");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(2).getContent());
                } else if (resId == mHolder.rd_D.getId()) {
                    mHolder.tv_A.setText("A.");
                    mHolder.tv_B.setText("B.");
                    mHolder.tv_C.setText("C.");
                    mHolder.tv_D.setText("D");
                    ClozeAnswerBean bean = mHolder.map.valueAt(mHolder.position);
                    bean.setAnswer(mHolder.list.get(3).getContent());
                }
            }

            if (mHolder.mCallBack != null) {
                mHolder.mCallBack.click(mHolder.map,mHolder.position);
            }
        }
    }

    public interface ClozeClickCallBack {
        void click(SparseArray<ClozeAnswerBean> map, int position);
    }

    public void setClickCallBack(ClozeClickCallBack clickCallBack) {
        mCallBack = clickCallBack;
    }

    //holder, 用来记录view
    private static class RadioHolder {
        List<AnswerListBean.ExamOptions> list;
        SparseArray<ClozeAnswerBean> map;//当前题目
        ClozeClickCallBack mCallBack;
        RadioView rd_A;
        RadioView rd_B;
        RadioView rd_C;
        RadioView rd_D;
        TextView tv_A;
        TextView tv_B;
        TextView tv_C;
        TextView tv_D;
        int position;

        RadioHolder(List<AnswerListBean.ExamOptions> list, SparseArray<ClozeAnswerBean> currentSeq, ClozeClickCallBack mCallBack,
                    RadioView rd_A, RadioView rd_B, RadioView rd_C, RadioView rd_D,
                    TextView tv_A, TextView tv_B, TextView tv_C, TextView tv_D, int position) {
            this.list = list;
            this.map = currentSeq;
            this.mCallBack = mCallBack;
            this.rd_A = rd_A;
            this.rd_B = rd_B;
            this.rd_C = rd_C;
            this.rd_D = rd_D;
            this.tv_A = tv_A;
            this.tv_B = tv_B;
            this.tv_C = tv_C;
            this.tv_D = tv_D;
            this.position = position;
        }
    }
}
