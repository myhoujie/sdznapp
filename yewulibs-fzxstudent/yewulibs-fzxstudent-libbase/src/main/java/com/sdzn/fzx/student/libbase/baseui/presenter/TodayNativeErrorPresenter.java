package com.sdzn.fzx.student.libbase.baseui.presenter;

import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.baseui.activity.TodayNativeErrorActivity;
import com.sdzn.fzx.student.libbase.baseui.view.TodayNativeErrorView;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.ExamText;
import com.sdzn.fzx.student.vo.SubjectErrorBean;
import com.sdzn.fzx.student.vo.TodayExamVo;
import com.sdzn.fzx.student.vo.TodayNativeVo;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TodayNativeErrorPresenter extends BasePresenter<TodayNativeErrorView, TodayNativeErrorActivity> {

    /**
     * 加载试题
     */
    public void getToadySubjectList(Map<String, String> params) {
        Network.createTokenService(NetWorkService.TodayErrorList.class)
                .TodayErrorList(params)
                .map(new StatusFunc<TodayNativeVo>())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ProgressSubscriber<TodayNativeVo>(new SubscriberListener<TodayNativeVo>() {
                    @Override
                    public void onNext(TodayNativeVo todayNativeVo) {
                        initData(todayNativeVo);
                        mView.setTodayListSuccess(todayNativeVo);

                    }

                    private void initData(TodayNativeVo beans) {
                        List<TodayNativeVo.DataBean> list = beans.getData();
                        if (list == null) {
                            return;
                        }
                        for (TodayNativeVo.DataBean bean : list) {

                            for (TodayNativeVo.DataBean.ExamListBean examListBean : bean.getExamList()) {
                                if (examListBean.getType() == 3) {//纯文本
                                    continue;
                                }
                                if (examListBean.getType() == 2) {//资源
                                    String resourceText = examListBean.getExamText();
                                    TodayExamVo todayExamVo = GsonUtil.fromJson(resourceText, TodayExamVo.class);
                                    if (todayExamVo == null) {
                                        throw new RuntimeException();
                                    }
                                    examListBean.setTodayExamVo(todayExamVo);
                                    continue;
                                }
                                String resourceText = examListBean.getExamText();
                                TodayExamVo todayExamVo = GsonUtil.fromJson(resourceText, TodayExamVo.class);
                                if (todayExamVo == null) {
                                    throw new RuntimeException();
                                }
                                examListBean.setTodayExamVo(todayExamVo);

                           /* //试题
                            String examText = bean.getExamText();
                            AnswerListBean.ExamTextBean examTextBean = gson.fromJson(examText, AnswerListBean.ExamTextBean.class);
                            bean.setExamBean(examTextBean);
                            //综合题额外进行一轮解析
                            if (bean.getExamTemplateId() != 16) {
                                continue;
                            }
                            for (AnswerListBean.AnswerDataBean dataBean : bean.getExamList()) {
                                AnswerListBean.ExamTextBean json = gson.fromJson(dataBean.getExamText(), AnswerListBean.ExamTextBean.class);
                                dataBean.setExamBean(json);
                            }*/

                                //综合题额外进行一轮解析
                                if (examListBean.getExamTemplateId() != 16) {
                                    continue;
                                }

                                for (SubjectErrorBean.DataBean dataBean : examListBean.getExamList()) {
                                    ExamText json = GsonUtil.fromJson(dataBean.getExamText(), ExamText.class);
                                    dataBean.setExamTextVo(json);
                                }

                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                        String msg = "请求失败";
                        if (e instanceof ApiException) {
                            msg = ((ApiException) e).getMsg();
                        }
                        ToastUtil.showShortlToast(msg);
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, mActivity));
    }
}
