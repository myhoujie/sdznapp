package com.sdzn.fzx.student.libbase.baseui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;

/**
 * @author Reisen 2019.03.28 接入office365, 弃用pdf
 */
public class PDFActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvPdfTitle;
    private WebView mWebView;
    private TextView tvClose;

    private String path;
    private String name;
    private long size;

    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        tvPdfTitle = findViewById(R.id.tv_video_title);
        mWebView = findViewById(R.id.webview);
        tvClose = findViewById(R.id.tv_close);
        tvClose.setOnClickListener(this);

        initData();
        initView();
        startTime = System.currentTimeMillis();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(PDFActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }


    @Override
    protected void initView() {
        tvPdfTitle.setText(name);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        String url = "https://ow365.cn/?i=" + BuildConfig2.OFFICE_ID + "&n=5&furl=" + path;
        mWebView.loadUrl(url);
    }

    @Override
    protected void initData() {
        name = getIntent().getStringExtra("name");
        path = getIntent().getStringExtra("path");
        size = getIntent().getLongExtra("size", 0);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_close) {
            back();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return false;
            }
            back();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back() {
        Intent intent = new Intent();
        intent.putExtra("time", (System.currentTimeMillis() - startTime) / 1000);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.translucent_activity_in, R.anim.translucent_activity_out);
    }
}