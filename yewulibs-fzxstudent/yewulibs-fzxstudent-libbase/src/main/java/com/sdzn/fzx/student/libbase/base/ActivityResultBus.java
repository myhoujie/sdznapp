package com.sdzn.fzx.student.libbase.base;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public  class ActivityResultBus extends Bus {

    private static ActivityResultBus instance;

    public static ActivityResultBus getInstance() {
        if (instance == null)
            instance = new ActivityResultBus();
        return instance;
    }

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public void postQueue(final Object obj) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ActivityResultBus.getInstance().post(obj);
            }
        });
    }

}
