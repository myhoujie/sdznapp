package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.libpublic.event.DeleteEvent;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libpublic.views.exam.ExamListView;
import com.sdzn.fzx.student.libpublic.views.exam.FillHtmlTextView;
import com.sdzn.fzx.student.libpublic.views.exam.HtmlTextView;
import com.sdzn.fzx.student.vo.ExamText;
import com.sdzn.fzx.student.vo.SubjectErrorBean;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/13
 * 修改单号：
 * 修改内容:
 * on 2018/12/13
 */

public class ErrorSubjectNativeAdapter extends BaseRcvAdapter<SubjectErrorBean.DataBean> {

    private Activity mActivity = null;

    public ErrorSubjectNativeAdapter(Context context, List<SubjectErrorBean.DataBean> mList, Activity activity) {
        super(context, mList);
        mActivity = activity;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }
    @Override
    public int getItemViewType(int position) {
        SubjectErrorBean.DataBean bean = mList.get(position);
        return bean.getType() * 10000 + bean.getExamTemplateId();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 10001://单选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10002://多选
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10003://判断
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_select);
            case 10004://简答
                return BaseViewHolder.get(context, null, parent, R.layout.item_jianda_exam);
            case 10006://填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_text);
            case 10014://完形填空
                return BaseViewHolder.get(context, null, parent, R.layout.item_fill_exam_);
            case 10016://综合
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);
            default:
                return BaseViewHolder.get(context, null, parent, R.layout.item_error_exam_short_answer);

        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, SubjectErrorBean.DataBean bean) {
        if (bean.getType() == 2) {//资源
            //bindResource(holder, position, bean);
            return;
        }
        if (bean.getType() == 3) {//文字
            // bindText(holder, position, bean);
            return;
        }
        switch (bean.getExamTemplateId()) {//试题
            case 1:
                bindSelector(holder, position, bean);//单选
                break;
            case 2:
                bindSelector(holder, position, bean);//多选
                break;
            case 3:
                bindSelector(holder, position, bean);//判断
                break;
            case 4:
                bindShortAnswer(holder, position, bean);//简答
                break;
            case 6:
                bindFillBlank(holder, position, bean);//填空
                break;
            case 14:
                bindFillExam(holder, position, bean);//完型填空
                break;
            case 16:
                bindAllAnswer(holder, position, bean);//综合
                break;
            default:
                // bindOtherType(holder, bean);//其他类型
                break;
        }
    }

    /*  *//**
     * 选择判断多选
     */
  /* private void bindSelector(BaseViewHolder holder, int position, SubjectErrorBean.DataBean bean) {
        bindExamTitle(holder, position, bean);
        HtmlTextView tv = holder.getView(R.id.tv);
        HtmlTextView tvPars = holder.getView(R.id.tv_pars);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tvPars.setHtmlText(bean.getExamTextVo().getExamAnalysis());

        ExamListView listView = holder.getView(R.id.rg_answer);
        ExamSelectAdapter examSelectAdapter = new ExamSelectAdapter(bean.getExamTextVo().getExamOptions(), context);
        listView.setAdapter(examSelectAdapter);
    }*/

    /**
     * 选择判断多选
     */
    private void bindSelector(BaseViewHolder holder, int position, final SubjectErrorBean.DataBean bean) {
        bindExamTitle(holder, position, bean);
        ExamText examTextVo = bean.getExamTextVo();
        final HtmlTextView tv = holder.getView(R.id.tv);
        HtmlTextView tvPars = holder.getView(R.id.tv_pars);
        tv.setHtmlText(examTextVo.getExamStem());
        tvPars.setHtmlText(bean.getExamTextVo().getExamAnalysis());
        RadioGroup rg = holder.getView(R.id.rg_answer);
        List<ExamText.ExamOptionsBean> options = examTextVo.getExamOptions();
        if (options == null || options.isEmpty()) {
            rg.removeAllViews();
            return;
        }
        Collections.sort(options);
        int size = options.size();
        int childCount = rg.getChildCount();
        if (childCount > size) {//控件多, 选项少
            rg.removeViews(size, childCount - size);
        } else if (childCount < size) {//选项多, 控件少
            while (childCount < size) {
                LayoutInflater.from(context).inflate(R.layout.item_child_select, rg, true);
                childCount++;
            }
        }
        for (int i = 0; i < size; i++) {
            View child = rg.getChildAt(i);
            TextView tvNumber = child.findViewById(R.id.tv_number);
            HtmlTextView tvText = child.findViewById(R.id.tv_text);
            tvNumber.setText(String.valueOf((char) (65 + i)));
            tvText.setHtmlText(options.get(i).getContent());
            // final List<AnswerListBean.ExamOptionBean> examList = bean.getEx;
            List<ExamText.ExamOptionsBean> examOptions = bean.getExamTextVo().getExamOptions();
            if (examOptions == null || examOptions.isEmpty()) {
                tvNumber.setSelected(false);
            } else if (examTextVo.getExamTypeId() == 1) {//单选/判断
                Collections.sort(examOptions);
                tvNumber.setSelected(TextUtils.equals(tvNumber.getText().toString().trim(), examOptions.get(0).getContent()));
            } else {//多选
                Collections.sort(examOptions);
                for (ExamText.ExamOptionsBean optionBean : examOptions) {
                    if (TextUtils.equals(tvNumber.getText().toString().trim(), optionBean.getContent())) {
                        tvNumber.setSelected(true);
                        break;
                    } else {
                        tvNumber.setSelected(false);
                    }
                }
            }
        }

        final ImageView zoom = holder.getView(R.id.zoom);
        final LinearLayout parsLy = holder.getView(R.id.pars_ly);
        if (tv.getHtmlText().length() > 500){
            zoom.setVisibility(View.VISIBLE);
        }else {
            zoom.setVisibility(View.GONE);
        }
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tv.getHtmlText().length() > 500) {

                    zoom.setImageResource(R.mipmap.zhankia_icon);
                    parsLy.setVisibility(View.GONE);
                    tv.setHtmlText(bean.getExamTextVo().getExamStem().substring(0, 500));
                } else {
                    tv.setMaxEms(tv.getHtmlText().length());
                    tv.setHtmlText(bean.getExamTextVo().getExamStem());
                    zoom.setImageResource(R.mipmap.shouqi_icon);
                    parsLy.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * 填空
     */
    private void bindFillBlank(final BaseViewHolder holder, final int position, final SubjectErrorBean.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final FillHtmlTextView tv = holder.getView(R.id.tv);
        HtmlTextView tvPars = holder.getView(R.id.tv_pars);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tvPars.setHtmlText(bean.getExamTextVo().getExamAnalysis());
        final ImageView zoom = holder.getView(R.id.zoom);
        final LinearLayout parsLy = holder.getView(R.id.pars_ly);
        if (tv.getHtmlText().length() > 500){
            zoom.setVisibility(View.VISIBLE);
        }else {
            zoom.setVisibility(View.GONE);
        }
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tv.getHtmlText().length() > 500) {

                    zoom.setImageResource(R.mipmap.zhankia_icon);
                    parsLy.setVisibility(View.GONE);
                    tv.setHtmlText(bean.getExamTextVo().getExamStem().substring(0, 500));
                } else {
                    tv.setMaxEms(tv.getHtmlText().length());
                    tv.setHtmlText(bean.getExamTextVo().getExamStem());
                    zoom.setImageResource(R.mipmap.shouqi_icon);
                    parsLy.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setExamDifficulty(TextView textdif, int examDifficulty) {
        switch (examDifficulty) {
            case 1:
                textdif.setText("简单  ");
                break;
            case 2:
                textdif.setText("中等  ");
                break;
            case 3:
                textdif.setText("较难  ");
                break;
            case 4:
                textdif.setText("难  ");
                break;
        }

    }

    private void bindExamTitle(BaseViewHolder holder, int position, SubjectErrorBean.DataBean bean) {
        final String id = String.valueOf(bean.getId());
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textScore = holder.getView(R.id.tv_score);
        TextView textdif = holder.getView(R.id.tv_dif);
        TextView textType = holder.getView(R.id.tv_type);
        TextView textTime = holder.getView(R.id.tv_time);
        textCount.setText(position + 1 + ". " + bean.getExamTemplateStyleName() + "");
        textScore.setText(bean.getCount() + "次");
        setExamDifficulty(textdif, bean.getExamDifficulty());
        textType.setText(bean.getExamTextVo().getChapterNodeNamePath());
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf2.format(bean.getTimeUpdate());
        textTime.setText(format + "  " + dateToWeek(format));
        HtmlTextView textAnswer = holder.getView(R.id.tvAnswer);
        textAnswer.setHtmlText(bean.getExamTextVo().getExamAnswer());
        CheckBox checkBox = holder.getView(R.id.checkbox);
        ImageView delImg = holder.getView(R.id.del_icon);
        final LinearLayout parsLy = holder.getView(R.id.pars_ly);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (parsLy.getVisibility() == View.GONE) {
                    parsLy.setVisibility(View.VISIBLE);
                } else {
                    parsLy.setVisibility(View.GONE);
                }
            }
        });
        delImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delExam(id);
            }
        });


    }

    /**
     * 删除试题
     */
    private void delExam(final String id) {
        showDialog("是否删除该题", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Network.createTokenService(NetWorkService.DelExam.class)
                        .DelExam(id)
                        .map(new StatusFunc<Object>())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                            @Override
                            public void onNext(Object o) {
                                mList.clear();
                                EventBus.getDefault().post(new DeleteEvent());
                            }

                            @Override
                            public void onError(Throwable e) {


                            }

                            @Override
                            public void onCompleted() {

                            }
                        }, mActivity));
            }
        });
    }

    /**
     * 简答题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindShortAnswer(final BaseViewHolder holder, final int position, final SubjectErrorBean.DataBean bean) {
        bindExamTitle(holder, position, bean);
        final HtmlTextView tv = holder.getView(R.id.tv_exam);
        HtmlTextView tvPars = holder.getView(R.id.tv_pars);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tvPars.setHtmlText(bean.getExamTextVo().getExamAnalysis());
        final ImageView zoom = holder.getView(R.id.zoom);
        final CheckBox checkBox = holder.getView(R.id.checkbox);
        final LinearLayout parsLy = holder.getView(R.id.pars_ly);
        if (tv.getHtmlText().length() > 500){
            zoom.setVisibility(View.VISIBLE);
        }else {
            zoom.setVisibility(View.GONE);
        }
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tv.getHtmlText().length() > 500) {
                    checkBox.setChecked(false);
                    zoom.setImageResource(R.mipmap.zhankia_icon);
                    parsLy.setVisibility(View.GONE);
                    tv.setHtmlText(bean.getExamTextVo().getExamStem().substring(0, 500));
                } else {
                    checkBox.setChecked(true);
                    tv.setMaxEms(tv.getHtmlText().length());
                    tv.setHtmlText(bean.getExamTextVo().getExamStem());
                    zoom.setImageResource(R.mipmap.shouqi_icon);
                    parsLy.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void showDialog(String msg, final DialogInterface.OnClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.onClick(dialog, which);
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 完型填空
     */
    private void bindFillExam(final BaseViewHolder holder, final int position, final SubjectErrorBean.DataBean bean) {
        bindExamTitle(holder, position, bean);
       final FillHtmlTextView tv = holder.getView(R.id.tv);
        HtmlTextView tvPars = holder.getView(R.id.tv_pars);
        tv.setHtmlText(bean.getExamTextVo().getExamStem());
        tvPars.setHtmlText(bean.getExamTextVo().getExamAnalysis());
        final ImageView zoom = holder.getView(R.id.zoom);
        final LinearLayout parsLy = holder.getView(R.id.pars_ly);
        final CheckBox checkBox = holder.getView(R.id.checkbox);
        if (tv.getHtmlText().length() > 500){
            zoom.setVisibility(View.VISIBLE);
        }else {
            zoom.setVisibility(View.GONE);
        }
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tv.getHtmlText().length() > 500) {
                    checkBox.setChecked(false);
                    zoom.setImageResource(R.mipmap.zhankia_icon);
                    parsLy.setVisibility(View.GONE);
                    tv.setHtmlText(bean.getExamTextVo().getExamStem().substring(0, 500));
                } else {
                    checkBox.setChecked(true);
                    tv.setMaxEms(tv.getHtmlText().length());
                    tv.setHtmlText(bean.getExamTextVo().getExamStem());
                    zoom.setImageResource(R.mipmap.shouqi_icon);
                    parsLy.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * 综合题
     *
     * @param holder
     * @param position
     * @param bean
     */

    private void bindAllAnswer(final BaseViewHolder holder, final int position, final SubjectErrorBean.DataBean bean) {
        final List<SubjectErrorBean.DataBean> list = new ArrayList();
        final String id = String.valueOf(bean.getId());
        ImageView delImg = holder.getView(R.id.del_icon);
        TextView textCount = holder.getView(R.id.tv_count);
        TextView textScore = holder.getView(R.id.tv_score);
        TextView textdif = holder.getView(R.id.tv_dif);
        TextView textType = holder.getView(R.id.tv_type);
        TextView textTime = holder.getView(R.id.tv_time);
        textCount.setText(position + 1 + ". " + bean.getExamTemplateStyleName() + "");
        textScore.setText(bean.getCount() + "次");
        setExamDifficulty(textdif, bean.getExamDifficulty());
        textType.setText(bean.getExamTextVo().getChapterNodeNamePath());
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf2.format(bean.getTimeUpdate());
        textTime.setText(format);
        ExamListView examListView = holder.getView(R.id.lv);
        list.addAll(bean.getExamList());
        final AllAnserAdapter allAnserAdapter = new AllAnserAdapter(list, context);
        examListView.setAdapter(allAnserAdapter);
        final CheckBox checkBox = holder.getView(R.id.checkbox);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                allAnserAdapter.setGoneParse(isChecked);
                list.clear();
                list.addAll(bean.getExamList());
                allAnserAdapter.notifyDataSetChanged();
            }
        });
        delImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delExam(id);
            }
        });
    }

    /**
     * 日期转星期
     *
     * @param datetime
     * @return
     */
    public static String dateToWeek(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance(); // 获得一个日历
        Date datet = null;
        try {
            datet = f.parse(datetime);
            cal.setTime(datet);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
        if (w < 0)
            w = 0;
        return weekDays[w];
    }


}
