package com.sdzn.fzx.student.libbase.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libpublic.event.LingChuangLogoutEvent;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;

/**
 * 监听此广播, 处理领创广播事件
 */
public class LingChuangEventReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null) {
            Log.i(intent.toString());
            return;
        }
        switch (action) {
            case "com.android.launcher3.mdm.LOGOUT":
                EventBus.getDefault().post(new LingChuangLogoutEvent());
                App2.get().stopService(new Intent(App2.get(), MqttService.class));
                ActivityManager.exit();
                break;
            case "com.sdzn.fzx.student.LOGINBEAN":
                LoginBean bean = GsonUtil.fromJson(intent.getStringExtra("loginbean"), LoginBean.class);
                StudentSPUtils.saveLoginBean(bean);
                break;
            case "com.sdzn.fzx.student.AUTOLOGIN":
                boolean autoLogin = intent.getBooleanExtra("autoLogin",false);
                String name = intent.getStringExtra("name");
                String pwd = intent.getStringExtra("pwd");
                StudentSPUtils.setAutoLogin(autoLogin);
                StudentSPUtils.saveLoginUserNum(name);
                StudentSPUtils.saveLoginUserPwd(pwd);
                break;
        }
    }
}
