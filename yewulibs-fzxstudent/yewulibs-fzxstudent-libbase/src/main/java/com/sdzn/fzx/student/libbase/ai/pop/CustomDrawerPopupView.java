package com.sdzn.fzx.student.libbase.ai.pop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lxj.xpopup.core.DrawerPopupView;
import com.sdzn.fzx.student.bean.GrzxRecActBean;
import com.sdzn.fzx.student.bean.GrzxRecActBean1;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.ai.pop.adapter.GRZXBaseRecActAdapter;
import com.sdzn.fzx.student.libbase.ai.pop.bean.GRZXBaseRecActBean;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.presenter.CehuaPresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.view.CehuaViews;
import com.sdzn.fzx.student.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 自定义抽屉弹窗
 * <p>
 * //通过设置topMargin，可以让Drawer弹窗进行局部阴影展示
 * //        ViewGroup.MarginLayoutParams params = (MarginLayoutParams) getPopupContentView().getLayoutParams();
 * //        params.topMargin = 450;
 */
public class CustomDrawerPopupView extends DrawerPopupView implements CehuaViews {
    private RecyclerView mRecyclerView;
    private List<GRZXBaseRecActBean> mList;
    private GRZXBaseRecActAdapter mAdapter;
    GrzxNextCallBack grzxNextCallBack;
    private ImageView ivHead;
    private TextView tvName;


    private long mCurrentMs = System.currentTimeMillis();
    CehuaPresenter cehuaPresenter;

    public CustomDrawerPopupView(@NonNull Context context) {
        super(context);
    }

    public CustomDrawerPopupView(@NonNull Context context, GrzxNextCallBack mGrzxNextCallBack) {
        super(context);
        this.grzxNextCallBack = mGrzxNextCallBack;
    }


    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_right_drawer;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        cehuaPresenter = new CehuaPresenter();
        cehuaPresenter.onCreate(this);
        cehuaPresenter.queryCehua();
        mList = new ArrayList<>();
        findview();

    }


    private void findview() {
        mRecyclerView = findViewById(R.id.rv_list);
        ivHead = findViewById(R.id.iv_head);
        tvName = findViewById(R.id.tv_name);
        LoginBean.DataBean data = UserController.getLoginBean().getData();
        if (data == null) {
            return;
        } else {
            if (!TextUtils.isEmpty(data.getUser().getRealName())) {
                tvName.setText(data.getUser().getRealName());
            }
            if (!TextUtils.isEmpty(data.getUser().getPhoto())) {
                Glide.with(App2.get())
                        .load(data.getUser().getPhoto())
                        .error(R.mipmap.mrtx_img)
                        .transform(new CircleTransform(App2.get())).into(ivHead);
            }
        }
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));

    }

    private void onclicklistener() {
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                GRZXBaseRecActBean addressBean = mList.get(position);
                int i = view.getId();
                if (i == R.id.iv_item) {
                    if (!addressBean.getGrzxRecActBean2().getAct().isEmpty()) {
                        if (addressBean.getGrzxRecActBean2().getName().equals("智囊学堂")) {
                            if (AppUtils.isAppInstalled("com.sdzn.pkt.student.hd")) {
                                String name = StudentSPUtils.getLoginUserNum();
                                String pwd = StudentSPUtils.getLoginUserPwd();
                                String state = "1";
                                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd + "&state=" + state));
                                getContext().startActivity(intent);
                            } else {
                                ToastUtils.showLong("请先安装智囊学堂学生端APP");
                            }
                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(addressBean.getGrzxRecActBean2().getAct()));
                            getContext().startActivity(intent);

                        }
                    }
                    dismiss();
                }
            }
        });

        findViewById(R.id.btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.rl_data).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
                getContext().startActivity(intent);
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
//                intent.putExtra("show", 0);
//                getContext().startActivity(intent);
            }
        });
    }

    private void donetwork() {
        mAdapter = new GRZXBaseRecActAdapter(mList);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SLIDEIN_LEFT);
        mAdapter.setNotDoAnimationCount(3);
        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int type = mList.get(position).type;
                if (type == GRZXBaseRecActBean.style1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        onclicklistener();
    }

    @Override
    public void onCehuaSuccess(GrzxRecActBean grzxRecActBean) {
        for (int i = 0; i < grzxRecActBean.getList().size(); i++) {
            GrzxRecActBean1 grzxRecActBean1 = grzxRecActBean.getList().get(i);
            for (int j = 0; j < grzxRecActBean1.getList().size(); j++) {
                if (j == 0) {
                    GrzxRecActBean1 classCommonbean = new GrzxRecActBean1();
                    classCommonbean.setTitle(grzxRecActBean1.getTitle());
                    classCommonbean.setList(grzxRecActBean1.getList());
                    mList.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style1, classCommonbean));
                }
                mList.add(new GRZXBaseRecActBean(GRZXBaseRecActBean.style2, grzxRecActBean1.getList().get(j)));
            }
        }
        donetwork();
    }

    @Override
    public void onCehuaNodata(String msg) {

    }

    @Override
    public void onCehuaFail(String msg) {

    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

    public interface GrzxNextCallBack {
        void toGrzxNextClick();
    }


    @Override
    protected void onShow() {
        super.onShow();

    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        Log.e("tag", "CustomDrawerPopupView onDismiss");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cehuaPresenter.onDestory();
    }
}