package com.sdzn.fzx.student.libbase.login.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.login.fragment.ForgetPswNumFragment;
import com.sdzn.fzx.student.libbase.login.presenter.ForgetPswPresenter;
import com.sdzn.fzx.student.libbase.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ForgetPswActivity extends MBaseActivity<ForgetPswPresenter> implements BaseView {

    private RelativeLayout netErrorRy;


    @Override
    public void initPresenter() {
        mPresenter = new ForgetPswPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_psw);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent!=null){
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction!=null){
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData!=null){
                    ToastUtils.showLong("ForgetPswActivity");
                }
            }
        }
    }

    @Override
    protected void initView() {
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        initFragment();
    }

    @Override
    protected void initData() {

    }

    private void initFragment() {
        ForgetPswNumFragment fragment = new ForgetPswNumFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.forget_psw_fragment, fragment);
        transaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
