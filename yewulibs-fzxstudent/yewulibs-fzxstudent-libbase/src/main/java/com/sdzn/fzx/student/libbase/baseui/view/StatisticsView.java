package com.sdzn.fzx.student.libbase.baseui.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.StatisticsListVo;
import com.sdzn.fzx.student.vo.StatisticsScoreVo;
import com.sdzn.fzx.student.vo.StatisticsTimeVo;

/**
 * 作答页面
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public interface StatisticsView extends BaseView {
    void getStatisticsScoreSuccess(StatisticsScoreVo statisticsScoreVo);

    void getStatisticsTimeSuccess(StatisticsTimeVo statisticsTimeVo);

    void getStatisticsListSuccess(StatisticsListVo statisticsTimeVo);

    void networkError(String msg);

}
