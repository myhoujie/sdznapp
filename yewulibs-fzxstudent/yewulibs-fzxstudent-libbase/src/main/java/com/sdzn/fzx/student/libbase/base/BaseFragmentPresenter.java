package com.sdzn.fzx.student.libbase.base;


import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * Created by bin_li on 2017/4/18.
 */

public class BaseFragmentPresenter<V extends BaseView, A extends BaseFragment> {
    public V mView;
    public A mActivity;

//    public Application appContext;
    protected List<Subscription> subscriptions = new ArrayList<>();

    public void attachView(V v, A a) {
        this.mView = v;
        this.mActivity = a;
//        this.appContext = (Application) a.getContext();
    }

    public void detachView() {
        this.mView = null;
        this.mActivity = null;
//        this.appContext = null;
        for (Subscription subscription : subscriptions) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();//取消訂閱
            }
        }
    }

}
