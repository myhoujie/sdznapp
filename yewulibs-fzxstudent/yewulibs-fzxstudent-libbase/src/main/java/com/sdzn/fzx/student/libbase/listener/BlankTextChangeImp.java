package com.sdzn.fzx.student.libbase.listener;

import com.sdzn.fzx.student.libpublic.views.exam.FillBlankEditText;
import com.sdzn.fzx.student.vo.AnswerListBean;

/**
 * 填空内容修改回调接口
 * @author Reisen at 2018-11-22
 */
public abstract class BlankTextChangeImp implements FillBlankEditText.BlankTextChangeListener {
    public AnswerListBean.AnswerDataBean mAnswerDataBean;

    public BlankTextChangeImp(AnswerListBean.AnswerDataBean answerDataBean) {
        mAnswerDataBean = answerDataBean;
    }
}
