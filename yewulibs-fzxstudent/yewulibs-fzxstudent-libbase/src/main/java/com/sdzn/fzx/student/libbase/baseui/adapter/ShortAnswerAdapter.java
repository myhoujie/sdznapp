package com.sdzn.fzx.student.libbase.baseui.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.sdzn.fzx.student.libbase.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.vo.AnswerListBean;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 简答题adapter
 *
 * @author Reisen at 2018-08-29
 */
public class ShortAnswerAdapter extends BaseRcvAdapter<String> {
    private OnClickListener mListener;
    private boolean showAdd;
    private ArrayList<String> rowList;
    private int seq;

    public ShortAnswerAdapter(Context context, boolean showAdd) {
        super(context, R.layout.item_add_pic, new ArrayList<String>());
        this.showAdd = showAdd;

    }

    public void isShowAdd(boolean showAdd) {
        this.showAdd = showAdd;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public void setList(String[] arr, String[] arr2) {
        mList.clear();
        mList.addAll(Arrays.asList(arr));
        if (arr2 != null) {
            rowList = new ArrayList<>(Arrays.asList(arr2));
        } else {
            rowList = null;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (showAdd && seq != 1 && mList.size() < 4) {
            return mList.size() + 1;
        }
        return mList.size();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (showAdd && mList.size() < 4) {
            if (position == 0) {
                convert(holder, position, null);
            } else {
                convert(holder, position, mList.get(position - 1));
            }
        } else {
            convert(holder, position, mList.get(position));
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, String s) {
        final int currentPosition;
        View holderView = holder.getView(R.id.add_pic);
        RelativeLayout rlShowImage = holder.getView(R.id.rl_show_image);
        ImageView ivAdd = holder.getView(R.id.iv_add);
        ImageView ivImage = holder.getView(R.id.iv_image);


        if (showAdd && seq != 1 && mList.size() < 4) {
            currentPosition = position - 1;
            if (position == 0) {
                holderView.setBackground(context.getResources().getDrawable(R.drawable.item_add_pic));
                ivAdd.setVisibility(View.VISIBLE);
                ivImage.setVisibility(View.GONE);
            } else {
                holderView.setBackground(context.getResources().getDrawable(R.drawable.item_add_pic_2));
                ivAdd.setVisibility(View.GONE);
                ivImage.setVisibility(View.VISIBLE);


                Glide.with(context).load(mList.get(currentPosition)+"?imageView2/5/w/200/h/200/q/50").asBitmap().into(ivImage);
            }
        } else {
            currentPosition = position;
            holderView.setBackground(context.getResources().getDrawable(R.drawable.item_add_pic_2));
            ivAdd.setVisibility(View.GONE);
            ivImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(mList.get(currentPosition)).asBitmap().into(ivImage);
        }
        holderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!showAdd || mList.size() >= 4) {
                    return;
                }
                if (mListener != null) {
                    mListener.clickAdd(mDataBean, mList.size());
                }
            }
        });
        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    if (seq == 1) {
                        mListener.clickWriteImage(currentPosition);
                    } else {
                        mListener.clickImage(currentPosition, rowList == null ? mList.get(currentPosition) : rowList.get(currentPosition));
                    }
                }
            }
        });
    }

    public void setListener(OnClickListener listener) {
        mListener = listener;
    }

    private AnswerListBean.AnswerDataBean mDataBean;

    public void setDataBean(AnswerListBean.AnswerDataBean bean) {
        mDataBean = bean;
    }

    public AnswerListBean.AnswerDataBean getDataBean() {
        return mDataBean;
    }

    public interface OnClickListener {
        void clickAdd(AnswerListBean.AnswerDataBean dataBean, int picLen);

        /**
         * 图片索引位置
         */
        void clickImage(int index, String url);

        void clickWriteImage(int index);
    }
}
