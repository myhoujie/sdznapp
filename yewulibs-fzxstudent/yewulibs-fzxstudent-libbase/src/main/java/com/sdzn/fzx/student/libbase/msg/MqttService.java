package com.sdzn.fzx.student.libbase.msg;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.baseui.LockScreenActivity;
import com.sdzn.fzx.student.libbase.baseui.OtherLoginTipActivity;
import com.sdzn.fzx.student.libbase.baseui.TiWenActivity;
import com.sdzn.fzx.student.libbase.baseui.ToolsPopActivity;
import com.sdzn.fzx.student.libpublic.event.CloseActivityEvent;
import com.sdzn.fzx.student.libpublic.event.CloseLockScreenEvent;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.StudentStatus;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MqttService extends Service implements MqttCallback {


    private String queueName = String.valueOf(UserController.getLoginBean().getData().getUser().getId());
    ConnectionFactory factory = new ConnectionFactory();
    private boolean isFirstLogin = false;

    public MqttService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");

    }

    Thread threadConfig;
    Thread threadConnect;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    private Thread threadConsume;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setupConnectionFactory();

        new Thread(new Runnable() {
            @Override
            public void run() {
                connectMqtt();
            }
        }).start();

        //rabbitmq
        threadConsume = new Thread(new Runnable() {
            @Override
            public void run() {
                basicConsume();
            }
        });
        threadConsume.start();


        getStudentStatus();
        return START_STICKY;
    }

    private void connectMqtt() {
        MqttManager.getInstance(this).createConnect(Network.BASE_MQTT_URL);

        final String deviceID = AndroidUtil.getDeviceID(App2.get());
        final String accountId = UserController.getAccountId();
        final String other_login_topic = "S-" + accountId + "-" + deviceID;
        MqttManager.getInstance(this).subscribe(other_login_topic, 1);
    }


    @Override
    public void connectionLost(Throwable cause) {
        MqttManager.getInstance(this).reconnect();
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        final String deviceID = AndroidUtil.getDeviceID(App2.get());
        final String accountId = UserController.getAccountId();
        final String other_login_topic = "S-" + accountId + "-" + deviceID;

        if (!message.isRetained()) {
            if (topic.equals(other_login_topic)) {

//                onOtherLogin(new String(message.toString().getBytes(), "UTF-8"));
                onOtherLogin(message.toString());
            }
        } else {
            Log.e("收到老消息", "。。。。。");
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    private void onOtherLogin(String msg) {
//        StudentSPUtils.savetoken("");
        Intent intent = new Intent(this, OtherLoginTipActivity.class);
        intent.putExtra(OtherLoginTipActivity.EXTRA_OTHER_LOGIN_MSG, msg);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        try {
            MqttManager.getInstance(this).disConnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
//        threadConsume.interrupt();
        super.onDestroy();

    }

    /**
     * Rabbit配置
     */
    private void setupConnectionFactory() {
        factory.setUsername(BuildConfig2.RABBIT_NAME);
        factory.setPassword(BuildConfig2.RABBIT_PASSWORD);
        factory.setHost(BuildConfig2.RABBIT_MQ);
        factory.setPort(BuildConfig2.RABBIT_HOST);
        factory.setAutomaticRecoveryEnabled(true);// 设置连接恢复

    }

    /**
     * 收消息
     */
    Channel channel;

    private void basicConsume() {
        try {
            //连接
            Connection connection = factory.newConnection();
            //通道
            channel = connection.createChannel();
            //2.创建队列的参数
            Map<String, Object> queueArgs = new HashMap<String, Object>();
            //queueArgs.put("x-dead-letter-exchange", "refreshDispatcherDeadExchange");  //死信队列
            //queueArgs.put("x-message-ttl", 10000);     // 消息超时：让发布的message在队列中可以存活多长时间，以毫秒为单位。
            queueArgs.put("x-expires", 5000);          // 队列超时：当前的queue在指定的时间内，没有消费者订阅就会被删除，以毫秒为单位。
            //queueArgs.put("x-max-length", 100);        // 队列最大长度：当超过了这个大小的时候，会删除之前最早插入的消息为本次的留出空间。
            //queueArgs.put("x-queue-mode", "lazy");     //延迟加载：queue的信息尽可能的都保存在磁盘上，仅在有消费者订阅的时候才会加载到RAM中。

            //3.声明队列。-将队列参数传到队列 （队列名字，是否持久化，是否排外，是否自动清理，参数）
            channel.queueDeclare(queueName, false, false, true, queueArgs);
            //实现Consumer的最简单方法是将便捷类DefaultConsumer子类化。可以在basicConsume 调用上传递此子类的对象以设置订阅：
            //实现Consumer的最简单方法是将便捷类DefaultConsumer子类化。可以在basicConsume 调用上传递此子类的对象以设置订阅：
            channel.basicConsume(queueName, false, "administrator", new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    super.handleDelivery(consumerTag, envelope, properties, body);

                    String rountingKey = envelope.getRoutingKey();
                    String contentType = properties.getContentType();
                    String msg = new String(body);
                    long deliveryTag = envelope.getDeliveryTag();

                    android.util.Log.e("TAG", rountingKey + "：rountingKey");
                    android.util.Log.e("TAG", contentType + "：contentType");
                    android.util.Log.e("TAG", msg + "：msg");
                    android.util.Log.e("TAG", deliveryTag + "：deliveryTag");
                    android.util.Log.e("TAG", "isRede" + "" + envelope.isRedeliver());
                    if (true == CommonUtils.isReceiveRbMq && queueName.equals(String.valueOf(UserController.getLoginBean().getData().getUser().getId()))) {
                        getIntentMsf(msg, channel, deliveryTag);
                    }

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }


    }


    Handler handler;

    private void getIntentMsf(String msg, Channel channel, long deliveryTag) {
        String mName;
        int mId;
        int type; // 1 抢答      2  随机
        int studentId = 0;
        /*
        1 上课 2 下课 3 锁屏 4 解锁
         */
        if (!msg.isEmpty()) {
            JSONObject object = null;
            try {
                object = new JSONObject(msg);
                mName = object.getString("name");
                mId = object.getInt("id");

                switch (mId) {
                    case 1:
                        EventBus.getDefault().post(new CloseActivityEvent());
                        Intent intentClass = new Intent(App2.get(), ToolsPopActivity.class);
                        intentClass.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentClass);
                        break;
                    case 2:
                        EventBus.getDefault().post(new CloseActivityEvent());
                        //判断是否锁屏状态
                        EventBus.getDefault().post(new CloseLockScreenEvent());

                        Log.e("老师已下课", "老师已下课");
                        handler = new Handler(Looper.getMainLooper());

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtil.showLonglToast("老师已下课");
                            }
                        });
                        break;
                    case 3:
                        EventBus.getDefault().post(new CloseActivityEvent());
                        Intent intentLock = new Intent(App2.get(), LockScreenActivity.class);
                        intentLock.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intentLock);
                        break;
                    case 4:
                        EventBus.getDefault().post(new CloseActivityEvent());
                        EventBus.getDefault().post(new CloseLockScreenEvent());
                        break;
                    default:
                        /*
                       答题问题
                         */
                        type = object.getInt("type");
                        if (type == 2) {
                            studentId = object.getInt("studentId");
                        }
                        if (type == 2 && UserController.getLoginBean().getData().getUser().getId() != studentId) {
                            //随机 未选中
                        } else if (type == 1 || type == 2) {
                            EventBus.getDefault().post(new CloseActivityEvent());
                            Intent intentTiWen = new Intent(App2.get(), TiWenActivity.class);
                            intentTiWen.putExtra("type", type);
                            intentTiWen.putExtra("Id", object.getString("name"));
                            if (type == 1) {
                                intentTiWen.putExtra("teacherId", object.getInt("teacherId"));
                            }
                            intentTiWen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intentTiWen);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } /*finally {
                try {
                    channel.basicAck(deliveryTag, false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/

        }

    }

    private void getStudentStatus() {
        Network.createTokenService(NetWorkService.ToolsService.class)
                .getStudentStatus(String.valueOf(UserController.getLoginBean().getData().getUser().getId()), UserController.getClassId())
                .map(new StatusFunc<StudentStatus>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<StudentStatus>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StudentStatus studentStatus) {
                        if (studentStatus.getData().getClassStatus() != null) {
                            if (studentStatus.getData().getClassStatus().size() > 0 && studentStatus.getData().getClassStatus().get(0).getStatus() == 1) {
                                //上课
                                if (studentStatus.getData().getInclass() != null
                                        && studentStatus.getData().getInclass().getStatus() == 1
                                        && studentStatus.getData().getClassScreen() != null
                                        && studentStatus.getData().getClassScreen().size() > 0
                                        && studentStatus.getData().getClassScreen().get(0).getStatus() == 1) {
                                    //锁屏
                                    Intent intentLock = new Intent(App2.get(), LockScreenActivity.class);
                                    intentLock.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intentLock);
                                } else if (studentStatus.getData().getInclass() != null && studentStatus.getData().getInclass().getStatus() == 1) {
                                    //只是上课
                                    Intent intentClass = new Intent(App2.get(), ToolsPopActivity.class);
                                    intentClass.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intentClass);
                                }

                            } else {
                            }

                        }
                    }
                });
    }


}
