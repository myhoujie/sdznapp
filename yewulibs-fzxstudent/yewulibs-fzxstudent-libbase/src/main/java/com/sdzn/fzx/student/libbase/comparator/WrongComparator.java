package com.sdzn.fzx.student.libbase.comparator;

import com.sdzn.fzx.student.vo.WrongVo;

import java.util.Comparator;

/**
 * 错题数量排序器
 *
 * @author wangchunxiao
 * @date 2018/1/12
 */
public class WrongComparator implements Comparator<WrongVo.DataBean> {
    @Override
    public int compare(WrongVo.DataBean w1, WrongVo.DataBean w2) {
        int count1 = w1.getCount();
        int count2 = w2.getCount();
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            return 0;
        }
    }
}
