package com.sdzn.fzx.student.libbase.app;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.interceptor.TokenInterceptor;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.libutils.util.SaveLogUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.BoxInfoBean;
import com.sdzn.fzx.student.vo.FileUrlBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.QiNiuConfigBean;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * SchoolBoxWatcher〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class SchoolBoxWatcher {
    private static final int PERIOD_TIME = 2 * 60 * 1000;

    public static String FILE_IP = "";

    public static SchoolBoxWatcher mInstance;
    private long lastTime;

//    public static String qiniuDomain;
//    public static boolean isReceiveRbMq = false;

    private SchoolBoxWatcher() {
    }

    public static SchoolBoxWatcher getInstance() {
        if (mInstance == null) {
            mInstance = new SchoolBoxWatcher();
        }
        return mInstance;
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (System.currentTimeMillis() - lastTime > PERIOD_TIME) {
                lastTime = System.currentTimeMillis();

                checkNetwork();
            }

            handler.sendEmptyMessageDelayed(1, PERIOD_TIME / 2);

        }
    };

    public void startWatch() {

        if (handler != null)
            handler.sendEmptyMessage(0);
    }

    public void stopWatch() {
        if (handler != null)
            handler.removeMessages(1);
    }

    public void getQiNiuDomain() {
        if (CommonUtils.qiniuDomain == null) {
            Network.createTokenService(NetWorkService.LoginService.class)
                    .getQiNiuConfig()
                    .map(new StatusFunc<QiNiuConfigBean>())
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.immediate())
                    .subscribe(new Subscriber<QiNiuConfigBean>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getQiNiuDomain();
                                }
                            }, 10000);
                        }

                        @Override
                        public void onNext(QiNiuConfigBean qiNiuConfigBean) {
                            if (qiNiuConfigBean != null && qiNiuConfigBean.getData() != null) {
                                CommonUtils.qiniuDomain = qiNiuConfigBean.data.domian;
                            }
                        }
                    });
        }
    }


    public void getBoxUrl() {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean == null)
            return;
        final String customerSchoolId = loginBean.getData().getUser().getCustomerSchoolId() + "";

        Network.createService(NetWorkService.LoginService.class)
                .getBoxUrl(customerSchoolId)
                .map(new StatusFunc<BoxInfoBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.immediate())
                .subscribe(new Subscriber<BoxInfoBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BoxInfoBean o) {
                        try {
                            CommonUtils.BOX_URL = "http://" + o.getData().getIp();
                            SaveLogUtils.saveInfo("box_info.txt", "获取到盒子的判定文件文件地址：" + CommonUtils.BOX_URL);
                            checkNetwork();
                        } catch (Exception e) {
                            Log.e("盒子地址获取失败");
                        }
                    }
                });
    }

    private static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    private static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(tokenInterceptor)
            .readTimeout(3000, TimeUnit.SECONDS)
            .connectTimeout(300, TimeUnit.SECONDS).build();

    private void checkNetwork() {
        if (TextUtils.isEmpty(CommonUtils.BOX_URL)) {
            getBoxUrl();
            return;
        }
        new Thread(runnable).start();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Request request = new Request.Builder()
                    .post(RequestBody.create(null, new byte[0]))
                    .url(CommonUtils.BOX_URL + "/schoolbox/connect")
                    .build();
            try {
                final Response execute = client.newCall(request).execute();
                if (execute.isSuccessful()) {
                    FILE_IP = CommonUtils.BOX_URL + "/schoolbox/resource/access";
                    SaveLogUtils.saveInfo("box_info.txt", "循环判定资源地址成功：" + FILE_IP);

                } else {
                    FILE_IP = "";
                    SaveLogUtils.saveInfo("box_info.txt", "循环判定资源地址失败");
                    //失败后立即重新检查盒子地址
                    CommonUtils.BOX_URL = null;

                }
            } catch (IOException e) {
                e.printStackTrace();
                FILE_IP = "";
                //失败后立即重新检查盒子地址
                CommonUtils.BOX_URL = null;
            } finally {
                Log.e("盒子路径是：" + FILE_IP);
            }
        }
    };

    /**
     * 系统视频播放器不能自动重定向, 改为调用{@link #getFileAddress(String, String, Activity, RequestFileAddressCallBack)}先访问接口, 从回调中获取真实url
     *
     * @see #getFileAddress(String, String, Activity, RequestFileAddressCallBack)
     */
    @Deprecated
    public static String getFileAddress(String resourceId, String address) {
        if (TextUtils.isEmpty(FILE_IP)) {
            return address;
        } else {
            return FILE_IP + "?resourceId=" + resourceId + "&convertMediaPath=" + address;
        }
    }

    /**
     * 每次打开资源要重新获取盒子地址
     */
    public static void getFileAddress(final String resourceId, final String address, final Activity activity, final RequestFileAddressCallBack callBack) {
        if (callBack == null) {
            return;
        }
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean == null) {
            callBack.onSuccess(address);
            return;
        }
        final String customerSchoolId = loginBean.getData().getUser().getCustomerSchoolId() + "";

        Network.createService(NetWorkService.LoginService.class)
                .getBoxUrl(customerSchoolId)
                .map(new StatusFunc<BoxInfoBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.immediate())
                .subscribe(new Subscriber<BoxInfoBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onSuccess(address);
                    }

                    @Override
                    public void onNext(BoxInfoBean o) {
                        if (TextUtils.isEmpty(o.getData().getIp())) {
                            callBack.onSuccess(address);
                            return;
                        } else {
                            CommonUtils.BOX_URL = "http://" + o.getData().getIp();
                            SaveLogUtils.saveInfo("box_info.txt", "获取到盒子的判定文件文件地址：" + CommonUtils.BOX_URL);

                            Request request = new Request.Builder()
                                    .post(RequestBody.create(null, new byte[0]))
                                    .url(CommonUtils.BOX_URL + "/schoolbox/connect")
                                    .build();
                            try {
                                Field readTimeoutField = client.getClass().getDeclaredField("readTimeout");
                                readTimeoutField.setAccessible(true);
                                //设置读写超时
                                Field connectTimeoutField = client.getClass().getDeclaredField("connectTimeout");
                                connectTimeoutField.setAccessible(true);
                                readTimeoutField.setInt(client, 10000);
                                connectTimeoutField.setInt(client, 3000);
                                final Response execute = client.newCall(request).execute();
                                if (execute.isSuccessful()) {
                                    FILE_IP = CommonUtils.BOX_URL + "/schoolbox/resource/access";
                                    SaveLogUtils.saveInfo("box_info.txt", "循环判定资源地址成功：" + FILE_IP);
                                    if (TextUtils.isEmpty(FILE_IP)) {
                                        callBack.onSuccess(address);
                                    } else {
                                        Network.createService(NetWorkService.RequestFilePath.class, CommonUtils.BOX_URL)
                                                .getFilePath(resourceId, address)
                                                .map(new StatusFunc<FileUrlBean>())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(new ProgressSubscriber<FileUrlBean>(new SubscriberListener<FileUrlBean>() {
                                                    @Override
                                                    public void onNext(FileUrlBean o) {
                                                        callBack.onSuccess(o.getUrl());
                                                    }

                                                    @Override
                                                    public void onError(Throwable e) {
                                                        callBack.onFailed(e);
                                                    }

                                                    @Override
                                                    public void onCompleted() {
                                                    }
                                                }, activity));
                                    }
                                } else {
                                    FILE_IP = "";
                                    SaveLogUtils.saveInfo("box_info.txt", "循环判定资源地址失败");
                                    //失败后立即重新检查盒子地址
                                    CommonUtils.BOX_URL = null;
                                    callBack.onSuccess(address);
                                }
                            } catch (Exception e) {
                                FILE_IP = "";
                                CommonUtils.BOX_URL = null;
                                e.printStackTrace();
                                callBack.onSuccess(address);
                            }

                        }
                    }
                });
    }

    public interface RequestFileAddressCallBack {
        void onFailed(Throwable e);

        void onSuccess(String path);
    }
}
