package com.sdzn.fzx.student.libbase.base;

import android.util.SparseArray;

import com.sdzn.fzx.student.libbase.baseui.adapter.SynthesizeListAdapter;
import com.sdzn.fzx.student.libpublic.views.exam.ClozeTestTextView;
import com.sdzn.fzx.student.libpublic.views.exam.FillBlankEditText;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

/**
 * 试题题目点击事件接口
 * @author Reisen at 2018-11-16
 */
public interface OnExamClickListener {

    /**
     * 添加图片 (相机/相册
     */
    void addPic(AnswerListBean.AnswerDataBean bean, int picLen);

    /**
     * 手写笔添加图片
     */
    void addWritePic(int position, AnswerListBean.AnswerDataBean bean);

    /**
     * 清空图片
     */
    void cleanImg(BaseViewHolder holder, int position, AnswerListBean.AnswerDataBean bean);

    /**
     * 综合题用接口
     */
    void cleanImg(SynthesizeListAdapter.SynShortAnswerHolder holder, int position, AnswerListBean.AnswerDataBean bean);

    /**
     * 打开图片
     */
    void openImg(int index, String url, boolean showDel, AnswerListBean.AnswerDataBean bean);

    /**
     * 收藏/取消收藏 该接口已取消
     *
     //* @param isCollect     true=要收藏,false=要取消收藏
     //* @param id            bean.id, 收藏时传入该参数, 结果返回studentBookId
     //* @param studentBookId bean.studentBookId 取消收藏时要上传的参数
     */
//    void changeCollect(AnswerListBean.AnswerDataBean bean);

    /**
     * 打开资源文件
     */
    void showResource(AnswerListBean.AnswerDataBean bean);

    /**
     * 弹起试题作答弹窗
     */
    void showClozeTestTextWindow(ClozeTestTextView view,
                                 AnswerListBean.AnswerDataBean bean,
                                 SparseArray<ClozeAnswerBean> map,
                                 int position);

    /**
     * 关闭输入窗
     */
    void closeInput(FillBlankEditText et);

    /**
     * 点击填空题空位, 处理调起键盘等逻辑
     */
    void onBlankClick(FillBlankEditText et, ClozeAnswerBean bean, int position);

    /**
     * 填空内容改变监听, 处理文本修改时逻辑
     */
    void onBlankTextChange(ClozeAnswerBean bean);

}
