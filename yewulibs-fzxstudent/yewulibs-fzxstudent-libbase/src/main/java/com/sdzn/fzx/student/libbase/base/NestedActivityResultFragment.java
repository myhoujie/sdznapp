package com.sdzn.fzx.student.libbase.base;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.squareup.otto.Subscribe;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class NestedActivityResultFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

}
