package com.sdzn.fzx.student.libbase.listener;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/18
 */
public interface OnPageChangeListener {
    void onPageChange(int pos);
}
