package com.sdzn.fzx.student.libbase.login.presenter;

import android.app.Activity;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.libbase.login.view.LoginViews;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.LoginBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * LoginPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoginPresenter1 extends Presenter<LoginViews> {

    public void login(final String name, String psw, Activity activity) {
//        if (!vertifyNum(name, psw)) {
//            if (CommonAppUtils.isLingChuangPad()) {
//                CommonAppUtils.logout();
//                App2.get().stopService(new Intent(App2.get(), MqttService.class));
//                ActivityManager.exit();
//            }
//            return;
//        }


//        final String deviceId = AndroidUtil.getDeviceID(App2.get());
//        if ("/auth".equals(BuildConfig2.AUTH)) {
//        }

        //这里上传敏感数据会导致无法通过安全测试
//        CrashReport.putUserData(App2.get(), "studentName", Base64.encodeToString(name.getBytes(), Base64.NO_WRAP));
//        psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
//        CrashReport.putUserData(App2.get(), "studentPwd", psw);


//        Network.createService(NetWorkService.LoginService.class)
//                .login(name, psw, 1, deviceId, "Android")
//                .map(new StatusFunc<LoginBean>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
//                    @Override
//                    public void onNext(LoginBean o) {
//                        mView.loginSuccess(o);
//                        CommonUtils.isReceiveRbMq = true;
////                        StudentSPUtils.savetoken(o.getData().getAccessToken());
//                        SPUtils.getInstance().put(StudentSPUtils.SP_TOKEN,o.getData().getAccessToken());
//                        Log.e("aaatest", "token" + o.getData().getAccessToken());
//                        startMqttService();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
//                            ApiException exception = (ApiException) e;
//                            int code = exception.getStatus().getCode();
//                            if (code == 21001 && CommonAppUtils.isLingChuangPad()) {
//                                StudentSPUtils.setAutoLogin(false);
//                                StudentSPUtils.saveLoginUserNum("");
//                                StudentSPUtils.saveLastPenAddr("");
//                                CommonAppUtils.logout();
//                                App2.get().stopService(new Intent(App2.get(), MqttService.class));
//                                ActivityManager.exit();
//                            }
//                        } else {
//                            ToastUtil.showShortlToast("登录失败");
//                        }
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity, true, false, true, null));
        final ProgressDialogManager pdm = new ProgressDialogManager(activity);
        pdm.getProgressDialog().setCancelable(false);
        pdm.getProgressDialog().setCanceledOnTouchOutside(false);
        pdm.showWaiteDialog("登录中...");//压缩中...

        RetrofitNetNew.build(Api.class, getIdentifier())
                .login(name,psw)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<LoginBean.DataBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<LoginBean.DataBean>> call, Response<ResponseSlbBean1<LoginBean.DataBean>> response) {
                        pdm.cancelWaiteDialog();
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().loginFailed(response.body().getMessage());
                            return;
                        }
                        getView().loginSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<LoginBean.DataBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        pdm.cancelWaiteDialog();
                        getView().loginFailed(t.getMessage());
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }




    public boolean vertifyNum(final String userName, final String psw) {
        if (userName.length() == 8 || userName.length() == 18) {
            if (StringUtils.vertifyPsw(psw)) {
                return true;
            } else {
                ToastUtil.showShortlToast("账号或密码错误");
            }
        } else {
            ToastUtil.showShortlToast("账号或密码错误");
        }
        return false;
    }


}
