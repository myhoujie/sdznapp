package com.sdzn.fzx.student.libbase.baseui.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.AnswerResultInfoBean;

/**
 * @author Reisen at 2018-12-11
 */
public interface AnswerResultNativeView extends BaseView {

    /**
     * 加载试题内容成功
     */
    void loadExamInfoSuccess(AnswerResultInfoBean.AnswerResultDataBean bean);

    /**
     * 失败
     */
    void loadExamInfoFailed(String msg);

    /**
     * 收藏成功
     */
    void saveCollectSuccess();

    /**
     * 取消收藏成功
     */
    void deleteCollectSuccess();

    /**
     * 收藏/取消收藏失败
     */
    void collectError(String msg);
}
