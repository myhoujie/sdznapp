package com.sdzn.fzx.student.libbase.show;

import android.graphics.RectF;

import com.sdzn.fzx.student.dao.Dots;
import com.sdzn.fzx.student.libbase.base.BaseView;

import java.util.List;

/**
 * @author Reisen at 2018-08-06
 */
public interface WhiteBoardView extends BaseView {
    void loadLocalDataSuccess(List<Dots> list);
    void saveDotsSuccess(int flag, RectF rectF);
    void saveDotsFailed(String msg, int flag);
}
