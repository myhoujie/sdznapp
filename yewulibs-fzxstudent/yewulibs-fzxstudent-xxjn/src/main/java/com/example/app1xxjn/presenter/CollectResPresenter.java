package com.example.app1xxjn.presenter;

import com.example.app1xxjn.view.CollectResView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.SubjectFilterBean;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * CollectResPresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectResPresenter extends BasePresenter<CollectResView, BaseActivity> {

    public void getSubjectList() {
        final LoginBean loginBean = StudentSPUtils.getLoginBean();
        final int baseGradeId = loginBean.getData().getUser().getBaseGradeId();
        Network.createTokenService(NetWorkService.StudyPackageService.class)
                .getSubjectFilterList(baseGradeId + "", "true")
                .map(new StatusFunc<SubjectFilterBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SubjectFilterBean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.onSubjectFilterFailed(status.getMsg());
                            } else {
                                mView.onSubjectFilterFailed("数据获取失败");
                            }
                        } else {
                            mView.onSubjectFilterFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onNext(SubjectFilterBean o) {
                        mView.onSubjectFilterOk(o);
                    }
                });
    }

}
