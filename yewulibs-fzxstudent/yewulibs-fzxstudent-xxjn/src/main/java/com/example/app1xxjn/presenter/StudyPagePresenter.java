package com.example.app1xxjn.presenter;

import com.example.app1xxjn.view.StudyPageView;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;

/**
 * StudyPagePresenter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class StudyPagePresenter extends BasePresenter<StudyPageView,BaseActivity> {
}
