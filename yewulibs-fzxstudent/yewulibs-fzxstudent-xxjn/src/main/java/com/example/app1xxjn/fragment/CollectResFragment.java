package com.example.app1xxjn.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.app1xxjn.R;
import com.example.app1xxjn.adapter.CollectAdapter;
import com.example.app1xxjn.adapter.SubjectFilterRvAdapter;
import com.example.app1xxjn.presenter.CollectResPresenter;
import com.example.app1xxjn.view.CollectResView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libpublic.views.LoadStatusView;
import com.sdzn.fzx.student.vo.SubjectFilterBean;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.internal.CustomAdapt;

/**
 * A simple {@link Fragment} subclass.
 * 我的错题，收藏试题
 */
public class CollectResFragment extends MBaseFragment<CollectResPresenter> implements CollectResView, CustomAdapt {
    private RecyclerView subjectFilterRv;
    private ViewPager resVp;
    private LoadStatusView errorRy;
    SubjectFilterRvAdapter subjectFilterRvAdapter;
    private CollectAdapter collectAdapter;


    public static CollectResFragment newInstance(Bundle bundle) {
        CollectResFragment fragment = new CollectResFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collect_res, container, false);
        subjectFilterRv = (RecyclerView) view.findViewById(R.id.subject_filter_rv);
        resVp = (ViewPager) view.findViewById(R.id.res_vp);
        errorRy = (LoadStatusView) view.findViewById(R.id.error_ry);
        initViews();
        initData();
        return view;
    }


    private void initViews() {
        subjectFilterRv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        subjectFilterRvAdapter = new SubjectFilterRvAdapter(getContext());
        subjectFilterRv.setAdapter(subjectFilterRvAdapter);

        collectAdapter = new CollectAdapter(getFragmentManager());
        resVp.setAdapter(collectAdapter);


        resVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                subjectFilterRvAdapter.setCurItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        subjectFilterRvAdapter.setItemChangedListener(new SubjectFilterRvAdapter.OnItemChangedListener() {
            @Override
            public void onItemChangedByClicked(int pos) {
                resVp.setCurrentItem(pos, false);
            }
        });
        errorRy.onEmpty();

        errorRy.setReloadListener(new LoadStatusView.ReloadListener() {
            @Override
            public void onReloadClicked() {
                mPresenter.getSubjectList();
            }
        });

    }

    private void initData() {
        mPresenter.getSubjectList();
    }

    @Override
    public void initPresenter() {
        mPresenter = new CollectResPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initData();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    @Override
    public void onSubjectFilterOk(SubjectFilterBean bean) {
        if (bean.getData().size() == 0) {
            errorRy.onEmpty();
        } else {
            errorRy.onSuccess();
        }

        subjectFilterRvAdapter.setSubjectList(bean.getData());
        List<Fragment> list = new ArrayList<>();
        for (int i = 0; i < bean.getData().size(); i++) {
            final ResListFragment resListFragment = new ResListFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(ResListFragment.SUBJECT_ID_TAG, bean.getData().get(i).getId());
            resListFragment.setArguments(bundle);
            list.add(resListFragment);
        }
        collectAdapter.setFragments(list);
    }

    @Override
    public void onSubjectFilterFailed(String msg) {
        errorRy.onError();
    }

    @Override
    public boolean isBaseOnWidth() {
        return false;
    }

    @Override
    public float getSizeInDp() {
        return 1280;
    }
}
