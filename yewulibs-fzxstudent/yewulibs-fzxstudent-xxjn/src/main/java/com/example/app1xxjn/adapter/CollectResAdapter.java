package com.example.app1xxjn.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1xxjn.R;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.baseui.activity.DocViewActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.ImageDisplayActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.PDFActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.PlayerActivity;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.CollectResBean;
import com.sdzn.fzx.student.vo.ResourceVo;

import java.util.List;


/**
 * CollectResAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectResAdapter extends BaseAdapter {

    private Activity context;

    private List<CollectResBean.DataBean> list;

    public CollectResAdapter(Activity context) {

        this.context = context;
    }

    public void setResList(List<CollectResBean.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void removeItem(String id) {

        if (list == null) {
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            final String itemId = list.get(i).getId() + "";
            if (id.equals(itemId)) {
                list.remove(i);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final CollectResBean.DataBean dataBean = list.get(i);
        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_collect_res_layout, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.resName.setText(dataBean.getResourceName());
        if (dataBean.getResourceType() == 1) {
            ResourceVo.ResourceTextVo vo = GsonUtil.fromJson(dataBean.getResourceText(), ResourceVo.ResourceTextVo.class);
            if (vo != null && !TextUtils.isEmpty(vo.getResourceSuffix()) && ".txt".equalsIgnoreCase(vo.getResourceSuffix())) {
                holder.resLogo.setImageResource(R.mipmap.txt);
            } else {
                holder.resLogo.setImageResource(R.drawable.list_resource_type_bg);
                holder.resLogo.setImageLevel(dataBean.getResourceType());
            }
        } else {
            holder.resLogo.setImageResource(R.drawable.list_resource_type_bg);
            holder.resLogo.setImageLevel(dataBean.getResourceType());
        }
        holder.resTypeTxt.setText(dataBean.getSubjectStructureName());
        holder.resPullDate.setText(StringUtils.transTime(dataBean.getCreateTime(), "yyyy-MM-dd"));
        holder.resDetail.setText(dataBean.getChapterName());
        holder.isCollectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemRemoved(dataBean.getId() + "");
                }
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ResourceVo.ResourceTextVo resourceTextVo = GsonUtil.fromJson(dataBean.getResourceText(), ResourceVo.ResourceTextVo.class);
                if (resourceTextVo == null) {
                    ToastUtil.showShortlToast("找不到资源");
                    return;
                }
                switch (dataBean.getResourceType()) {
                    case 1:
                    case 2:
                        showSelectOpenDialog(new OpenListener() {
                            @Override
                            public void openByApp() {
                                Intent intentDoc = new Intent(context, PDFActivity.class);
                                intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                                intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                context.startActivity(intentDoc);
                            }

                            @Override
                            public void openByOther() {
                                Intent intentDoc = new Intent(context, DocViewActivity.class);
                                intentDoc.putExtra("path", resourceTextVo.getResourcePath());
                                intentDoc.putExtra("name", resourceTextVo.getResourceName());
                                switch (resourceTextVo.getResourceType()) {
                                    case 1:
                                        if (".txt".equalsIgnoreCase(resourceTextVo.getResourceSuffix())) {
                                            intentDoc.putExtra("type", "txt");
                                        } else {
                                            intentDoc.putExtra("type", "doc");
                                        }
                                        break;
                                    case 2:
                                        intentDoc.putExtra("type", "ppt");
                                        break;
                                }
                                context.startActivity(intentDoc);
                            }
                        });
                        break;
                    case 3:
                        SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath(), context, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentVideo = new Intent(context, PlayerActivity.class);
                                intentVideo.putExtra("videoUrl", path);
                                intentVideo.putExtra("title", resourceTextVo.getResourceName());
                                context.startActivity(intentVideo);
                            }
                        });
//                        Intent intentVideo = new Intent(context, PlayerActivity.class);
//                        intentVideo.putExtra("videoUrl", SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath()));
//                        intentVideo.putExtra("title", resourceTextVo.getResourceName());
//                        context.startActivity(intentVideo);
                        break;
                    case 4:
                        SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath(), context, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentImage = new Intent(context, ImageDisplayActivity.class);
                                intentImage.putExtra("photoUrl", path);
                                context.startActivity(intentImage);
                            }
                        });
//                        Intent intentImage = new Intent(context, ImageDisplayActivity.class);
//                        intentImage.putExtra("photoUrl", SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath()));
//                        context.startActivity(intentImage);
                        break;
                    case 5:
                        SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath(), context, new SchoolBoxWatcher.RequestFileAddressCallBack() {
                            @Override
                            public void onFailed(Throwable e) {
                                ToastUtil.showShortlToast("获取文件地址失败");
                            }

                            @Override
                            public void onSuccess(String path) {
                                Intent intentRadio = new Intent(context, PlayerActivity.class);
                                intentRadio.putExtra("radioUrl", path);
                                intentRadio.putExtra("title", resourceTextVo.getResourceName());
                                context.startActivity(intentRadio);
                            }
                        });
//                        Intent intentRadio = new Intent(context, PlayerActivity.class);
//                        intentRadio.putExtra("radioUrl", SchoolBoxWatcher.getFileAddress(dataBean.getResourceId() + "", resourceTextVo.getConvertPath()));
//                        intentRadio.putExtra("title", resourceTextVo.getResourceName());
//                        context.startActivity(intentRadio);
                        break;
                    case 6:
                        Intent intentDoc = new Intent(context, PDFActivity.class);
                        intentDoc.putExtra("path", resourceTextVo.getConvertPath());
//                        intentDoc.putExtra("path", SchoolBoxWatcher.getFileAddress(resourceTextVo.getId() + "", resourceTextVo.getConvertPath()));
                        intentDoc.putExtra("name", resourceTextVo.getResourceName());
                        context.startActivity(intentDoc);
                        break;
                }
            }
        });

        return view;
    }

    public Dialog showSelectOpenDialog(final OpenListener listener) {
        final Dialog dialog = new Dialog(context, R.style.Dialog);
        View view = context.getLayoutInflater().inflate(R.layout.dialog_open_res, null);
        view.findViewById(R.id.tv_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByApp();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openByOther();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    interface OpenListener {
        void openByApp();

        void openByOther();
    }

    static class ViewHolder {
       /* @BindView(R.id.is_collect_img)
        ImageView isCollectImg;
        @BindView(R.id.res_logo)
        ImageView resLogo;
        @BindView(R.id.res_name)
        TextView resName;
        @BindView(R.id.res_type_txt)
        TextView resTypeTxt;
        @BindView(R.id.res_pull_date)
        TextView resPullDate;
        @BindView(R.id.res_detail)
        TextView resDetail;*/

        private ImageView isCollectImg;
        private ImageView resLogo;
        private TextView resName;
        private TextView resTypeTxt;
        private TextView resPullDate;
        private TextView resDetail;
        private TextView tvApp;
        private TextView tvOther;


        ViewHolder(View view) {
            tvApp = (TextView) view.findViewById(R.id.tv_app);
            tvOther = (TextView) view.findViewById(R.id.tv_other);
            isCollectImg = (ImageView) view.findViewById(R.id.is_collect_img);
            resLogo = (ImageView) view.findViewById(R.id.res_logo);
            resName = (TextView) view.findViewById(R.id.res_name);
            resTypeTxt = (TextView) view.findViewById(R.id.res_type_txt);
            resPullDate = (TextView) view.findViewById(R.id.res_pull_date);
            resDetail = (TextView) view.findViewById(R.id.res_detail);
        }
    }

    private RemoveResListener listener;

    public void setListener(RemoveResListener listener) {
        this.listener = listener;
    }

    public interface RemoveResListener {
        void onItemRemoved(String id);
    }
}
