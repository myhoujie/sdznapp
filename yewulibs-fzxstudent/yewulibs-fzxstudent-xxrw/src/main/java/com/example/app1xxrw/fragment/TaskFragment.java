package com.example.app1xxrw.fragment;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app1xxrw.R;
import com.example.app1xxrw.listener.OnTaskListener;
import com.example.app1xxrw.presenter.TaskPresenter;
import com.example.app1xxrw.view.TaskView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libbase.listener.OnSearchClickListener;
import com.sdzn.fzx.student.libbase.pop.TaskSearchPop;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.views.ImageHintEditText;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;



/**
 * 学习任务
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public class TaskFragment extends MBaseFragment<TaskPresenter> implements TaskView, RadioGroup.OnCheckedChangeListener, View.OnClickListener{
    //, CustomAdapt111

    private View viewStatus;
    private RadioGroup rgGroup;
    private RadioButton rbTask;
    private RadioButton rbTaskFile;
    private View line;
    private RecyclerView rvSubject;
    private LinearLayout llBar;
    private LinearLayout llCheck;
    private CheckBox checkbox;
    private ImageHintEditText btnSearch;
    private FrameLayout framelayoutTask;

    private List<Fragment> fragments;
    private int containerId = R.id.framelayoutTask;
    private Fragment currFragment;
    private Y_MultiRecyclerAdapter subjectAdapter;
    private List<SubjectVo.DataBean> subjects = new ArrayList<>();
    private Y_ItemEntityList itemEntityListSub = new Y_ItemEntityList(); // 学科列表
    private TaskSearchPop taskSearchPop;
    private String searchStr;
    private SubjectVo.DataBean currentSubject;
    private String scene = "task";
    private boolean check;

    public static TaskFragment newInstance(Bundle bundle) {
        TaskFragment fragment = new TaskFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void initPresenter() {
        mPresenter = new TaskPresenter();
        mPresenter.attachView(this, activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task, container, false);
        viewStatus = (View) rootView.findViewById(R.id.viewStatus);
        rgGroup = (RadioGroup) rootView.findViewById(R.id.rg_group);
        rbTask = (RadioButton) rootView.findViewById(R.id.rb_task);
        rbTaskFile = (RadioButton) rootView.findViewById(R.id.rb_task_file);
        line = (View) rootView.findViewById(R.id.line);
        rvSubject = (RecyclerView) rootView.findViewById(R.id.rvSubject);
        llBar = (LinearLayout) rootView.findViewById(R.id.llBar);
        llCheck = (LinearLayout) rootView.findViewById(R.id.llCheck);
        checkbox = (CheckBox) rootView.findViewById(R.id.checkbox);
        btnSearch = (ImageHintEditText) rootView.findViewById(R.id.btnSearch);
        framelayoutTask = (FrameLayout) rootView.findViewById(R.id.framelayoutTask);
        // x.view().inject(this, rootView);
        initFragment();
        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (subjects.size() == 0) {
            mPresenter.getSubjects();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initView() {
        rgGroup.setOnCheckedChangeListener(this);
        btnSearch.setOnClickListener(this);
        rvSubject.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        subjectAdapter = new Y_MultiRecyclerAdapter(App2.get(), itemEntityListSub);
        rvSubject.setAdapter(subjectAdapter);
        rvSubject.addOnItemTouchListener(new OnItemTouchListener(rvSubject) {
            @Override
            public void onItemClick(final RecyclerView.ViewHolder viewHolder) {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        int position = viewHolder.getAdapterPosition();
                        if (position >= 0) {
                            setSelected(position);
                        }
                    }
                });
            }
        });
        checkbox.setChecked(check);
        changeStatus();
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                changeStatus();
            }
        });
    }

    private void setSelected(int pos) {
        for (int i = 0; i < subjects.size(); i++) {
            SubjectVo.DataBean dataBean = subjects.get(i);
            if (i == pos) {
                dataBean.setSelected(true);
            } else {
                dataBean.setSelected(false);
            }
        }
        currentSubject = subjects.get(pos);
        subjectAdapter.notifyDataSetChanged();
        changeStatus();

        for (Fragment fragment : fragments) {
            ((OnTaskListener) fragment).onSubjectItemClick(pos);
        }
    }

    /**
     * 切换全部/待完成
     */
    private void changeStatus() {
        ((OnTaskListener) fragments.get(0)).onStatusChanged(checkbox.isChecked() ? 2 : 0);
        btnSearch.setCenter(true);
        btnSearch.setText("");
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if (checkedId == R.id.rb_task) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    showAssignedFragment(0);
                    scene = "task";
                    llCheck.setVisibility(View.VISIBLE);
                }
            });
        } else if (checkedId == R.id.rb_task_file) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    showAssignedFragment(1);
                    scene = "taskFile";
                    llCheck.setVisibility(View.GONE);
                }
            });
        } else {
        }
    }

    private void initFragment() {
        fragments = new ArrayList<>();

        fragments.add(TaskContentFragment.newInstance(null));
        fragments.add(TaskFileContentFragment.newInstance(null));
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        if (currFragment != fragment) {
            if (!fragment.isAdded()) {
                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
            } else {
                ft.hide(currFragment).show(fragment);
            }
        }
        currFragment = fragment;
        ft.commit();
    }

    @Override
    public void getSubjectSuccess(SubjectVo subjectVo) {
        subjects.clear();
        SubjectVo.DataBean all = new SubjectVo.DataBean();
        all.setId(0);
        all.setName("全部");
        all.setSelected(true);
        subjects.add(0, all);
        currentSubject = all;
        if (subjectVo != null && subjectVo.getData() != null) {
            List<SubjectVo.DataBean> data = subjectVo.getData();
            subjects.addAll(data);
        }
        setSubjects();

        for (Fragment fragment : fragments) {
            ((OnTaskListener) fragment).onSubjectGet(subjects);
            ((OnTaskListener) fragment).setOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageChange(int pos) {
                    setSelected(pos);
                }
            });
        }
    }

    /**
     * 设置学科列表
     */
    private void setSubjects() {
        itemEntityListSub.clear();
        itemEntityListSub.addItems(R.layout.item_fragment_task_content_sub, subjects)
                .addOnBind(R.layout.item_fragment_task_content_sub, new Y_OnBind() {
                    @Override
                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                        SubjectVo.DataBean dataBean = (SubjectVo.DataBean) itemData;
                        Subject subject = Subject.subjects.get(dataBean.getId());
                        if (dataBean.isSelected()) {
                            if (subject != null) {
                                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageBitmap(BitmapFactory.decodeResource(getResources(), subject.getDrawableId_sel()));
                            }
                            ((TextView) holder.getChildView(R.id.tvXueke)).setTextColor(getResources().getColor(R.color.fragment_main_chart_line_text));
                        } else {
                            if (subject != null) {
                                ((ImageView) holder.getChildView(R.id.ivXueke)).setImageBitmap(BitmapFactory.decodeResource(getResources(), subject.getDrawableId_nor()));
                            }
                            ((TextView) holder.getChildView(R.id.tvXueke)).setTextColor(getResources().getColor(R.color.fragment_task_subject_item_text_g));
                        }
                        holder.setText(R.id.tvXueke, dataBean.getName());
                    }
                });
        subjectAdapter.notifyDataSetChanged();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btnSearch) {
            showPop();
        }
    }

    private void showPop() {
        if (taskSearchPop == null) {
            taskSearchPop = new TaskSearchPop(activity, new OnSearchClickListener() {
                @Override
                public void onSearch(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);

                    if ("task".equals(scene)) {
                        ((OnTaskListener) fragments.get(0)).onSearch(searchStr);
                    } else if ("taskFile".equals(scene)) {
                        ((OnTaskListener) fragments.get(1)).onSearch(searchStr);
                    }
                    TaskFragment.this.searchStr = searchStr;
                }

                @Override
                public void onTextChanged(String searchStr) {
                    btnSearch.setCenter(TextUtils.isEmpty(searchStr));
                    btnSearch.setText(searchStr);
                    if (TextUtils.isEmpty(searchStr) && !TextUtils.isEmpty(TaskFragment.this.searchStr)) {
                        if ("task".equals(scene)) {
                            ((OnTaskListener) fragments.get(0)).onSearch(searchStr);
                        } else if ("taskFile".equals(scene)) {
                            ((OnTaskListener) fragments.get(1)).onSearch(searchStr);
                        }
                    }
                }
            });
        }

        int subjectId = 0;
        if (currentSubject != null) {
            subjectId = currentSubject.getId();
        }
        taskSearchPop.showPopupWindow(scene, btnSearch, subjectId);
    }

    @Override
    public void onHiddenChanged(boolean isHidden) {
        if (subjects.size() == 0) {
            mPresenter.getSubjects();
        }
    }

    public void setCheck(boolean check) {
        this.check = check;
        if (checkbox != null) {
            checkbox.setChecked(check);
            changeStatus();
        }
    }

/*    @Override
    public boolean isBaseOnWidth() {
        return false;
    }

    @Override
    public float getSizeInDp() {
        return 1280;
    }*/
}
