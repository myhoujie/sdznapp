package com.example.app1xxrw.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1xxrw.R;
import com.example.app1xxrw.presenter.TaskListPresenter;
import com.example.app1xxrw.view.TaskListView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.baseui.activity.AnswerNativeActivity;
import com.sdzn.fzx.student.libbase.baseui.activity.AnswerResultNativeActivity;
import com.sdzn.fzx.student.libbase.comparator.TaskComparator;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.event.RefreshTaskEvent;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.TaskListVo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学习任务列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskListFragment extends MBaseFragment<TaskListPresenter> implements TaskListView, OnRefreshListener, OnLoadMoreListener {

    private SmartRefreshLayout refreshLayout;
    private EmptyRecyclerView rvTask;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;

    private int subjectId;
    private int status = 0;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter taskAdapter;
    private List<TaskListPresenter.MyCountDownTimer> downTimers = new ArrayList<>();
    private int currPage = 0;
    private int pageSize = 10;
    private String searchStr;
    private int historyCount = 0;
    public  Intent answerIntent;

    @Override
    public void initPresenter() {
        mPresenter = new TaskListPresenter();
        mPresenter.attachView(this, activity);
    }

    public static TaskListFragment newInstance(Bundle bundle) {
        TaskListFragment fragment = new TaskListFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_list, container, false);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        rvTask = (EmptyRecyclerView) rootView.findViewById(R.id.swipe_target);
        llTaskEmpty = (LinearLayout) rootView.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) rootView.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) rootView.findViewById(R.id.tvTaskEmpty);
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        subjectId = getArguments().getInt("subjectId");
        status = getArguments().getInt("status");
        initView();
        return rootView;
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));

        rvTask.setEmptyView(llTaskEmpty);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == historyCount) {
                    return 3;
                }
                return 1;
            }
        });
        rvTask.setLayoutManager(gridLayoutManager);
        taskAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvTask.setAdapter(taskAdapter);
        rvTask.addOnItemTouchListener(new OnItemTouchListener(rvTask) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
                Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
                if (itemData instanceof TaskListVo.DataBean) {
                    final TaskListVo.DataBean dataBean = (TaskListVo.DataBean) itemData;
                    if (dataBean.getStatus() == 2 || dataBean.getStatus() == 3) {
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
//                        Intent answerIntent;
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerResultActivity.class);
//                        }else {
                        //答题完成进入统计界面
//                        answerIntent = new Intent(activity, AnswerResultNativeActivity.class);
//                        }
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("taskDataBean", dataBean);
//                        answerIntent.putExtras(bundle);
//                        startActivity(answerIntent);
                    } else {
                        SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                answerIntent = new Intent(activity, AnswerNativeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("taskDataBean", dataBean);
                                answerIntent.putExtras(bundle);
                                startActivity(answerIntent);
                            }
                        });
//                        Intent answerIntent;
//                        if (AppLike.showH5) {
//                            answerIntent = new Intent(activity, AnswerActivity.class);
//                        }else {
                        //没有作答，进入作答界面
//                        answerIntent = new Intent(activity, AnswerNativeActivity.class);
//                        }
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("taskDataBean", dataBean);
//                        answerIntent.putExtras(bundle);
//                        startActivity(answerIntent);
                    }
                }
            }
        });

        currPage = 1;
        getTasks();
    }

    /**
     * 设置任务状态（全部/待完成）
     *
     * @param status
     */
    public void setStatus(int status) {
        this.searchStr = null;
        if (this.status != status) {
            this.status = status;
            currPage = 1;
            itemEntityList.clear();
            getTasks();
        }
    }

    /**
     * 搜索任务
     *
     * @param searchStr
     */
    public void search(String searchStr) {
        currPage = 1;
        itemEntityList.clear();
        this.searchStr = searchStr;
        getTasks();
    }

    /**
     * 获取任务列表
     */
    private void getTasks() {
        Map<String, String> params = new HashMap<>();
        params.put("userStudentId", UserController.getUserId());
        if (subjectId > 0) {
            params.put("baseSubjectId", String.valueOf(subjectId));
        }
        if (status == 2) {
            params.put("status", "2");
        }
        if (searchStr != null) {
            params.put("keyword", searchStr);
        }
        params.put("page", String.valueOf(currPage));
        params.put("rows", String.valueOf(pageSize));
        mPresenter.getTasks(params);
    }

    @Override
    public void getTaskListSuccess(TaskListVo taskListVo) {
        cancilLoadState();
        for (TaskListPresenter.MyCountDownTimer myCountDownTimer : downTimers) {
            myCountDownTimer.cancel();
        }

        // 第一页，需要先清空
        if (currPage == 1) {
            itemEntityList.clear();
            historyCount = 0;
        }
//        if (!itemEntityList.getItems().contains(getString(R.string.fragment_task_content_text9))) {
//            // 添加标签
//            itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, getString(R.string.fragment_task_content_text9))
//                    .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
//                        @Override
//                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
//                            holder.setText(R.id.tvDecoration, (String) itemData);
//                        }
//                    });
//        }

        if (taskListVo != null && taskListVo.getData() != null && taskListVo.getData().size() > 0) {
            List<TaskListVo.DataBean> data = taskListVo.getData();
            Collections.sort(data, new TaskComparator());

//            if (itemEntityList.getItems().contains("历史任务")) {
//                itemEntityList.addItems(R.layout.item_fragment_task_list, data)
//                        .addOnBind(R.layout.item_fragment_task_list, new Y_OnBind() {
//                            @Override
//                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
//                                TaskListPresenter.MyCountDownTimer myCountDownTimer = mPresenter.bindTaskListHolder(holder, (TaskListVo.AnswerDataBean) itemData);
//                                if (myCountDownTimer != null) {
//                                    downTimers.add(myCountDownTimer);
//                                }
//                            }
//                        });
//            } else {
            for (TaskListVo.DataBean dataBean : data) {
                long time = dataBean.getStartTime();
                int days = DateUtil.differentDays(time, System.currentTimeMillis());
                if (days <= 7 && !itemEntityList.getItems().contains(getString(R.string.fragment_task_content_text9))) {
                    itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, getString(R.string.fragment_task_content_text9))
                            .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                                @Override
                                public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                    holder.setText(R.id.tvDecoration, (String) itemData);
                                }
                            });
                } else if (days > 7 && !itemEntityList.getItems().contains(getString(R.string.fragment_task_content_text8))) {
                    // 添加标签
                    historyCount = itemEntityList.getItemCount();
                    itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, getString(R.string.fragment_task_content_text8))
                            .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                                @Override
                                public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                    holder.setText(R.id.tvDecoration, (String) itemData);
                                }
                            });
                }

                itemEntityList.addItem(R.layout.item_fragment_task_list, dataBean)
                        .addOnBind(R.layout.item_fragment_task_list, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                TaskListPresenter.MyCountDownTimer myCountDownTimer = mPresenter.bindTaskListHolder(holder, (TaskListVo.DataBean) itemData);
                                if (myCountDownTimer != null) {
                                    downTimers.add(myCountDownTimer);
                                }
                            }
                        });
            }
//            }

            taskAdapter.notifyDataSetChanged();
            if (data.size() < pageSize) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }
        } else if (itemEntityList.getItemCount() == 1) {
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_task_content_text4));
            rvTask.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
        } else {
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_task_content_text4));
            rvTask.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void networkError(String msg) {
        ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text18));
        rvTask.setEmptyView(llTaskEmpty);
        cancilLoadState();
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        getTasks();
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getTasks();
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(Event event) {
        if (event instanceof RefreshTaskEvent) {
            currPage = 1;
            getTasks();
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
