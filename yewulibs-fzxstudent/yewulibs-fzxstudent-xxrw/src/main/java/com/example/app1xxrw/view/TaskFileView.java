package com.example.app1xxrw.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.TaskFileVo;

/**
 * 任务内容列表PresenterView
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public interface TaskFileView extends BaseView {
    void getTaskFileSuccess(TaskFileVo taskFileVo);

    void networkError(String msg);
}
