package com.example.app1xxrw.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1xxrw.R;
import com.example.app1xxrw.activity.TaskFileDetailActivity;
import com.example.app1xxrw.presenter.TaskFilePresenter;
import com.example.app1xxrw.view.TaskFileView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.comparator.TaskFileComparator;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libpublic.views.EmptyRecyclerView;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_ItemEntityList;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.Y_OnBind;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.vo.TaskFileVo;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学习资料
 *
 * @author wangchunxiao
 * @date 2018/1/18
 */
public class TaskFileFragment extends MBaseFragment<TaskFilePresenter> implements TaskFileView, OnRefreshListener, OnLoadMoreListener {

    //@ViewInject(R.id.refreshLayout)
    private SmartRefreshLayout refreshLayout;
    //@ViewInject(R.id.swipe_target)
    private EmptyRecyclerView rvTask;
    //@ViewInject(R.id.llTaskEmpty)
    private LinearLayout llTaskEmpty;
    //@ViewInject(R.id.ivTaskEmpty)
    private ImageView ivTaskEmpty;
    //@ViewInject(R.id.tvTaskEmpty)
    private TextView tvTaskEmpty;


    private int subjectId;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();
    private Y_MultiRecyclerAdapter taskAdapter;
    private int currPage = 0;
    private int pageSize = 10;
    private String searchStr;
    private int historyCount = 0;

    @Override
    public void initPresenter() {
        mPresenter = new TaskFilePresenter();
        mPresenter.attachView(this, activity);
    }

    public static TaskFileFragment newInstance(Bundle bundle) {
        TaskFileFragment fragment = new TaskFileFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subjectId = getArguments().getInt("subjectId");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_file, container, false);
        refreshLayout = rootView.findViewById(R.id.refreshLayout);
        rvTask = rootView.findViewById(R.id.swipe_target);
        llTaskEmpty = rootView.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = rootView.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = rootView.findViewById(R.id.tvTaskEmpty);
        //x.view().inject(this, rootView);
        initView();
//        subjectId = getArguments().getInt("subjectId");
        return rootView;
    }

    private void initView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadMoreListener(this);
        refreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        refreshLayout.setRefreshFooter(new ClassicsFooter(activity));

        rvTask.setEmptyView(llTaskEmpty);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == historyCount) {
                    return 3;
                }
                return 1;
            }
        });
        rvTask.setLayoutManager(gridLayoutManager);
        taskAdapter = new Y_MultiRecyclerAdapter(activity, itemEntityList);
        rvTask.setAdapter(taskAdapter);
        rvTask.addOnItemTouchListener(new OnItemTouchListener(rvTask) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
                Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
                if (itemData instanceof TaskFileVo.DataBean) {
                    TaskFileVo.DataBean dataBean = (TaskFileVo.DataBean) itemData;
                    Intent intent = new Intent(activity, TaskFileDetailActivity.class);
                    intent.putExtra("lessonId", String.valueOf(dataBean.getLessonId()));
                    intent.putExtra("classId", String.valueOf(dataBean.getClassId()));
                    intent.putExtra("chapterName", String.valueOf(dataBean.getChapterName()));
                    startActivity(intent);
                }
            }
        });

        currPage = 1;
        getTaskFiles();
    }

    /**
     * 搜索任务
     *
     * @param searchStr
     */
    public void search(String searchStr) {
        currPage = 1;
        itemEntityList.clear();
        this.searchStr = searchStr;
        getTaskFiles();
    }

    /**
     * 获取任务资料列表
     */
    private void getTaskFiles() {
        Map<String, String> params = new HashMap<>();
        params.put("classId", UserController.getClassId());
        if (subjectId > 0) {
            params.put("baseSubjectId", String.valueOf(subjectId));
        }
        if (searchStr != null) {
            params.put("keyword", searchStr);
        }
        params.put("page", String.valueOf(currPage));
        params.put("rows", String.valueOf(pageSize));
        mPresenter.getTaskFiles(params);
    }

    @Override
    public void getTaskFileSuccess(TaskFileVo taskFileVo) {
        cancilLoadState();

        // 第一页，需要先清空
        if (currPage == 1) {
            itemEntityList.clear();
            historyCount = 0;
        }

        if (!itemEntityList.getItems().contains(getString(R.string.fragment_task_content_text9))) {
            // 添加标签
            itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, getString(R.string.fragment_task_content_text9))
                    .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            holder.setText(R.id.tvDecoration, (String) itemData);
                        }
                    });
        }

        if (taskFileVo != null && taskFileVo.getData() != null && taskFileVo.getData().size() > 0) {
            List<TaskFileVo.DataBean> data = taskFileVo.getData();
            Collections.sort(data, new TaskFileComparator());

            if (itemEntityList.getItems().contains("历史任务")) {
                itemEntityList.addItems(R.layout.item_fragment_task_file, data)
                        .addOnBind(R.layout.item_fragment_task_file, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                mPresenter.bindTaskFileHolder(holder, (TaskFileVo.DataBean) itemData);
                            }
                        });
            } else {
                for (TaskFileVo.DataBean dataBean : data) {
                    long time = dataBean.getUpdateTime();
                    int days = DateUtil.differentDays(time, System.currentTimeMillis());
                    if (days > 7 && !itemEntityList.getItems().contains(getString(R.string.fragment_task_content_text8))) {
                        // 添加标签
                        historyCount = itemEntityList.getItemCount();
                        itemEntityList.addItem(R.layout.item_fragment_task_list_decoration, getString(R.string.fragment_task_content_text8))
                                .addOnBind(R.layout.item_fragment_task_list_decoration, new Y_OnBind() {
                                    @Override
                                    public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                        holder.setText(R.id.tvDecoration, (String) itemData);
                                    }
                                });
                    }
                    itemEntityList.addItem(R.layout.item_fragment_task_file, dataBean)
                            .addOnBind(R.layout.item_fragment_task_file, new Y_OnBind() {
                                @Override
                                public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {

                                    mPresenter.bindTaskFileHolder(holder, (TaskFileVo.DataBean) itemData);
                                }
                            });
                }
            }

            taskAdapter.notifyDataSetChanged();
            if (data.size() < pageSize) {
                refreshLayout.setEnableLoadMore(false);
            } else {
                refreshLayout.setEnableLoadMore(true);
            }
        } else if (itemEntityList.getItemCount() == 1) {
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_task_content_text13));
            rvTask.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
        } else {
            itemEntityList.clear();
            taskAdapter.notifyDataSetChanged();
            ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_task_content_text13));
            rvTask.setEmptyView(llTaskEmpty);
            refreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void networkError(String msg) {
        ivTaskEmpty.setBackgroundDrawable(getResources().getDrawable(R.mipmap.no_data));
        tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text18));
        rvTask.setEmptyView(llTaskEmpty);
        cancilLoadState();
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        currPage = 1;
        getTaskFiles();
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        currPage++;
        getTaskFiles();
    }

    /**
     * 取消加载状态
     */
    private void cancilLoadState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        } else if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadMore();
        }
    }
}
