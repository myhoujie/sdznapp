package com.example.app1xxrw.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.TaskListVo;

/**
 * 任务内容列表PresenterView
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public interface TaskListView extends BaseView {
    void getTaskListSuccess(TaskListVo taskListVo);

    void networkError(String msg);
}
