package com.example.app1xxrw.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.SubjectVo;

/**
 * 首页PresenterView
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public interface TaskView extends BaseView {

    void getSubjectSuccess(SubjectVo subjectVo);

    void networkError(String msg);
}
