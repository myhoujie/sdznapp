package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/3/16 0016.
 */

public class StatisticsTimeVo {

    /**
     * data : {"useTime":0,"useTimeAvg":0}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * useTime : 0
         * useTimeAvg : 0
         */

        private int useTime;
        private float useTimeAvg;

        public int getUseTime() {
            return useTime;
        }

        public void setUseTime(int useTime) {
            this.useTime = useTime;
        }

        public float getUseTimeAvg() {
            return useTimeAvg;
        }

        public void setUseTimeAvg(float useTimeAvg) {
            this.useTimeAvg = useTimeAvg;
        }
    }
}
