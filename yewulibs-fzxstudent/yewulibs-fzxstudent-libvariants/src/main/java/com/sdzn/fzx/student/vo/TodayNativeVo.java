package com.sdzn.fzx.student.vo;

import java.util.List;

public class TodayNativeVo {

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {


        private int baseSubjectId;
        private String baseSubjectName;
        private List<ExamListBean> examList;

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public List<ExamListBean> getExamList() {
            return examList;
        }

        public void setExamList(List<ExamListBean> examList) {
            this.examList = examList;
        }

        public static class ExamListBean {

            private int id;
            private String examId;
            private int examTemplateId;
            private int examTemplateStyleId;
            private String examTemplateStyleName;
            private int examEmptyCount;
            private int examDifficulty;
            private int parentId;
            private int userStudentId;
            private String userStudentName;
            private long timeCreate;
            private long timeUpdate;
            private int chapterId;
            private String chapterName;
            private String chapterNodeIdPath;
            private String chapterNodeNamePath;
            private int baseVolumeId;
            private String baseVolumeName;
            private int baseSubjectId;
            private String baseSubjectName;
            private int baseGradeId;
            private String baseGradeName;
            private int type;
            private Object typeReview;
            private Object typeReviewName;
            private int model;
            private int flag;
            private int count;
            private String examText;
            private List<SubjectErrorBean.DataBean> examList;
            private transient TodayExamVo todayExamVo;
            private transient ExamText examTextVo;

            public ExamText getExamTextVo() {
                return examTextVo;
            }

            public void setExamTextVo(ExamText examTextVo) {
                this.examTextVo = examTextVo;
            }

            public TodayExamVo getTodayExamVo() {
                return todayExamVo;
            }

            public void setTodayExamVo(TodayExamVo todayExamVo) {
                this.todayExamVo = todayExamVo;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getExamId() {
                return examId;
            }

            public void setExamId(String examId) {
                this.examId = examId;
            }

            public int getExamTemplateId() {
                return examTemplateId;
            }

            public void setExamTemplateId(int examTemplateId) {
                this.examTemplateId = examTemplateId;
            }

            public int getExamTemplateStyleId() {
                return examTemplateStyleId;
            }

            public void setExamTemplateStyleId(int examTemplateStyleId) {
                this.examTemplateStyleId = examTemplateStyleId;
            }

            public String getExamTemplateStyleName() {
                return examTemplateStyleName;
            }

            public void setExamTemplateStyleName(String examTemplateStyleName) {
                this.examTemplateStyleName = examTemplateStyleName;
            }

            public int getExamEmptyCount() {
                return examEmptyCount;
            }

            public void setExamEmptyCount(int examEmptyCount) {
                this.examEmptyCount = examEmptyCount;
            }

            public int getExamDifficulty() {
                return examDifficulty;
            }

            public void setExamDifficulty(int examDifficulty) {
                this.examDifficulty = examDifficulty;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getUserStudentId() {
                return userStudentId;
            }

            public void setUserStudentId(int userStudentId) {
                this.userStudentId = userStudentId;
            }

            public String getUserStudentName() {
                return userStudentName;
            }

            public void setUserStudentName(String userStudentName) {
                this.userStudentName = userStudentName;
            }

            public long getTimeCreate() {
                return timeCreate;
            }

            public void setTimeCreate(long timeCreate) {
                this.timeCreate = timeCreate;
            }

            public long getTimeUpdate() {
                return timeUpdate;
            }

            public void setTimeUpdate(long timeUpdate) {
                this.timeUpdate = timeUpdate;
            }

            public int getChapterId() {
                return chapterId;
            }

            public void setChapterId(int chapterId) {
                this.chapterId = chapterId;
            }

            public String getChapterName() {
                return chapterName;
            }

            public void setChapterName(String chapterName) {
                this.chapterName = chapterName;
            }

            public String getChapterNodeIdPath() {
                return chapterNodeIdPath;
            }

            public void setChapterNodeIdPath(String chapterNodeIdPath) {
                this.chapterNodeIdPath = chapterNodeIdPath;
            }

            public String getChapterNodeNamePath() {
                return chapterNodeNamePath;
            }

            public void setChapterNodeNamePath(String chapterNodeNamePath) {
                this.chapterNodeNamePath = chapterNodeNamePath;
            }

            public int getBaseVolumeId() {
                return baseVolumeId;
            }

            public void setBaseVolumeId(int baseVolumeId) {
                this.baseVolumeId = baseVolumeId;
            }

            public String getBaseVolumeName() {
                return baseVolumeName;
            }

            public void setBaseVolumeName(String baseVolumeName) {
                this.baseVolumeName = baseVolumeName;
            }

            public int getBaseSubjectId() {
                return baseSubjectId;
            }

            public void setBaseSubjectId(int baseSubjectId) {
                this.baseSubjectId = baseSubjectId;
            }

            public String getBaseSubjectName() {
                return baseSubjectName;
            }

            public void setBaseSubjectName(String baseSubjectName) {
                this.baseSubjectName = baseSubjectName;
            }

            public int getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(int baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return baseGradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.baseGradeName = baseGradeName;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public Object getTypeReview() {
                return typeReview;
            }

            public void setTypeReview(Object typeReview) {
                this.typeReview = typeReview;
            }

            public Object getTypeReviewName() {
                return typeReviewName;
            }

            public void setTypeReviewName(Object typeReviewName) {
                this.typeReviewName = typeReviewName;
            }

            public int getModel() {
                return model;
            }

            public void setModel(int model) {
                this.model = model;
            }

            public int getFlag() {
                return flag;
            }

            public void setFlag(int flag) {
                this.flag = flag;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public String getExamText() {
                return examText;
            }

            public void setExamText(String examText) {
                this.examText = examText;
            }

            public List<SubjectErrorBean.DataBean> getExamList() {
                return examList;
            }

            public void setExamList(List<SubjectErrorBean.DataBean> examList) {
                this.examList = examList;
            }
        }
    }
}
