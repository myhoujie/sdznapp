package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/13
 * 修改单号：
 * 修改内容:
 */
public class TodayExamVo {

    /**
     * baseEducationId : 1
     * baseEducationName : 六三制
     * baseGradeId : 7
     * baseGradeName : 七年级
     * baseLevelId : 2
     * baseLevelName : 初中
     * baseSubjectId : 1
     * baseSubjectName : 语文
     * baseVersionId : 1
     * baseVersionName : 人教版（部编版）
     * baseVolumeId : 1
     * baseVolumeName : 上册
     * chapterId : 0
     * chapterName : 全部章节
     * chapterNodeIdPath : 0
     * chapterNodeNamePath : 全部章节
     * createTime : 1543456807272
     * createUserId : 233
     * createUserName : 王银勇
     * customerSchoolId : 60
     * customerSchoolName : 时代智囊测试
     * difficulty : 1
     * examAnalysis :
     * examAnswer : 1 2 3 4 5 6 7 8
     * examExplain :
     * examOptions : [{"analysis":"","content":"1","right":true,"seq":1},{"analysis":"","content":"2","right":true,"seq":2},{"analysis":"","content":"3","right":true,"seq":3},{"analysis":"","content":"4","right":true,"seq":4},{"analysis":"","content":"5","right":true,"seq":5},{"analysis":"","content":"6","right":true,"seq":6},{"analysis":"","content":"7","right":true,"seq":7},{"analysis":"","content":"8","right":true,"seq":8}]
     * examStem : <p style='line-height:24px'><strong><span style=''>I.</span></strong> <strong><span style=''>根据音标写单词</span></strong></p><p style='margin-top:10px;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;line-height:24px'><strong><span style=''>1.</span></strong><strong> </strong><strong><span style=''>/ne</span></strong><strong><span style=''>I</span></strong><strong><span style=''>m/<input index='1' class='cus-com' readonly='readonly' type='text' value='(1)'/>2. /mi:t/<input index='2' class='cus-com' readonly='readonly' type='text' value='(2)'/>3. /tu:/<input index='3' class='cus-com' readonly='readonly' type='text' value='(3)'/>4. /ænd/<input index='4' class='cus-com' readonly='readonly' type='text' value='(4)'/>  </span></strong></p><p style='margin-top:10px;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;line-height:24px'><strong><span style=''>5. /ʃi:/<input index='5' class='cus-com' readonly='readonly' type='text' value='(5)'/>6. /hə:/<input index='6' class='cus-com' readonly='readonly' type='text' value='(6)'/>7. /jɔ:/<input index='7' class='cus-com' readonly='readonly' type='text' value='(7)'/>8. /na</span></strong><strong><span style=''>I</span></strong><strong><span style=''>s/<input index='8' class='cus-com' readonly='readonly' type='text' value='(8)'/></span></strong></p><p><br/></p>
     * examTypeId : 6
     * flagShare : 0
     * holdUserId : 233
     * holdUserName : 王银勇
     * hots : 0
     * id : 5bff4827b2c0132a6cc3ac84
     * optionNumber : 8
     * points : []
     * score : -1
     * templateStyleId : 63
     * templateStyleName : 填空题
     * type : 1
     * updateTime : 1543456807272
     * zoneIdPath : 0.15.223.5025
     * zoneName : 高新区
     */

    private int baseEducationId;
    private String baseEducationName;
    private int baseGradeId;
    private String baseGradeName;
    private int baseLevelId;
    private String baseLevelName;
    private int baseSubjectId;
    private String baseSubjectName;
    private int baseVersionId;
    private String baseVersionName;
    private int baseVolumeId;
    private String baseVolumeName;
    private int chapterId;
    private String chapterName;
    private String chapterNodeIdPath;
    private String chapterNodeNamePath;
    private long createTime;
    private int createUserId;
    private String createUserName;
    private int customerSchoolId;
    private String customerSchoolName;
    private int difficulty;
    private String examAnalysis;
    private String examAnswer;
    private String examExplain;
    private String examStem;
    private int examTypeId;
    private int flagShare;
    private int holdUserId;
    private String holdUserName;
    private int hots;
    private String id;
    private int optionNumber;
    private int score;
    private int templateStyleId;
    private String templateStyleName;
    private int type;
    private long updateTime;
    private String zoneIdPath;
    private String zoneName;
    private List<ExamOptionsBean> examOptions;
    private List<ExamText.PointsBean> points;

    public int getBaseEducationId() {
        return baseEducationId;
    }

    public void setBaseEducationId(int baseEducationId) {
        this.baseEducationId = baseEducationId;
    }

    public String getBaseEducationName() {
        return baseEducationName;
    }

    public void setBaseEducationName(String baseEducationName) {
        this.baseEducationName = baseEducationName;
    }

    public int getBaseGradeId() {
        return baseGradeId;
    }

    public void setBaseGradeId(int baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public String getBaseGradeName() {
        return baseGradeName;
    }

    public void setBaseGradeName(String baseGradeName) {
        this.baseGradeName = baseGradeName;
    }

    public int getBaseLevelId() {
        return baseLevelId;
    }

    public void setBaseLevelId(int baseLevelId) {
        this.baseLevelId = baseLevelId;
    }

    public String getBaseLevelName() {
        return baseLevelName;
    }

    public void setBaseLevelName(String baseLevelName) {
        this.baseLevelName = baseLevelName;
    }

    public int getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(int baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getBaseSubjectName() {
        return baseSubjectName;
    }

    public void setBaseSubjectName(String baseSubjectName) {
        this.baseSubjectName = baseSubjectName;
    }

    public int getBaseVersionId() {
        return baseVersionId;
    }

    public void setBaseVersionId(int baseVersionId) {
        this.baseVersionId = baseVersionId;
    }

    public String getBaseVersionName() {
        return baseVersionName;
    }

    public void setBaseVersionName(String baseVersionName) {
        this.baseVersionName = baseVersionName;
    }

    public int getBaseVolumeId() {
        return baseVolumeId;
    }

    public void setBaseVolumeId(int baseVolumeId) {
        this.baseVolumeId = baseVolumeId;
    }

    public String getBaseVolumeName() {
        return baseVolumeName;
    }

    public void setBaseVolumeName(String baseVolumeName) {
        this.baseVolumeName = baseVolumeName;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterNodeIdPath() {
        return chapterNodeIdPath;
    }

    public void setChapterNodeIdPath(String chapterNodeIdPath) {
        this.chapterNodeIdPath = chapterNodeIdPath;
    }

    public String getChapterNodeNamePath() {
        return chapterNodeNamePath;
    }

    public void setChapterNodeNamePath(String chapterNodeNamePath) {
        this.chapterNodeNamePath = chapterNodeNamePath;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(int createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public int getCustomerSchoolId() {
        return customerSchoolId;
    }

    public void setCustomerSchoolId(int customerSchoolId) {
        this.customerSchoolId = customerSchoolId;
    }

    public String getCustomerSchoolName() {
        return customerSchoolName;
    }

    public void setCustomerSchoolName(String customerSchoolName) {
        this.customerSchoolName = customerSchoolName;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getExamAnalysis() {
        return examAnalysis;
    }

    public void setExamAnalysis(String examAnalysis) {
        this.examAnalysis = examAnalysis;
    }

    public String getExamAnswer() {
        return examAnswer;
    }

    public void setExamAnswer(String examAnswer) {
        this.examAnswer = examAnswer;
    }

    public String getExamExplain() {
        return examExplain;
    }

    public void setExamExplain(String examExplain) {
        this.examExplain = examExplain;
    }

    public String getExamStem() {
        return examStem;
    }

    public void setExamStem(String examStem) {
        this.examStem = examStem;
    }

    public int getExamTypeId() {
        return examTypeId;
    }

    public void setExamTypeId(int examTypeId) {
        this.examTypeId = examTypeId;
    }

    public int getFlagShare() {
        return flagShare;
    }

    public void setFlagShare(int flagShare) {
        this.flagShare = flagShare;
    }

    public int getHoldUserId() {
        return holdUserId;
    }

    public void setHoldUserId(int holdUserId) {
        this.holdUserId = holdUserId;
    }

    public String getHoldUserName() {
        return holdUserName;
    }

    public void setHoldUserName(String holdUserName) {
        this.holdUserName = holdUserName;
    }

    public int getHots() {
        return hots;
    }

    public void setHots(int hots) {
        this.hots = hots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTemplateStyleId() {
        return templateStyleId;
    }

    public void setTemplateStyleId(int templateStyleId) {
        this.templateStyleId = templateStyleId;
    }

    public String getTemplateStyleName() {
        return templateStyleName;
    }

    public void setTemplateStyleName(String templateStyleName) {
        this.templateStyleName = templateStyleName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getZoneIdPath() {
        return zoneIdPath;
    }

    public void setZoneIdPath(String zoneIdPath) {
        this.zoneIdPath = zoneIdPath;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public List<ExamOptionsBean> getExamOptions() {
        return examOptions;
    }

    public void setExamOptions(List<ExamOptionsBean> examOptions) {
        this.examOptions = examOptions;
    }

    public List<ExamText.PointsBean> getPoints() {
        return points;
    }

    public void setPoints(List<ExamText.PointsBean> points) {
        this.points = points;
    }

    public static class ExamOptionsBean implements Comparable<ExamOptionsBean> {
        /**
         * analysis :
         * content : 1
         * right : true
         * seq : 1
         */

        private String analysis;
        private String content;
        private boolean right;
        private int seq;

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptionsBean examOptionsBean) {
            return Integer.compare(seq, examOptionsBean.seq);
        }
    }
}
