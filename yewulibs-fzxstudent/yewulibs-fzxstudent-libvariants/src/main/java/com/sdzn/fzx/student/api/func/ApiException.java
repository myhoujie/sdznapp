package com.sdzn.fzx.student.api.func;


import com.sdzn.fzx.student.api.module.StatusVo;

/**
 * 描述：请求错误处理
 * -
 * 创建人：wangchunxiao
 * 创建时间：2016/10/25
 */
public class ApiException extends RuntimeException {

    private int resultCode;
    private StatusVo statusVo;
    private String detailMessage;

    public ApiException(int resultCode) {
        this.resultCode = resultCode;
    }

    public ApiException(int resultCode,String msg) {
        this.resultCode = resultCode;
    }

    public ApiException(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    public ApiException(StatusVo statusVo) {
        this.statusVo = statusVo;
    }

    public int getCode() {
        return resultCode;
    }

    public StatusVo getStatus() {
        return statusVo;
    }

    public String getMsg() {
        return detailMessage;
    }
}

