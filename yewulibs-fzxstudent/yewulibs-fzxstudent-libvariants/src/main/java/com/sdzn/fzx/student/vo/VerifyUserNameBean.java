package com.sdzn.fzx.student.vo;

/**
 * VerifyUserNameBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class VerifyUserNameBean {


    /**
     * id : 75
     * realName : 卫璨
     * accountId : 169
     * accountName : 72350486
     * available : 1
     * nickName : null
     * sex : 2
     * photo : null
     * createAccountId : 11
     * createUserId : null
     * createUserName : admin
     * createTime : 1514941485000
     * updateTime : null
     * userType : null
     * telephone : 13563636363
     * accountRoleList : null
     * email : null
     * identityId : 620982198906031424
     * studentNumber : 12345678
     * birthday : 1320076800000
     * baseEducationId : 1
     * baseEducationName : 六三制
     * baseLevelId : 2
     * baseLevelName : 初中
     * baseGradeId : 7
     * baseGradeName : 七年级
     * points : null
     * zoneId : null
     * zoneName : null
     * zoneIdPath : null
     * zoneNamePath : null
     * scope : 2017
     * customerSchoolName : 测试学校勿删
     * customerSchoolId : 9
     */
    private LoginBean.DataBean data;

    public LoginBean.DataBean getData() {
        return data;
    }

    public void setData(LoginBean.DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int id;
        private String realName;
        private int accountId;
        private String accountName;
        private int available;
        private Object nickName;
        private int sex;
        private Object photo;
        private int createAccountId;
        private Object createUserId;
        private String createUserName;
        private long createTime;
        private Object updateTime;
        private Object userType;
        private String telephone;
        private Object accountRoleList;
        private Object email;
        private String identityId;
        private String studentNumber;
        private long birthday;
        private int baseEducationId;
        private String baseEducationName;
        private int baseLevelId;
        private String baseLevelName;
        private int baseGradeId;
        private String baseGradeName;
        private Object points;
        private Object zoneId;
        private Object zoneName;
        private Object zoneIdPath;
        private Object zoneNamePath;
        private int scope;
        private String customerSchoolName;
        private int customerSchoolId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public int getAccountId() {
            return accountId;
        }

        public void setAccountId(int accountId) {
            this.accountId = accountId;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public int getAvailable() {
            return available;
        }

        public void setAvailable(int available) {
            this.available = available;
        }

        public Object getNickName() {
            return nickName;
        }

        public void setNickName(Object nickName) {
            this.nickName = nickName;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public Object getPhoto() {
            return photo;
        }

        public void setPhoto(Object photo) {
            this.photo = photo;
        }

        public int getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(int createAccountId) {
            this.createAccountId = createAccountId;
        }

        public Object getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(Object createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getUserType() {
            return userType;
        }

        public void setUserType(Object userType) {
            this.userType = userType;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public Object getAccountRoleList() {
            return accountRoleList;
        }

        public void setAccountRoleList(Object accountRoleList) {
            this.accountRoleList = accountRoleList;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public String getIdentityId() {
            return identityId;
        }

        public void setIdentityId(String identityId) {
            this.identityId = identityId;
        }

        public String getStudentNumber() {
            return studentNumber;
        }

        public void setStudentNumber(String studentNumber) {
            this.studentNumber = studentNumber;
        }

        public long getBirthday() {
            return birthday;
        }

        public void setBirthday(long birthday) {
            this.birthday = birthday;
        }

        public int getBaseEducationId() {
            return baseEducationId;
        }

        public void setBaseEducationId(int baseEducationId) {
            this.baseEducationId = baseEducationId;
        }

        public String getBaseEducationName() {
            return baseEducationName;
        }

        public void setBaseEducationName(String baseEducationName) {
            this.baseEducationName = baseEducationName;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public Object getPoints() {
            return points;
        }

        public void setPoints(Object points) {
            this.points = points;
        }

        public Object getZoneId() {
            return zoneId;
        }

        public void setZoneId(Object zoneId) {
            this.zoneId = zoneId;
        }

        public Object getZoneName() {
            return zoneName;
        }

        public void setZoneName(Object zoneName) {
            this.zoneName = zoneName;
        }

        public Object getZoneIdPath() {
            return zoneIdPath;
        }

        public void setZoneIdPath(Object zoneIdPath) {
            this.zoneIdPath = zoneIdPath;
        }

        public Object getZoneNamePath() {
            return zoneNamePath;
        }

        public void setZoneNamePath(Object zoneNamePath) {
            this.zoneNamePath = zoneNamePath;
        }

        public int getScope() {
            return scope;
        }

        public void setScope(int scope) {
            this.scope = scope;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }
    }
}
