package com.sdzn.fzx.student.vo;

/**
 * StatisticCountBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class StatisticCountBean {

    /**
     * todayAdd : 0
     * weekAdd : 5
     * monthAdd : 5
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int todayAdd;
        private int weekAdd;
        private int monthAdd;

        public int getTodayAdd() {
            return todayAdd;
        }

        public void setTodayAdd(int todayAdd) {
            this.todayAdd = todayAdd;
        }

        public int getWeekAdd() {
            return weekAdd;
        }

        public void setWeekAdd(int weekAdd) {
            this.weekAdd = weekAdd;
        }

        public int getMonthAdd() {
            return monthAdd;
        }

        public void setMonthAdd(int monthAdd) {
            this.monthAdd = monthAdd;
        }
    }
}
