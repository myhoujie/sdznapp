package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * CollectResTyepBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CollectResTyepBean {

    /**
     * id : 1
     * name : 学案
     * nodeIdPath : 0.1
     * nodeNamePath : 学案
     * status : 0
     * parentId : 0
     * leaf : 0
     * seq : 1
     * baseSubjectId : 1
     * baseSubjectName : 语文
     * childList : [{"id":7,"name":"新授课","nodeIdPath":"0.1.7","nodeNamePath":"学案>新授课","status":0,"parentId":1,"leaf":1,"seq":1,"baseSubjectId":1,"baseSubjectName":"语文","childList":[]},{"id":8,"name":"复习","nodeIdPath":"0.1.8","nodeNamePath":"学案>复习","status":0,"parentId":1,"leaf":1,"seq":2,"baseSubjectId":1,"baseSubjectName":"语文","childList":[]},{"id":9,"name":"同步作业","nodeIdPath":"0.1.9","nodeNamePath":"学案>同步作业","status":0,"parentId":1,"leaf":1,"seq":3,"baseSubjectId":1,"baseSubjectName":"语文","childList":[]}]
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String name;
        private String nodeIdPath;
        private String nodeNamePath;
        private int status;
        private int parentId;
        private int leaf;
        private int seq;
        private String baseSubjectId;
        private String baseSubjectName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNodeIdPath() {
            return nodeIdPath;
        }

        public void setNodeIdPath(String nodeIdPath) {
            this.nodeIdPath = nodeIdPath;
        }

        public String getNodeNamePath() {
            return nodeNamePath;
        }

        public void setNodeNamePath(String nodeNamePath) {
            this.nodeNamePath = nodeNamePath;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getLeaf() {
            return leaf;
        }

        public void setLeaf(int leaf) {
            this.leaf = leaf;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public String getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(String baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }
    }
}
