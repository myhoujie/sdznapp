package com.sdzn.fzx.student.vo;

/**
 * @author Reisen at 2018-11-09
 */
public class SSOInfoBean {
    private String name;
    private String pwd;
    private boolean autoLogin;

    public SSOInfoBean() {
    }

    public SSOInfoBean(String name, String pwd, boolean autoLogin) {
        this.name = name;
        this.pwd = pwd;
        this.autoLogin = autoLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public boolean isAutoLogin() {
        return autoLogin;
    }

    public void setAutoLogin(boolean autoLogin) {
        this.autoLogin = autoLogin;
    }
}
