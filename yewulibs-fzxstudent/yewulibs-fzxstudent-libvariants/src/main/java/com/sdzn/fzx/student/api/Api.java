package com.sdzn.fzx.student.api;

import com.haier.cellarette.libretrofit.common.ResponseSlbBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.fzx.student.bean.GrzxRecActBean;
import com.sdzn.fzx.student.bean.HCategoryBean;
import com.sdzn.fzx.student.bean.RecyclerTabBean;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.UserBean;
import com.sdzn.fzx.student.vo.VerifyUserMobile;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {
    String SERVER_ISERVICE = "https://m.hexiangjiaoyu.com/bcapi/";

    // 所有分类的列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/liveEnumLists")
    Call<ResponseSlbBean<HCategoryBean>> get_category(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_category2(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_update(@Body RequestBody body);

    // 检查更新
    @FormUrlEncoded
    @POST("http://doc.znclass.com" + ApiInterface.QUERY_VERSION_INFO)
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Field("programId") String programId, @Field("type") String type);//@Body RequestBody body@Header("Authorization") String token,

    // 侧滑pop
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_CEHUA_INFO)
    Call<ResponseSlbBean1<GrzxRecActBean>> queryCehua();//@Header("Authorization") String token

    // 动态获取tab数据
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_TAB_INFO)
    Call<ResponseSlbBean1<RecyclerTabBean>> queryTab(@Body RequestBody body);//@Header("Authorization") String token,

    // 首页数据
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://49.4.1.81:7799" + ApiInterface.QUERY_SHOUYE_INFO)
    Call<ResponseSlbBean1<ShouyeRecActBean>> queryShouye(@Header("Authorization") String token);//@Header("Authorization") String token,

    // 个人中心获取资料
    @POST("http://49.4.1.81:7799" + ApiInterface.STUDENT_INFO)
    Call<ResponseSlbBean1<UserBean>> studentInfo(@Header("Authorization") String token);

    // 登录
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.LOGIN_INFO)
    Call<ResponseSlbBean1<LoginBean.DataBean>> login(@Field("username") String username, @Field("password") String password);//@Body RequestBody body@Header("Authorization") String token,

    // 发送验证码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.SEND_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> sendYZM(@Field("telephone") String telephone);

    // 验证验证码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.CHECK_VERIFYCODE)
    Call<ResponseSlbBean1<Object>> checkYZM(@Field("mobile") String mobile,@Field("code") String code);

    // 学生更新手机号
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.STUDENT_MOBILE_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePhone(@Header("Authorization") String token, @Field("newTel") String mobile, @Field("code") String code ,@Field("userId") String userId);

  // 学生更新密码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.STUDENT_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updatePassword(@Header("Authorization") String token, @Field("id") String id,
                                                  @Field("oldPassword") String oldPassword, @Field("password") String password);

    // 忘记密码--获取手机号
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://49.4.1.81:7799" + ApiInterface.STUDENT_LOGIN_MOBILE)
    Call<ResponseSlbBean1<VerifyUserMobile>> studentFindMobile(@Body RequestBody body);

    // 忘记密码--更新密码
    @FormUrlEncoded
    @POST("http://49.4.1.81:7799" + ApiInterface.STUDENT_LOGIN_PASSWORD_UPDATE)
    Call<ResponseSlbBean1<Object>> updateForgetPas(@Field("mobile") String mobile,@Field("password") String password,@Field("code") String code);

}
