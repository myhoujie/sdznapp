package com.sdzn.fzx.student.presenter;

import com.alibaba.fastjson.JSONObject;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.RecyclerTabBean;
import com.sdzn.fzx.student.view.TabViews;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class TabPresenter extends Presenter<TabViews> {

    public void querytab(String type) {
        JSONObject requestData = new JSONObject();
        requestData.put("type", type);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryTab(requestBody)//"Bearer "+(String) SPUtils.getInstance().getString("token", ""),
                .enqueue(new Callback<ResponseSlbBean1<RecyclerTabBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<RecyclerTabBean>> call, Response<ResponseSlbBean1<RecyclerTabBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onTabNodata(response.body().getMessage());
                            return;
                        }
                        getView().onTabSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<RecyclerTabBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onTabFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
