package com.sdzn.fzx.student.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/14
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */
@Entity
public class WhiteBoardHistory {
    @Id(autoincrement = true)
    private Long id;
    private String exam;
    @Unique
    private Integer page;
    private String examId;
    private Integer X;
    private Integer Y;
    private String url;
    @Generated(hash = 288169766)
    public WhiteBoardHistory(Long id, String exam, Integer page, String examId,
                             Integer X, Integer Y, String url) {
        this.id = id;
        this.exam = exam;
        this.page = page;
        this.examId = examId;
        this.X = X;
        this.Y = Y;
        this.url = url;
    }
    @Generated(hash = 154802170)
    public WhiteBoardHistory() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getExam() {
        return this.exam;
    }
    public void setExam(String exam) {
        this.exam = exam;
    }
    public Integer getPage() {
        return this.page;
    }
    public void setPage(Integer page) {
        this.page = page;
    }
    public String getExamId() {
        return this.examId;
    }
    public void setExamId(String examId) {
        this.examId = examId;
    }
    public Integer getX() {
        return this.X;
    }
    public void setX(Integer X) {
        this.X = X;
    }
    public Integer getY() {
        return this.Y;
    }
    public void setY(Integer Y) {
        this.Y = Y;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
}
