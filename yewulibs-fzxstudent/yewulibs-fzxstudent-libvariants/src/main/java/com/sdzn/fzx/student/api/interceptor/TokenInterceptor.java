package com.sdzn.fzx.student.api.interceptor;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * TokenInterceptor〈当token过期后，重新刷新，获取token，然后再次访问网络〉
 * 〈token过期处理〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class TokenInterceptor implements Interceptor {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private String accessToken;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        LoginBean loginBean = StudentSPUtils.getLoginBean();

        if (loginBean == null) {
            StudentSPUtils.saveLoginBean(null);
            EventBus.getDefault().post(Event.EVENT_TOKEN_MISS);
            throw new ApiException(new StatusVo(10000, "拦截器中无法获取个人消息，无法继续操作"));
        }
        final String  accessToken = loginBean.getData().getAccessToken();
        if (!TextUtils.isEmpty(accessToken)) {

            HttpUrl.Builder authorizedUrlBuilder = request.url()
                    .newBuilder()
                    .scheme(request.url().scheme())
                    .host(request.url().host())
                    .addQueryParameter("access_token", accessToken);

            request = request.newBuilder()
                    .method(request.method(), request.body())
                    .url(authorizedUrlBuilder.build())
                    .build();
//            Log.e("带有access_token的URL", "带有access_token的URL  " + request.url());
//            SaveLogUtils.saveInfo("token.txt", "带有access_token的URL  " + request.url());
        } else {
            return chain.proceed(request);
        }


        /**
         * 通过如下的办法曲线取到请求完成的数据
         * 原本想通过  originalResponse.body().string()
         * 去取到请求完成的数据,但是一直报错,不知道是okhttp的bug还是操作不当
         * 然后去看了okhttp的源码,找到了这个曲线方法,取到请求完成的数据后,根据特定的判断条件去判断token过期
         */
//        Response originalResponse = chain.proceed(request);
//
//        StatusVo func = getResult(originalResponse);
//        if (func != null) {
//            if (Status.TOKEN_Invalid.getCode() == func.getCode()) {
//                final Response newResponse = RefreshTokenHandler.getInstance().redirectNewToken(accessToken, request, chain);
//                if (newResponse == null) {
//                    return originalResponse;
//                }
//                return newResponse;
//            }
//        }
        return chain.proceed(request);
    }


    /**
     * 访问该接口，获取到数据，用来判断token
     *
     * @param originalResponse
     * @return
     * @throws IOException
     */
    private StatusVo getResult(Response originalResponse) throws IOException {
        ResponseBody responseBody = originalResponse.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        Charset charset = UTF8;
        MediaType contentType = responseBody.contentType();
        if (contentType != null) {
            charset = contentType.charset(UTF8);
        }
        String bodyString = buffer.clone().readString(charset);
        Gson gson = new Gson();
        StatusVo func = gson.fromJson(bodyString, StatusVo.class);
        return func;
    }

    //刷新token
//    private Response redirectNewToken(Request request, Chain chain) throws IOException {
//
//        final String access_token = refreshToken();
//
//        if (TextUtils.isEmpty(access_token)) {//获取token 失败
//            SPUtils.saveLoginBean(null);
//            gotoLogin();
//            return null;
//        } else {
//            HttpUrl.Builder newUrlBuilder = request.url()
//                    .newBuilder()
//                    .scheme(request.url().scheme())
//                    .host(request.url().host())
//                    .removeAllQueryParameters("access_token")
//                    .addQueryParameter("access_token", access_token);
//
//            Request newRequest = request.newBuilder()
//                    .method(request.method(), request.body())
//                    .url(newUrlBuilder.build())
//                    .build();
//            return chain.proceed(newRequest);
//        }
//
//    }
//
//    public synchronized String refreshToken() throws IOException {
//        final LoginBean loginBean = SPUtils.getLoginBean();
//        if (loginBean == null) {
//            return null;
//        }
//        if (TextUtils.equals(loginBean.getData().getAccessToken(), loginBean.getData().getOldAccessToken())) {
//            return loginBean.getData().getAccessToken();
//        }
//
//
//        final String refreToken = loginBean.getData().getRefreshToken();
//        final String accountId = loginBean.getData().getUser().getAccountId() + "";
//        final String userTypeId = "4";//user类型 id 学生端为4
//        final String deviceId = AndroidUtil.getDeviceID(App.mContext);
//        String deviceToken = (String) SPUtils.get(App.mContext, SPUtils.SAVE_UMENG_DEVICE_TOKEN, "");
//        if (TextUtils.isEmpty(deviceToken)) {
//            deviceToken = Config.DEVICES_DEFULT_TOKEN;
//        }
//        Log.e("刷新token接口---token:" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");
//
//        //请求刷新token 接口获取新token
//        Call<RefreshTokenBean> objectObservable = Network.createService(NetWorkService.RefreshToken.class)
//                .refresh(refreToken, accountId, userTypeId, deviceId, deviceToken);
//        RefreshTokenBean refreshTokenBean = objectObservable.execute().body();
//
//        if (refreshTokenBean == null || refreshTokenBean.getCode() != 0) {
//            Log.e("刷新token失败:---token：" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");
//            SPUtils.saveLoginBean(null);
//            gotoLogin();
//            return null;
//        }
//
//
//        loginBean.getData().setOldAccessToken(loginBean.getData().getAccessToken());
//        loginBean.getData().setRefreshToken(refreshTokenBean.getResult().getData().getRefresh_token());
//        loginBean.getData().setAccessToken(refreshTokenBean.getResult().getData().getAccess_token());
//        SPUtils.saveLoginBean(loginBean);
//        final String access_token = refreshTokenBean.getResult().getData().getAccess_token();
//
//        Log.e("刷新token成功:---token：" + loginBean.getData().getAccessToken() + "，refreToken:" + loginBean.getData().getRefreshToken() + "，请求接口:");
//        return access_token;
//
//    }

}
