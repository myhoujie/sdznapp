package com.sdzn.fzx.student.vo;

/**
 * BoxInfoBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class BoxInfoBean {


    /**
     * total : 2
     * data : [{"fullPath":"山东省济南市历下区山东大学附属中学小学","createUserId":null,"ip":"192.168.0.33","boxVersion":"0.0.1","customerSchoolName":"山东大学附属中学","createUserName":null,"updateTime":1520493972000,"type":1,"mac":"D0:53:49:58:B0:82","customerSchoolId":1,"groupName":null,"createTime":null,"id":1,"status":1},{"fullPath":"山东省济南市历下区山东大学附属中学小学","createUserId":4,"ip":"192.168.0.100","boxVersion":"0.0.1","customerSchoolName":"山东大学附属中学","createUserName":"田斌","updateTime":1519893943000,"type":1,"mac":"D2:11:49:78:B0:12","customerSchoolId":1,"groupName":null,"createTime":1517539484000,"id":5,"status":1}]
     */

    private int total;
    private DataBean data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * fullPath : 山东省济南市历下区山东大学附属中学小学
         * createUserId : null
         * ip : 192.168.0.33
         * boxVersion : 0.0.1
         * customerSchoolName : 山东大学附属中学
         * createUserName : null
         * updateTime : 1520493972000
         * type : 1
         * mac : D0:53:49:58:B0:82
         * customerSchoolId : 1
         * groupName : null
         * createTime : null
         * id : 1
         * status : 1
         */

        private String fullPath;
        private Object createUserId;
        private String ip;
        private String boxVersion;
        private String customerSchoolName;
        private Object createUserName;
        private long updateTime;
        private int type;
        private String mac;
        private int customerSchoolId;
        private Object groupName;
        private Object createTime;
        private int id;
        private int status;

        public String getFullPath() {
            return fullPath;
        }

        public void setFullPath(String fullPath) {
            this.fullPath = fullPath;
        }

        public Object getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(Object createUserId) {
            this.createUserId = createUserId;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getBoxVersion() {
            return boxVersion;
        }

        public void setBoxVersion(String boxVersion) {
            this.boxVersion = boxVersion;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public Object getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(Object createUserName) {
            this.createUserName = createUserName;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public Object getGroupName() {
            return groupName;
        }

        public void setGroupName(Object groupName) {
            this.groupName = groupName;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
