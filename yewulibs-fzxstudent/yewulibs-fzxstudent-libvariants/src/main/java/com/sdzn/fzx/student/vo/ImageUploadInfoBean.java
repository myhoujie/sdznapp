package com.sdzn.fzx.student.vo;

import android.graphics.Bitmap;

/**
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-29
 */
public class ImageUploadInfoBean {
    private String key;
    private String upToken;
    private String domain;
    private String imageStyle;
    private Bitmap mBitmap;

    public ImageUploadInfoBean(String key, String upToken, String domain, String imageStyle, Bitmap bitmap) {
        this.key = key;
        this.upToken = upToken;
        this.domain = domain;
        this.imageStyle = imageStyle;
        mBitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUpToken() {
        return upToken;
    }

    public void setUpToken(String upToken) {
        this.upToken = upToken;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getImageStyle() {
        return imageStyle;
    }

    public void setImageStyle(String imageStyle) {
        this.imageStyle = imageStyle;
    }
}
