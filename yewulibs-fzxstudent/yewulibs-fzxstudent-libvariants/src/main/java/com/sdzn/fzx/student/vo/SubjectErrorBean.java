package com.sdzn.fzx.student.vo;

import java.util.List;

public class SubjectErrorBean {

    /**
     * total : 1
     * data : [{"id":3913,"examId":"5bfb4a901cb0766fcc843bdf","examTemplateId":6,"examTemplateStyleId":63,"examTemplateStyleName":"填空题","examEmptyCount":4,"examDifficulty":2,"parentId":0,"userStudentId":5167,"userStudentName":"学生63","timeCreate":1543282925000,"timeUpdate":1543282925000,"chapterId":2932,"chapterName":"3* 雨的四季/刘湛秋","chapterNodeIdPath":"0.2888.2895.2932","chapterNodeNamePath":"第一单元>阅读>3* 雨的四季/刘湛秋","baseVolumeId":1,"baseVolumeName":"上册","baseSubjectId":1,"baseSubjectName":"语文","baseGradeId":7,"baseGradeName":"七年级","type":1,"typeReview":null,"typeReviewName":null,"model":1,"flag":0,"count":1,"examText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2932,\"chapterName\":\"3* 雨的四季/刘湛秋\",\"chapterNodeIdPath\":\"0.2888.2895.2932\",\"chapterNodeNamePath\":\"第一单元>阅读>3* 雨的四季/刘湛秋\",\"createTime\":1543195280404,\"createUserId\":233,\"createUserName\":\"王银勇\",\"customerSchoolId\":60,\"customerSchoolName\":\"时代智囊测试\",\"difficulty\":2,\"downCount\":5,\"examAnalysis\":\"解答这道题要熟知常见的修辞手法有：比喻、比拟、夸张、排比、对偶、反问、设问、互文等。其中比喻包括暗喻、明喻、借喻，比拟包括拟人和拟物。学生要明确其各自特点，<br />答案：<br />（1）比喻（2）比喻（3）排比（4）拟人\",\"examAnswer\":\"比喻 比喻 排比 拟人\",\"examOptions\":[{\"content\":\"比喻\",\"right\":true,\"seq\":1},{\"content\":\"比喻\",\"right\":true,\"seq\":2},{\"content\":\"排比\",\"right\":true,\"seq\":3},{\"content\":\"拟人\",\"right\":true,\"seq\":4}],\"examStem\":\"指出下列句子所运用的修辞手法。<br />（1）半空中似乎总挂着透明的水雾的丝帘，牵动着阳光的彩棱镜。<input index=1 class='cus-com' readonly='readonly' type='text' value=(1)><br />（2）而那萌发的叶子，简直就像起伏着一层绿茵茵的波浪。<input index=2 class='cus-com' readonly='readonly' type='text' value=(2)><br />（3）那是雨，是使人静谧、使人怀想、使人动情的秋雨啊！<input index=3 class='cus-com' readonly='readonly' type='text' value=(3)><br />（4）但这时候，雨已经化了妆，它经常变成美丽的雪花，飘然莅临人间。<input index=4 class='cus-com' readonly='readonly' type='text' value=(4)>。\",\"examTypeId\":6,\"favTime\":0,\"flagShare\":0,\"holdUserId\":233,\"holdUserName\":\"王银勇\",\"hots\":0,\"id\":\"5bfb4a901cb0766fcc843bdf\",\"label\":\"\",\"optionNumber\":4,\"paperCount\":126,\"partnerExamId\":\"fffbdc05-e80d-435f-8eaa-e6e7f9f8ff3b\",\"points\":[{\"eName\":\"chinese\",\"id\":3664,\"levelId\":2,\"name\":\"修辞手法及运用\",\"no\":\"61\",\"nodeIdPath\":\"0.3627.3663.3664\",\"parentId\":\"3663\",\"subjectId\":1}],\"score\":3,\"sourceId\":3,\"sourceName\":\"菁优网\",\"templateStyleId\":63,\"templateStyleName\":\"填空题\",\"type\":1,\"updateTime\":1543195280404,\"viewCount\":100,\"zoneIdPath\":\"0.15.223.5025\",\"zoneName\":\"高新区\"}","examList":[]}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3913
         * examId : 5bfb4a901cb0766fcc843bdf
         * examTemplateId : 6
         * examTemplateStyleId : 63
         * examTemplateStyleName : 填空题
         * examEmptyCount : 4
         * examDifficulty : 2
         * parentId : 0
         * userStudentId : 5167
         * userStudentName : 学生63
         * timeCreate : 1543282925000
         * timeUpdate : 1543282925000
         * chapterId : 2932
         * chapterName : 3* 雨的四季/刘湛秋
         * chapterNodeIdPath : 0.2888.2895.2932
         * chapterNodeNamePath : 第一单元>阅读>3* 雨的四季/刘湛秋
         * baseVolumeId : 1
         * baseVolumeName : 上册
         * baseSubjectId : 1
         * baseSubjectName : 语文
         * baseGradeId : 7
         * baseGradeName : 七年级
         * type : 1
         * typeReview : null
         * typeReviewName : null
         * model : 1
         * flag : 0
         * count : 1
         * examText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文","baseVersionId":1,"baseVersionName":"人教版（部编版）","baseVolumeId":1,"baseVolumeName":"上册","chapterId":2932,"chapterName":"3* 雨的四季/刘湛秋","chapterNodeIdPath":"0.2888.2895.2932","chapterNodeNamePath":"第一单元>阅读>3* 雨的四季/刘湛秋","createTime":1543195280404,"createUserId":233,"createUserName":"王银勇","customerSchoolId":60,"customerSchoolName":"时代智囊测试","difficulty":2,"downCount":5,"examAnalysis":"解答这道题要熟知常见的修辞手法有：比喻、比拟、夸张、排比、对偶、反问、设问、互文等。其中比喻包括暗喻、明喻、借喻，比拟包括拟人和拟物。学生要明确其各自特点，<br />答案：<br />（1）比喻（2）比喻（3）排比（4）拟人","examAnswer":"比喻 比喻 排比 拟人","examOptions":[{"content":"比喻","right":true,"seq":1},{"content":"比喻","right":true,"seq":2},{"content":"排比","right":true,"seq":3},{"content":"拟人","right":true,"seq":4}],"examStem":"指出下列句子所运用的修辞手法。<br />（1）半空中似乎总挂着透明的水雾的丝帘，牵动着阳光的彩棱镜。<input index=1 class='cus-com' readonly='readonly' type='text' value=(1)><br />（2）而那萌发的叶子，简直就像起伏着一层绿茵茵的波浪。<input index=2 class='cus-com' readonly='readonly' type='text' value=(2)><br />（3）那是雨，是使人静谧、使人怀想、使人动情的秋雨啊！<input index=3 class='cus-com' readonly='readonly' type='text' value=(3)><br />（4）但这时候，雨已经化了妆，它经常变成美丽的雪花，飘然莅临人间。<input index=4 class='cus-com' readonly='readonly' type='text' value=(4)>。","examTypeId":6,"favTime":0,"flagShare":0,"holdUserId":233,"holdUserName":"王银勇","hots":0,"id":"5bfb4a901cb0766fcc843bdf","label":"","optionNumber":4,"paperCount":126,"partnerExamId":"fffbdc05-e80d-435f-8eaa-e6e7f9f8ff3b","points":[{"eName":"chinese","id":3664,"levelId":2,"name":"修辞手法及运用","no":"61","nodeIdPath":"0.3627.3663.3664","parentId":"3663","subjectId":1}],"score":3,"sourceId":3,"sourceName":"菁优网","templateStyleId":63,"templateStyleName":"填空题","type":1,"updateTime":1543195280404,"viewCount":100,"zoneIdPath":"0.15.223.5025","zoneName":"高新区"}
         * examList : []
         */

        private int id;
        private String examId;
        private int examTemplateId;
        private int examTemplateStyleId;
        private String examTemplateStyleName;
        private int examEmptyCount;
        private int examDifficulty;
        private int parentId;
        private int userStudentId;
        private String userStudentName;
        private long timeCreate;
        private long timeUpdate;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private int baseVolumeId;
        private String baseVolumeName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseGradeId;
        private String baseGradeName;
        private int type;
        private Object typeReview;
        private Object typeReviewName;
        private int model;
        private int flag;
        private int count;
        private String examText;
        private List<DataBean> examList;
        private transient ExamText examTextVo;

        public ExamText getExamTextVo() {
            return examTextVo;
        }

        public void setExamTextVo(ExamText examTextVo) {
            this.examTextVo = examTextVo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExamId() {
            return examId;
        }

        public void setExamId(String examId) {
            this.examId = examId;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public String getExamTemplateStyleName() {
            return examTemplateStyleName;
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(int examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public int getExamDifficulty() {
            return examDifficulty;
        }

        public void setExamDifficulty(int examDifficulty) {
            this.examDifficulty = examDifficulty;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }

        public long getTimeCreate() {
            return timeCreate;
        }

        public void setTimeCreate(long timeCreate) {
            this.timeCreate = timeCreate;
        }

        public long getTimeUpdate() {
            return timeUpdate;
        }

        public void setTimeUpdate(long timeUpdate) {
            this.timeUpdate = timeUpdate;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public Object getTypeReview() {
            return typeReview;
        }

        public void setTypeReview(Object typeReview) {
            this.typeReview = typeReview;
        }

        public Object getTypeReviewName() {
            return typeReviewName;
        }

        public void setTypeReviewName(Object typeReviewName) {
            this.typeReviewName = typeReviewName;
        }

        public int getModel() {
            return model;
        }

        public void setModel(int model) {
            this.model = model;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public List<DataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<DataBean> examList) {
            this.examList = examList;
        }
    }
}
