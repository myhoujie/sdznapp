package com.sdzn.fzx.student.api.network;

import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.vo.AnalyzeSubjectBean;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.AnswerResultInfoBean;
import com.sdzn.fzx.student.vo.ArrayBean;
import com.sdzn.fzx.student.vo.BookVersionBean;
import com.sdzn.fzx.student.vo.BoxInfoBean;
import com.sdzn.fzx.student.vo.ChapterSectionBean;
import com.sdzn.fzx.student.vo.CollectResBean;
import com.sdzn.fzx.student.vo.CollectResTyepBean;
import com.sdzn.fzx.student.vo.CollectVo;
import com.sdzn.fzx.student.vo.ErrorSubjectListBean;
import com.sdzn.fzx.student.vo.ExamTypeVo;
import com.sdzn.fzx.student.vo.FileUrlBean;
import com.sdzn.fzx.student.vo.GroupAnalyzeBean;
import com.sdzn.fzx.student.vo.HotFixVo;
import com.sdzn.fzx.student.vo.KnowledgePointBean;
import com.sdzn.fzx.student.vo.LoginBean;
import com.sdzn.fzx.student.vo.QiNiuConfigBean;
import com.sdzn.fzx.student.vo.QiniuUptoken;
import com.sdzn.fzx.student.vo.RefreshTokenBean;
import com.sdzn.fzx.student.vo.ResponderData;
import com.sdzn.fzx.student.vo.RightRateBean;
import com.sdzn.fzx.student.vo.RightRateVo;
import com.sdzn.fzx.student.vo.ServerTimeVo;
import com.sdzn.fzx.student.vo.StatisticCountBean;
import com.sdzn.fzx.student.vo.StatisticsListVo;
import com.sdzn.fzx.student.vo.StatisticsScoreVo;
import com.sdzn.fzx.student.vo.StatisticsTimeVo;
import com.sdzn.fzx.student.vo.StringBean;
import com.sdzn.fzx.student.vo.StudentStatus;
import com.sdzn.fzx.student.vo.SubjectErrorBean;
import com.sdzn.fzx.student.vo.SubjectFilterBean;
import com.sdzn.fzx.student.vo.SubjectVo;
import com.sdzn.fzx.student.vo.TaskFileDetailVo;
import com.sdzn.fzx.student.vo.TaskFileVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.TeachIntelTask;
import com.sdzn.fzx.student.vo.TodayNativeVo;
import com.sdzn.fzx.student.vo.UpdateVo;
import com.sdzn.fzx.student.vo.UpgradeAnswerBean;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;
import com.sdzn.fzx.student.vo.UploadArrayVo;
import com.sdzn.fzx.student.vo.UploadPicVo;
import com.sdzn.fzx.student.vo.UserBean;
import com.sdzn.fzx.student.vo.VerifyUserNameBean;
import com.sdzn.fzx.student.vo.WrongVo;
import com.sdzn.fzx.student.vo.chatroombean.ChatOtherBean;
import com.sdzn.fzx.student.vo.chatroombean.ClassGroupingVo;
import com.sdzn.fzx.student.vo.chatroombean.DiscussionPicList;
import com.sdzn.fzx.student.vo.chatroombean.GroupListBean;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by bin_li on 2017/4/13.
 */

public class NetWorkService {

    public interface TestService {
        @POST("/auth/account/info")
        Observable<StatusVo<Object>> control();
    }

    //正式区域
    public interface RefreshToken {
        @POST(BuildConfig2.AUTH + "/login/refresh/token/student/pad")
        Call<RefreshTokenBean> refresh(@Query("refresh_token") String refreshToken, @Query("accountId") String accountId,
                                       @Query("userTypeId") String userTypeId, @Query("deviceId") String deviceId,
                                       @Query("deviceToken") String deviceToken);
    }


    public interface LoginService {
        @POST(BuildConfig2.AUTH + "/login/student/pad")
        @FormUrlEncoded
        Observable<StatusVo<LoginBean>> login(@Field("name") String name, @Field("password") String password, @Field("auth") int auth, @Field("deviceId") String deviceId, @Field("deviceType") String deviceType);

        @POST("/customer/school/box/listForAndriod")
        @FormUrlEncoded
        Observable<StatusVo<BoxInfoBean>> getBoxUrl(@Field("customerSchoolId") String customerSchoolId);

        @POST("/teach/QiniuController/getQiniuConfig")
        Observable<StatusVo<QiNiuConfigBean>> getQiNiuConfig();

    }

    public interface ForgetPswService {
        @POST(BuildConfig2.AUTH + "/login/student/idCards/validate")
        @FormUrlEncoded
        Observable<StatusVo<VerifyUserNameBean>> verityUserName(@Field("idCards") String idCards);

        @POST(BuildConfig2.AUTH + "/login/code/get")
        @FormUrlEncoded
        Observable<StatusVo<Object>> sendVerityCode(@Field("phone") String phone, @Field("type") String type);

        @POST(BuildConfig2.AUTH + "/login/code/validate")
        @FormUrlEncoded
        Observable<StatusVo<Object>> checkVerityCode(@Field("phone") String phone, @Field("code") String code);

        @POST(BuildConfig2.AUTH + "/login/student/password/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> checkVerityCode(@Field("phone") String phone, @Field("code") String code, @Field("newPassword") String newPassword, @Field("confirmPassword") String confirmPassword);

    }

    public interface MainService {
        @POST("/customer/app/version/new")
        @FormUrlEncoded
        Observable<StatusVo<UpdateVo>> checkUpdate(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/task/student/student/unfinished/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskListVo>> getUnFinishes(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/task/student/today/student/finish/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskListVo>> getFinishes(@FieldMap Map<String, String> params);

        @POST("/teach/teachStudentBook/errors/subject/statistic")
        @FormUrlEncoded
        Observable<StatusVo<WrongVo>> getWronges(@FieldMap Map<String, String> params);

        @POST("/teach/task/respondence/accuracy/statistic")
        @FormUrlEncoded
        Observable<StatusVo<RightRateVo>> getRightRates(@FieldMap Map<String, String> params);

        @POST("/upgrade/AppVersionShortController/selectLastOne")
        @FormUrlEncoded
        Observable<StatusVo<HotFixVo>> checkHotfix(@Field("programName") int programName);

        @POST("auth/AppversionController/saveStudentVersion")
        @FormUrlEncoded
        Observable<StatusVo<Object>> saveStudentVersion(@Field("studentId") int studentId, @Field("appVersion") String appVersion);
    }

    public interface StudyPackageService {

        @POST("teach/teachStudentBook/errors/subject/statistic")
        @FormUrlEncoded
        Observable<StatusVo<ErrorSubjectListBean>> subjectStatistic(@Field("userStudentId") String userStudentId, @Field("model") String model, @Field("baseLevelId") String baseLevelId);


        @POST("/teach/teachStudentBook/add/statistic")
        @FormUrlEncoded
        Observable<StatusVo<StatisticCountBean>> countStatistic(@Field("userStudentId") String userStudentId, @Field("model") String model);

        @POST("/customer/school/version/subject/get")
        @FormUrlEncoded
        Observable<StatusVo<SubjectFilterBean>> getSubjectFilterList(@Field("baseGradeId") String baseGradeId, @Field("flag") String flag);

        @POST("/teach/resource/subject/structure/list?")
        @FormUrlEncoded
        Observable<StatusVo<CollectResTyepBean>> getCollectTypeList(@Field("baseSubjectId") String baseSubjectId);


        @POST("/teach/teachStudentResource/resource/collect/list")
        @FormUrlEncoded
        Observable<StatusVo<CollectResBean>> getCollectResList(@Field("userStudentId") String userStudentId,
                                                               @Field("examTemplateStyleId") String examTemplateStyleId,
                                                               @Field("baseSubjectId") String baseSubjectId,
                                                               @Field("timeType") String timeType,
                                                               @Field("chapterName") String chapterName);

        @POST("/teach/teachStudentResource/resource/collect/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> removeCollectRes(@Field("resourceCollectId") String id);

    }

    public interface TaskService {

        @POST("/customer/school/version/subject/get")
        @FormUrlEncoded
        Observable<StatusVo<SubjectVo>> getSubjects(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/task/student/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskListVo>> getTasks(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/task/student/update/task/isRead")
        @FormUrlEncoded
        Observable<StatusVo<Object>> updateStatus(@FieldMap Map<String, String> params);

        @POST("/teach/student/lesson/resource/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskFileVo>> getTaskFiles(@FieldMap Map<String, String> params);

        @POST("/teach/student/lesson/resource/detail/list")
        @FormUrlEncoded
        Observable<StatusVo<TaskFileDetailVo>> getTaskFileDetail(@FieldMap Map<String, String> params);

        @POST("/teach/teachStudentResource/resource/collect/save")
        @FormUrlEncoded
        Observable<StatusVo<CollectVo>> collect(@FieldMap Map<String, String> params);

        @POST("/teach/teachStudentResource/resource/collect/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> delete(@FieldMap Map<String, String> params);
    }

    public interface AnswerService {

        @Deprecated
        @POST(BuildConfig2.AUTH + "/login/get/time")
        @FormUrlEncoded
        Observable<StatusVo<ServerTimeVo>> getServerTime(@FieldMap Map<String, String> params);

        @POST("teach/student/exam/get/time")
        @FormUrlEncoded
        Observable<StatusVo<ServerTimeVo>> getServerTime2(@Field("lessonTaskStudentId") int lessonTaskStudentId);

        @POST("/teach/student/exam/exam/picture/save")
        @Multipart
        Observable<StatusVo<UploadPicVo>> uploadPic(@PartMap Map<String, RequestBody> files);

        @POST("/teach/task/respondence/score/statistic")
        @FormUrlEncoded
        Observable<StatusVo<StatisticsScoreVo>> getStatisticScore(@FieldMap Map<String, String> params);

        @POST("/teach/task/respondence/time/statistic")
        @FormUrlEncoded
        Observable<StatusVo<StatisticsTimeVo>> getStatisticTime(@FieldMap Map<String, String> params);

        @POST("/teach/lesson/answer/exam/analyse")
        @FormUrlEncoded
        Observable<StatusVo<StatisticsListVo>> getStatisticList(@FieldMap Map<String, String> params);

        @POST("/teach/student/exam/list")
        @FormUrlEncoded
        Observable<StatusVo<AnswerListBean>> getAnswers(@FieldMap Map<String, String> params);

        @POST("/teach/QiniuController/getUptoken")
        @FormUrlEncoded
        Observable<QiniuUptoken> getQiNiuUptoken(@Field("key") String key);//获取七牛云token接口
    }

    /**
     * 改原生接口
     */
    public interface AnswerNativeService {
        @POST("/teach/student/exam/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> saveExam(@FieldMap Map<String, Object> params);

        @POST("/teach/student/exam/book/collect/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> deleteCollect(@Field("studentBookId") long id);

        @POST("/teach/student/exam/book/collect/save")
        @FormUrlEncoded
        Observable<StatusVo<Map<String, Long>>> saveCollect(@Field("id") long id);

        @POST("/teach/teachStudentResource/resource/collect/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> deleteResCollect(@Field("resourceCollectId") long id);

        @POST("/teach/teachStudentResource/resource/collect/insert")
        @FormUrlEncoded
        Observable<StatusVo<Map<String, Long>>> saveResCollect(@Field("id") long id,
                                                               @Field("lessonId") long lessonId,
                                                               @Field("lessonName") String lessonName);

        /**
         * 已作答试题获取试题内容
         */
        @POST("/teach/student/exam/info")
        @FormUrlEncoded
        Observable<StatusVo<AnswerResultInfoBean>> loadExamInfo(
                @FieldMap Map<String, String> params
        );

        @POST("teach/student/exam/submit")
        @FormUrlEncoded
        Observable<StatusVo<Object>> submitAnswer(@FieldMap Map<String, Object> params);
    }

    public interface SavePhoneService {
        @POST("/auth/user/student/phone/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> SavePhone(@Field("phone") String phone);

        @POST("/auth/phone/code/get")
        @FormUrlEncoded
        Observable<StatusVo<Object>> sendVerityCode(@Field("phone") String phone);
    }

    public interface ChangePasswordService {
        @POST("/auth/account/password/update")
        @FormUrlEncoded
        Observable<StatusVo<Object>> ChangePassword(@Field("oldPassword") String oldPassword, @Field("newPassword") String newPassword, @Field("confirmPassword") String confirmPassword);
    }

    public interface StudyStatstic {
        @POST("/teach/task/respondence/condition/accuracy/statistic")
        @FormUrlEncoded
        Observable<StatusVo<AnalyzeSubjectBean>> accuracyStatistic(@Field("userStudentId") String userStudentId,
                                                                   @Field("classId") String classId,
                                                                   @Field("timeType") String timeType);


        @POST("/teach/task/subject/right/rate")
        @FormUrlEncoded
        Observable<StatusVo<RightRateBean>> rightRateStatistic(@FieldMap Map<String, String> params);


        @POST("/auth/class/group/point/total/list")
        @FormUrlEncoded
        Observable<StatusVo<GroupAnalyzeBean>> groupAnalyze(@Field("subjectId") String subjectId,
                                                            @Field("classId") String classId,
                                                            @Field("timeStyle") String timeType);
    }

    public interface HeadUploadService {
        @POST("/auth/user/student/head/upload")
        @Multipart
        Observable<StatusVo<Object>> uploadHead(@Part("file\"; filename=\"avatar.jpg") RequestBody file);

    }

    public interface GetUserInfoService {
        @GET("auth/account/info")
        Observable<StatusVo<UserBean>> getUserInfo();


    }

    public interface updateService {
        @Multipart
        @POST("/teach/student/exam/picture/array/save")
        Observable<StatusVo<UploadArrayVo>> uploadHead(@Part() List<MultipartBody.Part> arrayFile);

    }

    public interface ExamService {
        @POST("/teach/exam/template/list")
        @FormUrlEncoded
        Observable<StatusVo<ExamTypeVo>> getExamList(@Field("baseLevelId") String baseLevelId, @Field("baseSubjectId") String baseSubjectId);
    }

    //    public interface UpdateService {
//        @GET("/app.info")
//        Observable<StatusVo<VersionCode>> getVersion();
//    }

///#################################原生接口

    /**
     * 获取错题列表
     */
    public interface SubjectErrorList {
        @POST("/teach/teachStudentBook/subject/list")
        @FormUrlEncoded
        Observable<StatusVo<SubjectErrorBean>> SubjectErrorList(@FieldMap Map<String, String> params);

    }

    /**
     * 删除错题
     */
    public interface DelExam {
        @POST("/teach/student/exam/book/error/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> DelExam(@Field("studentBookId") String studentBookId);

    }

    /**
     * 获取今日试题列表
     */
    public interface TodayErrorList {
        @POST("/teach/teachStudentBook/today/subject/list")
        @FormUrlEncoded
        Observable<StatusVo<TodayNativeVo>> TodayErrorList(@FieldMap Map<String, String> params);

    }

    /**
     * 试题收藏
     */
    public interface CollectExam {
        @POST("/teach/student/exam/book/collect/save")
        @FormUrlEncoded
        Observable<StatusVo<Object>> DelExam(@Field("examId") String studentBookId);
    }

    /**
     * 试题取消收藏
     */
    public interface CancleCollectExam {
        @POST("/teach/student/exam/book/collect/delete")
        @FormUrlEncoded
        Observable<StatusVo<Object>> DelExam(@Field("examId") String studentBookId);
    }

    public interface RuanYun {

        /**
         * 学生端-显示学生端某个学生某个学科阶段的学情分析
         *
         * @param courseId  学科id
         * @param startTime 开始时间,格式为yyyy-MM-dd,如果type有值，则按照type查询
         * @param endTime   结束时间,格式为yyyy-MM-dd,如果type有值，则按照type查询
         * @param studentId 学生id
         * @param type      如果查询一周内的数据，传1；如果查询一个月的数据，传2
         */
        @POST("/analysis/StudentAnalysisController/showStudentAssessementHistory")
        @FormUrlEncoded
        Observable<StatusVo<StringBean>> showStudentAssessementHistory(@Field("courseId") int courseId,
                                                                       @Field("startTime") String startTime,
                                                                       @Field("endTime") String endTime,
                                                                       @Field("studentId") int studentId,
                                                                       @Field("type") int type);

        /**
         * 学生端-展示单次任务分析结果
         *
         * @param taskStudentId 学生任务id,对应teach_lesson_task_student的id
         */
        @POST("/analysis/StudentAnalysisController/showStudentTaskAnalysis")
        @FormUrlEncoded
        Observable<StatusVo<StringBean>> showStudentTaskAnalysis(@Field("taskStudentId") int taskStudentId);

        /**
         * 学生端-展示单次提升练习分析结果
         *
         * @param examResultId 学生测评结果id软云
         */
        @POST("/analysis/StudentAnalysisController/showStudentPromoteAnalysis")
        @FormUrlEncoded
        Observable<StatusVo<StringBean>> showStudentPromoteAnalysis(@Field("examResultId") String examResultId);

        /**
         * 根据练习结果生成练习id
         *
         * @param examResultId  练习结果ID
         * @param questionCount 题目数量
         */
        @POST("/analysis/StudentTaskExerciseController/getPromotionExercise")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getPromotionExercise(@Field("examResultId") String examResultId,
                                                          @Field("questionCount") int questionCount);

        /**
         * 获取学生某个学科的知识点推荐练习接口
         *
         * @param subjectId        学科id
         * @param questionCount    题目数量
         * @param knowledgePointId 软云知识点id
         */
        @POST("/teach/ruanyun/getKnowledgePointExercise")
        @FormUrlEncoded
        Observable<StatusVo<UpgradeAnswerBean>> getKnowledgePointExercise(@Field("baseSubjectId") long subjectId,
                                                                          @Field("questionCount") int questionCount,
                                                                          @Field("knowledgePointId") long knowledgePointId);

        /**
         * 通过章节获取学生某个学科的推荐练习接口
         *
         * @param subjectId        学科id
         * @param questionCount    题目数量
         * @param courseMappingId  软云教材版本id
         * @param chapterSectionId 软云章节id
         */
        @POST("/teach/ruanyun/getExerciseByChapterSection")
        @FormUrlEncoded
        Observable<StatusVo<UpgradeAnswerBean>> getExerciseByChapterSection(@Field("baseSubjectId") long subjectId,
                                                                            @Field("questionCount") int questionCount,
                                                                            @Field("courseMappingId") long courseMappingId,
                                                                            @Field("chapterSectionId") long chapterSectionId);

        /**
         * 软云智能推题的学生作答后提交接口
         *
         * @param subjectId        学科id
         * @param questionCount    题目数量
         * @param teacherExamId    软云题集id
         * @param studentReplyExam 学生作答信息
         */
        @POST("/teach/ruanyun/submitRuanYunExercise")
        @FormUrlEncoded
        Observable<StatusVo<Object>> submitRuanYunExercise(@Field("baseSubjectId") long subjectId,
                                                           @Field("questionCount") int questionCount,
                                                           @Field("teacherExamId") String teacherExamId,
                                                           @Field("studentReplyExam") String studentReplyExam);

        /**
         * 创建提升练习
         *
         * @param subjectId     学科id
         * @param questionCount 题目数量
         * @param examResultId  练习结果id
         */
        @POST("/teach/ruanyun/createPromotionExercise")
        @FormUrlEncoded
        Observable<StatusVo<Object>> createPromotionExercise(@Field("baseSubjectId") long subjectId,
                                                             @Field("questionCount") int questionCount,
                                                             @Field("examResultId") long examResultId);

        /**
         * 查询练习记录
         *
         * @param subjectId 学科id
         * @param offset    开始任务序号, 从1开始
         * @param rows      查多少个
         */
        @POST("/teach/ruanyun/queryIntelExerciseTask")
        @FormUrlEncoded
        Observable<StatusVo<ArrayBean<TeachIntelTask>>> queryIntelExerciseTask(@Field("baseSubjectId") long subjectId,
                                                                               @Field("offset") int offset,
                                                                               @Field("rows") int rows);

        /**
         * 查询软云已做任务详情
         *
         * @param intelTaskId 练习记录id
         */
        @POST("/teach/ruanyun/queryIntelExerciseTaskDetail")
        @FormUrlEncoded
        Observable<StatusVo<ArrayBean<UpgradeAnswerResultBean>>> queryIntelExerciseTaskDetail(@Field("intelTaskId") long intelTaskId);

        /**
         * 同步练习——设置教材版本和科别
         *
         * @param subjectId 学科id
         */
        @POST("/teach/ruanyun/queryBookVersionAndCourseMapping")
        @FormUrlEncoded
        Observable<StatusVo<ArrayBean<BookVersionBean>>> queryBookVersion(@Field("baseSubjectId") long subjectId);

        /**
         * 知识点练习——查询章节列表
         *
         * @param baseSubjectId 学科id
         */
        @POST("/teach/PointController/student/list")
        @FormUrlEncoded
        Observable<StatusVo<ArrayBean<KnowledgePointBean>>> queryKnowledgePointScoreList(@Field("baseSubjectId") long baseSubjectId);


        /**
         * 同步练习——查询章节列表
         *
         * @param courseMappingId 教材id
         */
        @POST("/teach/ruanyun/queryChapterSection")
        @FormUrlEncoded
        Observable<StatusVo<ArrayBean<ChapterSectionBean>>> queryChapterSection(@Field("courseMappingId") long courseMappingId);

        /**
         * 智能练习—    题目
         *
         */
        @POST("/teach/ruanyun/getIntelligentExercise")
        @FormUrlEncoded
        Observable<StatusVo<UpgradeAnswerBean>> queryIntelligentExercise(@Field("baseSubjectId") long baseSubjectId, @Field("questionCount") long questionCount);
    }

    public interface RequestFilePath {
        @POST("/schoolbox/resource/access")
        @FormUrlEncoded
        Observable<StatusVo<FileUrlBean>> getFilePath(@Field("resourceId") String resourceId, @Field("convertMediaPath") String address);
    }

    public interface ChatService {
        @POST("/group/list/GroupChatResult")
        @FormUrlEncoded
        Observable<StatusVo<GroupListBean>> getChatResult(@FieldMap Map<String, Object> params);

        @POST("/group/insert/chatTaskPic/result")
        @FormUrlEncoded
        Observable<StatusVo<Object>> insertChatTaskPic(@FieldMap Map<String, Object> params);

        @POST("/group/list/chatTaskPic/result")
        @FormUrlEncoded
        Observable<StatusVo<DiscussionPicList>> getChatTaskPic(@FieldMap Map<String, Object> params);

        @POST("group/delete/chatTaskPic/result")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getDeleteResult(@Field("id") String Id);

        @POST("group/list/result/GroupChatTask")
        @FormUrlEncoded
        Observable<StatusVo<ChatOtherBean>> getGroupChatTask(@Field("groupChatTaskId") String groupChatTaskId);

        @POST("/auth/class/group/student/list")
        @FormUrlEncoded
        Observable<StatusVo<ClassGroupingVo>> GetClassGrouping(@Field("classId") String classId);

        @POST("/group/isRead/GroupChat")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getChatIsRead(@Field("groupChatTaskId") String id);

    }

    public interface ToolsService {

//        @POST("inclass/student/inclass/stauts")
//        @FormUrlEncoded
//        Observable<StatusVo<Object>> getGoClassStatus(@Field("studentId") String studentId);

        @POST("inclass/student/synstauts")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getGoClassStatus(@Field("studentId") String studentId, @Field("classId") String classId);

//        @POST("inclass/student/screen/stauts")
//        @FormUrlEncoded
//        Observable<StatusVo<Object>> getLockStatus(@Field("studentId") String studentId);

        @POST("inclass/student/synstauts/screen")
        @FormUrlEncoded
        Observable<StatusVo<Object>> getLockStatus(@Field("studentId") String studentId, @Field("classId") String classId);

        @POST("inclass/class/responder")
        @FormUrlEncoded
        Observable<StatusVo<ResponderData>> getResponder(@FieldMap Map<String, Object> params);

        @POST("inclass/student/stauts")
        @FormUrlEncoded
        Observable<StatusVo<StudentStatus>> getStudentStatus(@Field("studentId") String studentId, @Field("classId") String classId);
    }
}
