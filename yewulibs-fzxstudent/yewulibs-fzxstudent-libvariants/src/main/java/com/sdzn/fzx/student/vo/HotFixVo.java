package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/6/13 0013.
 */

public class HotFixVo {
    /**
     * data : {"id":13,"appVersionNum":"1.5","appVersionName":"1.5版本","appVersionDesc":"阿斯顿发的说法大沙发大师傅三大范德萨发大水发","appName":null,"appPath":"http://115.28.91.19:9001/resource/image/2018/04/6e991a4a3f21406f896855c3366a9a7a.png","mustUpgrade":1,"status":0,"boxPath":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 13
         * appVersionNum : 1.5
         * appVersionName : 1.5版本
         * appVersionDesc : 阿斯顿发的说法大沙发大师傅三大范德萨发大水发
         * appName : null
         * appPath : http://115.28.91.19:9001/resource/image/2018/04/6e991a4a3f21406f896855c3366a9a7a.png
         * mustUpgrade : 1
         * status : 0
         * boxPath : null
         */

        private int id;
        private int appVersionNum;
        private String appVersionName;
        private String appVersionDesc;
        private Object appName;
        private String appPath;
        private int mustUpgrade;
        private int status;
        private String boxPath;
        private String tinkerId;

        public String getTinkerId() {
            return tinkerId;
        }

        public void setTinkerId(String tinkerId) {
            this.tinkerId = tinkerId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAppVersionNum() {
            return appVersionNum;
        }

        public void setAppVersionNum(int appVersionNum) {
            this.appVersionNum = appVersionNum;
        }

        public String getAppVersionName() {
            return appVersionName;
        }

        public void setAppVersionName(String appVersionName) {
            this.appVersionName = appVersionName;
        }

        public String getAppVersionDesc() {
            return appVersionDesc;
        }

        public void setAppVersionDesc(String appVersionDesc) {
            this.appVersionDesc = appVersionDesc;
        }

        public Object getAppName() {
            return appName;
        }

        public void setAppName(Object appName) {
            this.appName = appName;
        }

        public String getAppPath() {
            return appPath;
        }

        public void setAppPath(String appPath) {
            this.appPath = appPath;
        }

        public int getMustUpgrade() {
            return mustUpgrade;
        }

        public void setMustUpgrade(int mustUpgrade) {
            this.mustUpgrade = mustUpgrade;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getBoxPath() {
            return boxPath;
        }

        public void setBoxPath(String boxPath) {
            this.boxPath = boxPath;
        }
    }
}
