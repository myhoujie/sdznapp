package com.sdzn.fzx.student.utils;

import android.database.sqlite.SQLiteDatabase;

import com.sdzn.fzx.student.dao.DaoMaster;
import com.sdzn.fzx.student.dao.DaoSession;
import com.sdzn.fzx.student.libutils.app.App2;

public class CommonDaoUtils {

    private DaoSession daoSession;

    private static CommonDaoUtils sInstance;

    public static CommonDaoUtils getInstance() {
        if (sInstance == null) {
            sInstance = new CommonDaoUtils();
        }
        return sInstance;
    }

    private CommonDaoUtils() {
    }

    public void init() {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(App2.get(), "basemvp.db");
        SQLiteDatabase writableDatabase = devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(writableDatabase);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            init();
        }
        return daoSession;
    }

}
