package com.sdzn.fzx.student.vo.chatroombean;

import java.util.List;

public class DiscussionPicList {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 6
         * picUrl : http://csfile.fuzhuxian.com/7249653d-1ccd-4c30-b970-c4118983d157.jpg
         * groupChatResultId : 33
         * userStudentId : 4596
         * createTime : 2019-08-26T07:35:56.000+0000
         * groupId : 253
         * groupName : 测试1组
         * groupChatTaskId : 21
         * userStudentName : 测试15
         */

        private int id;
        private String picUrl;
        private String groupChatResultId;
        private int userStudentId;
        private String createTime;
        private String groupId;
        private String groupName;
        private String groupChatTaskId;
        private String userStudentName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }


        public int getUserStudentId() {
            return userStudentId;
        }

        public void setUserStudentId(int userStudentId) {
            this.userStudentId = userStudentId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }



        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupChatResultId() {
            return groupChatResultId;
        }

        public void setGroupChatResultId(String groupChatResultId) {
            this.groupChatResultId = groupChatResultId;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupChatTaskId() {
            return groupChatTaskId;
        }

        public void setGroupChatTaskId(String groupChatTaskId) {
            this.groupChatTaskId = groupChatTaskId;
        }

        public String getUserStudentName() {
            return userStudentName;
        }

        public void setUserStudentName(String userStudentName) {
            this.userStudentName = userStudentName;
        }
    }
}
