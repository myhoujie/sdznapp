package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/4/9 0009.
 */

public class CollectVo {
    /**
     * data : 445
     */

    private int data;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
