package com.sdzn.fzx.student.dao.controller;

import com.sdzn.fzx.student.dao.SearchHistory;
import com.sdzn.fzx.student.dao.SearchHistoryDao;
import com.sdzn.fzx.student.utils.CommonDaoUtils;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Date;
import java.util.List;

/**
 * 搜索
 * @author wangchunxiao
 * @date 2018/1/17
 */
public class SearchController {
    public static List<SearchHistory> queryByTime() {
        return CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().queryBuilder().orderDesc(SearchHistoryDao.Properties.Date).list();
    }

    public static List<SearchHistory> queryByTime(String condition) {
        return CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().queryBuilder().
                where(SearchHistoryDao.Properties.SearchStr.eq(condition)).orderDesc(SearchHistoryDao.Properties.Date).list();
    }

    public static List<SearchHistory> queryByTime(String scene, int subject) {
        return CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().queryBuilder().
                where(SearchHistoryDao.Properties.Subject.eq(subject), SearchHistoryDao.Properties.Scene.eq(scene)).orderDesc(SearchHistoryDao.Properties.Date).list();
    }

    public static SearchHistory queryBySearchStr(String searchStr) {
        QueryBuilder<SearchHistory> searchHistoryQueryBuilder = CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().queryBuilder();
        searchHistoryQueryBuilder.where(SearchHistoryDao.Properties.SearchStr.eq(searchStr));
        return searchHistoryQueryBuilder.build().unique();
    }

    public static SearchHistory queryBySearchStr(String searchStr, int subject) {
        return CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().queryBuilder()
                .where(SearchHistoryDao.Properties.SearchStr.eq(searchStr), SearchHistoryDao.Properties.Subject.eq(String.valueOf(subject)))
                .build().unique();
    }

    /**
     * 存储搜索字段
     *
     * @param keyword
     * @return
     */
    public static SearchHistory saveHistory(String scene, String keyword, int subject) {
        SearchHistory searchHistory = queryBySearchStr(keyword, subject);
        if (searchHistory != null) {
            deleteHistory(searchHistory);
        }

        // 最大数量10条,超出删除最早一条
        List<SearchHistory> searchHistories = queryByTime(scene, subject);
        if (searchHistories.size() > 9) {
            deleteHistory(searchHistories.get(searchHistories.size() - 1));
        }
        SearchHistory history = new SearchHistory();
        history.setSearchStr(keyword);
        history.setDate(new Date());
        history.setSubject(subject);
        history.setScene(scene);
        Long id = CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().insert(history);
        history.setId(id);
        return history;
    }

    /**
     * 删除搜索字段
     *
     * @param history
     */
    public static void deleteHistory(SearchHistory history) {
        CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().delete(history);
    }

    /**
     * 清除所有搜索历史
     */
    public static void clearHistory(List<SearchHistory> searchHistories) {
        for (SearchHistory searchHistory : searchHistories) {
            CommonDaoUtils.getInstance().getDaoSession().getSearchHistoryDao().delete(searchHistory);
        }
    }
}
