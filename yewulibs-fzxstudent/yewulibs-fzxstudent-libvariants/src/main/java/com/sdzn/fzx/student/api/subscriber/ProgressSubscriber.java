package com.sdzn.fzx.student.api.subscriber;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import com.blankj.utilcode.util.SPUtils;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.network.Status;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import rx.Subscriber;

/**
 * 描述：用于在Http请求开始时，自动显示一个ProgressDialog，在Http请求结束是，关闭ProgressDialog。调用者自己对请求数据进行处理
 * -
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public class ProgressSubscriber<T> extends Subscriber<T> implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {

    private SubscriberListener mSubscriberListener;
    private ProgressDialogManager mProgressDialogManager;

    private boolean showProgress;
    private String progressMsg;
    private boolean showUnConnectedToast;

    public ProgressSubscriber(SubscriberListener mSubscriberListener, Activity activity) {
        this.mSubscriberListener = mSubscriberListener;
        this.showProgress = true;
        this.showUnConnectedToast = true;
        if (mProgressDialogManager == null) {
            if (activity != null) {
                mProgressDialogManager = new ProgressDialogManager(activity);
                try {
                    mProgressDialogManager.getProgressDialog().setOnDismissListener(this);
                    mProgressDialogManager.getProgressDialog().setOnCancelListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ProgressSubscriber(SubscriberListener mSubscriberListener, Activity activity, boolean showProgress) {
        this.mSubscriberListener = mSubscriberListener;
        this.showProgress = showProgress;
        this.showUnConnectedToast = true;
        if (mProgressDialogManager == null) {
            if (activity != null) {
                mProgressDialogManager = new ProgressDialogManager(activity);
                try {
                    mProgressDialogManager.getProgressDialog().setOnDismissListener(this);
                    mProgressDialogManager.getProgressDialog().setOnCancelListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ProgressSubscriber(SubscriberListener mSubscriberListener, Activity activity, boolean showProgress, boolean cancleable, boolean showUnConnectedToast, String progressMsg) {
        this.mSubscriberListener = mSubscriberListener;
        this.showProgress = showProgress;
        this.progressMsg = progressMsg;
        this.showUnConnectedToast = showUnConnectedToast;
        if (mProgressDialogManager == null) {
            if (activity != null) {
                mProgressDialogManager = new ProgressDialogManager(activity);
                try {
                    mProgressDialogManager.getProgressDialog().setOnDismissListener(this);
                    mProgressDialogManager.getProgressDialog().setOnCancelListener(this);
                    mProgressDialogManager.getProgressDialog().setCancelable(cancleable);
                    mProgressDialogManager.getProgressDialog().setCanceledOnTouchOutside(cancleable);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ProgressSubscriber(SubscriberListener mSubscriberListener, Activity activity, boolean showProgress, boolean showUnConnectedToast) {
        this.mSubscriberListener = mSubscriberListener;
        this.showProgress = showProgress;
        this.showUnConnectedToast = showUnConnectedToast;
        if (mProgressDialogManager == null) {
            if (activity != null) {
                mProgressDialogManager = new ProgressDialogManager(activity);
                try {
                    mProgressDialogManager.getProgressDialog().setOnDismissListener(this);
                    mProgressDialogManager.getProgressDialog().setOnCancelListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showProgressDialog() {
        if (mProgressDialogManager != null && !mProgressDialogManager.isShow()) {
            if (TextUtils.isEmpty(progressMsg)) {
                mProgressDialogManager.showWaiteDialog();
            } else {
                mProgressDialogManager.showWaiteDialog(progressMsg);
            }
        }
    }

    private void dismissProgressDialog() {
        if (mProgressDialogManager != null && mProgressDialogManager.isShow()) {
            mProgressDialogManager.cancelWaiteDialog();
        }
    }

    /**
     * 订阅开始时调用
     * 显示ProgressDialog
     */
    @Override
    public void onStart() {
        if (showProgress) {
            showProgressDialog();
        }
    }

    /**
     * 完成，隐藏ProgressDialog
     */
    @Override
    public void onCompleted() {
        dismissProgressDialog();
        mSubscriberListener.onCompleted();
    }

    /**
     * 对错误进行统一处理
     * 隐藏ProgressDialog
     *
     * @param e
     */
    @Override
    public void onError(Throwable e) {
        try {
            dismissProgressDialog();
            if (e instanceof ApiException) {
                ApiException apiException = (ApiException) e;
                if (apiException.getStatus() != null) {
                    if (apiException.getStatus().getCode() == Status.TOKEN_TIME_OUT.getCode()) {
//                        StudentSPUtils.savetoken("");
                       SPUtils.getInstance().put(StudentSPUtils.SP_TOKEN,"");
                        ToastUtil.showShortlToast(apiException.getStatus().getMsg());
                    }
                    mSubscriberListener.onError(apiException);
                    return;
                }
            } else if (e instanceof SocketTimeoutException || e instanceof ConnectException || e instanceof UnknownHostException) {
                if (showUnConnectedToast) {
                    ToastUtil.showShortlToast("网段不可用或服务器异常");
                } else {
                    try {
                        mSubscriberListener.onError(new ApiException(Status.SYSTEM_ERROR.getCode()));
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                return;
            }
            try {
                mSubscriberListener.onError(e);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            try {
                mSubscriberListener.onError(e1);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        mSubscriberListener.onCompleted();
    }

    /**
     * 将onNext方法中的返回结果交给Activity或Fragment自己处理
     *
     * @param t 创建Subscriber时的泛型类型
     */
    @Override
    public void onNext(T t) {
        dismissProgressDialog();
        if (mSubscriberListener != null) {
            mSubscriberListener.onNext(t);
        }
    }

    /**
     * 取消ProgressDialog的时候，取消对observable的订阅，同时也取消了http请求
     */
    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.isUnsubscribed()) {
            this.unsubscribe();
        }
        mSubscriberListener.onCompleted();
    }

    /**
     * 取消ProgressDialog的时候，取消对observable的订阅，同时也取消了http请求
     */
    @Override
    public void onCancel(DialogInterface dialogInterface) {
        if (!this.isUnsubscribed()) {
            this.unsubscribe();
        }
        mSubscriberListener.onCompleted();
    }
}