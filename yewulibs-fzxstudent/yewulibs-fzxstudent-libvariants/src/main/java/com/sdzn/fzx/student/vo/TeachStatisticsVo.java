package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/3/8
 * 修改单号：
 * 修改内容:
 */
public class TeachStatisticsVo {

    /**
     * wholeLevel : C
     * lineChart : [{"date":"2019-03-01","score":"B"},{"date":"2019-03-02","score":"C"},{"date":"2019-03-03","score":"C"},{"date":"2019-03-04","score":"C"},{"date":"2019-03-05","score":"D"},{"date":"2019-03-07","score":"E"}]
     * pieChart : {"aplus":{"accuracyForAplus":"0.0%","pointNamesForAplus":[]},"a":{"pointNamesForA":["有理数"],"accuracyForA":"28.57%"},"b":{"pointNamesForB":["数轴"],"accuracyForB":"28.57%"},"c":{"pointNamesForC":[],"accuracyForC":"0.0%"},"d":{"pointNamesForD":["正数和负数"],"accuracyForD":"42.86%"},"e":{"pointNamesForE":[],"accuracyForE":"0.0%"}}
     */

    private String wholeLevel;
    private PieChartBean pieChart;
    private List<LineChartBean> lineChart;

    public String getWholeLevel() {
        return wholeLevel;
    }

    public void setWholeLevel(String wholeLevel) {
        this.wholeLevel = wholeLevel;
    }

    public PieChartBean getPieChart() {
        return pieChart;
    }

    public void setPieChart(PieChartBean pieChart) {
        this.pieChart = pieChart;
    }

    public List<LineChartBean> getLineChart() {
        return lineChart;
    }

    public void setLineChart(List<LineChartBean> lineChart) {
        this.lineChart = lineChart;
    }

    public static class PieChartBean {
        /**
         * aplus : {"accuracyForAplus":"0.0%","pointNamesForAplus":[]}
         * a : {"pointNamesForA":["有理数"],"accuracyForA":"28.57%"}
         * b : {"pointNamesForB":["数轴"],"accuracyForB":"28.57%"}
         * c : {"pointNamesForC":[],"accuracyForC":"0.0%"}
         * d : {"pointNamesForD":["正数和负数"],"accuracyForD":"42.86%"}
         * e : {"pointNamesForE":[],"accuracyForE":"0.0%"}
         */

        private AplusBean aplus;
        private ABean a;
        private BBean b;
        private CBean c;
        private DBean d;
        private EBean e;

        public AplusBean getAplus() {
            return aplus;
        }

        public void setAplus(AplusBean aplus) {
            this.aplus = aplus;
        }

        public ABean getA() {
            return a;
        }

        public void setA(ABean a) {
            this.a = a;
        }

        public BBean getB() {
            return b;
        }

        public void setB(BBean b) {
            this.b = b;
        }

        public CBean getC() {
            return c;
        }

        public void setC(CBean c) {
            this.c = c;
        }

        public DBean getD() {
            return d;
        }

        public void setD(DBean d) {
            this.d = d;
        }

        public EBean getE() {
            return e;
        }

        public void setE(EBean e) {
            this.e = e;
        }

        public static class AplusBean {
            /**
             * accuracyForAplus : 0.0%
             * pointNamesForAplus : []
             */

            private String accuracyForAplus;
            private List<String> pointNamesForAplus;

            public String getAccuracyForAplus() {
                return accuracyForAplus;
            }

            public void setAccuracyForAplus(String accuracyForAplus) {
                this.accuracyForAplus = accuracyForAplus;
            }

            public List<String> getPointNamesForAplus() {
                return pointNamesForAplus;
            }

            public void setPointNamesForAplus(List<String> pointNamesForAplus) {
                this.pointNamesForAplus = pointNamesForAplus;
            }
        }

        public static class ABean {
            /**
             * pointNamesForA : ["有理数"]
             * accuracyForA : 28.57%
             */

            private String accuracyForA;
            private List<String> pointNamesForA;

            public String getAccuracyForA() {
                return accuracyForA;
            }

            public void setAccuracyForA(String accuracyForA) {
                this.accuracyForA = accuracyForA;
            }

            public List<String> getPointNamesForA() {
                return pointNamesForA;
            }

            public void setPointNamesForA(List<String> pointNamesForA) {
                this.pointNamesForA = pointNamesForA;
            }
        }

        public static class BBean {
            /**
             * pointNamesForB : ["数轴"]
             * accuracyForB : 28.57%
             */

            private String accuracyForB;
            private List<String> pointNamesForB;

            public String getAccuracyForB() {
                return accuracyForB;
            }

            public void setAccuracyForB(String accuracyForB) {
                this.accuracyForB = accuracyForB;
            }

            public List<String> getPointNamesForB() {
                return pointNamesForB;
            }

            public void setPointNamesForB(List<String> pointNamesForB) {
                this.pointNamesForB = pointNamesForB;
            }
        }

        public static class CBean {
            /**
             * pointNamesForC : []
             * accuracyForC : 0.0%
             */

            private String accuracyForC;
            private List<String> pointNamesForC;

            public String getAccuracyForC() {
                return accuracyForC;
            }

            public void setAccuracyForC(String accuracyForC) {
                this.accuracyForC = accuracyForC;
            }

            public List<String> getPointNamesForC() {
                return pointNamesForC;
            }

            public void setPointNamesForC(List<String> pointNamesForC) {
                this.pointNamesForC = pointNamesForC;
            }
        }

        public static class DBean {
            /**
             * pointNamesForD : ["正数和负数"]
             * accuracyForD : 42.86%
             */

            private String accuracyForD;
            private List<String> pointNamesForD;

            public String getAccuracyForD() {
                return accuracyForD;
            }

            public void setAccuracyForD(String accuracyForD) {
                this.accuracyForD = accuracyForD;
            }

            public List<String> getPointNamesForD() {
                return pointNamesForD;
            }

            public void setPointNamesForD(List<String> pointNamesForD) {
                this.pointNamesForD = pointNamesForD;
            }
        }

        public static class EBean {
            /**
             * pointNamesForE : []
             * accuracyForE : 0.0%
             */

            private String accuracyForE;
            private List<String> pointNamesForE;

            public String getAccuracyForE() {
                return accuracyForE;
            }

            public void setAccuracyForE(String accuracyForE) {
                this.accuracyForE = accuracyForE;
            }

            public List<String> getPointNamesForE() {
                return pointNamesForE;
            }

            public void setPointNamesForE(List<String> pointNamesForE) {
                this.pointNamesForE = pointNamesForE;
            }
        }
    }

    public static class LineChartBean {
        /**
         * date : 2019-03-01
         * score : B
         */

        private String date;
        private String score;
        private float rate;

        public float getRate() {
            return rate;
        }

        public void setRate(float rate) {
            this.rate = rate;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }
    }
}
