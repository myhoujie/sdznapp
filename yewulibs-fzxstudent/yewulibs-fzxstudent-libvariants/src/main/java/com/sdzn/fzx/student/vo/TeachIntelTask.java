package com.sdzn.fzx.student.vo;


public class TeachIntelTask {

    /**
     * 唯一id
     */
    private long id;

    /**
     * 学生id
     */
    private long userStudentId;

    /**
     * 学生name
     */
    private String userStudentName;

    /**
     * 软云题集id
     */
    private String teacherExamId;

    /**
     * 本系统学科id
     */
    private long baseSubjectId;

    /**
     * 本系统学段id
     */
    private long baseLevelId;

    /**
     * 本系统年级id
     */
    private long baseGradeId;

    /**
     * 题目数量
     */
    private int examCount;

    /**
     * 练习总分数
     */
    private double totalScore;

    /**
     * 创建时间
     */
    private long createTime;

    /**
     * 智能练习类型，1智能练习，2知识点练习，3章节练习，4提升练习
     */
    private int taskType;

    /**
     * 智能练习类型，0:未提交，1:已做达并提交，2:删除
     */
    private int status;

    /**
     * 做对的题的数量
     */
    private int rightCount;

    /**
     * 学生得分
     */
    private double rightScore;

    /**
     * 提交时间
     */
    private long commitTime;

    /**
     * 软云考试测评id
     */
    private String examVirtualSetResultId;

    /**
     * 软云测评结果id
     */
    private String examResultId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserStudentId() {
        return userStudentId;
    }

    public void setUserStudentId(long userStudentId) {
        this.userStudentId = userStudentId;
    }

    public String getUserStudentName() {
        return userStudentName;
    }

    public void setUserStudentName(String userStudentName) {
        this.userStudentName = userStudentName;
    }

    public String getTeacherExamId() {
        return teacherExamId;
    }

    public void setTeacherExamId(String teacherExamId) {
        this.teacherExamId = teacherExamId;
    }

    public long getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(long baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public long getBaseLevelId() {
        return baseLevelId;
    }

    public void setBaseLevelId(long baseLevelId) {
        this.baseLevelId = baseLevelId;
    }

    public long getBaseGradeId() {
        return baseGradeId;
    }

    public void setBaseGradeId(long baseGradeId) {
        this.baseGradeId = baseGradeId;
    }

    public int getExamCount() {
        return examCount;
    }

    public void setExamCount(int examCount) {
        this.examCount = examCount;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRightCount() {
        return rightCount;
    }

    public void setRightCount(int rightCount) {
        this.rightCount = rightCount;
    }

    public double getRightScore() {
        return rightScore;
    }

    public void setRightScore(double rightScore) {
        this.rightScore = rightScore;
    }

    public long getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(long commitTime) {
        this.commitTime = commitTime;
    }

    public String getExamVirtualSetResultId() {
        return examVirtualSetResultId;
    }

    public void setExamVirtualSetResultId(String examVirtualSetResultId) {
        this.examVirtualSetResultId = examVirtualSetResultId;
    }

    public String getExamResultId() {
        return examResultId;
    }

    public void setExamResultId(String examResultId) {
        this.examResultId = examResultId;
    }
}
