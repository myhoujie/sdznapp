package com.sdzn.fzx.student.vo.chatroombean;

import java.util.List;

/**
 *
 */

public class ClassGroupingVo {
    /**
     * total : 2
     * data : [{"id":1,"classId":1,"className":"时代一班","studentId":2,"studentName":"栗新斌","classGroupId":4,"classGroupName":"时代一班","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":"http://192.168.0.215:8886/head/2018/02/2a3f29e89bff4e1e81b1d5adb12662ac.png"},{"id":2,"classId":1,"className":"时代一班","studentId":3,"studentName":"孟杰","classGroupId":4,"classGroupName":"时代一班","createAccountId":null,"createTime":null,"createUserName":null,"sex":1,"photo":"http://192.168.0.215:8886/head/2018/02/2a3f29e89bff4e1e81b1d5adb12662ac.png"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * classId : 1
         * className : 时代一班
         * studentId : 2
         * studentName : 栗新斌
         * classGroupId : 4
         * classGroupName : 时代一班
         * createAccountId : null
         * createTime : null
         * createUserName : null
         * sex : 1
         * photo : http://192.168.0.215:8886/head/2018/02/2a3f29e89bff4e1e81b1d5adb12662ac.png
         */

        private int id;
        private int classId;
        private String className;
        private int studentId;
        private String studentName;
        private int classGroupId;
        private String classGroupName;
        private Object createAccountId;
        private Object createTime;
        private Object createUserName;
        private int sex;
        private String photo;

        private boolean checked;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getStudentId() {
            return studentId;
        }

        public void setStudentId(int studentId) {
            this.studentId = studentId;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public int getClassGroupId() {
            return classGroupId;
        }

        public void setClassGroupId(int classGroupId) {
            this.classGroupId = classGroupId;
        }

        public String getClassGroupName() {
            return classGroupName;
        }

        public void setClassGroupName(String classGroupName) {
            this.classGroupName = classGroupName;
        }

        public Object getCreateAccountId() {
            return createAccountId;
        }

        public void setCreateAccountId(Object createAccountId) {
            this.createAccountId = createAccountId;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(Object createUserName) {
            this.createUserName = createUserName;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
