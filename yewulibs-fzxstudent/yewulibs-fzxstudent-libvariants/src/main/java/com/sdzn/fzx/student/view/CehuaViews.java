package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.bean.GrzxRecActBean;

/**
 * 首页 view
 */
public interface CehuaViews extends IView {
    void onCehuaSuccess(GrzxRecActBean grzxRecActBean);

    void onCehuaNodata(String msg);

    void onCehuaFail(String msg);
}
