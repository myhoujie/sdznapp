package com.sdzn.fzx.student.vo;

/**
 * @author Reisen at 2018-12-26
 */
public class StudentTaskVo {
    private long answerTime;
    private int baseSubjectId;
    private String baseSubjectName;
    private int classGroupId;
    private String classGroupName;
    private long correctTime;
    private int correctType;
    private int countError;
    private int countExam;
    private int countHalf;
    private int countRescoure;
    private int countRight;
    private int countStudentSub;
    private int countStudentTotal;
    private int countTeacherCorrect;
    private int id;
    private int isCorrect;
    private int isRead;
    private int lessonTaskId;
    private int scope;
    private double score;
    private double scoreAvg;
    private double scoreObjective;
    private double scoreSubjective;
    private double scoreTotal;
    private int status;
    private long submitTime;
    private long useTime;//秒
    private int useTimeAvg;
    private int userStudentId;
    private String userStudentName;
    private int userTeacherId;
    private String userTeacherName;

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }

    public int getBaseSubjectId() {
        return baseSubjectId;
    }

    public void setBaseSubjectId(int baseSubjectId) {
        this.baseSubjectId = baseSubjectId;
    }

    public String getBaseSubjectName() {
        return baseSubjectName;
    }

    public void setBaseSubjectName(String baseSubjectName) {
        this.baseSubjectName = baseSubjectName;
    }

    public int getClassGroupId() {
        return classGroupId;
    }

    public void setClassGroupId(int classGroupId) {
        this.classGroupId = classGroupId;
    }

    public String getClassGroupName() {
        return classGroupName;
    }

    public void setClassGroupName(String classGroupName) {
        this.classGroupName = classGroupName;
    }

    public long getCorrectTime() {
        return correctTime;
    }

    public void setCorrectTime(long correctTime) {
        this.correctTime = correctTime;
    }

    public int getCorrectType() {
        return correctType;
    }

    public void setCorrectType(int correctType) {
        this.correctType = correctType;
    }

    public int getCountError() {
        return countError;
    }

    public void setCountError(int countError) {
        this.countError = countError;
    }

    public int getCountExam() {
        return countExam;
    }

    public void setCountExam(int countExam) {
        this.countExam = countExam;
    }

    public int getCountHalf() {
        return countHalf;
    }

    public void setCountHalf(int countHalf) {
        this.countHalf = countHalf;
    }

    public int getCountRescoure() {
        return countRescoure;
    }

    public void setCountRescoure(int countRescoure) {
        this.countRescoure = countRescoure;
    }

    public int getCountRight() {
        return countRight;
    }

    public void setCountRight(int countRight) {
        this.countRight = countRight;
    }

    public int getCountStudentSub() {
        return countStudentSub;
    }

    public void setCountStudentSub(int countStudentSub) {
        this.countStudentSub = countStudentSub;
    }

    public int getCountStudentTotal() {
        return countStudentTotal;
    }

    public void setCountStudentTotal(int countStudentTotal) {
        this.countStudentTotal = countStudentTotal;
    }

    public int getCountTeacherCorrect() {
        return countTeacherCorrect;
    }

    public void setCountTeacherCorrect(int countTeacherCorrect) {
        this.countTeacherCorrect = countTeacherCorrect;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getLessonTaskId() {
        return lessonTaskId;
    }

    public void setLessonTaskId(int lessonTaskId) {
        this.lessonTaskId = lessonTaskId;
    }

    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScoreAvg() {
        return scoreAvg;
    }

    public void setScoreAvg(double scoreAvg) {
        this.scoreAvg = scoreAvg;
    }

    public double getScoreObjective() {
        return scoreObjective;
    }

    public void setScoreObjective(double scoreObjective) {
        this.scoreObjective = scoreObjective;
    }

    public double getScoreSubjective() {
        return scoreSubjective;
    }

    public void setScoreSubjective(double scoreSubjective) {
        this.scoreSubjective = scoreSubjective;
    }

    public double getScoreTotal() {
        return scoreTotal;
    }

    public void setScoreTotal(double scoreTotal) {
        this.scoreTotal = scoreTotal;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(long submitTime) {
        this.submitTime = submitTime;
    }

    public long getUseTime() {
        return useTime;
    }

    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }

    public int getUseTimeAvg() {
        return useTimeAvg;
    }

    public void setUseTimeAvg(int useTimeAvg) {
        this.useTimeAvg = useTimeAvg;
    }

    public int getUserStudentId() {
        return userStudentId;
    }

    public void setUserStudentId(int userStudentId) {
        this.userStudentId = userStudentId;
    }

    public String getUserStudentName() {
        return userStudentName;
    }

    public void setUserStudentName(String userStudentName) {
        this.userStudentName = userStudentName;
    }

    public int getUserTeacherId() {
        return userTeacherId;
    }

    public void setUserTeacherId(int userTeacherId) {
        this.userTeacherId = userTeacherId;
    }

    public String getUserTeacherName() {
        return userTeacherName;
    }

    public void setUserTeacherName(String userTeacherName) {
        this.userTeacherName = userTeacherName;
    }
}
