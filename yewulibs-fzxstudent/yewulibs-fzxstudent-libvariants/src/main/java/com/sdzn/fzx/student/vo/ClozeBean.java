package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * @author Reisen at 2018-11-19
 */
public class ClozeBean implements Comparable<ClozeBean>{
    private boolean right;
    private int seq;
    private ArrayList<ClozeItemBean> list;
    private int mySeq;
    private String myContent;

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public ArrayList<ClozeItemBean> getList() {
        return list;
    }

    public void setList(ArrayList<ClozeItemBean> list) {
        this.list = list;
    }

    public int getMySeq() {
        return mySeq;
    }

    public void setMySeq(int mySeq) {
        this.mySeq = mySeq;
    }

    public String getMyContent() {
        return myContent;
    }

    public void setMyContent(String myContent) {
        this.myContent = myContent;
    }

    @Override
    public int compareTo(@NonNull ClozeBean o) {
        return seq - o.seq;
    }

    public static class ClozeItemBean {
        private String content;
        private boolean right;
        private int seq;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }
    }
}
