package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.bean.RecyclerTabBean;

/**
 * 首页 view
 */
public interface TabViews extends IView {
    void onTabSuccess(RecyclerTabBean recyclerTabBean);

    void onTabNodata(String msg);

    void onTabFail(String msg);

}
