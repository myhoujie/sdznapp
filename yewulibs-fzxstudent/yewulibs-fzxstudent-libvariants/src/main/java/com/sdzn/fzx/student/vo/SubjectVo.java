package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * 学科
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class SubjectVo {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        public DataBean() {
        }

        public DataBean(String name, int id) {
            this.name = name;
            this.id = id;
        }

        /**
         * name : 语文
         * id : 1
         */

        private String name;
        private int id;
        private boolean isSelected;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
