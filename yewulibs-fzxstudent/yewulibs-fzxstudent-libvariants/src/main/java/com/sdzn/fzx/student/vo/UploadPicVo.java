package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * Created by wangc on 2018/3/8 0008.
 */

public class UploadPicVo {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * thumbnailPath : http://192.168.0.215:8886/exam/2018/04/3a7ae3730876438e9305c2a8467d653c_thn.png
         * originalPath : http://192.168.0.215:8886/exam/2018/04/3a7ae3730876438e9305c2a8467d653c.png
         */

        private String thumbnailPath;
        private String originalPath;

        public static DataBean create(String domain, String key, String imageStyle) {
            DataBean bean = new DataBean();
            bean.setOriginalPath(domain + "/" + key);
            bean.setThumbnailPath(domain + "/" + key + "?" + imageStyle);
            return bean;
        }

        @Deprecated
        public String getThumbnailPath() {
            return thumbnailPath;
        }

        @Deprecated
        public void setThumbnailPath(String thumbnailPath) {
            this.thumbnailPath = thumbnailPath;
        }

        public String getOriginalPath() {
            return originalPath;
        }

        public void setOriginalPath(String originalPath) {
            this.originalPath = originalPath;
        }
    }
}
