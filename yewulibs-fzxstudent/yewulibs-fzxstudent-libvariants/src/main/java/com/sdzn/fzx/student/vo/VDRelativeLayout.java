package com.sdzn.fzx.student.vo;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;


/**
 * @author Reisen at 2017-11-22
 */

public class VDRelativeLayout extends RelativeLayout {
    private boolean isDrag = true;//子控件是否可拖动 default: true
    private View dragView;//可拖拽view
    private View topView;//顶部view
    private View shapeView;//和dragView同步移动的view
    private View bottomView;//底部view
    private SparseArray<Rect> mViewsPoint = new SparseArray<>(4);

    public VDRelativeLayout(Context context) {
        this(context, null);
    }

    public VDRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VDRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public VDRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        if (!isDrag) {
            return;
        }
        setClickable(true);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isDrag ? shouldInterceptTouchEvent(ev) : super.onInterceptTouchEvent(ev);
    }

    private boolean isDraging;
    private float dragX;
    private float dragY;
    private static final int INVALID_DRAG = -1;

    private boolean shouldInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        final float x = ev.getX();
        final float y = ev.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (dragView == null) {
                    return false;
                }
                if (x > dragView.getLeft() && x < dragView.getRight()
                        && y > dragView.getTop() && y < dragView.getBottom()
                        && dragView.getLeft() > 0 && dragView.getRight() < getRight()
                        && dragView.getTop() > 0 && dragView.getBottom() < getBottom()) {
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (!isDraging) {
                    return false;
                }
                if (dragView.getLeft() > 0 && dragView.getRight() < getRight()
                        && dragView.getTop() > 0 && dragView.getBottom() < getBottom()) {
                }
                return true;
            case MotionEvent.ACTION_UP:
                if (!isDraging) {
                    return false;
                }
                break;
        }
        return false;
    }

    private boolean processTouchEvent(MotionEvent event) {
        final float x = event.getX();
        final float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (dragView == null) {
                    return false;
                }
                if (y > dragView.getTop() && y < dragView.getBottom()
                        && dragView.getTop() > 0 && dragView.getBottom() < getBottom()) {
                    isDraging = true;
                    dragX = x;
                    dragY = y;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (!isDraging) {
                    return false;
                }
                if (dragView.getTop() > pointSize && dragView.getBottom() + pointSize < getBottom()) {
                    onViewsMove(dragX, dragY, x, y);
                }
                dragX = x;
                dragY = y;
                return true;
            case MotionEvent.ACTION_UP:
                if (!isDraging) {
                    return false;
                }
                if (dragView.getTop() > 0 && dragView.getBottom() < getBottom()) {
                    onViewsMove(dragX, dragY, x, y);
                }
                isDraging = false;
                dragX = INVALID_DRAG;
                dragY = INVALID_DRAG;
                return true;
        }
        return false;
    }

    private static final int pointSize = 30;

    private void onViewsMove(float startX, float startY, float endX, float endY) {
        dragView.bringToFront();
        int ySize = (int) (endY - startY);
        if (dragView.getTop() + ySize <= pointSize || dragView.getBottom() + ySize + pointSize >= getBottom()) {
            return;
        }
        if (dragView != null) {
            resetPoint(dragView.getId(), dragView.getLeft(), dragView.getTop() + ySize,
                    dragView.getRight(), dragView.getBottom() + ySize);
        }

        if (topView != null) {
            resetPoint(topView.getId(), topView.getLeft(), topView.getTop(),
                    topView.getRight(), topView.getBottom() + ySize);
        }

        if (shapeView != null) {
            resetPoint(shapeView.getId(), shapeView.getLeft(), shapeView.getTop() + ySize,
                    shapeView.getRight(), shapeView.getBottom() + ySize);
        }

        if (bottomView != null) {
            resetPoint(bottomView.getId(), bottomView.getLeft(), bottomView.getTop() + ySize,
                    bottomView.getRight(), bottomView.getBottom());
        }
        if (topView != null) {
                    LayoutParams params = (LayoutParams) topView.getLayoutParams();
                    params.height += ySize;
                    topView.setLayoutParams(params);
                }

        if (mCallBack != null) {
            mCallBack.layoutView();
        } else {
            requestLayout();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isDrag) {
            processTouchEvent(event);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        layoutView(dragView);

        layoutView(topView);

        layoutView(shapeView);

        layoutView(bottomView);
    }

    private void layoutView(View view) {
        Rect rect;
        if (view != null) {
            rect = mViewsPoint.get(view.getId());
            if (rect != null) {
                view.layout(rect.left, rect.top, rect.right, rect.bottom);
            }
        }
    }

    private void resetPoint(int viewId, int l, int t, int r, int b) {
        Rect point = mViewsPoint.get(viewId);
        if (point == null) {
            mViewsPoint.put(viewId, new Rect(l, t, r, b));
        } else {
            point.set(l, t, r, b);
        }
    }

    public void setDragView(View dragView) {
        this.dragView = dragView;
        dragView.bringToFront();
    }

    public void setShapeView(View view) {
        this.shapeView = view;
    }

    public void setTopView(View topView) {
        this.topView = topView;
        resetPoint(topView.getId(), 0, 0, 1920, 348);
    }

    public void setBottomView(View bottomView) {
        this.bottomView = bottomView;
    }

    public interface VDCallBack {
        void layoutView();
    }

    private VDCallBack mCallBack;

    public void setCallBack(VDCallBack callBack) {
        mCallBack = callBack;
    }
}
