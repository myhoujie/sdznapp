package com.sdzn.fzx.student.vo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 任务列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskListVo {
    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {

        /**
         * id : 465
         * lessonTaskStudentId : 1774
         * name : 哈哈哈
         * startTime : 1522131070000
         * endTime : 1522166400000
         * createUserName : 王春晓
         * isCorrect : 0
         * countExam : 2
         * countError : 0
         * countHalf : 0
         * countRight : 0
         * countResource : 1
         * answerViewOpen : 0
         * baseSubjectId : 3
         * baseSubjectName : 英语老师
         * chapterId : 2145
         * chapterNodeIdPath : 0.2145
         * chapterNodeNamePath : 预备篇
         * status : 0
         * overtime : 25525500
         * isRead : 1
         * score : 2
         * scoreTotal : 0
         * lessonId : 320
         * lessonName : 002
         * examTemplateId : null
         * countAnswer : 0
         */

        private int id;
        private int lessonTaskStudentId;
        private String name;
        private long startTime;
        private long endTime;
        private String createUserName;
        private int isCorrect;
        private int countExam;
        private int countError;
        private int countHalf;
        private int countRight;
        private int countResource;
        private int answerViewType;
        private int answerViewOpen;
        private int baseSubjectId;
        private String baseSubjectName;
        private int chapterId;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private int status;
        private long overtime;
        private int isRead;
        private int score;
        private int scoreTotal;
        private int lessonId;
        private String lessonName;
        private int examTemplateId;
        private int countAnswer;

        public int getAnswerViewType() {
            return answerViewType;
        }

        public void setAnswerViewType(int answerViewType) {
            this.answerViewType = answerViewType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public int getIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(int isCorrect) {
            this.isCorrect = isCorrect;
        }

        public int getCountExam() {
            return countExam;
        }

        public void setCountExam(int countExam) {
            this.countExam = countExam;
        }

        public int getCountError() {
            return countError;
        }

        public void setCountError(int countError) {
            this.countError = countError;
        }

        public int getCountHalf() {
            return countHalf;
        }

        public void setCountHalf(int countHalf) {
            this.countHalf = countHalf;
        }

        public int getCountRight() {
            return countRight;
        }

        public void setCountRight(int countRight) {
            this.countRight = countRight;
        }

        public int getCountResource() {
            return countResource;
        }

        public void setCountResource(int countResource) {
            this.countResource = countResource;
        }

        public int getAnswerViewOpen() {
            return answerViewOpen;
        }

        public void setAnswerViewOpen(int answerViewOpen) {
            this.answerViewOpen = answerViewOpen;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getOvertime() {
            return overtime;
        }

        public void setOvertime(long overtime) {
            this.overtime = overtime;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(int scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getCountAnswer() {
            return countAnswer;
        }

        public void setCountAnswer(int countAnswer) {
            this.countAnswer = countAnswer;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.lessonTaskStudentId);
            dest.writeString(this.name);
            dest.writeLong(this.startTime);
            dest.writeLong(this.endTime);
            dest.writeString(this.createUserName);
            dest.writeInt(this.isCorrect);
            dest.writeInt(this.countExam);
            dest.writeInt(this.countError);
            dest.writeInt(this.countHalf);
            dest.writeInt(this.countRight);
            dest.writeInt(this.countResource);
            dest.writeInt(this.answerViewOpen);
            dest.writeInt(this.baseSubjectId);
            dest.writeString(this.baseSubjectName);
            dest.writeInt(this.chapterId);
            dest.writeString(this.chapterNodeIdPath);
            dest.writeString(this.chapterNodeNamePath);
            dest.writeInt(this.status);
            dest.writeLong(this.overtime);
            dest.writeInt(this.isRead);
            dest.writeInt(this.score);
            dest.writeInt(this.scoreTotal);
            dest.writeInt(this.lessonId);
            dest.writeString(this.lessonName);
            dest.writeInt(this.examTemplateId);
            dest.writeInt(this.countAnswer);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.id = in.readInt();
            this.lessonTaskStudentId = in.readInt();
            this.name = in.readString();
            this.startTime = in.readLong();
            this.endTime = in.readLong();
            this.createUserName = in.readString();
            this.isCorrect = in.readInt();
            this.countExam = in.readInt();
            this.countError = in.readInt();
            this.countHalf = in.readInt();
            this.countRight = in.readInt();
            this.countResource = in.readInt();
            this.answerViewOpen = in.readInt();
            this.baseSubjectId = in.readInt();
            this.baseSubjectName = in.readString();
            this.chapterId = in.readInt();
            this.chapterNodeIdPath = in.readString();
            this.chapterNodeNamePath = in.readString();
            this.status = in.readInt();
            this.overtime = in.readLong();
            this.isRead = in.readInt();
            this.score = in.readInt();
            this.scoreTotal = in.readInt();
            this.lessonId = in.readInt();
            this.lessonName = in.readString();
            this.examTemplateId = in.readInt();
            this.countAnswer = in.readInt();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
