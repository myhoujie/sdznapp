package com.sdzn.fzx.student.bean;

import java.io.Serializable;

public class GrzxRecActBean2 implements Serializable {
    /**
     * id : 1
     * picture : https://file.fuzhuxian.com/grzx_1.png
     * seq : 1
     * type : 0
     * act : app://cs.znclass.com/com.sdzn.fzx.teacher.hs.act.WdkcActivity
     * name : 我的课程
     */

    private int id;
    private String picture;
    private int seq;
    private int type;
    private String act;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
