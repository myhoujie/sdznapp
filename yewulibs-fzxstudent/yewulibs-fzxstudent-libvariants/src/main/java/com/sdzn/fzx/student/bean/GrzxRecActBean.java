package com.sdzn.fzx.student.bean;

import java.io.Serializable;
import java.util.List;

public class GrzxRecActBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<GrzxRecActBean1> list;

    public GrzxRecActBean() {
    }

    public GrzxRecActBean(List<GrzxRecActBean1> list) {
        this.list = list;
    }


    public List<GrzxRecActBean1> getList() {
        return this.list;
    }

    public void setList(List<GrzxRecActBean1> list) {
        this.list = list;
    }
}
