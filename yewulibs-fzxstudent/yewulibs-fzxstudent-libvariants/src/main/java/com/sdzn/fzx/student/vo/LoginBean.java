package com.sdzn.fzx.student.vo;

/**
 * LoginBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class LoginBean {


    /**
     * id : 30
     * name : 15207963
     * available : 1
     * loginTime : 1515830248000
     * modifyTime : 1515830248000
     * loginCount : 74
     * email : null
     * telephone : null
     * identityId : 420528199402267334
     * description : null
     * createTime : 1515663811000
     * createUserName : 93746521
     * accessToken : 59c27f3d42ba47248238a4eaf04d20f2
     * refreshToken : 6893a8bff94a4ff68683d64a25b1bd06
     * expiresIn : 7200
     * user : {"id":6,"realName":"王银勇","accountId":30,"accountName":"15207963","available":1,"nickName":null,"sex":1,"photo":null,"createAccountId":3,"createUserId":null,"createUserName":"93746521","createTime":1515663811000,"updateTime":null,"userType":null,"telephone":"15863150117","accountRoleList":null,"email":null,"identityId":"420528199402267334","studentNumber":"736284","birthday":1515600000000,"baseEducationId":1,"baseEducationName":"六三制","baseLevelId":1,"baseLevelName":"小学","baseGradeId":3,"baseGradeName":"三年级","points":null,"zoneId":null,"zoneName":null,"zoneIdPath":null,"zoneNamePath":null,"scope":2017,"customerSchoolName":"山东大学附属中学","customerSchoolId":1,"classId":1,"className":"一班"}
     * menuList : null
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String access_token;
        private String oldAccessToken;
        private String refresh_token;
        /**
         * id : 6
         * realName : 王银勇
         * accountId : 30
         * accountName : 15207963
         * available : 1
         * nickName : null
         * sex : 1
         * photo : null
         * createAccountId : 3
         * createUserId : null
         * createUserName : 93746521
         * createTime : 1515663811000
         * updateTime : null
         * userType : null
         * telephone : 15863150117
         * accountRoleList : null
         * email : null
         * identityId : 420528199402267334
         * studentNumber : 736284
         * birthday : 1515600000000
         * baseEducationId : 1
         * baseEducationName : 六三制
         * baseLevelId : 1
         * baseLevelName : 小学
         * baseGradeId : 3
         * baseGradeName : 三年级
         * points : null
         * zoneId : null
         * zoneName : null
         * zoneIdPath : null
         * zoneNamePath : null
         * scope : 2017
         * customerSchoolName : 山东大学附属中学
         * customerSchoolId : 1
         * classId : 1
         * className : 一班
         */

        private UserBean userDetail;
        private String menuList;



        public String getAccessToken() {
            return access_token;
        }

        public void setAccessToken(String accessToken) {
            this.access_token = accessToken;
        }

        public String getRefreshToken() {
            return refresh_token;
        }

        public void setRefreshToken(String refreshToken) {
            this.refresh_token = refreshToken;
        }


        public UserBean getUser() {
            return userDetail;
        }

        public void setUser(UserBean user) {
            this.userDetail = user;
        }

        public String getMenuList() {
            return menuList;
        }

        public void setMenuList(String menuList) {
            this.menuList = menuList;
        }

        public String getOldAccessToken() {
            return oldAccessToken;
        }

        public void setOldAccessToken(String oldAccessToken) {
            this.oldAccessToken = oldAccessToken;
        }

        public static class UserBean {
            private int id;
            private String name;
            private int account;
            private String accountName;
            private int sex;
            private String avatar;
            private int levelId;
            private String levelName;
            private int baseGradeId;
            private String gradeName;
            private String schoolName;
            private int schoolId;
            private int classId;
            private String className;
            private String classGroupId;
            private String classGroupName;


            private String classManagerName;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getRealName() {
                return name;
            }

            public void setRealName(String realName) {
                this.name = realName;
            }

            public int getAccountId() {
                return account;
            }

            public void setAccountId(int accountId) {
                this.account = accountId;
            }

            //无数据
            public String getAccountName() {
                return accountName;
            }

            public void setAccountName(String accountName) {
                this.accountName = accountName;
            }


            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public String getPhoto() {
                return avatar;
            }

            public void setPhoto(String photo) {
                this.avatar = photo;
            }


            public int getBaseLevelId() {
                return levelId;
            }

            public void setBaseLevelId(int baseLevelId) {
                this.levelId = baseLevelId;
            }

            public String getBaseLevelName() {
                return levelName;
            }

            public void setBaseLevelName(String baseLevelName) {
                this.levelName = baseLevelName;
            }

            public int getBaseGradeId() {
                return baseGradeId;
            }

            public void setBaseGradeId(int baseGradeId) {
                this.baseGradeId = baseGradeId;
            }

            public String getBaseGradeName() {
                return gradeName;
            }

            public void setBaseGradeName(String baseGradeName) {
                this.gradeName = baseGradeName;
            }


            public String getCustomerSchoolName() {
                return schoolName;
            }

            public void setCustomerSchoolName(String customerSchoolName) {
                this.schoolName = customerSchoolName;
            }

            public int getCustomerSchoolId() {
                return schoolId;
            }

            public void setCustomerSchoolId(int customerSchoolId) {
                this.schoolId = customerSchoolId;
            }

            public int getClassId() {
                return classId;
            }

            public void setClassId(int classId) {
                this.classId = classId;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }

            public String getClassGroupId() {
                return classGroupId;
            }

            public void setClassGroupId(String classGroupId) {
                this.classGroupId = classGroupId;
            }

            public String getClassGroupName() {
                return classGroupName;
            }

            public void setClassGroupName(String classGroupName) {
                this.classGroupName = classGroupName;
            }
        }
    }
}
