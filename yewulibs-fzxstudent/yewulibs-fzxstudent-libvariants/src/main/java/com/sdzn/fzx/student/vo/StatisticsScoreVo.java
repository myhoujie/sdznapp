package com.sdzn.fzx.student.vo;

/**
 * Created by wangc on 2018/3/16 0016.
 */

public class StatisticsScoreVo {
    /**
     * data : {"scoreTotal":0,"highestScore":1,"ranking":0,"sumScore":1,"scoreAvg":0}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * scoreTotal : 0
         * highestScore : 1
         * ranking : 0
         * sumScore : 1
         * scoreAvg : 0
         */

        private float scoreTotal;
        private float highestScore;
        private float ranking;
        private float sumScore;
        private float scoreAvg;

        public float getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(float scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public float getHighestScore() {
            return highestScore;
        }

        public void setHighestScore(float highestScore) {
            this.highestScore = highestScore;
        }

        public float getRanking() {
            return ranking;
        }

        public void setRanking(float ranking) {
            this.ranking = ranking;
        }

        public float getSumScore() {
            return sumScore;
        }

        public void setSumScore(float sumScore) {
            this.sumScore = sumScore;
        }

        public float getScoreAvg() {
            return scoreAvg;
        }

        public void setScoreAvg(float scoreAvg) {
            this.scoreAvg = scoreAvg;
        }
    }
}
