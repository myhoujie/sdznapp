package com.sdzn.fzx.student.dao.controller;

import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;

/**
 * 用户信息控制
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public class UserController {

    public static String getUserId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getId());
        }
        return null;
    }

    public static String getAccountId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getAccountId());
        }
        return null;
    }

    public static String getUserName() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null) {
            return String.valueOf(loginBean.getData().getUser().getRealName());
        }
        return null;
    }

    public static String getAccessToken() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null) {
            return loginBean.getData().getAccessToken();
        }
        return null;
    }

    public static String getSchoolId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return String.valueOf(loginBean.getData().getUser().getCustomerSchoolId());
        }
        return null;
    }

    public static String getRefreshToken() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null) {
            return loginBean.getData().getRefreshToken();
        }
        return null;
    }

    public static String getBaseLevelId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return String.valueOf(loginBean.getData().getUser().getBaseLevelId());
        }
        return null;
    }

    public static String getBaseGradeId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return String.valueOf(loginBean.getData().getUser().getBaseGradeId());
        }
        return null;
    }

    public static LoginBean getLoginBean() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return loginBean;
        }
        return null;
    }

    public static String getClassId() {
        LoginBean loginBean = StudentSPUtils.getLoginBean();
        if (loginBean != null && loginBean.getData() != null && loginBean.getData().getUser() != null) {
            return String.valueOf(loginBean.getData().getUser().getClassId());
        }
        return null;
    }
}
