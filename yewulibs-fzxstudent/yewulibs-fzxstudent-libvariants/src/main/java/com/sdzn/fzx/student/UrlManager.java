package com.sdzn.fzx.student;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UrlManager {
    //测试
    public static final String CCC = "测试";
    public static final String FLAVOR1 = "_192_168_0_204";
    public static final int VERSION_CODE1 = getAppVersionCode();
    public static final String VERSION_NAME1 = getAppVersionName(2);
    public static final String AUTH1 = "";
    public static final String BASE_ADDRESS1 = "http://192.168.0.204";
    public static final String BUGLY_CHANNEL1 = "204本地环境";
    public static final String DOWN1 = "http://192.168.0.204:8020/app.info";
    public static final String MQTT1 = "tcp://192.168.0.206:1883";
    public static final int OFFICE_ID1 = 17783;
    public static final String PORT1 = ":8000";
    public static final int RABBIT_HOST1 = 5672;
    public static final String RABBIT_MQ1 = "192.168.0.204";
    public static final String RABBIT_NAME1 = "root";
    public static final String RABBIT_PASSWORD1 = "123456";
    public static final String CLASSURL1 = "http://doc.znclass.com";
    // Fields from default config.
    public static final int TINKER_VERSION1 = 1;

    //预生产
    public static final String YYY = "预生产";
    public static final String FLAVOR2 = "_114_115_155_179";
    public static final int VERSION_CODE2 = getAppVersionCode();
    public static final String VERSION_NAME2 = getAppVersionName(2);
    public static final String AUTH2 = "";
    public static final String BASE_ADDRESS2 = "http://114.115.155.179";
    public static final String BUGLY_CHANNEL2 = "118测试环境";
    public static final String DOWN2 = "http://139.129.96.69/aap.info";
    public static final String MQTT2 = "tcp://120.27.24.185:1883";
    public static final int OFFICE_ID2 = 17783;
    public static final String PORT2 = ":8000";
    public static final int RABBIT_HOST2 = 5672;
    public static final String RABBIT_MQ2 = "114.115.155.179";
    public static final String RABBIT_NAME2 = "root";
    public static final String RABBIT_PASSWORD2 = "sdzn@123456";
    public static final String CLASSURL2 = "http://doc.znclass.com";
    // Fields from default config.
    public static final int TINKER_VERSION2 = 1;

    //线上
    public static final String OOO = "线上";
    public static final String FLAVOR3 = "_115_28_91_19";
    public static final int VERSION_CODE3 = getAppVersionCode();
    public static final String VERSION_NAME3 = getAppVersionName(1);
    public static final String AUTH3 = "";
    public static final String BASE_ADDRESS3 = "http://auth.fuzhuxian.com";
    public static final String BUGLY_CHANNEL3 = "115正式环境";
    public static final String DOWN3 = "http://teacher.fuzhuxian.com/app.info";
    public static final String MQTT3 = "tcp://fzxmq.fuzhuxian.com:80";
    public static final int OFFICE_ID3 = 17965;
    public static final String PORT3 = "";
    public static final int RABBIT_HOST3 = 5672;
    public static final String RABBIT_MQ3 = "49.4.30.58";
    public static final String RABBIT_NAME3 = "root";
    public static final String RABBIT_PASSWORD3 = "sdzn@123456";
    public static final String CLASSURL3 = "http://doc.znclass.com";
    // Fields from default config.
    public static final int TINKER_VERSION3 = 1;

    public static String versionCount() {
        return "01";
    }

    public String getTinkerVersionCode() {
        //每次发新版本/热更新这个值都要增加
        return "1";
    }

    public static String getAppVersionPre() {
        return "v4.1s_";
    }

    // 获取 version name
    public static String getAppVersionName(int type) {
        String ver = getAppVersionPre();
        String today = new SimpleDateFormat("MMdd").format(new Date());
        return ver + today + '_' + versionCount() + '_' + type;
    }

    public static int getAppVersionCode() {
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date()) + versionCount();
        return Integer.parseInt(today);
    }
}
