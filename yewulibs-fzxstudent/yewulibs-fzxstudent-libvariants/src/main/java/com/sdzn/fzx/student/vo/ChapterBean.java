package com.sdzn.fzx.student.vo;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/15.
 */
public class ChapterBean {
    private int subjectId;
    private long chapterId;

    public ChapterBean() {
    }

    public ChapterBean(int subjectId, long chapterId) {
        this.subjectId = subjectId;
        this.chapterId = chapterId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public long getChapterId() {
        return chapterId;
    }

    public void setChapterId(long chapterId) {
        this.chapterId = chapterId;
    }
}
