package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * AnalyzeSubjectBean〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class AnalyzeSubjectBean {


    /**
     * submitTime : 1517229155000
     * scoreTotal : 0.25
     * baseSubjectName : 语文
     * baseSubjectId : 1
     */

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private long submitTime;
        private double scoreTotal;
        private String baseSubjectName;
        private String baseSubjectId;

        public long getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(long submitTime) {
            this.submitTime = submitTime;
        }

        public double getScoreTotal() {
            return scoreTotal;
        }

        public void setScoreTotal(double scoreTotal) {
            this.scoreTotal = scoreTotal;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public String getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(String baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }
    }
}
