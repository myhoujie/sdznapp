package com.sdzn.fzx.student.presenter;

import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.view.CheckverionViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class CheckverionFzxPresenter extends Presenter<CheckverionViews> {

    public void checkVerion(String programId, String type) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryVersion(programId, type)//SPToken.getToken(),
                .enqueue(new Callback<ResponseSlbBean1<VersionInfoBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<VersionInfoBean>> call, Response<ResponseSlbBean1<VersionInfoBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        getView().OnUpdateVersionSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<VersionInfoBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnUpdateVersionFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
