package com.sdzn.fzx.student.view;


import com.haier.cellarette.libmvp.mvp.IView;

/**
 *
 */

public interface ChangePhoneViews extends IView {
    public void onCountDownChanged(int str);

    void verifySuccess(final String phone, final String code);

    void savePhone();

    void onFailed(String msg);
}
