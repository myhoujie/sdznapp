package com.sdzn.fzx.student.presenter;

import android.util.Log;

import com.blankj.utilcode.util.SPUtils;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.view.ShouyeViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ShouyePresenter extends Presenter<ShouyeViews> {
    public void queryShouye() {
        Log.e("queryShouye", "queryShouye: " + SPUtils.getInstance().getString("token", ""));
        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryShouye("Bearer " + (String) SPUtils.getInstance().getString("token", ""))
                .enqueue(new Callback<ResponseSlbBean1<ShouyeRecActBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ShouyeRecActBean>> call, Response<ResponseSlbBean1<ShouyeRecActBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            getView().onCehuaNodata("");
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCehuaNodata(response.body().getMessage());
                            return;
                        }
                        getView().onCehuaSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ShouyeRecActBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCehuaFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
