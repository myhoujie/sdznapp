package com.sdzn.fzx.student.vo;

import java.util.List;

/**
 * 任务列表
 *
 * @author wangchunxiao
 * @date 2018/1/15
 */
public class TaskFileDetailVo {

    /**
     * total : 1
     * data : [{"flagSync":0,"count":1,"classId":22,"className":"五班","lessonId":665,"lessonName":"002","lessonResourceId":null,"resourceText":"{\"baseEducationId\":1,\"baseEducationName\":\"六三制\",\"baseGradeId\":7,\"baseGradeName\":\"七年级\",\"baseLevelId\":2,\"baseLevelName\":\"初中\",\"baseSubjectId\":1,\"baseSubjectName\":\"语文老师\",\"baseVersionId\":1,\"baseVersionName\":\"人教版（部编版）\",\"baseVolumeId\":1,\"baseVolumeName\":\"上册\",\"chapterId\":2888,\"chapterIdPath\":\"0.2888\",\"chapterName\":\"第一单元\",\"chapterNamePath\":\"第一单元\",\"collectCount\":0,\"convertPath\":\"http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg\",\"convertStatus\":2,\"createDate\":1522833014000,\"createUserId\":77,\"createUserName\":\"Reisen.U.I\",\"customerSchoolId\":6,\"customerSchoolName\":\"老师端测试学校\",\"downloadCount\":0,\"hots\":0,\"id\":431,\"isShare\":1,\"resourceName\":\"DS24CugW0AEpMm2.jpg\",\"resourcePath\":\"http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg\",\"resourceSize\":353846,\"resourceSuffix\":\"jpg\",\"resourceType\":4,\"scene\":1,\"subjectStructureId\":3,\"subjectStructureIdPath\":\"0.1.3\",\"subjectStructureName\":\"复习学案\",\"subjectStructureNamePath\":\"学案>复习学案\",\"updateDate\":1522833037000,\"useCount\":0}","status":false,"collectId":null,"id":431,"resourceName":"DS24CugW0AEpMm2.jpg","resourceSize":353846,"resourcePath":"http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg","resourceType":4,"resourceSuffix":"jpg","convertDocumentPath":null,"convertDocumentSize":null,"convertVideoPath":null,"convertVideoSize":null,"convertVideoTimeLength":null,"convertMediaPath":null,"convertMediaSize":null,"convertMediaTimeLength":null,"convertDate":null,"convertStatus":2,"updateDate":1522833037000,"subjectStructureId":3,"subjectStructureName":"复习学案","subjectStructureIdPath":"0.1.3","subjectStructureNamePath":"学案>复习学案","chapterId":2888,"chapterName":"第一单元","chapterIdPath":"0.2888","chapterNamePath":"第一单元"}]
     */

    private int total;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * flagSync : 0
         * count : 1
         * classId : 22
         * className : 五班
         * lessonId : 665
         * lessonName : 002
         * lessonResourceId : null
         * resourceText : {"baseEducationId":1,"baseEducationName":"六三制","baseGradeId":7,"baseGradeName":"七年级","baseLevelId":2,"baseLevelName":"初中","baseSubjectId":1,"baseSubjectName":"语文老师","baseVersionId":1,"baseVersionName":"人教版（部编版）","baseVolumeId":1,"baseVolumeName":"上册","chapterId":2888,"chapterIdPath":"0.2888","chapterName":"第一单元","chapterNamePath":"第一单元","collectCount":0,"convertPath":"http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg","convertStatus":2,"createDate":1522833014000,"createUserId":77,"createUserName":"Reisen.U.I","customerSchoolId":6,"customerSchoolName":"老师端测试学校","downloadCount":0,"hots":0,"id":431,"isShare":1,"resourceName":"DS24CugW0AEpMm2.jpg","resourcePath":"http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg","resourceSize":353846,"resourceSuffix":"jpg","resourceType":4,"scene":1,"subjectStructureId":3,"subjectStructureIdPath":"0.1.3","subjectStructureName":"复习学案","subjectStructureNamePath":"学案>复习学案","updateDate":1522833037000,"useCount":0}
         * status : false
         * collectId : null
         * id : 431
         * resourceName : DS24CugW0AEpMm2.jpg
         * resourceSize : 353846
         * resourcePath : http://192.168.0.214:8886/resource/image/2018/04/fdd4cecfa4244e84aac27d4063e7a93b.jpg
         * resourceType : 4
         * resourceSuffix : jpg
         * convertDocumentPath : null
         * convertDocumentSize : null
         * convertVideoPath : null
         * convertVideoSize : null
         * convertVideoTimeLength : null
         * convertMediaPath : null
         * convertMediaSize : null
         * convertMediaTimeLength : null
         * convertDate : null
         * convertStatus : 2
         * updateDate : 1522833037000
         * subjectStructureId : 3
         * subjectStructureName : 复习学案
         * subjectStructureIdPath : 0.1.3
         * subjectStructureNamePath : 学案>复习学案
         * chapterId : 2888
         * chapterName : 第一单元
         * chapterIdPath : 0.2888
         * chapterNamePath : 第一单元
         */

        private int flagSync;
        private int count;
        private int classId;
        private String className;
        private int lessonId;
        private String lessonName;
        private int lessonResourceId;
        private String resourceText;
        private boolean status;
        private int collectId;
        private int id;
        private String resourceName;
        private int resourceSize;
        private String resourcePath;
        private int resourceType;
        private String resourceSuffix;
        private Object convertDocumentPath;
        private Object convertDocumentSize;
        private Object convertVideoPath;
        private Object convertVideoSize;
        private Object convertVideoTimeLength;
        private Object convertMediaPath;
        private Object convertMediaSize;
        private Object convertMediaTimeLength;
        private Object convertDate;
        private int convertStatus;
        private long updateDate;
        private int subjectStructureId;
        private String subjectStructureName;
        private String subjectStructureIdPath;
        private String subjectStructureNamePath;
        private int chapterId;
        private String chapterName;
        private String chapterIdPath;
        private String chapterNamePath;

        public boolean isDownlaoding() {
            return downlaoding;
        }

        public void setDownlaoding(boolean downlaoding) {
            this.downlaoding = downlaoding;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }

        private boolean downlaoding;
        private int progress;

        public int getFlagSync() {
            return flagSync;
        }

        public void setFlagSync(int flagSync) {
            this.flagSync = flagSync;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public int getLessonId() {
            return lessonId;
        }

        public void setLessonId(int lessonId) {
            this.lessonId = lessonId;
        }

        public String getLessonName() {
            return lessonName;
        }

        public void setLessonName(String lessonName) {
            this.lessonName = lessonName;
        }

        public int getLessonResourceId() {
            return lessonResourceId;
        }

        public void setLessonResourceId(int lessonResourceId) {
            this.lessonResourceId = lessonResourceId;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public int getCollectId() {
            return collectId;
        }

        public void setCollectId(int collectId) {
            this.collectId = collectId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public int getResourceSize() {
            return resourceSize;
        }

        public void setResourceSize(int resourceSize) {
            this.resourceSize = resourceSize;
        }

        public String getResourcePath() {
            return resourcePath;
        }

        public void setResourcePath(String resourcePath) {
            this.resourcePath = resourcePath;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceSuffix() {
            return resourceSuffix;
        }

        public void setResourceSuffix(String resourceSuffix) {
            this.resourceSuffix = resourceSuffix;
        }

        public Object getConvertDocumentPath() {
            return convertDocumentPath;
        }

        public void setConvertDocumentPath(Object convertDocumentPath) {
            this.convertDocumentPath = convertDocumentPath;
        }

        public Object getConvertDocumentSize() {
            return convertDocumentSize;
        }

        public void setConvertDocumentSize(Object convertDocumentSize) {
            this.convertDocumentSize = convertDocumentSize;
        }

        public Object getConvertVideoPath() {
            return convertVideoPath;
        }

        public void setConvertVideoPath(Object convertVideoPath) {
            this.convertVideoPath = convertVideoPath;
        }

        public Object getConvertVideoSize() {
            return convertVideoSize;
        }

        public void setConvertVideoSize(Object convertVideoSize) {
            this.convertVideoSize = convertVideoSize;
        }

        public Object getConvertVideoTimeLength() {
            return convertVideoTimeLength;
        }

        public void setConvertVideoTimeLength(Object convertVideoTimeLength) {
            this.convertVideoTimeLength = convertVideoTimeLength;
        }

        public Object getConvertMediaPath() {
            return convertMediaPath;
        }

        public void setConvertMediaPath(Object convertMediaPath) {
            this.convertMediaPath = convertMediaPath;
        }

        public Object getConvertMediaSize() {
            return convertMediaSize;
        }

        public void setConvertMediaSize(Object convertMediaSize) {
            this.convertMediaSize = convertMediaSize;
        }

        public Object getConvertMediaTimeLength() {
            return convertMediaTimeLength;
        }

        public void setConvertMediaTimeLength(Object convertMediaTimeLength) {
            this.convertMediaTimeLength = convertMediaTimeLength;
        }

        public Object getConvertDate() {
            return convertDate;
        }

        public void setConvertDate(Object convertDate) {
            this.convertDate = convertDate;
        }

        public int getConvertStatus() {
            return convertStatus;
        }

        public void setConvertStatus(int convertStatus) {
            this.convertStatus = convertStatus;
        }

        public long getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(long updateDate) {
            this.updateDate = updateDate;
        }

        public int getSubjectStructureId() {
            return subjectStructureId;
        }

        public void setSubjectStructureId(int subjectStructureId) {
            this.subjectStructureId = subjectStructureId;
        }

        public String getSubjectStructureName() {
            return subjectStructureName;
        }

        public void setSubjectStructureName(String subjectStructureName) {
            this.subjectStructureName = subjectStructureName;
        }

        public String getSubjectStructureIdPath() {
            return subjectStructureIdPath;
        }

        public void setSubjectStructureIdPath(String subjectStructureIdPath) {
            this.subjectStructureIdPath = subjectStructureIdPath;
        }

        public String getSubjectStructureNamePath() {
            return subjectStructureNamePath;
        }

        public void setSubjectStructureNamePath(String subjectStructureNamePath) {
            this.subjectStructureNamePath = subjectStructureNamePath;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterIdPath() {
            return chapterIdPath;
        }

        public void setChapterIdPath(String chapterIdPath) {
            this.chapterIdPath = chapterIdPath;
        }

        public String getChapterNamePath() {
            return chapterNamePath;
        }

        public void setChapterNamePath(String chapterNamePath) {
            this.chapterNamePath = chapterNamePath;
        }
    }
}
