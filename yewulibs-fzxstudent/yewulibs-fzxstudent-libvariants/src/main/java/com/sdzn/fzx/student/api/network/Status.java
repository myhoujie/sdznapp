/*
 * 文件名：Status
 * 版权：Copyright 2002-2017 SDZN. All Rights Reserved.
 * 描述：
 * 修改人： yizhou
 * 修改时间：16/7/11
 * 修改内容：新增
 */
package com.sdzn.fzx.student.api.network;

/**
 * 描述：状态码
 * <p>
 * 创建人：zhangchao
 * 创建时间：17/4/20
 * 枚举类型一些常量的限制
 */
public enum Status {

    // 正常
    SUCCESS(0, "success"),

    // 系统错误
    SYSTEM_ERROR(10001, "System error"), // 系统错误
    API_NOT_FOUND(10002, "Request api not found"), // 未找到请求的接口
    ILLEGAL_REQUEST(10003, "Illegal request"), // 非法请求
    AUTHENTICATION_FAIL(10004, "Authentication fail"), // 认证失败
    API_NOT_IMPLEMENT(10005, "Api not implement"), // 接口暂未实现
    TOKEN_TIME_OUT(10009, "登录过期，请重新登录"),//登录过期/顶号

    // 通用业务异常，错误码范围 20001-20100
    PARSE_PARAMETERS_ERROR(20001, "Parse parameters error"), // 参数处理失败
    VALIDATE_PARAMETERS_ERROR(20002, "Validate parameters error"), // 参数验证失败
    TOKEN_GIVE_ERROR(20003, "TOKEN 生成出错"),
    TOKEN_INVALID_REQUEST(20004, "The access token is missing"), // token 失效
//    TOKEN_Invalid(20005, "The access token is invalid or has expired"), // token 失效

    DB_OPERATION_ERROR(20201, "数据库操作失败"), // 参数处理失败
    NO_DATA_EXIST(20202, "没有数据"),

    // 账号业务，错误码范围 30001-30100
    ACCOUNT_NOT_FOUND(30001, "账号名不存在"),
    ACCOUNT_PASSWORD_ERROR(30002, "密码错误"),
    ACCOUNT_DISABLE(30003, "账号禁用"),
    ACCOUNT_EXPIRED(30004, "账号过期"),
    ACCOUNT_VALIDATE_CAPTCHA_ERROR(30005, "验证码错误"),
    ACCOUNT_FOUND(30006, "账号已创建"),
    // 用户业务，错误码范围 40001-40100
    USER_NOT_FOUND_ERROR(40001, "用户不存在"),


    // 未知异常
    UNKNOWN_ERROR(-1, "Unknown error"); //

    int code;

    String msg;

    Status(int code, String msg) {

        this.code = code;
        this.msg = msg;
    }

    public int getCode() {

        return code;
    }

    public String getMsg() {

        return msg;
    }

    public static Status getStatusByCode(int code) {

        for (Status status : Status.values()) {

            if (status.getCode() == code) {

                return status;
            }
        }
        return null;
    }

}