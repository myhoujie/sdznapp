package com.sdzn.fzx.student.bean;

import java.io.Serializable;
import java.util.List;

public class ShouyeRecActBean implements Serializable {


    /**
     * hztj : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.HZTJActivity","details":"与同学们一起成组讨论","name":"合作探究","picture":"https://file.fuzhuxian.com/new_icon_hztj.png"}
     * pktact : {"act":"otherapp","name":"智囊学堂"}
     * studentcount : {"studenttime":"2020-09-29星期二","studenttitle":"认真学习、不断努力<font color='#FA541C'>成就最好的自己！<\/font>。"}
     * studentlist1 : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.ZzxxActivity","dwctitle":"待完成:1","name":"自主学习","studentitemlist":[{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.ZzxxActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]}
     * studentlist2 : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdzyActivity","dwctitle":"待完成:1","name":"我的作业","studentitemlist":[{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.WdzyActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]}
     * studentlist3 : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KtxxActivity","dwctitle":"待完成:1","name":"课堂学习","studentitemlist":[{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.KtxxActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]}
     * wdks : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KSActivity","details":"课堂检测，知识测评","name":"我的考试","picture":"https://file.fuzhuxian.com/new_icon_kaoshi.png"}
     * wdpg : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdpgActivity","details":"同步练习，知识点练习","name":"我的批改","picture":"https://file.fuzhuxian.com/new_icon_taolun.png"}
     * xqfx : {"act":"app://cs.znclass.com/com.sdzn.fzx.student.hs.act.XqfxShouyeActivity","details":"我的学科水平变化趋势","name":"学情分析","picture":"https://file.fuzhuxian.com/new_icon_xqfx.png"}
     */

    private HztjBean hztj;
    private PktactBean pktact;
    private StudentcountBean studentcount;
    private Studentlist1Bean studentlist1;
    private Studentlist2Bean studentlist2;
    private Studentlist3Bean studentlist3;
    private WdksBean wdks;
    private WdpgBean wdpg;
    private XqfxBean xqfx;

    public HztjBean getHztj() {
        return hztj;
    }

    public void setHztj(HztjBean hztj) {
        this.hztj = hztj;
    }

    public PktactBean getPktact() {
        return pktact;
    }

    public void setPktact(PktactBean pktact) {
        this.pktact = pktact;
    }

    public StudentcountBean getStudentcount() {
        return studentcount;
    }

    public void setStudentcount(StudentcountBean studentcount) {
        this.studentcount = studentcount;
    }

    public Studentlist1Bean getStudentlist1() {
        return studentlist1;
    }

    public void setStudentlist1(Studentlist1Bean studentlist1) {
        this.studentlist1 = studentlist1;
    }

    public Studentlist2Bean getStudentlist2() {
        return studentlist2;
    }

    public void setStudentlist2(Studentlist2Bean studentlist2) {
        this.studentlist2 = studentlist2;
    }

    public Studentlist3Bean getStudentlist3() {
        return studentlist3;
    }

    public void setStudentlist3(Studentlist3Bean studentlist3) {
        this.studentlist3 = studentlist3;
    }

    public WdksBean getWdks() {
        return wdks;
    }

    public void setWdks(WdksBean wdks) {
        this.wdks = wdks;
    }

    public WdpgBean getWdpg() {
        return wdpg;
    }

    public void setWdpg(WdpgBean wdpg) {
        this.wdpg = wdpg;
    }

    public XqfxBean getXqfx() {
        return xqfx;
    }

    public void setXqfx(XqfxBean xqfx) {
        this.xqfx = xqfx;
    }

    public static class HztjBean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.HZTJActivity
         * details : 与同学们一起成组讨论
         * name : 合作探究
         * picture : https://file.fuzhuxian.com/new_icon_hztj.png
         */

        private String act;
        private String details;
        private String name;
        private String picture;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }
    }

    public static class PktactBean {
        /**
         * act : otherapp
         * name : 智囊学堂
         */

        private String act;
        private String name;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class StudentcountBean {
        /**
         * studenttime : 2020-09-29星期二
         * studenttitle : 认真学习、不断努力<font color='#FA541C'>成就最好的自己！</font>。
         */

        private String studenttime;
        private String studenttitle;

        public String getStudenttime() {
            return studenttime;
        }

        public void setStudenttime(String studenttime) {
            this.studenttime = studenttime;
        }

        public String getStudenttitle() {
            return studenttitle;
        }

        public void setStudenttitle(String studenttitle) {
            this.studenttitle = studenttitle;
        }
    }

    public static class Studentlist1Bean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.ZzxxActivity
         * dwctitle : 待完成:1
         * name : 自主学习
         * studentitemlist : [{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.ZzxxActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]
         */

        private String act;
        private String dwctitle;
        private String name;
        private List<StudentitemlistBean> studentitemlist;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDwctitle() {
            return dwctitle;
        }

        public void setDwctitle(String dwctitle) {
            this.dwctitle = dwctitle;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<StudentitemlistBean> getStudentitemlist() {
            return studentitemlist;
        }

        public void setStudentitemlist(List<StudentitemlistBean> studentitemlist) {
            this.studentitemlist = studentitemlist;
        }

        public static class StudentitemlistBean {
            /**
             * act : app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.ZzxxActivity?subjectid=
             * curriculumname : 光合作用
             * subjectid : 1
             * subjectname : 生物
             * subjectpicture : https://file.fuzhuxian.com/new_icon_shengwu.png
             * time : 2020-09-29 17:23:03
             */

            private String act;
            private String curriculumname;
            private String subjectid;
            private String subjectname;
            private String subjectpicture;
            private String time;

            public String getAct() {
                return act;
            }

            public void setAct(String act) {
                this.act = act;
            }

            public String getCurriculumname() {
                return curriculumname;
            }

            public void setCurriculumname(String curriculumname) {
                this.curriculumname = curriculumname;
            }

            public String getSubjectid() {
                return subjectid;
            }

            public void setSubjectid(String subjectid) {
                this.subjectid = subjectid;
            }

            public String getSubjectname() {
                return subjectname;
            }

            public void setSubjectname(String subjectname) {
                this.subjectname = subjectname;
            }

            public String getSubjectpicture() {
                return subjectpicture;
            }

            public void setSubjectpicture(String subjectpicture) {
                this.subjectpicture = subjectpicture;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    public static class Studentlist2Bean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdzyActivity
         * dwctitle : 待完成:1
         * name : 我的作业
         * studentitemlist : [{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.WdzyActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]
         */

        private String act;
        private String dwctitle;
        private String name;
        private List<StudentitemlistBeanX> studentitemlist;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDwctitle() {
            return dwctitle;
        }

        public void setDwctitle(String dwctitle) {
            this.dwctitle = dwctitle;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<StudentitemlistBeanX> getStudentitemlist() {
            return studentitemlist;
        }

        public void setStudentitemlist(List<StudentitemlistBeanX> studentitemlist) {
            this.studentitemlist = studentitemlist;
        }

        public static class StudentitemlistBeanX {
            /**
             * act : app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.WdzyActivity?subjectid=
             * curriculumname : 光合作用
             * subjectid : 1
             * subjectname : 生物
             * subjectpicture : https://file.fuzhuxian.com/new_icon_shengwu.png
             * time : 2020-09-29 17:23:03
             */

            private String act;
            private String curriculumname;
            private String subjectid;
            private String subjectname;
            private String subjectpicture;
            private String time;

            public String getAct() {
                return act;
            }

            public void setAct(String act) {
                this.act = act;
            }

            public String getCurriculumname() {
                return curriculumname;
            }

            public void setCurriculumname(String curriculumname) {
                this.curriculumname = curriculumname;
            }

            public String getSubjectid() {
                return subjectid;
            }

            public void setSubjectid(String subjectid) {
                this.subjectid = subjectid;
            }

            public String getSubjectname() {
                return subjectname;
            }

            public void setSubjectname(String subjectname) {
                this.subjectname = subjectname;
            }

            public String getSubjectpicture() {
                return subjectpicture;
            }

            public void setSubjectpicture(String subjectpicture) {
                this.subjectpicture = subjectpicture;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    public static class Studentlist3Bean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KtxxActivity
         * dwctitle : 待完成:1
         * name : 课堂学习
         * studentitemlist : [{"act":"app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.KtxxActivity?subjectid=","curriculumname":"光合作用","subjectid":1,"subjectname":"生物","subjectpicture":"https://file.fuzhuxian.com/new_icon_shengwu.png","time":"2020-09-29 17:23:03"}]
         */

        private String act;
        private String dwctitle;
        private String name;
        private List<StudentitemlistBeanXX> studentitemlist;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDwctitle() {
            return dwctitle;
        }

        public void setDwctitle(String dwctitle) {
            this.dwctitle = dwctitle;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<StudentitemlistBeanXX> getStudentitemlist() {
            return studentitemlist;
        }

        public void setStudentitemlist(List<StudentitemlistBeanXX> studentitemlist) {
            this.studentitemlist = studentitemlist;
        }

        public static class StudentitemlistBeanXX {
            /**
             * act : app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.KtxxActivity?subjectid=
             * curriculumname : 光合作用
             * subjectid : 1
             * subjectname : 生物
             * subjectpicture : https://file.fuzhuxian.com/new_icon_shengwu.png
             * time : 2020-09-29 17:23:03
             */

            private String act;
            private String curriculumname;
            private String subjectid;
            private String subjectname;
            private String subjectpicture;
            private String time;

            public String getAct() {
                return act;
            }

            public void setAct(String act) {
                this.act = act;
            }

            public String getCurriculumname() {
                return curriculumname;
            }

            public void setCurriculumname(String curriculumname) {
                this.curriculumname = curriculumname;
            }

            public String getSubjectid() {
                return subjectid;
            }

            public void setSubjectid(String subjectid) {
                this.subjectid = subjectid;
            }

            public String getSubjectname() {
                return subjectname;
            }

            public void setSubjectname(String subjectname) {
                this.subjectname = subjectname;
            }

            public String getSubjectpicture() {
                return subjectpicture;
            }

            public void setSubjectpicture(String subjectpicture) {
                this.subjectpicture = subjectpicture;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    public static class WdksBean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KSActivity
         * details : 课堂检测，知识测评
         * name : 我的考试
         * picture : https://file.fuzhuxian.com/new_icon_kaoshi.png
         */

        private String act;
        private String details;
        private String name;
        private String picture;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }
    }

    public static class WdpgBean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdpgActivity
         * details : 同步练习，知识点练习
         * name : 我的批改
         * picture : https://file.fuzhuxian.com/new_icon_taolun.png
         */

        private String act;
        private String details;
        private String name;
        private String picture;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }
    }

    public static class XqfxBean {
        /**
         * act : app://cs.znclass.com/com.sdzn.fzx.student.hs.act.XqfxShouyeActivity
         * details : 我的学科水平变化趋势
         * name : 学情分析
         * picture : https://file.fuzhuxian.com/new_icon_xqfx.png
         */

        private String act;
        private String details;
        private String name;
        private String picture;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }
    }
}
