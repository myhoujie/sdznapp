package com.sdzn.fzx.student.vo;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/23
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class UserBean {

    /**
     * data : {"id":7,"realName":"张超","accountId":31,"accountName":"07851634","available":1,"nickName":null,"sex":1,"photo":"http://192.168.0.215:8886/head/2018/01/a1db8912fd2d40d08cb83121d84fc9bb.jpg","createAccountId":3,"createUserId":null,"createUserName":"93746521","createTime":1515979145000,"updateTime":null,"userType":4,"telephone":"13260188138","accountRoleList":null,"email":null,"identityId":"210000199212024494","birthday":1516636800000,"zoneId":null,"zoneName":null,"zoneIdPath":null,"zoneNamePath":null,"baseLevelId":1,"baseLevelName":"小学","baseEducationId":1,"baseEducationName":"六三制","baseGradeId":3,"baseGradeName":"三年级","baseVersionId":null,"baseVersionName":null,"baseVolumeId":null,"baseVolumeName":null,"scope":2017,"customerSchoolName":"山东大学附属中学","customerSchoolId":1}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 7
         * realName : 张超
         * accountId : 31
         * accountName : 07851634
         * available : 1
         * nickName : null
         * sex : 1
         * photo : http://192.168.0.215:8886/head/2018/01/a1db8912fd2d40d08cb83121d84fc9bb.jpg
         * createAccountId : 3
         * createUserId : null
         * createUserName : 93746521
         * createTime : 1515979145000
         * updateTime : null
         * userType : 4
         * telephone : 13260188138
         * accountRoleList : null
         * email : null
         * identityId : 210000199212024494
         * birthday : 1516636800000
         * zoneId : null
         * zoneName : null
         * zoneIdPath : null
         * zoneNamePath : null
         * baseLevelId : 1
         * baseLevelName : 小学
         * baseEducationId : 1
         * baseEducationName : 六三制
         * baseGradeId : 3
         * baseGradeName : 三年级
         * baseVersionId : null
         * baseVersionName : null
         * baseVolumeId : null
         * baseVolumeName : null
         * scope : 2017
         * customerSchoolName : 山东大学附属中学
         * customerSchoolId : 1
         */

        private int id;
        private String realName;
        private String genderName;
        private String account;
        private String avatar;
        private String mobile;
        private String levelName;
        private String gradeName;
        private String schoolName;
        private String teacherName;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public String getGenderName() {
            return genderName;
        }

        public void setGenderName(String genderName) {
            this.genderName = genderName;
        }

        public String getAccountName() {
            return account;
        }

        public void setAccountName(String accountName) {
            this.account = accountName;
        }



        public String getPhoto() {
            if (avatar!=null) {
                return avatar;
            }
            return "";
        }

        public void setPhoto(String photo) {
            this.avatar = photo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }


        public String getLevelName() {
            return levelName;
        }

        public void setLevelName(String levelName) {
            this.levelName = levelName;
        }

        public String getGradeName() {
            return gradeName;
        }

        public void setGradeName(String gradeName) {
            this.gradeName = gradeName;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }
    }
}
