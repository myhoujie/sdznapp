package com.sdzn.fzx.student.presenter;


import com.sdzn.fzx.student.view.ChangepasswordViews;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.fzx.student.api.Api;
import com.sdzn.fzx.student.libutils.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class ChangepasswordPresenter1 extends Presenter<ChangepasswordViews> {

    public void changePassword(String token,String id,String oldPassword,String newPassword) {
        RetrofitNetNew.build(Api.class, getIdentifier())
                .updatePassword(token, id, oldPassword, newPassword)
                .enqueue(new Callback<ResponseSlbBean1<Object>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<Object>> call, Response<ResponseSlbBean1<Object>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onFailed(response.body().getMessage());
                            return;
                        }
                        getView().changePassswordSuccess();

                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<Object>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        Log.d("密码修改", "22222" + call.toString());
                        getView().onFailed("密码修改失败");
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
//    public void changePassword(String oldPassword, String newPassword, String confirmPassword) {
//        Network.createTokenService(NetWorkService.ChangePasswordService.class)
//                .ChangePassword(oldPassword, newPassword, confirmPassword)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onNext(Object o) {
//                        mView.changePassswordSuccess();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof ApiException) {
//                            if (((ApiException) e).getStatus().getCode() == 21009) {
//                                ToastUtil.showLonglToast("旧密码输入错误，请重新输入");
//                            }
//                            if (((ApiException) e).getStatus().getCode() == 21014) {
//                                ToastUtil.showLonglToast("新密码和原密码相同");
//                            }
//                        }
//
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity));
//
//    }
}
