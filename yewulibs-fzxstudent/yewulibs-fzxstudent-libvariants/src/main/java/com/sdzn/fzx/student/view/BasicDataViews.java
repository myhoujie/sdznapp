package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.vo.UserBean;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/1/15
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public interface BasicDataViews extends IView {

    void getUserInfo(UserBean loginBean);
}
