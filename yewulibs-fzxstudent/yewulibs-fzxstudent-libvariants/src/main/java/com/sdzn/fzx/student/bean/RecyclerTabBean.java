package com.sdzn.fzx.student.bean;

import java.io.Serializable;
import java.util.List;

public class RecyclerTabBean implements Serializable {

    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 8
         * name : 备课记录
         * url : autonomicLearningParent/autonomicLearning/oneselfStudy
         * type : 3
         */

        private String id;
        private String name;
        private String url;
        private int type;
        private boolean enable;

        public ListBean(String name, String url) {
            this.name = name;
            this.url = url;
        }

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
