package com.sdzn.fzx.student.api;

/**
 * 描述：API请求地址
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ApiInterface {
    public static final String QUERY_VERSION_INFO = "/usercenter/sysVersionAppInfo/selectOne";//查询版本信息
    public static final String QUERY_CEHUA_INFO = "/course/home/student/broadside/list";//侧边栏数据
    public static final String QUERY_TAB_INFO = "/course/home/tabJump/list";//动态获取tab数据
    public static final String QUERY_SHOUYE_INFO = "/course/home/student/data";//首页数据
    public static final String LOGIN_INFO = "/student/login/token";//学生端登录
    public static final String SEND_VERIFYCODE = "/student/login/sendVerifyCode";//发送短信验证码
    public static final String CHECK_VERIFYCODE = "/teacher/check/smsToken";//验证验证码
    public static final String STUDENT_INFO = "/usercenter/student/find/StudentUserCenter";//个人中心获取学生资料
    public static final String STUDENT_MOBILE_UPDATE = "/usercenter/student/update/mobile";//个人中心更新-手机号
    public static final String STUDENT_PASSWORD_UPDATE = "/usercenter/student/update/password";//个人中心更新-密码
    public static final String STUDENT_LOGIN_MOBILE = "/usercenter/student/find/studentMobile";//忘记密码--通过学生账号/手机号 获取手机号
    public static final String STUDENT_LOGIN_PASSWORD_UPDATE = "/usercenter/student/student/forget/password";//忘记密码--更新密码
}
