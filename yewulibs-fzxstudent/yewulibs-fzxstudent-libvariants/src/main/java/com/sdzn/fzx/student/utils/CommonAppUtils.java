package com.sdzn.fzx.student.utils;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.Uri;

import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.fzx.student.vo.SSOInfoBean;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class CommonAppUtils {

    private static final String Name = "Name";
    private static final String Pwd = "Pwd";
    private static final String AutoLogin = "autoLogin";
    private static final String SEED = "com.sdzn.fzx";

    public static SSOInfoBean getSSOInfo() {
        SSOInfoBean bean = null;
        Uri uri = Uri.parse("content://com.linspirer.android.fzxsso.provider.UserProvider/user/query");
        Cursor cursor = App2.get().getContentResolver().query(uri, new String[]{Name, Pwd, AutoLogin}, null, null, null);
        if (cursor == null) {
            return bean;
        }
        if (cursor.getCount() > 0 && cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(Name));
            String pwd = cursor.getString(cursor.getColumnIndex(Pwd));
            boolean autoLogin = cursor.getString(cursor.getColumnIndex(AutoLogin)).equals("1");
            try {
                name = decrypt(name);
                pwd = decrypt(pwd);
            } catch (Exception e) {
                e.printStackTrace();
            }
            bean = new SSOInfoBean(name, pwd, autoLogin);
        }
        cursor.close();
        return bean;
    }

    public static void logoutSSO() {
        // TODO: 2018-11-12 注销时将对应数据autoLogin字段改false
    }

    /**
     * 字符串解密
     */
    public static String decrypt(String content) throws Exception {
        // 创建AES秘钥
        SecretKeySpec key = new SecretKeySpec(SEED.getBytes(), "AES/CBC/PKCS5PADDING");
        // 创建密码器
        Cipher cipher = Cipher.getInstance("AES");
        // 初始化解密器
        cipher.init(Cipher.DECRYPT_MODE, key);
        // 解密
        byte[] decrypt = cipher.doFinal(content.getBytes("UTF-8"));
        return new String(decrypt, "UTF-8");
    }

    /**
     * 字符串加密
     */
    public static String encrypt(String content) throws Exception {
        // 创建AES秘钥
        SecretKeySpec key = new SecretKeySpec(SEED.getBytes(), "AES/CBC/PKCS5PADDING");
        // 创建密码器
        Cipher cipher = Cipher.getInstance("AES");
        // 初始化加密器
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // 加密
        byte[] bytes = cipher.doFinal(content.getBytes("UTF-8"));
        return new String(bytes, "UTF-8");
    }

    /*================ 领创对接 ================*/

    //判断当前rom是否为领创定制版
    public static boolean isLingChuangPad() {
        try {
            PackageInfo info = App2.get().getPackageManager().getPackageInfo("com.android.launcher3", 0);
            String appName = info.applicationInfo.loadLabel(App2.get().getPackageManager()).toString();
            Log.e("领创平板管理", "名字" + appName);
            return "领创平板管理".equals(appName);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 登录接口
     */
    public static void login(String userId, String userName, String schoolId, String className, String txUrl, String token) {
        Intent intent = new Intent("com.linspirer.edu.loginapkfinish");
        intent.putExtra("userid", userId);
        intent.putExtra("username", userName);
        intent.putExtra("schoolid", schoolId);
        intent.putExtra("classname", className);
        intent.putExtra("txurl", txUrl);
        intent.putExtra("token", token);
        App2.get().sendBroadcast(intent);
    }

    /**
     * 注销接口
     */
    public static void logout() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.logout"));
    }

    /**
     * 锁定按键 包括 Home键、 Back键、 Recent键、音量键、电源键、短按截屏组合键
     *
     * @param classLast todo 不知道什么意思
     * @param time      锁定时间, 单位分钟
     */
    public static void lockKey(String classLast, int time) {
        Intent intent = new Intent("com.linspirer.edu.class.begin");
        intent.putExtra(classLast, time);
        App2.get().sendBroadcast(intent);
    }

    /**
     * 解锁按键
     */
    public static void unLockKey() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.class.over"));
    }

    /**
     * 回到桌面
     */
    public static void returnLauncher() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.returnlauncher"));
    }

    /**
     * 禁用back键
     */
    public static void disableBack() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.disableback"));
    }

    /**
     * 启用back键
     */
    public static void enableBack() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.enableback"));
    }

    /**
     * 禁用home
     */
    public static void disableHome() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.disablehome"));
    }

    /**
     * 启用home
     */
    public static void enableHome() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.enablehome"));
    }

    /**
     * 禁用recent键
     */
    public static void disableRecent() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.disablerecent"));
    }

    /**
     * 启用recent键
     */
    public static void enableRecent() {
        App2.get().sendBroadcast(new Intent("com.linspirer.edu.enablerecent"));
    }

    /**
     * 呼叫管理员界面
     */
    public static void openAdmin() {
        App2.get().sendBroadcast(new Intent("com.android.launcher3.mdm.OPEN_ADMIN"));
    }

    /**
     * 检查更新
     */
    public static void checkUpdate() {
        App2.get().sendBroadcast(new Intent("com.android.laucher3.mdm.CHECK_UPDATE"));
    }
}
