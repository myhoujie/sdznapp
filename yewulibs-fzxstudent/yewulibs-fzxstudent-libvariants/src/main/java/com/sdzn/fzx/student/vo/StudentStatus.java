package com.sdzn.fzx.student.vo;

import java.util.List;

public class StudentStatus {
    /**
     * data : {"Inclass":{"id":16,"classId":604,"schoolId":36,"teacherId":216,"status":0,"classTime":null},"classStatus":[{"id":16,"classId":604,"schoolId":36,"teacherId":216,"status":0,"classTime":null}],"classScreen":[{"id":25,"teacherId":216,"teacherSchoolId":36,"teacherClassId":604,"status":0}],"Screen":{"id":6,"studentId":4596,"studentSchoolId":1,"studentClassId":604,"status":0}}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * Inclass : {"id":16,"classId":604,"schoolId":36,"teacherId":216,"status":0,"classTime":null}
         * classStatus : [{"id":16,"classId":604,"schoolId":36,"teacherId":216,"status":0,"classTime":null}]
         * classScreen : [{"id":25,"teacherId":216,"teacherSchoolId":36,"teacherClassId":604,"status":0}]
         * Screen : {"id":6,"studentId":4596,"studentSchoolId":1,"studentClassId":604,"status":0}
         */

        private InclassBean Inclass;
        private ScreenBean Screen;
        private List<ClassStatusBean> classStatus;
        private List<ClassScreenBean> classScreen;

        public InclassBean getInclass() {
            return Inclass;
        }

        public void setInclass(InclassBean Inclass) {
            this.Inclass = Inclass;
        }

        public ScreenBean getScreen() {
            return Screen;
        }

        public void setScreen(ScreenBean Screen) {
            this.Screen = Screen;
        }

        public List<ClassStatusBean> getClassStatus() {
            return classStatus;
        }

        public void setClassStatus(List<ClassStatusBean> classStatus) {
            this.classStatus = classStatus;
        }

        public List<ClassScreenBean> getClassScreen() {
            return classScreen;
        }

        public void setClassScreen(List<ClassScreenBean> classScreen) {
            this.classScreen = classScreen;
        }

        public static class InclassBean {
            /**
             * id : 16
             * classId : 604
             * schoolId : 36
             * teacherId : 216
             * status : 0
             * classTime : null
             */

            private int id;
            private int classId;
            private int schoolId;
            private int teacherId;
            private int status;
            private Object classTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getClassId() {
                return classId;
            }

            public void setClassId(int classId) {
                this.classId = classId;
            }

            public int getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(int schoolId) {
                this.schoolId = schoolId;
            }

            public int getTeacherId() {
                return teacherId;
            }

            public void setTeacherId(int teacherId) {
                this.teacherId = teacherId;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getClassTime() {
                return classTime;
            }

            public void setClassTime(Object classTime) {
                this.classTime = classTime;
            }
        }

        public static class ScreenBean {
            /**
             * id : 6
             * studentId : 4596
             * studentSchoolId : 1
             * studentClassId : 604
             * status : 0
             */

            private int id;
            private int studentId;
            private int studentSchoolId;
            private int studentClassId;
            private int status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getStudentId() {
                return studentId;
            }

            public void setStudentId(int studentId) {
                this.studentId = studentId;
            }

            public int getStudentSchoolId() {
                return studentSchoolId;
            }

            public void setStudentSchoolId(int studentSchoolId) {
                this.studentSchoolId = studentSchoolId;
            }

            public int getStudentClassId() {
                return studentClassId;
            }

            public void setStudentClassId(int studentClassId) {
                this.studentClassId = studentClassId;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }

        public static class ClassStatusBean {
            /**
             * id : 16
             * classId : 604
             * schoolId : 36
             * teacherId : 216
             * status : 0
             * classTime : null
             */

            private int id;
            private int classId;
            private int schoolId;
            private int teacherId;
            private int status;
            private Object classTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getClassId() {
                return classId;
            }

            public void setClassId(int classId) {
                this.classId = classId;
            }

            public int getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(int schoolId) {
                this.schoolId = schoolId;
            }

            public int getTeacherId() {
                return teacherId;
            }

            public void setTeacherId(int teacherId) {
                this.teacherId = teacherId;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getClassTime() {
                return classTime;
            }

            public void setClassTime(Object classTime) {
                this.classTime = classTime;
            }
        }

        public static class ClassScreenBean {
            /**
             * id : 25
             * teacherId : 216
             * teacherSchoolId : 36
             * teacherClassId : 604
             * status : 0
             */

            private int id;
            private int teacherId;
            private int teacherSchoolId;
            private int teacherClassId;
            private int status;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getTeacherId() {
                return teacherId;
            }

            public void setTeacherId(int teacherId) {
                this.teacherId = teacherId;
            }

            public int getTeacherSchoolId() {
                return teacherSchoolId;
            }

            public void setTeacherSchoolId(int teacherSchoolId) {
                this.teacherSchoolId = teacherSchoolId;
            }

            public int getTeacherClassId() {
                return teacherClassId;
            }

            public void setTeacherClassId(int teacherClassId) {
                this.teacherClassId = teacherClassId;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }


    /**
     * data : {"Inclass":{"id":null,"studentId":4597,"studentName":null,"studentSchoolId":null,"studentClassId":null,"status":0},"Screen":{"id":null,"studentId":4597,"studentSchoolId":null,"studentClassId":null,"status":0}}
     */


}
