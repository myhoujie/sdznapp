package com.sdzn.fzx.student.view;

import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;

/**
 * 首页 view
 */
public interface ShouyeViews extends IView {
    void onCehuaSuccess(ShouyeRecActBean grzxRecActBean);

    void onCehuaNodata(String msg);

    void onCehuaFail(String msg);
}
