package com.sdzn.fzx.student.vo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

/**
 * @author Reisen at 2018-08-28
 */
public class AnswerListBean {


    private List<AnswerDataBean> data;

    public List<AnswerDataBean> getData() {
        return data;
    }

    public void setData(List<AnswerDataBean> data) {
        this.data = data;
    }

    public static class AnswerDataBean {

        private long id;
        private String examId;//试题id 文字资源为0
        private int examTemplateId;//样式题型id
        private int examTemplateStyleId;//学科题型id
        private String examTemplateStyleName;//题型名字
        private int examEmptyCount;//填空题空格数量
        private int examDifficulty;//难度
        private long resourceId;//资源id
        private int resourceType;//资源类型
        private String resourceName;//资源名称
        private int type;//1=试题 2=资料 3=文字
        private long lessonLibId;//课程id
        private long parentId;//大题id；0说明该题没有小题
        private int seq;//试卷中试题排序
        private long lessonTaskId;//任务id
        private String examText;//试题文本
        private transient AnswerListBean.ExamTextBean examBean;
        private transient ResourceVo.ResourceTextVo resourceVoBean;
        private String resourceText;//资源文本；一般为一个路径
        private float score;//分数
        private int examSeq;//试题序号
        private int isAnswer;//已答=1, 未答=0
        private boolean isCollect;//是否收藏
        private long studentBookId;//试题收藏id
        private long lessonAnswerExamId;//作答试题记录id
        private long lessonAnswerExamParentId;
        private int isRight;/*1=正确 2=错误 3=半对 0为初始值*/
        private List<ExamOptionBean> examOptionList;//回答内容list
        private List<AnswerDataBean> childList;//小题列表
        private List<AnswerDataBean> examList;//综合题试题列表
        private long useTime;
        private int isRead;
        private int status;//1 = 支持键盘作答， 2 = 不支持

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getUseTime() {
            return useTime;
        }

        public void setUseTime(long useTime) {
            this.useTime = useTime;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        @Nullable
        public ResourceVo.ResourceTextVo getResourceVoBean() {
            return resourceVoBean;
        }

        public void setResourceVoBean(ResourceVo.ResourceTextVo resourceVoBean) {
            this.resourceVoBean = resourceVoBean;
        }

        @Nullable
        public ExamTextBean getExamBean() {
            return examBean;
        }

        public void setExamBean(ExamTextBean examBean) {
            this.examBean = examBean;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getExamId() {
            return examId;
        }

        public void setExamId(String examId) {
            this.examId = examId;
        }

        public int getExamTemplateId() {
            return examTemplateId;
        }

        public void setExamTemplateId(int examTemplateId) {
            this.examTemplateId = examTemplateId;
        }

        public int getExamTemplateStyleId() {
            return examTemplateStyleId;
        }

        public void setExamTemplateStyleId(int examTemplateStyleId) {
            this.examTemplateStyleId = examTemplateStyleId;
        }

        public String getExamTemplateStyleName() {
            return getExamType();
        }

        private String getExamType() {
            switch (examTemplateId) {
                case 1:
                    return "单选题";
                case 2:
                    return "多选题";
                case 3:
                    return "判断题";
                case 4:
                    return "简答题";
                case 6:
                    return "填空题";
                case 14:
                    return "完形填空";
                case 16:
                    return "综合题";
                default:
                    return "未知题型";
            }
        }

        public void setExamTemplateStyleName(String examTemplateStyleName) {
            this.examTemplateStyleName = examTemplateStyleName;
        }

        public int getExamEmptyCount() {
            return examEmptyCount;
        }

        public void setExamEmptyCount(int examEmptyCount) {
            this.examEmptyCount = examEmptyCount;
        }

        public int getExamDifficulty() {
            return examDifficulty;
        }

        public void setExamDifficulty(int examDifficulty) {
            this.examDifficulty = examDifficulty;
        }

        public long getResourceId() {
            return resourceId;
        }

        public void setResourceId(long resourceId) {
            this.resourceId = resourceId;
        }

        public int getResourceType() {
            return resourceType;
        }

        public void setResourceType(int resourceType) {
            this.resourceType = resourceType;
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public long getLessonLibId() {
            return lessonLibId;
        }

        public void setLessonLibId(long lessonLibId) {
            this.lessonLibId = lessonLibId;
        }

        public long getParentId() {
            return parentId;
        }

        public void setParentId(long parentId) {
            this.parentId = parentId;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public long getLessonTaskId() {
            return lessonTaskId;
        }

        public void setLessonTaskId(long lessonTaskId) {
            this.lessonTaskId = lessonTaskId;
        }

        public String getExamText() {
            return examText;
        }

        public void setExamText(String examText) {
            this.examText = examText;
        }

        public String getResourceText() {
            return resourceText;
        }

        public void setResourceText(String resourceText) {
            this.resourceText = resourceText;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public int getExamSeq() {
            return examSeq;
        }

        public void setExamSeq(int examSeq) {
            this.examSeq = examSeq;
        }

        public int getIsAnswer() {
            return isAnswer;
        }

        public void setIsAnswer(int isAnswer) {
            this.isAnswer = isAnswer;
        }

        public boolean isCollect() {
            return isCollect;
        }

        public void setCollect(boolean collect) {
            isCollect = collect;
        }

        public long getStudentBookId() {
            return studentBookId;
        }

        public void setStudentBookId(long studentBookId) {
            this.studentBookId = studentBookId;
        }

        public long getLessonAnswerExamId() {
            return lessonAnswerExamId;
        }

        public void setLessonAnswerExamId(long lessonAnswerExamId) {
            this.lessonAnswerExamId = lessonAnswerExamId;
        }

        public long getLessonAnswerExamParentId() {
            return lessonAnswerExamParentId;
        }

        public void setLessonAnswerExamParentId(long lessonAnswerExamParentId) {
            this.lessonAnswerExamParentId = lessonAnswerExamParentId;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public List<ExamOptionBean> getExamOptionList() {
            return examOptionList;
        }

        public void setExamOptionList(List<ExamOptionBean> examOptionList) {
            this.examOptionList = examOptionList;
        }

        public List<AnswerDataBean> getChildList() {
            return childList;
        }

        public void setChildList(List<AnswerDataBean> childList) {
            this.childList = childList;
        }

        public List<AnswerDataBean> getExamList() {
            return examList;
        }

        public void setExamList(List<AnswerDataBean> examList) {
            this.examList = examList;
        }
    }

    /**
     * 题目bean
     */
    public static class ExamTextBean {

        private int baseEducationId;
        private String baseEducationName;
        private int baseGradeId;
        private String baseGradeName;
        private int baseLevelId;
        private String baseLevelName;
        private int baseSubjectId;
        private String baseSubjectName;
        private int baseVersionId;
        private String baseVersionName;
        private int baseVolumeId;
        private String baseVolumeName;
        private int chapterId;
        private String chapterName;
        private String chapterNodeIdPath;
        private String chapterNodeNamePath;
        private long createTime;
        private int createUserId;
        private String createUserName;
        private int customerSchoolId;
        private String customerSchoolName;
        private int difficulty;
        private String examAnalysis;
        private String examAnswer;
        private String examExplain;
        private String examStem;
        private int examTypeId;//题目类型
        private int flagShare;
        private int holdUserId;
        private String holdUserName;
        private int hots;
        private String id;
        private int optionNumber;
        private int templateStyleId;
        private String templateStyleName;
        private int type;
        private long updateTime;
        private List<ExamOptions> examOptions;
        private List<ExamTextBean> examBases;

        public List<ExamTextBean> getExamBases() {
            return examBases;
        }

        public void setExamBases(List<ExamTextBean> examBases) {
            this.examBases = examBases;
        }

        public int getBaseEducationId() {
            return baseEducationId;
        }

        public void setBaseEducationId(int baseEducationId) {
            this.baseEducationId = baseEducationId;
        }

        public String getBaseEducationName() {
            return baseEducationName;
        }

        public void setBaseEducationName(String baseEducationName) {
            this.baseEducationName = baseEducationName;
        }

        public int getBaseGradeId() {
            return baseGradeId;
        }

        public void setBaseGradeId(int baseGradeId) {
            this.baseGradeId = baseGradeId;
        }

        public String getBaseGradeName() {
            return baseGradeName;
        }

        public void setBaseGradeName(String baseGradeName) {
            this.baseGradeName = baseGradeName;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public int getBaseVersionId() {
            return baseVersionId;
        }

        public void setBaseVersionId(int baseVersionId) {
            this.baseVersionId = baseVersionId;
        }

        public String getBaseVersionName() {
            return baseVersionName;
        }

        public void setBaseVersionName(String baseVersionName) {
            this.baseVersionName = baseVersionName;
        }

        public int getBaseVolumeId() {
            return baseVolumeId;
        }

        public void setBaseVolumeId(int baseVolumeId) {
            this.baseVolumeId = baseVolumeId;
        }

        public String getBaseVolumeName() {
            return baseVolumeName;
        }

        public void setBaseVolumeName(String baseVolumeName) {
            this.baseVolumeName = baseVolumeName;
        }

        public int getChapterId() {
            return chapterId;
        }

        public void setChapterId(int chapterId) {
            this.chapterId = chapterId;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getChapterNodeIdPath() {
            return chapterNodeIdPath;
        }

        public void setChapterNodeIdPath(String chapterNodeIdPath) {
            this.chapterNodeIdPath = chapterNodeIdPath;
        }

        public String getChapterNodeNamePath() {
            return chapterNodeNamePath;
        }

        public void setChapterNodeNamePath(String chapterNodeNamePath) {
            this.chapterNodeNamePath = chapterNodeNamePath;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public int getCustomerSchoolId() {
            return customerSchoolId;
        }

        public void setCustomerSchoolId(int customerSchoolId) {
            this.customerSchoolId = customerSchoolId;
        }

        public String getCustomerSchoolName() {
            return customerSchoolName;
        }

        public void setCustomerSchoolName(String customerSchoolName) {
            this.customerSchoolName = customerSchoolName;
        }

        public int getDifficulty() {
            return difficulty;
        }

        public void setDifficulty(int difficulty) {
            this.difficulty = difficulty;
        }

        public String getExamAnalysis() {
            return examAnalysis;
        }

        public void setExamAnalysis(String examAnalysis) {
            this.examAnalysis = examAnalysis;
        }

        public String getExamAnswer() {
            return examAnswer;
        }

        public void setExamAnswer(String examAnswer) {
            this.examAnswer = examAnswer;
        }

        public String getExamExplain() {
            return examExplain;
        }

        public void setExamExplain(String examExplain) {
            this.examExplain = examExplain;
        }

        public String getExamStem() {
            return examStem;
        }

        public void setExamStem(String examStem) {
            this.examStem = examStem;
        }

        public int getExamTypeId() {
            return examTypeId;
        }

        public void setExamTypeId(int examTypeId) {
            this.examTypeId = examTypeId;
        }

        public int getFlagShare() {
            return flagShare;
        }

        public void setFlagShare(int flagShare) {
            this.flagShare = flagShare;
        }

        public int getHoldUserId() {
            return holdUserId;
        }

        public void setHoldUserId(int holdUserId) {
            this.holdUserId = holdUserId;
        }

        public String getHoldUserName() {
            return holdUserName;
        }

        public void setHoldUserName(String holdUserName) {
            this.holdUserName = holdUserName;
        }

        public int getHots() {
            return hots;
        }

        public void setHots(int hots) {
            this.hots = hots;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getOptionNumber() {
            return optionNumber;
        }

        public void setOptionNumber(int optionNumber) {
            this.optionNumber = optionNumber;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public List<ExamOptions> getExamOptions() {
            return examOptions;
        }

        public void setExamOptions(List<ExamOptions> examOptions) {
            this.examOptions = examOptions;
        }
    }

    /**
     * 答题内容bean
     */
    public static class ExamOptionBean implements Comparable<ExamOptionBean> {

        private int id;//试题答案选项id
        private int isRight;//是否正确答案
        private int lessonAnswerExamId;//试题答案id
        private int lessonTaskStudentId;//学生对应试卷的id
        /**
         * 先按;切, 再按,切
         */
        private String myAnswer;//答题内容, 填空题
        private String myAnswerHtml;
        private String myOldAnswer;//最开始提交的作答结果集
        private int seq;//选择:第几个选项;填空:第几个空;完形填空:第几个小题;简答:2=拍照1=手写;
        private int answerType;//填空：0 = 未作答，1 = 键盘作答， 2 = 手写作答， 3 = 拍照作答

        public int getAnswerType() {
            return answerType;
        }

        public void setAnswerType(int answerType) {
            this.answerType = answerType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsRight() {
            return isRight;
        }

        public void setIsRight(int isRight) {
            this.isRight = isRight;
        }

        public int getLessonAnswerExamId() {
            return lessonAnswerExamId;
        }

        public void setLessonAnswerExamId(int lessonAnswerExamId) {
            this.lessonAnswerExamId = lessonAnswerExamId;
        }

        public int getLessonTaskStudentId() {
            return lessonTaskStudentId;
        }

        public void setLessonTaskStudentId(int lessonTaskStudentId) {
            this.lessonTaskStudentId = lessonTaskStudentId;
        }

        public String getMyAnswer() {
            return myAnswer;
        }

        public void setMyAnswer(String myAnswer) {
            this.myAnswer = myAnswer;
        }

        public String getMyAnswerHtml() {
            return myAnswerHtml;
        }

        public void setMyAnswerHtml(String myAnswerHtml) {
            this.myAnswerHtml = myAnswerHtml;
        }

        public String getMyOldAnswer() {
            return myOldAnswer;
        }

        public void setMyOldAnswer(String myOldAnswer) {
            this.myOldAnswer = myOldAnswer;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptionBean o) {
            return Integer.compare(seq, o.seq);
        }
    }

    public static class ExamOptions implements Comparable<ExamOptions> {
        private String analysis;
        private String content;
        private boolean right;
        private int seq;//选项序号
        private List<ExamOptions> list;

        public List<ExamOptions> getList() {
            return list;
        }

        public void setList(List<ExamOptions> list) {
            this.list = list;
        }

        public String getAnalysis() {
            return analysis;
        }

        public void setAnalysis(String analysis) {
            this.analysis = analysis;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptions o) {
            return Integer.compare(seq, o.seq);
        }
    }
}
