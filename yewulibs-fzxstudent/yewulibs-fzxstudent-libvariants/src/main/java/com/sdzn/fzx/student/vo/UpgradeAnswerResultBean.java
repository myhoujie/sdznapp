package com.sdzn.fzx.student.vo;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/15.
 */
public class UpgradeAnswerResultBean implements Comparable<UpgradeAnswerResultBean>,Parcelable {
    private int examSeq;
    private long id;
    private long intelTaskId;
    private int isRight;
    private String myAnswer;//作答选项
    private long questionId;
    private RyExam ryExam;
    private String ryExamId;
    private double score;
    private int templateId;
    private int type;
    private String templateStyleName;//题型

    private int examTypeId;//样式题型id

    protected UpgradeAnswerResultBean(Parcel in) {
        examSeq = in.readInt();
        id = in.readLong();
        intelTaskId = in.readLong();
        isRight = in.readInt();
        myAnswer = in.readString();
        questionId = in.readLong();
        ryExamId = in.readString();
        score = in.readDouble();
        templateId = in.readInt();
        type = in.readInt();
        templateStyleName = in.readString();
        examTypeId = in.readInt();
    }

    public static final Creator<UpgradeAnswerResultBean> CREATOR = new Creator<UpgradeAnswerResultBean>() {
        @Override
        public UpgradeAnswerResultBean createFromParcel(Parcel in) {
            return new UpgradeAnswerResultBean(in);
        }

        @Override
        public UpgradeAnswerResultBean[] newArray(int size) {
            return new UpgradeAnswerResultBean[size];
        }
    };

    public int getExamTypeId() {
        return examTypeId;
    }

    public void setExamTypeId(int examTypeId) {
        this.examTypeId = examTypeId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTemplateStyleName() {
        return templateStyleName;
    }

    public void setTemplateStyleName(String templateStyleName) {
        this.templateStyleName = templateStyleName;
    }

    public int getExamSeq() {
        return examSeq;
    }

    public void setExamSeq(int examSeq) {
        this.examSeq = examSeq;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIntelTaskId() {
        return intelTaskId;
    }

    public void setIntelTaskId(long intelTaskId) {
        this.intelTaskId = intelTaskId;
    }

    public int getIsRight() {
        return isRight;
    }

    public void setIsRight(int isRight) {
        this.isRight = isRight;
    }

    public String getMyAnswer() {
        return myAnswer;
    }

    public void setMyAnswer(String myAnswer) {
        this.myAnswer = myAnswer;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public RyExam getRyExam() {
        return ryExam;
    }

    public void setRyExam(RyExam ryExam) {
        this.ryExam = ryExam;
    }

    public String getRyExamId() {
        return ryExamId;
    }

    public void setRyExamId(String ryExamId) {
        this.ryExamId = ryExamId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    @Override
    public int compareTo(@NonNull UpgradeAnswerResultBean o) {
        return Integer.compare(examSeq,o.examSeq);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(examSeq);
        dest.writeLong(id);
        dest.writeLong(intelTaskId);
        dest.writeInt(isRight);
        dest.writeString(myAnswer);
        dest.writeLong(questionId);
        dest.writeString(ryExamId);
        dest.writeDouble(score);
        dest.writeInt(templateId);
        dest.writeInt(type);
        dest.writeString(templateStyleName);
        dest.writeInt(examTypeId);
    }

    public static class RyExam implements Parcelable{
        private String abilityStructureRy;
        private String analyticMethodRy;
        private int baseLevelId;
        private String baseLevelName;
        private int baseSubjectId;
        private String baseSubjectName;
        private long createTime;
        private int difficulty;
        private int downCount;
        private String examAnalysis;
        private String examAnswer;
        private List<ExamOptions> examOptions;
        private String examStem;
        private int templateStyleId;
        private String templateStyleName;
        private int type;

        protected RyExam(Parcel in) {
            abilityStructureRy = in.readString();
            analyticMethodRy = in.readString();
            baseLevelId = in.readInt();
            baseLevelName = in.readString();
            baseSubjectId = in.readInt();
            baseSubjectName = in.readString();
            createTime = in.readLong();
            difficulty = in.readInt();
            downCount = in.readInt();
            examAnalysis = in.readString();
            examAnswer = in.readString();
            examStem = in.readString();
            templateStyleId = in.readInt();
            templateStyleName = in.readString();
            type = in.readInt();
        }

        public static final Creator<RyExam> CREATOR = new Creator<RyExam>() {
            @Override
            public RyExam createFromParcel(Parcel in) {
                return new RyExam(in);
            }

            @Override
            public RyExam[] newArray(int size) {
                return new RyExam[size];
            }
        };

        public String getAbilityStructureRy() {
            return abilityStructureRy;
        }

        public void setAbilityStructureRy(String abilityStructureRy) {
            this.abilityStructureRy = abilityStructureRy;
        }

        public String getAnalyticMethodRy() {
            return analyticMethodRy;
        }

        public void setAnalyticMethodRy(String analyticMethodRy) {
            this.analyticMethodRy = analyticMethodRy;
        }

        public int getBaseLevelId() {
            return baseLevelId;
        }

        public void setBaseLevelId(int baseLevelId) {
            this.baseLevelId = baseLevelId;
        }

        public String getBaseLevelName() {
            return baseLevelName;
        }

        public void setBaseLevelName(String baseLevelName) {
            this.baseLevelName = baseLevelName;
        }

        public int getBaseSubjectId() {
            return baseSubjectId;
        }

        public void setBaseSubjectId(int baseSubjectId) {
            this.baseSubjectId = baseSubjectId;
        }

        public String getBaseSubjectName() {
            return baseSubjectName;
        }

        public void setBaseSubjectName(String baseSubjectName) {
            this.baseSubjectName = baseSubjectName;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getDifficulty() {
            return difficulty;
        }

        public void setDifficulty(int difficulty) {
            this.difficulty = difficulty;
        }

        public int getDownCount() {
            return downCount;
        }

        public void setDownCount(int downCount) {
            this.downCount = downCount;
        }

        public String getExamAnalysis() {
            return examAnalysis;
        }

        public void setExamAnalysis(String examAnalysis) {
            this.examAnalysis = examAnalysis;
        }

        public String getExamAnswer() {
            return examAnswer;
        }

        public void setExamAnswer(String examAnswer) {
            this.examAnswer = examAnswer;
        }

        public List<ExamOptions> getExamOptions() {
            return examOptions;
        }

        public void setExamOptions(List<ExamOptions> examOptions) {
            this.examOptions = examOptions;
        }

        public String getExamStem() {
            return examStem;
        }

        public void setExamStem(String examStem) {
            this.examStem = examStem;
        }

        public int getTemplateStyleId() {
            return templateStyleId;
        }

        public void setTemplateStyleId(int templateStyleId) {
            this.templateStyleId = templateStyleId;
        }

        public String getTemplateStyleName() {
            return templateStyleName;
        }

        public void setTemplateStyleName(String templateStyleName) {
            this.templateStyleName = templateStyleName;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(abilityStructureRy);
            dest.writeString(analyticMethodRy);
            dest.writeInt(baseLevelId);
            dest.writeString(baseLevelName);
            dest.writeInt(baseSubjectId);
            dest.writeString(baseSubjectName);
            dest.writeLong(createTime);
            dest.writeInt(difficulty);
            dest.writeInt(downCount);
            dest.writeString(examAnalysis);
            dest.writeString(examAnswer);
            dest.writeString(examStem);
            dest.writeInt(templateStyleId);
            dest.writeString(templateStyleName);
            dest.writeInt(type);
        }
    }

    public static class ExamOptions implements Comparable<ExamOptions>{
        private String content;
        private boolean right;
        private int seq;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isRight() {
            return right;
        }

        public void setRight(boolean right) {
            this.right = right;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        @Override
        public int compareTo(@NonNull ExamOptions o) {
            return Integer.compare(seq,o.seq);
        }
    }
}
