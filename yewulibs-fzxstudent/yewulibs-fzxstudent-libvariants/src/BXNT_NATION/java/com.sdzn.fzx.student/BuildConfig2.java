package com.sdzn.fzx.student;

public class BuildConfig2 {
    //    public static final VersionInfo versionInfoConfig = VersionInfo.YYY;
//    public static final String versionNameConfig = VersionChoose.getVersionNameString(VersionChoose.YYY);
    public static final String versionNameConfig = UrlManager.CCC;

//    public static final String SERVER_ISERVICE = UrlManager.PREPROD_SERVER_ISERVICE;
//    public static final String SERVER_ISERVICE2 = UrlManager.PREPROD_SERVER_ISERVICE2;

    public static final String FLAVOR = UrlManager.FLAVOR1;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE1;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME1;
    public static final String AUTH = UrlManager.AUTH1;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS1;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL1;
    public static final String DOWN = UrlManager.DOWN1;
    public static final String MQTT = UrlManager.MQTT1;
    public static final int OFFICE_ID = UrlManager.OFFICE_ID1;
    public static final String PORT = UrlManager.PORT1;
    public static final int RABBIT_HOST = UrlManager.RABBIT_HOST1;
    public static final String RABBIT_MQ = UrlManager.RABBIT_MQ1;
    public static final String RABBIT_NAME = UrlManager.RABBIT_NAME1;
    public static final String RABBIT_PASSWORD = UrlManager.RABBIT_PASSWORD1;
    public static final String CLASSURL = UrlManager.CLASSURL1;
    // Fields from default config.
    public static final int TINKER_VERSION = UrlManager.TINKER_VERSION1;

}
