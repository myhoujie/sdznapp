package com.sdzn.fzx.student.libpublic.utils.chatroom;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;


/**
 * zhaosen
 */

public final class GlideHelper {
    public static void load(Context context, Object url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .error(R.mipmap.error_image)
                .placeholder(R.drawable.img_placeholder)
                .into(imageView);
    }

    public static void load(Boolean isRound, Context context, Object url, ImageView imageView) {
        DrawableTypeRequest manager=Glide.with(context).load(url);
        if (isRound) {
            manager.transform(new CircleTransform(context));
            manager.error(R.mipmap.mrtx_img);
        } else {
            manager.error(R.mipmap.error_image);
        }

        manager.into(imageView);
    }

    public static void load(Activity activity, Object url, ImageView imageView) {
        load(activity, url, imageView, false);
    }

    public static void load(final Activity activity, Object url, ImageView imageView, boolean isClickFinish) {
        Glide.with(activity)
                .load(url)
                .error(R.mipmap.error_image)
                .placeholder(R.drawable.img_placeholder)
                .into(imageView);

        if (isClickFinish) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
        }
    }
}
