package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 完形填空控件
 * @author Reisen at 2018-08-22
 */
public class ClozeTestTextView extends BaseHtmlTextView {
    private ArrayList<CharSequence> mTextBodyList = new ArrayList<>();
    private SparseArray<ClozeAnswerBean> mAnswerMap = new SparseArray<>();
    private static final int DOUBLE_BLANK_CELL_COUNT = 12;
    private int defaultColor = Color.BLACK;
    private ClozeTestChangeListener mListener;

    public ClozeTestTextView(Context context) {
        super(context);
        init();
    }

    public ClozeTestTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ClozeTestTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public ClozeTestTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setBackground(null);
        setLongClickable(false);
        setTextIsSelectable(false);
        setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        return true;
    }

    public void setHtmlBody(String html) {
        getImageSourceAndInput(html);
        Spanned spanned = HtmlTagHandler.fromHtml(html, this, this);
        setTextBody(spanned);
    }

    public void setHtmlBody(String html, SparseArray<String> blankTextList) {
        getImageSourceAndInput(html);
        setTextBody(HtmlTagHandler.fromHtml(html, this, this), blankTextList);
    }

    /**
     * 设置要展示的文本
     *
     * @param mask 带标记的文本
     */
    public void setTextBody(CharSequence mask) {
        setTextBody(mask, new SparseArray<String>());
    }

    /**
     * 设置要展示的文本
     *
     * @param mask          带标记的文本
     * @param blankTextList 填空内容
     */
    public void setTextBody(CharSequence mask, SparseArray<String> blankTextList) {
        ArrayList<CharSequence> arr = new ArrayList<>();//用input标记切割后的试题文本
        ArrayList<CharSequence> list = new ArrayList<>();//用imp替换input标记后的试题文本
        Matcher matcher = Pattern.compile(DEFAULT_TAG).matcher(mask);
        int index = 0;
        int blankCount = 0;
        while (matcher.find()) {
            blankCount++;
            arr.add(mask.subSequence(index, matcher.start()));
            index = matcher.end();
        }
        arr.add(mask.subSequence(index, mask.length()));

        for (int i = 0; i < arr.size(); i++) {
            list.add(arr.get(i));
            if (i < blankCount) {
                list.add(new CharSequenceImp());
            }
        }

        //当填空位不足时补足填空位长度
        int count = blankCount - blankTextList.size();
        if (count > 0) {
            for (int i = 1; i <= blankCount; i++) {
                String str = blankTextList.get(i);
                if (str == null) {
                    blankTextList.append(i,"");
                }
            }
        }
        //填空内容添加到作答map中
        for (int i = 0; i < blankTextList.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            String str = blankTextList.valueAt(i);
            bean.setAnswer(str);
        }
        setTextList(list);

    }

    private void setTextList(ArrayList<CharSequence> list) {
        if (list != null) {
            mTextBodyList.clear();
            mTextBodyList.addAll(list);
        }
        buildText();
    }

    public void refresh() {
        buildText();
    }

    public void select(int position) {
        for (int i = 0; i < mAnswerMap.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.get(i);
            if (bean != null) {
                bean.setSelected(i == position);
            }
        }
        refresh();
    }

    /**
     * 设置文本后将原始文本与填充内容整合显示
     */
    private void buildText() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int count = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {
                int length = builder.length();
                builder.append(BLANK_CELL_STR);
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                builder.append(bean.getValue())
                        .append(bean.getAnswer());
                builder.append(BLANK_CELL_STR);
                builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (bean.isSelected()) {
                    builder.setSpan(new ForegroundColorSpan(0xff4291ff), length, builder.length() - 1,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    builder.setSpan(new ForegroundColorSpan(defaultColor), length, builder.length() - 1,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                count++;
            } else {//文本
                builder.append(sequence);
            }
        }
        setText(builder);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //控件中文本所占空间
        int left = getPaddingLeft();
        int right = getWidth() - getPaddingRight();
        int top = getPaddingTop();
        int bottom = getHeight() - getPaddingBottom();
        //触摸位置
        float x = event.getX();
        float y = event.getY();

        //padding外, 清除光标
        if (x < left || x > right || y < top || y > bottom) {
            clickCloze(-1);
            return super.onTouchEvent(event);
        }

        //相对文本的坐标
        x -= left;
        y -= top;

        int startCharIndex = 0;//空开始位置
        int endCharIndex = 0;//空结束位置
        int blankCount = 0;//第几个空

        int textIndex = getPreciseOffset(x, y);
        //找到每个空格的位置
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {//空格位
                //空格位开始位点不动, 结束位点+12;
                ClozeAnswerBean bean = mAnswerMap.valueAt(blankCount);
                endCharIndex = endCharIndex + bean.getValue().length() + bean.getAnswer().length();
                endCharIndex += DOUBLE_BLANK_CELL_COUNT;

                if (textIndex >= startCharIndex && textIndex <= endCharIndex) {
                    clickCloze(blankCount);
                    return false;
                }
                blankCount++;
                startCharIndex = startCharIndex + bean.getValue().length() + bean.getAnswer().length();
                startCharIndex += DOUBLE_BLANK_CELL_COUNT;
            } else {
                startCharIndex += sequence.length();
                endCharIndex += sequence.length();
            }
        }
        clickCloze(-1);
        return false;
    }

    /**
     * 获取指定坐标是第几个字符
     */
    private int getPreciseOffset(float x, float y) {
        Layout layout = getLayout();
        if (layout != null) {
            int topVisibleLine = layout.getLineForVertical((int) y);
            int offset = layout.getOffsetForHorizontal(topVisibleLine, x);

            int offsetX = (int) layout.getPrimaryHorizontal(offset);

            if (offsetX > x) {
                return layout.getOffsetToLeftOf(offset);
            } else {
                return offset;
            }
        } else {
            return -1;
        }
    }

    /*================ 图片 空位解析 ================*/
    @Override
    protected void glideLoadImageSuccess() {
        SparseArray<String> list = new SparseArray<>();
        for (int i = 0; i < mAnswerMap.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            list.put(bean.getIndex(), bean.getAnswer());
        }
        setHtmlBody(html, list);
    }

    /**
     * 解析获取图片信息和空位信息
     */
    private void getImageSourceAndInput(String source) {
        html = source;
        mMap.clear();
        mAnswerMap.clear();
        Document doc = Jsoup.parse(source);
        //解析img标签
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (TextUtils.isEmpty(width)) {
                width = "0";
            }
            if (TextUtils.isEmpty(height)) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
        //解析input标签
        Elements input = doc.getElementsByTag("input");
        for (Element element : input) {
            int index = Integer.valueOf(element.attr("index").trim());
            String value = element.attr("value");
            mAnswerMap.put(index, new ClozeAnswerBean(index, value));
        }
    }
    /*================ 图片 空位解析 ================*/
    public void clickCloze(int position) {
        for (int i = 0; i < mAnswerMap.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            if (bean != null) {
                bean.setSelected(i == position);
            }
        }
        refresh();
        if (position == -1) {//-1时不传出事件
            return;
        }
        if (mListener != null) {
            mListener.clickCloze(ClozeTestTextView.this, mAnswerMap, position);
        }
    }

    public void setClozeTestChangeListener(ClozeTestChangeListener listener) {
        mListener = listener;
    }

    public void setDefaultColor(int color) {
        defaultColor = color;
    }

    public interface ClozeTestChangeListener {
        /**
         * 完型填空点击回调
         *
         * @param map      作答map
         * @param position 点击空位位置, -1表示点击到外面
         */
        void clickCloze(ClozeTestTextView view, SparseArray<ClozeAnswerBean> map, int position);
    }
}
