package com.sdzn.fzx.student.libpublic.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sdzn.fzx.student.libpublic.R;


/**
 * DialogUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class DialogUtils {

    public Dialog createTipDialog(Context context, String contextTxt) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_i_know_layout, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(context).setView(view).create();
        alertDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView confirm = (TextView) view.findViewById(R.id.sure_btn);
        final TextView content = (TextView) view.findViewById(R.id.tip_content_txt);
        content.setText(contextTxt);
        alertDialog.setCancelable(false);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        return alertDialog;
    }


    public PopupWindow createWhiteDialog(final Context context, String contextTxt) {
        final View contextView = LayoutInflater.from(context).inflate(
                R.layout.dialog_i_know_layout, null);

        final PopupWindow popupWindow = new PopupWindow(contextView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        final TextView confirm = (TextView) contextView.findViewById(R.id.sure_btn);
        final TextView content = (TextView) contextView.findViewById(R.id.tip_content_txt);

        content.setText(contextTxt);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //popupwindow消失的时候恢复成原来的透明度
                setAlpha(context, 1f);
            }
        });
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        setAlpha(context, 0.8f);

        return popupWindow;
    }

    private void setAlpha(Context context, float alpha) {
        Activity activity = (Activity) context;
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = alpha; //0.0-1.0
        activity.getWindow().setAttributes(lp);
    }

}
