package com.sdzn.fzx.student.libpublic.views.wps;

/**
 * 文件名：ListFile.java
 * 创建者:fanguangcheng
 * 创建时间:2013.7.18
 * 作用：文件列表显示，并响应打开wps文件等一系列动作
 */

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.FileProvider;

import com.blankj.utilcode.util.AppUtils;

import java.io.File;


public class Wps {
    public static final String ACTION_BACK_KEY_DOWN = "com.kingsoft.writer.back.key.down";
    public static final String ACTION_HOME_KEY_DOWN = "com.kingsoft.writer.home.key.down";
    public static final String ACTION_FILE_SAVE = "cn.wps.moffice.file.save";
    public static final String ACITON_FILE_CLOSE = "cn.wps.moffice.file.close";

    private static final String SHARED_PREFERENCE_NAME = "WPS";

    private Context mContext;
    private StorePreference storePreference;

    public static Wps createInstance(Context context) {
        return new Wps(context);
    }

    public Wps(Context context) {
        // 实现将第三方包名写入文件，以便wps读取
        mContext = context;
        storePreference = new StorePreference(mContext, SHARED_PREFERENCE_NAME);
        storePreference.storeData(Define.KEY, mContext.getPackageName());
    }

    public Wps open(final File file) {
        if (file.isFile()) {
            if (IsWPSFile(file)) {
                // 以第三方方式打开
                openFile(file.getAbsolutePath());

            } else {// 不是wps文件则让用户选择
                String type = getMIMEType(file);
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), type);
                mContext.startActivity(intent);
            }
        }
        return this;
        // TODO 如果是目录，获取用户点击的文件夹 下的所有文件
    }

    /**
     * 识别文件的类型
     *
     * @param f
     * @return
     */
    private String getMIMEType(File f) {
        String end = f.getName().substring(f.getName().lastIndexOf(".") + 1, f.getName().length()).toLowerCase();
        String type = "";
        if (end.equals("mp3")) {
            type = "audio";
        } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            type = "image";
        } else if (end.equals("doc") || end.equals("docx") || end.equals("ppt") || end.equals("pptx") || end.equals("xls") || end.equals("xlsx") || end.equals("pdf") || end.equals("txt")) {
            type = "application/msword";
            return type;
        } else if (end.equals("notepad")) {
            type = "notepad";
            return type;
        } else if (end.equals("mp4") || end.equals("avi") || end.equals("wmv") ||
                end.equals("mkv") || end.equals("3gp") || end.equals("mpeg4") || end.equals("rm") ||
                end.equals("rmvb") || end.equals("flv") || end.equals("f4v")) {
//			type = "audio";
            type = "video/*";
            return type;
        } else {
            type = "*";
        }
        type += "/*";
        return type;
    }

    /**
     * 如果是wps文件，则用wps打开，并对其设置一下参数
     *
     * @param path
     * @return
     */
    private boolean openFile(String path) {
        String wps_open_mode = storePreference.getData(Define.WPS_OPEN_MODE, "null");

        // 获得上次打开的文件信息
        String closeFilePath = storePreference.getData(Define.CLOSE_FILE, "null");
        String packageName = storePreference.getData(Define.THIRD_PACKAGE, mContext.getPackageName());
        float ViewProgress = storePreference.getData(Define.VIEW_PROGRESS, (float) 0.0);
        float ViewScale = storePreference.getData(Define.VIEW_SCALE, (float) 1.0);
        int ViewScrollX = storePreference.getData(Define.VIEW_SCROLL_X, 0);
        int ViewScrollY = storePreference.getData(Define.VIEW_SCROLL_Y, 0);
        String savePath = storePreference.getData(Define.SAVE_PATH, "");
        // String userName = storePreference.getData(Define.USER_NAME, "");
        // String packageName = getPackageName();

        // 获取用户设置的参数信息
        boolean IsViewScale = storePreference.getData(Define.IS_VIEW_SCALE, false);
        boolean AutoJump = storePreference.getData(Define.AUTO_JUMP, false);

        Intent intent = new Intent();
        Bundle bundle = new Bundle();

        bundle.putString(Define.OPEN_MODE, Define.READ_MODE); // 打开模式

        bundle.putBoolean(Define.SEND_SAVE_BROAD, true); // 保存文件的广播
        bundle.putBoolean(Define.SEND_CLOSE_BROAD, true); // 关闭文件的广播
        bundle.putBoolean(Define.HOME_KEY_DOWN, true);
        bundle.putBoolean(Define.BACK_KEY_DOWN, true);

        bundle.putBoolean(Define.CLEAR_BUFFER, true); // 清除临时文件
        bundle.putBoolean(Define.CLEAR_TRACE, true); // 清除使用记录
        bundle.putBoolean(Define.CLEAR_FILE, false); // 删除打开文件
        bundle.putBoolean(Define.AUTO_JUMP, AutoJump); // 自动跳转，包括页数和xy坐标
        bundle.putString(Define.THIRD_PACKAGE, packageName);
        bundle.putString(Define.SAVE_PATH, savePath); // 保存路径
        bundle.putBoolean(Define.CACHE_FILE_INVISIBLE, true); //
        bundle.putBoolean(Define.ENTER_REVISE_MODE, false); //
        bundle.putBoolean(Define.IS_SHOW_VIEW, false);
        // bundle.putString(Define.USER_NAME, userName);
        // bundle.putString(Define.CHECK_PACKAGE_NAME, "cn.wps.moffice");
        // bundle.putString(Define.JSON_DATA, JSON_DATA);
        // //特殊需求，直接跳过agent，连接client参数

        if (path.equals(closeFilePath)) // 如果打开的文档时上次关闭的
        {
            if (IsViewScale)
                bundle.putFloat(Define.VIEW_SCALE, ViewScale); // 视图比例
            if (AutoJump) {
                bundle.putFloat(Define.VIEW_PROGRESS, ViewProgress); // 阅读进度
                bundle.putInt(Define.VIEW_SCROLL_X, ViewScrollX); // x
                bundle.putInt(Define.VIEW_SCROLL_Y, ViewScrollY); // y
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Intent.ACTION_VIEW);

        String version = checkVersion();
        intent.setClassName(version, Define.CLASSNAME);

        File file = new File(path);
        if (file == null || !file.exists()) {
            return false;
        }

        Uri uri = Uri.fromFile(file);
//		intent.setData(uri);
        intent.putExtras(bundle);
        //判断是否是AndroidN以及更高的版本
        if (Build.VERSION.SDK_INT >= 24) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(mContext, AppUtils.getAppPackageName() + ".fileProvider", file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        try {
            mContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String checkVersion() {
        // 检测安装的wps版本
        if (checkPackage(Define.PACKAGENAME_ENT)) {
            return Define.PACKAGENAME_ENT;
        } else if (checkPackage(Define.PACKAGENAME)) {
            return Define.PACKAGENAME;
        } else if (checkPackage(Define.PACKAGENAME_ENG)) {
            return Define.PACKAGENAME_ENG;
        } else if (checkPackage(Define.PACKAGENAME_KING_ENT)) {
            return Define.PACKAGENAME_KING_ENT;
        } else if (checkPackage(Define.PACKAGENAME_KING_PRO)) {
            return Define.PACKAGENAME_KING_PRO;
        } else if (checkPackage(Define.PACKAGENAME_KING_PRO_HW)) {
            return Define.PACKAGENAME_KING_PRO_HW;
        } else {
            return "";
        }
    }

    /**
     * 判断是否是wps能打开的文件
     *
     * @param file
     * @return
     */
    private boolean IsWPSFile(File file) {
        String end = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase();
        if (end.equals("doc") || end.equals("docx") || end.equals("wps") || end.equals("dot") || end.equals("wpt") || end.equals("xls") || end.equals("xlsx") || end.equals("et") || end.equals("ppt") || end.equals("pptx") || end.equals("dps") || end.equals("txt") || end.equals("pdf"))
            return true;

        return false;
    }

    /**
     * 检测该包名所对应的应用是否存在
     *
     * @param packageName
     * @return
     */
    private boolean checkPackage(String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            mContext.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (NameNotFoundException e) {
            e.printStackTrace();

            return false;
        }
    }

    public void registerReceiver(String wpsAction, BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(wpsAction);
        mContext.registerReceiver(receiver, intentFilter);
    }

    public void registerAllReceiver(BroadcastReceiver receiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_BACK_KEY_DOWN);
        intentFilter.addAction(ACTION_HOME_KEY_DOWN);
        intentFilter.addAction(ACTION_FILE_SAVE);
        intentFilter.addAction(ACITON_FILE_CLOSE);

        mContext.registerReceiver(receiver, intentFilter);
    }

}
