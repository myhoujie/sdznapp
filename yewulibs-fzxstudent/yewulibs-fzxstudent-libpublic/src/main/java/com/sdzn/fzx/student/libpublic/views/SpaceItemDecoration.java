package com.sdzn.fzx.student.libpublic.views;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

        private int lrSpace;
        private int tbSpace;

        public SpaceItemDecoration(Context context) {
            lrSpace = DisplayUtil.dip2px(context, 5);
            tbSpace = DisplayUtil.dip2px(context, 2);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = outRect.left = lrSpace;
            outRect.bottom = tbSpace;
        }
    }