package com.sdzn.fzx.student.libpublic.event;

/**
 * Created by admin on 2019/8/15.
 * 左右相册  （删除）图片处理
 */

public class DiscussDeleteEvBus {
    String type;

    public DiscussDeleteEvBus(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
