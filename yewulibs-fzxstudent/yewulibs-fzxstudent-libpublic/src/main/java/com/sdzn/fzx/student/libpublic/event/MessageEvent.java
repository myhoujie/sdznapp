package com.sdzn.fzx.student.libpublic.event;

import android.net.Uri;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/13
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class MessageEvent extends Event {
    private Uri message;
    public  MessageEvent(Uri message){
        this.message=message;
    }
    public Uri getMessage() {
        return message;
    }

    public void setMessage(Uri message) {
        this.message = message;
    }


}
