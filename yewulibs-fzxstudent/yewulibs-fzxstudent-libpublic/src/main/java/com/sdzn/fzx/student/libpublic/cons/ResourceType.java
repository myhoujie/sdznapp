package com.sdzn.fzx.student.libpublic.cons;


import com.sdzn.fzx.student.libpublic.R;

/**
 * ResourceType〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public enum ResourceType {
    word("文档", "1", R.mipmap.word);

    private String name;
    private String value;
    private int drawableId;

    private ResourceType(String name, String value, int drawableId) {
        this.name = name;
        this.value = value;
        this.drawableId = drawableId;
    }
}
