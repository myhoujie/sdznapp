/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sdzn.fzx.student.libpublic.utils;

//import android.app.ActivityThread;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.ParagraphStyle;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;

import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libpublic.views.fontStyle.UnderPointSpan;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.Log;
import com.tencent.bugly.crashreport.CrashReport;

import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.ccil.cowan.tagsoup.HTMLSchema;
//import org.ccil.cowan.tagsoup.Parser;

/**
 * This class processes HTML strings into displayable styled text.
 * Not all HTML tags are supported.
 */
public class HtmlTagHandler {
    /**
     * Retrieves images for HTML &lt;img&gt; tags.
     */
    public interface ImageGetter {
        /**
         * This method is called when the HTML parser encounters an
         * &lt;img&gt; tag.  The <code>source</code> argument is the
         * string from the "src" attribute; the return value should be
         * a Drawable representation of the image or <code>null</code>
         * for a generic replacement image.  Make sure you call
         * setBounds() on your Drawable if it doesn't already have
         * its bounds set.
         */
        Drawable getDrawable(String source);
    }

    public interface TagHandler {
        /**
         * 处理其他标签方法
         */
        void handleTag(boolean opening, String tag,
                       Editable output, @Nullable Attributes attrs);
    }

    private static class HtmlParser {
        private static final HTMLSchema schema = new HTMLSchema();
    }

    /**
     * Returns displayable styled text from the provided HTML string. Any &lt;img&gt; tags in the
     * HTML will use the specified ImageGetter to request a representation of the image (use null
     * if you don't want this) and the specified TagHandler to handle unknown tags (specify null if
     * you don't want this).
     * <p>
     * <p>This uses TagSoup to handle real HTML, including all of the brokenness found in the wild.
     */
    public static Spanned fromHtml(String source, ImageGetter imageGetter,
                                   TagHandler tagHandler) {
        Parser parser = new Parser();
        try {
            parser.setProperty(Parser.schemaProperty, HtmlParser.schema);
        } catch (org.xml.sax.SAXNotRecognizedException | org.xml.sax.SAXNotSupportedException e) {
            // Should not happen.
            e.printStackTrace();
//            throw new RuntimeException(e);
        }

        HtmlToSpannedConverter converter =
                new HtmlToSpannedConverter(source, imageGetter, tagHandler, parser);
        Spanned spanned = converter.convert();
        if (spanned instanceof SpannableStringBuilder) {
            SpannableStringBuilder ssb = (SpannableStringBuilder) spanned;
            int length = ssb.length();
            if (length < 2) {
                return spanned;
            }
            if (ssb.charAt(length - 1) == '\n' && ssb.charAt(length - 2) == '\n') {
                ssb.delete(length - 2, length);
            }
        }
        return spanned;

//        return new HtmlToSpannedConverter(source, imageGetter, tagHandler).convert();
    }

    static class HtmlToSpannedConverter implements ContentHandler {

        private static final float[] HEADING_SIZES = {
                1.5f, 1.4f, 1.3f, 1.2f, 1.1f, 1f,
        };

        private String mSource;
        private SpannableStringBuilder mSpannableStringBuilder;
        private ImageGetter mImageGetter;
        private TagHandler mTagHandler;
        private XMLReader mReader;

        private static Pattern sTextAlignPattern;
        private static Pattern sForegroundColorPattern;
        private static Pattern sBackgroundColorPattern;
        private static Pattern sTextDecorationPattern;

        /**
         * Name-value mapping of HTML/CSS colors which have different values in {@link Color}.
         */
        private static final Map<String, Integer> sColorMap;

        static {
            sColorMap = new HashMap<>();
            sColorMap.put("darkgray", 0xFFA9A9A9);
            sColorMap.put("gray", 0xFF808080);
            sColorMap.put("lightgray", 0xFFD3D3D3);
            sColorMap.put("darkgrey", 0xFFA9A9A9);
            sColorMap.put("grey", 0xFF808080);
            sColorMap.put("lightgrey", 0xFFD3D3D3);
            sColorMap.put("green", 0xFF008000);
        }

        private static Pattern getTextAlignPattern() {
            if (sTextAlignPattern == null) {
                sTextAlignPattern = Pattern.compile("(?:\\s+|\\A)text-align\\s*:\\s*(\\S*)\\b");
            }
            return sTextAlignPattern;
        }

        private static Pattern getForegroundColorPattern() {
            if (sForegroundColorPattern == null) {
                sForegroundColorPattern = Pattern.compile(
                        "(?:\\s+|\\A)color\\s*:\\s*(\\S*)\\b");
            }
            return sForegroundColorPattern;
        }

        private static Pattern getBackgroundColorPattern() {
            if (sBackgroundColorPattern == null) {
                sBackgroundColorPattern = Pattern.compile(
                        "(?:\\s+|\\A)background(?:-color)?\\s*:\\s*(\\S*)\\b");
            }
            return sBackgroundColorPattern;
        }

        private static Pattern getTextDecorationPattern() {
            if (sTextDecorationPattern == null) {
                sTextDecorationPattern = Pattern.compile(
                        "(?:\\s+|\\A)text-decoration\\s*:\\s*(\\S*)\\b");
            }
            return sTextDecorationPattern;
        }

        public HtmlToSpannedConverter(String source, ImageGetter imageGetter,
                                      TagHandler tagHandler, Parser parser) {
            mSource = source;
            mSpannableStringBuilder = new SpannableStringBuilder();
            mImageGetter = imageGetter;
            mTagHandler = tagHandler;
            mReader = parser;
        }

        public Spanned convert() {

//            try{
//                SAXParserFactory saxFactory = SAXParserFactory.newInstance();
//
//                XMLReader xmlReader =saxFactory.newSAXParser().getXMLReader(); //获取一个XMLReader
//                xmlReader.setContentHandler(this);
//                xmlReader.parse(new InputSource(new StringReader(mSource)));
//            }catch (Exception e){
//                e.printStackTrace();
//            }
            mReader.setContentHandler(this);
            try {
                mReader.parse(new InputSource(new StringReader(mSource)));
            } catch (IOException | SAXException e) {
                // We are reading from a string. There should not be IO problems.
                e.printStackTrace();
//                throw new RuntimeException(e);
            }

            // Fix flags and range for paragraph-type markup.
            Object[] obj = mSpannableStringBuilder.getSpans(0, mSpannableStringBuilder.length(), ParagraphStyle.class);
            for (int i = 0; i < obj.length; i++) {
                int start = mSpannableStringBuilder.getSpanStart(obj[i]);
                int end = mSpannableStringBuilder.getSpanEnd(obj[i]);

                // If the last line of the range is blank, back off by one.
                if (end - 2 >= 0) {
                    if (mSpannableStringBuilder.charAt(end - 1) == '\n' &&
                            mSpannableStringBuilder.charAt(end - 2) == '\n') {
                        end--;
                    }
                }

                if (end == start) {
                    mSpannableStringBuilder.removeSpan(obj[i]);
                } else {
                    try {
                        mSpannableStringBuilder.setSpan(obj[i], start, end, Spannable.SPAN_PARAGRAPH);
                    } catch (Exception e) {
                        Log.e("source = " + mSource);
                        e.printStackTrace();
                        CrashReport.postCatchedException(e);
                    }
                }
            }

            return mSpannableStringBuilder;
        }

        private void handleStartTag(String tag, Attributes attributes) {
            if (tag.equalsIgnoreCase("br")) {
                // We don't need to handle this. TagSoup will ensure that there's a </br> for each <br>
                // so we can safely emit the linebreaks when we handle the close tag.
            } else if (tag.equalsIgnoreCase("p")) {
                startBlockElement(mSpannableStringBuilder, attributes, getMarginParagraph());
                startCssStyle(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("ul")) {
                startBlockElement(mSpannableStringBuilder, attributes, getMarginList());
            } else if (tag.equalsIgnoreCase("li")) {
                startLi(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("div")) {
                startBlockElement(mSpannableStringBuilder, attributes, getMarginDiv());
            } else if (tag.equalsIgnoreCase("span")) {
                startCssStyle(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("strong")) {
                start(mSpannableStringBuilder, new Bold());
            } else if (tag.equalsIgnoreCase("b")) {
                start(mSpannableStringBuilder, new Bold());
            } else if (tag.equalsIgnoreCase("em")) {
                start(mSpannableStringBuilder, new Italic());
            } else if (tag.equalsIgnoreCase("cite")) {
                start(mSpannableStringBuilder, new Italic());
            } else if (tag.equalsIgnoreCase("dfn")) {
                start(mSpannableStringBuilder, new Italic());
            } else if (tag.equalsIgnoreCase("i")) {
                start(mSpannableStringBuilder, new Italic());
            } else if (tag.equalsIgnoreCase("big")) {
                start(mSpannableStringBuilder, new Big());
            } else if (tag.equalsIgnoreCase("small")) {
                start(mSpannableStringBuilder, new Small());
            } else if (tag.equalsIgnoreCase("font")) {
                startFont(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("blockquote")) {
                startBlockquote(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("tt")) {
                start(mSpannableStringBuilder, new Monospace());
            } else if (tag.equalsIgnoreCase("a")) {
                startA(mSpannableStringBuilder, attributes);
            } else if (tag.equalsIgnoreCase("u")) {
                start(mSpannableStringBuilder, new Underline());
            } else if (tag.equalsIgnoreCase("del")) {
                start(mSpannableStringBuilder, new Strikethrough());
            } else if (tag.equalsIgnoreCase("s")) {
                start(mSpannableStringBuilder, new Strikethrough());
            } else if (tag.equalsIgnoreCase("strike")) {
                start(mSpannableStringBuilder, new Strikethrough());
            } else if (tag.equalsIgnoreCase("sup")) {
                start(mSpannableStringBuilder, new Super());
            } else if (tag.equalsIgnoreCase("sub")) {
                start(mSpannableStringBuilder, new Sub());
            } else if (tag.equalsIgnoreCase("bdo")) {
                startBdo(mSpannableStringBuilder, attributes);
            } else if (tag.length() == 2 &&
                    Character.toLowerCase(tag.charAt(0)) == 'h' &&
                    tag.charAt(1) >= '1' && tag.charAt(1) <= '6') {
                startHeading(mSpannableStringBuilder, attributes, tag.charAt(1) - '1');
            } else if (tag.equalsIgnoreCase("img")) {
                startImg(mSpannableStringBuilder, attributes, mImageGetter);
            } else if (mTagHandler != null) {
                mTagHandler.handleTag(true, tag, mSpannableStringBuilder, attributes);
            }
        }

        private void handleEndTag(String tag) {
            if (tag.equalsIgnoreCase("br")) {
                handleBr(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("p")) {
                endCssStyle(mSpannableStringBuilder);
                endBlockElement(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("ul")) {
                endBlockElement(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("li")) {
                endLi(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("div")) {
                endBlockElement(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("span")) {
                endCssStyle(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("strong")) {
                end(mSpannableStringBuilder, Bold.class, new StyleSpan(Typeface.BOLD));
            } else if (tag.equalsIgnoreCase("b")) {
                end(mSpannableStringBuilder, Bold.class, new StyleSpan(Typeface.BOLD));
            } else if (tag.equalsIgnoreCase("em")) {
                end(mSpannableStringBuilder, Italic.class, new StyleSpan(Typeface.ITALIC));
            } else if (tag.equalsIgnoreCase("cite")) {
                end(mSpannableStringBuilder, Italic.class, new StyleSpan(Typeface.ITALIC));
            } else if (tag.equalsIgnoreCase("dfn")) {
                end(mSpannableStringBuilder, Italic.class, new StyleSpan(Typeface.ITALIC));
            } else if (tag.equalsIgnoreCase("i")) {
                end(mSpannableStringBuilder, Italic.class, new StyleSpan(Typeface.ITALIC));
            } else if (tag.equalsIgnoreCase("big")) {
                end(mSpannableStringBuilder, Big.class, new RelativeSizeSpan(1.25f));
            } else if (tag.equalsIgnoreCase("small")) {
                end(mSpannableStringBuilder, Small.class, new RelativeSizeSpan(0.8f));
            } else if (tag.equalsIgnoreCase("font")) {
                endFont(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("blockquote")) {
                endBlockquote(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("tt")) {
                end(mSpannableStringBuilder, Monospace.class, new TypefaceSpan("monospace"));
            } else if (tag.equalsIgnoreCase("a")) {
                endA(mSpannableStringBuilder);
            } else if (tag.equalsIgnoreCase("u")) {
                end(mSpannableStringBuilder, Underline.class, new UnderlineSpan());
            } else if (tag.equalsIgnoreCase("del")) {
                end(mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
            } else if (tag.equalsIgnoreCase("s")) {
                end(mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
            } else if (tag.equalsIgnoreCase("strike")) {
                end(mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
            } else if (tag.equalsIgnoreCase("sup")) {
                end(mSpannableStringBuilder, Super.class, new SuperscriptSpan());
            } else if (tag.equalsIgnoreCase("sub")) {
                end(mSpannableStringBuilder, Sub.class, new SubscriptSpan());
            } else if (tag.equalsIgnoreCase("bdo")) {
                endCssStyle(mSpannableStringBuilder);
            } else if (tag.length() == 2 &&
                    Character.toLowerCase(tag.charAt(0)) == 'h' &&
                    tag.charAt(1) >= '1' && tag.charAt(1) <= '6') {
                endHeading(mSpannableStringBuilder);
            } else if (mTagHandler != null) {
                mTagHandler.handleTag(false, tag, mSpannableStringBuilder, null);
            }
        }

        private int getMarginParagraph() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH);
        }

        private int getMarginHeading() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING);
        }

        private int getMarginListItem() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM);
        }

        private int getMarginList() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_LIST);
        }

        private int getMarginDiv() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV);
        }

        private int getMarginBlockquote() {
            return getMargin(Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE);
        }

        /**
         * Returns the minimum number of newline characters needed before and after a given block-level
         * element.
         *
         * @param flag the corresponding option flag defined in {@link Html} of a block-level element
         */
        private int getMargin(int flag) {
            return 2;
        }

        private static void appendNewlines(Editable text, int minNewline) {
            final int len = text.length();

            if (len == 0) {
                return;
            }

            int existingNewlines = 0;
            for (int i = len - 1; i >= 0 && text.charAt(i) == '\n'; i--) {
                existingNewlines++;
            }

            for (int j = existingNewlines; j < minNewline; j++) {
                text.append("\n");
            }
        }

        private static void startBlockElement(Editable text, Attributes attributes, int margin) {
            final int len = text.length();
            if (margin > 0) {
                appendNewlines(text, margin);
                start(text, new Newline(margin));
            }

            String style = attributes.getValue("", "style");
            if (style != null) {
                Matcher m = getTextAlignPattern().matcher(style);
                if (m.find()) {
                    String alignment = m.group(1);
                    if (alignment.equalsIgnoreCase("start")) {
                        start(text, new Alignment(Layout.Alignment.ALIGN_NORMAL));
                    } else if (alignment.equalsIgnoreCase("center")) {
                        start(text, new Alignment(Layout.Alignment.ALIGN_CENTER));
                    } else if (alignment.equalsIgnoreCase("end")) {
                        start(text, new Alignment(Layout.Alignment.ALIGN_OPPOSITE));
                    }
                }
            }
        }

        private static void endBlockElement(Editable text) {
            Newline n = getLast(text, Newline.class);
            if (n != null) {
                appendNewlines(text, n.mNumNewlines);
                text.removeSpan(n);
            }

            Alignment a = getLast(text, Alignment.class);
            if (a != null) {
                setSpanFromMark(text, a, new AlignmentSpan.Standard(a.mAlignment));
            }
        }

        private static void handleBr(Editable text) {
            text.append('\n');
        }

        private void startLi(Editable text, Attributes attributes) {
            startBlockElement(text, attributes, getMarginListItem());
            start(text, new Bullet());
            startCssStyle(text, attributes);
        }

        private static void endLi(Editable text) {
            endCssStyle(text);
            endBlockElement(text);
            end(text, Bullet.class, new BulletSpan());
        }

        private void startBlockquote(Editable text, Attributes attributes) {
            startBlockElement(text, attributes, getMarginBlockquote());
            start(text, new Blockquote());
        }

        private static void endBlockquote(Editable text) {
            endBlockElement(text);
            end(text, Blockquote.class, new QuoteSpan());
        }

        private void startHeading(Editable text, Attributes attributes, int level) {
            startBlockElement(text, attributes, getMarginHeading());
            start(text, new Heading(level));
        }

        private static void endHeading(Editable text) {
            // RelativeSizeSpan and StyleSpan are CharacterStyles
            // Their ranges should not include the newlines at the end
            Heading h = getLast(text, Heading.class);
            if (h != null) {
                setSpanFromMark(text, h, new RelativeSizeSpan(HEADING_SIZES[h.mLevel]),
                        new StyleSpan(Typeface.BOLD));
            }

            endBlockElement(text);
        }

        private static <T> T getLast(Spanned text, Class<T> kind) {
            /*
             * This knows that the last returned object from getSpans()
             * will be the most recently added.
             */
            T[] objs = text.getSpans(0, text.length(), kind);

            if (objs.length == 0) {
                return null;
            } else {
                return objs[objs.length - 1];
            }
        }

        private static void setSpanFromMark(Spannable text, Object mark, Object... spans) {
            int where = text.getSpanStart(mark);
            text.removeSpan(mark);
            int len = text.length();
            if (where != len) {
                for (Object span : spans) {
                    text.setSpan(span, where, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        private static void start(Editable text, Object mark) {
            int len = text.length();
            text.setSpan(mark, len, len, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        private static void end(Editable text, Class kind, Object repl) {
            int len = text.length();
            Object obj = getLast(text, kind);
            if (obj != null) {
                setSpanFromMark(text, obj, repl);
            }
        }

        private void startCssStyle(Editable text, Attributes attributes) {
            String style = attributes.getValue("", "style");
            if (style != null) {
                Matcher m = getForegroundColorPattern().matcher(style);
                if (m.find()) {
                    int c = getHtmlColor(m.group(1));
                    if (c != -1) {
                        start(text, new Foreground(c | 0xFF000000));
                    }
                }

                m = getBackgroundColorPattern().matcher(style);
                if (m.find()) {
                    int c = getHtmlColor(m.group(1));
                    if (c != -1) {
                        start(text, new Background(c | 0xFF000000));
                    }
                }

                m = getTextDecorationPattern().matcher(style);
                if (m.find()) {
                    String textDecoration = m.group(1);
                    if (textDecoration.equalsIgnoreCase("line-through")) {
                        start(text, new Strikethrough());
                    } else if (textDecoration.equalsIgnoreCase("underline") ||
                            textDecoration.equalsIgnoreCase("underline;")) {
                        start(text, new Underline());
                    }
                }
            }
        }

        private static void endCssStyle(Editable text) {
            Underline u = getLast(text, Underline.class);
            if (u != null) {
                end(text, Underline.class, new UnderlineSpan());
            }

            UnderPoint up = getLast(text, UnderPoint.class);
            if (up != null) {
                setSpanFromMark(text, up, new UnderPointSpan());
            }

            Strikethrough s = getLast(text, Strikethrough.class);
            if (s != null) {
                setSpanFromMark(text, s, new StrikethroughSpan());
            }

            Background b = getLast(text, Background.class);
            if (b != null) {
                setSpanFromMark(text, b, new BackgroundColorSpan(b.mBackgroundColor));
            }

            Foreground f = getLast(text, Foreground.class);
            if (f != null) {
                setSpanFromMark(text, f, new ForegroundColorSpan(f.mForegroundColor));
            }
        }

        private static void startBdo(Editable text, Attributes attributes) {
            String clazz = attributes.getValue("", "class");
            if (clazz != null) {
                if (clazz.equalsIgnoreCase("mathjye-underpoint2") ||
                        clazz.equalsIgnoreCase("mathjye-underpoint")) {
                    start(text, new UnderPoint());
                } else if (clazz.equalsIgnoreCase("mathjye-underline")) {
                    start(text, new Underline());
                }
            }
        }

        private static void endBdo(Editable text) {
            UnderPoint u = getLast(text, UnderPoint.class);
            if (u != null) {
                setSpanFromMark(text, u, new UnderPointSpan());
            }
        }

        private static void startImg(Editable text, Attributes attributes, ImageGetter img) {
            String src = attributes.getValue("", "src");
            Drawable d = null;

            if (img != null) {
                d = img.getDrawable(src);
            }

            if (d == null) {
                d = App2.get().getResources().getDrawable(R.drawable.default_unknow_image);
//                d = Resources.getSystem().getDrawable(
//                        Resources.getSystem().getIdentifier("unknown_image","drawable","android"));
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            }

            int len = text.length();
            text.append("\uFFFC");

            text.setSpan(new ImageSpan(d, src, ImageSpan.ALIGN_BASELINE), len, text.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        private void startFont(Editable text, Attributes attributes) {
            String color = attributes.getValue("", "color");
            String face = attributes.getValue("", "face");

            if (!TextUtils.isEmpty(color)) {
                int c = getHtmlColor(color);
                if (c != -1) {
                    start(text, new Foreground(c | 0xFF000000));
                }
            }

            if (!TextUtils.isEmpty(face)) {
                start(text, new Font(face));
            }
        }

        private static void endFont(Editable text) {
            Font font = getLast(text, Font.class);
            if (font != null) {
                setSpanFromMark(text, font, new TypefaceSpan(font.mFace));
            }

            Foreground foreground = getLast(text, Foreground.class);
            if (foreground != null) {
                setSpanFromMark(text, foreground,
                        new ForegroundColorSpan(foreground.mForegroundColor));
            }
        }

        private static void startA(Editable text, Attributes attributes) {
            String href = attributes.getValue("", "href");
            start(text, new Href(href));
        }

        private static void endA(Editable text) {
            Href h = getLast(text, Href.class);
            if (h != null) {
                if (h.mHref != null) {
                    setSpanFromMark(text, h, new URLSpan((h.mHref)));
                }
            }
        }

        private int getHtmlColor(String color) {
            if (0 == Html.FROM_HTML_OPTION_USE_CSS_COLORS) {
                Integer i = sColorMap.get(color.toLowerCase(Locale.US));
                if (i != null) {
                    return i;
                }
            }
            try {
                @SuppressLint("SoonBlockedPrivateApi") Method method = Color.class.getDeclaredMethod("getHtmlColor", String.class);
                return (int) method.invoke(null, color);
            } catch (Exception e) {
                e.printStackTrace();
                CrashReport.postCatchedException(e);
                return -1;
            }
//            return Color.getHtmlColor(color);
        }

        public void setDocumentLocator(Locator locator) {

        }

        public void startDocument() throws SAXException {
        }

        public void endDocument() throws SAXException {
        }

        public void startPrefixMapping(String prefix, String uri) throws SAXException {
        }

        public void endPrefixMapping(String prefix) throws SAXException {
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes)
                throws SAXException {
            handleStartTag(localName, attributes);
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            handleEndTag(localName);
        }

        public void characters(char ch[], int start, int length) throws SAXException {
            StringBuilder sb = new StringBuilder();

            /*
             * Ignore whitespace that immediately follows other whitespace;
             * newlines count as spaces.
             */

            for (int i = 0; i < length; i++) {
                char c = ch[i + start];

                if (c == ' ' || c == '\n') {
                    char pred;
                    int len = sb.length();

                    if (len == 0) {
                        len = mSpannableStringBuilder.length();

                        if (len == 0) {
                            pred = '\n';
                        } else {
                            pred = mSpannableStringBuilder.charAt(len - 1);
                        }
                    } else {
                        pred = sb.charAt(len - 1);
                    }

                    if (pred != ' ' && pred != '\n') {
                        sb.append(' ');
                    }
                } else {
                    sb.append(c);
                }
            }

            mSpannableStringBuilder.append(sb);
        }

        public void ignorableWhitespace(char ch[], int start, int length) throws SAXException {
        }

        public void processingInstruction(String target, String data) throws SAXException {
        }

        public void skippedEntity(String name) throws SAXException {
        }

        private static class Bold {
        }

        private static class Italic {
        }

        //文字下划线样式
        private static class Underline {
        }

        //加点字样式
        private static class UnderPoint {
        }

        private static class Strikethrough {
        }

        private static class Big {
        }

        private static class Small {
        }

        private static class Monospace {
        }

        private static class Blockquote {
        }

        private static class Super {
        }

        private static class Sub {
        }

        private static class Bullet {
        }

        private static class Font {
            public String mFace;

            public Font(String face) {
                mFace = face;
            }
        }

        private static class Href {
            public String mHref;

            public Href(String href) {
                mHref = href;
            }
        }

        private static class Foreground {
            private int mForegroundColor;

            public Foreground(int foregroundColor) {
                mForegroundColor = foregroundColor;
            }
        }

        private static class Background {
            private int mBackgroundColor;

            public Background(int backgroundColor) {
                mBackgroundColor = backgroundColor;
            }
        }

        private static class Heading {
            private int mLevel;

            public Heading(int level) {
                mLevel = level;
            }
        }

        private static class Newline {
            private int mNumNewlines;

            public Newline(int numNewlines) {
                mNumNewlines = numNewlines;
            }
        }

        private static class Alignment {
            private Layout.Alignment mAlignment;

            public Alignment(Layout.Alignment alignment) {
                mAlignment = alignment;
            }
        }
    }
}