package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import androidx.annotation.Nullable;
import android.text.Spanned;
import android.util.AttributeSet;

import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Reisen at 2018-09-07
 */
public class HtmlTextView extends BaseHtmlTextView {

    public HtmlTextView(Context context) {
        super(context);
    }

    public HtmlTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public HtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public HtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void getImageSource(String source) {
        html = source;
        mMap.clear();
        Document doc = Jsoup.parse(source);
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (width == null || width.isEmpty()) {
                width = "0";
            }
            if (height == null || height.isEmpty()) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
    }

    public void setHtmlText(String htmlText) {
        getImageSource(htmlText);
        Spanned spanned = HtmlTagHandler.fromHtml(htmlText, this, this);
        setText(spanned, BufferType.SPANNABLE);
    }

    public String getHtmlText() {
        return html;
    }

    @Override
    protected void glideLoadImageSuccess() {
        setText(HtmlTagHandler.fromHtml(html, HtmlTextView.this, HtmlTextView.this), BufferType.SPANNABLE);
    }
}
