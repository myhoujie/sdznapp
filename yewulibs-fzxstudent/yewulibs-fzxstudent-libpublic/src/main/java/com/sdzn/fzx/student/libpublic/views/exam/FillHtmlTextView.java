package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sdzn.fzx.student.libutils.util.FileUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.XMLReader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2018/12/14
 * 修改单号：
 * 修改内容:
 */
public class FillHtmlTextView extends TextView implements Html.ImageGetter, Html.TagHandler {
    private HashMap<String, Point> mMap = new HashMap<>();
    private String html;

    public FillHtmlTextView(Context context) {
        super(context);
    }

    public FillHtmlTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FillHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public FillHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private String getFileName(String url) {
        String[] split = url.split("/");
        return split[split.length - 1];
    }

    private void getImageSource(String source) {
        html = source;
        mMap.clear();
        Document doc = Jsoup.parse(source);
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (width == null || width.isEmpty()) {
                width = "0";
            }
            if (height == null || height.isEmpty()) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
    }

    public void setHtmlText(String htmlText) {
        getImageSource(htmlText);
        Spanned spanned = Html.fromHtml(htmlText, this, this);
        setText(spanned);
    }

    public String getHtmlText() {
        return html;
    }

    @Override
    public Drawable getDrawable(String source) {
        Drawable drawable;
        if (source == null) {
            return null;
        }
        if (source.startsWith("data:image/png;base64,")) {
            String src = source.split("data:image/png;base64,")[1];
            byte[] bytes = Base64.decode(src, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            drawable = new BitmapDrawable(bitmap);
            drawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
            return drawable;
        }
        String fileName = getFileName(source);
        File file = new File(getContext().getExternalCacheDir(), fileName);
        Bitmap bitmap = ImageManager.getInstance().getCache(file.getAbsolutePath());
        if (bitmap != null) {
            drawable = new BitmapDrawable(bitmap);
            Point point = mMap.get(source);
            if (point == null || point.x <= 0 || point.y <= 0) {
                drawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
//                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            } else {
                drawable.setBounds(0, 0, point.x, point.y);
            }
            return drawable;
        }
        if (file.exists()) {
            drawable = Drawable.createFromPath(file.getAbsolutePath());
            if (drawable == null) {
                return null;
            }
            ImageManager.getInstance().addCache(file.getAbsolutePath(), ((BitmapDrawable) drawable).getBitmap());
            Point point = mMap.get(source);
            if (point == null || point.x <= 0 || point.y <= 0) {
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            } else {
                drawable.setBounds(0, 0, point.x, point.y);
            }
            return drawable;
        } else {
            final File f2 = file;
            Glide.with(getContext()).load(source).downloadOnly(new SimpleTarget<File>() {
                @Override
                public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
                    try {
                        FileUtil.copyFile(resource.getAbsolutePath(), f2.getAbsolutePath());
                        ImageManager.getInstance().addCache(f2.getAbsolutePath(), BitmapFactory.decodeFile(f2.getAbsolutePath()));
                        setText(Html.fromHtml(html, FillHtmlTextView.this, FillHtmlTextView.this));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }
    }

    @Override
    public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
        if ("img".equals(tag.toLowerCase(Locale.getDefault()))) {
            // 获取长度
            int len = output.length();
            // 获取图片地址
            ImageSpan[] images = output.getSpans(len - 1, len, ImageSpan.class);
            String imgURL = images[0].getSource();
            output.setSpan(new ClickableImage(imgURL), len - 1, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        if ("input".equals(tag.toLowerCase(Locale.getDefault()))) {
            String value = getProperty(xmlReader, "value");


            if (value != null) {
                output.append("_____" + value + "_____");
            }


        }

    }

    private String getProperty(XMLReader xmlReader, String property) {
        try {
            Field elementField = xmlReader.getClass().getDeclaredField("theNewElement");
            elementField.setAccessible(true);
            Object element = elementField.get(xmlReader);
            Field attsField = element.getClass().getDeclaredField("theAtts");
            attsField.setAccessible(true);
            Object atts = attsField.get(element);
            Field dataField = atts.getClass().getDeclaredField("data");
            dataField.setAccessible(true);
            String[] data = (String[]) dataField.get(atts);
            Field lengthField = atts.getClass().getDeclaredField("length");
            lengthField.setAccessible(true);
            int len = (Integer) lengthField.get(atts);

            for (int i = 0; i < len; i++) {
                // 这边的property换成你自己的属性名就可以了
                if (property.equals(data[i * 5 + 1])) {
                    return data[i * 5 + 4];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private class ClickableImage extends ClickableSpan {
        private String url;

        public ClickableImage(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View widget) {
            if (mListener != null) {
                mListener.clickImage(url);
            }
        }
    }

    private HtmlTextView.ImageClickListener mListener;

    public void setListener(HtmlTextView.ImageClickListener listener) {
        mListener = listener;
    }

    public interface ImageClickListener {
        void clickImage(String url);
    }
}
