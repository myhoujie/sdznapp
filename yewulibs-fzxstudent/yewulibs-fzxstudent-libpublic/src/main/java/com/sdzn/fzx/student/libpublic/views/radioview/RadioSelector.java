package com.sdzn.fzx.student.libpublic.views.radioview;

import android.view.View;

import androidx.annotation.IdRes;

/**
 * 实现该接口的控件与RadioLayout合用, 产生类似RadioButton效果
 *
 * @author Reisen at 2017-12-05
 */

public interface RadioSelector extends View.OnClickListener {
    /**
     * 该View是否被选中
     *
     * @return true:被选中 false:未选中
     */
    boolean isChecked();

    /**
     * 获取该控件id, View对其有默认实现
     *
     * @return 控件id
     */
    @IdRes
    int getId();

    /**
     * 设置子控件及本控件选中状态变化
     *
     * @param isChecked true:被选中 false 未选中
     * @return 选择结果是否改变
     */
    boolean setChecked(boolean isChecked);

    /**
     * 继承自{@link View.OnClickListener} 的点击事件回调, 通过该方法来
     * @param view
     */
    void onClick(View view);
}
