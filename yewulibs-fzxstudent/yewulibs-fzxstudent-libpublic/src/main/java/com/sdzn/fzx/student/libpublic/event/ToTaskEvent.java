package com.sdzn.fzx.student.libpublic.event;

/**
 * 添加注释
 *
 * @author wangchunxiao
 * @date 2018/1/19
 */
public class ToTaskEvent extends Event {

    private boolean isCheck;

    public ToTaskEvent(boolean isCheck) {
        super();
        this.isCheck = isCheck;
    }

    public boolean isCheck() {
        return isCheck;
    }
}
