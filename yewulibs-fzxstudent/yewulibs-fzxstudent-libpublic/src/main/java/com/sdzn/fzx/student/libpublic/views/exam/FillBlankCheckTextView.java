package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libutils.annotations.CheckState;
import com.sdzn.fzx.student.libutils.util.Dp2Px;
import com.sdzn.fzx.student.libutils.util.InputTools;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 填空批改
 * 空位可插入图片, 可点击, 文字空位按正误区分颜色, 图片空位按正误区分图标
 *
 * @author Reisen at 2019-01-30
 */
public class FillBlankCheckTextView extends BaseHtmlTextView {
    private ArrayList<CharSequence> mTextBodyList = new ArrayList<>();
    private SparseArray<ClozeAnswerBean> mAnswerMap = new SparseArray<>();

    private static final int DOUBLE_BLANK_CELL_COUNT = 12;
    private int defaultTextColor = Color.BLACK;
    private int trueTextColor;
    private int falseTextColor;
    private @DrawableRes
    int trueIcon;
    private @DrawableRes
    int falseIcon;

    private ImageClickListener mListener;

    public FillBlankCheckTextView(Context context) {
        super(context);
        init(context, null);
    }

    public FillBlankCheckTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FillBlankCheckTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public FillBlankCheckTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            saveAttrs(context, attrs);
        }
        setBackground(null);
        setLongClickable(false);
        setTextIsSelectable(false);
        setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    private void saveAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FillBlankCheckTextView);
        defaultTextColor = a.getColor(R.styleable.FillBlankCheckTextView_defaultTextColor, Color.BLACK);
        trueTextColor = a.getColor(R.styleable.FillBlankCheckTextView_trueTextColor, 0xFF68C426);
        falseTextColor = a.getColor(R.styleable.FillBlankCheckTextView_falseTextColor, 0xFFFF6D4A);

        trueIcon = a.getResourceId(R.styleable.FillBlankCheckTextView_trueAnswerIcon, -1);
        falseIcon = a.getResourceId(R.styleable.FillBlankCheckTextView_falseAnswerIcon, -1);
        a.recycle();

        setTextColor(defaultTextColor);
    }

    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        return true;
    }

    public void setHtmlBody(String html) {
        getImageSourceAndInput(html);
        Spanned spanned = HtmlTagHandler.fromHtml(html, this, this);
        setTextBody(spanned);
    }

    public void setHtmlBody(String html, SparseArray<ClozeAnswerBean> blankTextList) {
        getImageSourceAndInput(html);
        setTextBody(HtmlTagHandler.fromHtml(html, this, this), blankTextList);
    }

    /**
     * 设置要展示的文本
     *
     * @param mask 带标记的文本
     */
    public void setTextBody(CharSequence mask) {
        setTextBody(mask, new SparseArray<ClozeAnswerBean>());
    }

    /**
     * 设置要展示的文本
     *
     * @param mask          带标记的文本
     * @param blankTextList 填空内容
     */
    public void setTextBody(CharSequence mask, SparseArray<ClozeAnswerBean> blankTextList) {
        ArrayList<CharSequence> arr = new ArrayList<>();//用input标记切割后的试题文本
        ArrayList<CharSequence> list = new ArrayList<>();//用imp替换input标记后的试题文本
        Matcher matcher = Pattern.compile(DEFAULT_TAG).matcher(mask);
        int index = 0;
        int blankCount = 0;
        while (matcher.find()) {
            blankCount++;
            arr.add(mask.subSequence(index, matcher.start()));
            index = matcher.end();
        }
        arr.add(mask.subSequence(index, mask.length()));

        for (int i = 0; i < arr.size(); i++) {
            list.add(arr.get(i));
            if (i < blankCount) {
                list.add(new CharSequenceImp());
            }
        }

        //当填空位不足时补足填空位长度
        if (blankCount > blankTextList.size()) {
            for (int i = 1; i <= blankCount; i++) {
                if (blankTextList.get(i) == null) {
                    blankTextList.append(i, new ClozeAnswerBean(i, ""));
                }
            }
        }else if (blankCount < blankTextList.size()){
            while (blankTextList.size() > blankCount) {
                blankTextList.removeAt(blankTextList.size() - 1);
            }
        }

        //填空内容添加到作答map中
        for (int i = 0; i < blankTextList.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            ClozeAnswerBean str = blankTextList.valueAt(i);
            bean.setData(str);
//            bean.setAnswer(str.getAnswer());
//            bean.setInputMethod(str.getInputMethod());
        }
        setTextList(list);
    }

    private void setTextList(ArrayList<CharSequence> list) {
        if (list != null) {
            mTextBodyList.clear();
            mTextBodyList.addAll(list);
        }
        buildText();
    }

    /**
     * 刷新数据
     */
    public void refresh() {
        buildText();
    }

    private static final String TAG = "FillBlankCheckTextView";

    public void changeAnswerState(int position, @CheckState int checkState) {
        ClozeAnswerBean bean = mAnswerMap.valueAt(position);
        if (bean == null) {
            Log.e(TAG, position + "位置上没有数据");
            return;
        }
        bean.setCheckState(checkState);
        buildText();
    }

    protected void setInputImageList() {
        mInputImageList.clear();
        for (int i = 0; i < mAnswerMap.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            if (bean == null) {
                continue;
            }
            if (InputTools.isKeyboard(bean.getInputMethod()) && !TextUtils.isEmpty(bean.getAnswer())) {
                mInputImageList.add(bean.getAnswer());
            }
        }
    }

    /**
     * 设置文本后将原始文本与填充内容整合显示
     */
    private void buildText() {
        setInputImageList();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int count = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {
                int length = builder.length();
                builder.append(BLANK_CELL_STR);
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                builder.append(bean.getValue());
                //此位置可以插入图片
                //输入内容为文本或无作答时按文本展示
                if (InputTools.isKeyboard(bean.getInputMethod()) || TextUtils.isEmpty(bean.getAnswer())) {
                    if (TextUtils.isEmpty(bean.getAnswer())) {
                        builder.append(BLANK_CELL_STR);
                        builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }else {
                        builder.append(bean.getAnswer()).append(BLANK_CELL_STR);
                        builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    switch (bean.getCheckState()) {
                        case CheckState.TRUE:
                            builder.setSpan(new ForegroundColorSpan(trueTextColor), length, builder.length() - 1,
                                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            break;
                        case CheckState.FALSE:
                            builder.setSpan(new ForegroundColorSpan(falseTextColor), length, builder.length() - 1,
                                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            break;
                        case CheckState.UN_CHECK:
                        default:
                            builder.setSpan(new ForegroundColorSpan(defaultTextColor), length, builder.length() - 1,
                                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            break;
                    }
                } else {//否则按图片展示
                    appendImg(builder, bean.getAnswer());
                    @DrawableRes int resId;
                    switch (bean.getCheckState()) {
                        case CheckState.TRUE:
                            resId = trueIcon;
                            break;
                        case CheckState.FALSE:
                            resId = falseIcon;
                            break;
                        case CheckState.UN_CHECK:
                        default:
                            resId = -1;
                            break;
                    }
                    if (resId != -1) {
                        int len = builder.length();
                        builder.append("\uFFFC");
                        Drawable drawable = getContext().getResources().getDrawable(resId);
                        drawable.setBounds(0,0, Dp2Px.convert(getContext(),20),Dp2Px.convert(getContext(),20));
                        builder.setSpan(new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE), len, builder.length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    builder.append(BLANK_CELL_STR);
                    builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                count++;
            } else {//文本
                builder.append(sequence);
            }
        }
        setText(builder);
    }

    /**
     * 将图片插入到空位上
     *
     * @param src 图片地址
     */
    private void appendImg(SpannableStringBuilder text, String src) {
        Drawable d;

        d = getDrawable(src);

        if (d == null) {
            d = getResources().getDrawable(R.drawable.default_unknow_image);
//            d = Resources.getSystem().getDrawable(
//                    Resources.getSystem().getIdentifier("unknown_image", "drawable", "android"));
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());d.getBounds();
        }else {
            Bitmap bmp = ((BitmapDrawable)d).getBitmap();
            int height = bmp.getHeight();
            int width = bmp.getWidth();
            if (height > 45) {
                float scale = 45f / height;
                Matrix matrix = new Matrix();
                matrix.postScale(scale,scale);
                bmp = Bitmap.createBitmap(bmp,0,0,width,height,matrix,true);
                d = new BitmapDrawable(bmp);
                width = (int) (width * scale);
                height = (int) (height * scale);
                d.setBounds(0,0,width,height);
            }
        }

        int len = text.length();
        text.append("\uFFFC");

        text.setSpan(new ImageSpan(d, src,ImageSpan.ALIGN_BASELINE), len, text.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 点击时判断点击位置, 当点击在图片空位上时触发回调
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //控件中文本所占空间
        int left = getPaddingLeft();
        int right = getWidth() - getPaddingRight();
        int top = getPaddingTop();
        int bottom = getHeight() - getPaddingBottom();
        //触摸位置
        float x = event.getX();
        float y = event.getY();

        //padding外, 清除光标
        if (x < left || x > right || y < top || y > bottom) {
            return super.onTouchEvent(event);
        }

        //相对文本的坐标
        x -= left;
        y -= top;

        int startCharIndex = 0;//空开始位置
        int endCharIndex = 0;//空结束位置
        int blankCount = 0;//第几个空

        int textIndex = getPreciseOffset(x, y);
        //找到每个空格的位置
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {//空格位
                //空格位结束位点+文本内容空间
                int charCount;
                ClozeAnswerBean bean = mAnswerMap.valueAt(blankCount);
                if (InputTools.isKeyboard(bean.getInputMethod())) {
                    if (bean.getAnswer()!=null){
                        charCount = bean.getAnswer().length();
                    }else {
                        charCount=10;
                    }

                } else {
                    charCount = TextUtils.isEmpty(bean.getAnswer()) ? 0 : 1;//图片占位
                    if ((bean.getCheckState() == CheckState.TRUE && trueIcon != -1) ||
                            (bean.getCheckState() == CheckState.FALSE && falseIcon != -1)) {
                        charCount += 1;
                    }
                }
                endCharIndex += charCount;
                //空格位开始位点不动, 结束位点+12;
                endCharIndex += DOUBLE_BLANK_CELL_COUNT;
                endCharIndex += bean.getValue().length();

                if (!InputTools.isKeyboard(bean.getInputMethod()) &&
                        textIndex >= startCharIndex &&
                        textIndex <= endCharIndex &&
                        !TextUtils.isEmpty(bean.getAnswer())) {
                    if (mListener != null) {
                        mListener.clickImage(bean.getAnswer());
                    }
                    return false;
                }
                blankCount++;
                startCharIndex += charCount;
                startCharIndex += DOUBLE_BLANK_CELL_COUNT;
                startCharIndex += bean.getValue().length();
            } else {
                startCharIndex += sequence.length();
                endCharIndex += sequence.length();
            }
        }
        return false;
    }

    /**
     * 获取指定坐标是第几个字符
     */
    private int getPreciseOffset(float x, float y) {
        Layout layout = getLayout();
        if (layout != null) {
            int topVisibleLine = layout.getLineForVertical((int) y);
            int offset = layout.getOffsetForHorizontal(topVisibleLine, x);

            int offsetX = (int) layout.getPrimaryHorizontal(offset);

            if (offsetX > x) {
                return layout.getOffsetToLeftOf(offset);
            } else {
                return offset;
            }
        } else {
            return -1;
        }
    }

    /*================ 图片 空位解析 ================*/
    @Override
    protected void glideLoadImageSuccess() {
        SparseArray<ClozeAnswerBean> list = new SparseArray<>();
        for (int i = 0; i < mAnswerMap.size(); i++) {
            list.put(mAnswerMap.keyAt(i), mAnswerMap.valueAt(i));
        }
        setHtmlBody(html, list);
    }

    /**
     * 解析获取图片信息和空位信息
     */
    private void getImageSourceAndInput(String source) {
        html = source;
        mMap.clear();
        mAnswerMap.clear();
        Document doc = Jsoup.parse(source);
        //解析img标签
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (TextUtils.isEmpty(width)) {
                width = "0";
            }
            if (TextUtils.isEmpty(height)) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
        //解析input标签
        Elements input = doc.getElementsByTag("input");
        for (Element element : input) {
            int index = Integer.valueOf(element.attr("index").trim());
            String value = element.attr("value");
            mAnswerMap.put(index, new ClozeAnswerBean(index, value));
        }
    }

    /*================ 图片 空位解析 ================*/

    public SparseArray<ClozeAnswerBean> getAnswerMap() {
        return mAnswerMap;
    }

    public void setFillBlankCheckClickListener(ImageClickListener listener) {
        mListener = listener;
    }

    public void setDefaultTextColor(int defaultTextColor) {
        this.defaultTextColor = defaultTextColor;
    }

    public void setTrueTextColor(int trueTextColor) {
        this.trueTextColor = trueTextColor;
    }

    public void setFalseTextColor(int falseTextColor) {
        this.falseTextColor = falseTextColor;
    }

    public void setTrueIcon(@DrawableRes int trueIcon) {
        this.trueIcon = trueIcon;
    }

    public void setFalseIcon(@DrawableRes int falseIcon) {
        this.falseIcon = falseIcon;
    }

    public interface ImageClickListener {
        /**
         * 填空图片点击回调
         *
         * @param imageSrc 图片地址
         */
        void clickImage(String imageSrc);
    }
}
