package com.sdzn.fzx.student.libpublic.views.statistic_chat;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.sdzn.fzx.student.libutils.util.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bin_li on 2017/4/8.
 */

public class StatisticsChat extends View {

    private List<ItemInfoVo> mDatas = new ArrayList<>();

    private String COLUMN_Text[] = {};
    private final static String UNIT = "NO.";
    //垂直能量柱的宽度
    private final static int COLUMN_WIDTH = 6;
    private final static int LEVEL_WIDTH = 3;

    private final static String COLOR_BG = "#F6F5F3";
    private final static String COLOR_SELECTED_BG = "#FFFFFF";
    private final static String COLOR_LINE = "#CBCBCB";
    private final static String COLOR_COLUMN = "#DDDDDD";
    private final static String COLOR_COLUMN_SELECTED = "#27D4D7";
    private final static String COLOR_TEXT = "#747578";
    private final static String COLOR_TEXT_NO = "#b1b1b1";
    private final static String COLOR_TEXT_SELETED = "#4FC602";
    private final static String COLOR_LEVEL_START = "#00ACF6";
    private final static String COLOR_LEVEL_END = "#4FC602";
    private int mColumnBottomH;

    private int mChatWidth;
    private int mChatHeight;
    private int mUnitWidth;
//    private Bitmap mPointBitmap;

    private Paint mBgPaint;
    private Paint mSelectBgPaint;

    private Paint mChatLinePaint;

    private Paint mColumnPaint;
    private Paint mSelectColumnPaint;
    private Paint mLevelPaint;

    private TextPaint mTxtPaint;
    private TextPaint mSelectPaint;
    private TextPaint mNoPaint;
    private Paint mBitmapPaint;

    private int mCurShwoItem = 0;


    private GestureDetector mGestureDetector;
    private int mMaxHorizontalScrollDis;


    public StatisticsChat(Context context) {
        super(context);
        init();
    }


    public StatisticsChat(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        initPaint();
        mGestureDetector = new GestureDetector(getContext(), new OnGestureListener());
    }

    private void initPaint() {
        mBgPaint = new Paint();
        mBgPaint.setAntiAlias(true);
        mBgPaint.setStyle(Paint.Style.FILL);
        mBgPaint.setStrokeWidth(UiUtils.dp2px(getContext(), COLUMN_WIDTH));
        mBgPaint.setColor(Color.parseColor(COLOR_BG));

        mSelectBgPaint = new Paint();
        mSelectBgPaint.setAntiAlias(true);
        mSelectBgPaint.setStyle(Paint.Style.FILL);
        mSelectBgPaint.setStrokeWidth(UiUtils.dp2px(getContext(), COLUMN_WIDTH));
        mSelectBgPaint.setColor(Color.parseColor(COLOR_SELECTED_BG));

        mChatLinePaint = new Paint();
        mChatLinePaint.setAntiAlias(true);
        mChatLinePaint.setStyle(Paint.Style.FILL);
        mChatLinePaint.setStrokeWidth(1);
        mChatLinePaint.setColor(Color.parseColor(COLOR_LINE));

        mColumnPaint = new Paint();
        mColumnPaint.setAntiAlias(true);
        mColumnPaint.setStyle(Paint.Style.FILL);
        mColumnPaint.setStrokeWidth(UiUtils.dp2px(getContext(), COLUMN_WIDTH));
        mColumnPaint.setStrokeCap(Paint.Cap.ROUND);
        mColumnPaint.setColor(Color.parseColor(COLOR_COLUMN));

        mSelectColumnPaint = new Paint();
        mSelectColumnPaint.setAntiAlias(true);
        mSelectColumnPaint.setStyle(Paint.Style.FILL);
        mSelectColumnPaint.setStrokeWidth(UiUtils.dp2px(getContext(), COLUMN_WIDTH));
        mSelectColumnPaint.setStrokeCap(Paint.Cap.ROUND);
        mSelectColumnPaint.setColor(Color.parseColor(COLOR_COLUMN_SELECTED));

        mLevelPaint = new Paint();
        mLevelPaint.setAntiAlias((true));
        mLevelPaint.setStrokeWidth(UiUtils.dp2px(getContext(), COLUMN_WIDTH / 2));
        mLevelPaint.setStyle(Paint.Style.STROKE);
        mLevelPaint.setAntiAlias(true);
        mLevelPaint.setStrokeCap(Paint.Cap.ROUND);


        mTxtPaint = new TextPaint();
        mTxtPaint.setAntiAlias(true);
        mTxtPaint.setTextAlign(TextPaint.Align.CENTER);
        mTxtPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mTxtPaint.setColor(Color.parseColor(COLOR_TEXT));
        mTxtPaint.setTextSize(UiUtils.sp2px(getContext(), 12));

        mNoPaint = new TextPaint();
        mNoPaint.setAntiAlias(true);
        mNoPaint.setTextAlign(TextPaint.Align.CENTER);
        mNoPaint.setTypeface(Typeface.DEFAULT);
        mNoPaint.setColor(Color.parseColor(COLOR_TEXT_NO));
        mNoPaint.setTextSize(UiUtils.sp2px(getContext(), 12));


        mSelectPaint = new TextPaint();
        mSelectPaint.setAntiAlias(true);
        mSelectPaint.setTextAlign(TextPaint.Align.CENTER);
        mSelectPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mSelectPaint.setColor(Color.parseColor(COLOR_TEXT_SELETED));
        mSelectPaint.setTextSize(UiUtils.sp2px(getContext(), 12));

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mChatWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        mChatHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        mUnitWidth = mChatWidth / 7;
        if (mDatas.size() > 7) {
            mMaxHorizontalScrollDis = mDatas.size() * mUnitWidth - mChatWidth;
        } else {
            mMaxHorizontalScrollDis = 0;
        }
        mColumnBottomH = UiUtils.dp2px(getContext(), 12);
    }


    /**
     * 更新圆弧画笔
     */
    private void updateArcPaint(float startX, float startY, float endX, float endY) {
        // 设置渐变
        LinearGradient linerGradient = new LinearGradient(startX, startY, endX, endY,
                Color.parseColor(COLOR_LEVEL_START), Color.parseColor(COLOR_LEVEL_END),
                Shader.TileMode.CLAMP);
        mLevelPaint.setShader(linerGradient);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBg(canvas);
        drawChatLine(canvas);
        drawColumn(canvas);
//        drawScrollLine(canvas);
        drawText(canvas);
//        drawPoint(canvas);
    }

    private void drawBg(Canvas canvas) {
        RectF rectF = new RectF();
        rectF.left = 0;
        rectF.top = 0;
        rectF.right = getScrollX() + mChatWidth;
        rectF.bottom = mChatHeight;
        canvas.drawRect(rectF, mBgPaint);


        RectF showRectF = new RectF();
        showRectF.left = mCurShwoItem * mUnitWidth;
        showRectF.right = (mCurShwoItem + 1) * mUnitWidth;
        showRectF.top = 0;
        showRectF.bottom = mChatHeight;
        canvas.drawRect(showRectF, mSelectBgPaint);

        RectF bottomRectF = new RectF();
        bottomRectF.left = showRectF.left;
        bottomRectF.right = showRectF.right;
        showRectF.top = mChatHeight - mColumnBottomH / 5;
        showRectF.bottom = mChatHeight;
        canvas.drawRect(showRectF, mSelectColumnPaint);
    }

    private void drawChatLine(Canvas canvas) {
        int size = Math.max(mDatas.size(), 7);
        for (int i = 1; i < size; i++) {
            canvas.drawLine(i * mUnitWidth, 0, i * mUnitWidth, mChatHeight, mChatLinePaint);
        }
    }

    private void drawColumn(Canvas canvas) {
        int columnH = mChatHeight - mColumnBottomH;
        for (int i = 0; i < mDatas.size(); i++) {
            final int startX = i * mUnitWidth + mUnitWidth / 2;
            final int startY = columnH - (int) (columnH * mDatas.get(i).getValue() * 5 / 7);
            Paint paint = i == mCurShwoItem ? mSelectColumnPaint : mColumnPaint;
            canvas.drawLine(startX, startY, startX, columnH, paint);
        }
    }

    private void drawScrollLine(Canvas canvas) {
        if (mDatas.size() <= 0)
            return;
        updateArcPaint(0, 0,
                mDatas.get(mDatas.size() - 1).getValue() * mUnitWidth + mUnitWidth / 2,
                mChatHeight - (int) (mChatHeight * mDatas.get(mDatas.size() - 1).getValue() * 5 / 7)
        );
        final int increaseH = UiUtils.dp2px(getContext(), 15);
        int columnH = mChatHeight - mColumnBottomH;
        for (int i = 0; i < mDatas.size() - 1; i++) {
            final int startX = i * mUnitWidth + mUnitWidth / 2;
            final int startY = columnH - (int) (columnH * mDatas.get(i).getValue() * 5 / 7) - increaseH;
            final int endX = (i + 1) * mUnitWidth + mUnitWidth / 2;
            final int endY = columnH - (int) (columnH * mDatas.get(i + 1).getValue() * 5 / 7) - increaseH;

            int wt = (startX + endX) / 2;
            Point p3 = new Point();
            Point p4 = new Point();
            p3.y = startY;
            p3.x = wt;
            p4.y = endY;
            p4.x = wt;

            Path path = new Path();
            path.moveTo(startX, startY);
            path.cubicTo(p3.x, p3.y, p4.x, p4.y, endX, endY);

            canvas.drawPath(path, mLevelPaint);
        }
    }

    public void setColumnHeightSet(List<ItemInfoVo> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        this.mDatas = list;
        if (mDatas.size() > 7) {
            mMaxHorizontalScrollDis = mDatas.size() * mUnitWidth - mChatWidth;
        } else {
            mMaxHorizontalScrollDis = 0;
        }
        int item = mDatas.size() > 0 ? mDatas.size() - 1 : 0;
        setShowItem(item);
    }

    private void drawText(Canvas canvas) {
        float dis = UiUtils.dp2px(getContext(), 5);
        for (int i = 0; i < mDatas.size(); i++) {
            float x = i * mUnitWidth + mUnitWidth / 2;
            float y1 = dis;
            TextPaint noPaint = i == mCurShwoItem ? mSelectPaint : mNoPaint;
            StaticLayout layout = new StaticLayout(mDatas.get(i).getName(), noPaint, mUnitWidth, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, true);

            canvas.save();
            canvas.translate(x, y1);
            layout.draw(canvas);
            canvas.restore();
        }
    }

    private void setShowItem(int item) {
        mCurShwoItem = item;
        if (itemClickListener != null) {
            if (item < mDatas.size()) {
                itemClickListener.onItemChanged(mDatas.get(item));
            }
        }
        invalidate();
    }

    /**********************
     * 手势区域分割线
     ********************/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            float x = e.getX() + getScrollX();
            int item = (int) (x / mUnitWidth);
            if (item < mDatas.size())
                setShowItem(item);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            if (Math.abs(distanceX) < Math.abs(distanceY)) {
                return false;
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);//屏蔽父控件拦截onTouch事件
            }
            int dx = (int) distanceX;
            int x = getScrollX();

            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (mMaxHorizontalScrollDis < 0) {
                dx = 0;
            } else if (x + dx > mMaxHorizontalScrollDis) {
                dx = mMaxHorizontalScrollDis - x;
            }

            scrollBy(dx, 0);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {

            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    public static class ItemInfoVo {
        private String name;
        private float value;
        private String id;

        public ItemInfoVo() {
        }

        public ItemInfoVo(String id, String name, float value) {
            this.name = name;
            this.value = value;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    private ItemChangedListener itemClickListener;

    public void setItemClickListener(ItemChangedListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemChangedListener {
        void onItemChanged(ItemInfoVo pos);
    }

}
