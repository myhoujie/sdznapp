package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.yanyusong.y_divideritemdecoration.Dp2Px;

import org.xml.sax.Attributes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * TextView展示html文本基类
 *
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-15
 */
public abstract class BaseHtmlTextView extends TextView implements HtmlTagHandler.ImageGetter, HtmlTagHandler.TagHandler {
    protected HashMap<String, Point> mMap = new HashMap<>();//图片map
    protected String html;
    protected ArrayList<String> mInputImageList = new ArrayList<>();//作答图片路径
    protected float mMaxImageHeight;//填空作答图片最大高度
    protected static final String DEFAULT_TAG = "<input>";
    protected static final String BLANK_CELL_STR = "      ";

    public BaseHtmlTextView(Context context) {
        super(context);
        init();
    }

    public BaseHtmlTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public BaseHtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mMaxImageHeight = Dp2Px.convert(getContext(), 32);
        setLineSpacing(12, 1);
    }

    @Override
    public Drawable getDrawable(String source) {
        Drawable drawable;
        if (source == null) {
            return null;
        }
        if (source.startsWith("data:image/png;base64,")) {
            String src = source.split("data:image/png;base64,")[1];
            byte[] bytes = Base64.decode(src, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            drawable = new BitmapDrawable(bitmap);
            resetDrawableBounds(null, drawable, bitmap, source);
            return drawable;
        }
        String fileName = getFileName(source);
        File file = new File(getContext().getExternalCacheDir(), fileName);
        Bitmap bitmap = ImageManager.getInstance().getCache(file.getAbsolutePath());
        if (bitmap != null) {
            drawable = new BitmapDrawable(bitmap);
            Point point = mMap.get(source);
            resetDrawableBounds(point, drawable, bitmap, source);
            return drawable;
        }
        if (file.exists()) {
            drawable = Drawable.createFromPath(file.getAbsolutePath());
            if (drawable == null) {
                return null;
            }
            Bitmap bmp = ((BitmapDrawable) drawable).getBitmap();
            ImageManager.getInstance().addCache(file.getAbsolutePath(), bmp);
            Point point = mMap.get(source);
            resetDrawableBounds(point, drawable, bmp, source);
            return drawable;
        } else {
            final File f2 = file;
            Glide.with(getContext()).load(source).downloadOnly(new SimpleTarget<File>() {
                @Override
                public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
                    try {
                        FileUtil.copyFile(resource.getAbsolutePath(), f2.getAbsolutePath());
                        ImageManager.getInstance().addCache(f2.getAbsolutePath(), BitmapFactory.decodeFile(f2.getAbsolutePath()));
                        glideLoadImageSuccess();
//                        setText(HtmlTagHandler.fromHtml(html, BaseHtmlTextView.this, BaseHtmlTextView.this));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            return null;
        }
    }


    /**
     * 设置图片尺寸, 不要太大(手动上传的图片)或太小(化学方程式)
     *
     * @param source   图片地址
     * @param bitmap   图片
     * @param drawable 图片drawable
     * @param point    html中解析得到的图片宽高
     */
    private void resetDrawableBounds(Point point, Drawable drawable, Bitmap bitmap, String source) {
        if (point == null || point.x <= 0 || point.y <= 0) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (mInputImageList.contains(source)) {//填空题作答的图片
                float f = mMaxImageHeight / height;
                width *= f;
                height *= f;
            } else if (width <= 50 || height <= 50) {
                width *= 2;
                height *= 2;
            }
            drawable.setBounds(0, 10, width, height + 10);//小图有出现图片顶端超出控件的问题
        } else {
            drawable.setBounds(0, 0, point.x, point.y);
        }
    }

    /**
     * 子类重写该方法, 区分处理图片加载完成后的逻辑
     */
    protected abstract void glideLoadImageSuccess();

    /**
     * 七牛云图片显示逻辑修改
     */
    protected String getFileName(String url) {
        //七牛云文件
//        if (url.contains("file.fuzhuxian.com") || url.contains("csfile.fuzhuxian.com")) {
//            String rotate = url.split("/rotate/")[1];
//            String fileUrl = url.split("\\?")[0];
//            String[] split = fileUrl.split("/");
//            return rotate + "_" + split[split.length - 1];
//        }
        //普通文件
        String[] split = url.split("/");
        return split[split.length - 1];
    }

    @Override
    public void handleTag(boolean opening, String tag, Editable output, @Nullable Attributes attrs) {
        switch (tag.toLowerCase(Locale.getDefault())) {
//            case "img":
//                handleImg(opening, tag, output, attrs);
//                break;
            case "input":
                handleInput(opening, tag, output, attrs);
                break;
//            case "table":
//                handleTable(opening, tag, output, attrs);
//                break;
//            case "tbody":
//                handleTbody(opening, tag, output, attrs);
//                break;
//            case "tr":
//                handleTr(opening, tag, output, attrs);
//                break;
//            case "td":
//                handleTd(opening, tag, output, attrs);
//                break;
            default:
                break;
        }
    }

//    private void handleImg(boolean opening, String tag, Editable output, Attributes xmlReader) {
//        // 获取长度
//        int len = output.length();
//        // 获取图片地址
//        ImageSpan[] images = output.getSpans(len - 1, len, ImageSpan.class);
//        String imgURL = images[0].getSource();
//        output.setSpan(new ClickableImage(imgURL), len - 1, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//    }

    private void handleInput(boolean opening, String tag, Editable output, Attributes xmlReader) {
        if (!opening) {
            output.append(DEFAULT_TAG);
        }
    }

//    private void handleTable(boolean opening, String tag, Editable output, Attributes xmlReader) {
//        if (!opening) {
//            output.append("\n");
//        }
//    }
//
//    private void handleTbody(boolean opening, String tag, Editable output, Attributes xmlReader) {
//    }
//
//    private void handleTr(boolean opening, String tag, Editable output, Attributes xmlReader) {
//        if (!opening) {
//            output.append("\n");
//        }
//    }
//
//    private void handleTd(boolean opening, String tag, Editable output, Attributes xmlReader) {
//        if (!opening) {
//            output.append("\t");
//        }
//    }

    protected class ClickableImage extends ClickableSpan {
        private String url;

        public ClickableImage(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View widget) {
            if (mListener != null) {
                mListener.clickImage(url);
            }
        }
    }

    protected ImageClickListener mListener;

    public void setListener(ImageClickListener listener) {
        mListener = listener;
    }

    public interface ImageClickListener {
        void clickImage(String url);
    }
}
