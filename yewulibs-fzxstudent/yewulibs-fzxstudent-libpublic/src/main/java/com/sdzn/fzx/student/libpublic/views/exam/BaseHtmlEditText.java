package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.yanyusong.y_divideritemdecoration.Dp2Px;

import org.xml.sax.Attributes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * EditText展示html文本基类
 *
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-31
 */
public abstract class BaseHtmlEditText extends EditText implements HtmlTagHandler.ImageGetter, HtmlTagHandler.TagHandler {

    protected HashMap<String, Point> mMap = new HashMap<>();//图片map
    protected ArrayList<String> mInputImageList = new ArrayList<>();//作答图片路径
    protected String html;
    protected float mMaxImageHeight;//填空作答图片最大高度

    public BaseHtmlEditText(Context context) {
        super(context);
        init();
    }

    public BaseHtmlEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseHtmlEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public BaseHtmlEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    protected void init() {
        mMaxImageHeight = Dp2Px.convert(getContext(), 32);
        setLineSpacing(12, 1);
        setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        setSingleLine(false);
        setBackground(null);
        setInputFilter(null);
        setLongClickable(false);
        setTextIsSelectable(false);
        setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    /**
     * 设置输入过滤器
     */
    protected abstract void setInputFilter(InputFilter filter);

    /**
     * 设置作答结果图片路径, 在子类调用bulidText()之前调用
     */
    protected abstract void setInputImageList();

    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        return true;
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        outAttrs.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI;
        return super.onCreateInputConnection(outAttrs);
    }

    @Override
    public Drawable getDrawable(String source) {
        Drawable drawable;
        if (source == null) {
            return null;
        }
        if (source.startsWith("data:image/png;base64,")) {
            String src = source.split("data:image/png;base64,")[1];
            byte[] bytes = Base64.decode(src, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            drawable = new BitmapDrawable(bitmap);
            resetDrawableBounds(null, drawable, bitmap, source);
            return drawable;
        }
        String fileName = getFileName(source);
        File file = new File(getContext().getExternalCacheDir(), fileName);
        Bitmap bitmap = ImageManager.getInstance().getCache(file.getAbsolutePath());
        if (bitmap != null) {
            drawable = new BitmapDrawable(bitmap);
            Point point = mMap.get(source);
            resetDrawableBounds(point, drawable, bitmap, source);
            return drawable;
        }
        if (file.exists()) {
            drawable = Drawable.createFromPath(file.getAbsolutePath());
            if (drawable == null) {
                return null;
            }
            Bitmap bmp = ((BitmapDrawable) drawable).getBitmap();
            ImageManager.getInstance().addCache(file.getAbsolutePath(), bmp);
            Point point = mMap.get(source);
            resetDrawableBounds(point, drawable, bmp, source);
            return drawable;
        } else {
            final File f2 = file;
            Glide.with(getContext()).load(source).downloadOnly(new SimpleTarget<File>() {
                @Override
                public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
                    try {
                        FileUtil.copyFile(resource.getAbsolutePath(), f2.getAbsolutePath());
                        ImageManager.getInstance().addCache(f2.getAbsolutePath(), BitmapFactory.decodeFile(f2.getAbsolutePath()));
                        glideLoadImageSuccess();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            return null;
        }
    }

    /**
     * 设置图片尺寸, 不要太大(手动上传的图片)或太小(化学方程式)
     *
     * @param source   图片地址
     * @param bitmap   图片
     * @param drawable 图片drawable
     * @param point    html中解析得到的图片宽高
     */
    private void resetDrawableBounds(Point point, Drawable drawable, Bitmap bitmap, String source) {
        if (point == null || point.x <= 0 || point.y <= 0) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (mInputImageList.contains(source)) {//填空题作答的图片
                float f = mMaxImageHeight / height;
                width *= f;
                height *= f;
            } else if (width <= 50 || height <= 50) {
                width *= 2;
                height *= 2;
            }
            drawable.setBounds(0, 10, width, height + 10);//小图有出现图片顶端超出控件的问题
        } else {
            drawable.setBounds(0, 0, point.x, point.y);
        }
    }

    /**
     * 七牛云图片显示逻辑修改
     */
    protected String getFileName(String url) {
        String[] split = url.split("/");
        return split[split.length - 1];
    }

    /**
     * 子类重写该方法, 区分处理图片加载完成后的逻辑
     */
    protected abstract void glideLoadImageSuccess();

    @Override
    public void handleTag(boolean opening, String tag, Editable output, @Nullable Attributes attrs) {
        switch (tag.toLowerCase(Locale.getDefault())) {
            case "img":
                handleImg(opening, tag, output, attrs);
                break;
            case "input":
                handleInput(opening, tag, output, attrs);
                break;
            default:
                break;
        }
    }

    protected void handleImg(boolean opening, String tag, Editable output, Attributes xmlReader) {
        // 获取长度
        int len = output.length();
        // 获取图片地址
        ImageSpan[] images = output.getSpans(len - 1, len, ImageSpan.class);
        String imgURL = images[0].getSource();
        output.setSpan(new ClickableImage(imgURL), len - 1, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    protected abstract void handleInput(boolean opening, String tag, Editable output, Attributes xmlReader);

    protected class ClickableImage extends ClickableSpan {
        private String url;

        public ClickableImage(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View widget) {
            if (mListener != null) {
                mListener.clickImage(url);
            }
        }
    }

    protected ImageClickListener mListener;

    public void setImageClickListener(ImageClickListener listener) {
        mListener = listener;
    }

    public interface ImageClickListener {
        void clickImage(String url);
    }
}
