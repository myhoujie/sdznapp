package com.sdzn.fzx.student.libpublic.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.sdzn.fzx.student.libpublic.R;


//新建一个类继承View
public class DrawImageView extends View {
    public Paint paint;//声明画笔
    public Canvas canvas;//画布
    public Bitmap backBitmap;
    public static final int penConnectedRes = R.drawable.pcover;
    public static final int penUnConnectedRes = R.drawable.pen_unconnected;
    private int currectBg = R.drawable.pcover;


    public DrawImageView(Context context) {
        super(context);
        setWillNotDraw(false);
        initDraw(context);
    }

    public DrawImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        initDraw(context);
    }

    public DrawImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
        initDraw(context);
    }

    @TargetApi(21)
    public DrawImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setWillNotDraw(false);
        initDraw(context);
    }

    //画位图
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(backBitmap, 0, 0, paint);
    }

    public void setImageResource(@DrawableRes int resId) {
        createBitmap(getContext(), resId);
        canvas.setBitmap(backBitmap);
        postInvalidate();
    }

    public void setPenIsConnected(boolean isConnected) {
        currectBg = isConnected ? penConnectedRes : penUnConnectedRes;
    }

    public void initDraw(Context context) {
        createBitmap(context, currectBg);
        paint = new Paint(Paint.DITHER_FLAG);//创建一个画笔
        canvas = new Canvas();
        canvas.setBitmap(backBitmap);
        paint.setStyle(Style.STROKE);//设置非填充
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);//锯齿不显示
        paint.setDither(true);  //防抖动
        requestLayout();
        invalidate();
    }

    public void initDraw(Context context, @DrawableRes int resId) {
        createBitmap(context, resId);
        paint = new Paint(Paint.DITHER_FLAG);//创建一个画笔
        canvas = new Canvas();
        canvas.setBitmap(backBitmap);
        paint.setStyle(Style.STROKE);//设置非填充
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);//锯齿不显示
        paint.setDither(true);  //防抖动
        postInvalidate();
    }

    private void createBitmap(Context context, @DrawableRes int resId) {
        backBitmap = BitmapFactory.decodeResource(context.getResources(), resId);
        int wid = backBitmap.getWidth();
        int hei = backBitmap.getHeight();
        Matrix matrix = new Matrix();
        float wid2 = 992f / wid;
        float hei2 = 1403f / hei;
        matrix.postScale(wid2, hei2);
        backBitmap = Bitmap.createBitmap(backBitmap, 0, 0, wid, hei, matrix, true);
    }

    public void DrawDestroy() {
        paint = null;
        canvas = null;
    }

    public void drawLine(float fromX, float fromY, float toX, float toY) {
        canvas.drawLine(fromX, fromY, toX, toY, paint);
        postInvalidate();
    }
}
