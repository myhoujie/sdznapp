package com.sdzn.fzx.student.libpublic.views;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatEditText;

import com.sdzn.fzx.student.libpublic.R;

/**
 * CalenderClearEditText〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class CalenderClearEditText extends AppCompatEditText {

    private static final int DRAWABLE_LEFT = 0;
    private static final int DRAWABLE_TOP = 1;
    private static final int DRAWABLE_RIGHT = 2;
    private static final int DRAWABLE_BOTTOM = 3;
    private Drawable mClearDrawable;
    private Drawable mCalenderDrawable;

    public CalenderClearEditText(Context context) {
        super(context);
        init();
    }

    public CalenderClearEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CalenderClearEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
//        mClearDrawable = getResources().getDrawable(R.mipmap.qcwz_icon);
//        mCalenderDrawable = getResources().getDrawable(R.mipmap.date_icon);
        mClearDrawable = getResources().getDrawable(R.drawable.new_icon_gbtime);
        mCalenderDrawable = getResources().getDrawable(R.drawable.new_icon_titletime);
        final Editable text = getText();
        setClearIconVisible(text != null && text.length() > 0);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        setClearIconVisible(text.length() > 0);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        setClearIconVisible(length() > 0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                final boolean empty = TextUtils.isEmpty(getText());
                if (empty) {
                    break;
                }
                Drawable drawable = getCompoundDrawables()[DRAWABLE_RIGHT];
                if (drawable != null && event.getX() <= (getWidth() - getPaddingRight())
                        && event.getX() >= (getWidth() - getPaddingRight() - drawable.getBounds().width())) {
                    setText("");
                    if (listener != null) {
                        listener.onTextClear();
                    }
                    return false;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void setClearIconVisible(boolean visible) {
        setCompoundDrawablesWithIntrinsicBounds(getCompoundDrawables()[DRAWABLE_LEFT], getCompoundDrawables()[DRAWABLE_TOP],
                visible ? mClearDrawable : mCalenderDrawable, getCompoundDrawables()[DRAWABLE_BOTTOM]);
    }

    private ClearableEditText.ClearTextListener listener;

    public void setClearTextListener(ClearableEditText.ClearTextListener listener) {
        this.listener = listener;
    }

    public interface ClearTextListener {
        void onTextClear();
    }
}
