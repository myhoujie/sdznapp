package com.sdzn.fzx.student.libpublic.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdzn.fzx.student.libpublic.R;


/**
 * 自定义图标说明
 *
 * @author wangchunxiao
 * @date 2018/1/9
 */
public class ChartLegendView extends LinearLayout {

    private View vDot;
    private TextView tvName;
    private TextView tvCount;
    private Context context;

    public ChartLegendView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.view_main_fragment_chart_legend, this, true);
        vDot = findViewById(R.id.vDot);
        tvName = findViewById(R.id.tvName);
        tvCount = findViewById(R.id.tvCount);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ChartLegendView);
        if (attributes != null) {
            Drawable drawable = attributes.getDrawable(R.styleable.ChartLegendView_dot);
            if (drawable != null) {
                vDot.setBackgroundDrawable(drawable);
            }
            String subject = attributes.getString(R.styleable.ChartLegendView_subject);
            setText(subject);
            int count = attributes.getInteger(R.styleable.ChartLegendView_count, 0);
            tvCount.setText(String.valueOf(count));
            attributes.recycle();
        }
    }

    private void setText(String subject) {
        if (subject != null) {
            tvName.setText(subject);
        }
    }

    public void setTextColor(int color) {
        tvCount.setTextColor(color);
    }

    public void setSubject(String subject) {
        setText(subject);
    }

    public void setCount(float count) {
        tvCount.setText(String.valueOf(count));
    }

    public void setCount(int count) {
        tvCount.setText(String.valueOf(count));
    }

    public void setNameColor(int color) {
        tvName.setTextColor(color);
    }
}
