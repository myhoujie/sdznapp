package com.sdzn.fzx.student.libpublic.utils;

import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;

import java.io.File;
import java.io.IOException;

/**
 * MediaRecorderManager〈一句话功能简述〉
 * 〈录音管理类〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class MediaRecorderManager {

    private MediaRecorder mediaRecorder;

    private static MediaRecorderManager mInstance;

    private boolean isAlive;

    private MediaRecorderManager() {
    }

    public synchronized static MediaRecorderManager getInstance() {
        if (mInstance == null) {
            mInstance = new MediaRecorderManager();
        }
        return mInstance;
    }

    public MediaRecorderManager init() {
        if (mediaRecorder == null) {
            mediaRecorder = new MediaRecorder();
        }
        mediaRecorder.reset();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
        mediaRecorder.setAudioSamplingRate(16000);
        return this;
    }

    public String start(String fileName) throws IOException {

        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        mediaRecorder.setOutputFile(file.getAbsolutePath());
        mediaRecorder.setMaxDuration(1000 * 60 * 10);
        mediaRecorder.prepare();
        mediaRecorder.start();
        startUpataDecibe();
        return file.getAbsolutePath();
    }

    public void reset() {
        if (mediaRecorder != null)
            mediaRecorder.reset();
    }

    public void stop() {
        isAlive = false;
        mediaRecorder.stop();
    }

    public void release() {
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    private void startUpataDecibe() {
        isAlive = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isAlive) {
                    handler.sendEmptyMessage(0);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (mediaRecorder == null) return;
            double ratio = (double) mediaRecorder.getMaxAmplitude() / 100;
            double db = 0;// 分贝
            //默认的最大音量是100,可以修改，但其实默认的，在测试过程中就有不错的表现
            //你可以传自定义的数字进去，但需要在一定的范围内，比如0-200，就需要在xml文件中配置maxVolume
            //同时，也可以配置灵敏度sensibility
            if (ratio > 1)
                db = 20 * Math.log10(ratio);

            if (mCallback != null)
                mCallback.onDecibelUpdata(db);
        }
    };

    private RecorderCallback mCallback;

    public MediaRecorderManager setRecorderCallback(RecorderCallback callback) {
        this.mCallback = callback;
        return this;
    }

    public static interface RecorderCallback {

        public void onDecibelUpdata(double db);
    }
}
