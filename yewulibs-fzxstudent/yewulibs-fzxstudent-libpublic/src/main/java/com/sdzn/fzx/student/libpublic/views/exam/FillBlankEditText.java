package com.sdzn.fzx.student.libpublic.views.exam;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.sdzn.fzx.student.libpublic.utils.HtmlTagHandler;
import com.sdzn.fzx.student.libpublic.R;
import com.sdzn.fzx.student.libutils.annotations.InputMethod;
import com.sdzn.fzx.student.libutils.util.InputTools;
import com.sdzn.fzx.student.vo.AnswerListBean;
import com.sdzn.fzx.student.vo.ClozeAnswerBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 填空题控件
 *
 * @author Reisen at 2018-08-22
 */
public class FillBlankEditText extends BaseHtmlEditText {
    private ArrayList<CharSequence> mTextBodyList = new ArrayList<>();
    private SparseArray<ClozeAnswerBean> mAnswerMap = new SparseArray<>();
    private static final int BLANK_CELL_COUNT = 6;
    private static final String BLANK_CELL_STR = "      ";
    private static final String DEFAULT_TAG = "<input>";
    private String html;
    private BlankTextChangeListener mListener;

    private AnswerListBean.AnswerDataBean mAnswerDataBean;

    public AnswerListBean.AnswerDataBean getAnswerDataBean() {
        return mAnswerDataBean;
    }

    public void setAnswerDataBean(AnswerListBean.AnswerDataBean answerDataBean) {
        mAnswerDataBean = answerDataBean;
    }

    private @InputMethod
    int input = InputMethod.KeyBoard;

    public FillBlankEditText(Context context) {
        super(context);
    }

    public FillBlankEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FillBlankEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public FillBlankEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setHtmlBody(String html) {
        getImageSourceAndInput(html);
        Spanned spanned = HtmlTagHandler.fromHtml(html, this, this);
        setTextBody(spanned, DEFAULT_TAG);
    }

    public void setHtmlBody(String html, SparseArray<ClozeAnswerBean> blankTextList) {
        getImageSourceAndInput(html);
        setTextBody(HtmlTagHandler.fromHtml(html, this, this), DEFAULT_TAG, blankTextList);
    }

    public String getHtml() {
        return html;
    }

    /**
     * 设置要展示的文本
     *
     * @param mask        带标记的文本
     * @param notMaskText 文本标记
     */
    public void setTextBody(CharSequence mask, String notMaskText) {
        setTextBody(mask, notMaskText, new SparseArray<ClozeAnswerBean>());
    }

    /**
     * 设置要展示的文本
     *
     * @param mask          带标记的文本
     * @param notMaskText   文本标记
     * @param blankTextList 填空内容
     */
    public void setTextBody(CharSequence mask, String notMaskText, SparseArray<ClozeAnswerBean> blankTextList) {
        ArrayList<CharSequence> arr = new ArrayList<>();
        ArrayList<CharSequence> list = new ArrayList<>();
        Matcher matcher = Pattern.compile(notMaskText).matcher(mask);
        int index = 0;
        int blankCount = 0;
        while (matcher.find()) {
            blankCount++;
            arr.add(mask.subSequence(index, matcher.start()));
            index = matcher.end();
        }
        arr.add(mask.subSequence(index, mask.length()));

        for (int i = 0; i < arr.size(); i++) {
            list.add(arr.get(i));
            if (i < blankCount) {
                list.add(new CharSequenceImp());
            }
        }

        //当填空位不足时补足填空位长度
        int count = arr.size() - blankTextList.size();
        if (count > 0) {
            for (int i = 1; i <= blankCount; i++) {
                if (blankTextList.get(i) == null) {
                    blankTextList.append(i, new ClozeAnswerBean(i, "", "", false,input));
                }
            }
        }

        //填空内容添加到作答map中
        for (int i = 0; i < blankTextList.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            ClozeAnswerBean str = blankTextList.valueAt(i);
            if (bean!=null){
                bean.setAnswer(str.getAnswer());
                bean.setInputMethod(str.getInputMethod());
            }

        }
        setTextList(list);

    }

    private void setTextList(ArrayList<CharSequence> list) {
        if (list != null) {
            mTextBodyList.clear();
            mTextBodyList.addAll(list);
        }

        buildText();
    }

    @Override
    protected void setInputImageList() {
        mInputImageList.clear();
        for (int i = 0; i < mAnswerMap.size(); i++) {
            ClozeAnswerBean bean = mAnswerMap.valueAt(i);
            if (bean == null) {
                continue;
            }
            if (InputTools.isKeyboard(bean.getInputMethod()) && !TextUtils.isEmpty(bean.getAnswer())) {
                mInputImageList.add(bean.getAnswer());
            }
        }
    }

    public void rebuildText() {
        buildText();
    }

    /**
     * 设置文本后将原始文本与填充内容整合显示
     */
    private void buildText() {
        setInputImageList();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int count = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {
                int length = builder.length();
                builder.append(BLANK_CELL_STR);
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                builder.append(bean.getValue());
                //此位置可以插入图片
                if (InputTools.isKeyboard(bean.getInputMethod()) || TextUtils.isEmpty(bean.getAnswer())) {
                    builder.append(bean.getAnswer() == null ? "" : bean.getAnswer());
                } else {
                    appendImg(builder, bean.getAnswer());
                }
                builder.append(BLANK_CELL_STR);
                builder.setSpan(new UnderlineSpan(), length, builder.length() - 1,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                count++;
            } else {//文本
                builder.append(sequence);
            }
        }
        setText(builder, BufferType.EDITABLE);
    }

    private void appendImg(SpannableStringBuilder text, String src) {
        Drawable d;

        d = getDrawable(src);

        if (d == null) {
            d = getResources().getDrawable(R.drawable.default_unknow_image);
//            d = Resources.getSystem().getDrawable(
//                    Resources.getSystem().getIdentifier("unknown_image", "drawable", "android"));
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        }else {
            Bitmap bmp = ((BitmapDrawable)d).getBitmap();
            int height = bmp.getHeight();
            int width = bmp.getWidth();
            if (height > 45) {
                float scale = 45f / height;
                Matrix matrix = new Matrix();
                matrix.postScale(scale,scale);
                bmp = Bitmap.createBitmap(bmp,0,0,width,height,matrix,true);
                d = new BitmapDrawable(bmp);
                width = (int) (width * scale);
                height = (int) (height * scale);
                d.setBounds(0,0,width,height);
            }
        }

        int len = text.length();
        text.append("\uFFFC");

        text.setSpan(new ImageSpan(d, src, ImageSpan.ALIGN_BASELINE), len, text.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 设置输入过滤器, 默认用自带的InputFilter
     */
    public void setInputFilter(InputFilter filter) {
        if (filter == null) {
            filter = new FillBlankInputFilter();
        }
        setFilters(new InputFilter[]{filter});
    }

    @SuppressWarnings("all")
    private void setInsertionDisabled() {
        try {
            Field editorField = TextView.class.getDeclaredField("mEditor");
            editorField.setAccessible(true);
            Object editorObject = editorField.get(this);

            Class editorClass = Class.forName("android.widget.Editor");
            Field mInsertionControllerEnabledField = editorClass.getDeclaredField("mInsertionControllerEnabled");
            mInsertionControllerEnabledField.setAccessible(true);
            mInsertionControllerEnabledField.set(editorObject, false);
        } catch (Exception ignored) {
            // ignore exception here
        }
    }

    //要判断触摸事件的action, 只有点击时才处理, 按下/滑动不处理事件
    private float touchDownX;
    private float touchDownY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            setInsertionDisabled();
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDownX = event.getX();
                touchDownY = event.getY();
                return true;
            case MotionEvent.ACTION_MOVE://x或y方向滑动超过20像素就不认为是点击事件
                return !(Math.abs(touchDownX - event.getX()) > 20) &&
                        !(Math.abs(touchDownY - event.getY()) > 20);
            case MotionEvent.ACTION_UP://抬起时x或y方向超过20像素就不认为是点击事件
                if (Math.abs(touchDownX - event.getX()) > 20 ||
                        Math.abs(touchDownY - event.getY()) > 20) {
                    return false;
                }
                break;//只有抬起时才处理事件
            default:
                return false;
        }
        //控件中文本所占空间
        int left = getPaddingLeft();
        int right = getWidth() - getPaddingRight();
        int top = getPaddingTop();
        int bottom = getHeight() - getPaddingBottom();
        //触摸位置
        float x = event.getX();
        float y = event.getY();

        //padding外, 清除光标
        if (x < left || x > right || y < top || y > bottom) {
            cleanFocusAndCloseKeyBoard();
            return false;
        }

        //相对文本的坐标
        x -= left;
        y -= top;

        int startCharIndex = 0;//空开始位置
        int endCharIndex = 0;//空结束位置
        int blankCount = 0;//第几个空

        int textIndex = getPreciseOffset(x, y);
        //找到空格位置
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {//空格位
                //空格位开始位点不动, 结束位点+12;
                ClozeAnswerBean bean = mAnswerMap.valueAt(blankCount);
                @InputMethod int inputMethod = bean.getInputMethod();
                String answer = bean.getAnswer();
                endCharIndex += BLANK_CELL_COUNT;//end+6
                endCharIndex += bean.getValue().length();//end+题号
                if (textIndex >= startCharIndex && textIndex <= endCharIndex) {//空格前半部分, 选中空格最后一位
                    selectionAndOpenKeyBoard(endCharIndex, bean);
                    return true;
                }
                startCharIndex += BLANK_CELL_COUNT;
                startCharIndex += bean.getValue().length();
                endCharIndex += InputTools.isKeyboard(inputMethod) ? answer.length() : (TextUtils.isEmpty(answer) ? 0 : 1);
                if (textIndex >= startCharIndex && textIndex <= endCharIndex) {//空格文本中, 直接点击对应位置
                    selectionAndOpenKeyBoard(textIndex, bean);
                    return true;
                }
                startCharIndex += InputTools.isKeyboard(inputMethod) ? answer.length() : (TextUtils.isEmpty(answer) ? 0 : 1);
                endCharIndex += BLANK_CELL_COUNT;
                if (textIndex >= startCharIndex && textIndex <= endCharIndex) {//空格后半部分, 选中空格首位
                    selectionAndOpenKeyBoard(startCharIndex, bean);
                    return true;
                }
                startCharIndex += BLANK_CELL_COUNT;
                blankCount++;
            } else {
                startCharIndex += sequence.length();
                endCharIndex += sequence.length();
            }
        }
        cleanFocusAndCloseKeyBoard();
        return false;
    }

    /**
     * 获取指定坐标是第几个字符
     */
    private int getPreciseOffset(float x, float y) {
        Layout layout = getLayout();
        if (layout != null) {
            int topVisibleLine = layout.getLineForVertical((int) y);
            int offset = layout.getOffsetForHorizontal(topVisibleLine, x);

            int offsetX = (int) layout.getPrimaryHorizontal(offset);

            if (offsetX > x) {
                return layout.getOffsetToLeftOf(offset);
            } else {
                return offset;
            }
        } else {
            return -1;
        }
    }

    /**
     * 清除焦点并关闭软键盘
     */
    private void cleanFocusAndCloseKeyBoard() {
        clearFocus();
        if (mListener != null) {
            mListener.closeInput(this);
        } else {
            Context context = getContext();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
    }

    /**
     * 移动焦点并开启软键盘
     */
    private void selectionAndOpenKeyBoard(int position, ClozeAnswerBean bean) {
        requestFocus();
        setSelection(position);
        if (mListener != null) {
            mListener.onBlankClick(this, bean, mAnswerDataBean);
        } else {
            Context context = getContext();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(this, InputMethodManager.SHOW_FORCED);
            }
        }

    }

    public void blankTextChange() {
        if (mListener != null) {
            mListener.onBlankTextChange(mAnswerMap, null);
        }
    }

    /**
     * 设置输入过滤器
     */
    private class FillBlankInputFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source instanceof SpannableStringBuilder) {
                return source;
            }
            //获取选中的开始和结束位置
            int selectionStart = getSelectionStart();
            int selectionEnd = getSelectionEnd();

            int count = 0;
            int textStart = 0;
            int textEnd = 0;
            for (CharSequence sequence : mTextBodyList) {
                if (sequence instanceof CharSequenceImp) {//空格位
                    //空格前半段
                    ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                    @InputMethod int inputMethod = bean.getInputMethod();
                    String text = bean.getAnswer();//当inputMethod != 1时, 此处为图片url
                    textEnd += BLANK_CELL_COUNT;
                    textEnd += bean.getValue().length();
                    boolean inputStart = selectionStart >= textStart && selectionStart <= textEnd;
                    boolean inputEnd = selectionEnd >= textStart && selectionEnd <= textEnd;
                    if (inputStart || inputEnd) {
                        if (end == 0 && dstart != dend) {//删除
                            return dest.toString().substring(dstart, dend);
                        } else {//输入
                            if (!InputTools.isKeyboard(inputMethod)) {//禁止插入内容
                                return null;
                            }
                            bean.setAnswer(source + text);
                            if (mListener != null) {
                                mListener.onBlankTextChange(mAnswerMap, bean);
                            }
                            return source;
                        }
                    }
                    textStart += BLANK_CELL_COUNT;
                    textStart += bean.getValue().length();
                    //中间内容
                    textEnd += InputTools.isKeyboard(inputMethod) ? text.length() : (TextUtils.isEmpty(text) ? 0 : 1);
                    inputStart = selectionStart >= textStart && selectionStart <= textEnd;
                    inputEnd = selectionEnd >= textStart && selectionEnd <= textEnd;
                    if (inputStart && inputEnd) {
                        if (end == 0 && dstart != dend) {//删除
                            if (!InputTools.isKeyboard(inputMethod)) {//禁止删除图片
                                return dest.toString().substring(dstart, dend);
                            }
                            //删除一段指定位置的文字
                            int i = textEnd - dstart;
                            i = text.length() - i;
                            int len = dend - dstart;
                            String str1 = text.substring(0, i);
                            String str2 = text.substring(i + len, text.length());
                            bean.setAnswer(str1 + str2);
                            if (mListener != null) {
                                mListener.onBlankTextChange(mAnswerMap, bean);
                            }
                            return source;
                        } else {//输入
                            if (!InputTools.isKeyboard(inputMethod)) {//禁止插入内容
                                return null;
                            }
                            //插入文字到指定位置
                            int len = textEnd - dstart;//距离空内容末尾距离
                            len = text.length() - len;
                            String str = text.substring(0, len) + source + text.substring(len);
                            bean.setAnswer(str);
                            if (mListener != null) {
                                mListener.onBlankTextChange(mAnswerMap, bean);
                            }
                            return source;
                        }
                    }
                    textStart += InputTools.isKeyboard(inputMethod) ? text.length() : (TextUtils.isEmpty(text) ? 0 : 1);
                    //空格后半段
                    textEnd += BLANK_CELL_COUNT;
                    inputStart = selectionStart > textStart && selectionStart <= textEnd;
                    inputEnd = selectionEnd > textStart && selectionEnd <= textEnd;
                    if (inputStart || inputEnd) {
                        if (end == 0 && dstart != dend) {//删除
                            if (!InputTools.isKeyboard(inputMethod)) {//禁止删除图片
                                return dest.toString().substring(dstart, dend);
                            }
                            //删除一段指定位置的文字
                            int i = textEnd - dstart;
                            i = text.length() - i;
                            int len = dend - dstart;
                            String str1 = text.substring(0, i);
                            String str2 = text.substring(i + len, text.length());
                            bean.setAnswer(str1 + str2);
                            if (mListener != null) {
                                mListener.onBlankTextChange(mAnswerMap, bean);
                            }
                            return source;
                        } else {//输入
                            if (!InputTools.isKeyboard(inputMethod)) {//禁止插入内容
                                return null;
                            }
                            //插入文字到指定位置
                            int len = textEnd - dstart;//距离空内容末尾距离
                            len = text.length() - len;
                            String str = text.substring(0, len) + source + text.substring(len);
                            bean.setAnswer(str);
                            if (mListener != null) {
                                mListener.onBlankTextChange(mAnswerMap, bean);
                            }
                            return source;
                        }
                    }
                    textStart += BLANK_CELL_COUNT;
                    count++;
                } else {//非编辑位
                    textEnd += sequence.length();
                    if ((selectionStart >= textStart && selectionStart <= textEnd)
                            || (selectionEnd >= textStart && selectionEnd <= textEnd)) {
                        cleanFocusAndCloseKeyBoard();
                        if (end == 0 && dstart != dend) {//删除
                            return dest.toString().substring(dstart, dend);
                        } else {//输入
                            return "";
                        }
                    }
                    textStart += sequence.length();
                }
            }
            return source;
        }
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        //监听光标变化, 当光标进入/跨编辑区时失去焦点,进入空位时阻止进入(重新设定光标
        //禁止全选
        int length = getText().toString().length();
        if (selStart == 0 && selEnd != 0 && selEnd == length) {
            cleanFocusAndCloseKeyBoard();
            return;
        }
        if (mTextBodyList == null || mAnswerMap == null) {
            cleanFocusAndCloseKeyBoard();
            return;
        }

        int count = 0;
        int textStart = 0;
        int textEnd = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {//空格位
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                //空格前半段
                textEnd += BLANK_CELL_COUNT;
                textEnd += bean.getValue().length();
                boolean start = selStart >= textStart && selStart <= textEnd;
                boolean end = selEnd >= textStart && selEnd <= textEnd;
                if (start && end) {
                    setSelection(selEnd);
                    return;
                } else if (start) {
                    setSelection(selEnd);
                    return;
                } else if (end) {
                    setSelection(selEnd);
                    return;
                }
                textStart += BLANK_CELL_COUNT;
                textStart += bean.getValue().length();
                //中间内容
                textEnd += InputTools.isKeyboard(bean.getInputMethod()) ? bean.getAnswer().length() : 1;
//                textEnd += mBlankTextList.get(count).length();
                start = selStart >= textStart && selStart <= textEnd;
                end = selEnd >= textStart && selEnd <= textEnd;
                if (start && end) {
                    setSelection(selEnd);
                    return;
                }
//                textStart += mBlankTextList.get(count).length();
                textStart += InputTools.isKeyboard(bean.getInputMethod()) ? bean.getAnswer().length() : 1;
                //空格后半段
                textEnd += BLANK_CELL_COUNT;
                start = selStart >= textStart && selStart <= textEnd;
                end = selEnd >= textStart && selEnd <= textEnd;
                if (start && end) {
                    setSelection(textStart);
                    return;
                } else if (start) {
                    setSelection(textStart);
                    return;
                } else if (end) {
                    setSelection(textStart);
                    return;
                }
                textStart += BLANK_CELL_COUNT;
                count++;
            } else {//非编辑区
                textEnd += sequence.length();
                if ((selStart >= textStart && selStart <= textEnd)
                        || (selEnd >= textStart && selEnd <= textEnd)) {
                    cleanFocusAndCloseKeyBoard();
                    return;
                }
                textStart += sequence.length();
            }
        }
        super.onSelectionChanged(selStart, selEnd);
    }

    /**
     * 选中某一个空
     */
    public void selectBlank(int index) {
        int count = 0;
        int textIndex = 0;
        for (CharSequence sequence : mTextBodyList) {
            if (sequence instanceof CharSequenceImp) {
                textIndex += BLANK_CELL_COUNT;
                if (index == count) {
                    setSelection(textIndex);
                    return;
                }
                ClozeAnswerBean bean = mAnswerMap.valueAt(count);
                textIndex += InputTools.isKeyboard(bean.getInputMethod()) ? bean.getAnswer().length() : 1;
                textIndex += BLANK_CELL_COUNT;
            } else {
                textIndex += sequence.length();
            }
        }
    }

    @Override
    protected void glideLoadImageSuccess() {
        SparseArray<ClozeAnswerBean> list = new SparseArray<>();
        for (int i = 0; i < mAnswerMap.size(); i++) {
            list.put(mAnswerMap.keyAt(i), mAnswerMap.valueAt(i));
        }
        setHtmlBody(html, list);
    }

    @Override
    protected void handleInput(boolean opening, String tag, Editable output, Attributes xmlReader) {
        if (!opening) {
            output.append(DEFAULT_TAG);
        }
    }

    private void getImageSourceAndInput(String source) {
        html = source;
        mMap.clear();
        mAnswerMap.clear();
        Document doc = Jsoup.parse(source);
        Elements img = doc.getElementsByTag("img");
        for (Element element : img) {
            String width = element.attr("width");
            String height = element.attr("height");
            String src = element.attr("src");
            if (TextUtils.isEmpty(width)) {
                width = "0";
            }
            if (TextUtils.isEmpty(height)) {
                height = "0";
            }
            mMap.put(src, new Point(Integer.valueOf(width), Integer.valueOf(height)));
        }
        //解析input标签
        Elements input = doc.getElementsByTag("input");
        for (Element element : input) {
            int index = Integer.valueOf(element.attr("index").trim());
            String value = element.attr("value");
            mAnswerMap.put(index, new ClozeAnswerBean(index, value));
        }
    }


    public @InputMethod
    int getInputMethod() {
        return input;
    }

    public void setInputMethod(@InputMethod int input) {
        this.input = input;
    }

    public void setBlankTextChangeListener(BlankTextChangeListener listener) {
        mListener = listener;
    }

    public SparseArray<ClozeAnswerBean> getAnswerMap() {
        return mAnswerMap;
    }

    public interface BlankTextChangeListener {
        /**
         * 文本改变时触发接口调用
         *
         * @param blankTextList 结果集
         * @param bean          修改的数据bean
         */
        void onBlankTextChange(SparseArray<ClozeAnswerBean> blankTextList, @Nullable ClozeAnswerBean bean);

        /**
         * 开启软键盘
         *
         * @param et   本填空题对象
         * @param bean 填空题bean
         */
        void onBlankClick(FillBlankEditText et, ClozeAnswerBean bean, AnswerListBean.AnswerDataBean answerDataBean);

        /**
         * 主动关闭输入框
         */
        void closeInput(FillBlankEditText et);
    }
}
