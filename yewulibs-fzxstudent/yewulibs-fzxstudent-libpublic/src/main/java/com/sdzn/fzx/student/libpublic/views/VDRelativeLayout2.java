package com.sdzn.fzx.student.libpublic.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Point;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.customview.widget.ViewDragHelper;

import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.sdzn.fzx.student.libpublic.R;

import java.util.HashSet;


/**
 * @author Reisen at 2017-11-22
 */

public class VDRelativeLayout2 extends RelativeLayout {
    private ViewDragHelper mHelper;
    private boolean isDrag = true;//子控件是否可拖动 default: true
    private boolean isAdsorb;//是否靠边吸附 default: false
    private int oldOrientation = Configuration.ORIENTATION_PORTRAIT;
    private HashSet<Integer> mSet = new HashSet<>(4);
    private SparseArray<Point> mViewsPoint = new SparseArray<>(4);

    public VDRelativeLayout2(Context context) {
        this(context, null);
    }

    public VDRelativeLayout2(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VDRelativeLayout2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(context, attrs);
        init();
    }

    @TargetApi(21)
    public VDRelativeLayout2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttr(context,attrs);
        init();
    }

    private void initAttr(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VDRelativeLayout2);
            isDrag = typedArray.getBoolean(R.styleable.VDRelativeLayout2_isDrag, true);
            isAdsorb = typedArray.getBoolean(R.styleable.VDRelativeLayout2_isAdsorb, false);
            typedArray.recycle();
        }
    }

    private void init() {
        if (!isDrag) {
            return;
        }
        setClickable(true);
        initViewDragHelper();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isDrag ? mHelper.shouldInterceptTouchEvent(ev) : super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isDrag) {
            mHelper.processTouchEvent(event);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void computeScroll() {
        if (isDrag && mHelper.continueSettling(true)) {
            invalidate();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        int orientation = this.getResources().getConfiguration().orientation;
        boolean screenRotate = false;//屏幕是否旋转
        if (this.getResources().getConfiguration().orientation != oldOrientation) {
            oldOrientation = orientation;
            //屏幕旋转时, 直接移动到目标位置
            screenRotate = true;
        }
        a:
        for (Integer id : mSet) {
            int count = getChildCount();
            for (int i = 0; i < count; i++) {
                View view = getChildAt(i);
                if (view.getId() != id) {
                    continue;
                }
                Point point = mViewsPoint.get(id);
                if (point == null) {
                    continue a;
                }
                if (screenRotate) {
                    Point adsorb = adsorb(point, view);
                    if (adsorb != null) {
                        point.set(adsorb.x, adsorb.y);
                    }
                }
                view.layout(point.x, point.y, point.x + view.getMeasuredWidth(), point.y + view.getMeasuredHeight());
            }
        }
    }

    private void initViewDragHelper() {
        mHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                for (Integer childId : mSet) {
                    if (child.getId() == childId) {
                        child.bringToFront();
                        return true;
                    }
                }
                return false;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                if (left + child.getMeasuredWidth() >= getMeasuredWidth()) {
                    return getMeasuredWidth() - child.getMeasuredWidth();
                }
                return Math.max(left, 0);
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                if (child.getMeasuredHeight() + top > getMeasuredHeight()) {
                    return getMeasuredHeight() - child.getMeasuredHeight();
                }
                return Math.max(top, 0);
            }

            @Override
            public int getViewHorizontalDragRange(View child) {
                for (Integer childId : mSet) {
                    if (child.getId() == childId) {
                        return getMeasuredWidth() - child.getMeasuredWidth();
                    }
                }
                return 0;
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                for (Integer childId : mSet) {
                    if (child.getId() == childId) {
                        return getMeasuredHeight() - child.getMeasuredHeight();
                    }
                }
                return 0;
            }

            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                resetPoint(releasedChild.getId(), releasedChild.getLeft(), releasedChild.getTop());
                //吸附边缘
                if (!isAdsorb) {
                    return;
                }
                Point end = adsorb(null, releasedChild);
                if (end == null) {
                    return;
                }
                mHelper.settleCapturedViewAt(end.x, end.y);
                invalidate();
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                resetPoint(changedView.getId(), left, top);
            }

            private void resetPoint(int viewId, int x, int y) {
                Point point = mViewsPoint.get(viewId);
                if (point == null) {
                    mViewsPoint.put(viewId, new Point(x, y));
                } else {
                    point.set(x, y);
                }
            }

        });
        mHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_ALL);
    }

    /**
     * 吸附至屏幕位置
     *
     * @param point         记录下的控件最后位置
     * @param releasedChild 被测量的控件
     * @return 要吸附到的目标位置, null表示不吸附
     */
    @Nullable
    private Point adsorb(Point point, View releasedChild) {
        int width = getMeasuredWidth() - releasedChild.getMeasuredWidth();
        int height = getMeasuredHeight() - releasedChild.getMeasuredHeight();
        //吸附条件判断 : 本控件去掉子控件宽高后的边缘 1/4处吸附, 否则不吸附
        boolean toTop, toBottom, toLeft, toRight;
        if (point == null) {
            toTop = releasedChild.getTop() < height >> 2;
            toBottom = releasedChild.getTop() > height - (height >> 2);
            toLeft = releasedChild.getLeft() < width >> 2;
            toRight = releasedChild.getLeft() > width - (width >> 2);
        } else {
            toTop = point.y < height >> 2;
            toLeft = point.x < width >> 2;
            toRight = point.x > width - (width >> 2);
            toBottom = point.y > height - (height >> 2);
        }
        if (!toTop && !toBottom && !toLeft && !toRight) {//中间不吸附
            return null;
        }
        if (toTop && toLeft) {//左上
            return new Point();
        } else if (toTop && toRight) {//右上
            return new Point(width, 0);
        } else if (toBottom && toLeft) {//左下
            return new Point(0, height);
        } else if (toBottom && toRight) {//右下
            return new Point(width, height);
        } else if (toTop) {//上中
            return new Point(releasedChild.getLeft(), 0);
        } else if (toBottom) {//右中
            return new Point(releasedChild.getLeft(), height);
        } else if (toLeft) {//左中
            return new Point(0, releasedChild.getTop());
        } else {//下中
            return new Point(width, releasedChild.getTop());
        }
    }

    /**
     * 子控件是否可拖动
     *
     * @param drag 默认是可拖动的
     */
    public void setDrag(boolean drag) {
        if (drag && mHelper == null) {
            initViewDragHelper();
        }
        isDrag = drag;
    }

    public boolean isDrag() {
        return isDrag;
    }

    /**
     * 拖动控件是否贴边吸附
     *
     * @param adsorb 默认false子控件不吸附边缘
     */
    public void setAdsorb(boolean adsorb) {
        isAdsorb = adsorb;
    }

    public boolean isAdsorb() {
        return isAdsorb;
    }

    public void addDragView(@IdRes int viewId) {
        mSet.add(viewId);
    }

    public boolean removeDragView(@IdRes int viewId) {
        return mSet.remove(viewId);
    }

    public HashSet<Integer> getDragViews() {
        return mSet;
    }

    public void setDragViews(HashSet<Integer> set) {
        if (set == null) {
            return;
        }
        mSet = set;
    }

    public SparseArray<Point> getViewsPoint() {
        return mViewsPoint;
    }

    public void setViewsPoint(SparseArray<Point> viewsPoint) {
        mViewsPoint = viewsPoint;
    }
}
