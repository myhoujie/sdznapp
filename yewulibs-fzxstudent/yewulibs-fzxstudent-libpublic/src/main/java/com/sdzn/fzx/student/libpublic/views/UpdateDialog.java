package com.sdzn.fzx.student.libpublic.views;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.sdzn.fzx.student.libpublic.R;


/**
 * 升级dialog
 *
 * @author Reisen
 */
public class UpdateDialog extends CustomDialog {

    public UpdateDialog(Context context) {
        super(context);
    }

    public UpdateDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String message;
        private OnClickListener positiveClickListener;
        private OnClickListener negativeClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        /**
         * 设置显示内容
         */
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * 设置显示内容的资源文件
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * 设置确定按钮
         */
        public Builder setPositive(OnClickListener listener) {
            this.positiveClickListener = listener;
            return this;
        }


        /**
         * 设置取消按钮
         */
        public Builder setNegative(OnClickListener listener) {
            this.negativeClickListener = listener;
            return this;
        }

        /**
         * 创建Dialog
         *
         * @return
         */
        public UpdateDialog create() {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final UpdateDialog dialog = new UpdateDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_update, null);
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            ((TextView) layout.findViewById(R.id.tvPositive))
                    .setText("立即更新");
            if (positiveClickListener != null) {
                (layout.findViewById(R.id.tvPositive))
                        .setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                positiveClickListener.onClick(dialog,
                                        DialogInterface.BUTTON_POSITIVE);
                            }
                        });
            }
            ((TextView) layout.findViewById(R.id.tvNegative))
                    .setText("取消");
            if (negativeClickListener != null) {
                (layout.findViewById(R.id.tvNegative))
                        .setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                negativeClickListener.onClick(dialog,
                                        DialogInterface.BUTTON_NEGATIVE);
                            }
                        });
            }
            TextView tv = layout.findViewById(R.id.tvMessage);
            tv.setText(message == null ? "" : message);
            dialog.setContentView(layout);
            return dialog;
        }
    }
}
