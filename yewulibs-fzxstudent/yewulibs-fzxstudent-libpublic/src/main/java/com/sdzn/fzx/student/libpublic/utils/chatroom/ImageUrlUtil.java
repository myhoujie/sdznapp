package com.sdzn.fzx.student.libpublic.utils.chatroom;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2019/8/13.
 */

public class ImageUrlUtil {
    public static String getImagetUrl(String s){
        //解析数据
        JSONObject jsonObject = null;
        String path="";
        try {
            jsonObject = new JSONObject(s);
            path = jsonObject.getString("url");
            return path;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  path;
    }
}
