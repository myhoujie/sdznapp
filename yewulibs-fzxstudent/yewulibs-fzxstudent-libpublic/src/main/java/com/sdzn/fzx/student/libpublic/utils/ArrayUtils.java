package com.sdzn.fzx.student.libpublic.utils;

import java.util.List;

/**
 * ArrayUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class ArrayUtils {

    public static int[] toIntArray(List<Integer> oldArray) {

        int[] newArray = new int[oldArray.size()];
        for (int i = 0; i < oldArray.size(); i++) {
            newArray[i] = oldArray.get(i);
        }
        return newArray;
    }

}
