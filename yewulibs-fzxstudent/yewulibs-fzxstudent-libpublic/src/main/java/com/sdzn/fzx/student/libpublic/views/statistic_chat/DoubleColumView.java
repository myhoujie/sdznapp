package com.sdzn.fzx.student.libpublic.views.statistic_chat;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.sdzn.fzx.student.libutils.util.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * DoubleColumView〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class DoubleColumView extends ViewGroup {
    private static final String sss = "分值";

    private String[] flagValue = {"100", "80", "60", "40", "20", "0"};

    private final static int COLOR_BASE_LINE = 0xFF323C47;
    private final static int COLOR_BASE_TEXT = 0xFF323C47;
    private final static int COLOR_SELECTED_BG = 0xFFEDF5FF;
    private final static int COLOR_CHAT_LINE = 0xFFEDEDED;
    private final static int COLOR_VALUE_TXT = 0xFF808FA3;
    private final static int COLOR_POP_WINDOW_BG = 0x99000000;
    private final static int COLOR_POP_WINDOW_TEXT = 0xFFFFFFFF;
    private final static int COLOR_POP_WINDOW_TEXT_1 = 0xFFF0D444;
    private final static int COLOR_POP_WINDOW_TEXT_2 = 0xFF4AAB74;


    private final static int COLOR_COLUM_TOP = 0xFF4AAB74;
    private final static int COLOR_COLUM_BOTTOM = 0xFFF0D444;

    private List<ItemInfoVo> mDatas = new ArrayList<>();


    private GestureDetector mGestureDetector;

    StaticLayout m;

    /**
     * 柱状图的宽度
     */
    private float defColumWidth = 48;

    /**
     * 柱状图选中时候的宽度
     */
    private float defColumBgWidth = 88;

    /**
     * 每条数据所占用的最小宽度
     */
    private float defItemMinWidth = 180;

    /**
     * 每条数据实际占用的宽度
     */
    private float itemWidth;

    /**
     * 最大的滑动距离
     */
    private float mMaxHorizontalScrollDis;

    /**
     * 当前选中的item
     */
    private int curShwoItem;

    /**
     * View 的宽度,高度
     */
    private float viewWidth, viewHeight;

    /**
     * 除去周边文字的图形 区域
     */
    private RectF chatRectF = new RectF();

    private Paint baseLinePaint, selectBgPaint, chatLinePaint, columPaint, popwindowBgPaint;

    private TextPaint baseTxtPaint, valuePaint, infoPaint;

    public DoubleColumView(Context context) {
        this(context, null);
    }

    public DoubleColumView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DoubleColumView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }

    private void init() {
        mGestureDetector = new GestureDetector(getContext(), new OnGestureListener());
        initPaint();
    }

    public void setData(List<ItemInfoVo> data) {
        this.mDatas = data;
        onDataChanged();
        invalidate();
    }

    private void initPaint() {
        baseLinePaint = new Paint();
        baseLinePaint.setAntiAlias(true);
        baseLinePaint.setStyle(Paint.Style.FILL);
        baseLinePaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        baseLinePaint.setColor(COLOR_BASE_LINE);

        selectBgPaint = new Paint();
        selectBgPaint.setAntiAlias(true);
        selectBgPaint.setStyle(Paint.Style.FILL);
        selectBgPaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        selectBgPaint.setColor(COLOR_SELECTED_BG);

        chatLinePaint = new Paint();
        chatLinePaint.setAntiAlias(true);
        chatLinePaint.setStyle(Paint.Style.FILL);
        chatLinePaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        chatLinePaint.setColor(COLOR_CHAT_LINE);


        popwindowBgPaint = new Paint();
        popwindowBgPaint.setAntiAlias(true);
        popwindowBgPaint.setStyle(Paint.Style.FILL);
        popwindowBgPaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        popwindowBgPaint.setColor(COLOR_POP_WINDOW_BG);

        baseTxtPaint = new TextPaint();
        baseTxtPaint.setAntiAlias(true);
        baseTxtPaint.setStyle(Paint.Style.FILL);
        baseTxtPaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        baseTxtPaint.setTextSize(UiUtils.sp2px(getContext(), 12));
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        baseTxtPaint.setColor(COLOR_BASE_TEXT);

        valuePaint = new TextPaint();
        valuePaint.setAntiAlias(true);
        valuePaint.setStyle(Paint.Style.FILL);
        valuePaint.setTextSize(UiUtils.sp2px(getContext(), 12));
        valuePaint.setTextAlign(Paint.Align.CENTER);
        valuePaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        valuePaint.setColor(COLOR_VALUE_TXT);

        columPaint = new Paint();
        columPaint.setAntiAlias(true);
        columPaint.setStyle(Paint.Style.FILL);
        columPaint.setTextSize(UiUtils.sp2px(getContext(), 12));


        infoPaint = new TextPaint();
        infoPaint.setAntiAlias(true);
        infoPaint.setStyle(Paint.Style.FILL);
        infoPaint.setTextSize(UiUtils.sp2px(getContext(), 12));
        infoPaint.setTextAlign(Paint.Align.LEFT);
        infoPaint.setStrokeWidth(UiUtils.dp2px(getContext(), 1));
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        viewWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        viewHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();

        chatRectF.left = 112;
        chatRectF.top = 80;
        chatRectF.right = viewWidth - 20;
        chatRectF.bottom = viewHeight - 92;
        onDataChanged();
    }

    private void onDataChanged() {
        final float defTotalWidth = mDatas.size() * defItemMinWidth;

        if (defTotalWidth > chatRectF.width()) {
            itemWidth = defItemMinWidth;
            mMaxHorizontalScrollDis = defTotalWidth - chatRectF.width();
        } else {
            itemWidth = viewWidth / Math.max(1, mDatas.size());
            mMaxHorizontalScrollDis = 0;
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBaseLine(canvas);
        drawChatLine(canvas);
        drawFlagValue(canvas);
        drawColum(canvas);
        drawValue(canvas);
        drawPopWindow(canvas);
    }

    private void drawBaseLine(Canvas canvas) {
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.left + getScrollX(), chatRectF.bottom, baseLinePaint);
        canvas.drawLine(chatRectF.left + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX(), chatRectF.bottom, baseLinePaint);
    }

    private void drawChatLine(Canvas c) {
        final float chatH = chatRectF.bottom - chatRectF.top;
        final float itemHeight = chatH / 5;
        final float baseHeight = chatRectF.top;
        for (int i = 0; i < 5; i++) {
            c.drawLine(chatRectF.left + getScrollX(), baseHeight + itemHeight * i, chatRectF.right + getScrollX(), baseHeight + itemHeight * i, chatLinePaint);
        }
    }

    private void drawFlagValue(Canvas canvas) {
        final float h = (chatRectF.bottom - chatRectF.top) / 5;
        baseTxtPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(sss, 0, sss.length(), getScrollX() + chatRectF.left - 10, 40, baseTxtPaint);
        for (int i = 0; i < flagValue.length; i++) {
            canvas.drawText(flagValue[i], 0, flagValue[i].length(), getScrollX() + chatRectF.left - 20, chatRectF.top + h * i, baseTxtPaint);
        }

        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left + getScrollX(), chatRectF.bottom, chatRectF.right + getScrollX(), viewHeight));

        baseTxtPaint.setTextAlign(Paint.Align.CENTER);
        for (int i = 0; i < mDatas.size(); i++) {
            final String name = mDatas.get(i).getName();
            canvas.drawText(name, 0, name.length(), chatRectF.left + itemWidth * i + itemWidth / 2, chatRectF.bottom + 40, baseTxtPaint);

        }
        canvas.restore();
    }

    private void drawColum(Canvas canvas) {
        final float columHeight = chatRectF.bottom - chatRectF.top;
        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom));

        for (int i = 0; i < mDatas.size(); i++) {
            RectF tempRectF = new RectF();
            RectF topRectF = new RectF();
            RectF bottomF = new RectF();

            if (i == curShwoItem) {
                tempRectF.left = chatRectF.left + itemWidth * i + itemWidth / 2 - defColumBgWidth / 2;
                tempRectF.top = chatRectF.top;
                tempRectF.right = chatRectF.left + itemWidth * i + itemWidth / 2 + defColumBgWidth / 2;
                tempRectF.bottom = chatRectF.bottom;
                canvas.drawRect(tempRectF, selectBgPaint);
                resetPoint(tempRectF.right, -1);
            }

            tempRectF.left = chatRectF.left + itemWidth * i + itemWidth / 2 - defColumWidth / 2;
            tempRectF.top = chatRectF.top + columHeight * (1 - mDatas.get(i).getValue() / 100f);
            tempRectF.right = chatRectF.left + itemWidth * i + itemWidth / 2 + defColumWidth / 2;
            tempRectF.bottom = chatRectF.bottom;

            topRectF.left = tempRectF.left;
            topRectF.right = tempRectF.right;
            topRectF.top = tempRectF.top;
            topRectF.bottom = tempRectF.top + tempRectF.height() * (1 - mDatas.get(i).getTopValue() / (float) mDatas.get(i).getValue());

            bottomF.left = tempRectF.left;
            bottomF.right = tempRectF.right;
            bottomF.top = topRectF.bottom;
            bottomF.bottom = tempRectF.bottom;

            if (i == curShwoItem) {
                resetPoint(-1, tempRectF.top);
            }
            columPaint.setColor(COLOR_COLUM_TOP);
            canvas.drawRect(topRectF, columPaint);

            columPaint.setColor(COLOR_COLUM_BOTTOM);
            canvas.drawRect(bottomF, columPaint);


        }

        canvas.restore();
    }

    private Point p = new Point();

    private void resetPoint(float x, float y) {
        if (x > 0)
            p.x = (int) x;
        if (y > 0)
            p.y = (int) y;

    }


    private void drawValue(Canvas canvas) {
        final float columHeight = chatRectF.bottom - chatRectF.top;
        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left + getScrollX(), getPaddingTop(), chatRectF.right + getScrollX(), chatRectF.bottom));

        for (int i = 0; i < mDatas.size(); i++) {
            final ItemInfoVo itemInfoVo = mDatas.get(i);
            final String value = itemInfoVo.getValue() + "分";
            final float valueY = chatRectF.top + columHeight * (1 - mDatas.get(i).getValue() / 100f) - 20;
            canvas.drawText(value, chatRectF.left + itemWidth * i + itemWidth / 2, valueY, valuePaint);
        }
        canvas.restore();
    }

    private void drawPopWindow(Canvas canvas) {

        if (mDatas.size() <= curShwoItem) {
            return;
        }
        final ItemInfoVo itemInfoVo = mDatas.get(curShwoItem);

        String totalString = "综合得分" + itemInfoVo.getValue() + "分";
        String taskString = "任务得分" + itemInfoVo.getBottomValue() + "分";
        String classString = "课堂得分" + itemInfoVo.getTopValue() + "分";

        final float textHeight = UiUtils.measureTextHeight(infoPaint);
        final float textWidth = UiUtils.measureTextWidth(infoPaint, totalString);

        RectF tempRect = new RectF();

        float padding = 20;
        float paddingColum = 20;

        final int popWidth = (int) (textWidth + padding * 2);
        final int popHeight = (int) (textHeight * 3 + paddingColum * 4);

        if (popWidth + p.x - getScrollX() > chatRectF.right) {
            tempRect.right = p.x - defColumBgWidth;
            tempRect.left = tempRect.right - popWidth;
        } else {
            tempRect.left = p.x;
            tempRect.right = p.x + popWidth;
        }

        if (p.y + padding + popHeight / 2 > chatRectF.bottom) {
            tempRect.bottom = chatRectF.bottom;
            tempRect.top = tempRect.bottom - popHeight;
        } else if (p.y - padding - popHeight / 2 < chatRectF.top) {
            tempRect.top = chatRectF.top;
            tempRect.bottom = chatRectF.top + popHeight;
        } else {
            tempRect.top = p.y - popHeight / 2;
            tempRect.bottom = p.y + popHeight / 2;
        }


        canvas.save();
        canvas.clipRect(new RectF(chatRectF.left + getScrollX(), chatRectF.top, chatRectF.right + getScrollX(), chatRectF.bottom));
        canvas.drawRoundRect(tempRect, 8, 8, popwindowBgPaint);
        canvas.restore();

        canvas.save();
        canvas.translate(tempRect.left + paddingColum, tempRect.top + padding + textHeight);
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT);
        canvas.drawText(totalString, 0, 0, infoPaint);
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT_1);
        canvas.drawText(taskString, 0, textHeight + padding, infoPaint);
        infoPaint.setColor(COLOR_POP_WINDOW_TEXT_2);
        canvas.drawText(classString, 0, (textHeight + padding) * 2, infoPaint);
        canvas.restore();
    }

    /**********************
     * 手势区域分割线
     ********************/

    private void setShowItem(int item) {
        curShwoItem = item;
        if (itemClickListener != null) {
            if (item < mDatas.size()) {
                itemClickListener.onItemChanged(mDatas.get(item));
            }
        }
        invalidate();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            float x = e.getX() + getScrollX();
            int item = (int) ((x - chatRectF.left) / itemWidth);
            if (item < mDatas.size())
                setShowItem(item);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            if (Math.abs(distanceX) < Math.abs(distanceY)) {
                return false;
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);//屏蔽父控件拦截onTouch事件
            }
            float dx = distanceX;
            int x = getScrollX();

            if (x + dx < 0) {
                dx = 0 - x;
            }
            if (mMaxHorizontalScrollDis < 0) {
                dx = 0;
            } else if (x + dx > mMaxHorizontalScrollDis) {
                dx = mMaxHorizontalScrollDis - x;
            }

            scrollBy((int) dx, 0);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float vX, float vY) {

            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

    }

    public static class ItemInfoVo {
        private String name;
        private int value;
        private String id;
        private int topValue;
        private int bottomValue;

        public int getTopValue() {
            return topValue;
        }

        public void setTopValue(int topValue) {
            this.topValue = topValue;
        }

        public int getBottomValue() {
            return bottomValue;
        }

        public void setBottomValue(int bottomValue) {
            this.bottomValue = bottomValue;
        }

        public ItemInfoVo() {
        }

        public ItemInfoVo(String id, String name, int value) {
            this.name = name;
            this.value = value;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    private ItemChangedListener itemClickListener;

    public void setItemClickListener(ItemChangedListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemChangedListener {
        void onItemChanged(ItemInfoVo pos);
    }


}
