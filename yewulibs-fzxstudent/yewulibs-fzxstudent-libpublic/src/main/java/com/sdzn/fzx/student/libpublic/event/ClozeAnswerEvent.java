package com.sdzn.fzx.student.libpublic.event;

/**
 * @author Reisen at 2018-11-26
 */
public class ClozeAnswerEvent {
    public String examId;
    public int examSeq;
    public String optionList;

    public ClozeAnswerEvent(String examId, int examSeq, String optionList) {
        this.examId = examId;
        this.examSeq = examSeq;
        this.optionList = optionList;
    }
}
