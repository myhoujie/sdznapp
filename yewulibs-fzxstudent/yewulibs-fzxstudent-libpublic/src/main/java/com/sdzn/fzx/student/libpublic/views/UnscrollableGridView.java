package com.sdzn.fzx.student.libpublic.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;


/**
 * 自定义图标说明
 *
 * @author wangchunxiao
 * @date 2018/1/9
 */
public class UnscrollableGridView extends GridView {

    public UnscrollableGridView(Context context) {
        super(context);
    }

    public UnscrollableGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnscrollableGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}