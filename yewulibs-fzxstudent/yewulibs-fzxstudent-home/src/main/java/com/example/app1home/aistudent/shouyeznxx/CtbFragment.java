package com.example.app1home.aistudent.shouyeznxx;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1home.R;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.NewBaseFragment;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

/**
 * 错题本
 *
 * @author houjie
 * @date 2020/09/16
 */
public class CtbFragment extends NewBaseFragment implements View.OnClickListener, CheckverionViews {
    //    private XRecyclerView recyclerViewAiznxuexi;//底部显示
    CheckverionFzxPresenter checkverionFzxPresenter;
    private RelativeLayout rl_ct, rl_ct1, rl_ct2, rl_ct3, rl_ct4, rl_ct5, rl_ct6, rl_ct7, rl_ct8, rl_ct9;

//    private Znxuexidapter znxuexidapter;
//    private List<OneKtxx> mDataTablayout;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ctb;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
//        recyclerViewAiznxuexi = rootView.findViewById(R.id.recycler_view_aiznxuexi);
        rl_ct = rootView.findViewById(R.id.rl_ct);
        rl_ct1 = rootView.findViewById(R.id.rl_ct1);
        rl_ct2 = rootView.findViewById(R.id.rl_ct2);
        rl_ct3 = rootView.findViewById(R.id.rl_ct3);
        rl_ct4 = rootView.findViewById(R.id.rl_ct4);
        rl_ct5 = rootView.findViewById(R.id.rl_ct5);
        rl_ct6 = rootView.findViewById(R.id.rl_ct6);
        rl_ct7 = rootView.findViewById(R.id.rl_ct7);
        rl_ct8 = rootView.findViewById(R.id.rl_ct8);
        rl_ct9 = rootView.findViewById(R.id.rl_ct9);
        rl_ct.setOnClickListener(this);
        rl_ct1.setOnClickListener(this);
        rl_ct2.setOnClickListener(this);
        rl_ct3.setOnClickListener(this);
        rl_ct4.setOnClickListener(this);
        rl_ct5.setOnClickListener(this);
        rl_ct6.setOnClickListener(this);
        rl_ct7.setOnClickListener(this);
        rl_ct8.setOnClickListener(this);
        rl_ct9.setOnClickListener(this);
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        checkverionFzxPresenter.checkVerion("3", "0");
    }


    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        checkverionFzxPresenter.checkVerion("3", "0");
    }

    @Override
    protected void donetwork() {
        super.donetwork();
//        onclick();

    }

    @Override
    public void loadData() {
        super.loadData();
//        donetworkketangxuexi();
    }

    private void donetworkketangxuexi() {
//        HCategoryKtxxBean hCategoryKtxxBean = new HCategoryKtxxBean();
//        List<HKtxxBean> mDataTablayout1 = new ArrayList<>();
//        mDataTablayout1.add(new HKtxxBean("1", "1", "今日错题", "128", R.drawable.new_icon_quanbu));
//        mDataTablayout1.add(new HKtxxBean("2", "2", "语文", "128", R.drawable.new_icon_yuwen));
//        mDataTablayout1.add(new HKtxxBean("3", "3", "数学", "128", R.drawable.new_icon_shuxue));
//        mDataTablayout1.add(new HKtxxBean("4", "4", "英语", "128", R.drawable.new_icon_yingyu));
//        mDataTablayout1.add(new HKtxxBean("5", "5", "历史", "128", R.drawable.new_icon_lishi));
//        mDataTablayout1.add(new HKtxxBean("6", "6", "地理", "128", R.drawable.new_icon_dili));
//        mDataTablayout1.add(new HKtxxBean("7", "7", "生物", "128", R.drawable.new_icon_shengwu));
//        mDataTablayout1.add(new HKtxxBean("8", "8", "物理", "128", R.drawable.new_icon_wuli));
//        mDataTablayout1.add(new HKtxxBean("9", "9", "化学", "128", R.drawable.new_icon_huaxue));
//        mDataTablayout1.add(new HKtxxBean("10", "10", "政治", "128", R.drawable.new_icon_zhengzhi));
//        hCategoryKtxxBean.setList(mDataTablayout1);
//        mDataTablayout = new ArrayList<>();
//        for (int i = 0; i < hCategoryKtxxBean.getList().size(); i++) {
//            mDataTablayout.add(new OneKtxx(hCategoryKtxxBean.getList().get(i).getCode(), hCategoryKtxxBean.getList().get(i).getName(), hCategoryKtxxBean.getList().get(i).getSubject(), hCategoryKtxxBean.getList().get(i).getTime(), hCategoryKtxxBean.getList().get(i).getIcon()));
//        }
//        znxuexidapter.setNewData(mDataTablayout);
    }

    private void onclick() {
        /*课堂学习*/
//        recyclerViewAiznxuexi.setLayoutManager(new GridLayoutManager(getActivity(), 5));
//        recyclerViewAiznxuexi.setHasFixedSize(true);
//        recyclerViewAiznxuexi.setNestedScrollingEnabled(false);
//        recyclerViewAiznxuexi.setFocusable(false);
//        MyGridLayoutManager gridLayoutManager = new MyGridLayoutManager(getActivity(), 5);
//        gridLayoutManager.setScrollEnabled(false);
//        recyclerViewAiznxuexi.setLayoutManager(gridLayoutManager);
//        recyclerViewAiznxuexi.addItemDecoration(new SpaceItemDecoration(getActivity()));
//        mDataTablayout = new ArrayList<>();
//        znxuexidapter = new Znxuexidapter();
//        recyclerViewAiznxuexi.setAdapter(znxuexidapter);
//        znxuexidapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CtbActivity");
//                intent.putExtra("url_key", "file:///android_asset/js_aiznxx/ctb.html");
//                startActivity(intent);
//            }
//        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.rl_ct) {
            activoCtb();
        } else if (i == R.id.rl_ct1) {
            activoCtb();
        } else if (i == R.id.rl_ct2) {
            activoCtb();
        } else if (i == R.id.rl_ct3) {
            activoCtb();
        } else if (i == R.id.rl_ct4) {
            activoCtb();
        } else if (i == R.id.rl_ct5) {
            activoCtb();
        } else if (i == R.id.rl_ct6) {
            activoCtb();
        } else if (i == R.id.rl_ct7) {
            activoCtb();
        } else if (i == R.id.rl_ct8) {
            activoCtb();
        } else if (i == R.id.rl_ct9) {
            activoCtb();
        }
    }

    /*我的错题本*/
    private void activoCtb() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CtbActivity");
//        intent.putExtra("url_key", SPUtils.getInstance().getString("url", "https://www.baidu.com/") + "errorsBook/singleCourseQuestion?course=1");
//        intent.putExtra("url_key", "file:///android_asset/js_aiznxx/ctb.html");
        startActivity(intent);
    }
}
