package com.example.app1home.aistudent.shouye.adapter;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app1home.R;
import com.example.app1home.aistudent.shouye.OneKtxx;

public class Xxgjdapter extends BaseQuickAdapter<OneKtxx, BaseViewHolder> {

    public Xxgjdapter() {
        super(R.layout.recycleview_xuexigongju_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, OneKtxx item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        RelativeLayout rvxxgj = helper.itemView.findViewById(R.id.rv_xxgj);
        tv1.setText(item.getTab_name());
        tv2.setText(item.getSubject());
        tv3.setText(item.getTime());
        iv1.setImageResource(item.getTab_icon());
        iv1.setImageResource(item.getTab_icon());
        Resources res = mContext.getResources(); //resource handle
        Drawable drawable = res.getDrawable(item.getFrame()); //new Image that was added to the res folder
        rvxxgj.setBackground(drawable);
        if (item.getTab_id().equals("1")) {
            tv3.setTextColor(Color.parseColor("#FFB100"));
        } else if (item.getTab_id().equals("2")) {
            tv3.setTextColor(Color.parseColor("#D26DD9"));
        } else if (item.getTab_id().equals("3")) {
            tv3.setTextColor(Color.parseColor("#53BEA7"));
        } else if (item.getTab_id().equals("4")) {
            tv3.setTextColor(Color.parseColor("#3BB2E9"));
        }
    }
}
