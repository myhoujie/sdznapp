package com.example.app1home.aistudent.wdzy;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.AppUtils;
import com.example.app1home.R;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

/**
 * 我的课程发布记录
 */
public  class WdzyDetailsActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        refreshLayout1 = findViewById(R.id.refreshLayout1_order);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(3);
        setBaseOnClickListener(this);
    }

    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }

    public void Tijiaoanniu() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdzyTijiaoActivity");
        intent.putExtra("url_key", "file:///android_asset/js_wdzy/wdzytijiao.html");
        startActivity(intent);
    }


    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
