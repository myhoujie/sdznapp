package com.example.app1home.presenter;

import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app1home.R;
import com.example.app1home.view.MainView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.vo.RightRateVo;
import com.sdzn.fzx.student.vo.SubjectVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.WrongVo;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 首页Presenter
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public class MainPresenter extends BasePresenter<MainView, BaseActivity> {

    private int currentData = 0;

    public void getUnFinishes() {
        currentData = 0;
        Map<String, String> params1 = new HashMap<>();
        params1.put("userStudentId", UserController.getUserId());
        params1.put("status", "3");
        params1.put("page", "1");
        params1.put("rows", "3");
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getUnFinishes(params1)
                .map(new StatusFunc<TaskListVo>())
                .flatMap(new Func1<TaskListVo, Observable<StatusVo<TaskListVo>>>() {
                    @Override
                    public Observable<StatusVo<TaskListVo>> call(final TaskListVo unFinishVo) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentData = 1;
                                mView.getUnFinishSuccess(unFinishVo);
                            }
                        });
                        Map<String, String> params2 = new HashMap<>();
                        params2.put("userStudentId", UserController.getUserId());
                        params2.put("status", "2");
                        params2.put("page", "1");
                        params2.put("rows", "3");
                        return Network.createTokenService(NetWorkService.MainService.class).getFinishes(params2);
                    }
                })
                .map(new StatusFunc<TaskListVo>())
                .flatMap(new Func1<TaskListVo, Observable<StatusVo<WrongVo>>>() {
                    @Override
                    public Observable<StatusVo<WrongVo>> call(final TaskListVo finishVo) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentData = 2;
                                mView.getFinishSuccess(finishVo);
                            }
                        });
                        Map<String, String> params3 = new HashMap<>();
                        params3.put("userStudentId", UserController.getUserId());
                        params3.put("model", "1");
                        params3.put("timeUpdate", "1");
                        params3.put("baseLevelId", UserController.getBaseLevelId());
                        return Network.createTokenService(NetWorkService.MainService.class).getWronges(params3);
                    }
                })
                .map(new StatusFunc<WrongVo>())
                .flatMap(new Func1<WrongVo, Observable<StatusVo<SubjectVo>>>() {
                    @Override
                    public Observable<StatusVo<SubjectVo>> call(final WrongVo wrongVo) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getWrongSuccess(wrongVo);
                            }
                        });
                        Map<String, String> params3 = new HashMap<>();
                        params3.put("baseGradeId", UserController.getBaseGradeId());
                        params3.put("flag", "true");
                        return Network.createTokenService(NetWorkService.TaskService.class).getSubjects(params3);
                    }
                })
                .map(new StatusFunc<SubjectVo>())
                .flatMap(new Func1<SubjectVo, Observable<StatusVo<RightRateVo>>>() {
                    @Override
                    public Observable<StatusVo<RightRateVo>> call(final SubjectVo subjectVo) {
                        mView.setLastSubjectId(subjectVo);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mView.getSubjectSuccess(subjectVo);
                            }
                        });
                        Map<String, String> params4 = new HashMap<>();
                        params4.put("userStudentId", UserController.getUserId());
                        if (subjectVo != null && subjectVo.getData() != null && subjectVo.getData().size() > 0) {
                            int id = mView.getLastSubjectId();
                            if (id == -1) {
                                id = subjectVo.getData().get(0).getId();
                            }
                            params4.put("baseSubjectId", String.valueOf(id));
                        }
                        return Network.createTokenService(NetWorkService.MainService.class).getRightRates(params4);
                    }
                })
                .map(new StatusFunc<RightRateVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<RightRateVo>(new SubscriberListener<RightRateVo>() {
                    @Override
                    public void onNext(RightRateVo rightRateVo) {
                        mView.getRightRateSuccess(rightRateVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                        if (currentData == 0) {
                            mView.getUnFinishFailure();
                        } else if (currentData == 1) {
                            mView.getFinishFailure();
                        }
                    }

                    @Override
                    public void onCompleted() {
                        mView.onCompleted();
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public void getRightRates(int subjectId) {
        Map<String, String> params = new HashMap<>();
        params.put("userStudentId", UserController.getUserId());
        params.put("baseSubjectId", String.valueOf(subjectId));
        Subscription subscribe = Network.createTokenService(NetWorkService.MainService.class)
                .getRightRates(params)
                .map(new StatusFunc<RightRateVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<RightRateVo>(new SubscriberListener<RightRateVo>() {
                    @Override
                    public void onNext(RightRateVo rightRateVo) {
                        mView.getRightRateSuccess(rightRateVo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                        mView.onCompleted();
                    }
                }, mActivity, true));
        subscriptions.add(subscribe);
    }

    public void updateStatus(String lessonTaskStudentId) {
        Map<String, String> params = new HashMap<>();
        params.put("lessonTaskStudentId", lessonTaskStudentId);
        Subscription subscribe = Network.createTokenService(NetWorkService.TaskService.class)
                .updateStatus(params)
                .map(new StatusFunc<Object>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        if (mView == null) {
                            return;
                        }
                        mView.updateStatusSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, false, true, false, ""));
        subscriptions.add(subscribe);
    }

    public MyCountDownTimer bindUnfHolder(int rvHeight, GeneralRecyclerViewHolder holder, TaskListVo.DataBean itemData) {
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlUnfItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlUnfItem).setLayoutParams(layoutParams);

        Subject subject = Subject.subjects.get(itemData.getBaseSubjectId());
        if (subject != null) {
            holder.getChildView(R.id.ivXueke).setBackground(App2.get().getResources().getDrawable(subject.getDrawableId()));
        }
        if (itemData.getIsRead() == 0) {
            holder.getChildView(R.id.dot).setVisibility(View.VISIBLE);
        } else {
            holder.getChildView(R.id.dot).setVisibility(View.GONE);
        }
        holder.setText(R.id.tvTaskName, itemData.getName());
        holder.setText(R.id.tvNum, "试题 " + itemData.getCountExam() + "/资料 " + itemData.getCountResource());
        holder.setText(R.id.tvDate, DateUtil.getTimeStrByTimemillis(itemData.getEndTime(), "yyyy-MM-dd HH:mm"));

        if (itemData.getOvertime() < 0) {
            holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.count_down));
            holder.setText(R.id.tvTime, "超时");
        } else if (itemData.getOvertime() <= 30 * 60 * 1000) {
            holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.count_down));
            MyCountDownTimer myCountDownTimer = new MyCountDownTimer((TextView) holder.getChildView(R.id.tvTime), itemData.getOvertime(), 1000);
            myCountDownTimer.start();
            return myCountDownTimer;
        } else {
            holder.getChildView(R.id.tvTime).setVisibility(View.GONE);
        }
        return null;
    }

    public class MyCountDownTimer extends CountDownTimer {
        TextView textView;

        public MyCountDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.textView = textView;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (textView != null) {
                textView.setText(DateUtil.millsecondsToStr(millisUntilFinished));
            }
        }

        @Override
        public void onFinish() {
            textView.setText("超时");
            cancel();
        }
    }

    public void bindFHolder(int rvHeight, GeneralRecyclerViewHolder holder, TaskListVo.DataBean itemData) {
        ViewGroup.LayoutParams layoutParams = holder.getChildView(R.id.rlFItem).getLayoutParams();
        layoutParams.height = rvHeight / 3;
        holder.getChildView(R.id.rlFItem).setLayoutParams(layoutParams);

        Subject subject = Subject.subjects.get(itemData.getBaseSubjectId());
        if (subject != null) {
            holder.getChildView(R.id.ivXueke).setBackground(App2.get().getResources().getDrawable(subject.getDrawableId()));
        }
        if (itemData.getIsRead() == 0) {
            holder.getChildView(R.id.dot).setVisibility(View.VISIBLE);
        } else {
            holder.getChildView(R.id.dot).setVisibility(View.GONE);
        }
        holder.setText(R.id.tvTaskName, itemData.getName());
        if (itemData.getIsCorrect() == 0) {
            holder.setText(R.id.tvPigai, App2.get().getResources().getString(R.string.fragment_main_text5));
            ((TextView) holder.getChildView(R.id.tvPigai)).setTextColor(App2.get().getResources().getColor(R.color.fragment_main_item_num));
        } else {
            holder.setText(R.id.tvPigai, App2.get().getResources().getString(R.string.fragment_main_text4));
            ((TextView) holder.getChildView(R.id.tvPigai)).setTextColor(App2.get().getResources().getColor(R.color.fragment_main_item_pigai));
        }
        if (itemData.getAnswerViewOpen() == 0) {
            holder.getChildView(R.id.tvGongbu).setVisibility(View.GONE);
        } else {
            holder.getChildView(R.id.tvGongbu).setVisibility(View.VISIBLE);
        }
        if (itemData.getScore() <= 0) {
            holder.getChildView(R.id.tvScoreT).setVisibility(View.INVISIBLE);
            holder.getChildView(R.id.tvScore).setVisibility(View.INVISIBLE);
            holder.getChildView(R.id.tvSeparator).setVisibility(View.INVISIBLE);
            holder.getChildView(R.id.tvCount).setVisibility(View.INVISIBLE);
        } else {
            holder.getChildView(R.id.tvScoreT).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvScore).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvSeparator).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvCount).setVisibility(View.VISIBLE);
            holder.setText(R.id.tvScore, itemData.getScoreTotal() <= 0 ? "0" : String.valueOf(itemData.getScoreTotal()));
            holder.setText(R.id.tvCount, String.valueOf(itemData.getScore()));
        }

        if (itemData.getStatus() == 3) {
            holder.getChildView(R.id.tvTime).setVisibility(View.VISIBLE);
            holder.getChildView(R.id.tvTime).setBackground(App2.get().getResources().getDrawable(R.mipmap.bujiao_icon));
            holder.setText(R.id.tvTime, "补交");
        } else if (itemData.getStatus() == 2) {
            holder.getChildView(R.id.tvTime).setVisibility(View.GONE);
        }
    }
}
