package com.example.app1home.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.RightRateVo;
import com.sdzn.fzx.student.vo.SubjectVo;
import com.sdzn.fzx.student.vo.TaskListVo;
import com.sdzn.fzx.student.vo.WrongVo;

/**
 * 首页PresenterView
 *
 * @author wangchunxiao
 * @date 2018/1/11
 */
public interface MainView extends BaseView {

    void getUnFinishSuccess(TaskListVo unFinishVo);

    void getUnFinishFailure();

    void getFinishSuccess(TaskListVo finishVo);

    void getFinishFailure();

    void getWrongSuccess(WrongVo wrongVo);

    void getRightRateSuccess(RightRateVo rightRateVo);

    void getSubjectSuccess(SubjectVo subjectVo);

    void updateStatusSuccess();

    void networkError(String msg);

    void onCompleted();

    int getLastSubjectId();

    void setLastSubjectId(SubjectVo subjectVo);
}
