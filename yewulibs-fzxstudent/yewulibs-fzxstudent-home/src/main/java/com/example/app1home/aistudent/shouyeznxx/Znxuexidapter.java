package com.example.app1home.aistudent.shouyeznxx;

import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app1home.R;
import com.example.app1home.aistudent.shouye.OneKtxx;

public class Znxuexidapter extends BaseQuickAdapter<OneKtxx, BaseViewHolder> {

    public Znxuexidapter() {
        super(R.layout.recycleview_znxuexi_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, OneKtxx item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        String strs = "共" + "<font color = '#FA541C'>" + item.getTime() + "</font>" + "道";
        tv1.setText(item.getSubject());
        tv2.setText(Html.fromHtml(strs));
        iv1.setImageResource(item.getTab_icon());
    }
}
