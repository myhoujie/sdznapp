package com.example.app1home.aistudent.wdkc;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1home.R;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

/**
 * 我的课程发布记录
 */
public  class WdkcJiLuActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter biz3CheckverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        biz3CheckverionPresenter = new CheckverionFzxPresenter();
        biz3CheckverionPresenter.onCreate(this);
        TitleShowHideState(1);
        setBaseOnClickListener(this);
    }

    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        biz3CheckverionPresenter.checkVerion("3", "0");
    }

    @Override
    public void Titleshijian() {
        showCalendarDialog();
        BaseActWebActivity1.setOnDisplayRefreshListener(new BaseActWebActivity1.refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    /*展开时间*/
    @Override
    public void Titlezankaishijian() {
        showCalendarDialog();
        BaseActWebActivity1.setOnDisplayRefreshListener(new BaseActWebActivity1.refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });

    }

    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxSearchActivity");
        startActivity(intent);
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        ToastUtils.showLong("请求接口成功");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
