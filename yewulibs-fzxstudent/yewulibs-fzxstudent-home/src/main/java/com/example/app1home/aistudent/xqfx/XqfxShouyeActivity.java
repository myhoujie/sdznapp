package com.example.app1home.aistudent.xqfx;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutdapter;
import com.example.app1home.aistudent.xqfx.xq.XqfxBaogaoFragment;
import com.example.app1home.aistudent.xqfx.xq.XqfxJieduanFragment;
import com.example.app1home.aistudent.xqfx.xq.XqfxShouyeFragment;
import com.just.agentweb.App2;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.interfaces.XPopupCallback;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.bean.RecyclerTabBean;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.TabPresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.view.TabViews;

import java.util.ArrayList;
import java.util.List;

/*
 * 学情分析 首页
 * */
public  class XqfxShouyeActivity extends BaseActNoWebActivity1 implements BaseOnClickListener, TabViews {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;
    private int checkedPos = 4;//默认选中 学科

    private MessageReceiverIndex mMessageReceiver;
    private int defCurrentItem = 0;//设置默认见面

    TabPresenter tabPresenter;
    private String type = "10";//查询

    final String[] strArray = {"语文", "数学", "英语", "历史", "生物", "物理", "政治", "地理", "化学"};
    final int[] intArray = {R.drawable.new_icon_yuwen, R.drawable.new_icon_shuxue, R.drawable.new_icon_yingyu, R.drawable.new_icon_lishi, R.drawable.new_icon_shengwu,
            R.drawable.new_icon_wuli, R.drawable.new_icon_zhengzhi, R.drawable.new_icon_dili, R.drawable.new_icon_huaxue};


    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("XqfxShouyeActivity".equals(intent.getAction())) {
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
//        StatusBarUtil.setThisStatusBarColor(this, R.drawable.bg_title_jianbian);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("XqfxShouyeActivity");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
        tabPresenter = new TabPresenter();
        tabPresenter.onCreate(this);
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
//        if ("1".equals(getIntent().getStringExtra("show"))) {
//            defCurrentItem = 0;
//        } else if ("2".equals(getIntent().getStringExtra("show"))) {
//            defCurrentItem = 1;
//        }
        TitleShowHideState(3);
        setBaseOnClickListener(this);
        tvTitleName.setText("学情分析");
        initviw();
    }

    private void initviw() {
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i].equals(StudentSPUtils.getXueKeStr())) {
                checkedPos = i;
            }
        }
        if (strArray[checkedPos].equals("生物") || strArray[checkedPos].equals("英语")) {
            StudentSPUtils.saveXueKeStr(strArray[checkedPos]);
        } else {
            checkedPos = 4;
            StudentSPUtils.saveXueKeStr("生物");
        }
        tvDownXueKe.setText(strArray[checkedPos]);
        ivXueKe.setImageResource(intArray[checkedPos]);
        onclick();
        tabPresenter.querytab(type);
    }

    private void setData(List<RecyclerTabBean.ListBean> recyclerTabBeans) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < recyclerTabBeans.size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), recyclerTabBeans.get(i).getUrl(), true));
            } else {
                mDataTablayout.add(new OneBean1(recyclerTabBeans.get(i).getId(), recyclerTabBeans.get(i).getName(), recyclerTabBeans.get(i).getUrl(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
//        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        //
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter(50);
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }

    public static final String URL_KEY = "url_key";

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            initFragment(mFragmentList, mlist, i);
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    /**
     * 首次初始化 fragment
     */
    private void initFragment(List<Fragment> mFragmentList, List<OneBean1> mlist, int i) {
        String strUrl1 = "file:///android_asset/js_xqfx/shengwu_shouye.html";
        String strUrl2 = "file:///android_asset/js_xqfx/shengwu_baogao.html";
        String strUrl3 = "file:///android_asset/js_xqfx/xq_jieduan.html";
        if ("生物".equals(StudentSPUtils.getXueKeStr())) {
            strUrl1 = "file:///android_asset/js_xqfx/shengwu_shouye.html";
            strUrl2 = "file:///android_asset/js_xqfx/shengwu_baogao.html";
            strUrl3 = "file:///android_asset/js_xqfx/xq_jieduan.html";
        } else if ("英语".equals(StudentSPUtils.getXueKeStr())) {
            strUrl1 = "file:///android_asset/js_xqfx/yingyu_shouye.html";
            strUrl2 = "file:///android_asset/js_xqfx/yingyu_baogao.html";
            strUrl3 = "file:///android_asset/js_xqfx/xq_jieduan.html";
        }


        Bundle bundle = new Bundle();
        bundle.putString("id", mlist.get(i).getTab_id());
        if (i == 0) {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxShouyeFragment fragment1 = FragmentHelper.newFragment(XqfxShouyeFragment.class, bundle);
            mFragmentList.add(fragment1);
        } else if (i == 1) {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxBaogaoFragment fragment2 = FragmentHelper.newFragment(XqfxBaogaoFragment.class, bundle);
            mFragmentList.add(fragment2);
        } else {
            bundle.putString(URL_KEY, mlist.get(i).getUrl());
            XqfxJieduanFragment fragment3 = FragmentHelper.newFragment(XqfxJieduanFragment.class, bundle);
            mFragmentList.add(fragment3);
        }
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        current_id = bean1.getTab_id();
        set_footer_change(bean1);
//        initMultView();
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        tabPresenter.onDestory();
        super.onDestroy();
    }

    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcSouSuoActivity");
        intent.putExtra("show", "str");
        startActivity(intent);
    }

    @Override
    public void TitleFabupg() {
        new XPopup.Builder(XqfxShouyeActivity.this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asConfirm("发布批改", "点击任意按钮后，将把当前的批改情况发布给",
                        "结束批改", "稍后再批",
                        new OnConfirmListener() {
                            @Override
                            public void onConfirm() {
                                ToastUtils.showShort("click confirm");
                            }
                        }, null, false, com.sdzn.fzx.student.libbase.R.layout.popup_center_text) //最后一个参数绑定已有布局
                .show();
    }

    @Override
    public void Titlegrzx() {
        CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");

            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }


    private boolean is_onclik;

    @Override
    public void TitleXueKeDropdown() {
        if (!is_onclik) {
            arrowIvXk.setRotation(180);
            is_onclik = true;
        } else {
            arrowIvXk.setRotation(0);
            is_onclik = false;
        }
        new XPopup.Builder(XqfxShouyeActivity.this)
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .setPopupCallback(new XPopupCallback() {
                    @Override
                    public void onCreated(BasePopupView popupView) {

                    }

                    @Override
                    public void beforeShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                        arrowIvXk.setRotation(0);
                        is_onclik = false;
                    }

                    @Override
                    public void beforeDismiss(BasePopupView popupView) {

                    }

                    @Override
                    public boolean onBackPressed(BasePopupView popupView) {
                        return false;
                    }
                })

                .offsetY(30)
                .offsetX(-60)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                .atView(llDownXueKe)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList1(strArray,
                        intArray,
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
//                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                tvDownXueKe.setText(strArray[position]);
                                ivXueKe.setImageResource(intArray[position]);
                                arrowIvXk.setRotation(0);
                                is_onclik = false;
                                if (text.equals("生物") || text.equals("英语")) {
                                    StudentSPUtils.saveXueKeStr(text);
                                    sendBroadToFragment();
                                }
                            }
                        }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                .show();

    }

    private void sendBroadToFragment() {
        String strUrl1 = "";
        String strUrl2 = "";
        String strUrl3 = "";
        if ("生物".equals(StudentSPUtils.getXueKeStr())) {
            strUrl1 = "file:///android_asset/js_xqfx/shengwu_shouye.html";
            strUrl2 = "file:///android_asset/js_xqfx/shengwu_baogao.html";
            strUrl3 = "file:///android_asset/js_xqfx/xq_jieduan.html";

            Intent msgIntent1 = new Intent();
            msgIntent1.putExtra("url_key", strUrl1);
            msgIntent1.setAction("XqfxShouyeFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent1);

            Intent msgIntent2 = new Intent();
            msgIntent2.putExtra("url_key", strUrl2);
            msgIntent2.setAction("XqfxBaogaoFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent2);

            Intent msgIntent3 = new Intent();
            msgIntent3.putExtra("url_key", strUrl3);
            msgIntent3.setAction("XqfxJieduanFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent3);
        } else if ("英语".equals(StudentSPUtils.getXueKeStr())) {
            strUrl1 = "file:///android_asset/js_xqfx/yingyu_shouye.html";
            strUrl2 = "file:///android_asset/js_xqfx/yingyu_baogao.html";
            strUrl3 = "file:///android_asset/js_xqfx/xq_jieduan.html";

            Intent msgIntent1 = new Intent();
            msgIntent1.putExtra("url_key", strUrl1);
            msgIntent1.setAction("XqfxShouyeFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent1);

            Intent msgIntent2 = new Intent();
            msgIntent2.putExtra("url_key", strUrl2);
            msgIntent2.setAction("XqfxBaogaoFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent2);

            Intent msgIntent3 = new Intent();
            msgIntent3.putExtra("url_key", strUrl3);
            msgIntent3.setAction("XqfxJieduanFragment");
            com.example.baselibrary.LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent3);
        }


    }

    private int checkedPos1;
    private int checkedPos2;
    private int checkedPos3;
    private int checkedmul = 0;
    ArrayList<String> stringArrayList = new ArrayList<String>();

    /**
     *
     */
    private void initMultView() {
        stringArrayList.clear();
        if ("1".equals(current_id)) {
            stringArrayList.add("全部层级");
            stringArrayList.add("A+");
            stringArrayList.add("A");
            stringArrayList.add("B");
            stringArrayList.add("C");
            stringArrayList.add("D");
            checkedmul = checkedPos1;
        } else if ("2".equals(current_id)) {
            stringArrayList.add("全部场景");
            stringArrayList.add("自主学习");
            stringArrayList.add("课堂学习");
            stringArrayList.add("作业");
            stringArrayList.add("考试");
            checkedmul = checkedPos2;
        } else if ("3".equals(current_id)) {
            stringArrayList.add("正确率");
            stringArrayList.add("完成率");
            stringArrayList.add("班均分");
            checkedmul = checkedPos3;
        }
        tvDownMult.setText(stringArrayList.get(checkedmul));
    }

    @Override
    public void TitleMultDropdown() {
//        initMultView();
        final String[] strArrayMult = stringArrayList.toArray(new String[stringArrayList.size()]);

        if (!is_onclik) {
            arrowIvMult.setRotation(180);
            is_onclik = true;
        } else {
            arrowIvXk.setRotation(0);
            is_onclik = false;
        }
        new XPopup.Builder(XqfxShouyeActivity.this)
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                .setPopupCallback(new XPopupCallback() {
                    @Override
                    public void onCreated(BasePopupView popupView) {

                    }

                    @Override
                    public void beforeShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onShow(BasePopupView popupView) {

                    }

                    @Override
                    public void onDismiss(BasePopupView popupView) {
                        arrowIvMult.setRotation(0);
                        is_onclik = false;
                    }

                    @Override
                    public void beforeDismiss(BasePopupView popupView) {

                    }

                    @Override
                    public boolean onBackPressed(BasePopupView popupView) {
                        return false;
                    }
                })

                .offsetY(30)
                .offsetX(-60)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                .atView(llDownMult)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList1(strArrayMult,
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                if ("1".equals(current_id)) {
                                    checkedPos1 = position;
                                } else if ("2".equals(current_id)) {
                                    checkedPos2 = position;
                                } else if ("3".equals(current_id)) {
                                    checkedPos3 = position;
                                }
//                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                tvDownMult.setText(strArrayMult[position]);
                                arrowIvMult.setRotation(0);
                                is_onclik = false;
                            }
                        }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedmul)
                .show();
    }


    @Override
    public void onTabSuccess(RecyclerTabBean recyclerTabBean) {
        setData(recyclerTabBean.getList());
    }

    @Override
    public void onTabNodata(String msg) {

    }

    @Override
    public void onTabFail(String msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }
}