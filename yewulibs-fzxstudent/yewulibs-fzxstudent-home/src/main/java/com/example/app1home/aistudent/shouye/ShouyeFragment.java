package com.example.app1home.aistudent.shouye;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.aistudent.shouye.adapter.Ktxxdapter;
import com.example.app1home.aistudent.shouye.adapter.Wdzydapter;
import com.example.app1home.aistudent.shouye.adapter.Zzxxdapter;
import com.sdzn.fzx.student.bean.ShouyeRecActBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebFragment1;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.presenter.ShouyePresenter;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.view.ShouyeViews;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页
 *
 * @author wangchunxiao
 * @date 2018/1/31
 */
public class ShouyeFragment extends BaseActNoWebFragment1 implements View.OnClickListener, ShouyeViews {
    private TextView tvDynamicTitle;//头部标题
    private TextView tvShouyeTime;//头部时间
    private XRecyclerView recyclerViewKtxx;//课堂学习
    private XRecyclerView recyclerViewZzxx;//自主学习
    private XRecyclerView recyclerViewWdzy;//我的作业
    //    private XRecyclerView recyclerViewXxgj;//学习工具
    private RelativeLayout rlZnxtApp;//跳转学生端App
    private RelativeLayout rlWdks;//我的考试
    private RelativeLayout rlHztj;//合作探究
    private RelativeLayout rlXqfx;//学情分析
    private RelativeLayout rlWdpg;//我的批改
    /*我的考试*/
    private ImageView ivWdksPicture;
    private TextView tvWdksTitle;
    private TextView tvWdksContent;
    /*合作探究*/
    private ImageView ivHztjPicture;
    private TextView tvHztjTitle;
    private TextView tvHztjContent;
    /*学情分析*/
    private ImageView ivXqfxPicture;
    private TextView tvXqfxTitle;
    private TextView tvXqfxContent;
    /*我的批改*/
    private ImageView ivWdpgPictrue;
    private TextView tvWdpgTitle;
    private TextView tvWdpgContent;

    /*自主学习*/
    private TextView tvZzxxTitle;//标题
    private TextView tvZzxxComplete;//完成度
    private TextView tvZzxxTasks;//自主学习全部任务
    private LinearLayout llZzxxNodata;//自主学习暂无数据

    /*我的作业*/
    private TextView tvWdzyTitle;//我的作业标题
    private TextView tvWdzyComplete;//完成度
    private TextView tvWdzyTasks;//我的作业全部任务
    private LinearLayout llZuoyeNodata;//作业暂无数据

    /*课堂学习*/
    private TextView tvKtxxTitle;//我的课堂标题
    private TextView tvKtxxTasks;//课堂学习全部任务
    private LinearLayout llKtxxNodata;//课堂学习暂无数据

    private Ktxxdapter ktxxdapter;
    private Zzxxdapter zzxxdapter;
    private Wdzydapter wdzydapter;
    private List<OneKtxx> mDataTablayout;
    private List<OneKtxx> mDataTablayoutzzxx;
    private List<OneKtxx> mDataTablayout2wdzy;
    ShouyePresenter shouyePresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_student_shouye;
    }


    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        tvDynamicTitle = rootView.findViewById(R.id.tv_dynamic_title);//首页标题
        tvShouyeTime = rootView.findViewById(R.id.tv_shouye_time);//首页时间
        recyclerViewKtxx = rootView.findViewById(R.id.recycler_view_ktxx);//课堂学习列表
        recyclerViewZzxx = rootView.findViewById(R.id.recycler_view_zzxx);//自主学习列表
        recyclerViewWdzy = rootView.findViewById(R.id.recycler_view_wdzy);//我的作业列表
        rlWdks = rootView.findViewById(R.id.rl_wdks);//我的考试
        rlHztj = rootView.findViewById(R.id.rl_hztj);//合作探究
        rlXqfx = rootView.findViewById(R.id.rl_xqfx);//学情分析
        rlWdpg = rootView.findViewById(R.id.rl_wdpg);//我的批改

        //我的考试
        ivWdksPicture = (ImageView) rootView.findViewById(R.id.iv_wdks_picture);
        tvWdksTitle = (TextView) rootView.findViewById(R.id.tv_wdks_title);
        tvWdksContent = (TextView) rootView.findViewById(R.id.tv_wdks_content);
        //合作探究
        ivHztjPicture = (ImageView) rootView.findViewById(R.id.iv_hztj_picture);
        tvHztjTitle = (TextView) rootView.findViewById(R.id.tv_hztj_title);
        tvHztjContent = (TextView) rootView.findViewById(R.id.tv_hztj_content);
        //学情分析
        ivXqfxPicture = (ImageView) rootView.findViewById(R.id.iv_xqfx_picture);
        tvXqfxTitle = (TextView) rootView.findViewById(R.id.tv_xqfx_title);
        tvXqfxContent = (TextView) rootView.findViewById(R.id.tv_xqfx_content);
        //我的批改
        ivWdpgPictrue = (ImageView) rootView.findViewById(R.id.iv_wdpg_pictrue);
        tvWdpgTitle = (TextView) rootView.findViewById(R.id.tv_wdpg_title);
        tvWdpgContent = (TextView) rootView.findViewById(R.id.tv_wdpg_content);

        /*自主学习*/
        tvZzxxTitle = rootView.findViewById(R.id.tv_zzxx_title);//自主学习标题
        tvZzxxComplete = rootView.findViewById(R.id.tv_zzxx_complete);//完成度
        tvZzxxTasks = rootView.findViewById(R.id.tv_zzxx_tasks);//自主学习全部任务按钮
        llZzxxNodata = rootView.findViewById(R.id.ll_zzxx_nodata);

        /*我的作业*/
        tvWdzyTitle = rootView.findViewById(R.id.tv_wdzy_title);//我的作业标题
        tvWdzyComplete = rootView.findViewById(R.id.tv_wdzy_complete);//我的作业标题
        tvWdzyTasks = rootView.findViewById(R.id.tv_wdzy_tasks);//我的作业全部任务按钮
        llZuoyeNodata = rootView.findViewById(R.id.ll_zuoye_nodata);

        /*课堂学习*/
        tvKtxxTitle = rootView.findViewById(R.id.tv_ktxx_title);//课堂学习标题
        tvKtxxTasks = rootView.findViewById(R.id.tv_ktxx_tasks);//课堂学习全部任务按钮
        llKtxxNodata = rootView.findViewById(R.id.ll_ktxx_nodata);

        /*跳转app*/
        rlZnxtApp = rootView.findViewById(R.id.rl_znxt_app);//跳转拼课堂app按钮

        tvWdzyTasks.setOnClickListener(this);
        tvZzxxTasks.setOnClickListener(this);
        tvKtxxTasks.setOnClickListener(this);
        rlZnxtApp.setOnClickListener(this);
        rlWdks.setOnClickListener(this);
        rlHztj.setOnClickListener(this);
        rlXqfx.setOnClickListener(this);
        rlWdpg.setOnClickListener(this);

        shouyePresenter = new ShouyePresenter();
        shouyePresenter.onCreate(this);
        onclick();
    }


    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        shouyePresenter.queryShouye();
    }

    private void donetworkketangwodezuoye(List<ShouyeRecActBean.Studentlist1Bean.StudentitemlistBean> studentitemlist) {
//        HCategoryKtxxBean hCategoryKtxxBean = new HCategoryKtxxBean();
//        List<HKtxxBean> mDataTablayout1 = new ArrayList<>();
//        mDataTablayout1.add(new HKtxxBean("1", "English immortalle hahaha", "英语", "2020.06.30 16:00", R.drawable.new_icon_yingyu));
//        mDataTablayout1.add(new HKtxxBean("2", "狼牙山五壮士", "语文 第一单元 第一节", "2020.06.30 16:00", R.drawable.new_icon_yuwen));
//        mDataTablayout1.add(new HKtxxBean("3", "我的世界", "地理 第二章 我的世界", "2020.06.30 16:00", R.drawable.new_icon_dili));
//        hCategoryKtxxBean.setList(mDataTablayout1);
//        mDataTablayout2wdzy = new ArrayList<>();
        if (studentitemlist.size() == 0 || studentitemlist == null) {
            llZuoyeNodata.setVisibility(View.VISIBLE);
        } else {
            llZuoyeNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlist.size(); i++) {
                mDataTablayout2wdzy.add(new OneKtxx(studentitemlist.get(i).getSubjectid(), studentitemlist.get(i).getCurriculumname(), studentitemlist.get(i).getSubjectname(), studentitemlist.get(i).getTime(), studentitemlist.get(i).getSubjectpicture(), studentitemlist.get(i).getAct()));
            }
            wdzydapter.setNewData(mDataTablayout2wdzy);
        }
    }

    private void donetworkketangzizhuxuexi(List<ShouyeRecActBean.Studentlist2Bean.StudentitemlistBeanX> studentitemlistBeanXList) {
//        HCategoryKtxxBean hCategoryKtxxBean = new HCategoryKtxxBean();
//        List<HKtxxBean> mDataTablayout1 = new ArrayList<>();
//        mDataTablayout1.add(new HKtxxBean("1", "English immortalle hahaha", "英语", "2020.06.30 16:00", R.drawable.new_icon_yingyu));
//        mDataTablayout1.add(new HKtxxBean("2", "光合作用", "生物 第一单元 第一节", "2020.06.30 16:00", R.drawable.new_icon_shengwu));
//        mDataTablayout1.add(new HKtxxBean("3", "我的世界", "地理 第二章 我的世界", "2020.06.30 16:00", R.drawable.new_icon_dili));
//        hCategoryKtxxBean.setList(mDataTablayout1);
//        mDataTablayoutzzxx = new ArrayList<>();
        if (studentitemlistBeanXList.size() == 0 || studentitemlistBeanXList == null) {
            llZzxxNodata.setVisibility(View.VISIBLE);
        } else {
            llZzxxNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlistBeanXList.size(); i++) {
                mDataTablayoutzzxx.add(new OneKtxx(studentitemlistBeanXList.get(i).getSubjectid(), studentitemlistBeanXList.get(i).getCurriculumname(), studentitemlistBeanXList.get(i).getSubjectname(), studentitemlistBeanXList.get(i).getTime(), studentitemlistBeanXList.get(i).getSubjectpicture(), studentitemlistBeanXList.get(i).getAct()));
            }
            zzxxdapter.setNewData(mDataTablayoutzzxx);
        }
    }

    private void donetworkketangxuexi(List<ShouyeRecActBean.Studentlist3Bean.StudentitemlistBeanXX> studentitemlistBeanXXList) {
//        HCategoryKtxxBean hCategoryKtxxBean = new HCategoryKtxxBean();
//        List<HKtxxBean> mDataTablayout1 = new ArrayList<>();
//        mDataTablayout1.add(new HKtxxBean("1", "English immortalle hahaha", "英语", "2020.06.30 16:00", R.drawable.new_icon_yingyu));
//        mDataTablayout1.add(new HKtxxBean("2", "认识集合的魅力 - 随堂测验", "数学", "2020.06.30 16:00", R.drawable.new_icon_shuxue));
//        mDataTablayout1.add(new HKtxxBean("3", "认识运动的世界 - 课堂检测", "物理", "2020.06.30 16:00", R.drawable.new_icon_wuli));
//        mDataTablayout1.add(new HKtxxBean("4", "English immortalle hahaha", "英语", "2020.06.30 16:00", R.drawable.new_icon_yingyu));
//        mDataTablayout1.add(new HKtxxBean("5", "认识集合的魅力 - 随堂测验", "数学", "2020.06.30 16:00", R.drawable.new_icon_shuxue));
//        hCategoryKtxxBean.setList(mDataTablayout1);
//        mDataTablayout = new ArrayList<>();
        if (studentitemlistBeanXXList.size() == 0 || studentitemlistBeanXXList == null) {
            llKtxxNodata.setVisibility(View.VISIBLE);
        } else {
            llKtxxNodata.setVisibility(View.GONE);
            for (int i = 0; i < studentitemlistBeanXXList.size(); i++) {
                mDataTablayout.add(new OneKtxx(studentitemlistBeanXXList.get(i).getSubjectid(), studentitemlistBeanXXList.get(i).getCurriculumname(), studentitemlistBeanXXList.get(i).getSubjectname(), studentitemlistBeanXXList.get(i).getTime(), studentitemlistBeanXXList.get(i).getSubjectpicture(), studentitemlistBeanXXList.get(i).getAct()));
            }
            ktxxdapter.setNewData(mDataTablayout);
        }
    }

    private void onclick() {
        /*课堂学习*/
        recyclerViewKtxx.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDataTablayout = new ArrayList<>();
        ktxxdapter = new Ktxxdapter();
        recyclerViewKtxx.setAdapter(ktxxdapter);
        ktxxdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.KtxxActivity"));//WdkcJiLuActivity
                startActivity(intent);
            }
        });


        /*自主学习*/
        recyclerViewZzxx.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDataTablayoutzzxx = new ArrayList<>();
        zzxxdapter = new Zzxxdapter();
        recyclerViewZzxx.setAdapter(zzxxdapter);
        zzxxdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.ZzxxActivity"));
                startActivity(intent);
            }
        });

        /*我的作业*/
        recyclerViewWdzy.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDataTablayout2wdzy = new ArrayList<>();
        wdzydapter = new Wdzydapter();
        recyclerViewWdzy.setAdapter(wdzydapter);
        wdzydapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.fzx.student.hs.act.WdzyActivity"));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_wdks) {//我的考试
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getWdks() != null) {
                    IntentShow(shouyeRecActBean.getWdks().getAct());
                }
            }
        } else if (id == R.id.rl_hztj) {//合作探究
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getHztj() != null) {
                    IntentShow(shouyeRecActBean.getHztj().getAct());
                }
            }
        } else if (id == R.id.rl_xqfx) {//学情分析
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getXqfx() != null) {
                    IntentShow(shouyeRecActBean.getXqfx().getAct());
                }
            }
        } else if (id == R.id.rl_wdpg) {//我的批改
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getWdpg() != null) {
                    IntentShow(shouyeRecActBean.getWdpg().getAct());
                }
            }
        } else if (id == R.id.tv_zzxx_tasks) {//自主学习
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getStudentlist1() != null) {
                    IntentShow(shouyeRecActBean.getStudentlist1().getAct());
                }
            }
        } else if (id == R.id.tv_wdzy_tasks) {//我的作业
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getStudentlist2() != null) {
                    IntentShow(shouyeRecActBean.getStudentlist2().getAct());
                }
            }
        } else if (id == R.id.tv_ktxx_tasks) {//课堂学习
            if (shouyeRecActBean == null) {
                return;
            } else {
                if (shouyeRecActBean.getStudentlist3() != null) {
                    IntentShow(shouyeRecActBean.getStudentlist3().getAct());
                }
            }
        } else if (id == R.id.rl_znxt_app) {//跳转app
            if (AppUtils.isAppInstalled("com.sdzn.pkt.student.hd")) {
//                Intent intent = new Intent();
//                intent.setAction("com.sdzn.pkt.student.hd.hs.act.SplashActivity");
//                startActivity(intent);
                String name = StudentSPUtils.getLoginUserNum();
                String pwd = StudentSPUtils.getLoginUserPwd();
                String state = "1";
                Log.e("aaatest", "账号：" + name + "密码：" + pwd);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/com.sdzn.pkt.student.hd.hs.act.SplashActivity?name=" + name + "&pwd=" + pwd + "&state=" + state));
                startActivity(intent);
            } else {
                ToastUtils.showLong("请先安装智囊学堂学生端APP");
            }
        }
    }

    private void IntentShow(String act) {
        if (act != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(act));
            startActivity(intent);
        }
    }


    private ShouyeRecActBean shouyeRecActBean;

    @Override
    public void onCehuaSuccess(ShouyeRecActBean grzxRecActBean) {
//        ToastUtils.showLong("成功....");
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        shouyeRecActBean = new ShouyeRecActBean();
        shouyeRecActBean = grzxRecActBean;
        mDataTablayout.clear();
        mDataTablayoutzzxx.clear();
        mDataTablayout2wdzy.clear();

        if (shouyeRecActBean == null) {
            return;
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(100);
                }
            }).start();

        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 100) {
                shouyeXxgj();//学习工具
                donetworkketangwodezuoye(shouyeRecActBean.getStudentlist1().getStudentitemlist());//获取我的作业列表数据
                donetworkketangzizhuxuexi(shouyeRecActBean.getStudentlist2().getStudentitemlist());//获取自主学习列表数据
                donetworkketangxuexi(shouyeRecActBean.getStudentlist3().getStudentitemlist());//获取课堂学习学习列表数据
            }
        }
    };

    @Override
    public void onCehuaNodata(String msg) {
//        ToastUtils.showLong("无数据....");
        emptyview1.nodata();
        refreshLayout1.finishRefresh(true);
    }

    @Override
    public void onCehuaFail(String msg) {
//        ToastUtils.showLong("失败....");
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(true);

    }

    /*学习工具*/
    private void shouyeXxgj() {
        if (shouyeRecActBean != null) {
            /*头部头部内容*/
            tvDynamicTitle.setText(Html.fromHtml(shouyeRecActBean.getStudentcount().getStudenttitle()));
            tvShouyeTime.setText(shouyeRecActBean.getStudentcount().getStudenttime());
            /*我的考试*/
            Glide.with(getActivity()).load(shouyeRecActBean.getWdks().getPicture()).skipMemoryCache(false).dontAnimate().centerCrop().into(ivWdksPicture);
            tvWdksTitle.setText(shouyeRecActBean.getWdks().getName());
            tvWdksContent.setText(shouyeRecActBean.getWdks().getDetails());

            /*合作探究*/
            Glide.with(getActivity()).load(shouyeRecActBean.getHztj().getPicture()).into(ivHztjPicture);
            tvHztjTitle.setText(shouyeRecActBean.getHztj().getName());
            tvHztjContent.setText(shouyeRecActBean.getHztj().getDetails());

            /*学情分析*/
            Glide.with(getActivity()).load(shouyeRecActBean.getXqfx().getPicture()).into(ivXqfxPicture);
            tvXqfxTitle.setText(shouyeRecActBean.getXqfx().getName());
            tvXqfxContent.setText(shouyeRecActBean.getXqfx().getDetails());

            /*我的批改*/
            Glide.with(getActivity()).load(shouyeRecActBean.getWdpg().getPicture()).into(ivWdpgPictrue);
            tvWdpgTitle.setText(shouyeRecActBean.getWdpg().getName());
            tvWdpgContent.setText(shouyeRecActBean.getWdpg().getDetails());

            /*自主学习*/
            tvZzxxTitle.setText(shouyeRecActBean.getStudentlist1().getName());
            tvZzxxComplete.setText(shouyeRecActBean.getStudentlist1().getDwctitle());

            /*我的作业*/
            tvWdzyTitle.setText(shouyeRecActBean.getStudentlist2().getName());
            tvWdzyComplete.setText(shouyeRecActBean.getStudentlist2().getDwctitle());

            /*课堂学习*/
            tvKtxxTitle.setText(shouyeRecActBean.getStudentlist3().getName());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shouyePresenter.onDestory();
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }


}
