package com.example.app1home.activity;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1home.R;
import com.example.app1home.adapter.GuidanceAdapter;
import com.example.app1home.fragment.GuidanceFragment;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.LoginProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.event.BLEServiceEvent;
import com.sdzn.fzx.student.libbase.login.activity.LoginActivity;
import com.sdzn.fzx.student.libbase.login.presenter.LoginPresenter1;
import com.sdzn.fzx.student.libbase.login.view.LoginViews;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libbase.show.PenActivity;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.CommonUtils;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.StringUtils;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.utils.CommonAppUtils;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.LoginBean;
import com.tencent.bugly.crashreport.CrashReport;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by
 * 功能介绍：欢迎页
 * 修改内容：修改
 * 修改时间：2020/4/17
 * 修改单号：
 * 修改内容：。。。。。。
 */
public class WelcomeActivity extends PenActivity implements LoginViews, EasyPermissions.PermissionCallbacks {

    private RelativeLayout netErrorRy;
    private ViewPager guidanceViewpager;
    private ImageView ivBottomWelcome;
    private ImageView ivWelcome;


    private GuidanceAdapter vpAdapter;
    SharedPreferences lastSp;
    SharedPreferences pairedSp;
    BluetoothAdapter mBluetoothAdapter;
    /**
     * 上次配对信息
     */
    public static final String SP_LAST_PAIRED = "last_paired_device";
    /**
     * 记录配对信息
     */
    public static final String SP_PAIRED_DEVICE = "sp_paird";
    /**
     * 关键字
     */
    public static final String SP_PAIRED_KEY = "address";

    private boolean isFromBrought;

    private static final int REQUEST_CODE_LOCATION = 1;
    String[] ALL_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private Dialog dialog;

    LoginPresenter1 mPresenter;

    public void initPresenter() {
        mPresenter = new LoginPresenter1();
        mPresenter.onCreate(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        /*屏幕适配autosize算法*/
       /* DisplayMetrics dm =new DisplayMetrics();
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getRealMetrics(dm);
        int widthPixels = dm.widthPixels;//单位为像素 px
        int height = dm.heightPixels;//单位为像素 px
        float density = dm.density;
        float scaledDensity = dm.scaledDensity;
        int densityDpi = dm.densityDpi;
        Log.e("gongshi", String.valueOf(densityDpi));
        Log.e("gongshi", (Math.sqrt(Math.pow(1280, 2) + (Math.pow(800, 2))) / 25.4 + ""));*/
        initPresenter();
        dialog = new Dialog(WelcomeActivity.this, R.style.notice_dialog);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            isFromBrought = true;
            finish();
            return;
        }
        if (!isTaskRoot()) {
            Intent i = getIntent();
            String action = i.getAction();
            if (i.hasCategory(Intent.CATEGORY_APP_CALENDAR)
                    && !TextUtils.isEmpty(action)
                    && action.equals(Intent.ACTION_MAIN)) {

                finish();
                return;
            }
        }
        setContentView(R.layout.activity_welcome);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initView();

        BLEServiceManger.getInstance().stopService();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //获取存储存储
        lastSp = this.getSharedPreferences(SP_LAST_PAIRED, MODE_PRIVATE);
        pairedSp = this.getSharedPreferences(SP_PAIRED_DEVICE, MODE_PRIVATE);
        methodRequiresTwoPermission2();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                    startActivity(intent);
                }
            }
        }

    }


    protected void initView() {
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        guidanceViewpager = (ViewPager) findViewById(R.id.guidance_viewpager);
        ivBottomWelcome = findViewById(R.id.iv_bottom_welcome);
        ivWelcome = findViewById(R.id.iv_welcome);
        vpAdapter = new GuidanceAdapter(getSupportFragmentManager());
        guidanceViewpager.setAdapter(vpAdapter);
    }

    private static final int JUMP = 0;
    private static final int EXIT = 1000;
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case JUMP:
                    handlerJump();
                    break;
                case EXIT:
                    CommonAppUtils.logout();
                    ActivityManager.exit();
//                    BaseAppManager.getInstance().closeApp();
                    System.exit(0);
                    break;
            }
        }
    };

    private void handlerJump() {
        //final boolean isGuidance = (boolean) SPUtils.get(WelcomeActivity.this, SPUtils.GUIDANCE_TAG, false);
        boolean is_first = SPUtils.getInstance().getBoolean("welcome", false);
        if (is_first) {
//                if (vpAdapter.getCount() == 0) {
            ivBottomWelcome.setVisibility(View.VISIBLE);
            ivWelcome.setVisibility(View.VISIBLE);
            guidanceViewpager.setVisibility(View.GONE);
            String num = StudentSPUtils.getLoginUserNum();
            String pwd = StudentSPUtils.getLoginUserPwd();
            if (!num.isEmpty() && !pwd.isEmpty()) {
                if (!mPresenter.vertifyNum(num, pwd)) {
                    return;
                }
                mPresenter.login(num, pwd, WelcomeActivity.this);
            } else {
                jump();
            }
//                }
            if (vpAdapter.getCount() != 0) {
                CrashReport.postCatchedException(new Throwable("vpAdapter.getCount() == " + vpAdapter.getCount()));
            }
        } else {
            guidanceViewpager.setVisibility(View.VISIBLE);
            ivBottomWelcome.setVisibility(View.GONE);
            ivWelcome.setVisibility(View.GONE);
            initGuidance();
        }
//        StudentSPUtils.put(WelcomeActivity.this, StudentSPUtils.GUIDANCE_TAG, true);
//        SPUtils.getInstance().put(StudentSPUtils.GUIDANCE_TAG, true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        if (!isFromBrought) {
            if (handler != null)
                handler.removeCallbacksAndMessages(null);
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    private void initGuidance() {
        List<Fragment> views = new ArrayList<>();
        for (int i = 0; i < GuidanceFragment.pics.length; i++) {
            Fragment fragment = new GuidanceFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(GuidanceFragment.TAG, i);
            fragment.setArguments(bundle);
            views.add(fragment);
        }
        vpAdapter.setData(views);
    }

    /**
     * 跳转登录
     */
    public synchronized void jump() {
        if (CommonAppUtils.isLingChuangPad()) {
            getInfo();
            if (autoLogin) {
                String name = WelcomeActivity.this.name;
                String pwd = WelcomeActivity.this.pwd;
                login(name, pwd);
            } else {
                ToastUtil.showShortlToast("登录状态失效, 请重新登录");
                exit();
            }
        } else {
            SPUtils.getInstance().put("welcome", true);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean autoLogin;
    private String name;
    private String pwd;

    private void getInfo() {
        //3.6之后开始使用provider存储账号密码
//        SSOInfoBean ssoInfo = App2.get().getSSOInfo();
//        if (ssoInfo != null) {
//            name = ssoInfo.getName();
//            pwd = ssoInfo.getPwd();
//            autoLogin = ssoInfo.isAutoLogin();
//            return;
//        }
        //3.6之前sso登录模块账号密码直接存本地文件
        File dir = Environment.getExternalStorageDirectory();
        if (dir != null) {
            dir = new File(dir, "sdzn/sso/sso");
            try {
                BufferedReader reader = new BufferedReader(new FileReader(dir));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] arr = line.split("=");
                    if (arr.length != 2) {
                        reader.close();
                        ToastUtil.showShortlToast("登录状态失效, 请重新登录");
                        exit();
                        return;
                    }
                    switch (arr[0]) {
                        case "autologin":
                            if ("1".equals(arr[1])) {
                                autoLogin = true;
                            }
                            break;
                        case "name":
                            name = arr[1];
                            break;
                        case "pwd":
                            pwd = arr[1];
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void login(final String name, String psw) {

        if (!vertifyNum(name, psw)) {
            exit();
            return;
        }
        final String deviceId = AndroidUtil.getDeviceID(App2.get());
        if ("/auth".equals(BuildConfig2.AUTH)) {
        }
        //这里货架化测试不通过
        CrashReport.putUserData(App2.get(), "studentName", Base64.encodeToString(name.getBytes(), Base64.NO_WRAP));
        psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
        CrashReport.putUserData(App2.get(), "studentPwd", psw);
        Network.createService(NetWorkService.LoginService.class)
                .login(name, psw, 1, deviceId, "Android")
                .map(new StatusFunc<LoginBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new LoginProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
                    @Override
                    public void onNext(LoginBean o) {
                        loginSuccess(o);
                        startMqttService();
                        CommonUtils.isReceiveRbMq = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof ApiException) {
                            if (((ApiException) e).getStatus() != null) {
                                ToastUtil.showShortlToast(((ApiException) e).getStatus().getMsg());
                            }
                        } else {
                            ToastUtil.showShortlToast("登录失败");
                        }
                        exit();
                    }

                    @Override
                    public void onCompleted() {

                    }
                }, WelcomeActivity.this, true, false, true, null));
    }

    private void exit() {
        handler.sendEmptyMessageDelayed(EXIT, 1000);
    }

    public void loginSuccess(LoginBean vo) {
        StudentSPUtils.saveLoginBean(vo);
//        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1");
        startActivity(intent);
        finish();
    }

    private boolean vertifyNum(final String userName, final String psw) {
        if (userName.length() == 8 || userName.length() == 18) {
            if (StringUtils.vertifyPsw(psw)) {
                return true;
            } else {
                ToastUtil.showShortlToast("账号或密码错误");
            }
        } else {
            ToastUtil.showShortlToast("账号或密码错误");
        }
        return false;

    }

    /**
     * 启动mqtt
     */
    public void startMqttService() {
        Intent intent = new Intent(WelcomeActivity.this, MqttService.class);
        WelcomeActivity.this.startService(intent);

    }

    /**
     * 蓝牙未开启请求
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 0xb) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            //判断蓝牙和位置如果授权的权限就可以进入登录else就在取掉一遍权限
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                checkDevice();//检测设备如果连接过则自动连接
                dialog.dismiss();
            } else {
                methodRequiresTwoPermission2();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        //将结果转发给EasyPermissions
//        this.permissions.onRequestPermissionsResult(permissions, grantResults);
////        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
//    }


    @AfterPermissionGranted(REQUEST_CODE_LOCATION)
    private void methodRequiresTwoPermission2() {
        if (EasyPermissions.hasPermissions(this, ALL_PERMISSIONS)) {
            // 已经申请过权限，做想做的事
            checkDevice();
            dialog.dismiss();
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUEST_CODE_LOCATION, ALL_PERMISSIONS);
        }
    }


    /**
     * 服务连接成功
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceConnected(BLEServiceEvent event) {
        if (mBluetoothAdapter.isEnabled()) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                checkDevice();//检测设备如果连接过则自动连接
                dialog.dismiss();
            } else {
                methodRequiresTwoPermission2();
            }
        } else {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            this.startActivityForResult(enableBtIntent, 0xb);
        }
    }

    /**
     * 检测设备连接 如果本次连接的是P1则禁止使用如果本次连接的是蓝牙设备则不处理
     * 如果本次未连接但上次已连接蓝牙设备则直接连接
     * 只有在onServiceConnected之后robotService才可以正常使用
     **/
    private void checkDevice() {
        BLEServiceManger.getInstance().startService();
        //手写板逻辑
//        try {
//            RobotDevice robotDevice = getPenServiceBinder().getConnectedDevice(); //获取目前连接的设备
//            if (robotDevice == null) {//已连接设备
//                //获取上次连接设备
//                if (!pairedSp.getString(SP_PAIRED_KEY, "").isEmpty() && mBluetoothAdapter.isEnabled()) {
//                    //已经连接过蓝牙设备 从pairedSp中获取
//                    String laseDeviceAddress = pairedSp.getString(SP_PAIRED_KEY, "");
//                    if (getPenServiceBinder().connectDevice(laseDeviceAddress)) {
//                        CommonUtils.flag = 1;
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        handler.removeCallbacksAndMessages(null);
        handler.sendEmptyMessageDelayed(JUMP, 500);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            if (dialog.isShowing()) {
                return;
            }
            dialog.setContentView(R.layout.notice_dialog);
            dialog.setCancelable(false);
            dialog.show();
            TextView tv_notice = dialog.findViewById(R.id.tv_notice);
            Button btn_concle = dialog.findViewById(R.id.btn_concle);
            Button btn_settings = dialog.findViewById(R.id.btn_settings);
            tv_notice.setText("当前应用缺少必要权限" + "\n\n" + "请点击|" + "设置|" + "权限" + "-打开所需权限");
            btn_concle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void loginSuccess(LoginBean.DataBean vo) {
        setResult(SlbLoginUtil2.LOGIN_RESULT_OK);
        LoginBean loginBean = new LoginBean();
        loginBean.setData(vo);
        StudentSPUtils.saveLoginBean(loginBean);
        com.blankj.utilcode.util.SPUtils.getInstance().put("token", vo.getAccessToken());

//        final Editable userNumText = userNumEdit.getText();
//        final Editable pswText = pswEdit.getText();
//        StudentSPUtils.put(this, StudentSPUtils.LOGIN_USER_NUM, userNumText);
//        StudentSPUtils.put(this, StudentSPUtils.LOGIN_USER_PWD, pswText);

        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);
        if (ActivityUtils.getActivityList().size() == 1) {
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity1"));
        }
        finish();
        startMqttService();
    }

    @Override
    public void loginFailed(String msg) {
        ToastUtils.showShort(msg);
    }
}
