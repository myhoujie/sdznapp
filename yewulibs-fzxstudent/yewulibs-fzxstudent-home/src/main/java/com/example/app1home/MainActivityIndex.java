package com.example.app1home;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.example.app1home.adapter.ShouyeFooterAdapter;
import com.example.app1home.adapter.ShouyeFooterBean;
import com.example.app1home.aistudent.shouye.ShouyeFragment;
import com.example.app1home.aistudent.shouyeznxx.ZnxuexiFragment;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebActivity1;
import com.sdzn.fzx.student.libpublic.utils.CircleTransform;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.view.CheckverionViews;
import com.sdzn.fzx.student.vo.LoginBean;

import java.util.ArrayList;
import java.util.List;


public class MainActivityIndex extends BaseActNoWebActivity1 implements CheckverionViews, View.OnClickListener {

    private RecyclerView recyclerView;
    private ShouyeFooterAdapter mAdapter;
    private TextView tvSubject;//个人中心
    private TextView tvName;//首页名称
    private RoundAngleImageView shouyeHead;//学生头像
    private FragmentManager mFragmentManager;

    private int current_pos = 0;
    private String tag_ids;
    private List<ShouyeFooterBean> mList;
    public static final String id1 = "11";
    public static final String id2 = "22";
    private static final String LIST_TAG1 = "list11";
    private static final String LIST_TAG2 = "list22";
    private static boolean isExit;// 标示是否退出

    private ShouyeFragment mFragment1; //
    private ZnxuexiFragment mFragment2; //

//
//    @Override
//    public Resources getResources() {
//        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
//        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
//        AutoSizeCompat.autoConvertDensity((super.getResources()), 800, false);//如果有自定义需求就用这个方法
//        return super.getResources();
//    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_hxkt_shouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        mFragmentManager = getSupportFragmentManager();
        // 解决fragment布局重叠错乱
        if (savedInstanceState != null) {
            mFragment1 = (ShouyeFragment) mFragmentManager.findFragmentByTag(LIST_TAG1);
            mFragment2 = (ZnxuexiFragment) mFragmentManager.findFragmentByTag(LIST_TAG2);
        }
        doNetWork();
    }

    private void doNetWork() {
        mList = new ArrayList<>();
        addList();
        recyclerView.setLayoutManager(new GridLayoutManager(this, mList.size(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();
        current_pos = 0;
        footer_onclick();
    }

    //点击item
    private void footer_onclick() {
        final ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(current_pos);
        if (model.isEnselect()) {
            // 不切换当前的item点击 刷新当前页面
            showFragment(model.getText_id(), true);
        } else {
            // 切换到另一个item
            if (model.getText_id().equalsIgnoreCase(id2)) {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            } else {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            }
        }
    }

    private void set_footer_change(ShouyeFooterBean model) {
        //设置为选中
        initList();
        model.setEnselect(true);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();

    }

    private void findview() {
        recyclerView = findViewById(R.id.recycler_view1);
        tvSubject = (TextView) findViewById(R.id.tv_subject);
        tvName = findViewById(R.id.tv_name);
        shouyeHead = findViewById(R.id.shouye_head);
        mAdapter = new ShouyeFooterAdapter(this);
        LoginBean.DataBean data = UserController.getLoginBean().getData();
        if (data == null) {
            return;
        } else {
            if (!TextUtils.isEmpty(data.getUser().getRealName())) {
                tvName.setText(data.getUser().getRealName());
            }
            if (!TextUtils.isEmpty(data.getUser().getPhoto())) {
                Glide.with(App2.get())
                        .load(data.getUser().getPhoto())
                        .error(R.mipmap.mrtx_img)
                        .transform(new CircleTransform(App2.get())).into(shouyeHead);
            }
        }
    }

    private void onclick() {
        mAdapter.setOnItemClickLitener(new ShouyeFooterAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item
                current_pos = position;
                footer_onclick();
            }
        });
        tvSubject.setOnClickListener(this);
        shouyeHead.setOnClickListener(this);
    }

    private void addList() {
        mList.add(new ShouyeFooterBean(id1, "我的首页", true));
        mList.add(new ShouyeFooterBean(id2, "AI智能学习", false));
    }

    private void initList() {
        for (int i = 0; i < mList.size(); i++) {
            ShouyeFooterBean item = mList.get(i);
            if (item.isEnselect()) {
                item.setEnselect(false);
            }
        }
    }

    private void showFragment(final String tag, final boolean isrefresh) {
        tag_ids = tag;
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragments(transaction);

        if (tag.equalsIgnoreCase("-1")) { //
//            if (mFragment1 == null) {
//                mFragment1 = new FragmentContent1();
//                transaction.add(R.id.container, mFragment1, LIST_TAG0);
//            } else {
//                transaction.show(mFragment1);
//                mFragment1.initData();
//            }
        } else if (tag.equalsIgnoreCase(id1)) {
            if (mFragment1 == null) {
                mFragment1 = new ShouyeFragment();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment1.setArguments(args);
                transaction.add(R.id.container, mFragment1, LIST_TAG1);
            } else {
                transaction.show(mFragment1);
                mFragment1.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id2)) {
            if (mFragment2 == null) {
                mFragment2 = new ZnxuexiFragment();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment2.setArguments(args);
                transaction.add(R.id.container, mFragment2, LIST_TAG2);
            } else {
                transaction.show(mFragment2);
                mFragment2.getCate(tag, isrefresh);
            }
        }
        transaction.commitAllowingStateLoss();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mFragment1 != null) {
            transaction.hide(mFragment1);
            mFragment1.give_id(tag_ids);
        }
        if (mFragment2 != null) {
            transaction.hide(mFragment2);
            mFragment2.give_id(tag_ids);
        }
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {

    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private long exitTime;

    /**
     * 连按两次退出程序
     */
    private void exit() {
        ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(0);
        if (model != null && !tag_ids.equals(model.getText_id())) {
            set_footer_change(model);
            showFragment(model.getText_id(), false);
        } else {
            if ((System.currentTimeMillis() - exitTime) < 1500) {
//                RichText.recycle();
                ActivityManager.exit();
//                BaseAppManager.getInstance().closeApp();
            } else {
                ToastUtils.showShort("再按一次退出程序 ~");
                exitTime = System.currentTimeMillis();
            }
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_subject) {//个人中心
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            startActivity(intent);
        } else if (id == R.id.shouye_head) {//头像
            new XPopup.Builder(MainActivityIndex.this)
                    //.dismissOnBackPressed(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .autoOpenSoftInput(true)
//                        .autoFocusEditText(false) //是否让弹窗内的EditText自动获取焦点，默认是true
                    .isRequestFocus(false)
                    //.moveUpToKeyboard(false)   //是否移动到软键盘上面，默认为true
                    .asInputConfirm("请输入要测试的地址", null, SPUtils.getInstance().getString("url", "http://49.4.7.45:8090/#/"), "默认Hint文字",
                            new OnInputConfirmListener() {
                                @Override
                                public void onConfirm(String text) {
                                    SPUtils.getInstance().put("url", text);
                                }
                            })
                    .show();
        }
    }
}
