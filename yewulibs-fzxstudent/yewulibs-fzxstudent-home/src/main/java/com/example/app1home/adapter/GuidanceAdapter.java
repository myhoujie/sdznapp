package com.example.app1home.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * GuidanceAdapter〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class GuidanceAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;

    public GuidanceAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setData(List<Fragment> l) {
        this.list = l;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
}
