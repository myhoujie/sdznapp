package com.example.app1home.aistudent.shouye;

import com.sdzn.fzx.student.bean.HCategoryBean1;

import java.io.Serializable;
import java.util.List;

public class HCategoryKtxxBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<HKtxxBean> list;//

    public HCategoryKtxxBean() {
    }

    public HCategoryKtxxBean(List<HKtxxBean> list) {
        this.list = list;
    }

    public List<HKtxxBean> getList() {
        return list;
    }

    public void setList(List<HKtxxBean> list) {
        this.list = list;
    }
}
