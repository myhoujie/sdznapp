package com.example.app1home.aistudent.shouyeznxx;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutznxuexidapter;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupAnimation;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.interfaces.XPopupCallback;
import com.sdzn.fzx.student.bean.HCategoryBean;
import com.sdzn.fzx.student.bean.HCategoryBean1;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebFragment1;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * AI智囊学习首页
 *
 * @author houjie
 * @date 2020/09/16
 */
public class ZnxuexiFragment extends BaseActNoWebFragment1 implements View.OnClickListener {
    private XRecyclerView recyclerViewZnxuexi;
    private ViewPagerSlide viewpagerViewZnxuexi;
    private ImageView iv_sousuo;
    private RelativeLayout rl_subject;
    private TextView tv_subject;
    private ImageView iv_xktupian;
    private View arrowIvXk;

    private String current_id;
    private Tablayoutznxuexidapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_znxuexi;
    }

    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
        recyclerViewZnxuexi = rootView.findViewById(R.id.recycler_view_znxuexi);
        viewpagerViewZnxuexi = rootView.findViewById(R.id.viewpager_view_znxuexi);
        iv_sousuo = rootView.findViewById(R.id.iv_sousuo);
        tv_subject = rootView.findViewById(R.id.tv_subject);
        iv_xktupian = rootView.findViewById(R.id.iv_xktupian);
        rl_subject = rootView.findViewById(R.id.rl_subject);
        arrowIvXk = rootView.findViewById(R.id.arrowIv_xueke);
        iv_sousuo.setOnClickListener(this);
        rl_subject.setOnClickListener(this);
        onclick();
        donetwork();
    }

    @Override
    public void RefreshLoad() {
        loadData();
    }

    @Override
    public void loadData() {
        init_viewp(mDataTablayout);
//        Log.e("---geekyun----", current_id);
    }

    public void donetwork() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "错题本"));
        mDataTablayout1.add(new HCategoryBean1("2", "我的笔记"));
//        mDataTablayout1.add(new HCategoryBean1("3", "试题收藏"));
//        mDataTablayout1.add(new HCategoryBean1("4", "资源收藏"));
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true, hCategoryBean.getList().get(i).getIcon()));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false, hCategoryBean.getList().get(i).getIcon()));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);

    }


    private void onclick() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerViewZnxuexi.setLayoutManager(new GridLayoutManager(getActivity(), 4, RecyclerView.VERTICAL, false));
        //禁用滑动
        gridLayoutManager.setSmoothScrollbarEnabled(false);
        recyclerViewZnxuexi.setLayoutManager(gridLayoutManager);
        recyclerViewZnxuexi.setNestedScrollingEnabled(false);
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutznxuexidapter();
        recyclerViewZnxuexi.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                if (current_id.equals("1")) {
                    rl_subject.setVisibility(View.GONE);
                } else if (current_id.equals("2")) {
                    rl_subject.setVisibility(View.VISIBLE);
                }
//                else if (current_id.equals("3")) {
//                    rl_subject.setVisibility(View.VISIBLE);
//                } else if (current_id.equals("4")) {
//                    rl_subject.setVisibility(View.VISIBLE);
//                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerViewZnxuexi.setCurrentItem(position, true);
            }
        });
    }

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                CtbFragment fragment1 = FragmentHelper.newFragment(CtbFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
//                bundle.putString("url_key", "file:///android_asset/js_aiznxx/wdbj.html");
                bundle.putString("url_key", SPUtils.getInstance().getString("url", "http://49.4.7.45:8090/#/") + "myNotes/index");
                WdbjFragment fragment1 = FragmentHelper.newFragment(WdbjFragment.class, bundle);
                mFragmentList.add(fragment1);
            }
//            else if (i == 2) {
//                bundle.putString("url_key", "file:///android_asset/js_aiznxx/stsc.html");
//                StscFragment fragment1 = FragmentHelper.newFragment(StscFragment.class, bundle);
//                mFragmentList.add(fragment1);
//            } else {
//                bundle.putString("url_key", "file:///android_asset/js_aiznxx/zysc.html");
//                ZyscFragment fragment1 = FragmentHelper.newFragment(ZyscFragment.class, bundle);
//                mFragmentList.add(fragment1);
//            }
        }
//        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(getChildFragmentManager(), getActivity(), mFragmentList);
        viewpagerViewZnxuexi.setAdapter(orderFragmentPagerAdapter);
        viewpagerViewZnxuexi.setOffscreenPageLimit(4);
        viewpagerViewZnxuexi.setScroll(true);//控制是否滑动
        viewpagerViewZnxuexi.setCurrentItem(0, false);
        viewpagerViewZnxuexi.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                OneBean1 bean1 = mAdapter11.getData().get(position);
                if (bean1.getTab_id().equals("1")) {
                    rl_subject.setVisibility(View.GONE);
                } else if (bean1.getTab_id().equals("2")) {
                    rl_subject.setVisibility(View.VISIBLE);
                }
//                else if (bean1.getTab_id().equals("3")) {
//                    rl_subject.setVisibility(View.VISIBLE);
//                } else if (bean1.getTab_id().equals("4")) {
//                    rl_subject.setVisibility(View.VISIBLE);
//                }
                set_footer_change(bean1);
            }
        });
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }

    private boolean is_onclik;
    private int checkedPos = 4;//默认选中 学科
    final String[] strArray = {"语文", "数学", "英语", "历史", "生物", "物理", "政治", "地理", "化学"};
    final int[] intArray = {R.drawable.new_icon_yuwen, R.drawable.new_icon_shuxue, R.drawable.new_icon_yingyu, R.drawable.new_icon_lishi, R.drawable.new_icon_shengwu,
            R.drawable.new_icon_wuli, R.drawable.new_icon_zhengzhi, R.drawable.new_icon_dili, R.drawable.new_icon_huaxue};

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_sousuo) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxSearchActivity");
            startActivity(intent);
        } else if (i == R.id.rl_subject) {
            if (!is_onclik) {
                arrowIvXk.setRotation(180);
                is_onclik = true;
            } else {
                arrowIvXk.setRotation(0);
                is_onclik = false;
            }
            new XPopup.Builder(getActivity())
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .popupAnimation(PopupAnimation.ScaleAlphaFromCenter)
                    .setPopupCallback(new XPopupCallback() {
                        @Override
                        public void onCreated(BasePopupView popupView) {

                        }

                        @Override
                        public void beforeShow(BasePopupView popupView) {

                        }

                        @Override
                        public void onShow(BasePopupView popupView) {

                        }

                        @Override
                        public void onDismiss(BasePopupView popupView) {
                        }

                        @Override
                        public void beforeDismiss(BasePopupView popupView) {

                        }

                        @Override
                        public boolean onBackPressed(BasePopupView popupView) {
                            return false;
                        }
                    })

                    .offsetY(30)
                    .offsetX(-60)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                    .atView(rl_subject)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                    .asAttachList1(strArray,
                            intArray,
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
//                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    tv_subject.setText(strArray[position]);
                                    iv_xktupian.setImageResource(intArray[position]);
                                    arrowIvXk.setRotation(0);
                                    is_onclik = false;
                                }
                            }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                    .show();
        }
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }
}
