package com.example.app1home.aistudent.wdpg;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.R;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutdapter;
import com.example.app1home.aistudent.AndroidInterface;
import com.just.agentweb.LocalBroadcastManagers;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.sdzn.fzx.student.bean.HCategoryBean;
import com.sdzn.fzx.student.bean.HCategoryBean1;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.libbase.ai.pop.CustomDrawerPopupView;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

/*
 * 自主学习主页
 * */
public  class PgDetailsShouyeActivity extends BaseActNoWebActivity1 implements BaseOnClickListener {
    private ViewPagerSlide viewpagerBaseactTablayout;
    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    private MessageReceiverIndex mMessageReceiver;
    private int defCurrentItem = 0;//设置默认见面

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("PgDetailsShouyeActivity".equals(intent.getAction())) {
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_zzxxshouye;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        viewpagerBaseactTablayout = findViewById(R.id.viewpager_baseact_tablayout);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("PgDetailsShouyeActivity");
        LocalBroadcastManagers.getInstance(this).registerReceiver(mMessageReceiver, filter);
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        if ("1".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 0;
        } else if ("2".equals(getIntent().getStringExtra("show"))) {
            defCurrentItem = 1;
        }
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        initviw();
    }

    private void initviw() {
        tvTitleName.setText("批改");
        onclick();
        donetworkdemo();
    }

    private void donetworkdemo() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "按学生批改"));
        mDataTablayout1.add(new HCategoryBean1("2", "按题目批改"));
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(defCurrentItem).getTab_id();

        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);

        if (1 == defCurrentItem) {
            changeEnable(defCurrentItem);
        }
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        //
        recyclerViewTitle.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerViewTitle.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerBaseactTablayout.setCurrentItem(position, true);
            }
        });
    }

    public static final String URL_KEY = "url_key";

    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                bundle.putString(URL_KEY, "file:///android_asset/js_pg/studentpg.html");
//                bundle.putString(URL_KEY, "http://49.4.7.45:8080/#/autonomicLearningParent/autonomicLearning/oneselfStudy");
                StudentpgFragment fragment1 = FragmentHelper.newFragment(StudentpgFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                bundle.putString(URL_KEY, "file:///android_asset/js_pg/timupg.html");
//                bundle.putString(URL_KEY, "http://49.4.7.45:8080/#/autonomicLearningParent/autonomicLearning/publishRecord");
                TimupgFragment fragment2 = FragmentHelper.newFragment(TimupgFragment.class, bundle);
                mFragmentList.add(fragment2);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerBaseactTablayout.setAdapter(orderFragmentPagerAdapter);
        viewpagerBaseactTablayout.setOffscreenPageLimit(4);
        viewpagerBaseactTablayout.setScroll(true);
        viewpagerBaseactTablayout.setCurrentItem(defCurrentItem, true);
        viewpagerBaseactTablayout.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                changeEnable(position);
            }
        });
    }

    private void changeEnable(int position) {
        OneBean1 bean1 = mAdapter11.getData().get(position);
        set_footer_change(bean1);
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManagers.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdkcSouSuoActivity");
        intent.putExtra("show", "str");
        startActivity(intent);
    }

    @Override
    public void TitleFabupg() {
        new XPopup.Builder(PgDetailsShouyeActivity.this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .asConfirm("发布批改", "点击任意按钮后，将把当前的批改情况发布给",
                        "结束批改", "稍后再批",
                        new OnConfirmListener() {
                            @Override
                            public void onConfirm() {
                                ToastUtils.showShort("click confirm");
                            }
                        }, null, false, com.sdzn.fzx.student.libbase.R.layout.popup_center_text) //最后一个参数绑定已有布局
                .show();
    }

    @Override
    public void Titlegrzx() {
        CustomDrawerPopupView customDrawerPopupView = new CustomDrawerPopupView(this, new CustomDrawerPopupView.GrzxNextCallBack() {
            @Override
            public void toGrzxNextClick() {
                ToastUtils.showShort("个人-----");

            }
        });
        new XPopup.Builder(this)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                .asCustom(customDrawerPopupView)
                .show();
    }
}