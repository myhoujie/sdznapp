package com.example.app1home.aistudent.shouye;

import java.io.Serializable;

public class OneKtxx1 implements Serializable {
    private String tab_id;
    private String tab_name;
    private int tab_icon;
    private boolean enable;
    private String subject;//学科
    private String time;//时间
    private int frame;//边框
    private String subjectpicture;
    private String subjectact;



    public OneKtxx1() {
    }

    public OneKtxx1(String tab_id, String tab_name, boolean enable, int tab_icon) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
        this.tab_icon = tab_icon;
    }

    public OneKtxx1(String tab_id, String tab_name, boolean enable) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.enable = enable;
    }

    public OneKtxx1(String tab_id, String tab_name, String subject, String time, int tab_icon) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.subject = subject;
        this.time = time;
        this.tab_icon = tab_icon;
    }
    public OneKtxx1(String tab_id, String tab_name, String subject, String time, String subjectpicture, String subjectact) {
        this.tab_id = tab_id;
        this.tab_name = tab_name;
        this.subject = subject;
        this.time = time;
        this.subjectpicture = subjectpicture;
        this.subjectact = subjectact;
    }


    public int getTab_icon() {
        return tab_icon;
    }

    public void setTab_icon(int tab_icon) {
        this.tab_icon = tab_icon;
    }

    public String getTab_id() {
        return tab_id;
    }

    public void setTab_id(String tab_id) {
        this.tab_id = tab_id;
    }

    public String getTab_name() {
        return tab_name;
    }

    public void setTab_name(String tab_name) {
        this.tab_name = tab_name;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }

    public String getSubjectpicture() {
        return subjectpicture;
    }

    public void setSubjectpicture(String subjectpicture) {
        this.subjectpicture = subjectpicture;
    }

    public String getSubjectact() {
        return subjectact;
    }

    public void setSubjectact(String subjectact) {
        this.subjectact = subjectact;
    }
}
