package com.example.app1home.aistudent.hztj;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.ToastUtils;
import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

/**
 * 合作探究
 */
public  class HZTJActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        TitleShowHideState(6);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
        }
    }

//    @Nullable
//    public String getUrl() {
//        return URL + "cooperation";
//    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        setTitleContent(title, "合作探究");
    }

    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
        mAgentWeb.getWebCreator().getWebView().loadUrl(URL + "cooperation");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    private int checkedPos = -1;

    @Override
    public void Shaixuananniu() {
        new XPopup.Builder(HZTJActivity.this)
                .hasShadowBg(false)
                .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                .offsetY(20)
                .offsetX(40)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                .atView(tvShaiXuan)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                .asAttachList1(new String[]{"进行中′", "已完成′", "待完成"},
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                checkedPos = position;
                                ToastUtils.showShort("click " + text + "\n pos" + position);
                            }
                        }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                .show();
    }
}
