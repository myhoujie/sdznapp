package com.example.app1home.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.example.app1home.R;
import com.example.app1home.activity.WelcomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class GuidanceFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "tag";
    //引导图片资源
    public static final int[] pics = {R.mipmap.guidance_1,
            R.mipmap.guidance_2, R.mipmap.guidance_3};
    private ImageView guidanceImg;
    private Button guidanceBtn;

    public GuidanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guidance, container, false);
        guidanceImg = (ImageView) view.findViewById(R.id.guidance_img);
        guidanceBtn = (Button) view.findViewById(R.id.guidance_btn);
        guidanceBtn.setOnClickListener(this);
        initData();
        return view;
    }

    void initData() {
        final int pos = getArguments().getInt(TAG);
        guidanceImg.setImageResource(pics[pos]);
        if (pos == pics.length - 1) {
            guidanceBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.guidance_btn) {
            ((WelcomeActivity) getActivity()).jump();
        }
    }
}
