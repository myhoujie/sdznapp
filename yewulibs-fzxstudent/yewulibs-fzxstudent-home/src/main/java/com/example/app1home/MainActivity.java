package com.example.app1home;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1grzx.fragment.MeFragment;
import com.example.app1home.fragment.MainFragment;
import com.example.app1xqfx.fragment.AnalyzeSurplusFragment;
import com.example.app1xxjn.fragment.StudyPageFragment;
import com.example.app1xxrw.fragment.TaskFragment;
import com.example.app1xztl.fragment.GroupDiscussionFragment;
import com.example.app1zwts.fragment.SelfImprovementFragment;
import com.sdzn.fzx.student.BuildConfig2;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.dao.controller.UserController;
import com.sdzn.fzx.student.libbase.app.BLEServiceManger;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.app.SchoolBoxWatcher;
import com.sdzn.fzx.student.libbase.base.ActivityManager;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libbase.base.BaseProgressCallBack;
import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.msg.MqttService;
import com.sdzn.fzx.student.libbase.service.BluetoothLEService;
import com.sdzn.fzx.student.libbase.service.DownService;
import com.sdzn.fzx.student.libpublic.event.Event;
import com.sdzn.fzx.student.libpublic.event.TinkerResult;
import com.sdzn.fzx.student.libpublic.event.ToTaskEvent;
import com.sdzn.fzx.student.libpublic.event.ToWrongEvent;
import com.sdzn.fzx.student.libpublic.views.CustomDialog;
import com.sdzn.fzx.student.libpublic.views.UpdateDialog;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.AndroidUtil;
import com.sdzn.fzx.student.libutils.util.FileUtil;
import com.sdzn.fzx.student.libutils.util.ProgressDialogManager;
import com.sdzn.fzx.student.libutils.util.ServiceUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.HotFixVo;
import com.sdzn.fzx.student.vo.UpdateVo;
import com.tencent.tinker.lib.tinker.TinkerInstaller;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends MBaseActivity<BasePresenter> implements BaseView, RadioGroup.OnCheckedChangeListener {

    private RelativeLayout netErrorRy;
    private RadioGroup radioGroup;
    private List<BaseFragment> fragments;

    private int containerId = R.id.framelayoutMain;

    private Fragment currFragment;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    ProgressDialogManager manager;

    private static boolean isExit;// 标示是否退出
    private RadioButton rbMain, rbTask, rbPak, rbGroup, rbAnalyze, rbTisheng, rbMe;

    @Override
    public void initPresenter() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //删除保存的Fragment，也可以重写onSaveInstanceState方法不让其保存，目的是防止activity被回收造成fragment数据或栈异常
        if (savedInstanceState != null) {
            savedInstanceState.putParcelable("android:support:fragments", null);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rbMain = findViewById(R.id.rbMain);
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        manager = new ProgressDialogManager(MainActivity.this);
        initFragment();
        initView();
        checkMqttAndSchoolBox();
        saveStudentVersion();
        checkUpdate();
        SchoolBoxWatcher.getInstance().getQiNiuDomain();//登录成功后获取七牛domain
        BLEServiceManger.getInstance().bindService(this);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(MainActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到loginactivity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入mainactivity
                        ToastUtils.showLong("进入mainActivity成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    private void checkMqttAndSchoolBox() {
        android.app.ActivityManager manager = (android.app.ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<android.app.ActivityManager.RunningServiceInfo> list = manager.getRunningServices(30);
        for (android.app.ActivityManager.RunningServiceInfo info : list) {
            if (MqttService.class.getName().equals(info.service.getClassName())) {
                return;
            }
        }
        Intent intent = new Intent(MainActivity.this, MqttService.class);
        startService(intent);
//        EventBus.getDefault().post(Event.ONLOGIN_SUCCESS);
    }

    @Override
    protected void initView() {
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    protected void initData() {

    }

    //通过event 首页切换到学习任务
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openFragment(Event event) {
        if (event instanceof ToTaskEvent) {
            ToTaskEvent toTaskEvent = (ToTaskEvent) event;
            TaskFragment taskFragment = (TaskFragment) fragments.get(1);
            taskFragment.setCheck(toTaskEvent.isCheck());
            radioGroup.check(R.id.rbTask);
        } else if (event instanceof ToWrongEvent) {
            radioGroup.check(R.id.rbPak);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (netErrorRy == null)
            return;
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbMain) {
            showAssignedFragment(0);
        } else if (checkedId == R.id.rbTask) {
            showAssignedFragment(1);
        } else if (checkedId == R.id.rbPak) {
            showAssignedFragment(2);
        } else if (checkedId == R.id.rbAnalyze) {
            showAssignedFragment(3);
        } else if (checkedId == R.id.rbTisheng) {
            showAssignedFragment(4);
        } else if (checkedId == R.id.rbMe) {
            showAssignedFragment(5);
        } else if (checkedId == R.id.rbGroup) {
            showAssignedFragment(6);
        }
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(MainFragment.newInstance(null));//首页
        fragments.add(TaskFragment.newInstance(null));//学习任务
        fragments.add(StudyPageFragment.newInstance(null));// 学习锦囊
//        fragments.add(AnalyzeFragment.newInstance(null));
        fragments.add(AnalyzeSurplusFragment.newInstance(null));//学情分析
        fragments.add(SelfImprovementFragment.newInstance(null));//自我提升
        fragments.add(MeFragment.newInstance(null));//个人中心
        fragments.add(GroupDiscussionFragment.newInstance(null));//小组讨论主界面
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        BaseFragment fragment = fragments.get(fragmentIndex);
        if (currFragment != fragment) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (!fragment.isAdded()) {
                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
            } else {
                ft.hide(currFragment).show(fragment);
            }
            currFragment = fragment;
            ft.commit();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 连按两次退出程序
     */
    public void exit() {
        if (!isExit) {
            isExit = true;
            ToastUtil.showShortlToast("再按一次退出程序");
            MyHandler myHandler = new MyHandler();
            myHandler.sendEmptyMessageDelayed(100, 2000);
        } else {
            //9.26 增加调用退出登录接口
            //友盟退出登录
            BluetoothLEService service = BLEServiceManger.getInstance().getService();
            if (service != null) {
                service.removeAllDataReceiverListener();
            }
            stopService(new Intent(this, MqttService.class));
            ActivityManager.exit();
        }
    }

    private static class MyHandler extends Handler {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            switch (msg.what) {
                case 100:
                    isExit = false;
                    break;
                default:
                    break;
            }
        }
    }

    private ServiceConnection conn;

    private void saveStudentVersion() {
        String versionName;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }
//        Network.createTokenService(NetWorkService.MainService.class)
//                .saveStudentVersion(StudentSPUtils.getLoginBean().getData().getId(), versionName)
//                .map(new StatusFunc<Object>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<Object>(new SubscriberListener<Object>() {
//                    @Override
//                    public void onNext(Object o) {
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                    }
//
//                    @Override
//                    public void onCompleted() {
//                    }
//                }, this, false));
    }

    public void checkUpdate() {
        Map<String, String> params = new HashMap<>();
        params.put("appVersionNum", String.valueOf(AndroidUtil.getVerCode(App2.get())));
        params.put("programName", "辅助线学生端APP");
        params.put("customerSchoolId", UserController.getSchoolId());
        Network.createTokenService(NetWorkService.MainService.class)
                .checkUpdate(params)
                .map(new StatusFunc<UpdateVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UpdateVo>(new SubscriberListener<UpdateVo>() {
                    @Override
                    public void onNext(UpdateVo updateVo) {
                        if (updateVo != null && updateVo.getData() != null && updateVo.getData().getAppVersionNum() > AndroidUtil.getVerCode(App2.get())) {
                            downloadApk(updateVo);
                        } else {
                            checkHotFix();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onCompleted() {
                    }
                }, MainActivity.this, false, true, false, ""));
    }

    private void checkHotFix() {
        Network.createTokenService(NetWorkService.MainService.class)
                .checkHotfix(2)
                .map(new StatusFunc<HotFixVo>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<HotFixVo>(new SubscriberListener<HotFixVo>() {
                    @Override
                    public void onNext(HotFixVo hotFixVo) {
                        if (hotFixVo != null && hotFixVo.getData() != null &&
                                hotFixVo.getData().getAppVersionNum() > BuildConfig2.TINKER_VERSION) {
                            downloadHotFix(hotFixVo.getData().getAppPath());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, MainActivity.this, false, true, false, ""));
    }

    private void downloadHotFix(String url) {
        File dir = new File(FileUtil.getAppFilesDir() + "/sdzn/student");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final File file = new File(dir, "hotfix.fix");
        if (file.exists()) {
            FileUtil.delete(file);
        }
        RequestParams params = new RequestParams(url);
        //设置断点续传
        params.setAutoResume(true);
        params.setSaveFilePath(file.getAbsolutePath());
        x.http().get(params, new BaseProgressCallBack<File>() {
            @Override
            public void onSuccess(File result) {
                if (result.getAbsoluteFile().exists()) {
                    manager.showWaiteDialog("正在安装补丁, 请稍后...", false);
                    TinkerInstaller.onReceiveUpgradePatch(MainActivity.this, result.getAbsolutePath());
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                ToastUtil.showShortlToast("下载补丁包失败");
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTinkerCallback(TinkerResult event) {
        if (manager == null) {
            return;
        }
        manager.cancelWaiteDialog();
        ToastUtil.showShortlToast(event.success ? "补丁安装成功, 请重启应用" : "补丁安装失败");
        if (event.cost == 0) {
            File dir = new File(FileUtil.getAppFilesDir() + "/sdzn/student");
            File file = new File(dir, "hotfix.fix");
            file.delete();
        }
    }

    private ProgressDialog pd;
    private UpdateVo vo;

    private void downloadApk(final UpdateVo updateVo) {
        vo = updateVo;
        // 需要更新，弹出更新窗口
        UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
        if (!TextUtils.isEmpty(updateVo.getData().getAppVersionDesc())) {
            builder.setMessage(updateVo.getData().getAppVersionDesc());
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 下载更新包
                Intent intent = new Intent(App2.get(), DownService.class);
                intent.putExtra(DownService.BOX_PATH, updateVo.getData().getBoxPath());
                intent.putExtra(DownService.APP_PATH, updateVo.getData().getAppPath());
                if (ServiceUtil.isServiceRunning(App2.get(), DownService.class.getName())) {
                    App2.get().stopService(intent);
                }
                App2.get().startService(intent);

                conn = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        final DownService.DownBinder downBinder = (DownService.DownBinder) service;
                        DownService downService = downBinder.getService();
                        downService.setOnProgressLintener(new DownService.OnProgressListener() {
                            @Override
                            public void onProgressChanged(int progress) {
                                if (pd != null) {
                                    pd.setProgress(progress);
                                }
                            }

                            @Override
                            public void onFailed(@Nullable Throwable e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                                if (updateVo.getData().getMustUpgrade() == 1) {
                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            ToastUtil.showShortlToast("强制更新失败, 应用即将退出");
                                            stopService(new Intent(MainActivity.this, MqttService.class));
                                            ActivityManager.exit();

                                        }
                                    }, 3000);
                                }
                            }

                            @TargetApi(26)
                            @Override
                            public void requestPermission(Uri data) {
                                downloadUri = data;
                                startInstallPermissionSettingActivity();
                            }
                        });
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {

                    }
                };
                App2.get().bindService(intent, conn, Service.BIND_AUTO_CREATE);
                // 如果是强制升级，显示一个进度条
                if (updateVo.getData().getMustUpgrade() == 1) {
                    pd = new ProgressDialog(MainActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
            }
        }).setNegative(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (updateVo.getData().getMustUpgrade() == 1) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtil.showShortlToast("该版本强制更新, 应用即将退出");
                            stopService(new Intent(MainActivity.this, MqttService.class));
                            ActivityManager.exit();
                        }
                    }, 3000);
                } else {
                    dialog.dismiss();
                }
            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private Uri downloadUri;

    public void installApk(boolean isok) {
        if (downloadUri == null) {
            return;
        }
        if (!isok) {
            if (vo.getData().getMustUpgrade() == 1) {
                //request
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                return;
                //return
            }
        }
        //install
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(downloadUri, "application/vnd.android.package-archive");
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {//注意这个是8.0新API
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(intent, REQUEST_INSTALL_PERMISSION);
    }

    public static final int REQUEST_INSTALL_PERMISSION = 13131;

    @Override
    protected void onActResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_INSTALL_PERMISSION) {
            installApk(resultCode == RESULT_OK);
        }
        super.onActResult(requestCode, resultCode, data);

    }

    /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_INSTALL_PERMISSION) {
            installApk(resultCode == RESULT_OK);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        BLEServiceManger.getInstance().destroyManger(this);
        super.onDestroy();
    }


}
