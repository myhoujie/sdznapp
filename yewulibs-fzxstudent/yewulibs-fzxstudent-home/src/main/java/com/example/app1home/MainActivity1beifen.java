package com.example.app1home;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.app1home.adapter.OrderFragmentPagerAdapter;
import com.example.app1home.adapter.Tablayoutdapter;
import com.example.app1home.aistudent.shouye.ShouyeFragment;
import com.example.app1home.aistudent.shouyeznxx.ZnxuexiFragment;
import com.sdzn.fzx.student.bean.HCategoryBean;
import com.sdzn.fzx.student.bean.HCategoryBean1;
import com.sdzn.fzx.student.bean.OneBean1;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActNoWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.FragmentHelper;
import com.sdzn.fzx.student.libpublic.views.RoundAngleImageView;
import com.sdzn.fzx.student.libpublic.views.ViewPagerSlide;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.AutoSizeCompat;


public class MainActivity1beifen extends BaseActNoWebActivity1 implements View.OnClickListener, CheckverionViews {
    private XRecyclerView recyclerView1Order11;
    private ViewPagerSlide viewpagerMy1Orderf2;
    private TextView tvSubject;//个人中心
    private TextView tvName;//首页名称
    private RoundAngleImageView shouyeHead;//学生头像

    private String current_id;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    private int defCurrentItem = 0;//设置默认见面

    CheckverionFzxPresenter checkverionFzxPresenter;


//    @Override
//    public LayoutParams generateLayoutParams(AttributeSet attrs) {
//        AutoSizeCompat.autoConvertDensityOfGlobal((getResources());//如果没有自定义需求用这个方法
//        AutoSizeCompat.autoConvertDensity((getResources(), 667, false);//如果有自定义需求就用这个方法
//        return super.generateLayoutParams(attrs);
//    }

//    @Override
//    public Resources getResources() {
//        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
//        AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources());//如果没有自定义需求用这个方法
//        AutoSizeCompat.autoConvertDensity(super.getResources(), 800, false);//如果有自定义需求就用这个方法
//        return super.getResources();
//    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main1;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        recyclerView1Order11 = (XRecyclerView) findViewById(R.id.recycler_view1_order11);
        viewpagerMy1Orderf2 = (ViewPagerSlide) findViewById(R.id.viewpager_my1_orderf2);
        tvSubject = (TextView) findViewById(R.id.tv_subject);
        tvName = findViewById(R.id.tv_name);
        shouyeHead = findViewById(R.id.shouye_head);
        tvSubject.setOnClickListener(this);
        shouyeHead.setOnClickListener(this);

        onclick();
        donetwork1();
        checkverionFzxPresenter = new CheckverionFzxPresenter();
        checkverionFzxPresenter.onCreate(this);
        checkverionFzxPresenter.checkVerion("2", "0");
    }


    public void donetwork1() {
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "我的首页"));
        mDataTablayout1.add(new HCategoryBean1("2", "AI智能学习"));
        hCategoryBean.setList(mDataTablayout1);
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true, hCategoryBean.getList().get(i).getIcon()));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false, hCategoryBean.getList().get(i).getIcon()));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        recyclerView1Order11.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerView1Order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerMy1Orderf2.setCurrentItem(position, true);
            }
        });
    }


    private void init_viewp(List<OneBean1> mlist) {
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                ShouyeFragment fragment1 = FragmentHelper.newFragment(ShouyeFragment.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                ZnxuexiFragment fragment1 = FragmentHelper.newFragment(ZnxuexiFragment.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(this.getSupportFragmentManager(), this, mFragmentList);
        viewpagerMy1Orderf2.setAdapter(orderFragmentPagerAdapter);
        viewpagerMy1Orderf2.setOffscreenPageLimit(2);
        viewpagerMy1Orderf2.setScroll(true);//控制是否滑动
        viewpagerMy1Orderf2.setCurrentItem(defCurrentItem, false);
        viewpagerMy1Orderf2.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                OneBean1 bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }

    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        checkverionFzxPresenter.onDestory();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_subject || id == R.id.shouye_head) {//个人中心
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MeActivity");
            startActivity(intent);
        }
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {

    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    public void net_con_none() {

    }

    @Override
    public void net_con_success() {

    }

    @Override
    public void showNetPopup() {

    }
}
