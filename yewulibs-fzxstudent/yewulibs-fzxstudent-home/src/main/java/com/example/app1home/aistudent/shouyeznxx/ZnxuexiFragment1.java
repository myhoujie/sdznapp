package com.example.app1home.aistudent.shouyeznxx;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.app1home.R;
import com.sdzn.fzx.student.libpublic.views.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * AI智囊学习首页
 *
 * @author houjie
 * @date 2020/09/16
 */
public class ZnxuexiFragment1 extends Fragment implements View.OnClickListener {
    private XRecyclerView recyclerViewZnxuexi;
    private TextView tvCtb;//错题本
    private TextView tvWdbj;//我的笔记
    private TextView tvStsc;//试题收藏
    private TextView tvZysc;//资源收藏


    private List<Fragment> fragments;
    private Fragment currFragment;
//    private int containerId = R.id.framelayout_shouyeaiznxx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_znxuexi, container, false);
        recyclerViewZnxuexi=view.findViewById(R.id.recycler_view_znxuexi);
//        tvCtb = view.findViewById(R.id.tv_ctb);
//        tvWdbj = view.findViewById(R.id.tv_wdbj);
//        tvStsc = view.findViewById(R.id.tv_stsc);
//        tvZysc = view.findViewById(R.id.tv_zysc);

        tvCtb.setOnClickListener(this);
        tvWdbj.setOnClickListener(this);
        tvStsc.setOnClickListener(this);
        tvZysc.setOnClickListener(this);

        initFragment();
        return view;
    }

    Bundle bundle = new Bundle();
    Bundle bundle1 = new Bundle();
    Bundle bundle2 = new Bundle();

    private void initFragment() {
//        bundle.putString("url_key", "file:///android_asset/js_aiznxx/wdbj.html");
//        bundle1.putString("url_key", "file:///android_asset/js_aiznxx/stsc.html");
//        bundle2.putString("url_key", "file:///android_asset/js_aiznxx/zysc.html");
//        fragments = new ArrayList<>();
//        fragments.add(CtbFragment.newInstance(null));
//        fragments.add(WdbjFragment.newInstance(bundle));
//        fragments.add(StscFragment.newInstance(bundle1));
//        fragments.add(ZyscFragment.newInstance(bundle2));
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
//        ft.replace(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        currFragment = fragment;
//        ft.replace(containerId, fragment);/**/
        ft.commit();
    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
//        if (i == R.id.tv_ctb) {//错题本
//            showAssignedFragment(0);
//            initBgAndIc();
//            tvCtb.setTextColor(Color.parseColor("#FA541C"));
//        } else if (i == R.id.tv_wdbj) {//我的笔记
//            showAssignedFragment(1);
//            initBgAndIc();
//            tvWdbj.setTextColor(Color.parseColor("#FA541C"));
//        } else if (i == R.id.tv_stsc) {//试题收藏
//            showAssignedFragment(2);
//            initBgAndIc();
//            tvStsc.setTextColor(Color.parseColor("#FA541C"));
//        } else if (i == R.id.tv_zysc) {//资源收藏
//            showAssignedFragment(3);
//            initBgAndIc();
//            tvZysc.setTextColor(Color.parseColor("#FA541C"));
//        }
    }

    private void initBgAndIc() {
        tvCtb.setTextColor(Color.parseColor("#323C47"));
        tvWdbj.setTextColor(Color.parseColor("#323C47"));
        tvStsc.setTextColor(Color.parseColor("#323C47"));
        tvZysc.setTextColor(Color.parseColor("#323C47"));

    }

}
