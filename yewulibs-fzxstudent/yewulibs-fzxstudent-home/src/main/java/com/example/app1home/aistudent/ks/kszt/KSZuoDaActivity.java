package com.example.app1home.aistudent.ks.kszt;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1home.R;
import com.example.app1home.aistudent.AndroidInterface;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.sdzn.fzx.student.bean.VersionInfoBean;
import com.sdzn.fzx.student.libbase.newbase.BaseActWebActivity1;
import com.sdzn.fzx.student.libbase.newbase.BaseOnClickListener;
import com.sdzn.fzx.student.libutils.util.statusbar.StatusBarUtil;
import com.sdzn.fzx.student.presenter.CheckverionFzxPresenter;
import com.sdzn.fzx.student.view.CheckverionViews;

/**
 * 考试  是否作答  1  2
 */
public  class KSZuoDaActivity extends BaseActWebActivity1 implements BaseOnClickListener, CheckverionViews {
    CheckverionFzxPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.white), 0);
        checkverionPresenter = new CheckverionFzxPresenter();
        checkverionPresenter.onCreate(this);
        initShaiXuan();//筛选
        TitleShowHideState(4);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
        }
    }

    @Override
    protected void RefreshLoad() {
        super.RefreshLoad();
        checkverionPresenter.checkVerion("3", "0");
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }

    private int checkedPos = 1;

    @Override
    public void Shaixuananniu() {
        if (currentItem.equals("1")) {//未作答

            new XPopup.Builder(KSZuoDaActivity.this)
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .offsetY(25)
                    .offsetX(0)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                    .atView(tvShaiXuan)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                    .asAttachList1(new String[]{"距开始时间>30′", "距开始时间＜30′"},
                            new int[]{},
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
                                    ToastUtils.showShort("click " + text + "\n pos" + position);
                                }
                            }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                    .show();

        } else if (currentItem.equals("2")) {//已作答结束
            new XPopup.Builder(KSZuoDaActivity.this)
                    .hasShadowBg(false)
                    .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                    .offsetY(25)
                    .offsetX(0)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                    .atView(tvShaiXuan)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                    .asAttachList1(new String[]{"我参加的考试", "我未参加的考试"},
                            new int[]{},
                            new OnSelectListener() {
                                @Override
                                public void onSelect(int position, String text) {
                                    checkedPos = position;
                                    ToastUtils.showShort("click " + text + "\n pos" + position);
                                }
                            }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                    .show();
        }
    }

    @Override
    public void Titlesousuo() {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxSearchActivity");
        startActivity(intent);
    }

    private String currentItem;

    private void initShaiXuan() {
        //1  未作答 列表  2已
        currentItem = getIntent().getStringExtra("show");

    }
}
