package com.example.app1home.aistudent.shouye.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.app1home.R;
import com.example.app1home.aistudent.shouye.OneKtxx;

public class Ktxxdapter extends BaseQuickAdapter<OneKtxx, BaseViewHolder> {

    public Ktxxdapter() {
        super(R.layout.recycleview_ktxx_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, OneKtxx item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        TextView tv2 = helper.itemView.findViewById(R.id.tv2);
        TextView tv3 = helper.itemView.findViewById(R.id.tv3);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        tv1.setText(item.getTab_name());
        tv2.setText(item.getSubject());
        tv3.setText(item.getTime());
        Glide.with(mContext).load(item.getSubjectpicture()).into(iv1);
//        iv1.setImageResource(item.getTab_icon());
    }
}
