package com.example.app1home.aistudent.shouye;

import java.io.Serializable;

public class HKtxxBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;//id
    private String name;//名称
    private int icon;//图片
    private String subject;//学科
    private String time;//时间
    private int frame;//边框

    public HKtxxBean() {
    }

    public HKtxxBean(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public HKtxxBean(String code, String name, int icon) {
        this.code = code;
        this.name = name;
        this.icon = icon;
    }

    public HKtxxBean(String code, String name,String subject,String time,int icon) {
        this.code = code;
        this.name = name;
        this.subject = subject;
        this.time = time;
        this.icon = icon;
    }

    public HKtxxBean(String code, String name,String subject,String time,int icon,int frame) {
        this.code = code;
        this.name = name;
        this.subject = subject;
        this.time = time;
        this.icon = icon;
        this.frame = frame;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }
}
