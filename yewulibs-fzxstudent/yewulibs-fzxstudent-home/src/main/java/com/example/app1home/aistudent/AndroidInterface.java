package com.example.app1home.aistudent;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.AppUtils;
import com.just.agentweb.AgentWeb;
import com.sdzn.fzx.student.utils.StudentSPUtils;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidInterface {
    private Handler deliver = new Handler(Looper.getMainLooper());
    private AgentWeb agent;
    private Activity context;

    public AndroidInterface(AgentWeb agent, Activity context) {
        this.agent = agent;
        this.context = context;
    }

    /*跳转课堂学习详情*/
    @JavascriptInterface
    public void ktxxdetails(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KtxxDetailsActivity");
        intent.putExtra("url_key", "file:///android_asset/js_ktxx/ktx_zdz.html");
        context.startActivity(intent);
    }

    /*跳转自主学习详情*/
    @JavascriptInterface
    public void zzxxdetails(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ZzxxDetailsActivity");
        intent.putExtra("url_key", "file:///android_asset/js_zzxx/zzxxdetails.html");
        context.startActivity(intent);
    }

    /*跳转我的作业详情*/
    @JavascriptInterface
    public void wdzydetails(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdzyDetailsActivity");
        intent.putExtra("url_key", "file:///android_asset/js_wdzy/wdzydetails.html");
        context.startActivity(intent);
    }

    /*跳转我的批改详情*/
    @JavascriptInterface
    public void wdpgdetails(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.PgDetailsShouyeActivity");
        context.startActivity(intent);
    }

    /*跳转我的批改查看大图详情*/
    @JavascriptInterface
    public void studentpg(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WdpgChakanActivity");
        intent.putExtra("url_key", "file:///android_asset/js_pg/wdpgchakan.html");
        context.startActivity(intent);
    }


    /**
     * 考试首页 到 首页单科
     */
    @JavascriptInterface
    public void ksdanke(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KSDanKeActivity");
        intent.putExtra("url_key", "file:///android_asset/js_ks/ksdanke.html");
        context.startActivity(intent);
    }

    /**
     * 考试首页 到  列表     1,未作答列表  2
     */
    @JavascriptInterface
    public void kszuoda(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KSZuoDaActivity");
        intent.putExtra("show", str);
        if (str.equals("1")) {
            intent.putExtra("url_key", "file:///android_asset/js_ks/ks_zt1.html");
        } else {
            intent.putExtra("url_key", "file:///android_asset/js_ks/ks_zt2.html");
        }
        context.startActivity(intent);
    }

    /**
     * 作答状态列表 到详情
     */
    @JavascriptInterface
    public void kszuodaDetails(final String str) {
        if (str.equals("1")) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KSWeiZuoDaActivity");
            intent.putExtra("url_key", "file:///android_asset/js_ks/ks_wei.html");
            context.startActivity(intent);
        } else {//已批改
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.KSYiZuoDaActivity");
            intent.putExtra("url_key", "file:///android_asset/js_ks/ks_yi.html");
            context.startActivity(intent);
        }

    }


    /**
     * 合作探究讨论  状态  1     2
     */
    @JavascriptInterface
    public void hztjtl(final String str) {
        if (str.equals("1")) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.HZTJTaoLunActivity");
            intent.putExtra("url_key", "file:///android_asset/js_hztj/hztj_tl.html");
            context.startActivity(intent);
        } else {//已结束
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.HZTJTaoLunJSActivity");
            intent.putExtra("url_key", "file:///android_asset/js_hztj/hztj_tl_js.html");
            context.startActivity(intent);
        }

    }


    /**
     * 合作探究  小组成果
     */
    @JavascriptInterface
    public void hztjOther(final String str) {

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.HZTJJieGuoActivity");
        intent.putExtra("url_key", "file:///android_asset/js_hztj/hztj_other.html");
        context.startActivity(intent);


    }

    /**
     * 合作探究  图
     */
    @JavascriptInterface
    public void hztjTu(final String str) {

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.HZTJTuActivity");
        intent.putExtra("url_key", "file:///android_asset/js_hztj/hztj_tu.html");
        context.startActivity(intent);
    }

    /**
     * 跳转到 学情分析知识点
     */
    @JavascriptInterface
    public void xqfxzhishi(final String str) {

        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XqZhiShiActivity");
        if ("生物".equals(StudentSPUtils.getXueKeStr())) {
            intent.putExtra("url_key", "file:///android_asset/js_xqfx/shengwu_zhishi.html");
        } else if ("英语".equals(StudentSPUtils.getXueKeStr())) {
            intent.putExtra("url_key", "file:///android_asset/js_xqfx/yingyu_zhishi.html");
        }
        context.startActivity(intent);
    }

    /**
     * 跳转到 学情分析 个人课程学情报告
     */
    @JavascriptInterface
    public void xqfxgrbaogao(final String str) {
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.XqGrBaogaoActivity");
        if ("生物".equals(StudentSPUtils.getXueKeStr())) {
            intent.putExtra("url_key", "file:///android_asset/js_xqfx/shengwu_grbaogao.html");
        } else if ("英语".equals(StudentSPUtils.getXueKeStr())) {
            intent.putExtra("url_key", "file:///android_asset/js_xqfx/yingyu_grbaogao.html");
        }
        context.startActivity(intent);
    }


    @JavascriptInterface
    public String callAndroid() {
        deliver.post(new Runnable() {
            @Override
            public void run() {
                Log.i("Info", "main Thread:" + Thread.currentThread());
//                Toast.makeText(context.getApplicationContext(), "" + msg, Toast.LENGTH_LONG).show();
            }
        });

        Log.i("Info", "Thread:" + Thread.currentThread());
        return "进入此方法";
    }

}
