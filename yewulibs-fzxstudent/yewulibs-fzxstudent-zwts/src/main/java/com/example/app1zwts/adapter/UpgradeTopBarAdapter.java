package com.example.app1zwts.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.vo.UpgradeAnswerListBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 试题页面顶部序号adapter
 *
 * 修改
 */
public class UpgradeTopBarAdapter extends BaseRcvAdapter<UpgradeAnswerListBean.AnswerDataBean> {

    public UpgradeTopBarAdapter(Context context) {
        super(context, new ArrayList<UpgradeAnswerListBean.AnswerDataBean>());
    }

    public void clear() {
        mList.clear();
    }

    public void add(List<UpgradeAnswerListBean.AnswerDataBean> list) {
        mList.addAll(list);
    }

    public UpgradeAnswerListBean.AnswerDataBean get(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        UpgradeAnswerListBean.AnswerDataBean bean = mList.get(position);
        if (bean.getParentId() != 0) {//综合题小题不展示
            return 0;
        }
        return bean.getExamSeq();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return BaseViewHolder.get(context, null, parent, R.layout.item_gone);
            default:
                BaseViewHolder holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_top);
                setListener(parent, holder, viewType);
                return holder;
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, UpgradeAnswerListBean.AnswerDataBean bean) {
        if (bean.getExamSeq() == 0 || bean.getParentId() != 0) {
            return;
        }
        TextView tv = holder.getView(R.id.tv_index);
        tv.setText(bean.getExamSeq() + "");

        switch (bean.getExamTypeId()) {
            case 1://单选
            case 2://多选
            case 3://判断
                convertSelect(tv, bean);
                return;
            case 4://简答
                convertAnswer(tv, bean);
                return;
            case 6://填空
            case 14://完型
                convertBlank(tv, bean);
                return;
            case 16://综合
                convertSyn(tv, bean);
        }
    }

    /**
     * 填空 完型要看是否每个都作答
     */
    private void convertBlank(TextView tv, UpgradeAnswerListBean.AnswerDataBean bean) {
        if (bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty() || bean.getExamBean() == null) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
            return;
        }
        //判断作答数量
        List<UpgradeAnswerListBean.ExamOptions> options = bean.getExamBean().getExamOptions();
        if (options == null || options.isEmpty()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
            return;
        }
        List<UpgradeAnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
        if (list == null || list.isEmpty()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
            return;
        }
        if (list.size() < options.size()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg));
            return;
        }
        //判断是否每个空都作答
        int unDoCount = 0;//未作答数量
        for (UpgradeAnswerListBean.ExamOptionBean optionBean : list) {
            if (optionBean.getMyAnswer() == null || optionBean.getMyAnswer().isEmpty()) {
                unDoCount ++;
            }
        }
        if (unDoCount == 0) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
        }else if (unDoCount == options.size()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
        }else {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg));
        }
        //
//        if (bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty()) {
//            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
//            return;
//        }
//        //判断作答数量
//        List<AnswerListBean.ExamOptions> options = bean.getExamBean().getExamOptions();
//        if (options == null || options.isEmpty()) {
//            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
//            return;
//        }
//        List<AnswerListBean.ExamOptionBean> list = bean.getExamOptionList();
//        if (list == null || list.size() < options.size()) {
//            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
//            return;
//        }
//        //判断是否每个空都作答
//        for (AnswerListBean.ExamOptionBean optionBean : list) {
//            if (optionBean.getMyAnswer() == null || optionBean.getMyAnswer().isEmpty()) {
//                tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
//                return;
//            }
//        }
//        tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
    }

    /**
     * 简答 看有没有图片
     */
    private void convertAnswer(TextView tv, UpgradeAnswerListBean.AnswerDataBean bean) {
        if (bean.getExamOptionList() == null || bean.getExamOptionList().isEmpty()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
            return;
        }
        tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
    }

    /**
     * 选择判断直接看isAnswer
     */
    private void convertSelect(TextView tv, UpgradeAnswerListBean.AnswerDataBean bean) {
        tv.setBackground(context.getResources().getDrawable(bean.getIsAnswer() == 0 ?
                R.drawable.zuoda_bg_0 : R.drawable.zuoda_bg_1));
    }

    private void convertSyn(TextView tv, UpgradeAnswerListBean.AnswerDataBean bean) {
        List<UpgradeAnswerListBean.AnswerDataBean> list = bean.getExamList();
        if (list == null || list.isEmpty()) {//没有试题
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
            return;
        }
        //有试题, 遍历试题, 确认作答状态
        int examCount = 0;
        for (UpgradeAnswerListBean.AnswerDataBean dataBean : list) {
            if (dataBean.getIsAnswer() == 1) {
                examCount++;
            } else {
                List<UpgradeAnswerListBean.ExamOptionBean> optionList = dataBean.getExamOptionList();
                if (optionList == null || optionList.isEmpty()) {
                    continue;
                }
                //综合填空
                int stateCount = 0;
                for (UpgradeAnswerListBean.ExamOptionBean optionBean : optionList) {
                    if (optionBean.getMyAnswer() != null) {
                        stateCount++;
                    }
                }
                if (stateCount == optionList.size()) {//全部作答
                    examCount++;
                    continue;
                }
                if (stateCount != 0) {//有填空, 未全部作答
                    tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg));
                    return;
                }
            }
        }
        if (examCount == list.size()) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_1));
        } else if (examCount == 0) {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg_0));
        } else {
            tv.setBackground(context.getResources().getDrawable(R.drawable.zuoda_bg));
        }
    }
}
