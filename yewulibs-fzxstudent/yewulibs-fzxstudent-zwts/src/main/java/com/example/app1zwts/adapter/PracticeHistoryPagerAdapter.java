package com.example.app1zwts.adapter;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.sdzn.fzx.student.libbase.base.BaseFragment;

import java.util.List;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class PracticeHistoryPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> mFragments;

    public PracticeHistoryPagerAdapter(FragmentManager fm, List<BaseFragment> fragmentList) {
        super(fm);
        mFragments = fragmentList;
    }

    @Override
    public BaseFragment getItem(int i) {
        return mFragments.get(i);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

}
