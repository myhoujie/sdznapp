package com.example.app1zwts.fragment;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.example.app1zwts.adapter.SetTextBookAdapter;
import com.example.app1zwts.adapter.SubjectAdapter;
import com.example.app1zwts.listener.OnSelfImproveListener;
import com.example.app1zwts.presenter.SelfImprovementPresenter;
import com.example.app1zwts.view.SelfImprovementView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.BookVersionBean;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;


import static com.example.app1zwts.adapter.SetTextBookAdapter.JIAO_CAI;
import static com.example.app1zwts.adapter.SetTextBookAdapter.NIAN_JI;

/**
 * 自我提升
 *
 * @author Reisen at 2019/2/27
 */
public class SelfImprovementFragment extends MBaseFragment<SelfImprovementPresenter>
        implements SelfImprovementView, RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private RadioGroup mRadioGroup;
    private RadioButton rbSync;
    private RadioButton rbKnowledge;
    private RadioButton rbIntellignet;
    private LinearLayout mPracticeRecord;
    private View line;
    private LinearLayout mSetTextBook;
    private RecyclerView mRvSubject;
    private View mVerticalLine;
    private View line2;
    private FrameLayout flPractice;

    private SubjectAdapter mAdapter;

    private List<Fragment> fragments;
    private int containerId = R.id.fl_practice;
    private Fragment currFragment;
    private List<SubjectVo.DataBean> subjects = new ArrayList<>();
    private int postion;

    public static SelfImprovementFragment newInstance(Bundle bundle) {
        SelfImprovementFragment fragment = new SelfImprovementFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void initPresenter() {
        mPresenter = new SelfImprovementPresenter();
        mPresenter.attachView(this, activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sync_practice, container, false);
        mRadioGroup = (RadioGroup) rootView.findViewById(R.id.rg_group);
        rbSync = (RadioButton) rootView.findViewById(R.id.rb_sync);
        rbKnowledge = (RadioButton) rootView.findViewById(R.id.rb_knowledge);
        rbIntellignet = (RadioButton) rootView.findViewById(R.id.rb_intellignet);
        mPracticeRecord = (LinearLayout) rootView.findViewById(R.id.practice_record);
        line = (View) rootView.findViewById(R.id.line);
        mSetTextBook = (LinearLayout) rootView.findViewById(R.id.set_textbook);
        mRvSubject = (RecyclerView) rootView.findViewById(R.id.rv_subject);
        mVerticalLine = (View) rootView.findViewById(R.id.vertical_line);
        line2 = (View) rootView.findViewById(R.id.line2);
        flPractice = (FrameLayout) rootView.findViewById(R.id.fl_practice);
        initFragment();
        initView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (subjects.isEmpty()) {
            mPresenter.getSubjects();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // ButterKnife.unbind(this);
    }

    private void initView() {
        mRadioGroup.setOnCheckedChangeListener(this);
        mSetTextBook.setOnClickListener(this);
        mPracticeRecord.setOnClickListener(this);
        mRvSubject.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new SubjectAdapter(App2.get(), subjects);
        mRvSubject.setAdapter(mAdapter);
        mRvSubject.addOnItemTouchListener(new OnItemTouchListener(mRvSubject) {
            @Override
            public void onItemClick(final RecyclerView.ViewHolder viewHolder) {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        int position = viewHolder.getAdapterPosition();
                        if (position >= 0) {
                            setSelected(position);
                        }
                    }
                });
            }
        });
    }

    private void setSelected(int pos) {
        for (int i = 0; i < subjects.size(); i++) {
            SubjectVo.DataBean dataBean = subjects.get(i);
            if (i == pos) {
                dataBean.setSelected(true);
            } else {
                dataBean.setSelected(false);
            }
        }
        mAdapter.notifyDataSetChanged();

        for (Fragment fragment : fragments) {
            ((OnSelfImproveListener) fragment).onSubjectItemClick(pos);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        //智能练习
        //知识点练习
        //同步练习
        if (checkedId == R.id.rb_intellignet) {
            mVerticalLine.setVisibility(View.GONE);
            mSetTextBook.setVisibility(View.GONE);
            showAssignedFragment(2);
        } else if (checkedId == R.id.rb_knowledge) {
            mVerticalLine.setVisibility(View.GONE);
            mSetTextBook.setVisibility(View.GONE);
            showAssignedFragment(1);
        } else if (checkedId == R.id.rb_sync) {
            mVerticalLine.setVisibility(View.VISIBLE);
            mSetTextBook.setVisibility(View.VISIBLE);
            showAssignedFragment(0);
        }
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(SelfContentFragment.newInstance(1));
        fragments.add(SelfContentFragment.newInstance(0));
        fragments.add(SelfContentFragment.newInstance(3));
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        currFragment = fragments.get(0);
        ft.add(containerId, currFragment).commit();
    }

    private void showAssignedFragment(int fragmentIndex) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        if (currFragment != fragment) {
            if (!fragment.isAdded()) {
                ft.hide(currFragment).add(containerId, fragment, fragment.getClass().getName());
            } else {
                ft.hide(currFragment).show(fragment);
            }
        }
        currFragment = fragment;
        ft.commit();
    }

    @Override
    public void getSubjectSuccess(SubjectVo subjectVo) {
        if (subjectVo == null || subjectVo.getData() == null) {
            return;
        }

        List<SubjectVo.DataBean> list = subjectVo.getData();
        if (!list.isEmpty()) {
            list.get(0).setSelected(true);
            subjects.addAll(list);
        }
        mAdapter.notifyDataSetChanged();

        for (Fragment fragment : fragments) {
            ((OnSelfImproveListener) fragment).onSubjectGet(subjects, postion);
            ((OnSelfImproveListener) fragment).setOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageChange(int pos) {
                    setSelected(pos);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.set_textbook) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    for (SubjectVo.DataBean subject : subjects) {
                        if (!subject.isSelected()) {
                            continue;
                        }
                        mPresenter.queryBookVersion(subject.getId());
                        return;
                    }
                }
            });
        } else if (i == R.id.practice_record) {
            SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                @Override
                public void run() {
                    ContextManager.startPracticeHistory(getContext());
                }
            });
        }
    }

    @Override
    public void queryBookVersionSuccess(List<BookVersionBean> list) {

        PopupWindow popupWindow = showPop(getActivity(), list, subjects);
        popupWindow.showAtLocation(activity.findViewById(R.id.fl_practice), Gravity.BOTTOM, 0, 0);
    }

    private PopupWindow showPop(final Activity activity, final List<BookVersionBean> list, final List<SubjectVo.DataBean> subjects) {

        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.bottom_popuwindow, null);
        final ListView listView = contentView.findViewById(R.id.listview);
        final TextView cancel = contentView.findViewById(R.id.cancel);
        final TextView confim = contentView.findViewById(R.id.confirm);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, 800, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetTextBookAdapter adapter = (SetTextBookAdapter) listView.getAdapter();
                if (adapter.getState() == JIAO_CAI) {
                    popupWindow.dismiss();
                } else {
                    adapter.setState(JIAO_CAI);
                    cancel.setText("取消");
                    confim.setText("下一步");
                    adapter.notifyDataSetChanged();
                }
            }
        });
        contentView.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetTextBookAdapter adapter = (SetTextBookAdapter) listView.getAdapter();
                if (adapter.getState() == NIAN_JI) {
                    long courseId = -1;
                    for (BookVersionBean bean : list) {
                        if (!bean.getBookVersion().isSelected()) {
                            continue;
                        }
                        for (BookVersionBean.Course course : bean.getCourseMapping()) {
                            if (course.isSelected()) {
                                courseId = course.getBookId();
                            }
                        }
                    }
                    if (courseId == -1) {
                        ToastUtil.showShortlToast("请选择教材版本");
                        return;
                    }
                    int subjectId = 0;
                    postion = 0;
                    for (SubjectVo.DataBean subject : subjects) {
                        if (subject.isSelected()) {
                            subjectId = subject.getId();
                            break;
                        } else {
                            postion++;
                        }
                    }
                    StudentSPUtils.saveStudentCourse(subjectId, courseId);
                    popupWindow.dismiss();
                    for (Fragment fragment : fragments) {
                        ((OnSelfImproveListener) fragment).onSubjectGet(subjects, postion);
                    }
                } else {
                    if (adapter.setState(NIAN_JI)) {
                        cancel.setText("上一步");
                        confim.setText("确定");
                        adapter.notifyDataSetChanged();
                    } else {
                        ToastUtil.showShortlToast("请选择教材");
                    }
                }
            }
        });
        //先展示教材列表
        listView.setAdapter(new SetTextBookAdapter(list, activity));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SetTextBookAdapter adapter = (SetTextBookAdapter) listView.getAdapter();
                if (adapter.getState() == JIAO_CAI) {
                    adapter.selectBook(position);
                } else {
                    adapter.selectCourse(position);
                }
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });
        popupWindow.setOutsideTouchable(true);
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.8f;
        activity.getWindow().setAttributes(lp);
        return popupWindow;
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void onHiddenChanged(boolean isHidden) {
        if (subjects.isEmpty()) {
            mPresenter.getSubjects();
        }
    }
}
