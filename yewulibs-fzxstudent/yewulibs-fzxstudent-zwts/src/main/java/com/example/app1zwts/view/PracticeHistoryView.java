package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.SubjectVo;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public interface PracticeHistoryView extends BaseView {
    void networkError(String msg);

    void getSubjectSuccess(SubjectVo subjectVo);
}
