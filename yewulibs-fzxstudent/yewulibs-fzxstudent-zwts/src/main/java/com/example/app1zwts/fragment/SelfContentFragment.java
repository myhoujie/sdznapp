package com.example.app1zwts.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.app1zwts.R;
import com.example.app1zwts.listener.OnSelfImproveListener;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.listener.OnPageChangeListener;
import com.sdzn.fzx.student.libpublic.baseadapter.TaskPagerAdapter;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 自我提升
 *
 * @author wangchunxiao
 */
public class SelfContentFragment extends BaseFragment implements OnSelfImproveListener {
    public static final String STATUS = "status";

    private ViewPager viewPager;

    private OnPageChangeListener onPageChangeListener;

    private int status;//0=知识点练习, 1=同步练习

    private List<SubjectVo.DataBean> subjects;
    private int position;

    public static SelfContentFragment newInstance(int status) {
        SelfContentFragment fragment = new SelfContentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(STATUS, status);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_content, container, false);
        viewPager = rootView.findViewById(R.id.pager);
        Bundle arg = getArguments();
        if (arg != null) {
            status = arg.getInt(STATUS);
        }
        initView();
        return rootView;
    }

    private void initView() {
        if (subjects != null) {
            initFragments(subjects);
            viewPager.setCurrentItem(position, false);
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelected(position);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onSubjectItemClick(int position) {
        if (viewPager == null) {
            this.position = position;
        } else {
            viewPager.setCurrentItem(position, false);
        }
    }

    @Override
    public void onSubjectGet(List<SubjectVo.DataBean> subjects, int pos) {
        if (viewPager == null) {
            this.subjects = subjects;
        } else {
            initFragments(subjects);
            viewPager.setCurrentItem(pos, false);
        }
    }

    private void initFragments(List<SubjectVo.DataBean> subjects) {
        List<Fragment> fragmentArrayList = new ArrayList<>();
        for (int i = 0; i < subjects.size(); i++) {
            SubjectVo.DataBean dataBean = subjects.get(i);
            Bundle bundle = new Bundle();
            if (status == 3) {
                bundle.putInt(IntelligentFragment.SUBJECTID, dataBean.getId());
                fragmentArrayList.add(IntelligentFragment.newInstance(bundle));//智能练习
            } else {
                bundle.putInt(UpgradeListFragment.SUBJECTID, dataBean.getId());
                bundle.putInt(UpgradeListFragment.STATUS, status);
                fragmentArrayList.add(UpgradeListFragment.newInstance(bundle));//知识点练习，同步练习
            }
        }
        viewPager.setOffscreenPageLimit(0);
        TaskPagerAdapter taskPagerAdapter = new TaskPagerAdapter(getChildFragmentManager(), fragmentArrayList);
        viewPager.setAdapter(taskPagerAdapter);
    }

    @Override
    public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.onPageChangeListener = onPageChangeListener;
    }

    private void setSelected(int pos) {
        if (onPageChangeListener != null) {
            onPageChangeListener.onPageChange(pos);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }
}
