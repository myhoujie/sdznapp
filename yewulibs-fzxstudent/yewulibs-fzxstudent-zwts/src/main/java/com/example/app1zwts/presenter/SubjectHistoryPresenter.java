package com.example.app1zwts.presenter;


import android.widget.ProgressBar;

import com.example.app1zwts.R;
import com.example.app1zwts.view.SubjectHistoryView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.libpublic.y_recycleradapter.GeneralRecyclerViewHolder;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.vo.ArrayBean;
import com.sdzn.fzx.student.vo.TeachIntelTask;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class SubjectHistoryPresenter extends BasePresenter<SubjectHistoryView, BaseActivity> {
    /**
     * 获取课程列表
     */
    public void getHistoryList(int subjectId, int offset) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .queryIntelExerciseTask(subjectId,offset,10)
                .map(new StatusFunc<ArrayBean<TeachIntelTask>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ArrayBean<TeachIntelTask>>(new SubscriberListener<ArrayBean<TeachIntelTask>>() {
                    @Override
                    public void onNext(ArrayBean<TeachIntelTask> bean) {
                        if (bean.getData() != null && !bean.getData().isEmpty()) {
                            mView.getHistoryListSuccess(bean.getData());
                        }else {
                            mView.cancelLoad();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.networkError(status.getMsg());
                            } else {
                                mView.networkError("数据获取失败");
                            }
                        } else {
                            mView.networkError("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true,false,true,null));
    }

    /**
     * 绑定数据
     */
    public void bindTaskListHolder(GeneralRecyclerViewHolder holder, TeachIntelTask itemData) {
        Subject subject = Subject.subjects.get(itemData.getTaskType()+100);
        if (subject != null) {
            holder.getChildView(R.id.ivXueke).setBackground(App2.get().getResources().getDrawable(subject.getDrawableId()));
            holder.setText(R.id.tvTaskName, subject.getName());
        }
        holder.setText(R.id.tvDateStart, DateUtil.getTimeStrByTimemillis(itemData.getCreateTime(), "yyyy-MM-dd HH:mm"));
        ((ProgressBar) holder.getChildView(R.id.progressbar)).setMax(itemData.getExamCount());
        ((ProgressBar) holder.getChildView(R.id.progressbar)).setProgress(itemData.getRightCount());
        holder.setText(R.id.tvAnswer, String.valueOf(itemData.getRightCount()));
        holder.setText(R.id.tvCount, String.valueOf(itemData.getExamCount()));
    }
}