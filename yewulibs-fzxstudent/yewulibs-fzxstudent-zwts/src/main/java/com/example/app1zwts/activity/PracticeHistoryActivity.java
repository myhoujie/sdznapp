package com.example.app1zwts.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1zwts.R;
import com.example.app1zwts.adapter.PracticeHistoryPagerAdapter;
import com.example.app1zwts.adapter.SubjectAdapter;
import com.example.app1zwts.fragment.SubjectHistoryFragment;
import com.example.app1zwts.presenter.PracticeHistoryPresenter;
import com.example.app1zwts.view.PracticeHistoryView;
import com.sdzn.fzx.student.libbase.base.BaseFragment;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.listener.OnItemTouchListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.ArrayList;
import java.util.List;


/**
 * 练习记录
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class PracticeHistoryActivity extends MBaseActivity<PracticeHistoryPresenter> implements PracticeHistoryView {
    private TextView mTvBack;
    private TextView tvTitle;
    private View line;
    private RecyclerView mRecyclerView;
    private ViewPager mViewPager;


    private List<SubjectVo.DataBean> mSubjectList = new ArrayList<>();
    private List<BaseFragment> mFragmentList = new ArrayList<>();
    private SubjectAdapter mAdapter;
    PracticeHistoryPagerAdapter mPageadapter;

    @Override
    public void initPresenter() {
        mPresenter = new PracticeHistoryPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_history);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(PracticeHistoryActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }
    @Override
    protected void initView() {
        mTvBack = (TextView) findViewById(R.id.tvBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        line = (View) findViewById(R.id.line);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvSubject);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        mTvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PracticeHistoryActivity.this.finish();
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new SubjectAdapter(App2.get(), mSubjectList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(new OnItemTouchListener(mRecyclerView) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position >= 0) {
                    setSelected(position);
                }
            }
        });
        mPageadapter = new PracticeHistoryPagerAdapter(getSupportFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mPageadapter);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setSelected(position);
            }
        });
    }

    private void setSelected(int pos) {
        for (int i = 0; i < mSubjectList.size(); i++) {
            SubjectVo.DataBean dataBean = mSubjectList.get(i);
            if (i == pos) {
                dataBean.setSelected(true);
            } else {
                dataBean.setSelected(false);
            }
        }
        mAdapter.notifyDataSetChanged();
        mViewPager.setCurrentItem(pos);
    }

    @Override
    protected void initData() {
        mPresenter.getSubjects();
    }

    @Override
    public void networkError(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void getSubjectSuccess(SubjectVo subjectVo) {
        //更新adapter, 请求viewpager中数据
        if (subjectVo == null || subjectVo.getData() == null || subjectVo.getData().isEmpty()) {
            return;
        }
        mSubjectList.clear();
        mSubjectList.addAll(subjectVo.getData());
        mSubjectList.get(0).setSelected(true);

        createFragments();
    }

    private void createFragments() {
        mFragmentList.clear();
        for (SubjectVo.DataBean bean : mSubjectList) {
            mFragmentList.add(SubjectHistoryFragment.newInstance(bean));
        }
        mPageadapter.notifyDataSetChanged();
    }
}
