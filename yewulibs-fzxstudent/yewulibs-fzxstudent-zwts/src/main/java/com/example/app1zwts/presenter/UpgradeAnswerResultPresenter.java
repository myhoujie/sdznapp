package com.example.app1zwts.presenter;

import com.example.app1zwts.view.UpgradeAnswerResultView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.vo.ArrayBean;
import com.sdzn.fzx.student.vo.UpgradeAnswerResultBean;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 提升练习
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class UpgradeAnswerResultPresenter extends BasePresenter<UpgradeAnswerResultView,BaseActivity> {
    /**
     * 加载试题
     */
    public void loadData(long id) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .queryIntelExerciseTaskDetail(id)
                .map(new StatusFunc<ArrayBean<UpgradeAnswerResultBean>>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<ArrayBean<UpgradeAnswerResultBean>>(new SubscriberListener<ArrayBean<UpgradeAnswerResultBean>>() {
                    @Override
                    public void onNext(ArrayBean<UpgradeAnswerResultBean> bean) {
                        mView.loadDataSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                mView.loadDataFailed(status.getMsg());
                            } else {
                                mView.loadDataFailed("数据获取失败");
                            }
                        } else {
                            mView.loadDataFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true,false,true,null));
    }
}
