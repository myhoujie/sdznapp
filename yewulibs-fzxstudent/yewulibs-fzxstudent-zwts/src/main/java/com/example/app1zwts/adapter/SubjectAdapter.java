package com.example.app1zwts.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.libpublic.cons.Subject;
import com.sdzn.fzx.student.vo.SubjectVo;

import java.util.List;

/**
 * 顶部课程adapter
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/6.
 */
public class SubjectAdapter extends BaseRcvAdapter<SubjectVo.DataBean> {
    public SubjectAdapter(Context context, List<SubjectVo.DataBean> mList) {
        super(context, R.layout.item_fragment_task_content_sub, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, SubjectVo.DataBean dataBean) {
        Subject subject = Subject.subjects.get(dataBean.getId());
        ImageView iv = holder.getView(R.id.ivXueke);
        TextView tv = holder.getView(R.id.tvXueke);
        if (dataBean.isSelected()) {
            if (subject != null) {
                iv.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), subject.getDrawableId_sel()));
            }
            tv.setTextColor(context.getResources().getColor(R.color.fragment_main_chart_line_text));
        } else {
            if (subject != null) {
                iv.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), subject.getDrawableId_nor()));
            }
            tv.setTextColor(context.getResources().getColor(R.color.fragment_task_subject_item_text_g));
        }
        tv.setText(dataBean.getName());
    }
}
