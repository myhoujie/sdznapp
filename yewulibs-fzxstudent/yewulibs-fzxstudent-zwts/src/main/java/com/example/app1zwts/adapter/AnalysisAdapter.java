package com.example.app1zwts.adapter;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.example.app1zwts.R;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.BaseViewHolder;
import com.sdzn.fzx.student.vo.AnalysisBean;

import java.util.List;

/**
 * 统计分析下方list
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/11.
 */
public class AnalysisAdapter extends BaseRcvAdapter<AnalysisBean.NameAccuracy> {
    private String ErrorRate = "---0.0%";//正确率

    public AnalysisAdapter(Context context, List<AnalysisBean.NameAccuracy> mList) {
        super(context, mList);
    }

    public void refresh(String ErrorRate) {
        this.ErrorRate = ErrorRate;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getId() == -1 ? 1 : 0;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return BaseViewHolder.get(context, null, parent, viewType == 1 ? R.layout.item_analysis_name : R.layout.item_analysis);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, AnalysisBean.NameAccuracy nameAccuracy) {
        AnalysisBean.NameAccuracy accuracy = mList.get(position);
        if (accuracy.getId() == -1) {//标题行
            if ("方法".equals(accuracy.getName())) {
                holder.setImageView(R.id.iv_name, R.mipmap.method);
            }else if("能力".equals(accuracy.getName())){
                holder.setImageView(R.id.iv_name, R.mipmap.ability);
            }else{
                holder.setVisible(R.id.rl_title,false);
                Log.d("Test", "当前方法集合没有参数不显示");
            }
            // holder.setImageView(R.id.iv_name, "方法".equals(accuracy.getName()) ? R.mipmap.method : R.mipmap.ability);
        } else {//填充数据
            holder.setText(R.id.tv_name, accuracy.getName());
            holder.setText(R.id.tv_degree, accuracy.getAccuracy() + "%");//掌握程度
            holder.setText(R.id.tv_accuracy, ErrorRate + "");//正确率
        }
    }
}
