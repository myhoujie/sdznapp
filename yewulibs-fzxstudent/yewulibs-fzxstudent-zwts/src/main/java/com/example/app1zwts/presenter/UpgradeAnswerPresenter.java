package com.example.app1zwts.presenter;

import android.util.Log;

import com.example.app1zwts.view.UpgradeAnswerView;
import com.sdzn.fzx.student.api.func.ApiException;
import com.sdzn.fzx.student.api.func.StatusFunc;
import com.sdzn.fzx.student.api.module.StatusVo;
import com.sdzn.fzx.student.api.network.NetWorkService;
import com.sdzn.fzx.student.api.network.Network;
import com.sdzn.fzx.student.api.subscriber.ProgressSubscriber;
import com.sdzn.fzx.student.api.subscriber.SubscriberListener;
import com.sdzn.fzx.student.libbase.base.BaseActivity;
import com.sdzn.fzx.student.libbase.base.BasePresenter;
import com.sdzn.fzx.student.libutils.util.GsonUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.UpgradeAnswerBean;
import com.sdzn.fzx.student.vo.UpgradeAnswerListBean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 提升练习
 *
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public class UpgradeAnswerPresenter extends BasePresenter<UpgradeAnswerView, BaseActivity> {

    /**
     * 加载试题  --1
     */
    public void getKnowledgePointExercise(int subjectId, long knowledgePointId) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .getKnowledgePointExercise(subjectId, 10, knowledgePointId)
                .map(new StatusFunc<UpgradeAnswerBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UpgradeAnswerBean>(new SubscriberListener<UpgradeAnswerBean>() {
                    @Override
                    public void onNext(UpgradeAnswerBean bean) {
                        mView.loadDataSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                if (status.getMsg().equals("获取软云知识点练习失败")) {
                                    mView.loadDataFailed("暂无相关试题");
                                } else {
                                    mView.loadDataFailed(status.getMsg());
                                }
                            } else {
                                mView.loadDataFailed("数据获取失败");
                            }
                        } else {
                            mView.loadDataFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, true, null));
    }

    /**
     * 加载试题  ----2
     */
    public void getExerciseByChapterSection(int subjectId, long chapterSectionId) {
        long course = StudentSPUtils.getStudentCourse(subjectId);
        if (course == -1) {
            mView.loadDataFailed("数据获取失败");
            return;
        }
        Network.createTokenService(NetWorkService.RuanYun.class)
                .getExerciseByChapterSection(subjectId, 10, course, chapterSectionId)
                .map(new StatusFunc<UpgradeAnswerBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UpgradeAnswerBean>(new SubscriberListener<UpgradeAnswerBean>() {
                    @Override
                    public void onNext(UpgradeAnswerBean bean) {
                        mView.loadDataSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                if (status.getMsg().equals("获取软云知识点练习失败")) {
                                    mView.loadDataFailed("暂无相关试题");
                                } else {
                                    mView.loadDataFailed(status.getMsg());
                                }
                            } else {
                                mView.loadDataFailed("数据获取失败");
                            }
                        } else {
                            mView.loadDataFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, true, null));
    }

    /**
     * 加载试题 -- 智能推荐
     */
    public void getIntelligent(int subjectId) {
        Network.createTokenService(NetWorkService.RuanYun.class)
                .queryIntelligentExercise(subjectId, 10)
                .map(new StatusFunc<UpgradeAnswerBean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<UpgradeAnswerBean>(new SubscriberListener<UpgradeAnswerBean>() {
                    @Override
                    public void onNext(UpgradeAnswerBean bean) {
                        mView.loadDataSuccess(bean.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                if (status.getMsg().equals("获取软云智能练习失败")) {
                                    mView.loadDataFailed("请先使用同步练习进行学习");
                                } else {
                                    mView.loadDataFailed(status.getMsg());
                                }
                            } else {
                                mView.loadDataFailed("数据获取失败");
                            }
                        } else {
                            mView.loadDataFailed("数据获取失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, true, null));
    }

    /**
     * 提交
     */
    public void submit(int subjectId, String teacherExamId, List<UpgradeAnswerListBean.AnswerDataBean> list) {
        String json = createJson(list);
        Log.e("test", "aaatest" + Network.createTokenService(NetWorkService.RuanYun.class));
        Log.e("aaatest", "json" + json);
        Network.createTokenService(NetWorkService.RuanYun.class)
                .submitRuanYunExercise(subjectId, 10, teacherExamId, json)
                .map(new StatusFunc<>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProgressSubscriber<>(new SubscriberListener<Object>() {
                    @Override
                    public void onNext(Object bean) {
                        Log.e("aaatest", "---->>>>>onNext" + String.valueOf(bean));
                        mView.submitSuccess();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView == null) {
                            return;
                        }
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            StatusVo status = apiException.getStatus();
                            if (status != null && status.getMsg() != null) {
                                if (status.getMsg().equals("操作失败")) {
                                    mView.loadDataFailed("你还没有答题，请答题以后再提交");
                                } else {
                                    mView.submitFailed(status.getMsg());
                                }
                                Log.e("aaatest", "---->>>>>onError: " + status.getMsg());
                            } else {
                                mView.submitFailed("数据提交失败");
                            }
                        } else {
                            mView.submitFailed("数据提交失败");
                        }
                    }

                    @Override
                    public void onCompleted() {
                    }
                }, mActivity, true, false, true, null));
    }

    private String createJson(List<UpgradeAnswerListBean.AnswerDataBean> list) {
        ArrayList<UpgradeAnswerBean.SubmitBean> beans = new ArrayList<>();
        Log.e("Test", "createJsonlist1: " + list);
        for (UpgradeAnswerListBean.AnswerDataBean bean : list) {//题目
            if (bean.getExamTypeId() == 1 || bean.getExamTypeId() == 2 || bean.getExamTypeId() == 3) {//样式题型id=1多选题  ---type=3判断题
                beans.add(createSelectorBean(bean));
            }
        }
        return GsonUtil.toJson(beans);
    }

    private UpgradeAnswerBean.SubmitBean createSelectorBean(UpgradeAnswerListBean.AnswerDataBean bean) {
        UpgradeAnswerBean.SubmitBean submitBean = new UpgradeAnswerBean.SubmitBean();//获取到的题号id
        submitBean.setQuestionId(bean.getId());
        List<UpgradeAnswerListBean.ExamOptionBean> upgradeexamOptionList = bean.getExamOptionList();
        if (upgradeexamOptionList == null || upgradeexamOptionList.isEmpty()) {
            return submitBean;
        } else {
            for (int i = 0; i < upgradeexamOptionList.size(); i++) {
                String myanswer = upgradeexamOptionList.get(i).getMyAnswer();
                if (bean.getExamTypeId() == 3) {
                    if (myanswer.equals("A")) {
                        submitBean.setStudentAnswer("对");
                    } else {
                        submitBean.setStudentAnswer("错");
                    }
                }
            }
        }
        List<UpgradeAnswerListBean.ExamOptions> options = bean.getExamOptions();//答题选项
        if (options == null || options.isEmpty()) {
            return submitBean;
        }
        ArrayList<UpgradeAnswerBean.SubmitOptionAnswer> optionAnswers = new ArrayList<>();
        List<UpgradeAnswerListBean.ExamOptionBean> optionList = bean.getExamOptionList();//答案
        if (optionList == null || optionList.isEmpty()) {
            return submitBean;
        }
        if (bean.getExamTypeId() == 1) {
            for (UpgradeAnswerListBean.ExamOptions option : options) {//遍历答案选项
                for (UpgradeAnswerListBean.ExamOptionBean optionBean : optionList) {//遍历选中答案
                    if (option.getSeq() == optionBean.getSeq()) {//判断答案
                        optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(true, option.getSeq()));
                        continue;
                    }
                    optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(false, option.getSeq()));
                }
            }
        }
        if (bean.getExamTypeId() == 2) {
//            Log.e("aaatest", "options.size()" + options.size());
//            Log.e("aaatest", "optionList.size()" + optionList.size());
//            for (UpgradeAnswerListBean.ExamOptions option : options) {//遍历答案选项
//                for (UpgradeAnswerListBean.ExamOptionBean optionBean : optionList) {//遍历选中答案
//                    Log.e("aaatest", "option.getSeq()" + option.getSeq());
//                    Log.e("aaatest", "optionBean.getSeq()" + option.getSeq());
//                    if (option.getSeq() == optionBean.getSeq()) {//判断答案
//                        optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(true, option.getSeq()));
//                        continue;
//                    }
//                }
//                optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(false, option.getSeq()));
//            }
            List<Integer> mList = new ArrayList<>();
            List<Integer> mList2 = new ArrayList<>();
            for (int i = 0; i < options.size(); i++) {
                mList.add(options.get(i).getSeq());
            }
            for (int i = 0; i < optionList.size(); i++) {
                mList2.add(optionList.get(i).getSeq());
            }
            //获取相同的
            Set<Integer> sameElementSet = getIds(mList, mList2);
            for (Integer i : sameElementSet) {
                optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(true,i));
            }
            //获取不同的
            List<Integer> list = compare(mList2, mList);
            for (Integer s : list) {
                optionAnswers.add(UpgradeAnswerBean.SubmitOptionAnswer.init(false, s));
            }

        }

        submitBean.setOptionAnswer(optionAnswers);
        return submitBean;
    }

    //相同的
    public static Set<Integer> getIds(List<Integer> a, List<Integer> b) {

        Set<Integer> same = new HashSet<Integer>();  //用来存放两个数组中相同的元素
        Set<Integer> temp = new HashSet<Integer>();  //用来存放数组a中的元素

        for (int i = 0; i < a.size(); i++) {
            temp.add(a.get(i));   //把数组a中的元素放到Set中，可以去除重复的元素
        }
        for (int j = 0; j < b.size(); j++) {
            //把数组b中的元素添加到temp中
            //如果temp中已存在相同的元素，则temp.add（b[j]）返回false
            if (!temp.add(b.get(j)))
                same.add(b.get(j));
        }
        return same;
    }

    //不同的
    public static <Integer> List<Integer> compare(List<Integer> t1, List<Integer> t2) {
        List<Integer> list2 = new ArrayList<Integer>();//用来存放2个数组中不相同的元素
        for (Integer t : t2) {
            if (!t1.contains(t)) {
                list2.add(t);
            }
        }
        return list2;
    }

}
