package com.example.app1zwts.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1zwts.R;
import com.example.app1zwts.adapter.UpgradeAnswerPagerAdapter;
import com.example.app1zwts.adapter.UpgradeTopBarAdapter;
import com.example.app1zwts.presenter.UpgradeAnswerPresenter;
import com.example.app1zwts.view.UpgradeAnswerView;
import com.sdzn.fzx.student.libbase.app.NetworkReceiver;
import com.sdzn.fzx.student.libbase.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.listener.OnExamRefreshListener;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.fzx.student.libutils.util.DateUtil;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.UpgradeAnswerBean;
import com.sdzn.fzx.student.vo.UpgradeAnswerListBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * 提升练习
 */
public class UpgradeAnswerActivity extends MBaseActivity<UpgradeAnswerPresenter>
        implements UpgradeAnswerView, View.OnClickListener, OnExamRefreshListener {
    public static final String STATUS = "status";
    public static final String SUBJECTID = "subjectId";
    public static final String KNOWLEDGEPOINTID = "knowledgePointId";
    public static final String CHAPTERSECTIONID = "chapterSectionId";

    private RelativeLayout netErrorRy;
    private TextView tvBack;
    private TextView tvCommit;
    private TextView tvTime;
    private RecyclerView rvNumber;
    private ViewPager mViewPager;


    private List<UpgradeAnswerListBean.AnswerDataBean> mList = new ArrayList<>();
    private UpgradeTopBarAdapter mTopAdapter;
    private UpgradeAnswerPagerAdapter mPagerAdapter;

    private long time;
    private Handler mHandler;

    private int status;
    private int subjectId;
    private long knowledgePointId;
    private long chapterSectionId;

    private String teacherExamId;

    @Override
    public void initPresenter() {
        mPresenter = new UpgradeAnswerPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_answer);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(UpgradeAnswerActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        netErrorRy = (RelativeLayout) findViewById(R.id.net_error_ry);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvCommit = (TextView) findViewById(R.id.tvCommit);
        tvTime = (TextView) findViewById(R.id.tvTime);
        rvNumber = (RecyclerView) findViewById(R.id.rv_number);
        mViewPager = (ViewPager) findViewById(R.id.vp);
        tvBack.setOnClickListener(this);
        tvCommit.setOnClickListener(this);


        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                time += 1000;
                tvTime.setText(DateUtil.millsecondsToStr(time));
                mHandler.sendEmptyMessageDelayed(1, 1000);
            }
        };

        mTopAdapter = new UpgradeTopBarAdapter(App2.get());
        rvNumber.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvNumber.setAdapter(mTopAdapter);
        mTopAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mViewPager.setCurrentItem(position, false);
            }
        });
        mPagerAdapter = new UpgradeAnswerPagerAdapter(this, mList, this);
        mViewPager.setAdapter(mPagerAdapter);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        subjectId = intent.getIntExtra(SUBJECTID, 0);
        knowledgePointId = intent.getLongExtra(KNOWLEDGEPOINTID, 0);
        chapterSectionId = intent.getLongExtra(CHAPTERSECTIONID, 0);
        status = intent.getIntExtra(STATUS, 0);
        if (status == 0) {
            mPresenter.getKnowledgePointExercise(subjectId, knowledgePointId);
        } else if (status == 1) {
            mPresenter.getExerciseByChapterSection(subjectId, chapterSectionId);
        } else {
            mPresenter.getIntelligent(subjectId);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkCallback(NetworkReceiver.NetworkEvent event) {
        if (event.isWork) {
            netErrorRy.setVisibility(View.GONE);
        } else {
            netErrorRy.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvBack) {
            finish();
        } else if (i == R.id.tvCommit) {
            if (time == 0) {
                return;
            }
            //提交
            mPresenter.submit(subjectId, teacherExamId, mList);

        }
    }

    @Override
    public void loadDataSuccess(UpgradeAnswerBean.Data data) {
        List<UpgradeAnswerListBean.AnswerDataBean> list = data.getExamList();
        teacherExamId = data.getTeacherExamId();
        if (list == null || list.isEmpty()) {
            return;
        }
        mList.clear();
        mList.addAll(list);
        for (int i = 0; i < mList.size(); i++) {//软云题没有题号
            UpgradeAnswerListBean.AnswerDataBean bean = mList.get(i);
            bean.setExamSeq(i + 1);
            bean.setExamTemplateId(1);
        }
        mTopAdapter.clear();
        mTopAdapter.add(mList);
        mTopAdapter.notifyDataSetChanged();
        mPagerAdapter.notifyDataSetChanged();
        mHandler.sendEmptyMessageDelayed(1, 1000);
    }

    @Override
    public void submitSuccess() {
        mHandler.removeMessages(1);
        finish();
    }

    @Override
    public void loadDataFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    public void submitFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void requestRefreshExam(int examType) {
        refreshTop();
        refreshList();
        if (examType != 2) {
            int index = mViewPager.getCurrentItem();
            if (index < mList.size() - 1) {
                mViewPager.setCurrentItem(index + 1, true);
            }
        }
    }

    @Override
    public void requestRefresh() {
        refreshTop();
        refreshList();
        int index = mViewPager.getCurrentItem();
        if (index < mList.size() - 1) {
            mViewPager.setCurrentItem(index + 1, true);
        }
    }

    @Override
    public void refreshTop() {
        mTopAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshList() {
        mPagerAdapter.notifyDataSetChanged();
    }
}
