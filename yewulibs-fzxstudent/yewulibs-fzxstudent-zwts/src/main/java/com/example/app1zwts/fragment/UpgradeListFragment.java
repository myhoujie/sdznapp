package com.example.app1zwts.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.example.app1zwts.presenter.UpgradeListPresenter;
import com.example.app1zwts.view.UpgradeListView;
import com.sdzn.fzx.student.libbase.base.MBaseFragment;
import com.sdzn.fzx.student.libbase.holder.UpgradeHeaderHolder;
import com.sdzn.fzx.student.libbase.holder.UpgradeTreeItemHolder;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.vo.ChapterSectionBean;
import com.sdzn.fzx.student.vo.KnowledgePointBean;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.Collections;
import java.util.List;

/**
 * 学习任务列表
 *
 * @author wangchunxiao
 */
public class UpgradeListFragment extends MBaseFragment<UpgradeListPresenter>
        implements UpgradeListView, TreeNode.TreeNodeClickListener {
    public static final String STATUS = "status";
    public static final String SUBJECTID = "subjectId";
    private FrameLayout mFrameLayout;
    private LinearLayout llTaskEmpty;
    private ImageView ivTaskEmpty;
    private TextView tvTaskEmpty;

    private int subjectId;
    private int status;

    @Override
    public void initPresenter() {
        mPresenter = new UpgradeListPresenter();
        mPresenter.attachView(this, activity);
    }

    public static UpgradeListFragment newInstance(Bundle bundle) {
        UpgradeListFragment fragment = new UpgradeListFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_upgrade_list, container, false);
        mFrameLayout = (FrameLayout) rootView.findViewById(R.id.fl_tree);
        llTaskEmpty = (LinearLayout) rootView.findViewById(R.id.llTaskEmpty);
        ivTaskEmpty = (ImageView) rootView.findViewById(R.id.ivTaskEmpty);
        tvTaskEmpty = (TextView) rootView.findViewById(R.id.tvTaskEmpty);

        subjectId = getArguments().getInt(SUBJECTID);
        status = getArguments().getInt(STATUS);
        initView();
        initData();
        return rootView;
    }

    private void initView() {
        mFrameLayout.setVisibility(View.VISIBLE);
        llTaskEmpty.setVisibility(View.GONE);
    }

    private void initData() {
        if (status == 0) {
            mPresenter.queryKnowledgePointScoreList(subjectId);
        } else {
            mPresenter.queryChapterSection(subjectId);
        }
    }

    @Override
    public void getDataSuccess(List<KnowledgePointBean> list) {
        if (list == null || list.isEmpty()) {
            ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.practice_empty));
            mFrameLayout.setVisibility(View.GONE);
            llTaskEmpty.setVisibility(View.VISIBLE);
            return;
        }
        Collections.sort(list);
        TreeNode node = TreeNode.root();
        createKnowledgeTreeNodes(node, list);

        AndroidTreeView tView = new AndroidTreeView(activity, node);
        tView.setDefaultAnimation(true);
        tView.setDefaultNodeClickListener(this);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom, false);
        mFrameLayout.addView(tView.getView(R.style.TreeNodeStyle));
    }

    @Override
    public void getChapterSectionSuccess(List<ChapterSectionBean> list) {
        if (list == null || list.isEmpty()) {
            ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.practice_empty));
            mFrameLayout.setVisibility(View.GONE);
            llTaskEmpty.setVisibility(View.VISIBLE);
            return;
        }
        Collections.sort(list);
        TreeNode node = TreeNode.root();
        createTreeNodes(node, list);

        AndroidTreeView tView = new AndroidTreeView(activity, node);
        tView.setDefaultAnimation(true);
        tView.setDefaultNodeClickListener(this);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom, false);
        mFrameLayout.addView(tView.getView(R.style.TreeNodeStyle));
    }

    private void createTreeNodes(TreeNode treeNode, List<ChapterSectionBean> list) {
        for (ChapterSectionBean bean : list) {
            TreeNode node = initTreeNode(bean.getNodeId(), bean.getNodeName(), bean.getScore(), bean.getParentNodeId());
            treeNode.addChild(node);
            if (bean.getChildren() != null && !bean.getChildren().isEmpty()) {
                createTreeNodes(node, bean.getChildren());
            }
        }
    }

    //    private void createTreeNodes(TreeNode treeNode, List<KnowledgePointBean> list, long parentId) {
//        for (KnowledgePointBean bean : list) {
//            if (bean.getParent_node_id() == parentId) {
//                TreeNode node = initTreeNode(bean.getNode_id(), bean.getNode_name(), bean.getScore(), bean.getParent_node_id());
//                treeNode.addChild(node);
//                createTreeNodes(node, list, bean.getNode_id());
//            }
//        }
//    }
    private void createKnowledgeTreeNodes(TreeNode treeNode, List<KnowledgePointBean> list) {
        for (KnowledgePointBean bean : list) {
            TreeNode node = initTreeNode(Integer.valueOf(bean.getNo()), bean.getName(), bean.getScore(), bean.getParentId());
            treeNode.addChild(node);
            if (bean.getChildren() != null && !bean.getChildren().isEmpty()) {
                createKnowledgeTreeNodes(node, bean.getChildren());
            }

        }
    }

    private TreeNode initTreeNode(long id, String name, double score, long parentId) {
        return new TreeNode(new UpgradeTreeItemHolder.TreeItem(id, parentId, name, score))
                .setViewHolder(new UpgradeHeaderHolder(activity));
    }

    @Override
    public void networkError(String msg) {
        if (msg.equals("暂无数据")) {
            ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.kongbaiye_img));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text19));
            mFrameLayout.setVisibility(View.GONE);
            llTaskEmpty.setVisibility(View.VISIBLE);
            //暂无数据
        } else {
            //数据获取失败
            ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.no_data));
            tvTaskEmpty.setText(getResources().getString(R.string.fragment_main_text18));
            mFrameLayout.setVisibility(View.GONE);
            llTaskEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void noSubject() {
        ivTaskEmpty.setBackground(getResources().getDrawable(R.mipmap.kongbaiye_img));
        tvTaskEmpty.setText(getResources().getString(R.string.practice_empty));
        mFrameLayout.setVisibility(View.GONE);
        llTaskEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(TreeNode treeNode, Object o) {
        if ((treeNode.getChildren() == null || treeNode.getChildren().isEmpty()) && o instanceof UpgradeTreeItemHolder.TreeItem) {
            final long id = ((UpgradeTreeItemHolder.TreeItem) o).id;
            if (status == 0) {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        ContextManager.startUpgradeAnswer(activity, subjectId, id, 0, 0);
                    }
                });
            } else {
                SlbLoginUtil2.get().loginTowhere(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        ContextManager.startUpgradeAnswer(activity, subjectId, 0, id, 1);
                    }
                });
            }
        }
    }
}
