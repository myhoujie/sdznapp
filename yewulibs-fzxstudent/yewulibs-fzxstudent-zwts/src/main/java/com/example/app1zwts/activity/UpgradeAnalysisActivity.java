package com.example.app1zwts.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app1zwts.ContextManager;
import com.example.app1zwts.R;
import com.example.app1zwts.adapter.AnalysisAdapter;
import com.example.app1zwts.presenter.UpgradeAnalysisPresenter;
import com.example.app1zwts.view.UpgradeAnalysisView;
import com.sdzn.fzx.student.libbase.base.MBaseActivity;
import com.sdzn.fzx.student.libbase.holder.IconTreeItemHolder;
import com.sdzn.fzx.student.libbase.holder.SelectableHeaderHolder;
import com.sdzn.fzx.student.libpublic.views.MyGridLayoutManager;
import com.sdzn.fzx.student.libutils.util.SlbLoginUtil2;
import com.sdzn.fzx.student.libutils.util.ToastUtil;
import com.sdzn.fzx.student.utils.StudentSPUtils;
import com.sdzn.fzx.student.vo.AnalysisBean;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/*统计分析*/
public class UpgradeAnalysisActivity extends MBaseActivity<UpgradeAnalysisPresenter>
        implements UpgradeAnalysisView, View.OnClickListener {
    public static final String ID = "id";
    public static final String SUBJECT_ID = "subjectId";

    private TextView mTvBack;
    private TextView mTvTest;
    private RelativeLayout mLlPoint;
    private TextView tvDegree;
    private TextView tvAccuracy;
    private TextView tvErrors;
    private RelativeLayout mFlPoint;
    private RecyclerView mRecyclerView;


    private AnalysisAdapter mAdapter;
    private List<AnalysisBean.NameAccuracy> mList = new ArrayList<>();
    private List<AnalysisBean.Point> newPointList = new ArrayList<>();
    private String amount, errors, accuracy;
    private int subjectId;

    @Override
    public void initPresenter() {
        mPresenter = new UpgradeAnalysisPresenter();
        mPresenter.attachView(this, this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_analysis);
        initView();
        initData();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
//                    final boolean isGuidance = (boolean) StudentSPUtils.get(UpgradeAnalysisActivity.this, StudentSPUtils.GUIDANCE_TAG, false);
//                    final boolean isGuidance = SPUtils.getInstance().getBoolean(StudentSPUtils.GUIDANCE_TAG, false);
                    if (!SlbLoginUtil2.get().isUserLogin()) {
                        //isGuidance=true失败回退到activity
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.loginactivity?query1=" + aaaa + "&query2=" + bbbb));
                        startActivity(intent);
                    } else {
                        //isGuidance=false成功进入ctivity
                        ToastUtils.showLong("进入成功" + "query1->" + aaaa + ",query2->" + bbbb);
                    }
                }
            }
        }
    }

    @Override
    protected void initView() {
        mTvBack = (TextView) findViewById(R.id.tvBack);
        mTvTest = (TextView) findViewById(R.id.tv_test);
        mLlPoint = (RelativeLayout) findViewById(R.id.ll_point);
        tvDegree = (TextView) findViewById(R.id.tv_degree);
        tvAccuracy = (TextView) findViewById(R.id.tv_accuracy);
        tvErrors = (TextView) findViewById(R.id.tv_errors);
        mFlPoint = (RelativeLayout) findViewById(R.id.fl_point);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv);

        mTvBack.setOnClickListener(this);
        mTvTest.setOnClickListener(this);
        mRecyclerView.setLayoutManager(new MyGridLayoutManager(getApplication(), 1));
        mAdapter = new AnalysisAdapter(activity, mList);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.stopScroll();
    }

    @Override
    protected void initData() {
        String id = getIntent().getStringExtra(ID);
        subjectId = getIntent().getIntExtra(SUBJECT_ID, 0);
        mPresenter.loadData(id);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tvBack) {
            finish();

        } else if (i == R.id.tv_test) {
            SlbLoginUtil2.get().loginTowhere(activity, new Runnable() {
                @Override
                public void run() {
                    ContextManager.startUpgradeAnswer(activity, subjectId, 0, 0, 3);
                }
            });

        }
    }

    @Override
    public void loadDataSuccess(AnalysisBean bean) {
        if (bean == null) {
            return;
        }
        mLlPoint.setVisibility(View.VISIBLE);
        amount = bean.getQuesTotal();
        errors = bean.getErrorTotal();
        ///accuracy = bean.getErrorRate() == 0 ? "0.0%" : bean.getErrorRate() + "%";
        accuracy = bean.getRightRate() == 0 ? "0.0%" : bean.getRightRate() + "%";//正确率
        newPointList.clear();
        newPointList = builTree(bean.getPoint());

        Collections.sort(newPointList);
        TreeNode node = TreeNode.root();
        createTreeNodes(node, newPointList);
        AndroidTreeView tView = new AndroidTreeView(this, node);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom, false);

        mFlPoint.addView(tView.getView(R.style.TreeNodeStyle));
        tView.expandAll();

        List<AnalysisBean.Ability> abilitys = bean.getAbility();
        List<AnalysisBean.Method> methods = bean.getMethod();
        mList.clear();
        if (bean.getMethod().size() <= 0) {
            mList.add(new AnalysisBean.NameAccuracy("能力1"));
        } else {
            mList.add(new AnalysisBean.NameAccuracy("能力"));
        }
        if (abilitys != null && !abilitys.isEmpty()) {
            for (AnalysisBean.Ability ability : abilitys) {
                mList.add(new AnalysisBean.NameAccuracy(ability));
            }
        }
        if (bean.getMethod().size() <= 0) {
            mList.add(new AnalysisBean.NameAccuracy("方法1"));
        } else {
            mList.add(new AnalysisBean.NameAccuracy("方法"));
        }
        if (methods != null && !methods.isEmpty()) {
            for (AnalysisBean.Method method : methods) {
                mList.add(new AnalysisBean.NameAccuracy(method));
            }
        }
        mAdapter.refresh(accuracy);
        mAdapter.notifyDataSetChanged();
    }


    /**
     * 获取根节点
     */
    private List<AnalysisBean.Point> getList(List<AnalysisBean.Point> pointList) {
        List<Long> utilsPoint = new ArrayList<>();
        List<Long> parentPoint = new ArrayList<>();
        List<AnalysisBean.Point> parentPointList = new ArrayList<>();

        for (int i = 0; i < pointList.size() - 1; i++) {
            //内存循环，是每一轮中进行的两两比较
            for (int j = 0; j < pointList.size() - 1; j++) {
                if (pointList.get(i).getPointParentId() == pointList.get(j + 1).getPointId()) {
                    utilsPoint.add(pointList.get(i).getPointParentId());
                }
            }
        }
        for (AnalysisBean.Point point : pointList) {
            if (!utilsPoint.contains(point.getPointParentId())) {
                parentPoint.add(point.getPointParentId());
                point.setLast(false);
                parentPointList.add(point);
            }
        }

        return parentPointList;
    }

    /**
     * 建立树形结构
     */
    private List<AnalysisBean.Point> builTree(List<AnalysisBean.Point> pointList) {
        List<AnalysisBean.Point> tree = new ArrayList<>();
        for (AnalysisBean.Point point : getList(pointList)) {
            point.setLast(true);
            point = buildChilTree(point, pointList);
            tree.add(point);
        }
        return tree;
    }

    /**
     * 建立子树型结构
     */
    private AnalysisBean.Point buildChilTree(AnalysisBean.Point point, List<AnalysisBean.Point> pointList) {
        List<AnalysisBean.Point> child = new ArrayList<>();
        for (AnalysisBean.Point myPoint : pointList) {
            if (myPoint.getPointParentId() == point.getPointId()) {
                point.setLast(false);
                myPoint.setLast(true);
                child.add(buildChilTree(myPoint, pointList));
            }
        }
        point.setChildren(child);

        return point;
    }


    private void createTreeNodes(TreeNode treeNode, List<AnalysisBean.Point> points) {
        for (AnalysisBean.Point point : points) {
            TreeNode node = initTreeNode(point.getPointName(), point.getPointAccuracy(), point.isLast());
            treeNode.addChild(node);
            if (point != null && !point.getChildren().isEmpty()) {
                createTreeNodes(node, point.getChildren(), point.getPointId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<AnalysisBean.Point> points, long parentId) {
        for (AnalysisBean.Point point : points) {
            if (point.getPointParentId() == parentId) {
                TreeNode node = initTreeNode(point.getPointName(), point.getPointAccuracy(), point.isLast());
                treeNode.addChild(node);
                createTreeNodes(node, points, point.getPointId());
            }
        }
    }


    private TreeNode initTreeNode(String name, double score, boolean isLast) {
        //分数为0时不显示
        return new TreeNode(new IconTreeItemHolder.IconTreeItem(name, score == 0 ? "0.0%" : score + "%", isLast, amount, errors, accuracy))
                .setViewHolder(new SelectableHeaderHolder(this));
    }

    @Override
    public void loadDataFailed(String msg) {
        ToastUtil.showShortlToast(msg);
    }
}
