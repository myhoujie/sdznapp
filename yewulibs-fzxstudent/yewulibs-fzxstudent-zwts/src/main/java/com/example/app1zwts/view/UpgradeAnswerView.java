package com.example.app1zwts.view;


import com.sdzn.fzx.student.libbase.base.BaseView;
import com.sdzn.fzx.student.vo.UpgradeAnswerBean;

/**
 * 提升练习
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/8.
 */
public interface UpgradeAnswerView extends BaseView {

    void loadDataSuccess(UpgradeAnswerBean.Data data);

    void loadDataFailed(String msg);

    void submitSuccess();

    void submitFailed(String msg);
}
