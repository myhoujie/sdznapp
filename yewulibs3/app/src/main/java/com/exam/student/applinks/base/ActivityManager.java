package com.exam.student.applinks.base;

import android.app.Activity;
import android.content.Intent;

import com.exam.student.applinks.App;
import com.exam.student.applinks.countdowndemo.server.TimerService;

import java.util.ArrayList;

/**
 * 描述：维护Activity栈
 * <p>
 * 创建人：zhangchao
 * 创建时间：16/7/28
 */
public class ActivityManager {

    private static final ArrayList<Activity> ActivityList = new ArrayList<Activity>();

    public static void addActivity(Activity Activity) {
        if (!ActivityList.contains(Activity)) {
            ActivityList.add(Activity);
        }
    }
//    /**
//     * 获取栈顶的activity
//     *
//     * @return
//     */
//    public Activity top() {
//        if (liveActivityList.isEmpty()) {
//            return null;
//        }
//        return liveActivityList.peek();
//    }


    public static void removeActivity(Activity activity) {
        ActivityList.remove(activity);
    }


    public static void finishAll() {
        for (Activity activity : ActivityList) {
            activity.finish();
        }
    }

    public static void exit() {
        App.mContext.stopService(new Intent(App.mContext, TimerService.class));
        finishAll();
    }

    public static Activity getForegroundActivity() {
        if (ActivityList.size() > 0) {
            return ActivityList.get(ActivityList.size() - 1);
        }
        return null;
    }
}
