package com.exam.student.applinks.pictureselectordemo;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.exam.student.applinks.R;
import com.exam.student.applinks.pictureselectordemo.picture.listener.AlbumOrCameraListener;
import com.exam.student.applinks.pictureselectordemo.util.PopupwindowUtils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author：luck
 * @data：2019/12/20 晚上 23:12
 * @描述: Demo
 */

public class MultiPictureActivity extends AppCompatActivity {
    private ImageView head_img;
    private final static String TAG = MultiPictureActivity.class.getSimpleName();

    private static final int REQUECT_CODE_SDCARD = 1000;
    private static final int REQUECT_CODE_CAMERA = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_picture);
        head_img = findViewById(R.id.head_img);
        head_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MultiPhotoSelector.showSelectImgDialog(new AlbumOrCameraListener() {
                    @Override
                    public void selectAlbum() {
                        MultiPhotoSelector.toSystemAlbum(MultiPictureActivity.this);
                        requestPermissiontest();
                    }

                    @Override
                    public void selectCamera() {
                        MultiPhotoSelector.toSystemCamera(MultiPictureActivity.this);
                        requestPermissiontest1();
                    }
                }, MultiPictureActivity.this);
            }
        });
    }

    @Override
    protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回五种path
                    // 1.media.getPath(); 原图path
                    // 2.media.getCutPath();裁剪后path，需判断media.isCut();切勿直接使用
                    // 3.media.getCompressPath();压缩后path，需判断media.isCompressed();切勿直接使用
                    // 4.media.getOriginalPath()); media.isOriginal());为true时此字段才有值
                    // 5.media.getAndroidQToPath();Android Q版本特有返回的字段，但如果开启了压缩或裁剪还是取裁剪或压缩路径；注意：.isAndroidQTransform 为false 此字段将返回空
                    // 如果同时开启裁剪和压缩，则取压缩路径为准因为是先裁剪后压缩
                    for (LocalMedia media : selectList) {
                        Log.i(TAG, "是否压缩:" + media.isCompressed());
                        Log.i(TAG, "压缩:" + media.getCompressPath());
                        Log.i(TAG, "原图:" + media.getPath());
                        Log.i(TAG, "是否裁剪:" + media.isCut());
                        Log.i(TAG, "裁剪:" + media.getCutPath());
                        Log.i(TAG, "是否开启原图:" + media.isOriginal());
                        Log.i(TAG, "原图路径:" + media.getOriginalPath());
                        Log.i(TAG, "Android Q 特有Path:" + media.getAndroidQToPath());
                        Log.i(TAG, "宽高: " + media.getWidth() + "x" + media.getHeight());
                        Log.i(TAG, "Size: " + media.getSize());

                        Glide.with(this).load(media.getPath()).apply(new RequestOptions().error(R.mipmap.tx_img).transform()).into(head_img);
                        // TODO 可以通过PictureSelectorExternalUtils.getExifInterface();方法获取一些额外的资源信息，如旋转角度、经纬度等信息
                    }
                    break;
            }
        }

    }



    @AfterPermissionGranted(REQUECT_CODE_SDCARD)
    public void requestPermissiontest() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事

        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_SDCARD, perms);
        }
    }

    public void requestPermissiontest1() {
        String[] perms = {
                Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事

        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_CAMERA, perms);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}

