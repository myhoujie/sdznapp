package com.exam.student.applinks.updateappdemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.R;

import constant.UiType;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;

public class UpdateAppActivity extends BaseActivity implements View.OnClickListener {
    private Button BtnUpdateapp;
    private String apkUrl = "http://118.24.148.250:8080/yk/update_signed.apk";
    private String updateTitle = "发现新版本V2.0.0";
    private String updateContent = "1、Kotlin重构版\n2、支持自定义UI\n3、增加md5校验\n4、更多功能等你探索";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateapp);
        BtnUpdateapp = findViewById(R.id.btn_updateapp);
        BtnUpdateapp.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_updateapp:
                /*属性配置*/
                UpdateConfig updateConfig = new UpdateConfig();
                updateConfig.setCheckWifi(true);
                updateConfig.setNeedCheckMd5(false);
                updateConfig.setNotifyImgRes(com.teprinciple.updateapputils.R.drawable.ic_logo);
                updateConfig.setApkSaveName("时代智囊");

                /*基础UI配置*/
                UiConfig uiConfig = new UiConfig();
                uiConfig.setUiType(UiType.PLENTIFUL);

                UpdateAppUtils
                        .getInstance()
                        .apkUrl(apkUrl)
                        .updateTitle(updateTitle)
                        .updateConfig(updateConfig)
                        .uiConfig(uiConfig)
                        .updateContent(updateContent)
                        .update();
                break;
        }
    }
}
