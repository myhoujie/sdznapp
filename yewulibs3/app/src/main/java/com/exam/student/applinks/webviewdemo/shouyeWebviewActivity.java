package com.exam.student.applinks.webviewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.exam.student.applinks.R;

/**
 * @author：luck
 * @data：2019/12/20 晚上 23:12
 * @描述: Demo
 */

public class shouyeWebviewActivity extends AppCompatActivity {
    private Button btnBasicUsage;
    private Button btnJsInteractive;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shouye_web);
        btnBasicUsage = (Button) findViewById(R.id.btn_basic_usage);
        btnJsInteractive = (Button) findViewById(R.id.btn_js_interactive);
        btnBasicUsage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(shouyeWebviewActivity.this, DemoWebviewActivity.class);
                intent.putExtra("url_key", "https://www.baidu.com/");
                startActivity(intent);
            }
        });
        btnJsInteractive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(shouyeWebviewActivity.this, JSDemoWebviewActivity.class);
                intent.putExtra("url_key", "file:///android_asset/js_interaction/hello.html");
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

