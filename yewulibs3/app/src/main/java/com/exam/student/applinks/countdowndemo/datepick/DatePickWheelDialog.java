package com.exam.student.applinks.countdowndemo.datepick;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.exam.student.applinks.R;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class DatePickWheelDialog extends Dialog implements
        View.OnClickListener {

    private static int START_YEAR = 1990, END_YEAR = 2100;
    private WheelView wv_year;
    private WheelView wv_month;
    private WheelView wv_day;
    private WheelView wv_hour;
    private WheelView wv_min;
    private TextView tvTitle;

    String[] months_big = {"1", "3", "5", "7", "8", "10", "12"};
    String[] months_little = {"4", "6", "9", "11"};

    final List<String> list_big = Arrays.asList(months_big);
    final List<String> list_little = Arrays.asList(months_little);
    private final Context mContext;
    private Button btn_sure;
    private CharSequence positiveText;
    private Button btn_cancel;
    private CharSequence negativeText;
    private Calendar calendar;
    private OnDatePickListener positiveClickListener;
    private OnDatePickListener negativeClickListener;

    private DatePickWheelDialog(Context context) {
        super(context, R.style.MyDialog);
        this.mContext = context;
    }

    private DatePickWheelDialog(Context context, Calendar instance) {
        this(context);
        calendar = instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.view_select_date);
        findView();
        adjustView();
        setListener();
        setDate(calendar, R.id.timePicker);
    }

    private void adjustView() {
        // 根据屏幕密度来指定选择器字体的大小
        int textSize = 0;

        textSize = pixelsToDip(mContext.getResources(), 20);

        wv_day.TEXT_SIZE = textSize;
        wv_month.TEXT_SIZE = textSize;
        wv_year.TEXT_SIZE = textSize;
        wv_hour.TEXT_SIZE = textSize;
        wv_min.TEXT_SIZE = textSize;

    }

    public static int pixelsToDip(Resources res, int pixels) {
        final float scale = res.getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }

    private void setListener() {
        wv_year.addChangingListener(wheelListener_year);
        wv_month.addChangingListener(wheelListener_month);
        wv_day.addChangingListener(wheelListener_day);
        wv_hour.addChangingListener(wheelListener_hour);
        wv_min.addChangingListener(wheelListener_min);

        if (negativeClickListener != null) {
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    negativeClickListener.onClick(DatePickWheelDialog.this);
                }
            });
        } else {
            btn_cancel.setOnClickListener(dismissListener);
        }
        if (positiveClickListener != null) {
            btn_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    positiveClickListener.onClick(DatePickWheelDialog.this);
                }
            });
        } else {
            btn_sure.setOnClickListener(dismissListener);
        }

    }

    private final View.OnClickListener dismissListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    public void setOnlyShowYear() {
        wv_year.setVisibility(View.VISIBLE);
        wv_month.setVisibility(View.GONE);
        wv_day.setVisibility(View.GONE);
        wv_hour.setVisibility(View.GONE);
        wv_min.setVisibility(View.GONE);
    }

    public void setOnlyShowDate() {
        wv_year.setVisibility(View.VISIBLE);
        wv_month.setVisibility(View.VISIBLE);
        wv_day.setVisibility(View.VISIBLE);
        wv_hour.setVisibility(View.GONE);
        wv_min.setVisibility(View.GONE);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    private void findView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        wv_year = (WheelView) findViewById(R.id.year);
        wv_year.setAdapter(new NumericWheelAdapter(START_YEAR, END_YEAR));// 设置"年"的显示数据
        wv_year.setLabel("年");

        wv_month = (WheelView) findViewById(R.id.month);
        wv_month.setAdapter(new NumericWheelAdapter(1, 12));
        wv_month.setLabel("月");

        wv_day = (WheelView) findViewById(R.id.day);
        wv_day.setLabel("日");

        wv_hour = (WheelView) findViewById(R.id.hour);
        wv_hour.setAdapter(new NumericWheelAdapter(0, 24));
        wv_hour.setLabel(":");

        wv_min = (WheelView) findViewById(R.id.min);
        wv_min.setAdapter(new NumericWheelAdapter(0, 60));
        wv_min.setLabel("时");

        btn_sure = (Button) findViewById(R.id.select_confirm);
        if (positiveText != null) {
            btn_sure.setVisibility(View.VISIBLE);
            btn_sure.setText(positiveText);
        }
        btn_cancel = (Button) findViewById(R.id.select_cancel);
        if (negativeText != null) {
            btn_cancel.setVisibility(View.VISIBLE);
            btn_cancel.setText(negativeText);
        }

    }
    //private int start_year_num;
    //private int start_month_num;
    //private int start_day_num;
    //private int start_hour_num;
    //private int start_min_num;


    // 添加"年"监听
    private final OnWheelChangedListener wheelListener_year = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            int year_num = newValue + START_YEAR;
            //start_year_num = year_num;
            // StringBuilder sb = new StringBuilder();
            // mStartTextView.setText(text)
            // 判断大小月及是否闰年,用来确定"日"的数据
            if (list_big
                    .contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 31));
            } else if (list_little.contains(String.valueOf(wv_month
                    .getCurrentItem() + 1))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 30));
            } else {
                if ((year_num % 4 == 0 && year_num % 100 != 0)
                        || year_num % 400 == 0)
                    wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                else
                    wv_day.setAdapter(new NumericWheelAdapter(1, 28));
            }
        }
    };
    // 添加"月"监听
    private final OnWheelChangedListener wheelListener_month = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            int month_num = newValue + 1;
            //start_month_num = month_num;

            // 判断大小月及是否闰年,用来确定"日"的数据
            if (list_big.contains(String.valueOf(month_num))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 31));
            } else if (list_little.contains(String.valueOf(month_num))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 30));
            } else {
                if (((wv_year.getCurrentItem() + START_YEAR) % 4 == 0 && (wv_year
                        .getCurrentItem() + START_YEAR) % 100 != 0)
                        || (wv_year.getCurrentItem() + START_YEAR) % 400 == 0)
                    wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                else
                    wv_day.setAdapter(new NumericWheelAdapter(1, 28));
            }
        }
    };

    // 添加"日"监听
    private final OnWheelChangedListener wheelListener_day = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            //int day_num = newValue + 1;
            //start_day_num = day_num;
            // 判断大小月及是否闰年,用来确定"日"的数据
            // if (list_big.contains(String.valueOf(month_num))) {
            // wv_day.setAdapter(new NumericWheelAdapter(1, 31));
            // } else if (list_little.contains(String.valueOf(month_num))) {
            // wv_day.setAdapter(new NumericWheelAdapter(1, 30));
            // } else {
            // if (((wv_year.getCurrentItem() + START_YEAR) % 4 == 0 && (wv_year
            // .getCurrentItem() + START_YEAR) % 100 != 0)
            // || (wv_year.getCurrentItem() + START_YEAR) % 400 == 0)
            // wv_day.setAdapter(new NumericWheelAdapter(1, 29));
            // else
            // wv_day.setAdapter(new NumericWheelAdapter(1, 28));
            // }
        }
    };

    //添加"时"监听
    private final OnWheelChangedListener wheelListener_hour = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            //int hour_num = newValue + 1;
            //start_hour_num = hour_num;
        }
    };

    //添加"时"监听
    private final OnWheelChangedListener wheelListener_min = new OnWheelChangedListener() {
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            //int min_num = newValue + 1;
            //start_min_num = min_num;
        }
    };

    private void setPositiveButton(CharSequence mPositiveButtonText,
                                   OnDatePickListener onClickListener) {
        positiveText = mPositiveButtonText;
        positiveClickListener = onClickListener;// can't use btn_sure here
        // because it's on defined yet
    }

    private void setNegativeButton(CharSequence mNegativeButtonText,
                                   OnDatePickListener onClickListener) {
        negativeText = mNegativeButtonText;
        negativeClickListener = onClickListener;// can't use btn_sure here
        // because it's on defined yet
    }

    private void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public DatePickWheelDialog setDate(Calendar calendar, int which) {
        if (calendar == null)
            return this;
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        if (which == R.id.timePicker) {
            wv_year.setCurrentItem(year - START_YEAR);
            wv_month.setCurrentItem(month);
            if (list_big.contains(String.valueOf(month + 1))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 31));
            } else if (list_little.contains(String.valueOf(month + 1))) {
                wv_day.setAdapter(new NumericWheelAdapter(1, 30));
            } else {
                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
                    wv_day.setAdapter(new NumericWheelAdapter(1, 29));
                else
                    wv_day.setAdapter(new NumericWheelAdapter(1, 28));
            }
            wv_day.setCurrentItem(day - 1);
            wv_hour.setCurrentItem(hour);
            wv_min.setCurrentItem(min);
        }
        return this;
    }

    public Calendar getSetCalendar(int which) {
        Calendar c = Calendar.getInstance();
//		c.set(wv_year.getCurrentItem() + START_YEAR,
//				wv_month.getCurrentItem(), wv_day.getCurrentItem() + 1);
        c.set(wv_year.getCurrentItem() + START_YEAR,
                wv_month.getCurrentItem(), wv_day.getCurrentItem() + 1,
                wv_hour.getCurrentItem(), wv_min.getCurrentItem());
        return c;
    }

    public static class Builder {
        private final DatePickParams P;

        public Builder(Context context) {
            P = new DatePickParams(context);
        }

        public Builder setTitle(CharSequence title) {
            P.mTitle = title;
            return this;
        }

        public Builder setIcon(int iconId) {
            P.mIconId = iconId;
            return this;
        }

        public Builder setPositiveButton(CharSequence text,
                                         final OnDatePickListener listener) {
            P.mPositiveButtonText = text;
            P.mPositiveButtonListener = listener;
            return this;
        }

        public Builder setNegativeButton(CharSequence text,
                                         final OnDatePickListener listener) {
            P.mNegativeButtonText = text;
            P.mNegativeButtonListener = listener;
            return this;
        }

        public DatePickWheelDialog create() {
            final DatePickWheelDialog dialog = new DatePickWheelDialog(
                    P.mContext);
            P.apply(dialog);
            return dialog;
        }
    }

    public static class DatePickParams {
        public int mIconId;
        public OnDatePickListener mPositiveButtonListener;
        public CharSequence mPositiveButtonText;
        public CharSequence mTitle;
        public final Context mContext;
        public Calendar calendar;
        private CharSequence mNegativeButtonText;
        private OnDatePickListener mNegativeButtonListener;

        public DatePickParams(Context context) {
            mContext = context;
            calendar = Calendar.getInstance();
        }

        public DatePickParams(Context context, Calendar calendar) {
            mContext = context;
            this.calendar = calendar;
        }

        public void apply(DatePickWheelDialog dialog) {
            if (mTitle != null) {
                dialog.setTitle(mTitle);
            }

            if (mPositiveButtonText != null) {
                dialog.setPositiveButton(mPositiveButtonText,
                        mPositiveButtonListener);
            }
            if (mNegativeButtonText != null) {
                dialog.setNegativeButton(mNegativeButtonText,
                        mNegativeButtonListener);
            }
            if (calendar != null)
                dialog.setCalendar(calendar);

        }
    }

    @Override
    public void onClick(View v) {

    }
}
