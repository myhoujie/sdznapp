package com.exam.student.applinks.recyclerDemo.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.exam.student.applinks.R;
import com.exam.student.applinks.recyclerDemo.modle.TextModel;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<TextModel> mTextModels;

    public MyAdapter(Context mContext, List<TextModel> mTextModels) {
        this.mContext = mContext;
        this.mTextModels = mTextModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_view2, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.e("ref", "position: " + position);
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        TextModel textModel = getItem(position);
        myViewHolder.tv.setText(textModel.getTextTitle());
    }

    @Override
    public int getItemCount() {
        return mTextModels.size();
    }


    public TextModel getItem(int position) {
        return mTextModels.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv;

        MyViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.item_tv);
        }
    }
}
