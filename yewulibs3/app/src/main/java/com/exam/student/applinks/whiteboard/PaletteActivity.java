package com.exam.student.applinks.whiteboard;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：白板界面
 * -
 * 创建人：wangchunxiao
 * 创建时间：2017/3/17
 */
public class PaletteActivity extends BaseActivity {

    private List<Fragment> fragments = new ArrayList<>();
    private Fragment currFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palette);
        initView();
    }

    protected void initView() {
        Bundle bundle = new Bundle();
        bundle.putString("type", getIntent().getStringExtra("type"));
        fragments.add(WhiteboardFragment.newInstance(bundle));

        showFragment(0);
    }

    private void showFragment(int fragmentIndex) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = fragments.get(fragmentIndex);
        if (currFragment != null) {
            if (currFragment != fragment) {
                if (!fragment.isAdded()) {
                    ft.hide(currFragment).add(R.id.framelayout, fragment, fragment.getClass().getName());
                } else {
                    ft.hide(currFragment).show(fragment);
                }
            }
        } else {
            ft.add(R.id.framelayout, fragment, fragment.getClass().getName());
        }
        currFragment = fragment;
        ft.commit();
    }
}