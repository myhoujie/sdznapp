package com.exam.student.applinks.whiteboard.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.exam.student.applinks.util.UiUtils;
import com.exam.student.applinks.whiteboard.listener.OnPaletteTouchListener;
import com.exam.student.applinks.whiteboard.listener.PathStackWatcher;
import com.luck.picture.lib.tools.ScreenUtils;

import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Created by huangziwei on 2016/9/3.
 */
public class GraffitiView extends View {

    public static final int ERROR_INIT = -1;
    public static final int ERROR_SAVE = -2;

    private static final float VALUE = 1f;
    private static final float MOVE_VALUE = 5f;
    private final int TIME_SPAN = 80;

    private Context mContext;

    private Bitmap mBitmap; // 原图
    private Bitmap mBitmapEraser; // 橡皮擦底图
    private Bitmap mGraffitiBitmap; // 用绘制涂鸦的图片
    private Canvas mBitmapCanvas;

    private float mPrivateScale; // 图片适应屏幕（mScale=1）时的缩放倍数
    private int mPrivateHeight, mPrivateWidth;// 图片在缩放mPrivateScale倍数的情况下，适应屏幕（mScale=1）时的大小（肉眼看到的在屏幕上的大小）
    private float mCentreTranX, mCentreTranY;// 图片在缩放mPrivateScale倍数的情况下，居中（mScale=1）时的偏移（肉眼看到的在屏幕上的偏移）

    private BitmapShader mBitmapShader; // 用于涂鸦的图片上!
    private BitmapShader mBitmapShaderEraser; // 橡皮擦底图
    private Path mCurrPath; // 当前手写的路径
    private Path mTempPath;

    private Paint mPaint;
    private int mTouchMode; // 触摸模式，用于判断单点或多点触摸
    private float mPaintSize;//画笔大小
    private float mArrowSize;//箭头大小
    private float mShapeSize;//形状大小
    private float mEraserSize;//橡皮大小
    private GraffitiColor mColor; // 画笔底色
    private float mScale; // 图片在相对于居中时的缩放倍数 （ 图片真实的缩放倍数为 mPrivateScale*mScale ）

    private float mTransX = 0, mTransY = 0; // 图片在相对于居中时且在缩放mScale倍数的情况下的偏移量 （ 图片真实偏移量为　(mCentreTranX + mTransX)/mPrivateScale*mScale ）

/*
      明白下面一点，对于理解涂鸦坐标系很重要：
      假设不考虑任何缩放，图片就是肉眼看到的那么大，此时图片的大小width =  mPrivateWidth * mScale ,
      偏移量x = mCentreTranX + mTransX，而view的大小为width = getWidth()。height和偏移量y以此类推。
*/

    private boolean mIsPainting = false; // 是否正在绘制
    private boolean isJustDrawOriginal; // 是否只绘制原图

    private boolean mIsDrawableOutside = false; // 触摸时，图片区域外是否绘制涂鸦轨迹
    private boolean mEraserImageIsResizeable = false;
    private boolean mReady = false;


    // 保存涂鸦操作，便于撤销
    private CopyOnWriteArrayList<GraffitiPath> mPathStack = new CopyOnWriteArrayList<GraffitiPath>();
    //保存撤销操作，便于恢复
    private CopyOnWriteArrayList<GraffitiPath> mRepealStack = new CopyOnWriteArrayList<>();
//    private CopyOnWriteArrayList<GraffitiPath> mPathStackBackup = new CopyOnWriteArrayList<GraffitiPath>();

    private Pen mPen;
    private Shape mShape;

    private float mTouchDownX;
    private float mTouchDownY;
    private float mLastTouchX;
    private float mLastTouchY;
    private float mLastTouchYprivate;
    private float mTouchX;
    private float mTouchY;
    private Matrix mShaderMatrix;
    private Matrix mMatrixTemp;

    private float mAmplifierRadius;
    private Path mAmplifierPath;
    private float mAmplifierScale = 0; // 放大镜的倍数
    private Paint mAmplifierPaint;
    private int mAmplifierHorizonX; // 放大器的位置的x坐标，使其水平居中
    private boolean isPaintAble = true;//画笔是否可用
    private OnPaletteTouchListener onPaletteTouchListener;
    private boolean showTools = true;
    private boolean isMoving = false;
    private MyHandler myHandler;

    public GraffitiView(Context context) {
        this(context, null);
    }

    public GraffitiView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraffitiView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GraffitiView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() throws Exception {
        // 关闭硬件加速，因为bitmap的Canvas不支持硬件加速
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        if (mBitmap == null) {
            int width = displayMetrics.widthPixels;
            int height = mContext.getResources().getDisplayMetrics().heightPixels;
            mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            if (mBitmap == null) {
                throw new Exception("mBitmap create failed");
            }
            mBitmap.eraseColor(Color.WHITE);//填充颜色
        }

        mScale = 1f;
        mPaintSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, displayMetrics);
        mArrowSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, displayMetrics);
        mShapeSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, displayMetrics);
        mEraserSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, displayMetrics);
        mColor = new GraffitiColor(Color.RED);
        mPaint = new Paint();
        mPaint.setStrokeWidth(mPaintSize);
        mPaint.setColor(mColor.getColor());
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);// 圆滑

        mPen = Pen.HAND;
        mShape = Shape.HAND_WRITE;

        this.mBitmapShader = new BitmapShader(this.mBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        if (mBitmapEraser != null) {
            this.mBitmapShaderEraser = new BitmapShader(this.mBitmapEraser, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        } else {
            this.mBitmapShaderEraser = mBitmapShader;
        }

        mShaderMatrix = new Matrix();
        mMatrixTemp = new Matrix();
        mTempPath = new Path();

        mAmplifierPaint = new Paint();
        mAmplifierPaint.setColor(0xaaffffff);
        mAmplifierPaint.setStyle(Paint.Style.STROKE);
        mAmplifierPaint.setAntiAlias(true);
        mAmplifierPaint.setStrokeJoin(Paint.Join.ROUND);
        mAmplifierPaint.setStrokeCap(Paint.Cap.ROUND);// 圆滑
        mAmplifierPaint.setStrokeWidth(ScreenUtils.dip2px(mContext, 10));

        myHandler = new MyHandler();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        try {
            setBG();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!mReady) {
            setPen(Pen.HAND);
            setShape(Shape.HAND_WRITE);
            mReady = true;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mBitmap == null || mGraffitiBitmap == null || mBitmap.isRecycled() || mGraffitiBitmap.isRecycled()) {
            return;
        }

        canvas.save();
        try {
            doDraw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        canvas.restore();

        if (mAmplifierScale > 0) { //启用放大镜
            canvas.save();

            if (mTouchY <= mAmplifierRadius * 2) { // 在放大镜的范围内， 把放大镜仿制底部
                canvas.translate(mAmplifierHorizonX, getHeight() - mAmplifierRadius * 2);
            } else {
                canvas.translate(mAmplifierHorizonX, 0);
            }
            canvas.clipPath(mAmplifierPath);
            canvas.drawColor(0xff000000);

            canvas.save();
            float scale = mAmplifierScale / mScale; // 除以mScale，无论当前图片缩放多少，都产生图片在居中状态下缩放mAmplifierScale倍的效果
            canvas.scale(scale, scale);
            canvas.translate(-mTouchX + mAmplifierRadius / scale, -mTouchY + mAmplifierRadius / scale);
            try {
                doDraw(canvas);
            } catch (Exception e) {
                e.printStackTrace();
            }
            canvas.restore();

            // 画放大器的边框
            DrawUtil.drawCircle(canvas, mAmplifierRadius, mAmplifierRadius, mAmplifierRadius, mAmplifierPaint);
            canvas.restore();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                touchDown(touchX, touchY);
                return true;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                touchUp(touchX, touchY);
                return true;
            case MotionEvent.ACTION_MOVE:
                touchMove(touchX, touchY);
                return true;
            case MotionEvent.ACTION_POINTER_UP:
                mTouchMode -= 1;

                invalidate();
                return true;
            case MotionEvent.ACTION_POINTER_DOWN:
                mTouchMode += 1;

                invalidate();
                return true;
        }
        return super.onTouchEvent(event);
    }

    public void setOnPaletteTouchListener(OnPaletteTouchListener listener) {
        this.onPaletteTouchListener = listener;
    }

    private void touchDown(float touchX, float touchY) {
        myHandler.removeCallbacksAndMessages(null);
        mTouchMode = 1;
        mTouchDownX = mTouchX = mLastTouchX = touchX;
        mTouchDownY = mTouchY = mLastTouchY = touchY;
        mCurrPath = new Path();
        mCurrPath.moveTo(toX(mTouchDownX), toY(mTouchDownY));
        mIsPainting = true;
    }

    private void touchMove(float touchX, float touchY) {

        if (Math.abs(touchX - mLastTouchX) < MOVE_VALUE && Math.abs(touchY - mLastTouchY) < MOVE_VALUE) {
            return;
        }
        if (!isMoving) {
            if (onPaletteTouchListener != null) {
                onPaletteTouchListener.hide();
            }
            isMoving = true;
        }

        mLastTouchX = mTouchX;
        mLastTouchY = mTouchY;
        mTouchX = touchX;
        mTouchY = touchY;

        if (mShape == Shape.HAND_WRITE) { // 手写
            mCurrPath.quadTo(
                    toX(mLastTouchX),
                    toY(mLastTouchY),
                    toX((mTouchX + mLastTouchX) / 2),
                    toY((mTouchY + mLastTouchY) / 2));
        } else { // 画图形

        }
        invalidate();
    }

    class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (onPaletteTouchListener != null) {
                onPaletteTouchListener.show();
            }
            super.handleMessage(msg);
        }
    }

    private void touchUp(float touchX, float touchY) {
        if (!isMoving) {
            // 单击
            if (showTools) {
                showTools = false;
                if (onPaletteTouchListener != null) {
                    onPaletteTouchListener.hide();
                }
            } else {
                showTools = true;
                if (onPaletteTouchListener != null) {
                    onPaletteTouchListener.show();
                }
            }
        } else {
            if (showTools) {
                myHandler.sendEmptyMessageDelayed(0, 500);
            }
        }
        isMoving = false;

        if (Math.abs(touchX - mTouchDownX) < MOVE_VALUE && Math.abs(touchY - mTouchDownY) < MOVE_VALUE) {
            mTouchDownX = -1;
            return;
        }

        mTouchMode = 0;
        mLastTouchX = mTouchX;
        mLastTouchY = mTouchY;
        mTouchX = touchX;
        mTouchY = touchY;

        // 为了仅点击时也能出现绘图，必须移动path
        if (mTouchDownX == mTouchX && mTouchDownY == mTouchY & mTouchDownX == mLastTouchX && mTouchDownY == mLastTouchY) {
            mTouchX += VALUE;
            mTouchY += VALUE;
        }

        if (mIsPainting && isPaintAble) {
            GraffitiPath path = null;

            // 把操作记录到加入的堆栈中
            if (mShape == Shape.HAND_WRITE) { // 手写
                mCurrPath.quadTo(
                        toX(mLastTouchX),
                        toY(mLastTouchY),
                        toX((mTouchX + mLastTouchX) / 2),
                        toY((mTouchY + mLastTouchY) / 2));
                path = GraffitiPath.toPath(mPen, mShape, mPaintSize, mColor.copy(), mCurrPath, mPen == Pen.COPY ? new Matrix(mShaderMatrix) : null);
            } else {  // 画图形
                path = GraffitiPath.toShape(mPen, mShape, mPaintSize, mColor.copy(),
                        toX(mTouchDownX), toY(mTouchDownY), toX(mTouchX), toY(mTouchY),
                        mPen == Pen.COPY ? new Matrix(mShaderMatrix) : null);
            }
            mPathStack.add(path);
            if (pathStackWatcher != null) {
                pathStackWatcher.pathStackChange(mPathStack.size());
            }
            try {
                draw(mBitmapCanvas, path); // 保存到图片中
            } catch (Exception e) {
                e.printStackTrace();
            }
            mIsPainting = false;

        }

        invalidate();
    }

    private void setBG() throws Exception {
        mPrivateHeight = getHeight();
        mPrivateWidth = getWidth();
        mCentreTranX = 0;
        mCentreTranY = 0;
        mPrivateScale = 1f;
        mPrivateScale = 1f;
//        mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mPrivateWidth, mPrivateHeight);
//        if (mPrivateHeight > mPrivateWidth) {
//            Matrix m = new Matrix();
//            m.postRotate(90);
//            mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(),
//                    mBitmap.getHeight(), m, false);
//
//        }
        if (mBitmap != null) {
            mBitmap = Bitmap.createScaledBitmap(mBitmap, mPrivateWidth, mPrivateHeight, false);
        }
        if (mBitmap == null) {
            throw new Exception("mBitmap create failed");
        }
        initCanvas();
        resetMatrix();

        mAmplifierRadius = Math.min(getWidth(), getHeight()) / 4;
        mAmplifierPath = new Path();
        mAmplifierPath.addCircle(mAmplifierRadius, mAmplifierRadius, mAmplifierRadius, Path.Direction.CCW);
        mAmplifierHorizonX = (int) (Math.min(getWidth(), getHeight()) / 2 - mAmplifierRadius);
        this.mBitmapShader = new BitmapShader(this.mBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        this.mBitmapShaderEraser = mBitmapShader;
        postInvalidate();
    }


    private void doDraw(Canvas canvas) throws Exception {
//        float left = (mCentreTranX + mTransX) / (mPrivateScale * mScale);
//        float top = (mCentreTranY + mTransY) / (mPrivateScale * mScale);
//        // 画布和图片共用一个坐标系，只需要处理屏幕坐标系到图片（画布）坐标系的映射关系
//        canvas.scale(mPrivateScale * mScale, mPrivateScale * mScale); // 缩放画布
//        canvas.translate(left, top); // 偏移画布

        Rect rect = new Rect(0, 0, getWidth(), getHeight());
        if (!mIsDrawableOutside) { // 裁剪绘制区域为图片区域
            canvas.clipRect(rect);
        }
        if (isJustDrawOriginal) { // 只绘制原图
            if (mBitmap == null) {
                throw new Exception("mBitmap is null");
            }
            canvas.drawBitmap(mBitmap, rect, rect, null);
            return;
        }

        // 绘制涂鸦
        canvas.drawBitmap(mGraffitiBitmap, rect, rect, null);

        if (mIsPainting && isPaintAble) {  //画在view的画布上
            Path path;
            float span;
            // 为了仅点击时也能出现绘图，必须移动path
            if (mTouchDownX == mTouchX && mTouchDownY == mTouchY && mTouchDownX == mLastTouchX && mTouchDownY == mLastTouchY) {
                mTempPath.reset();
                mTempPath.addPath(mCurrPath);
                mTempPath.quadTo(
                        toX(mLastTouchX),
                        toY(mLastTouchY),
                        toX((mTouchX + mLastTouchX + VALUE) / 2),
                        toY((mTouchY + mLastTouchY + VALUE) / 2));
                path = mTempPath;
                span = VALUE;
            } else {
                path = mCurrPath;
                span = 0;
            }
            // 画触摸的路径
            mPaint.setStrokeWidth(mPaintSize);
            if (mShape == Shape.HAND_WRITE) { // 手写
                draw(canvas, mPen, mPaint, path, mShaderMatrix, mColor);
            } else {  // 画图形
                draw(canvas, mPen, mShape, mPaint,
                        toX(mTouchDownX), toY(mTouchDownY), toX(mTouchX + span), toY(mTouchY + span), mShaderMatrix, mColor);
            }
        }


    }

    private void draw(Canvas canvas, Pen pen, Paint paint, Path path, Matrix matrix, GraffitiColor color) throws Exception {
        resetPaint(pen, paint, matrix, color);

        paint.setStyle(Paint.Style.STROKE);
        if (canvas == null) {
            throw new Exception("canvas is null");
        }
        canvas.drawPath(path, paint);
    }

    private void draw(Canvas canvas, Pen pen, Shape shape, Paint paint, float sx, float sy, float dx, float dy, Matrix matrix, GraffitiColor color) {
        resetPaint(pen, paint, matrix, color);

        paint.setStyle(Paint.Style.STROKE);
        if (shape == Shape.ARROW) {
            paint.setStrokeWidth(mArrowSize);
        } else {
            paint.setStrokeWidth(mShapeSize);
        }
        switch (shape) { // 绘制图形
            case ARROW:
                paint.setStyle(Paint.Style.FILL);
                DrawUtil.drawArrow(canvas, sx, sy, dx, dy, paint);
                break;
            case LINE:
                DrawUtil.drawLine(canvas, sx, sy, dx, dy, paint);
                break;
            case DASHED:
                DrawUtil.drawDashed(canvas, sx, sy, dx, dy, paint);
                break;
            case FILL_CIRCLE:
                paint.setStyle(Paint.Style.FILL);
            case HOLLOW_CIRCLE:
                DrawUtil.drawCircle(canvas, sx, sy,
                        (float) Math.sqrt((sx - dx) * (sx - dx) + (sy - dy) * (sy - dy)), paint);
                break;
            case OVAL:
                DrawUtil.drawOval(canvas, sx, sy, dx, dy, paint);
                break;
            case FILL_RECT:
                paint.setStyle(Paint.Style.FILL);
            case HOLLOW_RECT:
                DrawUtil.drawRect(canvas, sx, sy, dx, dy, paint);
                break;
            case SQUARE:
                DrawUtil.drawSquare(canvas, sx, sy, dx, dy, paint);
                break;
            case TRIANGLE:
                DrawUtil.drawTriangle(canvas, sx, sy, dx, dy, paint);
                break;
            default:
                throw new RuntimeException("unknown shape:" + shape);
        }
    }


    private void draw(Canvas canvas, CopyOnWriteArrayList<GraffitiPath> pathStack) {
        // 还原堆栈中的记录的操作
        for (GraffitiPath path : pathStack) {
            try {
                draw(canvas, path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void draw(Canvas canvas, GraffitiPath path) throws Exception {
        mPaint.setStrokeWidth(path.getStrokeWidth());
        if (path.getShape() == Shape.HAND_WRITE) { // 手写
            draw(canvas, path.getPen(), mPaint, path.getPath(), path.getMatrix(), path.getColor());
        } else { // 画图形
            draw(canvas, path.getPen(), path.getShape(), mPaint, path.getSx(), path.getSy(),
                    path.getDx(), path.getDy(), path.getMatrix(), path.getColor());
        }
    }

    private void resetPaint(Pen pen, Paint paint, Matrix matrix, GraffitiColor color) {
        switch (pen) { // 设置画笔
            case HAND:
                paint.setShader(null);
                paint.setStrokeWidth(mPaintSize);
                color.initColor(paint, null);
                paint.setAlpha(255);
                break;
            case HIGHLIGHTER:
                paint.setShader(null);
                paint.setStrokeWidth(mPaintSize);
                color.initColor(paint, null);
                paint.setAlpha(120);
                break;
            case COPY:
                // 调整copy图片位置
                mBitmapShader.setLocalMatrix(matrix);
                paint.setShader(this.mBitmapShader);
                break;
            case ERASER:
                paint.setAlpha(255);
                paint.setStrokeWidth(mEraserSize);
                if (mBitmapShader == mBitmapShaderEraser) { // 图片的矩阵不需要任何偏移
                    mBitmapShaderEraser.setLocalMatrix(null);
                }
                paint.setShader(this.mBitmapShaderEraser);
                break;
        }
    }


    /**
     * 将屏幕触摸坐标x转换成在图片中的坐标
     */
    public final float toX(float touchX) {
        return (touchX - mCentreTranX - mTransX) / (mPrivateScale * mScale);
    }

    /**
     * 将屏幕触摸坐标y转换成在图片中的坐标
     */
    public final float toY(float touchY) {
        return (touchY - mCentreTranY - mTransY) / (mPrivateScale * mScale);
    }

    /**
     * 坐标换算
     * （公式由toX()中的公式推算出）
     *
     * @param touchX    触摸坐标
     * @param graffitiX 在涂鸦图片中的坐标
     * @return 偏移量
     */
    public final float toTransX(float touchX, float graffitiX) {
        return -graffitiX * (mPrivateScale * mScale) + touchX - mCentreTranX;
    }

    public final float toTransY(float touchY, float graffitiY) {
        return -graffitiY * (mPrivateScale * mScale) + touchY - mCentreTranY;
    }


    private void initCanvas() throws Exception {
        if (mGraffitiBitmap != null) {
            mGraffitiBitmap.recycle();
        }
        if (mBitmap == null) {
            throw new Exception("mBitmap is null");
        }
        mGraffitiBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
        mBitmapCanvas = new Canvas(mGraffitiBitmap);
    }

    private void resetMatrix() throws Exception {

        this.mShaderMatrix.set(null);
        this.mBitmapShader.setLocalMatrix(this.mShaderMatrix);


        // 如果使用了自定义的橡皮擦底图，则需要调整矩阵
        if (mPen == Pen.ERASER && mBitmapShader != mBitmapShaderEraser) {
            mMatrixTemp.reset();
            mBitmapShaderEraser.getLocalMatrix(mMatrixTemp);
            mBitmapShader.getLocalMatrix(mMatrixTemp);
            // 缩放橡皮擦底图，使之与涂鸦图片大小一样
            if (mEraserImageIsResizeable) {
                if (mBitmap == null) {
                    throw new Exception("mBitmap is null");
                }
                mMatrixTemp.preScale(mBitmap.getWidth() * 1f / mBitmapEraser.getWidth(), mBitmap.getHeight() * 1f / mBitmapEraser.getHeight());
            }
            mBitmapShaderEraser.setLocalMatrix(mMatrixTemp);
        }
    }

    /**
     * 调整图片位置
     * <p>
     * 明白下面一点很重要：
     * 假设不考虑任何缩放，图片就是肉眼看到的那么大，此时图片的大小width =  mPrivateWidth * mScale ,
     * 偏移量x = mCentreTranX + mTransX，而view的大小为width = getWidth()。height和偏移量y以此类推。
     */
    private void judgePosition() {
        boolean changed = false;
        if (mPrivateWidth * mScale < getWidth()) { // 限制在view范围内
            if (mTransX + mCentreTranX < 0) {
                mTransX = -mCentreTranX;
                changed = true;
            } else if (mTransX + mCentreTranX + mPrivateWidth * mScale > getWidth()) {
                mTransX = getWidth() - mCentreTranX - mPrivateWidth * mScale;
                changed = true;
            }
        } else { // 限制在view范围外
            if (mTransX + mCentreTranX > 0) {
                mTransX = -mCentreTranX;
                changed = true;
            } else if (mTransX + mCentreTranX + mPrivateWidth * mScale < getWidth()) {
                mTransX = getWidth() - mCentreTranX - mPrivateWidth * mScale;
                changed = true;
            }
        }
        if (mPrivateHeight * mScale < getHeight()) { // 限制在view范围内
            if (mTransY + mCentreTranY < 0) {
                mTransY = -mCentreTranY;
                changed = true;
            } else if (mTransY + mCentreTranY + mPrivateHeight * mScale > getHeight()) {
                mTransY = getHeight() - mCentreTranY - mPrivateHeight * mScale;
                changed = true;
            }
        } else { // 限制在view范围外
            if (mTransY + mCentreTranY > 0) {
                mTransY = -mCentreTranY;
                changed = true;
            } else if (mTransY + mCentreTranY + mPrivateHeight * mScale < getHeight()) {
                mTransY = getHeight() - mCentreTranY - mPrivateHeight * mScale;
                changed = true;
            }
        }
        if (changed) {
            try {
                resetMatrix();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return 修改过的涂鸦
     */
    public Bitmap getGraffitiView() {
        return mGraffitiBitmap;
    }

    /**
     * 仅清除涂鸦
     */
    public void clear() {
        mPathStack.clear();
        if (pathStackWatcher != null) {
            pathStackWatcher.pathStackChange(mPathStack.size());
        }
        mRepealStack.clear();
//        mPathStackBackup.clear();
        try {
            initCanvas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        invalidate();
    }

    /**
     * 清理全部包括背景
     */
    public void clearAll() {
        try {
            mBitmap.recycle();
            mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            mBitmap.eraseColor(Color.WHITE);//填充颜色
            this.mBitmapShader = new BitmapShader(this.mBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            this.mBitmapShaderEraser = mBitmapShader;
            mPathStack.clear();
            if (pathStackWatcher != null) {
                pathStackWatcher.pathStackChange(mPathStack.size());
            }
            mRepealStack.clear();
            initCanvas();
            invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 撤销
     */
    public void undo() {
        if (!mPathStack.isEmpty()) {
            GraffitiPath graffitiPath = mPathStack.remove(mPathStack.size() - 1);
            if (pathStackWatcher != null) {
                pathStackWatcher.pathStackChange(mPathStack.size());
            }
            mRepealStack.add(graffitiPath);
            try {
                initCanvas();
            } catch (Exception e) {
                e.printStackTrace();
            }
            draw(mBitmapCanvas, mPathStack);
            invalidate();
        }
    }

    private int mGraffitiRotateDegree = 0; // 相对于初始图片旋转的角度

    public void rotate() {
        setRotation(getRotation() + 90);

    }

    PathStackWatcher pathStackWatcher;

    public void setPathStackWatcher(PathStackWatcher pathStackWatcher) {
        this.pathStackWatcher = pathStackWatcher;
    }

    /**
     * 恢复
     */
    public void redo() {
        if (!mRepealStack.isEmpty()) {
            mPathStack.add(mRepealStack.get(mRepealStack.size() - 1));
            if (pathStackWatcher != null) {
                pathStackWatcher.pathStackChange(mPathStack.size());
            }
            mRepealStack.remove(mRepealStack.size() - 1);
            try {
                initCanvas();
            } catch (Exception e) {
                e.printStackTrace();
            }
            draw(mBitmapCanvas, mPathStack);
            invalidate();
        }
    }

    /**
     * 是否有修改
     */
    public boolean isModified() {
        return !mPathStack.isEmpty();
    }

    /**
     * 居中图片
     */
    public void centrePic() {
        mScale = 1;
        // 居中图片
        mTransX = 0;
        mTransY = 0;
        judgePosition();
        invalidate();
    }

    /**
     * 只绘制原图
     *
     * @param justDrawOriginal
     */
    public void setJustDrawOriginal(boolean justDrawOriginal) {
        isJustDrawOriginal = justDrawOriginal;
        invalidate();
    }

    public boolean isJustDrawOriginal() {
        return isJustDrawOriginal;
    }

    /**
     * 设置画笔底色
     *
     * @param color
     */
    public void setColor(int color) {
        mColor.setColor(color);
        invalidate();
    }

    public void setColor(Bitmap bitmap) {
        mColor.setColor(bitmap);
        invalidate();
    }

    public void setColor(Bitmap bitmap, Shader.TileMode tileX, Shader.TileMode tileY) {
        mColor.setColor(bitmap, tileX, tileY);
        invalidate();
    }

    public GraffitiColor getGraffitiColor() {
        return mColor;
    }

    /**
     * 缩放倍数，图片真实的缩放倍数为 mPrivateScale*mScale
     *
     * @param scale
     */
    public void setScale(float scale) {
        this.mScale = scale;
        judgePosition();
        try {
            resetMatrix();
        } catch (Exception e) {
            e.printStackTrace();
        }
        invalidate();
    }

    public float getScale() {
        return mScale;
    }

    /**
     * 设置画笔
     *
     * @param pen
     */
    public void setPen(Pen pen) {
        if (pen == null) {
            throw new RuntimeException("Pen can't be null");
        }
        mPen = pen;
        try {
            resetMatrix();
        } catch (Exception e) {
            e.printStackTrace();
        }
        invalidate();
    }

    public Pen getPen() {
        return mPen;
    }

    /**
     * 设置画笔形状
     *
     * @param shape
     */
    public void setShape(Shape shape) {
        if (shape == null) {
            throw new RuntimeException("Shape can't be null");
        }
        mShape = shape;
        if (mShape != Shape.HAND_WRITE) {
            mPen = Pen.HAND;
        }
        invalidate();
    }

    public Shape getShape() {
        return mShape;
    }

    public void setTrans(float transX, float transY) {
        mTransX = transX;
        mTransY = transY;
        judgePosition();
        try {
            resetMatrix();
        } catch (Exception e) {
            e.printStackTrace();
        }
        invalidate();
    }

    /**
     * 设置图片偏移
     *
     * @param transX
     */
    public void setTransX(float transX) {
        this.mTransX = transX;
        judgePosition();
        invalidate();
    }

    public float getTransX() {
        return mTransX;
    }

    public void setTransY(float transY) {
        this.mTransY = transY;
        judgePosition();
        invalidate();
    }

    public float getTransY() {
        return mTransY;
    }


    public void setPaintSize(float paintSize) {
        mPaintSize = UiUtils.dp2px(paintSize);
        invalidate();
    }

    public void setShapeSize(float shapeSize) {
        mShapeSize = UiUtils.dp2px(shapeSize);
        invalidate();
    }

    public float getShapeSize() {
        return mShapeSize;
    }

    public float getPaintSize() {
        return mPaintSize;
    }

    /**
     * 触摸时，图片区域外是否绘制涂鸦轨迹
     *
     * @param isDrawableOutside
     */
    public void setIsDrawableOutside(boolean isDrawableOutside) {
        mIsDrawableOutside = isDrawableOutside;
    }

    /**
     * 触摸时，图片区域外是否绘制涂鸦轨迹
     */
    public boolean getIsDrawableOutside() {
        return mIsDrawableOutside;
    }

    /**
     * 设置放大镜的倍数，当小于等于0时表示不使用放大器功能
     *
     * @param amplifierScale
     */
    public void setAmplifierScale(float amplifierScale) {
        mAmplifierScale = amplifierScale;
        invalidate();
    }

    public float getAmplifierScale() {
        return mAmplifierScale;
    }


    /**
     * 设置原图
     *
     * @param mBitmap 背景图片
     * @return
     */
    public void setImageBg(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
        try {
            setBG();
        } catch (Exception e) {
            e.printStackTrace();
        }
        draw(mBitmapCanvas, mPathStack);
    }

    public void setColorBg(int Color) throws Exception {
        mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        if (mBitmap == null) {
            throw new Exception("mBitmap create failed");
        }
        mBitmap.eraseColor(Color);//填充颜色
        setBG();
        draw(mBitmapCanvas, mPathStack);
    }

    public void setEraserSize(float mEraserSize) {
        this.mEraserSize = UiUtils.dp2px(mEraserSize);
    }

    public void setPaintAble(boolean paintAble) {
        isPaintAble = paintAble;
    }

    public void recycleBitmap() {
        myHandler.removeCallbacksAndMessages(null);
        if (mBitmap != null) {
            mBitmap.recycle();
            mBitmap = null;
        }
        if (mGraffitiBitmap != null) {
            mGraffitiBitmap.recycle();
            mGraffitiBitmap = null;
        }
        if (mBitmapEraser != null) {
            mBitmapEraser.recycle();
            mBitmapEraser = null;
        }
    }

}
