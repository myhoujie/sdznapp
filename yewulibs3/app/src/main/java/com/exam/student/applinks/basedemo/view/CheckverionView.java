package com.exam.student.applinks.basedemo.view;

import com.exam.student.applinks.basedemo.bean.VersionInfoBean;
import com.haier.cellarette.libmvp.mvp.IView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface CheckverionView extends IView {
    void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean);

    void OnUpdateVersionNodata(String bean);

    void OnUpdateVersionFail(String msg);
}
