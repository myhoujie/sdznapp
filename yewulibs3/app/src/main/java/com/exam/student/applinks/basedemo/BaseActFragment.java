package com.exam.student.applinks.basedemo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.webviewdemo.AndroidInterface;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.just.agentweb.base.BaseCurrencyAgentWebFragment;


/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * <p>
 * 创建人：zhangchao
 * 创建时间：2017/3/20
 */
public abstract class BaseActFragment extends BaseCurrencyAgentWebFragment implements NetconListener2 {
    protected EmptyViewNew1 emptyview1;//网络监听
    private long mCurrentMs = System.currentTimeMillis();
    protected NetState netState;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        LogUtils.e("BaseActFragment-----onCreateView");
        setup(rootView, savedInstanceState);
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, getActivity());
        return rootView;
    }

    protected abstract int getLayoutId();


    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        LogUtils.e("BaseActFragment-----setup");
        emptyview1 = rootView.findViewById(R.id.emptyview2_order);
    }

    @Override
    public void net_con_none() {
        ToastUtils.showLong("网络异常，请检查网络连接！");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
    }


    public String ids;

    public void call(Object value) {
        LogUtils.e("BaseActFragment-----call");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (netState != null) {
            netState.unregisterReceiver();
        }
        super.onDestroy();
    }

    /**
     * 刷新webView
     */
    public void AgentwebRefresh(String url) {
//        mAgentWeb.getUrlLoader().reload(); // 刷新
        loadWebSite(url); // 刷新
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}