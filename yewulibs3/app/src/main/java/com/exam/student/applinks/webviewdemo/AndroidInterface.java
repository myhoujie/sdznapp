package com.exam.student.applinks.webviewdemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.exam.student.applinks.dynamirecyclerview.ShouyeActivity;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.just.agentweb.AgentWeb;

/**
 * Created by cenxiaozhong on 2017/5/14.
 * source code  https://github.com/Justson/AgentWeb
 */

public class AndroidInterface {
    private Handler deliver = new Handler(Looper.getMainLooper());
    private AgentWeb agent;
    private Context context;

    public AndroidInterface(AgentWeb agent, Context context) {
        this.agent = agent;
        this.context = context;
    }

    @JavascriptInterface
    public void BackToAndroid(final String str) {
//        Intent intent = new Intent(context, ShouyeActivity.class);//AppUtils.getAppPackageName() + "hs.act.slbapp.SimplePlayer"
//        intent.putExtra("webview", str);
//        context.startActivity(intent);
        HiosHelper.resolveAd((Activity) context, context, "hios://com.exam.student.applinks.dynamirecyclerview.ShouyeActivity?webview={s}" + str);
    }

    @JavascriptInterface
    public void Bac(final String str) {
        Intent intent = new Intent(context, ShouyeActivity.class);//AppUtils.getAppPackageName() + "hs.act.slbapp.SimplePlayer"
        intent.putExtra("webview", str);
        context.startActivity(intent);
    }

    @JavascriptInterface
    public String callAndroid() {
        deliver.post(new Runnable() {
            @Override
            public void run() {
                Log.i("Info", "main Thread:" + Thread.currentThread());
//                Toast.makeText(context.getApplicationContext(), "" + msg, Toast.LENGTH_LONG).show();
            }
        });

        Log.i("Info", "Thread:" + Thread.currentThread());
        return "进入此方法";
    }

}
