package com.exam.student.applinks;

import java.util.List;

public class GradeJson {

    /**
     * sectionName : 小学
     * childList : [{"gradeName":"一年级","gradeId":"1"},{"gradeName":"二年级","gradeId":"2"},{"gradeName":"三年级","gradeId":"3"},{"gradeName":"四年级","gradeId":"4"},{"gradeName":"五年级","gradeId":"5"},{"gradeName":"六年级","gradeId":"6"}]
     * sectionId : 316
     */

    private String sectionName;
    private String sectionId;
    private List<ChildListBean> childList;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public List<ChildListBean> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildListBean> childList) {
        this.childList = childList;
    }

    public static class ChildListBean {
        /**
         * gradeName : 一年级
         * gradeId : 1
         */

        private String gradeName;
        private String gradeId;
        private String sectionId;
        private String sectionName;
        private boolean isChecked;

        public String getSectionId() {
            return sectionId;
        }

        public String getSectionName() {
            return sectionName;
        }

        public void setSectionName(String sectionName) {
            this.sectionName = sectionName;
        }

        public void setSectionId(String sectionId) {
            this.sectionId = sectionId;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getGradeName() {
            return gradeName;
        }

        public void setGradeName(String gradeName) {
            this.gradeName = gradeName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }
    }
}

