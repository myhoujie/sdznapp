package com.exam.student.applinks.videoplaydemo;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.R;
import com.exam.student.applinks.videoplaydemo.gsy.FloatPlayerView;
import com.exam.student.applinks.videoplaydemo.gsy.LandLayoutVideo;
import com.exam.student.applinks.videoplaydemo.gsy.PreViewGSYVideoPlayer;
import com.exam.student.applinks.videoplaydemo.utlis.OrientationUtils;
import com.exam.student.applinks.videoplaydemo.utlis.floatUtil.FloatWindow;
import com.exam.student.applinks.videoplaydemo.utlis.floatUtil.MoveType;
import com.exam.student.applinks.videoplaydemo.utlis.floatUtil.Screen;
import com.exam.student.applinks.videoplaydemo.utlis.floatUtil.Util;
import com.just.agentweb.App2;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.listener.GSYVideoProgressListener;
import com.shuyu.gsyvideoplayer.listener.LockClickListener;
import com.shuyu.gsyvideoplayer.utils.Debuger;

/**
 * 多窗体下的悬浮窗页面
 */
public class WindowActivity extends BaseActivity implements View.OnClickListener {
    Button startWindow;
    private LandLayoutVideo detailPlayer;
    private OrientationUtils orientationUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window);
        detailPlayer = (LandLayoutVideo) findViewById(R.id.detail_player);
        startWindow = findViewById(R.id.start_window);
        startWindow.setOnClickListener(this);
       /* jumpOther = findViewById(R.id.jump_other);
        startWindow.setOnClickListener(this);
        jumpOther.setOnClickListener(this);*/
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Util.hasPermission(this)) {
                requestAlertWindowPermission();
            }
        }
        show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        GSYVideoManager.instance().releaseMediaPlayer();
        /**
         * 这里在返回主页的时候销毁了，因为不想和DEMO中其他页面冲突
         */
//        FloatWindow.destroy();
    }

    @RequiresApi(api = 23)
    private void requestAlertWindowPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, 1);
    }


    @RequiresApi(api = 23)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= 23) {
            //todo 用23以上编译即可出现canDrawOverlays
            if (Util.hasPermission(this)) {

            } else {
                this.finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_window:
                if (FloatWindow.get() != null) {
                    return;
                }
                FloatPlayerView floatPlayerView = new FloatPlayerView(App2.get());//getApplicationContext()

                FloatWindow
                        .with(App2.get())
                        .setView(floatPlayerView)
                        .setWidth(Screen.width, 0.4f)
                        .setHeight(Screen.width, 0.4f)
                        .setX(Screen.width, 0.8f)
                        .setY(Screen.height, 0.3f)
                        .setMoveType(MoveType.slide)
                        .setFilter(false)
                        .setMoveStyle(500, new BounceInterpolator())
                        .build();
                FloatWindow.get().show();

                break;
        }
    }

    private void show() {
        //增加封面
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.mipmap.xxx1);

        resolveNormalVideoUI();

        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, detailPlayer);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);

        String url = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4";
        GSYVideoOptionBuilder gsyVideoOption = new GSYVideoOptionBuilder();
        gsyVideoOption.setThumbImageView(imageView)
                .setIsTouchWiget(true)
                .setRotateViewAuto(false)
                .setLockLand(false)
                .setShowFullAnimation(false)
                .setNeedLockFull(true)
                .setSeekRatio(1)
                .setUrl(url)
                .setCacheWithPlay(false)
                .setVideoTitle("测试视频")
                .setVideoAllCallBack(new GSYSampleCallBack() {

                    @Override
                    public void onPrepared(String url, Object... objects) {
                        Debuger.printfError("***** onPrepared **** " + objects[0]);
                        Debuger.printfError("***** onPrepared **** " + objects[1]);
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
                        orientationUtils.setEnable(true);
                    }

                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        Debuger.printfError("***** onEnterFullscreen **** " + objects[0]);//title
                        Debuger.printfError("***** onEnterFullscreen **** " + objects[1]);//当前全屏player
                    }

                    @Override
                    public void onAutoComplete(String url, Object... objects) {
                        super.onAutoComplete(url, objects);
                    }

                    @Override
                    public void onClickStartError(String url, Object... objects) {
                        super.onClickStartError(url, objects);
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        Debuger.printfError("***** onQuitFullscreen **** " + objects[0]);//title
                        Debuger.printfError("***** onQuitFullscreen **** " + objects[1]);//当前非全屏player
                        if (orientationUtils != null) {
                            orientationUtils.backToProtVideo();
                        }
                    }
                })
                .setLockClickListener(new LockClickListener() {
                    @Override
                    public void onClick(View view, boolean lock) {
                        if (orientationUtils != null) {
                            //配合下方的onConfigurationChanged
                            orientationUtils.setEnable(!lock);
                        }
                    }
                })
                .setGSYVideoProgressListener(new GSYVideoProgressListener() {
                    @Override
                    public void onProgress(int progress, int secProgress, int currentPosition, int duration) {
                        Debuger.printfLog(" progress " + progress + " secProgress " + secProgress + " currentPosition " + currentPosition + " duration " + duration);
                    }
                })
                .build(detailPlayer);
    }

    private void resolveNormalVideoUI() {
        //增加title
        detailPlayer.getTitleTextView().setVisibility(View.GONE);
        detailPlayer.getBackButton().setVisibility(View.GONE);
    }
}
