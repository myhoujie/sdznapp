package com.exam.student.applinks.whiteboard.views;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.exam.student.applinks.R;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：
 * 修改时间：on 2019/6/13
 * 修改单号：
 * 修改内容:
 */
public class PopViewhuabi {

    static PopViewhuabi.onPenClick onPenClick = null;

    public static void setOnPenClick(PopViewhuabi.onPenClick onPen) {
        onPenClick = onPen;
    }

    public static PopupWindow showPenPopupWindow(View view, final Activity activity) {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(activity).inflate(
                R.layout.popup_pen, null);

        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0));
        contentView.findViewById(R.id.rbColorWhites).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(6);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorReds).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(8);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorBlacks).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(10);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorYellows).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(12);
                popupWindow.dismiss();
            }
        });
        contentView.findViewById(R.id.rbColorGreens).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPenClick.onClickPen(14);
                popupWindow.dismiss();
            }
        });
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);
            }
        });

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;

            }
        });
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
        // popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        return popupWindow;
    }



    public interface onPenClick {
        void onClickPen(int size);
    }
}
