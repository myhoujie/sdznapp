package com.exam.student.applinks.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 描述：时间工具类
 * -
 * 创建人：wangchunxiao
 * 创建时间：16/8/15
 */
public class DateUtil {

    public static final String defaultPatten = "yyyy-MM-dd HH:mm:ss";
    /**
     *时间延后 later
     */
    public static String getDateStr(String str,int later){
        String resultStr;
        resultStr=getTimeStrByTimemillis( (getTimeMillisByStr(str, "yyyy-MM-dd HH:mm")+(later*60*1000)),"yyyy-MM-dd HH:mm");
        return  resultStr;
    }

    /**
     * 根据时间戳得到时间
     *
     * @param timemillis
     * @param patten
     * @return
     */
    public static String getTimeStrByTimemillis(long timemillis, String patten) {
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        String dateNowStr = sdf.format(timemillis);
        return dateNowStr;
    }

    /**
     * 得到当前的日期
     *
     * @param patten
     * @return
     */
    public static String getCurrentDate(String patten) {
        return getTimeStrByTimemillis(System.currentTimeMillis(), patten);
    }

    /**
     * 得到默认格式的当前日期
     *
     * @return
     */
    public static String getCurrentDateDefault() {
        return getCurrentDate(defaultPatten);
    }

    /**
     * 根据日期格式的字符串得到时间戳
     *
     * @param time
     * @param patten
     * @return
     */
    public static long getTimeMillisByStr(String time, String patten) {
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        try {
            Date date = sdf.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 得到两个日期之间的时长
     *
     * @param time1
     * @param time2
     * @param patten
     * @return
     */
    public static long getTimeMillisBetween2Time(String time1, String time2, String patten) {
        long millis1 = getTimeMillisByStr(time1, patten);
        long millis2 = getTimeMillisByStr(time2, patten);
        return millis2 - millis1;
    }

    /**
     * 计算两个默认格式时间之间的时长
     *
     * @param time1
     * @param time2
     * @return
     */
    public static long getTimeMillisBetween2TimeDefault(String time1, String time2) {
        return getTimeMillisBetween2Time(time1, time2, defaultPatten);
    }

    /**
     * 将毫秒格式的时长 格式化成hh:mm:ss的格式
     *
     * @param duration
     * @return
     */
    public static String formatTimeDuration(long duration) {
        duration = duration / 1000;
        long a = 60;
        long second = duration % a;
        long minute = duration / a % a;
        long hour = duration / a / a;
        return hour + "：" + minute + "：" + second;
    }

    /**
     * 转换日期格式
     *
     * @param srcTime
     * @param srcPattern
     * @param descPattern
     * @return
     */
    public static String format2Target(String srcTime, String srcPattern, String descPattern) {
        long timemillis = getTimeMillisByStr(srcTime, srcPattern);
        return getTimeStrByTimemillis(timemillis, descPattern);
    }

    /**
     * 将秒数转换为时间长度
     *
     * @param seconds
     * @return
     */
    public static String millsecondsToStr(long seconds) {
        seconds = seconds / 1000;
        String result = "";
        long hour = 0, min = 0, second = 0;
        hour = seconds / 3600;
        min = (seconds - hour * 3600) / 60;
        second = seconds - hour * 3600 - min * 60;
        if (hour < 10 && hour > 0) {
            result += "0" + hour + ":";
        } else if (hour > 0) {
            result += hour + ":";
        }
        if (min < 10) {
            result += "0" + min + ":";
        } else {
            result += min + ":";
        }
        if (second < 10) {
            result += "0" + second;
        } else {
            result += second;
        }
        return result;
    }
    /**
     * 将秒数转换为时间长度 --》汉语
     *
     * @param seconds
     * @return
     */
    public static String millsecondsToString(long seconds) {
        seconds = seconds / 1000;
        String result = "";
        long hour = 0, min = 0, second = 0;
        hour = seconds / 3600;
        min = (seconds - hour * 3600) / 60;
        second = seconds - hour * 3600 - min * 60;
        if (hour < 10 && hour > 0) {
            result += "0" + hour + "时";
        } else if (hour > 0) {
            result += hour + "时";
        }
        if (min < 10) {
            result += "0" + min + "分";
        } else {
            result += min + "分";
        }
        if (second < 10) {
            result += "0" + second;
        } else {
            result += second;
        }
        return result;
    }

    public static String formatToWeek(long timemillis) {
        SimpleDateFormat formatter = new SimpleDateFormat(defaultPatten);
        try {
            String date = formatter.format(timemillis);
            Date date1 = formatter.parse(date);
            SimpleDateFormat formatter2 = new SimpleDateFormat("EEEE");
            String week = formatter2.format(date1);
            return week;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatToMonth(String date, String patten) {
        SimpleDateFormat formatter = new SimpleDateFormat(patten);
        try {
            Date date1 = formatter.parse(date);
            SimpleDateFormat newDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            String newDate = newDateFormatter.format(date1);
            String currentDate = getCurrentDate("yyyy-MM-dd");
            if (currentDate.equals(newDate)) {
                return "今天";
            }

            SimpleDateFormat formatter2 = new SimpleDateFormat("MM-dd");
            String month = formatter2.format(date1);
            return month;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Date parseDate(String dateStr, String patten) {
        SimpleDateFormat formatter = new SimpleDateFormat(patten);
        try {
            Date date = formatter.parse(dateStr);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int differentDays(long timeMillis1, long timeMillis2) {
        SimpleDateFormat formatter = new SimpleDateFormat(defaultPatten);
        try {
            String dateStr1 = formatter.format(timeMillis1);
            String dateStr2 = formatter.format(timeMillis2);
            Date date1 = formatter.parse(dateStr1);
            Date date2 = formatter.parse(dateStr2);
            return differentDays(date1, date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * date2比date1多的天数
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int differentDays(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 != year2) {//不同年
            int timeDistance = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0) {//闰年
                    timeDistance += 366;
                } else {//不是闰年
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2 - day1);
        } else {//同一年
            return day2 - day1;
        }
    }

    /**
     * 获取月份起始日期
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static String getMinMonthDate(long date, String patten) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(date));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat formatter = new SimpleDateFormat(patten);
        return formatter.format(calendar.getTime());
    }

    /**
     * 获取月份最后日期
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static String getMaxMonthDate(long date, String patten) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(date));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat formatter = new SimpleDateFormat(patten);
        return formatter.format(calendar.getTime());
    }
}
