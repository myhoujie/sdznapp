package com.exam.student.applinks.newwork.Presenter;

import com.alibaba.fastjson.JSONObject;
import com.exam.student.applinks.newwork.api.Api;
import com.exam.student.applinks.newwork.bean.HCategoryBean;
import com.exam.student.applinks.newwork.view.HCategoryView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HCategoryPresenter extends Presenter<HCategoryView> {

    public void get_category(String enumType, String needAll) {
        JSONObject requestData = new JSONObject();
        requestData.put("enumType", enumType);//
        requestData.put("needAll", needAll);// 是否需要全部:0-否,1-是
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier())
                .get_category(requestBody)
                .enqueue(new Callback<ResponseSlbBean<HCategoryBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean<HCategoryBean>> call, Response<ResponseSlbBean<HCategoryBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnCategoryNodata(response.body().getMsg());
                            return;
                        }
                        getView().OnCategorySuccess(response.body().getData());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean<HCategoryBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
//                String string = t.toString();
                        String string = BanbenUtils.getInstance().net_tips;
                        getView().OnCategoryFail(string);
                        call.cancel();
                    }
                });

    }

    public void get_category2(String grade) {
        JSONObject requestData = new JSONObject();
        requestData.put("grade", grade);//
//        requestData.put("thirdPid", thirdPid);//
//        requestData.put("source", source);//

//        requestData.put("token", CommonUtil.getEncryptToken(id, url_me, date, ((JSONObject) JSON.toJSON(params)).toJSONString()));
//        requestData.put("params", JSON.toJSON(params));

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());

        RetrofitNetNew.build(Api.class, getIdentifier()).get_category2(requestBody).enqueue(new Callback<ResponseSlbBean<HCategoryBean>>() {
            @Override
            public void onResponse(Call<ResponseSlbBean<HCategoryBean>> call, Response<ResponseSlbBean<HCategoryBean>> response) {
                if (!hasView()) {
                    return;
                }
                if (response.body() == null) {
                    return;
                }
                if (response.body().getCode() != 0) {
                    getView().OnCategoryNodata(response.body().getMsg());
                    return;
                }
                getView().OnCategorySuccess(response.body().getData());
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseSlbBean<HCategoryBean>> call, Throwable t) {
                if (!hasView()) {
                    return;
                }
//                String string = t.toString();
                String string = BanbenUtils.getInstance().net_tips;
                getView().OnCategoryFail(string);
                call.cancel();
            }
        });

    }

}
