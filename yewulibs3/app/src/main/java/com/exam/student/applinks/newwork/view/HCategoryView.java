package com.exam.student.applinks.newwork.view;

import com.exam.student.applinks.newwork.bean.HCategoryBean;
import com.haier.cellarette.libmvp.mvp.IView;

public interface HCategoryView extends IView {

    void OnCategorySuccess(HCategoryBean bean);

    void OnCategoryNodata(String bean);

    void OnCategoryFail(String msg);
}
