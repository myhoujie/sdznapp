package com.exam.student.applinks.videoplaydemo.gsy;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.exam.student.applinks.R;
import com.exam.student.applinks.videoplaydemo.utlis.floatUtil.FloatWindow;

/**
 * 适配了悬浮窗的view
 * Created by guoshuyu on 2017/12/25.
 */

public class FloatPlayerView extends FrameLayout {

    FloatingVideo videoPlayer;

    public FloatPlayerView(Context context) {
        super(context);
        init();
    }

    public FloatPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FloatPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        videoPlayer = new FloatingVideo(getContext());
//        videoPlayer = new PreViewGSYVideoPlayer(getContext(), new PreViewGSYVideoPlayer.onplayerListenter() {
//            @Override
//            public void playerListener() {
//                if (FloatWindow.get() != null) {
//                    return;
//                }
//                FloatWindow.get().hide();
//            }
//        });
//        videoPlayer.hideSmallVideo();

        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;

        addView(videoPlayer, layoutParams);

        String source1 = "http://9890.vod.myqcloud.com/9890_4e292f9a3dd011e6b4078980237cc3d3.f20.mp4";


        videoPlayer.setUp(source1, true, "测试视频");


        //增加封面
        ImageView imageView = new ImageView(getContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.mipmap.xxx1);
        videoPlayer.setThumbImageView(imageView);

        //是否可以滑动调整
        videoPlayer.setIsTouchWiget(true);
        //是否点击封面可以播放
        videoPlayer.setThumbPlay(true);
    }


    public void onPause() {
        videoPlayer.getCurrentPlayer().onVideoPause();
    }

    public void onResume() {
        videoPlayer.getCurrentPlayer().onVideoResume();
    }

}
