package com.exam.student.applinks.newwork.api;

import com.exam.student.applinks.newwork.bean.HCategoryBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {
    String SERVER_ISERVICE = "https://m.hexiangjiaoyu.com/bcapi/";

    // 所有分类的列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/liveEnumLists")
    Call<ResponseSlbBean<HCategoryBean>> get_category(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_category2(@Body RequestBody body);
}
