package com.exam.student.applinks.basedemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.webviewdemo.AndroidInterface;

import org.json.JSONObject;

public class BaseActDemoYewuJS extends BaseActDemo implements BaseOnClickListener {
    private TextView callJsNoParamsButton;
    private TextView callJsOneParamsButton;
    private TextView callJsMoreParamsButton;
    private TextView jsJavaCommunicationButton;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_js;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
    }

    protected void findiview() {
        callJsNoParamsButton = findViewById(R.id.callJsNoParamsButton);
        callJsOneParamsButton = findViewById(R.id.callJsOneParamsButton);
        callJsMoreParamsButton = findViewById(R.id.callJsMoreParamsButton);
        jsJavaCommunicationButton = findViewById(R.id.jsJavaCommunicationButton);
    }

    protected void onclickview() {
        callJsNoParamsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroid");
            }
        });
        callJsOneParamsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidParam", "Hello ! Agentweb");
            }
        });
        callJsMoreParamsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidMoreParams", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        Log.i("Info", "value:" + value);
                    }
                }, getJson());
            }
        });
        jsJavaCommunicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidInteraction", "你好Js");
            }
        });
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        TitleShowHideState(2);
        setBaseOnClickListener(this);
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, activity));
        }
    }

    private String getJson() {
        String result = "";
        try {
            JSONObject mJSONObject = new JSONObject();
            mJSONObject.put("id", 1);
            mJSONObject.put("name", "Agentweb");
            mJSONObject.put("age", 18);
            result = mJSONObject.toString();
        } catch (Exception e) {

        }
        return result;
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.ll_base_container);
    }

    //个人中心
    @Override
    public void Titlegrzx() {
        ToastUtils.showLong("点击了个人中心");
    }

    @Override
    public void Titleshijian() {

    }

    @Override
    public void Titlezankaishijian() {

    }

    /*搜索*/
    public void Titlesousuo() {
        ToastUtils.showLong("点击了搜索");
    }

    @Override
    public void Titletijiao() {

    }

    @Override
    public void TitleDropdown() {

    }
}
