package com.exam.student.applinks.dynamirecyclerview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.dynamirecyclerview.adapter.ShouyeFooterAdapter;
import com.exam.student.applinks.dynamirecyclerview.fragment.F1;
import com.exam.student.applinks.dynamirecyclerview.fragment.F2;
import com.exam.student.applinks.dynamirecyclerview.fragment.F3;
import com.exam.student.applinks.dynamirecyclerview.fragment.F4;
import com.exam.student.applinks.dynamirecyclerview.fragment.F5;
import com.exam.student.applinks.newwork.bean.ShouyeFooterBean;
import com.example.baselibrary.LocalBroadcastManagers;
import com.example.baselibrary.base.BaseAppManager;

import java.util.ArrayList;
import java.util.List;

public class ShouyeActivity extends BaseActivity {
    private RecyclerView recyclerView;
    private ShouyeFooterAdapter mAdapter;
    private int current_pos = 0;
    private String tag_ids;

    public static final String id1 = "11";
    public static final String id2 = "22";
    public static final String id3 = "33";
    public static final String id4 = "44";
    public static final String id5 = "55";
    private List<ShouyeFooterBean> mList;
    private static final String LIST_TAG1 = "list11";
    private static final String LIST_TAG2 = "list22";
    private static final String LIST_TAG3 = "list33";
    private static final String LIST_TAG4 = "list44";
    private static final String LIST_TAG5 = "list55";

    private FragmentManager mFragmentManager;
    private F1 mFragment1; //
    private F2 mFragment2; //
    private F3 mFragment3; //
    private F4 mFragment4; //
    private F5 mFragment5; //

    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("ShouyeActivity".equals(intent.getAction())) {
                    //点击item
                    int id1 = intent.getIntExtra("id", 0);
                    current_pos = id1;
                    footer_onclick();
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String webview = getIntent().getStringExtra("webview");
        ToastUtils.showLong(webview);
        if (!this.isTaskRoot()) {
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                    finish();
                    return;
                }
            }
        }
        setContentView(R.layout.activity_shouye);
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("ShouyeActivity");
        LocalBroadcastManagers.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver, filter);

        if (BaseAppManager.getInstance().top() != null) {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.AdCommImgActivity");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id1", id1);
            startActivity(intent);
        }

        findview();
        onclick();
        mFragmentManager = getSupportFragmentManager();
        // 解决fragment布局重叠错乱
        if (savedInstanceState != null) {
            mFragment1 = (F1) mFragmentManager.findFragmentByTag(LIST_TAG1);
            mFragment2 = (F2) mFragmentManager.findFragmentByTag(LIST_TAG2);
            mFragment3 = (F3) mFragmentManager.findFragmentByTag(LIST_TAG3);
            mFragment4 = (F4) mFragmentManager.findFragmentByTag(LIST_TAG4);
            mFragment5 = (F5) mFragmentManager.findFragmentByTag(LIST_TAG5);
        }
        doNetWork();
    }


    private void doNetWork() {
        mList = new ArrayList<>();
        addList();
        recyclerView.setLayoutManager(new GridLayoutManager(this, mList.size(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();
        current_pos = 0;
        footer_onclick();
    }


    private void findview() {
        recyclerView = findViewById(R.id.recycler_view1);
        mAdapter = new ShouyeFooterAdapter(this);

    }

    private void onclick() {
        mAdapter.setOnItemClickLitener(new ShouyeFooterAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item
                current_pos = position;
                footer_onclick();
            }
        });
    }

    //点击item
    private void footer_onclick() {
        final ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(current_pos);
        if (model.isEnselect()) {
            // 不切换当前的item点击 刷新当前页面
            showFragment(model.getText_id(), true);
        } else {
            // 切换到另一个item
            if (model.getText_id().equalsIgnoreCase(id2)) {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            } else {
                set_footer_change(model);
                showFragment(model.getText_id(), false);
            }
        }
    }

    private void set_footer_change(ShouyeFooterBean model) {
        //设置为选中
        initList();
        model.setEnselect(true);
        mAdapter.setContacts(mList);
        mAdapter.notifyDataSetChanged();

    }

    private void addList() {
        mList.add(new ShouyeFooterBean(id1, "首页", R.drawable.navi_home_normal, R.drawable.navi_home_selected, true));
        mList.add(new ShouyeFooterBean(id2, "我的课程", R.drawable.main_spell_normal, R.drawable.main_spell_down, false));
        mList.add(new ShouyeFooterBean(id3, "课程任务", R.drawable.main_school_normal, R.drawable.main_school_down, false));
        mList.add(new ShouyeFooterBean(id4, "小组讨论", R.drawable.main_course_normal, R.drawable.main_course_down, false));
        mList.add(new ShouyeFooterBean(id5, "我的", R.drawable.main_person_normal, R.drawable.main_person_down, false));
    }

    private void initList() {
        for (int i = 0; i < mList.size(); i++) {
            ShouyeFooterBean item = mList.get(i);
            if (item.isEnselect()) {
                item.setEnselect(false);
            }
        }
    }

    private void showFragment(final String tag, final boolean isrefresh) {
        tag_ids = tag;
        //pifubufen
//        pifu(id2);
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        hideFragments(transaction);

        if (tag.equalsIgnoreCase("-1")) { //
//            if (mFragment1 == null) {
//                mFragment1 = new FragmentContent1();
//                transaction.add(R.id.container, mFragment1, LIST_TAG0);
//            } else {
//                transaction.show(mFragment1);
//                mFragment1.initData();
//            }
        } else if (tag.equalsIgnoreCase(id1)) {
            if (mFragment1 == null) {
                mFragment1 = new F1();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment1.setArguments(args);
                transaction.add(R.id.container, mFragment1, LIST_TAG1);
            } else {
                transaction.show(mFragment1);
                mFragment1.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id2)) {
            if (mFragment2 == null) {
                mFragment2 = new F2();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment2.setArguments(args);
                transaction.add(R.id.container, mFragment2, LIST_TAG2);
            } else {
                transaction.show(mFragment2);
                mFragment2.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id3)) {
            if (mFragment3 == null) {
                mFragment3 = new F3();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment3.setArguments(args);
                transaction.add(R.id.container, mFragment3, LIST_TAG3);
            } else {
                transaction.show(mFragment3);
                mFragment3.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id4)) {
            if (mFragment4 == null) {
                mFragment4 = new F4();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment4.setArguments(args);
                transaction.add(R.id.container, mFragment4, LIST_TAG4);
            } else {
                transaction.show(mFragment4);
                mFragment4.getCate(tag, isrefresh);
            }
        } else if (tag.equalsIgnoreCase(id5)) {
            if (mFragment5 == null) {
                mFragment5 = new F5();
                Bundle args = new Bundle();
                args.putString("tablayoutId", tag);
                mFragment5.setArguments(args);
                transaction.add(R.id.container, mFragment5, LIST_TAG5);
            } else {
                transaction.show(mFragment5);
                mFragment5.getCate(tag, isrefresh);
            }
        }

        transaction.commitAllowingStateLoss();
    }

    private void hideFragments(FragmentTransaction transaction) {
        if (mFragment1 != null) {
            transaction.hide(mFragment1);
            mFragment1.give_id(tag_ids);
        }
        if (mFragment2 != null) {
            transaction.hide(mFragment2);
            mFragment2.give_id(tag_ids);
        }
        if (mFragment3 != null) {
            transaction.hide(mFragment3);
            mFragment3.give_id(tag_ids);
        }
        if (mFragment4 != null) {
            transaction.hide(mFragment4);
            mFragment4.give_id(tag_ids);
        }
        if (mFragment5 != null) {
            transaction.hide(mFragment5);
            mFragment5.give_id(tag_ids);
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManagers.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            exit();
//
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

//    private long exitTime;

//    private void exit() {
//        ShouyeFooterBean model = (ShouyeFooterBean) mAdapter.getItem(0);
//        if (model != null && !tag_ids.equals(model.getText_id())) {
//            set_footer_change(model);
//            showFragment(model.getText_id(), false);
//        } else {
//            if ((System.currentTimeMillis() - exitTime) < 1500) {
////                RichText.recycle();
////                BaseAppManager.getInstance().closeApp();
//            } else {
//                ToastUtils.showLong("再次点击退出程序哟 ~");
//                exitTime = System.currentTimeMillis();
//            }
//        }
//    }
}
