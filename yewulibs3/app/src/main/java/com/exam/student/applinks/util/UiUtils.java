package com.exam.student.applinks.util;

import android.content.Context;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.just.agentweb.App2;

/**
 * UiUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class UiUtils {
    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param pxValue
     * @param fontScale （DisplayMetrics类中属�?scaledDensity�?
     * @return
     */
    public static int px2sp(float pxValue, float fontScale) {
        return (int) (pxValue / fontScale + 0.5f);
    }

    public static int sp2px(float spValue) {
        final float fontScale = App2.get().getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     *
     * @param spValue
     * @param fontScale （DisplayMetrics类中属�?scaledDensity�?
     * @return
     */
    public static int sp2px(float spValue, float fontScale) {
        return (int) (spValue * fontScale + 0.5f);
    }

    public static int sp2px(Context context, float spValue) {
//        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        final float fontScale = 1.7f;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static int dp2px(float dpValue) {
        final float scale = App2.get().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int dp2px(Context context, float dpValue) {
//        final float scale = context.getResources().getDisplayMetrics().density;
        final float scale = 1.7f;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dip(float pxValue) {
        final float scale = App2.get().getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }


    public static int getScreenH() {
        WindowManager manager = (WindowManager) App2.get().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dmh = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(dmh);
        int height = dmh.heightPixels;
        return height;
    }

    public static int getScreenW() {
        WindowManager manager = (WindowManager) App2.get().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dmw = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(dmw);
        int width = dmw.widthPixels;
        return width;
    }

    public static float getDensity() {
        return App2.get().getResources().getDisplayMetrics().density;
    }

    /**
     * 测量文字高度
     *
     * @param paint
     * @return
     */
    public static float measureTextHeight(Paint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return (Math.abs(fontMetrics.ascent) - fontMetrics.descent);
    }

    /**
     * 测量文字宽度
     *
     * @param paint
     * @param str
     * @return
     */
    public static float measureTextWidth(Paint paint, String str) {
        return paint.measureText(str);
    }

}
