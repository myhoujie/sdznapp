package com.exam.student.applinks;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.projection.MediaProjection;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.camera.camera2.Camera2Config;
import androidx.camera.core.CameraXConfig;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.base.ActivityManager;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.countdowndemo.datepick.CountDownPickWheelDialog;
import com.exam.student.applinks.countdowndemo.datepick.OnCountDownListener;
import com.exam.student.applinks.graffitidemo.OnScreenCaptureEvent;
import com.exam.student.applinks.pictureselectordemo.picture.PictureSelectorEngineImp;
import com.exam.student.applinks.util.UiUtils;
import com.exam.student.applinks.whiteboard.PaletteActivity;
import com.exam.student.applinks.zxingdemo.SaomaActDemo;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.just.agentweb.App2;
import com.luck.picture.lib.app.IApp;
import com.luck.picture.lib.app.PictureAppMaster;
import com.luck.picture.lib.crash.PictureSelectorCrashUtils;
import com.luck.picture.lib.engine.PictureSelectorEngine;
import com.yhao.floatwindow.FloatWindow;
import com.yhao.floatwindow.IFloatWindowImpl;
import com.yhao.floatwindow.MoveType;
import com.yhao.floatwindow.PermissionListener;
import com.yhao.floatwindow.Screen;
import com.yhao.floatwindow.ViewStateListener;
import com.yhao.floatwindow.radioview.RadioLayout;

import org.greenrobot.eventbus.EventBus;

import update.UpdateAppUtils;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


/**
 * @author：luck
 * @date：2019-12-03 22:53
 * @describe：Application
 */

public class App extends Application implements IApp, CameraXConfig.Provider, View.OnClickListener {

    public static boolean showCountDown = false;
    public static boolean showSmallCountDown = true;
    public static Application mContext;
    public static MediaProjection mMediaProjection;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = (Application) getAppContext();
        UpdateAppUtils.init(this);
        /** PictureSelector日志管理配制开始 **/
        // PictureSelector 绑定监听用户获取全局上下文或其他...
        PictureAppMaster.getInstance().setApp(this);
        // PictureSelector Crash日志监听
        PictureSelectorCrashUtils.init((t, e) -> {
            // Crash之后的一些操作可再此处理，没有就忽略...
        });
        /** PictureSelector日志管理配制结束 **/
        //网络初始化
        configRetrofitNet();
        configHios();

        /*===================== 悬浮窗 =====================*/
        initFloatWindow();
    }

    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }

    @Override
    public Context getAppContext() {
        return this;
    }

    @Override
    public PictureSelectorEngine getPictureSelectorEngine() {
        return new PictureSelectorEngineImp();
    }

    private void configHios() {
        HiosHelper.config(App2.get().getPackageName() + ".slbapp.ad.web.page", App2.get().getPackageName() + ".slbapp.web.page");

    }

    @NonNull
    @Override
    public CameraXConfig getCameraXConfig() {
        return Camera2Config.defaultConfig();
    }

    /*===================== 悬浮窗 =====================*/
    private static final String floatWindowTag = "floatWindowTag";
    private boolean windowShow = false;//悬浮窗是否展开
    private int offset, width;
    private FrameLayout container;
    private LinearLayout llBg;
    private ImageView ivTool;
    private RadioLayout rg;

    private void initFloatWindow() {
        container = (FrameLayout) LayoutInflater.from(App2.get()).inflate(R.layout.layout_floatwindow, null);
        ivTool = container.findViewById(R.id.iv_tool);
        llBg = container.findViewById(R.id.ll_bg);
        rg = container.findViewById(R.id.rg);

        FloatWindow
                .with(App2.get())
                .setTag(floatWindowTag)
                .setView(container)
                .setX(Screen.width, 0.8f)
                .setY(Screen.height, 0.5f)
                .setMoveType(MoveType.slide)
                .setFilter(false, SaomaActDemo.class)//过滤指定页面显示悬浮窗/隐藏悬浮窗
                .setViewStateListener(mViewStateListener)
                .setPermissionListener(mPermissionListener)
                .setDesktopShow(false)
                .setMoveStyle(500, new AccelerateInterpolator())  //贴边动画时长为500ms，加速插值器
                .build();

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFloatWindowState();
            }
        });
        rg.findViewById(R.id.rb_baiban).setOnClickListener(this);
        rg.findViewById(R.id.rb_tuya).setOnClickListener(this);
        rg.findViewById(R.id.rb_daojishi).setOnClickListener(this);

    }

    private void changeFloatWindowState() {
        windowShow = !windowShow;
        if (windowShow) {//展开
            IFloatWindowImpl window = (IFloatWindowImpl) FloatWindow.get(floatWindowTag);
            window.setMoveType(MoveType.inactive);
            llBg.setBackground(App2.get().getResources().getDrawable(R.drawable.bj));
            rg.setVisibility(View.VISIBLE);
            ivTool.setSelected(true);
            width = window.getY();
            offset = window.getX();
            //960
            window.updateX(100);
        } else {//关闭
            IFloatWindowImpl window = (IFloatWindowImpl) FloatWindow.get(floatWindowTag);
            window.setMoveType(MoveType.slide);
            llBg.setBackground(null);
            rg.setVisibility(View.GONE);
            ivTool.setSelected(false);
            //1776
            window.updateX(offset);
        }
    }

    private PermissionListener mPermissionListener = new PermissionListener() {
        @Override
        public void onSuccess() {

        }

        @Override
        public void onFail() {
        }
    };


    private ViewStateListener mViewStateListener = new ViewStateListener() {
        @Override
        public void onPositionUpdate(int x, int y) {
        }

        @Override
        public void onShow() {
        }

        @Override
        public void onHide() {
        }

        @Override
        public void onDismiss() {
        }

        @Override
        public void onMoveAnimStart() {
        }

        @Override
        public void onMoveAnimEnd() {
        }

        @Override
        public void onBackToDesktop() {
        }
    };
    /*===================== 悬浮窗 =====================*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_baiban://白板
                changeFloatWindowState();
                Intent intent = new Intent(App2.get(), PaletteActivity.class);
                intent.putExtra("type", "baiban");
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                App2.get().startActivity(intent);
                break;
            case R.id.rb_tuya://涂鸦
                changeFloatWindowState();
                EventBus.getDefault().post(new OnScreenCaptureEvent());
                break;
            case R.id.rb_daojishi://倒计时
                changeFloatWindowState();
                showCountDownPop();
                break;
            default:
                rg.cleanRadioChecked();
                break;
        }
    }

    private void showCountDownPop() {
        BaseActivity activity = (BaseActivity) ActivityManager.getForegroundActivity();
        if (App.showCountDown) {
            ToastUtils.showShort(activity.getString(R.string.count_down_is_running));
            return;
        }
        CountDownPickWheelDialog countDownPickWheelDialog = new CountDownPickWheelDialog.Builder(activity)
                .setPositiveButton(new OnCountDownListener() {
                    @Override
                    public void onClick(CountDownPickWheelDialog dialog) {
                        if (dialog.getTime() != 0) {
                            BaseActivity foregroundActivity = (BaseActivity) ActivityManager.getForegroundActivity();
                            if (foregroundActivity != null) {
                                foregroundActivity.showCountDownPop(dialog.getTime());
                                App.showCountDown = true;
                            }
                            dialog.dismiss();
                        } else {
                            ToastUtils.showShort(activity.getString(R.string.time_is_zero));
                        }
                    }
                })
                .setNegativeButton(new OnCountDownListener() {
                    @Override
                    public void onClick(CountDownPickWheelDialog dialog) {
                        dialog.dismiss();
                    }
                }).create();
        countDownPickWheelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        countDownPickWheelDialog.setCanceledOnTouchOutside(false);

        countDownPickWheelDialog.show();

        WindowManager m = activity.getWindowManager();
        Display d = m.getDefaultDisplay();

        Window window = countDownPickWheelDialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = d.getWidth();
        lp.height = UiUtils.dp2px(256);
        window.setGravity(Gravity.BOTTOM);
        window.setAttributes(lp);
    }
}
