package com.exam.student.applinks.whiteboard.views;

import android.graphics.Matrix;
import android.graphics.Path;

public class GraffitiPath {
    private Pen pen; // 画笔类型
    private Shape shape; // 画笔形状
    private float strokeWidth; // 大小
    private GraffitiColor color; // 颜色
    private Path path; // 画笔的路径
    private float sx, sy; // 映射后的起始坐标，（手指点击）
    private float dx, dy; // 映射后的终止坐标，（手指抬起）
    private Matrix matrix; //　仿制图片的偏移矩阵


    public static GraffitiPath toShape(Pen pen, Shape shape, float width, GraffitiColor color,
                                       float sx, float sy, float dx, float dy, Matrix matrix) {
        GraffitiPath graffitiPath = new GraffitiPath();
        graffitiPath.pen = pen;
        graffitiPath.shape = shape;
        graffitiPath.strokeWidth = width;
        graffitiPath.color = color;
        graffitiPath.sx = sx;
        graffitiPath.sy = sy;
        graffitiPath.dx = dx;
        graffitiPath.dy = dy;
        graffitiPath.matrix = matrix;
        return graffitiPath;
    }

    public static GraffitiPath toPath(Pen pen, Shape shape, float width, GraffitiColor color, Path path, Matrix matrix) {
        GraffitiPath graffitiPath = new GraffitiPath();
        graffitiPath.pen = pen;
        graffitiPath.shape = shape;
        graffitiPath.strokeWidth = width;
        graffitiPath.color = color;
        graffitiPath.path = path;
        graffitiPath.matrix = matrix;
        return graffitiPath;
    }


    public Pen getPen() {
        return pen;
    }

    public void setPen(Pen pen) {
        this.pen = pen;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public GraffitiColor getColor() {
        return color;
    }

    public void setColor(GraffitiColor color) {
        this.color = color;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public float getSx() {
        return sx;
    }

    public void setSx(float sx) {
        this.sx = sx;
    }

    public float getSy() {
        return sy;
    }

    public void setSy(float sy) {
        this.sy = sy;
    }

    public float getDx() {
        return dx;
    }

    public void setDx(float dx) {
        this.dx = dx;
    }

    public float getDy() {
        return dy;
    }

    public void setDy(float dy) {
        this.dy = dy;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }
}