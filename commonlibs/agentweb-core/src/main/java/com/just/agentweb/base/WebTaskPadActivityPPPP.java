package com.just.agentweb.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;

import com.just.agentweb.AgentWeb;
import com.just.agentweb.App2;
import com.just.agentweb.LocalBroadcastManagers;
import com.just.agentweb.R;

import org.json.JSONException;
import org.json.JSONObject;

public class WebTaskPadActivityPPPP extends BaseAgentWebActivity {
    private String url;
    private String validateurl;
    private String AppToken;
    private String AnsweringState;
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";
    private TextView mTitleTextView;
    //    private String callbackData;
    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("WebTaskPadActivity".equals(intent.getAction())) {
                    finish();
                }
            } catch (Exception e) {
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_task);
        LinearLayout mLinearLayout = (LinearLayout) this.findViewById(R.id.container);
        Toolbar mToolbar = (Toolbar) this.findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setTitle("");
        mTitleTextView = (TextView) this.findViewById(R.id.toolbar_title);
        this.setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebTaskPadActivityPPPP.this.finish();
            }
        });
        getUrl();
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("WebTaskPadActivity");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);

    }


    protected String getUrl() {
        validateurl = getIntent().getStringExtra("validateurl");
        AppToken = getIntent().getStringExtra("AppToken");
        AnsweringState = getIntent().getStringExtra("AnsweringState");
        if (TextUtils.isEmpty(validateurl)) {
            finish();
        }
        //初始化js交互对象
        if (mAgentWeb != null) {
            //注入对象
            Log.e("testaaa", "AppToken: " + AppToken);
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterfaceWeb(mAgentWeb, this));
        }
//        String pingtoken = validateurl + "&token=" + AppToken;
        return validateurl;
    }

    private Handler deliver = new Handler(Looper.getMainLooper());

    public class AndroidInterfaceWeb {
        private AgentWeb agent;
        private Context context;
        private String callbackData;

        public AndroidInterfaceWeb(AgentWeb agent, Context context) {
            this.agent = agent;
            this.context = context;
        }

        @JavascriptInterface
        public String getAppToken() {
            deliver.post(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("token", AppToken);
                        callbackData = obj.toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            return callbackData;
        }
    }


    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.container);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected int getIndicatorColor() {
        return Color.parseColor("#ff0000");
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 20) {
                title = title.substring(0, 20).concat("...");
            }
        }
        mTitleTextView.setText(title);
    }

    @Override
    protected int getIndicatorHeight() {
        return 3;
    }


    @Override
    protected void onDestroy() {
        if (AnsweringState.equals(TYPE_TODY)) {
            Intent msgIntent = new Intent();
            msgIntent.putExtra("typestate", TYPE_TODY);
            msgIntent.setAction("WebViewPadEvent");
            LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
//            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_TODY));
        } else {
            Intent msgIntent = new Intent();
            msgIntent.putExtra("typestate", TYPE_RECENTLY);
            msgIntent.setAction("WebViewPadEvent");
            LocalBroadcastManagers.getInstance(App2.get()).sendBroadcast(msgIntent);
//            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_RECENTLY));
        }
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
