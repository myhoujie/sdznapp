package rain.coder.photopicker.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import rain.coder.library.R;
import rain.coder.photopicker.BaseActivity;
import rain.coder.photopicker.adapter.PhotoPickAdapter;
import rain.coder.photopicker.bean.Photo;
import rain.coder.photopicker.bean.PhotoDirectory;
import rain.coder.photopicker.bean.PhotoPickBean;
import rain.coder.photopicker.controller.PhotoPickConfig;
import rain.coder.photopicker.controller.PhotoPreviewConfig;
import rain.coder.photopicker.loader.MediaStoreHelper;
import rain.coder.photopicker.utils.ImageUtils;
import rain.coder.photopicker.utils.UCropUtils;
import rain.coder.photopicker.utils.UtilsHelper;

/**
 * Descriptions :照片选择器
 * GitHub : https://github.com/Rain0413
 * Blog   : http://blog.csdn.net/sinat_33680954
 * Created by Rain on 16-12-7.
 */
public class PhotoPickActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvPreView;
    private TextView tvConfirm;
    private TextView tvTitle;
    private TextView tvClip;
    //权限相关
    public static final int REQUEST_CODE_SDCARD = 100;             //读写权限请求码
    public static final int REQUEST_CODE_CAMERA = 200;             //拍照权限请求码

    public static final int REQUEST_CODE_SHOW_CAMERA = 0;// 拍照
    public static final int REQUEST_CODE_CLIP = 1;//裁剪头像

    private PhotoPickAdapter adapter;
    private PhotoPickBean pickBean;
    private Uri cameraUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_photo_pick);

        Bundle bundle = getIntent().getBundleExtra(PhotoPickConfig.EXTRA_PICK_BUNDLE);
        if (bundle == null) {
            throw new NullPointerException("bundle is null,please init it");
        }
        pickBean = bundle.getParcelable(PhotoPickConfig.EXTRA_PICK_BEAN);
        if (pickBean == null) {
            finish();
            return;
        }
        init();
    }

    private void init() {
        RecyclerView recyclerView = (RecyclerView) this.findViewById(R.id.recyclerView);
        TextView tvCancel = (TextView) this.findViewById(R.id.tvCancel);
        tvTitle = (TextView) this.findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.all_photo));
        tvTitle.setOnClickListener(this);
        tvConfirm = (TextView) this.findViewById(R.id.tvConfirm);
        tvClip = (TextView) this.findViewById(R.id.tvClip);
        tvPreView = (TextView) this.findViewById(R.id.tvPreView);
        tvClip.setVisibility(pickBean.isMustClip() ? View.GONE : View.VISIBLE);
        tvConfirm.setText(pickBean.isMustClip() ? "裁剪" : "确定");
        tvConfirm.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvClip.setOnClickListener(this);
        tvPreView.setOnClickListener(this);
        tvPreView.setClickable(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this, pickBean.getSpanCount()));
        adapter = new PhotoPickAdapter(this, pickBean);
        recyclerView.setAdapter(adapter);

        adapter.setOnUpdateListener(new PhotoPickAdapter.OnUpdateListener() {

            @Override
            public void updateSelect(int select) {
                if (select > 0) {
                    tvConfirm.setText(pickBean.isMustClip() ? "裁剪" : "确定"+"(" + select + ")");
                    tvPreView.setClickable(true);
                    tvPreView.setTextColor(0xFF323C47);
                    tvConfirm.setTextColor(0xFFFFFFFF);
                    tvConfirm.setBackground(getResources().getDrawable(R.drawable.selector_confirm));
                } else {
                    tvConfirm.setText(pickBean.isMustClip() ? "裁剪" : "确定");
                    tvPreView.setClickable(false);
                    tvPreView.setTextColor(0xFFB1B5BA);
                    tvConfirm.setTextColor(0xFFB1B5BA);
                    tvConfirm.setBackground(null);
                }
                if (select == 1) {
                    tvClip.setClickable(true);
                    tvClip.setTextColor(0xFF323C47);
                } else {
                    tvClip.setClickable(false);
                    tvClip.setTextColor(0xFFB1B5BA);
                }
            }
        });

        MediaStoreHelper.getPhotoDirs(this, new MediaStoreHelper.PhotosResultCallback() {
            @Override
            public void onResultCallback(final List<PhotoDirectory> directories) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.refresh(directories.get(0).getPhotos());
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_SHOW_CAMERA://相机
                findPhoto(adapter.getCameraUri());
                break;
            case UCrop.REQUEST_CROP:    //裁剪
                findClipPhoto(UCrop.getOutput(data));
                break;
            case UCrop.RESULT_ERROR:
                Throwable cropError = UCrop.getError(data);

                break;
            case PhotoPreviewConfig.REQUEST_CODE:
                boolean isBackPressed = data.getBooleanExtra("isBackPressed", false);
                if (!isBackPressed) {//如果上个activity不是按了返回键的，就是按了"发送"按钮
                    setResult(Activity.RESULT_OK, data);
                    finish();
                } else {//用户按了返回键，合并用户选择的图片集合
                    ArrayList<Photo> photoLists = data.getParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST);
                    if (photoLists == null) {
                        return;
                    }
                    ArrayList<Photo> selectedList = adapter.getSelectPhotos();//之前已经选了的图片
                    ArrayList<Photo> selected = adapter.getPhotos();//之前已经选了的图片
                    List<Photo> deleteList = new ArrayList<>();//这是去图片预览界面需要删除的图片
                    for (Photo s : selectedList) {
                        if (!photoLists.contains(s)) {
                            deleteList.add(s);
                        }
                    }
                    selectedList.removeAll(deleteList);//删除预览界面取消选择的图片
                    selected.removeAll(deleteList);//删除预览界面取消选择的图片
                    deleteList.clear();
                    //合并相同的数据
                    HashSet<Photo> set = new HashSet<>(photoLists);
                    for (Photo s : selectedList) {
                        set.add(s);
                    }
                    selectedList.clear();
                    selectedList.addAll(set);
                    selected.clear();
                    selected.addAll(set);
                    tvConfirm.setText(pickBean.isMustClip() ? "裁剪" : "确定"+"(" + selectedList.size() + ")");
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private void findClipPhoto(String path) {
        Intent intent = new Intent();
        intent.putExtra(PhotoPickConfig.EXTRA_CLIP_PHOTO, path);
        intent.putExtra("isClip", true);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void findPhoto(Uri imageUri) {
        String filePath = UtilsHelper.getRealPathFromURI(imageUri, this);
        if (imageUri == null) {
            Toast.makeText(this, R.string.unable_find_pic, Toast.LENGTH_LONG).show();
        } else {
            if (pickBean.isClipPhoto()) {//拍完照之后，如果要启动头像裁剪，则去裁剪再把地址传回来
                startClipPic(filePath);
            } else {
                ArrayList<String> pic = new ArrayList<>();
                pic.add(filePath);
                Intent intent = new Intent();
                intent.putStringArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST, pic);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    /**
     * TODO 裁剪图片
     *
     * @param picPath
     */
    public void startClipPic(String picPath) {
        String imagePath = ImageUtils.getImagePath(this, "/Crop/");
        File corpFile = new File(imagePath + ImageUtils.createFile());
        UCropUtils.start(this, new File(picPath), corpFile, false);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.image_pager_exit_animation);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvCancel) {
            finish();
        } else if (view.getId() == R.id.tvConfirm) {
            if (adapter == null || adapter.getSelectPhotos() == null || adapter.getSelectPhotos().isEmpty()) {
                Toast.makeText(PhotoPickActivity.this, "请选择照片", Toast.LENGTH_SHORT).show();
                return;
            }
            if (pickBean.isMustClip()) {
                GalleryFinal.init(this);
                GalleryFinal.openCrop(adapter.getSelectPhotos().get(0).getPath()
                        , new GalleryFinal.OnHanlderResultCallback() {
                    @Override
                    public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
                        if (resultList == null || resultList.isEmpty()) {
                            onHanlderFailure(reqeustCode, "裁剪失败");
                            return;
                        }
                        findClipPhoto(resultList.get(0).getPhotoPath());
                    }

                    @Override
                    public void onHanlderFailure(int requestCode, String errorMsg) {
                        Toast.makeText(PhotoPickActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                });
            }else {
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra(PhotoPickConfig.EXTRA_STRING_ARRAYLIST, adapter.getSelectPhotos());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

        } else if (view.getId() == R.id.tvClip) {
            if (adapter.getSelectPhotos() == null || adapter.getSelectPhotos().size() == 0) {
                Toast.makeText(PhotoPickActivity.this, "请选择照片", Toast.LENGTH_SHORT).show();
                return;
            }
            startClipPic(adapter.getSelectPhotos().get(0).getPath());
        } else if (view.getId() == R.id.tvPreView) {
            new PhotoPreviewConfig.Builder(this)
                    .setPosition(0)
                    .setMaxPickSize(pickBean.getMaxPickSize())
                    .setPhotos(adapter.getPhotos())
                    .setSelectPhotos(adapter.getSelectPhotos())
                    .setOriginalPicture(pickBean.isOriginalPicture())
                    .build();
        } else if (view.getId() == R.id.tvTitle) {
            final AlbumPop albumPop = new AlbumPop(this, new AlbumPop.OnItemClickListener() {
                @Override
                public void onClick(ArrayList<Photo> photos) {
                    if (adapter != null) {
                        adapter.refresh(photos);
                    }
                }
            });
            MediaStoreHelper.getPhotoDirs(this, new MediaStoreHelper.PhotosResultCallback() {
                @Override
                public void onResultCallback(final List<PhotoDirectory> directories) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            albumPop.showPopupWindow(tvTitle, directories, getWindowHeight() - getStatusBarHeight() - dp2px(44));
                        }
                    });
                }
            });
        }
    }

    public int getWindowHeight() {
        WindowManager m = getWindowManager();
        Display d = m.getDefaultDisplay();
        return d.getHeight();
    }

    /*
     * 获取状态栏高度
     * @return
     */
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int dp2px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
