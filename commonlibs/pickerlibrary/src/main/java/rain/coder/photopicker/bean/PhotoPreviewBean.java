package rain.coder.photopicker.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PhotoPreviewBean implements Parcelable {

    private int position;
    private ArrayList<Photo> photos;
    private ArrayList<Photo> selectPhotos;
    private int maxPickSize;
    private boolean originalPicture;//是否选择的是原图

    public PhotoPreviewBean() {
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    public ArrayList<Photo> getSelectPhotos() {
        return selectPhotos;
    }

    public void setSelectPhotos(ArrayList<Photo> selectPhotos) {
        this.selectPhotos = selectPhotos;
    }

    public int getMaxPickSize() {
        return maxPickSize;
    }

    public void setMaxPickSize(int maxPickSize) {
        this.maxPickSize = maxPickSize;
    }

    public boolean isOriginalPicture() {
        return originalPicture;
    }

    public void setOriginalPicture(boolean originalPicture) {
        this.originalPicture = originalPicture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.position);
        dest.writeTypedList(this.photos);
        dest.writeTypedList(this.selectPhotos);
        dest.writeInt(this.maxPickSize);
        dest.writeByte(this.originalPicture ? (byte) 1 : (byte) 0);
    }

    protected PhotoPreviewBean(Parcel in) {
        this.position = in.readInt();
        this.photos = in.createTypedArrayList(Photo.CREATOR);
        this.selectPhotos = in.createTypedArrayList(Photo.CREATOR);
        this.maxPickSize = in.readInt();
        this.originalPicture = in.readByte() != 0;
    }

    public static final Creator<PhotoPreviewBean> CREATOR = new Creator<PhotoPreviewBean>() {
        @Override
        public PhotoPreviewBean createFromParcel(Parcel source) {
            return new PhotoPreviewBean(source);
        }

        @Override
        public PhotoPreviewBean[] newArray(int size) {
            return new PhotoPreviewBean[size];
        }
    };
}
