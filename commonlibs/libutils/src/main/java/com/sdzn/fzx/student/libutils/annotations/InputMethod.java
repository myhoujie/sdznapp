package com.sdzn.fzx.student.libutils.annotations;

import androidx.annotation.IntDef;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static com.sdzn.fzx.student.libutils.annotations.InputMethod.HandWrite;
import static com.sdzn.fzx.student.libutils.annotations.InputMethod.KeyBoard;
import static com.sdzn.fzx.student.libutils.annotations.InputMethod.NONE;
import static com.sdzn.fzx.student.libutils.annotations.InputMethod.PenWrite;
import static com.sdzn.fzx.student.libutils.annotations.InputMethod.Picture;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.CLASS;

/**
 * @author 𝕽𝖊𝖎𝖘𝖊𝖓 at 2019-01-11
 */
@Retention(CLASS)
@IntDef({KeyBoard,PenWrite,HandWrite,Picture,NONE})
@Target({METHOD, PARAMETER, FIELD, LOCAL_VARIABLE})
public @interface InputMethod {
    int NONE = 0;
    int KeyBoard = 1;
    int PenWrite = 2;
    int Picture = 3;
    int HandWrite = 4;
}
