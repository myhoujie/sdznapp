package com.sdzn.fzx.student.libutils.util;


import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdzn.fzx.student.libutils.R;
import com.sdzn.fzx.student.libutils.app.App2;


/**
 * 描述：吐司工具类
 * <p>
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class ToastUtil {

    private static Toast mInstance;

    public static void showShortlToast(String msg) {
        showToast(App2.get(), msg, Toast.LENGTH_SHORT);
    }

    public static void showLonglToast(String msg) {
        showToast(App2.get(), msg, Toast.LENGTH_LONG);
    }

    public static void showToast(Context context, String msg, int length) {

        cancelToast();

        String toastStr = "<font color='#FFFFFF'>" + msg + "</font>";

        mInstance = Toast.makeText(context, Html.fromHtml(toastStr), length);
        mInstance.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout group = (LinearLayout) mInstance.getView();
        group.setBackground(context.getResources().getDrawable(R.drawable.shape_toast_background));
        int leftPadding = UiUtils.dp2px(context, 25);
        int topPadding = UiUtils.dp2px(context, 16);
        group.setPadding(leftPadding, topPadding, leftPadding, topPadding);
        mInstance.show();
    }

    public static void cancelToast() {
        if (mInstance != null) {
            mInstance.cancel();
            mInstance = null;
        }
    }

    /**
     * 添加换行符时，两行都可以同时居中
     *
     * @param msg
     * @author wangchunxiao 2017年3月6日
     */
    public static void showCenterBottomToast(String msg) {
        Toast toast = Toast.makeText(App2.get(), msg, Toast.LENGTH_SHORT);
        LinearLayout group = (LinearLayout) toast.getView();
        TextView tv = (TextView) group.getChildAt(0);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        toast.show();
    }

    /**
     * 网络异常顶部弹出框
     */
    public  static void showNetworkEorrToast(){
        Toast toast = new Toast(App2.get());
        View view=LayoutInflater.from(App2.get()).inflate(R.layout.layout_toast_network,null);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP,0,0);
        toast.show();
    }
}
