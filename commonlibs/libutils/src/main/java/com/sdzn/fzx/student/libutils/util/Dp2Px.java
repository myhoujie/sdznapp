package com.sdzn.fzx.student.libutils.util;

import android.content.Context;
import android.util.TypedValue;

import java.util.HashMap;

/**
 * dp转px
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/3/5.
 */
public class Dp2Px {
    private Dp2Px() {}

    private static HashMap<Float,Integer> dpMap = new HashMap<>();

    public static int convert(Context context, float dp) {
        Integer px = dpMap.get(dp);
        if (px != null) {
            return px;
        }
        px = (int)TypedValue.applyDimension(1, dp, context.getResources().getDisplayMetrics());
        dpMap.put(dp,px);
        return px;
    }
}
