package com.sdzn.fzx.student.libutils.util;

import android.text.TextUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by 张超
 * 功能介绍：
 * 修改内容：新增
 * 修改时间：2018/3/21
 * 修改单号：WSS368
 * 修改内容：。。。。。。
 */

public class FileSizeUtil {


    public static long getFolderSize(File file) throws Exception {

        long size = 0;

        File[] fileList = file.listFiles();

        for (int i = 0; i < fileList.length; i++)

        {

            if (fileList[i].isDirectory())

            {

                size = size + getFolderSize(fileList[i]);

            } else

            {

                size = size + fileList[i].length();

            }

        }

        return size / 1048576;

    }

    /**
     * 删除指定目录下文件及目录
     *
     * @param deleteThisPath
     * @param filepaths
     * @return
     */

    public static int deleteFolderFile(String filePath, boolean deleteThisPath) throws IOException {

        if (!TextUtils.isEmpty(filePath)) {

            File file = new File(filePath);


            if (file.isDirectory()) {// 处理目录

                File files[] = file.listFiles();

                for (int i = 0; i < files.length; i++) {

                    deleteFolderFile(files[i].getAbsolutePath(), true);

                }

            }

            if (deleteThisPath) {

                if (!file.isDirectory()) {// 如果是文件，删除

                    file.delete();

                } else {// 目录

                    if (file.listFiles().length == 0) {// 目录下没有文件或者目录，删除

                        file.delete();

                    }

                }

            }

        }
        return 0;
    }
}
