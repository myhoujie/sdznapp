package com.sdzn.fzx.student.libutils.util;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * StringUtils〈一句话功能简述〉
 * 〈功能详细描述〉
 *
 * @author lixinbin
 * @version v1.0.0
 * @see ［相关类/方法］
 * @since 产品/模块版本
 */
public class StringUtils {

    public static boolean StringsEmpty(List<String> strs) {
        for (String str : strs) {
            if (TextUtils.isEmpty(str))
                return false;
        }
        return true;
    }

    public static boolean vertifyStringCount(String str, int least, int mostest) {
        if (str.length() >= least && str.length() <= mostest) {
            return true;
        }
        return false;
    }

    /**
     * 验证中文英文数字下划线
     *
     * @param str
     * @return
     */
    public static boolean vertifyString(String str) {
        String s = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]+$";
        return str.matches(s);
    }

    public static boolean ZhString(String str) {
        String s = "[\\u4e00-\\u9fa5]+";
        return str.matches(s);
    }

    /**
     * 验证手机格式
     */
    public static boolean isMobile(String number) {
        String numMatcher = "[1][0123456789]\\d{9}";
        if (TextUtils.isEmpty(number)) {
            return false;
        } else {
            return number.matches(numMatcher);
        }
    }

    /**
     * 6-16位 英文数字下划线
     *
     * @param psw
     * @return
     */
    public static boolean vertifyPsw(String psw) {
        return psw.matches("[A-Za-z0-9_]{6,16}");
    }


    /**
     * 格式化时间
     *
     * @param time 时间long 值
     * @param str  返回的字符串
     * @return
     */
    public static String transTime(long time, String str) {
        Date date = new Date(time);
        return transTime(date, str);
    }

    /**
     * 格式化时间
     *
     * @param date 时间Date
     * @param str  返回的字符串
     * @return
     */
    public static String transTime(Date date, String str) {
        SimpleDateFormat format = new SimpleDateFormat(str);
        return format.format(date);
    }

    public static String formatPhoneNum(String phone) {

        if (phone.length() != 11)
            return phone;

        final String start = phone.substring(0, 3);
        final String mid = phone.substring(3, 7);
        final String end = phone.substring(7, 11);
        return start + " " + mid + " " + end;

    }


}

