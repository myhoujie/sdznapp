package com.scwang.smartrefresh.layout.api;


import android.annotation.SuppressLint;

import androidx.annotation.RestrictTo;

import static androidx.annotation.RestrictTo.Scope.LIBRARY;
import static androidx.annotation.RestrictTo.Scope.LIBRARY_GROUP;
import static androidx.annotation.RestrictTo.Scope.SUBCLASSES;

/**
 * 刷新头部
 * Created by SCWANG on 2017/5/26.
 */
@SuppressLint("SupportAnnotationUsage")
@RestrictTo({LIBRARY,LIBRARY_GROUP,SUBCLASSES})
public interface RefreshHeader extends RefreshInternal {

}
