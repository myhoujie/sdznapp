package com.scwang.smartrefresh.layout.api;

import android.content.Context;

/**
 * 默认Footer创建器
 * Created by SCWANG on 2018/1/26.
 */

public interface DefaultRefreshFooterCreator {
    RefreshFooter createRefreshFooter(Context context, RefreshLayout layout);
}
