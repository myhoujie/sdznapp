package com.sdzn.core.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;

import androidx.appcompat.widget.AppCompatImageView;

import com.sdzn.core.R;


/**
 * 描述：环形加载
 * -
 * 创建人：鲍圣祥
 * 创建时间：2016/11/14
 */
public class CircularView extends AppCompatImageView {
    private float mWidth = 0f;
    private float mPadding = 0f;
    private float startAngle = 0f;

    private Paint mPaint;
    private RectF rectF;
    private ValueAnimator valueAnimator;
    private int circularColor = Color.GRAY;
    private int paintWidth = 8;
    private float sweepAngle = 60;
    private static final int DEFAULT_MAX_WIDTH = 100;


    public CircularView(Context context) {
        this(context, null);
    }

    public CircularView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint(context, defStyleAttr);
    }

    private void initPaint(Context context, int defStyleAttr) {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(paintWidth);
        mPaint.setAntiAlias(true);
        rectF = new RectF();
        if (defStyleAttr != 0) {
            TypedArray typedArray = context.obtainStyledAttributes(defStyleAttr, R.styleable.CircularView);
            circularColor = typedArray.getColor(R.styleable.CircularView_circular_color, circularColor);
            typedArray.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measure(widthMeasureSpec), measure(heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mWidth = Math.min(getWidth(), getHeight());
        getPadding();
        rectF.set(mPadding + paintWidth, mPadding + paintWidth, mWidth - mPadding - paintWidth, mWidth - mPadding - paintWidth);
        mPaint.setColor(circularColor);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2 - mPadding - paintWidth, mPaint);
        mPaint.setColor(Color.WHITE);
        canvas.drawArc(rectF, startAngle, sweepAngle, false, mPaint);//第四个参数是否显示半径
    }

    private int measure(int origin) {
        int result = DEFAULT_MAX_WIDTH;
        int specMode = MeasureSpec.getMode(origin);
        int specSize = MeasureSpec.getSize(origin);
        if (specMode == MeasureSpec.UNSPECIFIED) {
            result = DEFAULT_MAX_WIDTH;
        } else if (specMode == MeasureSpec.AT_MOST) {
            result = Math.min(result, specSize);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        }
        return result;

    }

    private void getPadding() {
        int topPadding = getPaddingTop();
        int bottomPadding = getPaddingBottom();
        int leftPadding = getPaddingLeft();
        int rightPadding = getPaddingRight();
        mPadding = topPadding;
        if (bottomPadding > mPadding) {
            mPadding = bottomPadding;
        }
        if (leftPadding > mPadding) {
            mPadding = leftPadding;
        }
        if (rightPadding > mPadding) {
            mPadding = rightPadding;
        }
    }

    public void startAnim() {
        stopAnim();
        startViewAnim(0f, 1f, 1200);
    }

    public void stopAnim() {
        if (valueAnimator != null) {
            clearAnimation();
            valueAnimator.setRepeatCount(1);
            valueAnimator.cancel();
            valueAnimator.end();
        }
    }


    private ValueAnimator startViewAnim(float startF, float endF, long time) {
        valueAnimator = ValueAnimator.ofFloat(startF, endF);
        valueAnimator.setDuration(time);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);//无限循环
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);//

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                startAngle = 360 * value;
                invalidate();
            }
        });
        if (!valueAnimator.isRunning()) {
            valueAnimator.start();
        }

        return valueAnimator;
    }

    public int getCircularColor() {
        return circularColor;
    }

    public void setCircularColor(int circularColor) {
        this.circularColor = circularColor;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
    }
}