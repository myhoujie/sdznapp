package com.sdzn.core.utils;

import android.graphics.Bitmap;
import android.util.Base64;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 描述：转bate64
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/11
 */

public class ToBeat64Utils {

    /**
     * 将图片转成Base64字符串
     *
     * @param path 图片路径
     * @return base64字符串
     */
    public static String toBase64(@NonNull String path) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Bitmap bitmap = ImageUtils.comp(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, baos);
            bitmap.recycle();
//            byte[] bytesEnc = Base64.encode(baos.toByteArray());
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
//            return new String(bytesEnc);//返回Base64编码过的字节数组字符串
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
