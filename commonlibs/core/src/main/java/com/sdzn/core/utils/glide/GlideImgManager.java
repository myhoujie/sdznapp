package com.sdzn.core.utils.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sdzn.core.R;

import java.io.File;

public class GlideImgManager {

    public static void loadImage(Context context, String url, ImageView iv) {
        //原生 API
        Glide.with(context).load(url).crossFade().placeholder(R.mipmap.place)
                .error(R.mipmap.place).into(iv);
    }

    public static void loadImage(Context context, String url, int erroImg, int emptyImg, ImageView iv) {
        //原生 API  
        Glide.with(context).load(url).placeholder(emptyImg).error(erroImg).into(iv);
    }

    public static void loadImageNoErrImg(Context context, String url, final ImageView iv) {
        //原生 API
//        Glide.with(context).load(url).into(iv);
        Glide.with(context).load(url).asBitmap().error(R.mipmap.avatar).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                iv.setImageBitmap(resource);
            }
        });
    }


    public static void loadImage(Context context, File file, ImageView imageView) {
        Glide.with(context)
                .load(file)
                .into(imageView);
    }

    public static void loadImage(Context context, final int resourceId, final ImageView imageView) {
        Glide.with(context)
                .load(resourceId)
                .into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView iv, boolean isFitCenter) {
        //原生 API
        DrawableRequestBuilder<String> builder = Glide.with(context).load(url).fitCenter().crossFade()
                .placeholder(R.mipmap.place).error(R.mipmap.place).dontAnimate();
        if (isFitCenter) {
            builder.fitCenter().into(iv);
        } else {
            builder.centerCrop().into(iv);
        }
    }

//    //加载指定大小
//    public static void loadImageViewSize(Context mContext, String path, int width, int height, ImageView mImageView) {
//        Glide.with(mContext).load(path).override(width, height).into(mImageView);
//    }
//加载指定大小
    public static void loadImageViewSize(Context mContext, String path, int width, int height, ImageView mImageView) {
        Glide.with(mContext).load(path).override(width, height).placeholder(R.mipmap.place).centerCrop().into(mImageView);
    }

    //设置跳过内存缓存
    public static void loadImageViewCache(Context mContext, String path, ImageView mImageView) {
        Glide.with(mContext).load(path).skipMemoryCache(true).into(mImageView);
    }

    //设置加载动画
    public static void loadImageViewAnim(Context mContext, String path, int anim, ImageView mImageView) {
        Glide.with(mContext).load(path).animate(anim).into(mImageView);
    }

    //加载gif
    public static void loadGifImage(Context context, String url, ImageView iv) {
        Glide.with(context).load(url).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.place).error(R.mipmap.place).into(iv);
    }

    //加载静态gif
    public static void loadStaticGifImage(Context context, String url, ImageView iv) {
        Glide.with(context).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.mipmap.place).error(R.mipmap.place).into(iv);
    }

    //加载圆形图片
    public static void loadCircleImage(Context context, String url, ImageView iv) {
        Glide.with(context).load(url).placeholder(R.mipmap.place).error(R.mipmap.place)
                .transform(new GlideCircleTransform(context)).into(iv);
    }

    //加载圆角图片
    public static void loadRoundCornerImage(Context context, String url, ImageView iv, int radius) {
        Glide.with(context).load(url).placeholder(R.mipmap.place).error(R.mipmap.place)
                .transform(new GlideRoundTransform(context, radius)).into(iv);
    }

    //加载头像图片
    public static void loadAvatarImage(Context context, String url, ImageView iv) {
        //原生 API
        Glide.with(context).load(url).crossFade().placeholder(R.mipmap.avatar)
                .error(R.mipmap.avatar).into(iv);
    }

    public static void loadBitmap(Context context, String url, SimpleTarget simpleTarget) {
        Glide.with(context).load(url).asBitmap().into(simpleTarget); //方法中设置asBitmap可以设置回调类型
    }


    //设置监听的用处 可以用于监控请求发生错误来源，以及图片来源 是内存还是磁盘

    //设置监听请求接口
    public static void loadImageViewListener(Context mContext, String path, ImageView mImageView, RequestListener<String, GlideDrawable> requstlistener) {
        Glide.with(mContext).load(path).listener(requstlistener).into(mImageView);
    }

    //项目中有很多需要先下载图片然后再做一些合成的功能，比如项目中出现的图文混排

    //设置要加载的内容
    public static void loadImageViewContent(Context mContext, String path, SimpleTarget<GlideDrawable> simpleTarget) {
        Glide.with(mContext).load(path).centerCrop().into(simpleTarget);
    }

    //加载为bitmap
    public static void loadImageBitmapContent(Context mContext, String path, SimpleTarget<Bitmap> simpleTarget) {
        Glide.with(mContext).load(path).asBitmap().into(simpleTarget);
    }


    //清理磁盘缓存
    public static void GuideClearDiskCache(Context mContext) {
        //理磁盘缓存 需要在子线程中执行
        Glide.get(mContext).clearDiskCache();
    }

    //清理内存缓存
    public static void GuideClearMemory(Context mContext) {
        //清理内存缓存  可以在UI主线程中进行
        Glide.get(mContext).clearMemory();
    }
}