package com.sdzn.core.base;

import android.os.Bundle;

/**
 * 描述：所有Fragment的基类，任何Fragment必须继承它
 * 创建时间：16/11/4
 */
public abstract class BaseMVPFragment<V, T extends BasePresenter<V>> extends BaseFragment {

    protected T mPresenter; //presenter对象

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        mPresenter.attachView((V) this, getActivity());//view与presenter建立关联

    }

    @Override
    public void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }


    protected abstract T createPresenter();
}