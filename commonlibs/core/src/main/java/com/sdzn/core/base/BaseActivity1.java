package com.sdzn.core.base;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.NavigationBarUtil;
import com.sdzn.core.utils.StatusBarUtil;
import com.umeng.analytics.MobclickAgent;

import me.jessyan.autosize.AutoSizeCompat;


/**
 * 描述：
 * - activity基类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public abstract class BaseActivity1 extends AppCompatActivity {

    protected Context mContext = null;//context

    protected abstract int getLayoutResource();

    protected abstract void onInit(Bundle bundle);

    private long mCurrentMs = System.currentTimeMillis();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //去掉标题栏
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        StatusBarCompat.compat(this);
        AppManager.getAppManager().addActivity(this);
        int layoutResource = getLayoutResource();
        if (layoutResource != 0) {
            setContentView(layoutResource);
        }
//        ButterKnife.bind(this);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mContext = this;
        onInit(savedInstanceState);
        // 虚拟键盘
        if (NavigationBarUtil.hasNavigationBar(this)) {
//            NavigationBarUtil.initActivity(getWindow().getDecorView());
            NavigationBarUtil.hideBottomUIMenu(this);
        }
    }

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 375, true);//如果有自定义需求就用这个方法,,,,,宽度设置false,,高度设置true
        return super.getResources();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setStatusBar();
    }

    protected void setStatusBar() {
        StatusBarUtil.setColor(this, 0xFFFFB039, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);       //统计时长
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

//    /**
//     *点击空白处隐藏软键盘
//     */
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        if (null != this.getCurrentFocus()) {
//            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//            return mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
//        }
//        return super.onTouchEvent(event);
//    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().finishActivity(this);
        super.onDestroy();
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}
