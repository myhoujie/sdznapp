package com.sdzn.core.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.sdzn.core.R;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.NavigationBarUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.List;


public abstract class BaseFragment extends Fragment implements BaseView {

    protected View rootView;
    protected Context mContext = null;//context
    private static final String STATE_SAVE_IS_HIDDEN = "STATE_SAVE_IS_HIDDEN";


    protected boolean isVisible;
    protected boolean isPrepared; // 标志位，标志已经初始化完成。
    //    protected boolean mHasLoadedOnce;// 标志位，标志已经第一次请求完成。
    protected boolean isFirst = true;// 标志位，是否是第一次加载

    protected Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            BaseFragment.this.handleMessage(msg);
        }
    };

    protected void handleMessage(Message msg) {
    }

    protected abstract int getLayoutResource();

    protected abstract void onInit(Bundle savedInstanceState);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getLayoutResource() != 0) {
            rootView = inflater.inflate(getLayoutResource(), container, false);
        } else {
            rootView = super.onCreateView(inflater, container, savedInstanceState);
        }
        this.onInit(savedInstanceState);
        isPrepared = true;
        onVisible();
        if (savedInstanceState != null) {
            boolean isSupportHidden = savedInstanceState.getBoolean(STATE_SAVE_IS_HIDDEN);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (isSupportHidden) {
                ft.hide(this);
            } else {
                ft.show(this);
            }
            ft.commit();
        }
        // 虚拟键盘
        if (NavigationBarUtil.hasNavigationBar(getActivity())) {
//            NavigationBarUtil.initActivity(getWindow().getDecorView());
            NavigationBarUtil.hideBottomUIMenu(getActivity());
        }
        return rootView;
    }

    public void onBackPressed() {
        getFragmentManager().popBackStack();
    }

    public void showFragment(@IdRes int viewId, Fragment fragment, boolean addToBackStack) {
        showFragment(viewId, fragment, addToBackStack, false);
    }

    public void showFragmentAddAnimation(@IdRes int viewId, Fragment fragment, boolean addToBackStack) {
        showFragment(viewId, fragment, addToBackStack, true);
    }


    public void showFragment(@IdRes int viewId, Fragment fragment, boolean addToBackStack, boolean showAnimation) {
        if (fragment == null) {
            return;
        }
        FragmentManager manager = getActivity().getSupportFragmentManager();
        getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (showAnimation) {
            transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_motionless, 0, R.anim.slide_out_to_right);
        }
        String tag = fragment.getClass().getName();
        Fragment fr = manager.findFragmentByTag(tag);
        if (fr == null) {
            transaction.add(viewId, fragment, tag);
        } else {
            transaction.show(fr);
        }
        transaction.hide(this);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    /**
     * 在这里实现Fragment数据的缓加载.
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d("onCreate", getClass().getSimpleName());
        mContext = getActivity();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_SAVE_IS_HIDDEN, isHidden());
    }


    protected void onVisible() {
        if (isPrepared && isVisible && isFirst) {
            lazyLoad();
        }
    }

    protected void lazyLoad() {

    }

    protected void onInvisible() {
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(getActivity().getLocalClassName());
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(getActivity().getLocalClassName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }
}
