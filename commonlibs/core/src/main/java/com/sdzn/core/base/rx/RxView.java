package com.sdzn.core.base.rx;


import com.sdzn.core.base.BaseView;

public interface RxView<T> extends BaseView {

    void onReceiveData2Api(T t, boolean isMore);
}
