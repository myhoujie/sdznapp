package com.sdzn.core.network.func;

import android.util.Log;

import com.sdzn.core.network.exception.FactoryException;

import rx.Observable;
import rx.functions.Func1;

/**
 * 异常处理
 */

public class ExceptionFunc<T> implements Func1<Throwable, Observable<T>> {
    @Override
    public Observable call(Throwable throwable) {
        Log.e("Tag", "-------->" + throwable.getMessage());
        return Observable.error(FactoryException.analysisExcetpion(throwable));
    }
}