/*
 * Copyright (C) 2014 pengjianbo(pengjianbosoft@gmail.com), Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package cn.finalteam.galleryfinal;

//import androidx.annotation.IntRange;
//
//import cn.finalteam.galleryfinal.model.PhotoInfo;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Collection;

/**
 * Desction:
 * Author:pengjianbo
 * Date:15/12/2 上午10:45
 */
public class FunctionConfig implements Cloneable{

    protected boolean crop;//裁剪
    private boolean rotateReplaceSource;//旋转是否覆盖源文件
    private boolean cropReplaceSource;//裁剪是否覆盖源文件
    private boolean forceCrop;//强制裁剪
    private boolean forceCropEdit;//强制裁剪后是否可对图片编辑，默认不可以

    private FunctionConfig(final Builder builder) {
        this.crop = builder.crop;
        this.rotateReplaceSource = builder.rotateReplaceSource;
        this.cropReplaceSource = builder.cropReplaceSource;
        this.forceCrop = builder.forceCrop;
        this.forceCropEdit = builder.forceCropEdit;
    }

    public static class Builder {
        private boolean crop;//裁剪
        private boolean rotateReplaceSource;//旋转是否覆盖源文件
        private boolean cropReplaceSource;//裁剪是否覆盖源文件
        private boolean forceCrop;//强制裁剪
        private boolean forceCropEdit;//强制裁剪后是否可对图片编辑，默认不可以

        public Builder setEnableCrop(boolean enable) {
            this.crop = enable;
            return this;
        }

        /**
         * 设置旋转后是否替换原图
         */
        public Builder setRotateReplaceSource(boolean rotateReplaceSource) {
            this.rotateReplaceSource = rotateReplaceSource;
            return this;
        }

        /**
         * 设置裁剪后是否替换原图
         */
        public Builder setCropReplaceSource(boolean cropReplaceSource) {
            this.cropReplaceSource = cropReplaceSource;
            return this;
        }

        /**
         * 强制裁剪
         */
        public Builder setForceCrop(boolean forceCrop) {
            this.forceCrop = forceCrop;
            return this;
        }

        /**
         * 强制裁剪后是否可以对图片编辑，默认不可编辑
         */
        public Builder setForceCropEdit(boolean forceCropEdit) {
            this.forceCropEdit = forceCropEdit;
            return this;
        }

        public FunctionConfig build() {
            return new FunctionConfig(this);
        }
    }


    public boolean isCrop() {
        return crop;
    }

    public boolean isRotateReplaceSource() {
        return rotateReplaceSource;
    }

    public boolean isCropReplaceSource() {
        return cropReplaceSource;
    }

    public boolean isForceCrop() {
        return forceCrop;
    }

    public boolean isForceCropEdit() {
        return forceCropEdit;
    }

    @Override
    public FunctionConfig clone() {
        FunctionConfig o = null;
        try {
            o = (FunctionConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return o;
    }
}
