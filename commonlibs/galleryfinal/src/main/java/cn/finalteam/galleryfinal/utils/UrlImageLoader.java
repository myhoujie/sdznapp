package cn.finalteam.galleryfinal.utils;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.ImageViewTarget;

import cn.finalteam.galleryfinal.ImageLoader;
import cn.finalteam.galleryfinal.PhotoEditActivity;
import cn.finalteam.galleryfinal.R;

/**
 * @author Created by 𝕽𝖊𝖎𝖘𝖊𝖓 on 2019/4/11.
 */
public class UrlImageLoader implements ImageLoader {
    @Override
    public void displayImage(final Activity activity, String path, final ImageView imageView, Drawable defaultDrawable) {
        Glide.with(activity)
                .load("file://" + path)
                .placeholder(defaultDrawable)
                .error(defaultDrawable)
//                .override(width, height)
                .diskCacheStrategy(DiskCacheStrategy.NONE) //不缓存到SD卡
                .skipMemoryCache(true)
                .into(new ImageViewTarget<GlideDrawable>(imageView) {
                    @Override
                    protected void setResource(GlideDrawable glideDrawable) {
                        imageView.setImageDrawable(glideDrawable);
                        if (activity instanceof PhotoEditActivity) {
                            ((PhotoEditActivity) activity).onDisplayImageSuccess();
                        }
                    }

                    @Override
                    public void setRequest(Request request) {
                        imageView.setTag(R.id.image_key,request);
                    }

                    @Override
                    public Request getRequest() {
                        return (Request) imageView.getTag(R.id.image_key);
                    }
                });
    }

    @Override
    public void clearMemoryCache() {

    }
}
