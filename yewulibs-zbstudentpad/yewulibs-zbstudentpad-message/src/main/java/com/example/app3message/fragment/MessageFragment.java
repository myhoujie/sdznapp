package com.example.app3message.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.EmptySchoolLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.MessageBean;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3message.R;
import com.example.app3message.adapter.MessageAdapter;
import com.example.app3message.presenter.MessagePresenter;
import com.example.app3message.view.MessageView;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息列表
 *
 * @author Reisen at 2017-12-08
 */

public class MessageFragment extends BaseMVPFragment<MessageView, MessagePresenter> implements MessageView, OnRefreshLoadmoreListener, BaseRcvAdapter.OnItemClickListener {

    TitleBar titleBar;
    RecyclerView recyclerMessage;
    LinearLayout llSelectall;
    TextView tvDel;
    RelativeLayout rlCompile;
    TextView edit;
    SmartRefreshLayout refreshLayout;
    ImageView imgCheckbox;
    EmptyLayout emptyLayout;
    EmptySchoolLayout emptySchoolLayout;
    View views;
    private MessageAdapter messageAdapter;
    private List<MessageBean.LetterListBean> mData = new ArrayList<>();
    private List<String> ids = new ArrayList<>();//存储id用于批量删除
    private List<MessageBean.LetterListBean> mDelData = new ArrayList<>();//存储用于批量删除bean
    private boolean isOkShow = false;//默认是编辑字体，当完成出现时设为true，进入编辑状态
    private int pageIndex = 1;
    private int pageSize = 20;
    private String messageType;//消息的类型
    private boolean isRefresh = false;
    public static final String ID_MESSAGE = "message_id";
    public static final String TYPE_MESSAGE = "message_type";
    public static final String CONTENT = "message_content";
    public static final String ADD_TIME = "message_add_time";

    public static MessageFragment newInstance() {
        return new MessageFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_message;
    }

    @Override
    protected void onInit(Bundle bundle) {
        EventBus.getDefault().register(this);
        initView();
        setInitOnResume();
    }

    private void setInitOnResume() {
        if (!SPToken.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_login));
        } else {
            views.setVisibility(View.GONE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            initData();
//            mPresenter.getUnReadMessageCount();
        }

    }

    private void initData() {
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    private void initView() {
        titleBar = rootView.findViewById(R.id.title_bar);
        recyclerMessage = rootView.findViewById(R.id.swipe_target);
        llSelectall = rootView.findViewById(R.id.ll_selectall);
        TextView tvDel = rootView.findViewById(R.id.tv_del);
        rlCompile = rootView.findViewById(R.id.rl_compile);
        refreshLayout = rootView.findViewById(R.id.refresh_layout);
        imgCheckbox = rootView.findViewById(R.id.img_checkbox);
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        emptySchoolLayout = rootView.findViewById(R.id.empty_layout_user);
        views = rootView.findViewById(R.id.view);
        llSelectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onViewClicked();
            }
        });

        titleBar.getView(R.id.iv_right).setVisibility(View.INVISIBLE);
        edit = (TextView) titleBar.getView(R.id.tv_right);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOkShow) {
                    //当点击的是编辑时
                    edit.setText("完成");
                    ids.clear();
                    refreshLayout.setEnableRefresh(false);
                    refreshLayout.setEnableLoadmore(false);
                    for (MessageBean.LetterListBean bean : mData) {
                        bean.setSelected(false);
                    }
                    rlCompile.setVisibility(View.VISIBLE);
                    messageAdapter.setEdit(true);

                } else {//当点击的是完成时
                    edit.setText("编辑");
                    refreshLayout.setEnableRefresh(true);
                    refreshLayout.setEnableLoadmore(true);
                    rlCompile.setVisibility(View.GONE);
                    messageAdapter.setEdit(false);
                    imgCheckbox.setSelected(false);
                }
                isOkShow = !isOkShow;
                messageAdapter.notifyDataSetChanged();

            }
        });
        tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.delMessage(ids);

            }
        });
        recyclerMessage.setLayoutManager(new LinearLayoutManager(mContext));
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });
        messageAdapter = new MessageAdapter(mContext, mData);
        recyclerMessage.setAdapter((RecyclerView.Adapter) messageAdapter);
        refreshLayout.setOnRefreshLoadmoreListener(this);
        messageAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            setInitOnResume();
        }
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        initData();
//        mPresenter.getUnReadMessageCount();
    }

    public void onViewClicked() {
        boolean is = imgCheckbox.isSelected();
        if (imgCheckbox.isSelected()) {
            //是全选时
            ids.clear();
            imgCheckbox.setSelected(false);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(false);
            }
        } else {
            //非全选时
            for (MessageBean.LetterListBean bean : mData) {
                ids.add(String.valueOf(bean.getId()));
            }
            imgCheckbox.setSelected(true);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(true);
            }
        }
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    protected MessagePresenter createPresenter() {
        return new MessagePresenter();
    }

    @Override
    public void listMessage(List<MessageBean.LetterListBean> messages) {
        goneSwipView();
        if (messages != null && messages.size() > 0) {
            if (pageIndex == 1) {
                ids.clear();
                mData.clear();
            }
            mData.addAll(messages);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            messageAdapter.notifyDataSetChanged();
        } else {
            if (pageIndex == 1) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多消息了");
            }
        }
        mDelData.clear();

    }

    @Override
    public void onListError(String msg) {
        if (pageIndex == 1) {
            mData.clear();
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        }
        goneSwipView();
    }

    @Override
    public void delMessage() {
//           mData.removeAll(mDelData);
        pageIndex = 1;
        initData();
    }

    @Override
    public void delMessageOnError(String msg) {
        ToastUtils.showShort(msg);

    }

    @Override
    public void delAll() {

    }

    @Override
    public void delAllOnError(String msg) {

    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore(false);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (isOkShow) {
            //焦点应该作用在item的选择框上
            mData.get(position).setSelected(!mData.get(position).isSelected());
            messageAdapter.notifyDataSetChanged();
            if (mData.get(position).isSelected()) {
                ids.add(String.valueOf(mData.get(position).getId()));
                mDelData.add(mData.get(position));
            } else {
                ids.remove(String.valueOf(mData.get(position).getId()));
                mDelData.remove(mData.get(position));
            }
            //判断是否全选
            boolean isAll = true;
            for (MessageBean.LetterListBean bean : mData) {
                if (!bean.isSelected()) {
                    isAll = false;
                }

            }
            imgCheckbox.setSelected(isAll);
        } else if (mData.size() > position) {
            mData.get(position).setStatus("1");//进入详情标记为已读
            switch (mData.get(position).getType()) {
                case 1:
                    messageType = "系统消息";
                    break;
                case 2:
                    messageType = "站内信";
                    break;
                case 5:
                    messageType = "课程消息";
                    break;
                case 6:
                    messageType = "优惠券过期";
                    break;
                default:
                    break;
            }
//            IntentController.toMessageStatic(mContext, String.valueOf(mData.get(position).getId()), messageType);
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MessageDetailActivity");//MessageDetailActivity
//            intent.putExtra(ID_MESSAGE, String.valueOf(mData.get(position).getId()));
//            intent.putExtra(TYPE_MESSAGE, messageType);
//            startActivity(intent);

            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MessageDetailActivity");
            intent.putExtra(ID_MESSAGE, String.valueOf(mData.get(position).getId()));
            intent.putExtra(TYPE_MESSAGE, messageType);
            intent.putExtra(CONTENT, mData.get(position).getContent());
            intent.putExtra(ADD_TIME, mData.get(position).getAddTime());
            startActivity(intent);
            messageAdapter.notifyDataSetChanged();//表示已读消息
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            isRefresh = true;
        }
    }
}

