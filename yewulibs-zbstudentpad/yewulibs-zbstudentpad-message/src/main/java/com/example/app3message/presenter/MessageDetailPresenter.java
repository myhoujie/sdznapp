package com.example.app3message.presenter;

import android.text.TextUtils;

import com.example.app3libpublic.event.MsgCountEvent;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3message.R;
import com.example.app3message.view.MessageDetailView;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/21
 */

public class MessageDetailPresenter extends BasePresenter<MessageDetailView> {

    public void getMessageDetail(String id) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .setMessageDetail(id, "1")
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().getMessageSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    public void getUnReadMessageCount() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryUnReadMsgCount("0", SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        Double obj = (Double) o;
                        EventBus.getDefault().post(new MsgCountEvent(obj.intValue()));
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
