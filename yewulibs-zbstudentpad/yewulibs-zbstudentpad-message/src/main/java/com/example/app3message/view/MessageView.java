package com.example.app3message.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.MessageBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public interface MessageView extends BaseView {
    void listMessage(List<MessageBean.LetterListBean> message);

    void onListError(String msg);

    void delMessage();

    void delMessageOnError(String msg);

    void delAll();

    void delAllOnError(String msg);
}
