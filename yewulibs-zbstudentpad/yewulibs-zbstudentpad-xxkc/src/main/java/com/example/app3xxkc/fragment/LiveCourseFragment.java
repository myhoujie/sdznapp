package com.example.app3xxkc.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libpublic.widget.ExpandViewRect;
import com.example.app3xxkc.R;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;

/**
 * zs
 * <p>
 * 学校课程
 */
public class LiveCourseFragment extends BaseFragment {

    ImageView ivBarShop;

    public LiveCourseFragment() {

    }

    public static LiveCourseFragment newInstance() {
        return new LiveCourseFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_live;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        ivBarShop = rootView.findViewById(R.id.iv_bar_right);
        ExpandViewRect.expandViewTouchDelegate(ivBarShop, 10, 10, 10, 10);
        initView();
    }

    private void initData() {

    }

    private void initView() {
        ivBarShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!SPToken.autoLogin(mContext)) {
//                    IntentController.toLogin(mContext);
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
                    startActivity(intent);
                    return;
                }
//                IntentController.toShoppingCart(mContext);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");//ShoppingCartActivity
                startActivity(intent);
            }
        });

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_live_container, (Fragment) CourseFragment.newInstance(CourseCons.Type.LIVING, null)).commit();
    }

}
