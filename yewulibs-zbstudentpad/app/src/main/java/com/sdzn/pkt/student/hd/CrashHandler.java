package com.sdzn.pkt.student.hd;

/**
 * 描述：
 * - 全局异常捕获
 * 创建人：baoshengxiang
 * 创建时间：2017/8/24
 */
  /*public class CrashHandler implements Thread.UncaughtExceptionHandler {
  private static final String FILE_NAME_SUFFIX = ".trace";
    private Thread.UncaughtExceptionHandler mDefaultCrashHandler;
    private static CrashHandler mCrashHandler = new CrashHandler();

    private CrashHandler() {
    }


    public static CrashHandler getInstance() {
        return mCrashHandler;
    }

    public void init() {
        mDefaultCrashHandler = Thread.getDefaultUncaughtExceptionHandler();
        // 设置该 CrashHandler 为程序的默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        dumpExceptionToSDCard(ex);
        if (mDefaultCrashHandler != null) {
            mDefaultCrashHandler.uncaughtException(thread, ex);
        } else {
            killApp();
        }
    }

    private void killApp() {
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                ToastUtils.showShort("很抱歉，程序出现异常，即将退出");
                Looper.loop();
            }
        }.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LogUtils.w(e);
        }
        AppManager.getAppManager().appExit();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void dumpExceptionToSDCard(Throwable e) {
        if (!SDCardUtils.isSDCardEnable()) {
            if (BuildConfig.ISDEBUG) {
                LogUtils.w("sdcard unmounted,skip dump exception");
            }
        } else {
            File crashCache = CacheUtils.getCrashCache();
            String fileName = TimeUtils.millis2String(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
            File crashFile = new File(crashCache, fileName + FILE_NAME_SUFFIX);
            PrintWriter pw = null;
            FileWriter fw = null;
            BufferedWriter bw = null;
            try {
                fw = new FileWriter(crashFile);
                bw = new BufferedWriter(fw);
                pw = new PrintWriter(bw);
                pw.println(fileName);
                dumpPhoneInfo(pw);
                e.printStackTrace(pw);
                pw.close();
            } catch (Exception e1) {
                LogUtils.e("dump crash info failed");
            } finally {
                CloseUtils.closeIOQuietly(fw, bw, pw);
            }

        }

    }

    private void dumpPhoneInfo(PrintWriter pw) {
        pw.print("App version:" + App2Utils.getAppVersionName(App2.get()));
        pw.println("-" + App2Utils.getAppVersionCode(App2.get()));
        pw.print("OS version:" + Build.VERSION.RELEASE);
        pw.println("-" + Build.VERSION.SDK_INT);
        pw.println("Vendor:" + Build.MANUFACTURER);
        pw.println("Model:" + Build.MODEL);
        pw.println("CPU ABI:" + Build.CPU_ABI);
    }
}*/
