package com.sdzn.pkt.student.hd.application;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.baijiayun.BJYPlayerSDK;
import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.manager.DaoManger;
import com.example.app3libpublic.widget.ProgressHeader;
import com.example.slbappshare.fenxiang.JPushShareUtils;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.commonsdk.UMConfigure;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import cn.jiguang.analytics.android.api.JAnalyticsInterface;
import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.android.api.PlatformConfig;
import cn.jpush.android.api.JPushInterface;
import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.unit.Subunits;
import update.UpdateAppUtils;


public class App extends MultiDexApplication {
    public static App mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
//        initLog();//初始化日志配置
//        initCrashHandler();//全局捕获异常

        UpdateAppUtils.init(this);//初始化软件更新

        configHios();//初始化Hios协议

//        initNim(); //网易云
        initRefreshFram(); //初始化下拉刷新控件

        DaoManger.getInstance();//初始化GreenDao

        initBugly();//初始化bugly全局bug监管

        configShipei();//初始化屏幕适配

        //初始化极光分享
//        configShare();

        //初始化极光统计
//        configTongji();

        //网络初始化
        configRetrofitNet();

        //初始化极光推送
        InitJPush();

        //初始化Umeng统计
        configUmengTongji();

        /*初始化百家云*/
        InitBJYplayer();

    }

    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }

    private void InitBJYplayer() {
        new BJYPlayerSDK.Builder(this)
                .setDevelopMode(false)
                .setCustomDomain("b96152240")
                .build();
    }

    private void configShare() {
        JShareInterface.setDebugMode(true);
        PlatformConfig platformConfig = new PlatformConfig()
                .setWechat(JPushShareUtils.APP_ID, JPushShareUtils.APP_KEY)// wxa3fa50c49fcd271c 746c2cd0f414de2c256c4f2095316bd0
                .setQQ("1106011004", "YIbPvONmBQBZUGaN")
                .setSinaWeibo("374535501", "baccd12c166f1df96736b51ffbf600a2", "https://www.jiguang.cn")
                .setFacebook("1847959632183996", "JShareDemo")
                .setTwitter("fCm4SUcgYI1wUACGxB2erX5pL", "NAhzwYCgm15FBILWqXYDKxpryiuDlEQWZ5YERnO1D89VBtZO6q")
                .setJchatPro("1847959632183996");
        JShareInterface.init(this, platformConfig);
    }

    private void configTongji() {
        // 设置开启日志,发布时请关闭日志
        JAnalyticsInterface.setDebugMode(true);
        JAnalyticsInterface.init(this);
    }

    private void configUmengTongji() {
        UMConfigure.setLogEnabled(true);
        UMConfigure.init(this, "5f27c7f18b1bb63ff585ca69", "Umeng", UMConfigure.DEVICE_TYPE_PHONE, null);
    }

    private void InitJPush() {
        JPushInterface.setDebugMode(true);    // 设置开启日志,发布时请关闭日志
        JPushInterface.init(this);    // 初始化 JPush
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void configShipei() {
        AutoSizeConfig.getInstance().getUnitsManager()
                .setSupportDP(false)
                .setSupportSP(false)
                .setSupportSubunits(Subunits.MM);
        AutoSize.initCompatMultiProcess(mContext);
    }

    private void initBugly() {

        String packageName = App2Utils.getAppPackageName(mContext);
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());

        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setAppVersion(App2Utils.getAppVersionName(mContext));
        strategy.setAppPackageName(packageName);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));

        //正式发布 false    测试true
//        CrashReport.initCrashReport(this, "aac6574530", true,strategy);
        Bugly.init(this, "aac6574530", true, strategy);
    }

    private static String getProcessName(int pid) {

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    private void configHios() {
        HiosHelper.config(App2.get().getPackageName() + ".slbapp.ad.web.page", App2.get().getPackageName() + ".slbapp.web.page");
//        HiosHelper.config(AppUtils.getAppPackageName() + ".slbapp.ad.web.page", AppUtils.getAppPackageName() + ".slbapp.web.page");
//        HiosHelperzb.config(AppUtils.getAppPackageName() + ".slbpad.ad.web.page", AppUtils.getAppPackageName() + ".slbpad.agentweb.page");
    }

/*
    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init();
    }
*/


    //设置全局的Header构建器
    private void initRefreshFram() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                return new ProgressHeader(context).setSpinnerStyle(SpinnerStyle.Translate);
            }
        });
    }

//    private void initLog() {
//        new LogUtils.Builder(this)
//                .setGlobalTag(getPackageName())
//                .setBorderSwitch(false)
//                .setLogSwitch(BuildConfig.DEBUG);
//    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1) {
            getResources();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Resources getResources() {
        Resources resources = super.getResources();
        if (resources.getConfiguration().fontScale != 1) {
            Configuration configuration = new Configuration();
            configuration.setToDefaults();
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return resources;
    }
}