package com.example.app3libpublic.widget;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.example.app3libpublic.R;


/**
 * 描述：模拟Dialog效果的PopupWindow
 * -
 * 创建人：wangchunxiao
 * 创建时间：2017/3/10
 */
public class PopDialog extends PopupWindow {

    private View mView;
    private View viewBg;
    private final static int time = 200;
    private Activity activity;

    public PopDialog(Activity activity) {
        this.activity = activity;
    }

    public void setContentView(View view, int gravity) {
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = view;
//        int h = context.getWindowManager().getDefaultDisplay().getHeight();
        int w = activity.getWindowManager().getDefaultDisplay().getWidth();
        // 设置SelectPicPopupWindow的View
        View contentView = (View) inflater.inflate(R.layout.layout_popdialog, null);
        viewBg = contentView.findViewById(R.id.viewBg);
        RelativeLayout content = (RelativeLayout) contentView.findViewById(R.id.content);
        LinearLayout linearLayout = (LinearLayout) contentView.findViewById(R.id.linearLayout);

        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        content.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction()
                        == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK)
                    dismiss();
                return false;
            }
        });

        linearLayout.addView(mView);
        linearLayout.setGravity(gravity);
        this.setContentView(contentView);
        //防止被底部虚拟键挡住
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.MATCH_PARENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
//        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
//        this.setBackgroundDrawable(dw);
        this.setAnimationStyle(R.style.AnimationAlpha);
    }

    public void showPopDialog(View parent) {
        if (!this.isShowing()) {
            this.showAtLocation(parent, Gravity.RIGHT, 0, 0);
//
            AlphaAnimation alphaAnim = new AlphaAnimation(0f, 1.0f);
            ScaleAnimation scaleAnim = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

            AnimationSet aimationSet = new AnimationSet(true);
            aimationSet.addAnimation(alphaAnim);
            aimationSet.addAnimation(scaleAnim);
            aimationSet.setDuration(time);
            aimationSet.setFillAfter(true);

            mView.startAnimation(aimationSet);
            viewBg.startAnimation(alphaAnim);
        }
    }

    public void dismissDialog() {
        AlphaAnimation alphaAnim = new AlphaAnimation(1.0f, 0);
        ScaleAnimation scaleAnim = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        AnimationSet aimationSet = new AnimationSet(true);
        aimationSet.addAnimation(alphaAnim);
        aimationSet.addAnimation(scaleAnim);
        aimationSet.setDuration(time);
        aimationSet.setFillAfter(true);
        mView.startAnimation(aimationSet);

        // 这里用补间动画会闪动，所以用属性动画
        ObjectAnimator anim = ObjectAnimator.ofFloat(viewBg, "alpha", 1.0f, 0f);
        anim.setDuration(time);
        anim.start();

        PopDialog.this.getContentView().postDelayed(new Runnable() {
            @Override
            public void run() {
                PopDialog.this.dismiss();
            }
        }, time);
    }
}