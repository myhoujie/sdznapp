package com.example.app3libpublic.fragment.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.CourseList;

import java.util.List;

public interface SpellingContentView extends BaseView {
    void getDataCourse(List<CourseList> courseLists);

    void onFailed(String msg);
}
