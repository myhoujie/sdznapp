package com.example.app3libpublic.adtivity.view;

import com.example.app3libvariants.bean.CourseList;
import com.sdzn.core.base.BaseView;

import java.util.List;

/**
 * 精品课列表
 */
public interface CourseExcellentView extends BaseView {

    void getDataFailure(String msg);


    void getDataCourse(List<CourseList> recommendCourses);

}
