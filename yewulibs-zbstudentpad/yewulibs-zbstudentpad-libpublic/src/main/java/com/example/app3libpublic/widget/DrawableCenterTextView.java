package com.example.app3libpublic.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * 描述：
 * - 文字图片居中的textview
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class DrawableCenterTextView extends AppCompatTextView {
    public DrawableCenterTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DrawableCenterTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawableCenterTextView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable[] drawables = getCompoundDrawables();
        Drawable drawableLeft = drawables[0];
        final float textWidth = getPaint().measureText(getText().toString());
        final int drawablePadding = getCompoundDrawablePadding();
        if (drawableLeft != null) {
            int drawableWidth = drawableLeft.getIntrinsicWidth();
            float bodyWidth = textWidth + drawableWidth + drawablePadding;
            if (bodyWidth > getWidth()) {
                bodyWidth = getWidth();
            }
            canvas.translate((getWidth() - bodyWidth) / 2, 0);
        }
        Drawable drawableRight = drawables[2];
        if (drawableRight != null) {
            setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            int drawableWidth = drawableRight.getIntrinsicWidth();
            float bodyWidth = textWidth + drawableWidth + drawablePadding;
            if (bodyWidth > getWidth()) {
                bodyWidth = getWidth();
            }
            canvas.translate(-(getWidth() - bodyWidth) / 2, 0);
        }

        super.onDraw(canvas);
    }
}