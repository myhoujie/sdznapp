package com.example.app3libpublic.listener;

import com.example.app3libvariants.bean.GradeJson;

/*
年级选中时回传
 */
public interface SendSelectData {
    void send(String eduId, GradeJson.ChildListBean bean);
}
