package com.example.app3libpublic.utils;

/**
 * 描述：
 * - 防止多次点击button触发事件
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SingleButtonUtils {
    private static long lastClickTime;

    public synchronized static boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    public synchronized static boolean isFastClick(long intervalTime) {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < intervalTime) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
}