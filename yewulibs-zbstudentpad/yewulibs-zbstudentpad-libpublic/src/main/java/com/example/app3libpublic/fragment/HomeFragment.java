package com.example.app3libpublic.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.app3libpublic.R;
import com.example.app3libpublic.adapter.RecommendAdapter;
import com.example.app3libpublic.adapter.WrapAdapter;
import com.example.app3libpublic.adtivity.CourseExcellentActivity;
import com.example.app3libpublic.adtivity.CourseTopicActivity;
import com.example.app3libpublic.fragment.presenter.HomePresenter;
import com.example.app3libpublic.fragment.view.HomeView;
import com.example.app3libpublic.utils.PermissionUtils;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.RoundImageView;
import com.example.app3libpublic.widget.bannerview.MZBannerView;
import com.example.app3libpublic.widget.bannerview.holder.MZHolderCreator;
import com.example.app3libpublic.widget.bannerview.holder.MZViewHolder;
import com.example.app3libpublic.widget.bannerview.transformer.CustomTransformer;
import com.example.app3libvariants.bean.BannerInfoBean;
import com.example.app3libvariants.bean.CourseList;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 描述：
 * - 首页
 */
public class HomeFragment extends BaseMVPFragment<HomeView, HomePresenter> implements HomeView, OnRefreshListener, OnRefreshLoadmoreListener {

    SmartRefreshLayout refreshLayout;
    EmptyLayout emptyLayout;
    MZBannerView fvBanner;
    RecyclerView rvCourseRec;
    RecyclerView rvSubject;
    private TextView tv_course;//推荐课程查看全部
    private TextView tv_subject;//专题课查看全部


    public static final String COURSE_TYPE = "courseType";
    public static final String COURSE_ID = "courseId";
    public static final String SHOW_LIVE_BTN = "showLiveBtn";
    public static final String PACKAGE = "PACKAGE";

    private List<BannerInfoBean> bannerInfos;
    private List<String> bannerUrls;
    private List<String> bannerlinkAddress;
    private List<String> bannerTypeid;
    private List<String> bannerselltype;


    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private List<CourseList> subjectCourses;
    private RecommendAdapter subjectAdapter;
    private WrapAdapter<RecommendAdapter> subjectwrapAdapter;


    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        fvBanner.start();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {

        refreshLayout = rootView.findViewById(R.id.refresh_layout);
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        fvBanner = rootView.findViewById(R.id.fv_banner);
        rvCourseRec = rootView.findViewById(R.id.rv_course);
        rvSubject = rootView.findViewById(R.id.rv_subject);
        tv_course = rootView.findViewById(R.id.tv_course);
        tv_subject = rootView.findViewById(R.id.tv_subject);

        tv_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, CourseExcellentActivity.class));
            }
        });
        tv_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, CourseTopicActivity.class));
            }
        });
        initData();
        initView();
        loadNetData();
        mPresenter.getBanner();
    }

    private void initData() {
        bannerInfos = new ArrayList<>();
        bannerUrls = new ArrayList<>();
        bannerlinkAddress = new ArrayList<>();
        bannerTypeid = new ArrayList<>();
        bannerselltype = new ArrayList<>();
        recommendCourses = new ArrayList<>();
        subjectCourses = new ArrayList<>();
    }

    private void initView() {
        refreshLayout.setOnRefreshLoadmoreListener(this);
//        rvCourseRec.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 4) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");//CourseDetailActivity
                    intent.putExtra(PACKAGE, recommendCourses.get(position).getSellType());
                    intent.putExtra(COURSE_ID, recommendCourses.get(position).getCourseId());
                    intent.putExtra(SHOW_LIVE_BTN, false);
                    startActivity(intent);
                }
            }
        });

        rvSubject.setFocusable(false);
        rvSubject.setLayoutManager(new GridLayoutManager(mContext, 4) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        subjectAdapter = new RecommendAdapter(mContext, subjectCourses);
        subjectwrapAdapter = new WrapAdapter<>(subjectAdapter);
        subjectwrapAdapter.adjustSpanSize(rvSubject);
        rvSubject.setAdapter(subjectwrapAdapter);

        subjectAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < subjectCourses.size()) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");//CourseDetailActivity
                    intent.putExtra(PACKAGE, subjectCourses.get(position).getSellType());
                    intent.putExtra(COURSE_ID, subjectCourses.get(position).getCourseId());
                    intent.putExtra(SHOW_LIVE_BTN, false);
                    startActivity(intent);
                }
            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNetData();
            }
        });
    }

    private void loadNetData() {
        mPresenter.getCourse(new HashMap<String, String>());
    }

    private void initBanner() {
        bannerUrls.clear();
        fvBanner.pause();
        for (BannerInfoBean bannerInfoBean : bannerInfos) {
            bannerUrls.add("" + bannerInfoBean.getImageUrl());
            bannerlinkAddress.add("" + bannerInfoBean.getLinkAddress());
            bannerTypeid.add("" + bannerInfoBean.getRelationType());
            bannerselltype.add("" + bannerInfoBean.getSellType());
        }
        fvBanner.setBannerPageClickListener(bannerPageClickListener);

        // 设置数据
        fvBanner.setPages(bannerUrls, new MZHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });
        fvBanner.setPageTransformer(new CustomTransformer());
        fvBanner.start();
    }

    /**
     * banner跳转
     */
    MZBannerView.BannerPageClickListener bannerPageClickListener = new MZBannerView.BannerPageClickListener() {
        @Override
        public void onPageClick(View view, int position) {
//            HiosHelper.resolveAd(getActivity(), getActivity(), "hios://com.haier.cellarette.libwebview.base.WebViewMainActivity2?aid={s}"+bannerlinkAddress.get(position));
            if (bannerTypeid.get(position).equals("0")) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebViewMainActivity");
                intent.putExtra("validateurl", bannerlinkAddress.get(position));
                startActivity(intent);
            } else {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");//CourseDetailActivity
                intent.putExtra(PACKAGE, bannerselltype.get(position));
                intent.putExtra(COURSE_ID, Integer.valueOf(bannerlinkAddress.get(position)));
                intent.putExtra(SHOW_LIVE_BTN, true);
                startActivity(intent);
            }
        }
    };


    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void getCourseEmpty() {
        clearLoingState();
    }

    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        this.recommendCourses.clear();
        if (courses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void getSubjectDataCourse(List<CourseList> subjectCourses) {
        clearLoingState();
        this.subjectCourses.clear();
        if (subjectCourses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.subjectCourses.addAll(subjectCourses);
        subjectwrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }


    @Override
    public void getBannerData(List<BannerInfoBean> infoBeanList) {
        this.bannerInfos.clear();
        this.bannerInfos.addAll(infoBeanList);
        initBanner();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionUtils.checkPermissionResult(mContext, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        loadNetData();
        mPresenter.getBanner();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {

    }

    private class BannerViewHolder implements MZViewHolder<String> {
        private RoundImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局
            mImageView = new RoundImageView(context);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);// X和Y方向都填满
            mImageView.setType(RoundImageView.TYPE_ROUND);
            mImageView.setBorderRadius(8);
            mImageView.setImageResource(R.mipmap.place);
            return mImageView;
        }

        @Override
        public void onBind(Context context, int position, String data) {
            // 数据绑定
//            GlideImgManager.loadImage(context, data, R.mipmap.place, R.mipmap.place, mImageView);
            Glide.with(mContext).load(data).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    mImageView.setImageBitmap(resource);
                }
            });
        }
    }

    private void clearLoingState() {
        if (refreshLayout != null) {
            if (refreshLayout.isRefreshing()) {
                refreshLayout.finishRefresh();
            }
            if (refreshLayout.isLoading()) {
                refreshLayout.finishLoadmore();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fvBanner.pause();
    }


}
