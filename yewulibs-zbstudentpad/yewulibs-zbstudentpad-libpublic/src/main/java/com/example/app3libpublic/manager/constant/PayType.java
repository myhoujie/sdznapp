package com.example.app3libpublic.manager.constant;

/**
 * 描述：
 * - 支付类型相关常量
 * 创建人：baoshengxiang
 * 创建时间：2017/7/24
 */
public class PayType {
    public static final String ALIPAY = "ALIPAY";
    public static final String WXPAY = "WEIXIN";
}
