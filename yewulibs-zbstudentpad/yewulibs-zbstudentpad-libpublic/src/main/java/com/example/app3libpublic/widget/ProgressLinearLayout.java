package com.example.app3libpublic.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;

import com.example.app3libpublic.R;


/**
 * 背景显示进度的LinearLayout
 *
 * @author Reisen at 2018-05-24
 */

public class ProgressLinearLayout extends LinearLayout {
    //进度0-1
    private float progress;
    private float progressWidth;
    @ColorInt
    private int fromColor;
    @ColorInt
    private int toColor;//渐变颜色
    private Paint mPaint = new Paint();
    private LinearGradient mGradient;

    public ProgressLinearLayout(Context context) {
        super(context);
        init(context, null);
    }

    public ProgressLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ProgressLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @RequiresApi(21)
    public ProgressLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProgressLinearLayout);
        fromColor = typedArray.getColor(R.styleable.ProgressLinearLayout_llFromColor, Color.WHITE);
        toColor = typedArray.getColor(R.styleable.ProgressLinearLayout_llToColor, Color.WHITE);
        typedArray.recycle();
        mGradient = new LinearGradient(0, 0, progressWidth, 0,
                fromColor, toColor, Shader.TileMode.REPEAT);
        mPaint.setShader(mGradient);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        progressWidth = progress * getWidth() * progress;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //100%时不绘制进度
        if (progress >= 1 || progress <= 0) {
            return;
        }
        canvas.drawRect(0, 0, progressWidth, getHeight(), mPaint);
    }

    public int getFromColor() {
        return fromColor;
    }

    public void setFromColor(int fromColor) {
        this.fromColor = fromColor;
        mGradient = new LinearGradient(0, 0, progressWidth, 0,
                fromColor, toColor, Shader.TileMode.REPEAT);
        mPaint.setShader(mGradient);
        postInvalidate();
    }

    public int getToColor() {
        return toColor;
    }

    public void setToColor(int color) {
        toColor = color;
        mGradient = new LinearGradient(0, 0, progressWidth, 0,
                fromColor, toColor, Shader.TileMode.REPEAT);
        mPaint.setShader(mGradient);
        postInvalidate();
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        if (this.progress == progress) {
            return;
        }
        if (progress <= 0) {
            progress = 0;
        }
        if (progress >= 1) {
            progress = 1;
        }
        this.progress = progress;
        progressWidth = progress * getWidth() * progress;
        mGradient = new LinearGradient(0, 0, progressWidth, 0,
                fromColor, toColor, Shader.TileMode.REPEAT);
        mPaint.setShader(mGradient);
        postInvalidate();
    }
}
