package com.example.app3libpublic.utils;

import android.content.res.Configuration;
import android.widget.TextView;

import androidx.annotation.ColorRes;

import com.sdzn.fzx.student.libutils.app.App2;
import com.example.app3libpublic.manager.Config;

import java.util.Locale;

/**
 * @author Reisen at 2018-01-19
 */

public final class PriceUtil {
    private PriceUtil() {
    }

    public static boolean isFree(double price) {
        return price >= -0.000001 && price <= 0.000001;
    }

    public static void setViewText(TextView view, String text, @ColorRes int color) {
        view.setText(text);
        view.setTextColor(view.getContext().getResources().getColor(color));
    }

    public static void setViewText(TextView view, double price, @ColorRes int color) {
        setViewText(view, String.format(Locale.getDefault(), "￥%1.2f", price), color);
    }

    /**
     * 判断当前设备是手机还是平板，代码来自 Google I/O App for Android
     */
    public static String isPad() {
        return (App2.get().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE ? Config.TABLET : Config.PHONE;
    }
}
