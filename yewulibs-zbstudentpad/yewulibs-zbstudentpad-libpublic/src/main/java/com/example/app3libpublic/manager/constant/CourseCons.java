package com.example.app3libpublic.manager.constant;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 描述：
 * - 课程相关常量
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public class CourseCons {
    public static class Type {
        public static final int ALL = 0;//全部
        public static final int LIVING = 1;//直播课
        public static final int VIDEO = 2;//点播课

        @IntDef({ALL, LIVING, VIDEO})
        @Retention(RetentionPolicy.SOURCE)
        public @interface CourseType {
        }
    }

    public static final class State {
        public static int DEFAULT = 0b000;//点播 & 收费 & 未购买
        public static int IS_LIVE = 0b100;//直播?
        public static int IS_FREE = 0b010;//免费?
        public static int IS_PURCHASE = 0x001;//已购买?

        /**
         * 是否免费
         */
        public static boolean isFree(int state) {
            return (state & IS_FREE) == IS_FREE;
        }

        /**
         * 是否为视频
         */
        public static boolean isLive(int state) {
            return (state & IS_LIVE) == IS_LIVE;
        }

        /**
         * 是否已购买
         */
        public static boolean isPurchase(int state) {
            return (state & IS_PURCHASE) == IS_PURCHASE;
        }
    }

    /**
     * NOTBEGIN, //未开始
     * LIVING, //直播中
     * ENDED,//已结束
     * COMINGSOON //"敬请期待
     */
    public static final class LiveStatus {
        //        public static final String Status_NOTBEGIN = "NOTBEGIN"; //未开始
        public static final String Status_LIVING = "LIVING"; //直播中
//        public static final String Status_ENDED = "ENDED";//已结束
//        public static final String Status_COMINGSOON = "COMINGSOON";//"敬请期待
//        public static final String Status_INVALID = "INVALID";//已失效


        public static final String Status_Chs_REST = "休息中";
        public static final String VIDEO_STUDY = "立即学习";

        public static final String LIVE_NOTBEGIN = "未开始";
        public static final String LIVE_LIVING = "观看直播";
        public static final String LIVE_REPLAY = "查看回放";
        public static final String LIVE_FINISHED = "已结束";
        public static final String INVALID = "已过期";


        public static final String LIVE_NOTBEGIN_NEW ="3";
        public static final String LIVE_LIVING_NEW = "4";
        public static final String LIVE_FINISHED_NEW = "5";
        public static final String LIVE_REPLAY_NEW = "6";





//该方法已弃用, 直播课程章节状态由英文字符串改为中文字符串
//        public static String upStatus(String state) {
//            if (Status_NOTBEGIN.equalsIgnoreCase(state)) {
//                state = "未开始";
//            } else if (Status_LIVING.equalsIgnoreCase(state)) {
//                state = "直播中";
//
//            } else if (Status_ENDED.equalsIgnoreCase(state)) {
//                state = "观看回放";
//
//            } else if (Status_INVALID.equalsIgnoreCase(state)) {
//                state = "课程已失效";
//            } else {
//                state = "敬请期待";
//            }
//            return state;
//        }

        public static String upStatus(String state) {
            if (LIVE_NOTBEGIN_NEW.equalsIgnoreCase(state)){
                return "未开始";
            } else if (LIVE_LIVING_NEW.equalsIgnoreCase(state)) {
                return "进入直播";
            }else if (LIVE_FINISHED_NEW.equalsIgnoreCase(state)){
                return "已结束";
            }else if (LIVE_REPLAY_NEW.equalsIgnoreCase(state)){
                return "观看回放";
            }else if (VIDEO_STUDY.equalsIgnoreCase(state)){
                return "开始学习";
            }else{
                return "敬请期待";
            }
        }


        public static boolean isLiving(String state) {
            return LIVE_LIVING_NEW.equalsIgnoreCase(state);
        }

        public static boolean isRest(String state) {
            return Status_Chs_REST.equalsIgnoreCase(state);
        }

        public static boolean isEnded(String state) {
            return LIVE_FINISHED_NEW.equalsIgnoreCase(state);
        }

        public static boolean isInvalid(String state) {
            return INVALID.equalsIgnoreCase(state);
        }


    }

}
