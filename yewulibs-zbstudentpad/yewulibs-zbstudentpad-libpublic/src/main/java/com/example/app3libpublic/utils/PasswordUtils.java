package com.example.app3libpublic.utils;

public class PasswordUtils {
    /**
     *输入的不符合 即返回true
     */
    public static boolean isNo(String pwStr) {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";

        return !pwStr.matches(regex);
    }
}
