package com.example.app3libpublic.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

import com.example.app3libpublic.R;


/**
 * 中间透明, 角上有弧形颜色区的控件 覆盖在布局上产生弧形边缘效果
 *
 * @author Reisen at 2017-12-01
 */

public class ShadeView extends View {
    @ColorInt
    private int mShadeColor = 0;
    private float mRadius;
    private float mRadiusLeftTop;
    private float mRadiusRightTop;
    private float mRadiusLeftBottom;
    private float mRadiusRightBottom;

    private Paint mTransparentPaint;
    private float[] rids;
    private RectF rect;
    private Path path;

    public ShadeView(Context context) {
        this(context, null);
    }

    public ShadeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShadeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public ShadeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mTransparentPaint = new Paint();
        mTransparentPaint.setColor(Color.TRANSPARENT);
        mTransparentPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTransparentPaint.setStrokeWidth(1);

        rect = new RectF();
        path = new Path();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ShadeView);
//        mShadeColor = typedArray.getColor(R.styleable.ShadeView_shadeColor, Color.TRANSPARENT);
        mRadius = typedArray.getDimension(R.styleable.ShadeView_shadeRadius, 0);
        mRadiusLeftTop = typedArray.getDimension(R.styleable.ShadeView_shadeRadiusLeftTop, 0);
        mRadiusRightTop = typedArray.getDimension(R.styleable.ShadeView_shadeRadiusRightTop, 0);
        mRadiusLeftBottom = typedArray.getDimension(R.styleable.ShadeView_shadeRadiusLeftBottom, 0);
        mRadiusRightBottom = typedArray.getDimension(R.styleable.ShadeView_shadeRadiusRightBottom, 0);
        typedArray.recycle();
        /*圆角的半径，依次为左上角xy半径，右上角，右下角，左下角*/
        rids = new float[]{mRadiusLeftTop, mRadiusLeftTop, mRadiusRightTop, mRadiusRightTop, mRadiusRightBottom,
                mRadiusRightBottom, mRadiusLeftBottom, mRadiusLeftBottom};
    }

    @Override
    protected void onDraw(Canvas canvas) {
        rect.set(0, 0, getWidth(), getHeight());
        path.reset();
        path.addRoundRect(rect, rids, Path.Direction.CW);
        path.setFillType(Path.FillType.INVERSE_EVEN_ODD);
        canvas.drawPath(path, mTransparentPaint);
        canvas.clipPath(path);
        canvas.drawColor(mShadeColor);
    }

    public int getShadeColor() {
        return mShadeColor;
    }

    public void setShadeColor(int shadeColor) {
        mShadeColor = shadeColor;
        postInvalidate();
    }

    public void setRadius(float radius) {
        mRadius = radius;
        mRadiusLeftTop = mRadius;
        mRadiusRightTop = mRadius;
        mRadiusLeftBottom = mRadius;
        mRadiusRightBottom = mRadius;
        rids = new float[]{mRadiusLeftTop, mRadiusLeftTop, mRadiusRightTop, mRadiusRightTop, mRadiusRightBottom,
                mRadiusRightBottom, mRadiusLeftBottom, mRadiusLeftBottom};
        postInvalidate();
    }

    public void setRadius(float radiusLeftTop, float radiusRightTop,
                          float radiusLeftBottom, float radiusRightBottom) {
        mRadiusLeftTop = radiusLeftTop;
        mRadiusRightTop = radiusRightTop;
        mRadiusLeftBottom = radiusLeftBottom;
        mRadiusRightBottom = radiusRightBottom;
        rids = new float[]{mRadiusLeftTop, mRadiusLeftTop, mRadiusRightTop, mRadiusRightTop, mRadiusRightBottom,
                mRadiusRightBottom, mRadiusLeftBottom, mRadiusLeftBottom};
        postInvalidate();
    }
}
