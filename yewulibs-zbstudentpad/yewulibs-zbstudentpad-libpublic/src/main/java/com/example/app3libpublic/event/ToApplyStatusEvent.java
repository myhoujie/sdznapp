package com.example.app3libpublic.event;

public class ToApplyStatusEvent {
    private boolean status;

    public ToApplyStatusEvent(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
