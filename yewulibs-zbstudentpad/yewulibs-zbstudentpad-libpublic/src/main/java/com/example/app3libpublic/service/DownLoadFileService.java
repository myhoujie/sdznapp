package com.example.app3libpublic.service;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.example.app3libpublic.R;
import com.example.app3libpublic.utils.CacheUtils;
import com.example.app3libvariants.network.download.DownFileThread;
import com.example.app3libvariants.network.download.DownLoadNotification;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.HandlerUtils;

import java.io.File;
import java.util.UUID;

/**
 * 描述：
 * -文件下载
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class DownLoadFileService extends Service implements HandlerUtils.OnReceiveMessageListener {
    private static final int DOWNLOAD_COMPLETE = -2;
    private static final int DOWNLOAD_FAIL = -1;
    public static final String INTENT_SAVE_NAME = "service.intent.save_name";
    public static final String INTENT_DOWNLOAD_URL = "service.intent.download_url";
    private String downLoadUrl;
    private String fileName;
    private static final int NOTIFICATION_ID = UUID.randomUUID().hashCode();
    private DownLoadNotification downLoadNotification;
    private DownFileThread downFileThread;  //自定义文件下载线程

    private Handler updateHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        updateHandler = new HandlerUtils.HandlerHolder(this);
    }

    @Override
    public void onDestroy() {
        if (downFileThread != null)
            downFileThread.interuptThread();
        stopSelf();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.downLoadUrl = intent.getStringExtra(INTENT_DOWNLOAD_URL);
        fileName = intent.getStringExtra(INTENT_SAVE_NAME);
        if (TextUtils.isEmpty(fileName)) {
            fileName = getString(R.string.app_name) + ".apk";
        }
        startDownload();

        return super.onStartCommand(intent, flags, startId);
    }

    File targetFile;

    private void startDownload() {
        downLoadNotification = new DownLoadNotification(this, NOTIFICATION_ID);
        downLoadNotification.showDefaultNotification(R.mipmap.notify_icon, R.mipmap.ic_launcher, getString(R.string.app_name), "正在下载");
        targetFile = new File(CacheUtils.getDownloadCache(), this.fileName);
        FileUtils.createOrExistsFile(targetFile);
        //开启一个新的线程下载，如果使用Service同步下载，会导致ANR问题，Service本身也会阻塞
        downFileThread = new DownFileThread(updateHandler, downLoadUrl, targetFile);
        new Thread(downFileThread).start();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    @Override
    public void handlerMessage(Message msg) {
        switch (msg.what) {
            case DOWNLOAD_COMPLETE:
                //点击安装PendingIntent
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    installIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Uri uri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", targetFile);
                    installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                } else {
                    Uri uri = Uri.fromFile(downFileThread.getApkFile());
                    installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                    installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(installIntent);
                downLoadNotification.changeNotificationText("下载完成，请点击安装！");
                downLoadNotification.changeProgressStatus(DOWNLOAD_COMPLETE);
                //停止服务
                downLoadNotification.removeNotification();
                stopSelf();
                break;
            case DOWNLOAD_FAIL:
                //下载失败
                downLoadNotification.changeProgressStatus(DOWNLOAD_FAIL);
                downLoadNotification.changeNotificationText("文件下载失败！");
                FileUtils.deleteFile(new File(CacheUtils.getDownloadCache(), this.fileName));
                stopSelf();
                break;
            default:  //下载中
                Log.i("service", "default" + msg.what);
                downLoadNotification.changeNotificationText(msg.what + "%");
                downLoadNotification.changeProgressStatus(msg.what);
        }
    }
}
