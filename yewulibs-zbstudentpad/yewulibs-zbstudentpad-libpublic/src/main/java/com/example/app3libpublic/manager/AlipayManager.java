package com.example.app3libpublic.manager;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.example.app3libpublic.event.MineCourseEvent;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.net.alipay.sdk.PayResult;
import com.sdzn.core.utils.HandlerUtils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * 描述：
 * - 支付宝支付管理类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class AlipayManager implements HandlerUtils.OnReceiveMessageListener {
    private Context context;
    private static final int SDK_PAY_FLAG = 1;
    private Handler mHandler;


    public AlipayManager(Context context) {
        this.context = context;
        mHandler = new HandlerUtils.HandlerHolder(this);
    }

    /**
     * 获取服务器对订单信息进行签名后的信息
     */
    public void doStartALiPayPlugin(String orderStr) {
        try {
            startPay(orderStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据签名后的订单信息发起支付
     *
     * @param payIno 订单签名信息
     * @throws Exception
     */
    private void startPay(final String payIno) throws Exception {
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask((Activity) context);
                // 调用支付接口，获取支付结果
                Map<String, String> result = alipay.payV2(payIno, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @Override
    public void handlerMessage(Message msg) {
        switch (msg.what) {
            case SDK_PAY_FLAG: {
                PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                /**
                 对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                 */
                String resultInfo = payResult.getResult();// 同步返回需要验证的信息

                String resultStatus = payResult.getResultStatus();
                // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                if ("9000".equals(resultStatus)) {
                    EventBus.getDefault().post(new OrderPayEvent(true));
                    EventBus.getDefault().post(new MineCourseEvent(true));
                } else {
                    payFailure(resultStatus);
                    EventBus.getDefault().post(new OrderPayEvent(false));
                }
                break;
            }
            default:
                break;
        }
    }


    private void payFailure(String resultStatus) {
        // 判断resultStatus 为非"9000"则代表可能支付失败
        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
        if ("8000".equals(resultStatus)) {
            ToastUtils.showShort("支付结果确认中");

        } else {
            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
            ToastUtils.showShort("支付失败");

        }
    }


}
