package com.example.app3libpublic.adtivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.R;
import com.example.app3libpublic.adapter.SpellingTitleAdapter;
import com.example.app3libpublic.adtivity.presenter.CourseTopicPresenter;
import com.example.app3libpublic.adtivity.view.CourseTopicView;
import com.example.app3libpublic.fragment.CourseTopicContentFragment;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libpublic.widget.pager.PagerSlidingTabStrip;
import com.example.app3libvariants.bean.SubjectBean;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 专题课程
 */

public class CourseTopicActivity extends BaseMVPActivity<CourseTopicView, CourseTopicPresenter>
        implements CourseTopicView {
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    ViewPager viewPager;
    TitleBar titleBar;

    private SpellingTitleAdapter spellingTitleAdapter;
    private List<Fragment> spellListFragment = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_topic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        mPagerSlidingTabStrip = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.vp_pager);
        titleBar = findViewById(R.id.title_bar);
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
//                    IntentController.toLogin(mContext);
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                    startActivity(intent);
                    return;
                }
//                IntentController.toShoppingCart(mContext);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(intent);
            }
        });
        loadData();
    }

    @Override
    protected CourseTopicPresenter createPresenter() {
        return new CourseTopicPresenter();
    }

    private void loadData() {
        spellingTitleAdapter = new SpellingTitleAdapter(getSupportFragmentManager());
        mPresenter.getTopicTitle();
    }


    @Override
    public void onTopicSuccess(List<SubjectBean> subjectList) {
        this.listTitle.clear();
        this.spellListFragment.clear();
        spellListFragment.add(CourseTopicContentFragment.newInstance("-1"));
        listTitle.add("全部");

        for (int i = 0; i < subjectList.size(); i++) {
            spellListFragment.add(CourseTopicContentFragment.newInstance(String.valueOf(subjectList.get(i).getSubjectId())));
            listTitle.add(subjectList.get(i).getSubjectName());
        }
        spellingTitleAdapter.setmDatas(listTitle, spellListFragment);
        viewPager.setAdapter(spellingTitleAdapter);
        viewPager.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(0);

        mPagerSlidingTabStrip.clearConfigSet();
    }

    @Override
    public void onTopicFailed(String msg) {

    }
}
