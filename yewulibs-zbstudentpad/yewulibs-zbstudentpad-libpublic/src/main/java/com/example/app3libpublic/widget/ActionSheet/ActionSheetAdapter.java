package com.example.app3libpublic.widget.ActionSheet;

import android.content.Context;
import android.graphics.Color;

import com.example.app3libpublic.R;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/14
 */
public class ActionSheetAdapter extends BaseRcvAdapter<SheetItem> {

    public ActionSheetAdapter(Context context, List<SheetItem> mList) {
        super(context, R.layout.item_action_sheet, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, SheetItem sheetItem) {
        holder.setText(R.id.tv_item, sheetItem.getName());
        if (sheetItem.getColor() != null) {
            holder.setTextColor(R.id.tv_item, Color.parseColor(sheetItem.getColor().getName()));
        }
    }
}
