package com.example.app3libpublic.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.IdRes;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;

import com.example.app3libpublic.R;
import com.sdzn.core.utils.LogUtils;

import java.util.HashSet;

/**
 * 子控件可拖拽的布局
 *
 * @author Reisen at 2017-11-22
 */

public class VDRelativeLayout extends RelativeLayout {
    private ViewDragHelper mHelper;
    private float mScale;//指定本控件宽高比, 该值为高/宽
    private boolean isDrag = true;//子控件是否可拖动 default: true
    private boolean isAdsorb;//是否靠边吸附 default: false
    private int oldOrientation = Configuration.ORIENTATION_PORTRAIT;
    private HashSet<Integer> mSet = new HashSet<>(4);
    private SparseArray<Point> mViewsPoint = new SparseArray<>(4);

    public VDRelativeLayout(Context context) {
        this(context, null);
    }

    public VDRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VDRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public VDRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VDRelativeLayout);
            isDrag = typedArray.getBoolean(R.styleable.VDRelativeLayout_isDrag, true);
            isAdsorb = typedArray.getBoolean(R.styleable.VDRelativeLayout_isAdsorb, false);
            mScale = typedArray.getFloat(R.styleable.VDRelativeLayout_scale, 0);
            typedArray.recycle();
        }
        if (!isDrag) {
            return;
        }
        initViewDragHelper();
    }

    public void setScale(float scale) {
        mScale = scale;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mScale != 0) {
            int widthSize = MeasureSpec.getSize(widthMeasureSpec);
            int heightSize = (int) (mScale * widthSize);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isDrag ? mHelper.shouldInterceptTouchEvent(ev) : super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isDrag) {
            mHelper.processTouchEvent(event);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void computeScroll() {
        if (mHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        int orientation = this.getResources().getConfiguration().orientation;
        if (this.getResources().getConfiguration().orientation != oldOrientation) {
            oldOrientation = orientation;
        }
//        View id1 = findViewById(R.id.img_switcher);
//        if (id1 != null) {
//            int left = id1.getLeft();
//            int top = id1.getTop();
//            int right = id1.getRight();
//            int bottom = id1.getBottom();
//            int visibility = id1.getVisibility();
//            LogUtils.i("left = " + left + "\r\ntop = " + top + "\r\nright = " + right + "\r\nbottom = " + bottom + "\r\nvisibility = " + visibility);
//        }
        a:
        for (Integer id : mSet) {
            int count = getChildCount();
            for (int i = 0; i < count; i++) {
                View view = getChildAt(i);
                if (view.getId() != id) {
                    continue;
                }
                Point point = mViewsPoint.get(id);
                if (point == null) {
                    continue a;
                }
                if (changed) {
                    Point adsorb = adsorb(view);
                    if (adsorb != null) {
                        point.set(adsorb.x, adsorb.y);
                    }
                    view.bringToFront();
                }
                view.layout(point.x, point.y, point.x + view.getMeasuredWidth(), point.y + view.getMeasuredHeight());
            }
        }
    }

    private void initViewDragHelper() {
        mHelper = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                for (Integer childId : mSet) {
                    if (child.getId() == childId) {
                        child.bringToFront();
                        LogUtils.w("拖动 ==>" + child);
                        return true;
                    }
                }
                LogUtils.w("不拖动 ==>" + child);
                return false;
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                if (left + child.getMeasuredWidth() >= getMeasuredWidth()) {
                    int i = getMeasuredWidth() - child.getMeasuredWidth();
                    LogUtils.w("clampViewPositionHorizontal ==> " + i);
                    return i;
                }
                int max = Math.max(left, 0);
                LogUtils.w("clampViewPositionHorizontal ==> " + max);
                return max;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                if (child.getMeasuredHeight() + top > getMeasuredHeight()) {
                    int i = getMeasuredHeight() - child.getMeasuredHeight();
                    LogUtils.w("clampViewPositionVertical ==> " + i);
                    return i;
                }
                int max = Math.max(top, 0);
                LogUtils.w("clampViewPositionVertical ==> " + max);
                return max;
            }

            @Override
            public int getViewHorizontalDragRange(View child) {
                for (Integer childId : mSet) {
                    int id = child.getId();
                    if (id - childId == 0) {
                        int i = getMeasuredWidth() - child.getMeasuredWidth();
                        LogUtils.w("getViewHorizontalDragRange ==> " + i);
                        return i;
                    }
                }
                LogUtils.w("getViewHorizontalDragRange ==> 0");
                return 0;
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                for (Integer childId : mSet) {
                    if (child.getId() == childId) {
                        return getMeasuredHeight() - child.getMeasuredHeight();
                    }
                }
                return 0;
            }

            @Override
            public void onViewReleased(final View releasedChild, float xvel, float yvel) {
                resetPoint(releasedChild.getId(), releasedChild.getLeft(), releasedChild.getTop());
                //动画吸附
                if (!isAdsorb) {
                    return;
                }
                Point end = adsorb(releasedChild);
                if (end == null) {
                    return;
                }
                if (mHelper.settleCapturedViewAt(end.x, end.y)) {
                    isDrag = false;
                    ViewCompat.postInvalidateOnAnimation(VDRelativeLayout.this);
                }
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                resetPoint(changedView.getId(), left, top);
                if (left <= getLeft() || top <= getTop() ||
                        (left + changedView.getWidth()) >= getWidth() ||
                        (top + changedView.getHeight()) >= getHeight()) {
                    isDrag = true;
                }
            }

            private void resetPoint(int viewId, int x, int y) {
                Point point = mViewsPoint.get(viewId);
                if (point == null) {
                    mViewsPoint.put(viewId, new Point(x, y));
                } else {
                    point.set(x, y);
                }
            }

        });
        mHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_ALL);
    }

    /**
     * 吸附至屏幕位置
     *
     * @return 要吸附到的坐标, null表示不吸附
     */
    private Point adsorb(View releasedChild) {
        int width = getMeasuredWidth() - releasedChild.getMeasuredWidth();
        int height = getMeasuredHeight() - releasedChild.getMeasuredHeight();
        //吸附条件判断 : 本控件去掉子控件宽高后的边缘 1/4处吸附, 否则不吸附
        boolean toTop = releasedChild.getTop() < height >> 2;
        boolean toBottom = releasedChild.getTop() > height - (height >> 2);
        boolean toLeft = releasedChild.getLeft() < width >> 2;
        boolean toRight = releasedChild.getLeft() > width - (width >> 2);
        if (!toTop && !toBottom && !toLeft && !toRight) {//中间不吸附
            return null;
        }
        if (toTop && toLeft) {//左上
            return new Point();
        } else if (toTop && toRight) {//右上
            return new Point(width, 0);
        } else if (toBottom && toLeft) {//左下
            return new Point(0, height);
        } else if (toBottom && toRight) {//右下
            return new Point(width, height);
        } else if (toTop) {//上中
            return new Point(releasedChild.getLeft(), 0);
        } else if (toBottom) {//右中
            return new Point(releasedChild.getLeft(), height);
        } else if (toLeft) {//左中
            return new Point(0, releasedChild.getTop());
        } else {//下中
            return new Point(width, releasedChild.getTop());
        }
    }

    /**
     * 子控件是否可拖动
     *
     * @param drag 默认是可拖动的
     */
    public void setDrag(boolean drag) {
        if (drag && mHelper == null) {
            initViewDragHelper();
        }
        isDrag = drag;
    }

    public boolean isDrag() {
        return isDrag;
    }

    /**
     * 拖动控件是否贴边吸附
     *
     * @param adsorb 默认false子控件不吸附边缘
     */
    public void setAdsorb(boolean adsorb) {
        isAdsorb = adsorb;
    }

    public boolean isAdsorb() {
        return isAdsorb;
    }

    public void addDragView(@IdRes int viewId) {
        mSet.add(viewId);
    }

    public boolean removeDragView(@IdRes int viewId) {
        return mSet.remove(viewId);
    }

    public HashSet<Integer> getDragViews() {
        return mSet;
    }

}
