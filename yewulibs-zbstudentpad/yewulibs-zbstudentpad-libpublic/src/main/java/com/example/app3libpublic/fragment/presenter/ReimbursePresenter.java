package com.example.app3libpublic.fragment.presenter;

import android.text.TextUtils;

import com.example.app3libpublic.R;
import com.example.app3libpublic.fragment.view.ReimburseView;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/3
 */

public class ReimbursePresenter extends BasePresenter<ReimburseView> {
    public void dropOut(int orderId, String reason) {
        if (reason == null) {
            reason = "";
        }
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(orderId));
        requestParams.put("refundDesc", reason);
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .dropCourse(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().dropSuccess(o);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().dropError(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);

    }

//    public void dropOut(int orderId, String reason) {
//        if (reason == null) {
//            reason = "";
//        }
//        Subscription subscription = RestApi.getInstance()
//                .createNew(AccountService.class)
//                .dropCourse(orderId, reason)
//                .compose(TransformUtils.<ResultBean<ReimburseBean>>defaultSchedulers())
//                .map(new ResponseFunc<ReimburseBean>())
//                .subscribe(new MProgressSubscriber<ReimburseBean>(new SubscriberOnNextListener<ReimburseBean>() {
//
//                    @Override
//                    public void onNext(ReimburseBean o) {
//                        getView().dropSuccess(o);
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        String msg = mActivity.getString(R.string.request_failure_try_again);
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
//                        getView().dropError(msg);
//                    }
//                }, mActivity, true, "请求中..."));
//        addSubscribe(subscription);
//
//    }
}
