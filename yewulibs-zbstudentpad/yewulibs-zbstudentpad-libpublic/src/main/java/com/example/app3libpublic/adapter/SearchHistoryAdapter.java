package com.example.app3libpublic.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.app3libpublic.R;
import com.example.app3libpublic.widget.flowlayout.FlowLayout;
import com.example.app3libpublic.widget.flowlayout.TagAdapter;

import java.util.List;


/**
 * 描述：
 * - 搜索历史adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SearchHistoryAdapter<T> extends TagAdapter<T> {


    public SearchHistoryAdapter(Context context, List<T> datas) {
        super(context, datas);
    }

    public SearchHistoryAdapter(Context context, T[] datas) {
        super(context, datas);
    }

    @Override
    public View getView(FlowLayout parent, int position, T t) {
        TextView tvHistory = (TextView) mLayoutInflater.inflate(R.layout.item_search_history, parent, false);
        String searchHistory = (String) t;
        tvHistory.setText(searchHistory);
        return tvHistory;
    }

}
