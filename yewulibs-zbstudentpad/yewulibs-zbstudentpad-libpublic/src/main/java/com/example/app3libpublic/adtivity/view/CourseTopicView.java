package com.example.app3libpublic.adtivity.view;

import com.example.app3libvariants.bean.SubjectBean;
import com.sdzn.core.base.BaseView;

import java.util.List;

/**
 * 专题课
 */
public interface CourseTopicView extends BaseView {
    void onTopicSuccess(List<SubjectBean> subjectList);

    void onTopicFailed(String msg);

}
