package com.example.app3libpublic.fragment.presenter;

import com.example.app3libpublic.fragment.view.SpellingContentView;
import com.example.app3libvariants.bean.CourseListBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewSchoolFunc;
import com.google.gson.Gson;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

public class CourseTopicContentPresenter extends BasePresenter<SpellingContentView> {

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicCourse(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListBean>() {
                    @Override
                    public void onNext(CourseListBean courses) {
                        if (courses != null) {
                            getView().getDataCourse(courses.getList().getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().onFailed("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
