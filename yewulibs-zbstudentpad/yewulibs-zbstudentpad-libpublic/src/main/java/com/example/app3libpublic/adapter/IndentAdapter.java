package com.example.app3libpublic.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3libpublic.R;
import com.example.app3libpublic.listener.WechatOrAlipayListener;
import com.example.app3libpublic.manager.constant.PayType;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libvariants.bean.IndentBean;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.core.utils.ToastUtils;

import java.util.List;

import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_CANCEL;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_CHECKING;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_INIT;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_REFUND;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_REFUSE;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_SUCCESS;
import static com.example.app3libpublic.manager.constant.IndentCons.INDENT_WAITREFUND;

/**
 * 描述：订单
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/20
 */

public class IndentAdapter extends SectionedRecyclerViewAdapter {
    private OnItemViewListener listener;
    private OnBuyListener buyListener;
    private OnCancelListener cancelListener;
    private OnCanceApplylListener cancelApplyListener;
    private String pay_state = "";//订单的支付状态
    private SparseBooleanArray mBooleanMap;//记录下哪个section是被打开的

    public IndentAdapter(Context context, List mList) {
        super(context, R.layout.item_indent, mList);
        mBooleanMap = new SparseBooleanArray();
    }

    public SparseBooleanArray getmBooleanMap() {
        return this.mBooleanMap;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, Object o) {

    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }


    @Override
    protected int getSectionCount() {
        return mList.size();
    }

    @Override
    protected int getItemCountForSection(int section) {
        IndentBean indentBean = (IndentBean) mList.get(section);
        return indentBean.getTrxorderDetailList().size();
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return true;
    }

    @Override
    protected BaseViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        View headView = mLayoutInflater.inflate(R.layout.item_indent_header, parent, false);
        return new BaseViewHolder(context, headView);
    }

    @Override
    protected BaseViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        View footerView = mLayoutInflater.inflate(R.layout.item_indent_footer, parent, false);
        return new BaseViewHolder(context, footerView);
    }

    @Override
    protected BaseViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater.inflate(R.layout.item_indent, parent, false);
        return new BaseViewHolder(context, itemView);
    }

    @Override
    protected void onBindSectionHeaderViewHolder(BaseViewHolder holder, int section) {
        if (0 == section) {
            holder.setVisible(R.id.view_line, true);
        } else {
            holder.setVisible(R.id.view_line, false);
        }
        final IndentBean indentBean = (IndentBean) mList.get(section);
        holder.setText(R.id.tv_order_number_value, indentBean.getOrderNo());
        holder.setVisible(R.id.tv_indentpay_type, true);
        if (INDENT_SUCCESS.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "已付款";
        } else if (INDENT_INIT.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "待付款";
        } else if (INDENT_CANCEL.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "已取消";
        } else if (INDENT_REFUND.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "已退款";
        } else if (INDENT_CHECKING.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "退课待审核";
        } else if (INDENT_REFUSE.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "退课审核未通过";
        } else if (INDENT_WAITREFUND.equalsIgnoreCase(indentBean.getStates())) {
            pay_state = "待退款";
        }
        holder.setText(R.id.tv_indentpay_type, pay_state);
    }

    @Override
    protected void onBindSectionFooterViewHolder(final BaseViewHolder holder, final int section) {
        final IndentBean indentBean = (IndentBean) mList.get(section);
        holder.setText(R.id.text1, "共" + indentBean.getTrxorderDetailList().size() + "件  合计:");
        holder.setText(R.id.tv_actual_payment, "¥" + indentBean.getCashAmount());
        //判断当前订单的类型
        if (INDENT_INIT.equalsIgnoreCase(indentBean.getStates())) {
            holder.setVisible(R.id.rl_states, true);
            holder.setVisible(R.id.tv_payment, true);
            holder.setVisible(R.id.tv_cancel, true);
            holder.setText(R.id.tv_payment, "立即付款");
        } else if (INDENT_CHECKING.equalsIgnoreCase(indentBean.getStates())) {
            holder.setVisible(R.id.rl_states, true);
            holder.setVisible(R.id.tv_payment, true);
            holder.setVisible(R.id.tv_cancel, false);
            holder.setText(R.id.tv_payment, "取消申请");
        } else {
            holder.setVisible(R.id.rl_states, false);
        }


//        //判断展开的是当前哪一个item
//        boolean isOpen = mBooleanMap.get(section);
//        if (isOpen) {
//            holder.setVisible(R.id.ll_show_details, true);
//            setImgviewtoText((TextView) holder.getView(R.id.tv_video_details), R.mipmap.ic_arrow_up);
//        } else {
//            holder.setVisible(R.id.ll_show_details, false);
//            setImgviewtoText((TextView) holder.getView(R.id.tv_video_details), R.mipmap.ic_arrow_down);
//        }
//
//        holder.getView(R.id.tv_video_details).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isOpen = mBooleanMap.get(section);
//                mBooleanMap.put(section, !isOpen);
//                notifyDataSetChanged();
//            }
//        });
        holder.getView(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //支付
                DialogUtil.showDialog((Activity) context, "确定取消订单吗？", true, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cancelListener.cancelIndent(indentBean);
                    }
                });
            }
        });
        holder.getView(R.id.tv_payment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (INDENT_INIT.equalsIgnoreCase(indentBean.getStates())) {
                    //支付


                    DialogUtil.showSelectPaymentDialog((Activity)context, new WechatOrAlipayListener() {
                        @Override
                        public void selectWx() {
//                            ToastUtils.showShort("wx");
                            buyListener.buyClick(indentBean, PayType.WXPAY);
                        }

                        @Override
                        public void selectAlipay() {
//                            ToastUtils.showShort("alipay");
                            buyListener.buyClick(indentBean, PayType.ALIPAY);
                        }
                    });


                } else if (INDENT_CHECKING.equalsIgnoreCase(indentBean.getStates())) {
                    //退课
                    DialogUtil.showDialog((Activity) context, "确定撤销退课申请吗？", true, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            cancelApplyListener.cancelCourse(indentBean);

                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onBindItemViewHolder(BaseViewHolder holder, int section, final int position) {
        final IndentBean indentBean = (IndentBean) mList.get(section);
        final IndentBean.TrxorderDetailListBean trxorderDetailListBean = indentBean.getTrxorderDetailList().get(position);
        holder.setText(R.id.tv_class, trxorderDetailListBean.getCourseName());
        holder.setText(R.id.tv_course_video_price, "¥" + trxorderDetailListBean.getCurrentPirce());
        holder.setImageView(R.id.img_video, "" + trxorderDetailListBean.getCourseImgUrl());


        if ("PACKAGE".equalsIgnoreCase(trxorderDetailListBean.getSellType())) {
            if (trxorderDetailListBean.getPackageType() == 1) {
                holder.setText(R.id.tv_course_video_type, "直");
                holder.setTextColorRes(R.id.tv_course_video_type, R.color.color_FDB850);
            } else {
                holder.setText(R.id.tv_course_video_type, "点");
                holder.setTextColorRes(R.id.tv_course_video_type, R.color.color_2DD4CA);
            }
        } else {
            if ("LIVE".equalsIgnoreCase(trxorderDetailListBean.getSellType())) {
                holder.setText(R.id.tv_course_video_type, "直");
                holder.setTextColorRes(R.id.tv_course_video_type, R.color.color_FDB850);
            } else {
                holder.setText(R.id.tv_course_video_type, "点");
                holder.setTextColorRes(R.id.tv_course_video_type, R.color.color_2DD4CA);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击监听跳转到单个课程的详情界面
                listener.onItemClick(indentBean, trxorderDetailListBean, position);
            }
        });

    }


    public interface OnItemViewListener {
        void onItemClick(IndentBean indentBean, IndentBean.TrxorderDetailListBean trxorderDetailListBean, int position);

        void refundClick(IndentBean indentBean);
    }

    public interface OnBuyListener {
        void buyClick(IndentBean indentBean,String payType);
    }

    public interface OnCancelListener {
        void cancelIndent(IndentBean indentBean);
    }
    public interface OnCanceApplylListener {
        void cancelCourse(IndentBean indentBean);
    }

    public void setOnItemViewListener(OnItemViewListener listener) {
        this.listener = listener;
    }

    //支付
    public void setOnBuyListener(OnBuyListener buyListener) {
        this.buyListener = buyListener;
    }
    //订单取消
    public void setOnCancelListener(OnCancelListener cancelListener) {
        this.cancelListener = cancelListener;
    }
    //退课取消申请
    public void setOnCancelApplyListener(OnCanceApplylListener cancelApplyListener) {
        this.cancelApplyListener = cancelApplyListener;
    }

}

