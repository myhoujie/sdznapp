package com.example.app3libpublic.manager.constant;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 描述：
 * - 答题卡试题类型相关常量
 * 创建人：baoshengxiang
 * 创建时间：2017/9/27
 */
public class QuestionCons {
    public static class Type {
        public static final int SINGLE = 0;//单选
        public static final int MULTIPLE = 1;//多选
        public static final int JUDGE = 2;//判断题

        @IntDef({SINGLE, MULTIPLE, JUDGE})
        @Retention(RetentionPolicy.SOURCE)
        public @interface QuestionType {
        }
    }
}
