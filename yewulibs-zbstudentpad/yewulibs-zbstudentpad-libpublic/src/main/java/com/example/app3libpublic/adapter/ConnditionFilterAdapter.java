package com.example.app3libpublic.adapter;

import android.content.Context;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.example.app3libpublic.R;
import com.example.app3libvariants.bean.GradeBean;
import com.example.app3libvariants.bean.LiveStatusBean;
import com.example.app3libvariants.bean.SectionBean;
import com.example.app3libvariants.bean.SortBean;
import com.example.app3libvariants.bean.SubjectBean;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;

import java.util.List;

/**
 * 描述：
 * - 直播、点播条件筛选adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/7
 */
public class ConnditionFilterAdapter extends BaseRcvAdapter {

    private int selectPos;

    public ConnditionFilterAdapter(Context context, List mList) {
        super(context, R.layout.item_condition_filter, mList);
    }

    @Override
    public void convert(final BaseViewHolder holder, final int position, Object o) {
        String filterName = "";
        if (o instanceof SubjectBean) {
            SubjectBean subjectBean = (SubjectBean) o;
            filterName = subjectBean.getSubjectName();
        } else if (o instanceof SectionBean) {
            SectionBean sectionBean = (SectionBean) o;
            filterName = sectionBean.getSectionName();
        } else if (o instanceof SortBean) {
            SortBean sortBean = (SortBean) o;
            filterName = sortBean.getSortName();
        } else if (o instanceof LiveStatusBean) {
            LiveStatusBean liveStatusBean = (LiveStatusBean) o;
            filterName = liveStatusBean.getStatusName();
        } else if (o instanceof GradeBean) {
            GradeBean gradeBean = (GradeBean) o;
            filterName = gradeBean.getGradeName();
        }

        TextView tvCondition = holder.getView(R.id.tv_condition);
        if (selectPos == position) {
            holder.itemView.setBackgroundResource(R.color.gray_ea);
            holder.setVisible(R.id.iv_selected, true);
            tvCondition.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.colorAccent, null));
        } else {
            tvCondition.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.textMinor, null));
            holder.itemView.setBackgroundResource(R.color.background);
            holder.setVisible(R.id.iv_selected, false);
        }
        holder.setText(R.id.tv_condition, filterName);
    }

    public void setSelectPos(int selectPos) {
        this.selectPos = selectPos;
        notifyDataSetChanged();
    }

    public int getSelectPos() {
        return selectPos;
    }
}
