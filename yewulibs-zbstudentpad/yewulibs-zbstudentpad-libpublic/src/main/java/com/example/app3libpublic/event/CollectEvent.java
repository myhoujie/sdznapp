package com.example.app3libpublic.event;

/**
 * 收藏处
 */
public class CollectEvent {
    boolean isCollect;

    public CollectEvent(boolean isCollect) {
        this.isCollect = isCollect;
    }

    public boolean isCollect() {
        return isCollect;
    }

}
