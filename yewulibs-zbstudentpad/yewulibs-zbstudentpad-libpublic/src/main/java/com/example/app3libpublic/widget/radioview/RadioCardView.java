package com.example.app3libpublic.widget.radioview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;

import androidx.cardview.widget.CardView;

/**
 * 因为普通控件没有checked状态,<br/>
 * 所以子控件中用selected代替checked
 *
 * @author Reisen at 2017-12-05
 */

public class RadioCardView extends CardView implements RadioSelector {
    private boolean isChecked;

    public RadioCardView(Context context) {
        this(context, null);
    }

    public RadioCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RadioCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setClickable(true);
        setOnClickListener(this);
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public boolean setChecked(boolean isChecked) {
        return check(isChecked);
    }

    @Override
    public void onClick(View v) {
        if (isChecked) {
            return;
        }
        if (!setChecked(true)) {
            return;
        }
        ViewParent parent = getParent();
        if (!(parent instanceof RadioLayout)) {
            return;
        }
        RadioLayout layout = (RadioLayout) parent;
        layout.checkedRadioSelector(getId());
    }

    @Override
    public void setSelected(boolean selected) {
        check(selected);
    }

    private boolean check(boolean check) {
        if (this.isChecked == check) {
            return false;
        }
        this.isChecked = check;
        setCardElevation(isChecked ? 3 : 1);
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setSelected(isChecked);
        }
        return true;
    }
}
