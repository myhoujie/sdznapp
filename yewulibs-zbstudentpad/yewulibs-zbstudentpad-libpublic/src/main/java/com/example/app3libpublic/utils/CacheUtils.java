package com.example.app3libpublic.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.SDCardUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.example.app3libpublic.manager.Config;

import java.io.File;

/**
 * 描述：
 * - app缓存管理
 * 创建人：baoshengxiang
 * 创建时间：2017/7/26
 */
public class CacheUtils {
    /**
     * 获取应用程序缓存根文件
     *
     * @return
     */
    public static File getAppCache() {
        File cacheFile;
        if (SDCardUtils.isSDCardEnable()) {
            cacheFile = new File(Environment.getExternalStorageDirectory(), Config.ROOT_CACHE);
        } else {
            cacheFile = new File(Environment.getDataDirectory(), Config.ROOT_CACHE);
        }

        FileUtils.createOrExistsDir(cacheFile);
        return cacheFile;
    }

    /**
     * glide图片缓存目录
     *
     * @return
     */
    public static File getImageCache() {
        File imageCache = new File(App2.get().getExternalCacheDir(), Config.IMAGE_CACHE);
        FileUtils.createOrExistsDir(imageCache);
        return imageCache;
    }


    /**
     * glide图片缓存目录
     *
     * @return
     */
    public static File getAvatarCache() {
        File imageCache = new File(getAppCache(), Config.AVATAR_CACHE);
        FileUtils.createOrExistsDir(imageCache);
        return imageCache;
    }

    /**
     * 点播视频截图目录
     *
     * @return
     */
    public static File getVideoScreenshotCache() {
        File screenshotCache = new File(getAppCache(), Config.SCREENSHOT_FILE_DIR);
        FileUtils.createOrExistsDir(screenshotCache);
        return screenshotCache;
    }

    /**
     * 点播异常捕获目录
     *
     * @return
     */
    public static File getCrashCache() {
        File crashCache = new File(getAppCache(), Config.CRASH_CACHE);
        FileUtils.createOrExistsDir(crashCache);
        return crashCache;
    }

    /**
     * 点播视频截图目录
     *
     * @return
     */
    public static File getDownloadCache() {
        File downloadCache = new File(getAppCache(), Config.APP_DOWNLOAD);
        FileUtils.createOrExistsDir(downloadCache);
        return downloadCache;
    }

    /**
     * nim缓存目录
     *
     * @return
     */
    public static File getNimCache() {
        File downloadCache = new File(getAppCache(), Config.NIM_FILE_DIR);
        FileUtils.createOrExistsDir(downloadCache);
        return downloadCache;
    }


    public static void sendUpdateBroadcast(File file) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(file);
        intent.setData(uri);
        App2.get().sendBroadcast(intent);//这个广播的目的就是更新图库，发了这个广播进入相册就可以找到你保存的图片了！，记得要传你更新的file哦

    }


}
