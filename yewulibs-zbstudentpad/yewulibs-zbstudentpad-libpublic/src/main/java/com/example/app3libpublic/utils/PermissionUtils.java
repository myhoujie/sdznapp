package com.example.app3libpublic.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 描述：
 * - 请求权限管理
 */
public class PermissionUtils {
    public static final int REQUEST_PERMISSION_CODE = 0x99;
    private static OnHasGetPermissionListener mHasGetPermissionListener;
    private static final String TAG = "PermissionUtils";

    /**
     * 获取所需权限
     *
     * @param context               context
     * @param permissionArray       权限数组
     * @param getPermissionListener 回调
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static String[] getNeededPermission(Context context, OnHasGetPermissionListener getPermissionListener,
                                               String... permissionArray) {
        if (context == null || permissionArray == null || permissionArray.length == 0) {
            return new String[]{};
        }
        mHasGetPermissionListener = getPermissionListener;
        List<String> permissionList = new ArrayList<>();
        AppOpsManager appOpsManager = isMIUI() ? (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE) : null;
        for (String aPermissionArray : permissionArray) {
            if (isNeedAddPermission(context, appOpsManager, aPermissionArray)) {
                permissionList.add(aPermissionArray);
            }
        }
        if (!permissionList.isEmpty()) {
            String[] permissions = permissionList.toArray(new String[permissionList.size()]);
            ((Activity) context).requestPermissions(permissions, PermissionUtils.REQUEST_PERMISSION_CODE);
        } else {
            getPermissionListener.onSuccess();
        }
        return permissionList.toArray(new String[permissionList.size()]);
    }


    /**
     * fragment中获取所需权限
     *
     * @param fragment              fragment
     * @param permissionArray       权限数组
     * @param getPermissionListener 回调
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static String[] getNeededPermission(Fragment fragment, OnHasGetPermissionListener getPermissionListener,
                                               String... permissionArray) {
        if (fragment == null || permissionArray == null || permissionArray.length == 0) {
            return new String[]{};
        }
        mHasGetPermissionListener = getPermissionListener;
        List<String> permissionList = new ArrayList<>();
        AppOpsManager appOpsManager = isMIUI() ? (AppOpsManager) fragment.getActivity().getSystemService(Context.APP_OPS_SERVICE) : null;
        for (String aPermissionArray : permissionArray) {
            if (PermissionUtils.isNeedAddPermission(fragment.getActivity(),appOpsManager, aPermissionArray)) {
                permissionList.add(aPermissionArray);
            }
        }
        if (!permissionList.isEmpty()) {
            String[] permissions = permissionList.toArray(new String[permissionList.size()]);
            fragment.requestPermissions(permissions, PermissionUtils.REQUEST_PERMISSION_CODE);
        } else {
            getPermissionListener.onSuccess();
        }
        return permissionList.toArray(new String[permissionList.size()]);
    }

    /**
     * 检查权限是否已有
     * @return true=需要请求权限 false = 不需请求该权限
     */
    private static boolean isNeedAddPermission(Context context, @Nullable AppOpsManager appOpsManager, @NonNull String permission) {
        boolean needPermission = ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED;
        if (needPermission) {
            return true;
        }
        if (Build.VERSION.SDK_INT > 23 && appOpsManager != null) {
            needPermission = checkMiuiPermission(context, appOpsManager, permission);
        }
        return needPermission;
    }

    /**
     * 检查权限并显示dialog
     *
     * @param context      context
     * @param permissions  permissions
     * @param grantResults grantResults
     */
    public static void checkPermissionResult(Context context, String[] permissions, int[] grantResults) {
        if (context == null || permissions == null || grantResults == null) {
            return;
        }
        List<PermissionInfo> permissionInfos = new ArrayList<>();//Your  permission list
        if (grantResults.length <= 0) {
            mHasGetPermissionListener.onSuccess();
            return;
        }
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                PermissionInfo info = null;
                try {
                    info = context.getPackageManager().getPermissionInfo(permissions[i], 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                if (info != null) {
                    permissionInfos.add(info);
                }
            }
        }

        if (permissionInfos.isEmpty()) {//如果已有所有权限
            mHasGetPermissionListener.onSuccess();
        } else {
            mHasGetPermissionListener.onFail(permissionInfos);
            Log.d(TAG, "checkPermissionResult onFail");
        }
    }

    public interface OnHasGetPermissionListener {
        void onSuccess();

        void onFail(List<PermissionInfo> permissionInfos);
    }

    public static void showPermissionFailed(Context context, String title, String message,
                                            String positiveText, DialogInterface.OnClickListener positiveListener,
                                            String negativeText, DialogInterface.OnClickListener negativeListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title).setMessage(message).setCancelable(false)
                .setPositiveButton(positiveText, positiveListener)
                .setNegativeButton(negativeText, negativeListener).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /*================ 小米权限检查 ================*/

    //检测MIUI
    private static final String KEY_MIUI_VERSION_CODE = "ro.miui.ui.version.code";
    private static final String KEY_MIUI_VERSION_NAME = "ro.miui.ui.version.name";
    private static final String KEY_MIUI_INTERNAL_STORAGE = "ro.miui.internal.storage";

    /**
     * 检查手机是否是miui系统
     */
    public static boolean isMIUI() {
        String device = Build.MANUFACTURER;
        System.out.println("Build.MANUFACTURER = " + device);
        if (device.equals("Xiaomi")) {
            Properties prop = new Properties();
            try {
                prop.load(new FileInputStream(new File(Environment.getRootDirectory(), "build.prop")));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return prop.getProperty(KEY_MIUI_VERSION_CODE, null) != null
                    || prop.getProperty(KEY_MIUI_VERSION_NAME, null) != null
                    || prop.getProperty(KEY_MIUI_INTERNAL_STORAGE, null) != null;
        } else {
            return false;
        }
    }

    /**
     * 判断小米MIUI系统中授权管理中对应的权限授取
     *
     * @return true 需要授权 false 不需授权
     */
    @RequiresApi(Build.VERSION_CODES.M)
    private static boolean checkMiuiPermission(Context context, AppOpsManager appOpsManager, @NonNull String permission) {
        permission = AppOpsManager.permissionToOp(permission);
        int checkOp = appOpsManager.checkOp(permission, Binder.getCallingUid(), context.getPackageName());
        return checkOp == AppOpsManager.MODE_IGNORED;
    }

}
