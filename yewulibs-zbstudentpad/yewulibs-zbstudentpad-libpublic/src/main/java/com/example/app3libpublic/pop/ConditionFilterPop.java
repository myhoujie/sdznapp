package com.example.app3libpublic.pop;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3libpublic.R;
import com.example.app3libpublic.adapter.ConnditionFilterAdapter;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.fzx.student.libutils.app.App2;

import java.util.List;

/**
 * 描述：
 * - 直播、点播条件筛选pop
 * 创建人：baoshengxiang
 * 创建时间：2017/7/7
 */
public class ConditionFilterPop extends PopupWindow {
    private ConnditionFilterAdapter connditionFilterAdapter;
    private List mList;
    private Context mContext;
    private ConditionCallback conditionCallback;
    private View underView;
    private FrameLayout rl_root_view;

    public ConditionFilterPop(Context context, List mList) {
        super(context);
        this.mContext = context;
        this.mList = mList;
        initView();
    }

    public ConditionFilterPop(Context context, List mList, View underView) {
        super(context);
        this.mContext = context;
        this.mList = mList;
        this.underView = underView;
        initView();
    }

    private void initView() {
        // 一个自定义的布局，作为显示的内容
        View contentView = LayoutInflater.from(App2.get()).inflate(
                R.layout.pop_condition_filter, null);
        this.setContentView(contentView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setTouchable(true);
        this.setOutsideTouchable(true);
        // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));

        View view = contentView.findViewById(R.id.iv_triangle);
        rl_root_view = contentView.findViewById(R.id.rl_root_view);
        rl_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if (underView == null) {
            view.setVisibility(View.GONE);
        } else {
            int left = (underView.getWidth() >> 1) + underView.getLeft();
            ViewGroup.MarginLayoutParams margin = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
            margin.setMargins(left + 65, 0, 0, 0);
            view.setLayoutParams(new FrameLayout.LayoutParams(margin));
        }

        // 设置按钮的点击事件
        RecyclerView rvCondition = (RecyclerView) contentView.findViewById(R.id.rv_condition);
        rvCondition.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(mContext.getResources(), R.color.gray_ea, null), 1));
        rvCondition.setLayoutManager(new LinearLayoutManager(mContext));
        connditionFilterAdapter = new ConnditionFilterAdapter(mContext, mList);
        connditionFilterAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                conditionCallback.selectCondition(position);
                connditionFilterAdapter.setSelectPos(position);
                connditionFilterAdapter.notifyDataSetChanged();
                ConditionFilterPop.this.dismiss();
            }
        });
        rvCondition.setAdapter((RecyclerView.Adapter) connditionFilterAdapter);
        setAnimationStyle(R.style.AnimationAlpha);
    }

    public void setData(List datas) {
        this.mList = datas;
        connditionFilterAdapter.notifyDataSetChanged();
    }


    public void setConditionCallback(ConditionCallback conditionCallback) {
        this.conditionCallback = conditionCallback;
    }

    public interface ConditionCallback {
        void selectCondition(int position);
    }

    public int getSelectPos() {
        return connditionFilterAdapter.getSelectPos();
    }

    public void setSelectPos(int selectPos) {
        connditionFilterAdapter.setSelectPos(selectPos);
    }
}
