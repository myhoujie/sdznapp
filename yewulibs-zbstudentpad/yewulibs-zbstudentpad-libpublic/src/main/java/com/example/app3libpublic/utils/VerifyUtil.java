package com.example.app3libpublic.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述：验证格式是否正确
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/3/14
 */

public class VerifyUtil {
    /**
     * 判断邮箱是否合法
     *
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        if (null == email || "".equals(email)) return false;
        //Pattern p = Pattern.compile("\\w+@(\\w+.)+[a-z]{2,3}"); //简单匹配
        Pattern p = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");//复杂匹配
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * 判断手机号是否合法
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        /**
         * 移动号段正则表达式
         */
        String cmNum = "^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
        /**
         * 联通号段正则表达式
         */
        String cuNum = "^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
        /**
         * 电信号段正则表达式
         */
        String ctNum = "^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";

        /**
         * 1开头的
         */
        String num = "^1\\d{10}$";

        return Pattern.compile(cmNum).matcher(mobiles).matches()
                || Pattern.compile(cuNum).matcher(mobiles).matches()
                || Pattern.compile(ctNum).matcher(mobiles).matches()
                || Pattern.compile(num).matcher(mobiles).matches();
    }

    /**
     * 判断邮编
     *
     * @param paramString
     * @return
     */
    public static boolean isZipNO(String paramString) {
        String str = "^[1-9][0-9]{5}$";
        return Pattern.compile(str).matcher(paramString).matches();
    }
}
