package com.example.app3mykc.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.MineCourseBean;
import com.example.app3libvariants.bean.MineList;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public interface MineCourseView extends BaseView {
    void listTodyCourseSuccess(List<MineList> list);

    void listRecentlyCourseSuccess(List<MineList> list);

    void listCourseSuccess(List<MineList> list, int type);

    void listCourseEmpty();

    void listCourseError(String msg);

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void getReplayInfoSuccess(NewVideoInfo info);

}
