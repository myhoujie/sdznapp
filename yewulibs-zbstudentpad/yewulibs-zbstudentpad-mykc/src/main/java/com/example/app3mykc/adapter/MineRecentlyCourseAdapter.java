package com.example.app3mykc.adapter;

import android.content.Context;

import com.example.app3libvariants.bean.MineList;
import com.example.app3mykc.R;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;

import java.util.List;

/**
 * 描述：我的课程列表
 * -
 * type =1  过时
 */

public class MineRecentlyCourseAdapter extends BaseRcvAdapter<MineList> {
    private boolean isLiving;
    private static final String LIVE = "LIVE";
    private static final String COURSE = "COURSE";

    public MineRecentlyCourseAdapter(Context context, List<MineList> mList) {
        super(context, R.layout.item_mine_course, mList);
    }


    @Override
    public void convert(BaseViewHolder holder, int position, MineList mineCourseBean) {

        holder.setText(R.id.tv_class, mineCourseBean.getCourseName());
        holder.setImageView(R.id.img_video, "" + mineCourseBean.getLogo());


        if (mineCourseBean.getCourseType().equals(LIVE)) {
            holder.setText(R.id.tv_status, "直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
            isLiving = true;
            holder.setText(R.id.tv_video_type, "「直播」");
            holder.setText(R.id.tv_schedule, "学习进度：");
            holder.setTextColor(R.id.tv_video_type, context.getResources().getColor(R.color.red));
        } else {
            holder.setText(R.id.tv_status, "点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
            isLiving = false;
            holder.setText(R.id.tv_video_type, "「点播」");
            holder.setText(R.id.tv_schedule, "学习进度：");
            holder.setTextColor(R.id.tv_video_type, context.getResources().getColor(R.color.live_text_yellow));
        }
        int percent = (int) mineCourseBean.getProgress();
        holder.setText(R.id.tv_schedule_value, String.valueOf(percent));
        holder.setProgress(R.id.progress_schedule, percent, 100);
        holder.setText(R.id.tv_date_value, "讲师名：" + mineCourseBean.getTeacherName());


        /**
         * 点播
         */
        if (!isLiving) {
            holder.setVisible(R.id.tv_liveing, false);
            holder.setVisible(R.id.tv_course_content, false);
            return;
        } else if (isLiving) {
            //区分右上角课程状态
            holder.setVisible(R.id.tv_liveing, false);
            holder.setVisible(R.id.tv_course_content, false);
        }
        holder.setVisible(R.id.tv_video_type, false);
    }
}
