package com.example.app3pkt.view;

import com.example.app3libvariants.bean.SubjectBean;
import com.sdzn.core.base.BaseView;

import java.util.List;

public interface SpellingClassView extends BaseView {
    void onSubjectSuccess(List<SubjectBean> subjectList);

    void onSubjectFailed(String msg);
}
