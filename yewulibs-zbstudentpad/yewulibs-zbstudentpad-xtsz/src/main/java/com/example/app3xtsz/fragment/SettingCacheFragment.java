package com.example.app3xtsz.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.app3libpublic.utils.CacheUtils;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libpublic.utils.GlideCatchUtil;
import com.example.app3xtsz.R;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.ToastUtils;

/**
 * 缓存
 *
 * @author Reisen at 2017-12-08
 */

public class SettingCacheFragment extends BaseFragment {
    TextView tvCache;

    public static SettingCacheFragment newInstance() {
        return new SettingCacheFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_cache;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        tvCache = rootView.findViewById(R.id.tv_cache);
        tvCache.setText(getCacheSizes());
        rootView.findViewById(R.id.tv_cache).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCache();
            }
        });
    }

    public void clearCache() {
        DialogUtil.showDialog(getActivity(), "确定要清理缓存吗？", true, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearCaches();//清理缓存
            }
        });
    }

    private String getCacheSizes() {
        long imageCacheSize = FileUtils.getDirLength(CacheUtils.getImageCache());
        long appCacheSize = FileUtils.getDirLength(CacheUtils.getAppCache());
        return (imageCacheSize == -1 || appCacheSize == -1) ? "" :
                ConvertUtils.byte2FitMemorySize(imageCacheSize + appCacheSize);
    }

    private void clearCaches() {
        GlideCatchUtil.getInstance().cleanCacheDiskSelf();
        FileUtils.deleteFilesInDir(CacheUtils.getDownloadCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAvatarCache());
        FileUtils.deleteFilesInDir(CacheUtils.getVideoScreenshotCache());

        FileUtils.deleteFilesInDir(CacheUtils.getImageCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAppCache());
        //清除完数据后
        tvCache.setText(getCacheSizes());
        ToastUtils.showShort("清理成功");
    }
}
