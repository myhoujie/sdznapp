package com.example.app3xtsz.fragment;

import android.os.Bundle;
import android.widget.TextView;

import com.example.app3xtsz.R;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.App2Utils;

/**
 * 系统版本
 *
 * @author Reisen at 2017-12-08
 */

public class SettingVersionFragment extends BaseFragment {
    TextView mVersion;

    public static SettingVersionFragment newInstance() {
        return new SettingVersionFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_version;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mVersion = rootView.findViewById(R.id.tv_version);
        mVersion.setText(App2Utils.getAppVersionName(mContext));
    }
}
