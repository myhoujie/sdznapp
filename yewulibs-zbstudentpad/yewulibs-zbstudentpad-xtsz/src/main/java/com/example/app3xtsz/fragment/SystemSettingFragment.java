package com.example.app3xtsz.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libpublic.widget.radioview.FragmentTabUtils;
import com.example.app3libpublic.widget.radioview.RadioLayout;
import com.example.app3libpublic.widget.radioview.RadioView;
import com.example.app3libvariants.network.SPManager;
import com.example.app3xtsz.R;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.widget.SweetAlertDialog;

import java.util.ArrayList;

/**
 * 系统设置
 *
 * @author Reisen at 2017-12-08
 */

public class SystemSettingFragment extends BaseFragment {
    RadioLayout mRadioLayout;
    RadioView mRadioView;
    TextView tvOut;
    LinearLayout llOut;

    private ArrayList<Fragment> mFragments;
    public static final String AUTO_LOGIN = "autoLogin";

    public static SystemSettingFragment newInstance() {
        return new SystemSettingFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_systemsetting;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        mRadioLayout = rootView.findViewById(R.id.rl_system_radiolayout);
        mRadioView = rootView.findViewById(R.id.rv_cache);
        tvOut = rootView.findViewById(R.id.tv_out);
        llOut = rootView.findViewById(R.id.ll_out);
        tvOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExitDialog();
            }
        });
        initView();
    }

    private void initData() {
        mFragments = new ArrayList<>(3);
//        mFragments.add(SettingNetworkFragment.newInstance());
        mFragments.add(SettingCacheFragment.newInstance());
        mFragments.add(SettingVersionFragment.newInstance());

        mFragments.add(SettingWebFragment.newInstance("1"));
        mFragments.add(SettingWebFragment.newInstance("2"));
        mFragments.add(SettingRemoveFragment.newInstance());

        mFragments.add(SettingAboutFragment.newInstance());
    }

    private void initView() {
        FragmentTabUtils mTabUtils = new FragmentTabUtils(mContext, getActivity().getSupportFragmentManager(),
                mFragments, R.id.system_container, mRadioLayout, 0, true, false);
        mTabUtils.setNeedAnimation(true);
        mRadioView.setChecked(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SPToken.autoLogin(mContext)) {
            llOut.setVisibility(View.VISIBLE);
        } else {
            llOut.setVisibility(View.GONE);
        }
    }

    private void showExitDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定要退出登录吗？").setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
            @Override
            public void onClick(Dialog dialog, int which) {
                logout();
            }
        }).setNegativeButton("取消", new SweetAlertDialog.OnDialogClickListener() {
            @Override
            public void onClick(Dialog dialog, int which) {
                //隐藏dialog
            }
        });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
//        IntentController.toMain(mContext, false);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
        intent.putExtra(AUTO_LOGIN, false);
        startActivity(intent);
    }
}
