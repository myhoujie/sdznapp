package com.example.app3home.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3libpublic.adapter.SearchHistoryAdapter;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libpublic.widget.ClearEditText;
import com.example.app3libpublic.widget.flowlayout.FlowLayout;
import com.example.app3libpublic.widget.flowlayout.TagFlowLayout;
import com.example.app3libvariants.network.SPManager;
import com.example.app3pkt.fragment.SearchResultFragment;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.widget.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;


/**
 * 课程搜索（包含主页、直播、点播课程搜搜）
 *
 * @author Reisen at 2017-11-30
 */

public class SearchFragment extends BaseFragment implements View.OnClickListener {
    ClearEditText etSearch;
    TagFlowLayout tagSearchHistory;
    RelativeLayout rlHistory;

    private List<String> searchHistories;
    private SearchHistoryAdapter<String> searchHistoryAdapter;

    private int courseType;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    public static SearchFragment newInstance(@CourseCons.Type.CourseType int courseType) {

        Bundle args = new Bundle();
        args.putInt("courseType", courseType);
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_search;
    }

    @Override
    protected void onInit(Bundle bundle) {

        etSearch = rootView.findViewById(R.id.et_search);
        tagSearchHistory = rootView.findViewById(R.id.tag_search_history);
        rlHistory = rootView.findViewById(R.id.Rl_history);
        rootView.findViewById(R.id.tv_cancel).setOnClickListener(this);
        rootView.findViewById(R.id.iv_back).setOnClickListener(this);
        rootView.findViewById(R.id.iv_cart_shop).setOnClickListener(this);
        rootView.findViewById(R.id.ll_clear_history).setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {
        Bundle arg = getArguments();
        if (arg != null) {
            courseType = arg.getInt("courseType", CourseCons.Type.ALL);
        }
//        courseType = getActivity().getIntent().getIntExtra("courseType", CourseCons.Type.ALL);
        searchHistories = new ArrayList<>();
        searchHistories.addAll(SPManager.getSearchStr());
        if (searchHistories.isEmpty()) {
            rlHistory.setVisibility(View.GONE);
        } else {
            rlHistory.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
        etSearch.requestFocusFromTouch();
        searchHistoryAdapter = new SearchHistoryAdapter<>(mContext, searchHistories);
        tagSearchHistory.setAdapter(searchHistoryAdapter);
        tagSearchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                etSearch.setText(searchHistories.get(position));
                search(searchHistories.get(position));
                return true;
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        String searchStr = etSearch.getText().toString().trim();
                        search(searchStr);
                        KeyboardUtils.hideSoftInput(getActivity());
                    }
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnFocusChangedListener(new ClearEditText.onFocusChangedListener() {
            @Override
            public void onFocusChanged(View v, boolean hasFocus) {
                if (etSearch == v) {
                    if (hasFocus) {
                        KeyboardUtils.showSoftInput(mContext, etSearch);
                    } else {
                        KeyboardUtils.hideSoftInput(getActivity());
                    }
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyboardUtils.showSoftInput(mContext, etSearch);
            }
        }, 200);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_cancel) {
            KeyboardUtils.hideSoftInput(getActivity());
            onBackPressed();
        }else if( id == R.id.iv_back){
            KeyboardUtils.hideSoftInput(getActivity());
            onBackPressed();
        } else if (id == R.id.ll_clear_history) {
            showClearDialot();
        } else if (id == R.id.iv_cart_shop) {
            //IntentController.toShoppingCart(mContext);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");//ShoppingCartActivity
            startActivity(intent);
        }
    }

    private void showClearDialot() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定删除全部历史记录？")
                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        clearHistory();
                    }
                }).setCancelable(true).show();
    }

    private void search(String searchStr) {
//        if (TextUtils.isEmpty(searchStr)) {
//            ToastUtils.showShort("请输入搜索内容");
//        } else {
        rlHistory.setVisibility(View.VISIBLE);

        saveSearchStr(searchStr);
        searchHistoryAdapter.notifyDataChanged();
//        FragmentController.toSearchResult(SearchFragment.this, searchStr, courseType);
        showFragment(R.id.main_container, SearchResultFragment.newInstance(searchStr, courseType), true);
//        }
    }

    private void saveSearchStr(String searchStr) {
        if (searchStr.isEmpty()) {
            return;
        }
        if (searchHistories.contains(searchStr)) {
            searchHistories.remove(searchStr);
        }
        searchHistories.add(searchStr);
        SPManager.saveSearchStr(searchHistories);
    }

    private void clearHistory() {
        rlHistory.setVisibility(View.GONE);
        searchHistories.clear();
        SPManager.saveSearchStr(searchHistories);
        searchHistoryAdapter.notifyDataChanged();
    }


}
