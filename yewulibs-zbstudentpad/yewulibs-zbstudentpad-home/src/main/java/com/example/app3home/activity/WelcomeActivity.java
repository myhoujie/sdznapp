package com.example.app3home.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app3home.R;
import com.example.app3home.adapter.PageAdapterWithView;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.widget.SweetAlertDialog;

import java.util.ArrayList;
import java.util.List;


/**
 * 描述：
 * - 欢迎页
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WelcomeActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    ViewPager guideVp;
    LinearLayout ll;
    public static final String IS_MAIN_IN = "to_in_subject";
    public static final String AUTO_LOGIN = "autoLogin";
    public static final String INTENT_WEB = "userType";

    private final int[] IMGS = {R.mipmap.welcome1, R.mipmap.welcome2};
    private List<View> views;
    private PageAdapterWithView pageAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onInit(Bundle bundle) {
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    String bbbb = appLinkData.getQueryParameter("query2");
                    ToastUtils.showLong("query1->" + aaaa + ",query2->" + bbbb);
                }
            }
        }
        intiData();
        guideVp = findViewById(R.id.guide_vp);
        ll = findViewById(R.id.ll);
        findViewById(R.id.tv_user_agreement).setOnClickListener(this);
        findViewById(R.id.tv_privacy_agreement).setOnClickListener(this);
        findViewById(R.id.tv_agreement_no).setOnClickListener(this);
        findViewById(R.id.tv_agreement_yes).setOnClickListener(this);
        initView();
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.setColor(this, Color.WHITE, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    private void initView() {
        pageAdapter = new PageAdapterWithView(views);
        guideVp.setAdapter(pageAdapter);
        guideVp.addOnPageChangeListener(this);
        guideVp.setVisibility(View.GONE);
    }

    private void intiData() {
        views = new ArrayList<>();
        initImgs();
    }


    private void initImgs() {
        ImageView imageView = null;
        for (int i = 0; i < IMGS.length; i++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams imageviewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageviewParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageResource(IMGS[i]);
            views.add(imageView);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_user_agreement) {//                IntentController.toWeb(mContext, "1");
            Intent changeNameIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");// WebActivity.class
            changeNameIntent.putExtra(INTENT_WEB, "1");
            startActivity(changeNameIntent);
        } else if (id == R.id.tv_privacy_agreement) {//                IntentController.toWeb(mContext, "2");
            Intent changeNameIntent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");// WebActivity.class
            changeNameIntent1.putExtra(INTENT_WEB, "2");
            startActivity(changeNameIntent1);
        } else if (id == R.id.tv_agreement_no) {
            showDialog();
        } else if (id == R.id.tv_agreement_yes) {
            ll.setVisibility(View.GONE);
            guideVp.setVisibility(View.VISIBLE);
        }
    }

   /* public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_user_agreement:
                IntentController.toWeb(mContext, "1");
                break;
            case R.id.tv_privacy_agreement:
                IntentController.toWeb(mContext, "2");
                break;
            case R.id.tv_agreement_no:
                showDialog();
                break;
            case R.id.tv_agreement_yes:
                ll.setVisibility(View.GONE);
                guideVp.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }*/

    private void showDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("若不同意，则无法继续使用")
                .setPositiveButton("我再考虑下", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {

                    }
                })
                .setNegativeButton("不同意并退出", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        SPManager.setFirstEnterApp(mContext);
                        AppManager.getAppManager().appExit();
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == IMGS.length - 1) {
            views.get(position).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterApp();
                }
            });
        }
    }


    private void enterApp() {

        if (SPManager.isFirstSubjectId(mContext) && String.valueOf(SPManager.getgradeId()).isEmpty()) {
//            IntentController.toSelectSubject(mContext, false);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
            startIntent.putExtra(IS_MAIN_IN, false);
            startActivity(startIntent);
        } else {
//            IntentController.toMain(mContext, false);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
            intent.putExtra(AUTO_LOGIN, false);
            startActivity(intent);
        }
    }
}
