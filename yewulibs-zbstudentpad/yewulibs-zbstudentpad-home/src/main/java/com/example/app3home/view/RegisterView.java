package com.example.app3home.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/15
 */
public interface RegisterView extends BaseView {

    void getCodeSuccess();

    void getCodeFailure(String msg);

    void registerSuccess();

    void registerFailure(String msg);


}
