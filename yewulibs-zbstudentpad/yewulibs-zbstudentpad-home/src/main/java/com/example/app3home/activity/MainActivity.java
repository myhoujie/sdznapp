package com.example.app3home.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.azhon.appupdate.manager.DownloadManager;
import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.fragment.SpellingClassFragment;
import com.example.app3home.fragment.UserInfoFragment;
import com.example.app3home.persenter.CheckverionPresenter;
import com.example.app3home.persenter.MainPresenter;
import com.example.app3home.view.CheckverionView;
import com.example.app3home.view.MainView;
import com.example.app3libpublic.event.MsgCountEvent;
import com.example.app3libpublic.event.UpdateAvatarEvent;
import com.example.app3libpublic.permission.MPermission;
import com.example.app3libpublic.permission.annotation.OnMPermissionDenied;
import com.example.app3libpublic.permission.annotation.OnMPermissionGranted;
import com.example.app3libpublic.service.DownLoadDataService;
import com.example.app3libpublic.widget.CircleImageView;
import com.example.app3libpublic.widget.radioview.FragmentTabUtils;
import com.example.app3libpublic.widget.radioview.RadioLayout;
import com.example.app3libpublic.widget.radioview.RadioView;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.bean.VersionInfoBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.example.app3message.fragment.MessageFragment;
import com.example.app3mykc.fragment.MineFragment;
import com.example.app3xtsz.fragment.SystemSettingFragment;
import com.example.app3xxkc.fragment.LiveCourseFragment;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;


/**
 * 描述：
 * - 主页
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class MainActivity extends BaseMVPActivity<MainView, MainPresenter> implements CheckverionView, MainView, MineFragment.OnFragmentInteractionListener {
    RadioLayout rgNavi;
    CircleImageView iv_head;
    TextView tv_name;
    ImageView ivPoint;
    FrameLayout mainContainer;
    LinearLayout layout;

    private UserBean userBean;
    private List<Fragment> fragments;
    private FragmentTabUtils fragmentTabUtils;
    private SweetAlertDialog versionDialog;
    private RadioView mHome;
    private RadioView mMine;
    private long mCurrentMs = System.currentTimeMillis();
    public static final String AUTO_LOGIN = "autoLogin";
    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        requestBasicPermission();
        initData();
        rgNavi = findViewById(R.id.rg_navi_bottom);
        iv_head = findViewById(R.id.img_avatar);
        tv_name = findViewById(R.id.tv_name);
        ivPoint = findViewById(R.id.iv_point);
        mainContainer = findViewById(R.id.main_container);
        layout = findViewById(R.id.layout);

        initView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.setTranslucent(this, 0);
        StatusBarUtil.statusBarLightMode(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void initData() {
        EventBus.getDefault().register(this);
        userBean = SPManager.getUser();
        fragments = new ArrayList<>();
//        fragments.add(UserInfoFragment.newInstance());
        fragments.add(SpellingClassFragment.newInstance());//拼课堂
        fragments.add(LiveCourseFragment.newInstance());//学校课程
        fragments.add(MineFragment.newInstance());//我的课程
        fragments.add(UserInfoFragment.newInstance());//个人中心
        fragments.add(MessageFragment.newInstance());//消息中心
//        fragments.add(SystemSettingFragment.newInstance());
        fragments.add(SystemSettingFragment.newInstance());//设置
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        //检查更新
        checkverionPresenter.checkVerion("2", "0");

        if (SPToken.autoLogin(mContext)) {
            //获取未读消息数
            mPresenter.getUnReadMessageCount();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onResume() {
        DownLoadDataService.startDownloadService();
        super.onResume();

    }

    private void initView() {
        setImageHeader();
        mHome = (RadioView) findViewById(R.id.rb_home_page);
        mMine = (RadioView) findViewById(R.id.rb_mine);

        fragmentTabUtils = new FragmentTabUtils(mContext, getSupportFragmentManager(),
                fragments, R.id.main_container, rgNavi, 0, true, false);
        fragmentTabUtils.setNeedAnimation(true);
        fragmentTabUtils.setAnimation(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        mHome.setChecked(true);

    }


    @Override
    public void onBackPressed() {
        App2Utils.exitApp(mContext);
    }


    private ProgressDialog pd;
    private DownloadManager manager;
    private String updateTitle = "发现新版本";

/*
    @Override
    public void updateVersion(String updateInfo, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudenthd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂HD");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();

        // 需要更新，弹出更新窗口
      */
/*  UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(MainActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(MainActivity.this);
                manager.setApkName("拼课堂.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.live.tablet.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();*//*

    }
*/

    private void setImageHeader() {
        if (SPToken.autoLogin(mContext)) {
            GlideImgManager.loadImageNoErrImg(mContext, "" + userBean.getPicImg(), iv_head);
            if (!TextUtils.isEmpty(userBean.getStudentName())) {
                tv_name.setText(userBean.getStudentName());
            } else {
                tv_name.setText(userBean.getMobile());
            }
            iv_head.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rgNavi.checkedRadioSelector(R.id.rb_mine);
                    mMine.setChecked(true);
                }
            });
        } else {
            GlideImgManager.loadImage(mContext, R.mipmap.avatar, iv_head);
            tv_name.setText(getString(R.string.login_no));
            iv_head.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
                    startActivity(intent);
//                    IntentController.toLogin(mContext);
                }
            });
            ivPoint.setVisibility(View.GONE);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        switch (userInfoEvent.getName()) {
            case UpdateAccountEvent.CHANGE_NAME:
            case UpdateAccountEvent.CHANGE_SCHOOL:
            case UpdateAccountEvent.CHANGE_PHASE:
                userBean = SPManager.getUser();
                setImageHeader();
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void upPhoto(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            userBean = SPManager.getUser();
            setImageHeader();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void queryMessageCount(MsgCountEvent countEvent) {
        ivPoint.setVisibility(0 == countEvent.getCount() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        DownLoadDataService.stopDownloadService(MainActivity.this);
        super.onDestroy();
        EventBus.getDefault().unregister(MainActivity.this);
//        if (BuildConfig.ISDEBUG) {
        // FIXME: 2017-11-16 蒲公英sdk有问题, 会导致应用crash
//            PgyUpdateManager.unregister();
//        }
    }

    @Override
    public void onFragmentInteraction() {

    }

    public static final int PERMISSION_CODE = 12001;

    private void requestBasicPermission() {
        MPermission.with(this)
                .addRequestCode(PERMISSION_CODE)
                .permissions(
                        Manifest.permission.ACCESS_NETWORK_STATE,
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(PERMISSION_CODE)
    public void onBasicPermissionSuccess() {


    }

    @OnMPermissionDenied(PERMISSION_CODE)
    public void onBasicPermissionFailed() {
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        int currVersion = App2Utils.getAppVersionCode(App2.get());//获取版本号
        int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
        if (updateVersion > currVersion) {
            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
            }
        }
    }

    private void Updatemethod(String description, String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudenthd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂HD");//app名称
        String count = description.replace("|", "\n");

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
//            ToastUtils.showShort(bean);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
//            ToastUtils.showShort(msg);
    }

    @Override
    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
}