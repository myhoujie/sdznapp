package com.example.app3home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.app3home.R;
import com.example.app3libpublic.widget.TitleBar;
import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.StatusBarUtil;

/*跳转协议*/
public class WebActivity extends BaseActivity {
    public static final String INTENT_WEB = "userType";
    WebView mWebView;
    TitleBar titleBar;
    private String nameType = "";//1 拼课堂用户协议  2拼课堂用户隐私协议
    private String url = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_web;
    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
        StatusBarUtil.statusBarLightMode(this);
    }

    @Override
    protected void onInit(Bundle bundle) {
        mWebView = findViewById(R.id.web);
        titleBar = findViewById(R.id.title_bar);
        nameType = getIntent().getStringExtra("userType");
        if (!TextUtils.isEmpty(nameType)) {
            switch (nameType) {
                case "1":
                    url = "https://m.znclass.com/Useragreement1.html";
                    nameType = "用户协议";
                    break;
                case "2":
                    url = "https://m.znclass.com/agreement1.html";
                    nameType = "隐私协议";
                    break;
                default:
                    break;
            }
            titleBar.setTitleText(nameType);


            mWebView.loadUrl(url);
            WebSettings settings = mWebView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setLoadsImagesAutomatically(true);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
        }
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.this.finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        WebActivity.this.finish();
    }
}
