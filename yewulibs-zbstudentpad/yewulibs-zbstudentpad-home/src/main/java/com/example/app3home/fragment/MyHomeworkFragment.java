package com.example.app3home.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.example.app3home.R;
import com.example.app3libpublic.adapter.SpellingTitleAdapter;
import com.example.app3libpublic.event.UpdateAvatarEvent;
import com.example.app3libpublic.widget.EmptySchoolLayout;
import com.example.app3libpublic.widget.NoScrollViewPager;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libpublic.widget.pager.PagerSlidingTabStrip;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的作业
 */

public class MyHomeworkFragment extends BaseFragment {

    EmptySchoolLayout emptySchoolLayout;
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    NoScrollViewPager vpCourse;
    View views;
    TaskPageFragment todayCourseFragment = null;
    TitleBar mTitleBar;
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    private SpellingTitleAdapter fragmentAdapter;
    private List<Fragment> listFragment;//定义要装fragment的列表
    private ArrayList<String> listTitle;
    private OnFragmentInteractionListener mListener;
    private UserBean userBean;

    public MyHomeworkFragment() {
        // Required empty public constructor
    }

    public static MyHomeworkFragment newInstance() {
        return new MyHomeworkFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_homework;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        emptySchoolLayout = rootView.findViewById(R.id.empty_layout_mine);
        mPagerSlidingTabStrip = rootView.findViewById(R.id.tabs);
        vpCourse = rootView.findViewById(R.id.vp_course);
        mTitleBar = rootView.findViewById(R.id.title_bar);
        views = rootView.findViewById(R.id.view);
        initView();
        initData();
    }

    private void initData() {
//        userBean = SPManager.getUser();
//        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(), R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);

    }

    @Override
    public void onResume() {
        super.onResume();
        setInitOnResume();
    }

    private void initView() {
        listFragment = new ArrayList<>();
        listFragment.add(TaskPageFragment.newInstance(TaskPageFragment.TYPE_TODY));
        listFragment.add(TaskPageFragment.newInstance(TaskPageFragment.TYPE_RECENTLY));
        listTitle = new ArrayList<>();
        listTitle.add("未作答");
        listTitle.add("已作答");
        fragmentAdapter = new SpellingTitleAdapter(getChildFragmentManager());
        fragmentAdapter.setmDatas(listTitle, listFragment);
        vpCourse.setAdapter(fragmentAdapter);
        vpCourse.setNoScroll(false);
        vpCourse.setOffscreenPageLimit(2);
        vpCourse.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(vpCourse);
    }

//    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onFragmentInteraction();
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
/*        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setInitOnResume() {
        if (!SPToken.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_mine));
        } else {
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            views.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            initData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            setInitOnResume();
        }
    }
/*
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateWebViewInfo(WebViewAvatarEvent webViewAvatarEvent) {
        String url = webViewAvatarEvent.getUrl();
        String AnsweringState = webViewAvatarEvent.getAnsweringState();
        String token = webViewAvatarEvent.getToken();
        showFragmentAddAnimation(R.id.user_container, TaskWebFragment.newInstance(url, AnsweringState, token), true);
//        showFragmentAddAnimation(R.id.user_container, TaskWebFragment.newInstance(url), true);
    }*/
}