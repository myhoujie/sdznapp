package com.example.app3home.persenter;

import android.text.TextUtils;

import com.example.app3home.view.RetrievePwdView;
import com.example.app3libpublic.R;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public class RetrievePwdPresenter extends BasePresenter<RetrievePwdView> {

    public void getVerifyCode(String phoneNo) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void confirmVerifyCode(String phoneNo, String virifyCode) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .confirmVerifyCode(phoneNo, virifyCode)
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean loginBean) {
                        if (loginBean != null && !loginBean.getAccess_token().isEmpty()) {
                            String token = loginBean.getAccess_token();
                            SPToken.saveToken(token);
                            getView().confirmCodeSuccess();
                        } else {
                            onFail(new ApiException(new Throwable(), 10010));
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().confirmCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }
}
