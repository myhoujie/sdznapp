package com.example.app3home.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3home.R;
import com.example.app3home.adapter.OrderInfoAdapter;
import com.example.app3home.persenter.OrderSubmitPresenter;
import com.example.app3home.view.OrderSubmitView;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.manager.AlipayManager;
import com.example.app3libpublic.manager.WXPayManager;
import com.example.app3libpublic.manager.constant.PayType;
import com.example.app3libpublic.pop.CouponPop;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libpublic.widget.radioview.RadioCardView;
import com.example.app3libpublic.widget.radioview.RadioLayout;
import com.example.app3libvariants.bean.CouponBean;
import com.example.app3libvariants.bean.PayInfoBean;
import com.example.app3libvariants.bean.ShoppingCartBean;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.NetworkUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.SweetAlertDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 描述：
 * - 订单提交
 * 创建人：baoshengxiang
 * 创建时间：2017/7/8
 */
public class OrderSubmitActivity extends BaseMVPActivity<OrderSubmitView, OrderSubmitPresenter>
        implements OrderSubmitView, RadioLayout.OnCheckedChangeListener, View.OnClickListener {

    RecyclerView rcvCourse;
    TextView tvOrderAmount;
    TextView tvDiscountAmount;
    TextView tvActualPayment;
    TitleBar titleBar;
    RadioLayout mRadioLayout;
    RadioCardView mWXCard;
    RadioCardView mAliCard;
    TextView tvCoupon;
    Button btnCertainPay;
    RelativeLayout rlSettlement;

    private List<CouponBean> couponBeans;
    private List<ShoppingCartBean.ShopCartListBean> orderInfoBeans;
    private OrderInfoAdapter orderInfoAdapter;
    private double totalPrice;
    private double discountPrice;
    private String payType;
    private String couponCode;
    private CouponPop mCouponPop;

    private String shopCarIdStr = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_submit;
    }

    @Override
    protected OrderSubmitPresenter createPresenter() {
        return new OrderSubmitPresenter();
    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
        StatusBarUtil.statusBarLightMode(this);
    }

    @Override
    protected void onInit(Bundle bundle) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initData();
        rcvCourse = findViewById(R.id.rcv_course);
        tvOrderAmount = findViewById(R.id.tv_order_amount);
        tvDiscountAmount = findViewById(R.id.tv_discount_amount);
        tvActualPayment = findViewById(R.id.tv_actual_payment);
        titleBar = findViewById(R.id.title_bar);
        mRadioLayout = findViewById(R.id.rl_payment);
        mWXCard = findViewById(R.id.card_wx);
        mAliCard = findViewById(R.id.card_alipay);
        tvCoupon = findViewById(R.id.tv_coupon);
        btnCertainPay = findViewById(R.id.btn_certain_pay);
        rlSettlement = findViewById(R.id.rl_settlement);
        findViewById(R.id.btn_certain_pay).setOnClickListener(this);
        findViewById(R.id.rl_coupon).setOnClickListener(this);
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        orderInfoBeans = new ArrayList<>();
        couponBeans = new ArrayList<>();
        payType = PayType.ALIPAY;
        //通过课程详情或购物车传过来的课程数据
//        OrderGoodsBean goods = getIntent().getParcelableExtra("goods");
//        orderInfoBeans.addAll(goods.getGoodsBeanList());
//        totalPrice = goods.getTotalPrice();
        shopCarIdStr = getIntent().getStringExtra("goods");
    }

    private void initView() {
        mRadioLayout.setOnCheckedChangeListener(this);
        mAliCard.onClick(null);
        orderInfoAdapter = new OrderInfoAdapter(mContext, orderInfoBeans);
        mPresenter.queryShoppingCart(shopCarIdStr);//获取购物车数据
        rcvCourse.setLayoutManager(new GridLayoutManager(mContext, 1));
        rcvCourse.setAdapter((RecyclerView.Adapter) orderInfoAdapter);


        String discount = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
        tvDiscountAmount.setText(splitString("优惠金额：", discount));

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_certain_pay) {
            if (TextUtils.isEmpty(payType)) {
                ToastUtils.showShort("请选择支付方式后结算");
                return;
            }
            if (totalPrice - discountPrice <= 0) {
                ToastUtils.showShort("结算价格不能为负");
                return;
            }
            if (TextUtils.isEmpty(shopCarIdStr)) {
                ToastUtils.showShort("数据错误，请重新选择支付课程");
                return;
            }
            mPresenter.submitOrder(getOrderParams(),payType);
        } else if (id == R.id.rl_coupon) {
            if (couponBeans.isEmpty()) {
                return;
            }
            showCouponPop();
        }
    }

    private void showCouponPop() {
        if (mCouponPop == null) {
            mCouponPop = new CouponPop(mContext, couponBeans);
        }
        if (!mCouponPop.isShowing()) {
            mCouponPop.showAtViewCenter(findViewById(android.R.id.content), new CouponPop.OnItemClickedListener() {
                @Override
                public void clickItem(CouponBean bean) {
                    tvCoupon.setText(bean.getTitle());
                    couponCode = bean.getCouponCode();
                    String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
                    tvOrderAmount.setText(splitString("订单金额：", orderPrice));
                    discountPrice = bean.getAmount();
                    String couponPrice = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
                    tvDiscountAmount.setText(splitString("优惠金额：", couponPrice));
                    tvCoupon.setText(String.format("-%s", couponPrice));
                    double price = totalPrice - discountPrice;
                    if (price < 0) {
                        price = 0;
                    }
                    String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
                    tvActualPayment.setText(splitString("实付款：", actualPrice));
                }
            });
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart(shopCarIdStr);
        if (orderPayEvent.isSuccess()) {
            SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
            builder.setMessage("支付成功");
            builder.setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                @Override
                public void onClick(Dialog dialog, int which) {
                    finish();
                }
            }).show();
        } else {
            finish();
        }
    }

    @Override
    public void checkedChange(int resId) {
        if (resId == R.id.card_wx) {
            payType = PayType.WXPAY;
        } else if (resId == R.id.card_alipay) {
            payType = PayType.ALIPAY;
        }
    }

    private Map<String, String> getOrderParams() {
        Map<String, String> params = new HashMap<>();
        /*params.put("reqIp", NetworkUtils.getIPAddress(true));//请求ip地址
        params.put("orderType", "COURSE");//订单类型（COURSE课程、MEMBER会员、ACCOUNT账户充值）
        params.put("payType", payType);//支付方式，可选值： ALIPAY,支付宝；WEIXIN,微信
        if (!TextUtils.isEmpty(couponCode)) {
            params.put("couponCode", couponCode);//优惠券编码
        }*/
        params.put("couponPrice", "0");
        params.put("couponCode", "");
        params.put("ids", shopCarIdStr);
//        params.put("otherId","");//购买课程时otherId为空，账户充值时otherId是充值金额，购买会员时otherId为会员id
        return params;
    }

    private SpannableString splitString(String firstStr, String secStr) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textPrimary)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    @Override
    public void queryCartSuccess(ShoppingCartBean shoppingCartBeen) {
        orderInfoBeans.addAll(shoppingCartBeen.getShopcartList());
        this.totalPrice = shoppingCartBeen.getTotalPrice();
        String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
        tvOrderAmount.setText(splitString("订单金额：", orderPrice));
        double price = totalPrice - discountPrice;
        if (price < 0) {
            price = 0;
        }
        String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
        tvActualPayment.setText(splitString("实付款：", actualPrice));
        orderInfoAdapter.notifyDataSetChanged();
//        if (Config.ADD_TEST_DATA) {
//            List<CouponBean> list = shoppingCartBeen.getCouponCodeList();
//            shoppingCartBeen.setCouponCodeList(list);
//        }
        if (shoppingCartBeen.getCouponCodeList().isEmpty()) {
            tvCoupon.setText("暂无可用优惠券");
        } else {
            tvCoupon.setText("有可用优惠券");
            couponBeans.clear();
            couponBeans.addAll(shoppingCartBeen.getCouponCodeList());
        }
    }

    @Override
    public void getPayInfoSuccess(PayInfoBean payInfoBean) {
        if (PayType.WXPAY.equals(payInfoBean.getPayType())) {
            WXPayManager wxPayManager = new WXPayManager(mContext);
            wxPayManager.doStartWXPayPlugin(payInfoBean.getWxpayParams());
        } else {
            AlipayManager alipayManager = new AlipayManager(mContext);
            alipayManager.doStartALiPayPlugin(payInfoBean.getAlipayOrderStr());
        }
    }

    @Override
    public void getPayInfoFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void queryCartEmpty() {
        ToastUtils.showShort("订单列表为空，请返回重试");
    }

    @Override
    public void queryCartFailure(String msg) {
        ToastUtils.showShort("课程获取失败，请返回重试");
    }

    @Override
    public void submitOrderSuccess() {

    }

    @Override
    public void submitOrderFailure(String msg) {
        ToastUtils.showShort("课程购买失败，请稍后重试");
    }


}
