package com.example.app3home.persenter;

import android.text.TextUtils;

import com.example.app3home.view.MainView;
import com.example.app3libpublic.R;
import com.example.app3libpublic.event.MsgCountEvent;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.ResponseFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import org.greenrobot.eventbus.EventBus;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 */
public class MainPresenter extends BasePresenter<MainView> {
//    public void checkVerion() {
//        Subscription subscribe = RestApi.getInstance()
//                .createNew(AccountService.class)
//                .queryVersion(1)//0安卓手机  1安卓pad
//                .compose(TransformUtils.<ResultBean<VersionInfoBean>>defaultSchedulers())
//                .map(new ResponseFunc<VersionInfoBean>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfoBean>() {
//                    @Override
//                    public void onNext(VersionInfoBean versionInfoBean) {
//                        int currVersion = App2Utils.getAppVersionCode(App2.get());
//                        if (versionInfoBean.getVersionNumber() > currVersion) {//此处为
//                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
//                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        e.printStackTrace();
//                    }
//                }, mActivity, false));
//        addSubscribe(subscribe);
//    }

    public void getUnReadMessageCount() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryUnReadMsgCount("0", SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
//                        getView().queryUnReadMsgCountSuccess((int) resultBean.getUnreadMsgCount());
                        Double obj = (Double) o;
                        EventBus.getDefault().post(new MsgCountEvent(obj.intValue()));
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
//                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }


//    /**
//     * 辅助线
//     */
//    public void login(final String name, String psw) {
//
//        final String deviceId = AndroidUtil.getDeviceID(App.mContext);
//        //这里上传敏感数据会导致无法通过安全测试
//        psw = Base64.encodeToString(psw.getBytes(), Base64.NO_WRAP);
//        Network.createService(NetWorkService.LoginService.class)
//                .login(name, psw, 1, deviceId, "Android")
//                .map(new StatusFunc<LoginBean>())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new ProgressSubscriber<LoginBean>(new SubscriberListener<LoginBean>() {
//                    @Override
//                    public void onNext(LoginBean o) {
//                        getView().loginSuccess(o);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                       getView().loginFiled();
//                    }
//
//                    @Override
//                    public void onCompleted() {
//
//                    }
//                }, mActivity, false, false, true, null));
//    }
}
