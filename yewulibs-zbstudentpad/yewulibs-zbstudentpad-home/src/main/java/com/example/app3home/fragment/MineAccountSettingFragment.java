package com.example.app3home.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.example.app3grzx.fragment.BindingAccountFragment;
import com.example.app3grzx.fragment.BindingMobileFragment;
import com.example.app3grzx.fragment.ChangeClassFragment;
import com.example.app3grzx.fragment.ChangeNameFragment;
import com.example.app3grzx.fragment.ChangePwdFragment;
import com.example.app3grzx.fragment.ChangeSchoolNewFragment;
import com.example.app3grzx.presenter.AccountSettingPresenter;
import com.example.app3grzx.view.AccountSettingView;
import com.example.app3home.R;
import com.example.app3libpublic.event.BindEvent;
import com.example.app3libpublic.event.UpdateAvatarEvent;
import com.example.app3libpublic.listener.AlbumOrCameraListener;
import com.example.app3libpublic.utils.CacheUtils;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libpublic.utils.GradeIdToNameUtils;
import com.example.app3libpublic.utils.PermissionUtils;
import com.example.app3libpublic.widget.ActionSheet.ActionSheetDialog;
import com.example.app3libpublic.widget.ActionSheet.SheetItem;
import com.example.app3libpublic.widget.CircleImageView;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.GradeBean;
import com.example.app3libvariants.bean.GradeJson;
import com.example.app3libvariants.bean.SectionBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.app3libpublic.manager.constant.IntentCons.REQ_CODE_ALBUM;
import static com.example.app3libpublic.manager.constant.IntentCons.REQ_CODE_CAMERA;
import static com.example.app3libpublic.manager.constant.IntentCons.RESULT_OK;

/**
 * 我的账户
 *
 * @author Reisen at 2017-12-06
 */

public class MineAccountSettingFragment extends BaseMVPFragment<AccountSettingView, AccountSettingPresenter>
        implements AccountSettingView, View.OnClickListener {

    TitleBar titleBar;
    CircleImageView imgAvatar;

    TextView tvSex;
    TextView tvSchoolWarning;


    TextView tvMobile;
    TextView tvAccount;
    ImageView imAccount;
    TextView tvPwd;
    LinearLayout rlLogout;
    Button btnCertain;

    TextView tvName;
    ImageView imName;
    TextView tvSchool;
    ImageView imSchool;
    TextView tvPhase;
    ImageView imPhase;
    TextView tvGrade;
    ImageView imGrade;
    TextView tvClass;
    ImageView imClass;

    private File takeImageFile;
    UserBean userBean;
    private int selectType = 0;//0默认为拍照，1为相册选择图片
    private int sex = 1;
    private Map<String, String> requestParams;
    private List<GradeJson> gradeJsonList;


    private boolean hasSchool;
    private List<SectionBean> sections;
    private boolean isToB;
    private List<GradeBean> grades;
    private int sectionId = -1;
    private int eduId = -1;
    private int gradeId = -1;

    public static MineAccountSettingFragment newInstance() {
        return new MineAccountSettingFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_accountsetting;
    }

    @Override
    protected AccountSettingPresenter createPresenter() {
        return new AccountSettingPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        if (SPManager.isToCLogin()) {//toc
            isToB = false;
        } else {
            isToB = true;
        }
        titleBar = rootView.findViewById(R.id.title_bar);
        imgAvatar = rootView.findViewById(R.id.img_avatar);
        tvSex = rootView.findViewById(R.id.tv_sex);
        tvSchoolWarning = rootView.findViewById(R.id.tv_school_warning);
        tvMobile = rootView.findViewById(R.id.tv_mobile);
        tvAccount = rootView.findViewById(R.id.tv_account);
        imAccount = rootView.findViewById(R.id.im_account);
        tvPwd = rootView.findViewById(R.id.tv_pwd);
        rlLogout = rootView.findViewById(R.id.rl_logout);
        btnCertain = rootView.findViewById(R.id.btn_certain);
        tvName = rootView.findViewById(R.id.tv_name);
        imName = rootView.findViewById(R.id.im_name);
        tvSchool = rootView.findViewById(R.id.tv_school);
        imSchool = rootView.findViewById(R.id.im_school);
        tvPhase = rootView.findViewById(R.id.tv_phase);
        imPhase = rootView.findViewById(R.id.im_phase);
        tvGrade = rootView.findViewById(R.id.tv_grade);
        imGrade = rootView.findViewById(R.id.im_grade);
        tvClass = rootView.findViewById(R.id.tv_class);
        imClass = rootView.findViewById(R.id.im_class);
        rootView.findViewById(R.id.rl_change_img).setOnClickListener(this);
        rootView.findViewById(R.id.rl_name).setOnClickListener(this);
        rootView.findViewById(R.id.rl_sex).setOnClickListener(this);
        rootView.findViewById(R.id.rl_school).setOnClickListener(this);
        rootView.findViewById(R.id.rl_phase).setOnClickListener(this);
        rootView.findViewById(R.id.rl_grade).setOnClickListener(this);
        rootView.findViewById(R.id.rl_phone).setOnClickListener(this);
        rootView.findViewById(R.id.rl_password).setOnClickListener(this);
        rootView.findViewById(R.id.tv_logout).setOnClickListener(this);
        rootView.findViewById(R.id.btn_certain).setOnClickListener(this);
        rootView.findViewById(R.id.rl_class).setOnClickListener(this);
        rootView.findViewById(R.id.rl_account).setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {
        requestParams = new HashMap<>();
        gradeJsonList = new ArrayList<>();
        sections = new ArrayList<>();
        grades = new ArrayList<>();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        userBean = SPManager.getUser();
        onGradeSuccess(GradeIdToNameUtils.getGrade());
//        hasSchool = userBean.getCustomerId() != 0;
    }


    private void initView() {
        titleBar.setTitleText("基本信息");
//        updataUI();
    }

    /**
     * 头像  imgAvatar
     * 姓名  tv_name     im_name
     * 学校  tv_school   im_school
     * 学段  tv_phase    im_phase
     * 年级  tv_grade    im_grade
     * 班级  tv_class    im_class
     * <p>
     * 账号  tv_account  im_account
     */
    private void updataUI() {
        if (isToB) {//tob
            setVisivility(View.INVISIBLE);
            if (userBean.getUserName() != null && !userBean.getUserName().isEmpty()) {
                tvAccount.setText(userBean.getUserName());
            }
            SPManager.saveSchoolSection(sectionId, tvPhase.getText().toString(), eduId);
        } else {//toc
            setVisivility(View.VISIBLE);

        }

        if (userBean.getStudentName() != null && !userBean.getStudentName().isEmpty()) {
            tvName.setText(userBean.getStudentName());
            imName.setVisibility(View.INVISIBLE);
        } else {
            tvName.setText("");
        }
        if (userBean.getSchoolName() != null && !userBean.getSchoolName().isEmpty()) {
            tvSchool.setText(userBean.getSchoolName());
        } else {
            tvSchool.setText("");

        }

        if (userBean.getSubjectId() > -1 && gradeJsonList.size() > 0) {
            eduId = userBean.getEducationId();
            sectionId = userBean.getSubjectId();
            gradeId = userBean.getGrade();
            setSectionAndGrade(gradeJsonList);
        }

        if (userBean.getClasses() != null && !userBean.getClasses().isEmpty()) {
            tvClass.setText(userBean.getClasses());
        } else {
            tvClass.setText("");
        }
        if (userBean.getMobile() != null && !userBean.getMobile().isEmpty()) {
            tvMobile.setText(userBean.getMobile());
        }
        if (userBean.getPassword() != null && !userBean.getPassword().isEmpty()) {
            tvPwd.setText("******");
        }
        Glide.with(getActivity()).load("" + userBean.getPicImg())
                .placeholder(R.mipmap.ic_avatar) //占位图
                .dontAnimate()
                .error(R.mipmap.ic_avatar) //网络错误时候加载的图
                .into(imgAvatar);
//        GlideImgManager.loadImageNoErrImg(mContext, "" + userBean.getPicImg(), imgAvatar);
    }

    /**
     * 设置tob toc
     */
    private void setVisivility(int visivility) {
        imAccount.setVisibility(visivility);
        imName.setVisibility(visivility);
        imSchool.setVisibility(visivility);
        imPhase.setVisibility(visivility);
        imGrade.setVisibility(visivility);
        imClass.setVisibility(visivility);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.rl_change_img) {
            DialogUtil.showSelectImgDialog((Activity) mContext, new AlbumOrCameraListener() {
                @Override
                public void selectAlbum() {
                    selectType = 1;
                    getPermission();
                }

                @Override
                public void selectCamera() {
                    selectType = 0;
                    getPermission();
                }
            });
        } else if (id == R.id.rl_name) {
            if (!isToB) {
                if (tvName.getText().toString().isEmpty()) {
//                        FragmentController.toChangeName(this, tvName.getText().toString());
                    showFragmentAddAnimation(R.id.user_container, ChangeNameFragment.newInstance(tvName.getText().toString()), true);
                }
            }
        } else if (id == R.id.rl_school) {
            if (!isToB) {
//                    FragmentController.toChangeSchool(this, tvSchool.getText().toString());
                showFragmentAddAnimation(R.id.user_container, ChangeSchoolNewFragment.newInstance(tvSchool.getText().toString()), true);
            }
        } else if (id == R.id.rl_phase) {
            if (!isToB) {
                showChagePhaseDialog();
            }
        } else if (id == R.id.rl_grade) {
            if (!isToB) {
                showChageGradeDialog();
            }
        } else if (id == R.id.rl_class) {
            if (!isToB) {
//                    FragmentController.toChangeClass(this, tvClass.getText().toString());
                showFragmentAddAnimation(R.id.user_container, ChangeClassFragment.newInstance(tvClass.getText().toString()), true);
            }
        } else if (id == R.id.rl_phone) {//                showCheckPwdAlert();
//                FragmentController.toBindingMobile(MineAccountSettingFragment.this);
            showFragmentAddAnimation(R.id.user_container, BindingMobileFragment.newInstance(), true);
        } else if (id == R.id.rl_account) {
            if (userBean == null) {
                return;
            }
            if (!isToB) {//跳转
//                    FragmentController.toBindingAccount(MineAccountSettingFragment.this);
                showFragmentAddAnimation(R.id.user_container, BindingAccountFragment.newInstance(tvAccount.getText().toString()), true);
            }
        } else if (id == R.id.rl_password) {//                FragmentController.toChangePwd(this);
            showFragmentAddAnimation(R.id.user_container, ChangePwdFragment.newInstance(), true);
        } else if (id == R.id.tv_logout) {//                showExitDialog();
        } else if (id == R.id.btn_certain) {//                perfectAccount();
        }
    }

//    private void showCheckPwdAlert() {
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final CustomDialog dialog = new CustomDialog(mContext, R.style.Dialog);
//        final View layout = inflater.inflate(R.layout.dialog_check_pwd, null);
//        dialog.addContentView(layout, new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        (layout.findViewById(R.id.tvPositive))
//                .setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View v) {
//                        String text = ((PwdEditText)layout.findViewById(R.id.et_pwd)).getText().toString();
//                        if (TextUtils.equals(SPManager.getPwd(),text)) {
//                            FragmentController.toBindingMobile(MineAccountSettingFragment.this);
//                        }else {
//                            ToastUtils.showShort("密码错误");
//                        }
//                        KeyboardUtils.hideSoftInput(getActivity(),layout);
//                        dialog.dismiss();
//                    }
//                });
//        (layout.findViewById(R.id.tvNegative))
//                .setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View v) {
//                        KeyboardUtils.hideSoftInput(getActivity(),layout);
//                        dialog.dismiss();
//                    }
//                });
//        dialog.setContentView(layout);
//        dialog.show();
//    }


    private void takePhoto() {
        Intent takePtotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePtotoIntent.resolveActivity(mContext.getPackageManager()) != null) {
            String fileName = "head" + System.currentTimeMillis() + ".jpg";
            takeImageFile = new File(CacheUtils.getAvatarCache(), fileName);
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                uri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".fileprovider", takeImageFile);
                //加入uri权限 要不三星手机不能拍照
                List<ResolveInfo> resInfoList = mContext.getPackageManager().queryIntentActivities
                        (takePtotoIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    mContext.grantUriPermission(packageName, uri, Intent
                            .FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            } else {
                uri = Uri.fromFile(takeImageFile);
            }
            takePtotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            this.startActivityForResult(takePtotoIntent, REQ_CODE_CAMERA);
        }
    }


    private void showChageGradeDialog() {
        String phase = tvPhase.getText().toString().trim();
        if (TextUtils.isEmpty(phase)) {
            ToastUtils.showShort("请先选择学段");
            return;
        }
        List<SheetItem> sheetItems = new ArrayList<>();
        for (GradeBean grade : grades) {
            SheetItem si = new SheetItem(grade.getGradeName(), null, null);
            sheetItems.add(si);
        }
        new ActionSheetDialog(mContext, sheetItems)
                .builder()
                .setTitle("请选择年级")
                .setCancelable(false)
                .setCanceledOnTouchOutside(true)
                .addSheetItem(new BaseRcvAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String gradeName = grades.get(position).getGradeName();
                        tvGrade.setText(gradeName);
                        gradeId = grades.get(position).getGradeId();
                        //提交
                        requestParams.put(UpdateAccountEvent.CHANGE_GRADE,
                                String.valueOf(gradeId));
                        requestParams.put(UpdateAccountEvent.CHANGE_GRADE_NAME,
                                tvGrade.getText().toString());
                        mPresenter.perfectAccount("", "", "", "", String.valueOf(gradeId), tvGrade.getText().toString());
                    }
                }).show();
    }

    private void showChagePhaseDialog() {
        List<SheetItem> sheetItems = new ArrayList<>();
        for (SectionBean phase : sections) {
            SheetItem si = new SheetItem(phase.getSectionName(), null, null);
            sheetItems.add(si);
        }
        new ActionSheetDialog(mContext, sheetItems)
                .builder()
                .setTitle("请选择学段")
                .setCancelable(false)
                .setCanceledOnTouchOutside(true)
                .addSheetItem(new BaseRcvAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (sections.get(position).getSectionId() != sectionId) {
                            gradeId = Integer.valueOf(sections.get(position).getGradeId());
                        }
                        sectionId = sections.get(position).getSectionId();
                        eduId = Integer.valueOf(sections.get(position).getEducationId());

                        setSectionAndGrade(gradeJsonList);
                        //提交
                        requestParams.put(UpdateAccountEvent.CHANGE_PHASE,
                                String.valueOf(sectionId));
                        requestParams.put(UpdateAccountEvent.CHANGE_GRADE,
                                String.valueOf(gradeId));
                        requestParams.put(UpdateAccountEvent.CHANGE_GRADE_NAME,
                                tvGrade.getText().toString());

                        mPresenter.perfectAccount(String.valueOf(sections.get(position).getEducationId()), sections.get(position).getEducationName(),
                                String.valueOf(sections.get(position).getSectionId()), sections.get(position).getSectionName(),
                                sections.get(position).getGradeId(), sections.get(position).getGradeName());


                    }
                }).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_CAMERA:
                    mPresenter.upAvatar(takeImageFile.getAbsolutePath(), false);
                    CacheUtils.sendUpdateBroadcast(takeImageFile);
                    break;
                case REQ_CODE_ALBUM:
                    Uri uri = data.getData();
                    String path = FileUtils.getUriPath(uri, getActivity());
                    mPresenter.upAvatar(path, true);
                    break;
                default:
                    break;
            }
        }
    }

    //相册选照片
    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQ_CODE_ALBUM);
    }

    private void getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionUtils.getNeededPermission(mContext, new PermissionUtils.OnHasGetPermissionListener() {
                @Override
                public void onSuccess() {
                    if (selectType == 1) {
                        selectImage();
                    } else {
                        takePhoto();
                    }
                }

                @Override
                public void onFail(List<PermissionInfo> permissionInfos) {
                    showGetPermissFailure(permissionInfos);
                }
            }, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        } else {
            if (selectType == 1) {
                selectImage();
            } else {
                takePhoto();
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        userBean = SPManager.getUser();
        updataUI();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateAccount(BindEvent event) {
        onInit(null);
    }


    @Override
    public void changePhotoSuccess(String photo) {
        userBean.setPicImg(photo);
        SPManager.saveUser(userBean);
        updataUI();
        EventBus.getDefault().post(new UpdateAvatarEvent(1));
    }

    @Override
    public void changePhotoError(String msg) {
        ToastUtils.showShort(msg);
    }

    private void setSectionAndGrade(List<GradeJson> gradeJson) {
        this.sections.clear();
        this.grades.clear();
//        this.sections.add(new SectionBean(-1, getString(R.string.school_all_section)));
//        this.grades.add(new GradeBean(-1, getString(R.string.school_all_grade)));
        boolean isGradeCheck = true;
        for (int i = 0; i < gradeJson.size(); i++) {
            this.sections.add(new SectionBean(Integer.valueOf(gradeJson.get(i).getLevelId()), gradeJson.get(i).getLocalName(),
                    Integer.valueOf(gradeJson.get(i).getEducationId()), gradeJson.get(i).getEducationName(),
                    gradeJson.get(i).getChildList().get(0).getGradeId(), gradeJson.get(i).getChildList().get(0).getGradeName()));//添加学段列表

            if (gradeJson.get(i).getEducationId().equalsIgnoreCase(String.valueOf(eduId)) && gradeJson.get(i).getLevelId().equalsIgnoreCase(String.valueOf(sectionId))) {//根据学制，学段  来找年级
                tvPhase.setText(gradeJson.get(i).getLocalName());
                for (int j = 0; j < gradeJson.get(i).getChildList().size(); j++) {
                    this.grades.add(new GradeBean(Integer.valueOf(gradeJson.get(i).getChildList().get(j).getGradeId()), gradeJson.get(i).getChildList().get(j).getGradeName()));
                    if (gradeId > 0 && gradeId == Integer.valueOf(gradeJson.get(i).getChildList().get(j).getGradeId())) {//toc登录
                        isGradeCheck = false;
                        tvGrade.setText(gradeJson.get(i).getChildList().get(j).getGradeName());
                    }
                }
                if (isGradeCheck && gradeJson.get(i).getChildList().size() > 0) {//没有相等的年级
                    gradeId = Integer.valueOf(gradeJson.get(i).getChildList().get(0).getGradeId());
                    tvGrade.setText(gradeJson.get(i).getChildList().get(0).getGradeName());
                }
            }
        }
    }


    /**
     * 返回数据
     *
     * @param userBean
     */

    @Override
    public void perfectAccountSuccess(UserBean userBean) {
        this.userBean = userBean;
        SPManager.saveUser(userBean);
        SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
        SPManager.saveSection(userBean.getSubjectId(), userBean.getSubjectName(), userBean.getEducationId());

        if (!tvPhase.getText().toString().isEmpty()) {
            SPManager.getUser().setSubjectName(tvPhase.getText().toString());
        }
        if (requestParams.containsKey(UpdateAccountEvent.CHANGE_SCHOOL_NAME)) {

        } else if (requestParams.containsKey(UpdateAccountEvent.CHANGE_PHASE)) {
            SPManager.saveUser(userBean);
            SPManager.saveSection(userBean.getSubjectId(), userBean.getSubjectName(), userBean.getEducationId());
            SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));
        } else if (requestParams.containsKey(UpdateAccountEvent.CHANGE_GRADE)) {
            SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());
            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_GRADE));
        }
        updataUI();
        requestParams.clear();
    }

    @Override
    public void perfectAccountFailure(String msg) {
        ToastUtils.showShort(msg);
        if (hasSchool) {
            updataUI();
            requestParams.clear();
        }
    }

    private void onGradeSuccess(List<GradeJson> gradeJson) {
        this.gradeJsonList.clear();
        this.gradeJsonList.addAll(gradeJson);
        updataUI();
    }


    /**
     * 获取权限失败时的操作
     *
     * @param permissionInfos
     */
    private void showGetPermissFailure(List<PermissionInfo> permissionInfos) {
        StringBuilder permisiiionSb = new StringBuilder();
        for (PermissionInfo permissionInfo : permissionInfos) {
            if (permissionInfo.name.equals(Manifest.permission.CAMERA)) {
                permisiiionSb.append("相机的权限").append("/");
            } else if (permissionInfo.name.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    permissionInfo.name.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permisiiionSb.append("存储卡的读写权限").append("/");
            }
        }
        permisiiionSb.deleteCharAt(permisiiionSb.lastIndexOf("/"));
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("我们需要一些权限")
                .setMessage("获取" + permisiiionSb.toString() + "将使我们能够更好的提供图片服务。\n操作路径：设置->应用->"
                        + getString(R.string.app_name) + "->权限")
                .setPositiveButton("去设置", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ToastUtils.showShort("请在手机的：设置->应用->"
                                + getString(R.string.app_name) + "->权限，选项中允许" +
                                getString(R.string.app_name) + "访问您的相机和存储空间");
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionUtils.checkPermissionResult(mContext, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

}
