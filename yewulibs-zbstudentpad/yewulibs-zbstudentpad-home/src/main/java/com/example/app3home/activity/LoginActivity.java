package com.example.app3home.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.example.app3home.R;
import com.example.app3home.persenter.CheckverionPresenter;
import com.example.app3home.persenter.LoginPresenter;
import com.example.app3home.view.CheckverionView;
import com.example.app3home.view.LoginView;
import com.example.app3libpublic.manager.Config;
import com.example.app3libpublic.permission.MPermission;
import com.example.app3libpublic.permission.annotation.OnMPermissionDenied;
import com.example.app3libpublic.permission.annotation.OnMPermissionGranted;
import com.example.app3libpublic.utils.CountDownTimerUtils;
import com.example.app3libpublic.utils.VerifyUtil;
import com.example.app3libpublic.widget.CheckBoxSample;
import com.example.app3libpublic.widget.ClearEditText;
import com.example.app3libpublic.widget.PwdEditText;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.bean.VersionInfoBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AndroidBug5497Workaround;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;


/**
 * 描述：
 * - 用户登录
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */

public class LoginActivity extends BaseMVPActivity<LoginView, LoginPresenter> implements CheckverionView, View.OnClickListener, LoginView, View.OnLayoutChangeListener, RadioGroup.OnCheckedChangeListener {
    View rootLayout;
    View bottomSpace;
    LinearLayout llBg;
    ImageView ivLogo;
    ImageView ivLogo2;
    LinearLayout llLoginContent;
    EditText etAccount;
    PwdEditText etPassword;
    View regView;
    RadioGroup radioGroup;
    TextView tvCodeHint;
    TextView tvForgetPassword;
    RelativeLayout rlPassword;
    RelativeLayout rlCode;
    ClearEditText etCode;
    Button btnGetCode;
    ImageView ivBack;
    TextView tvUserAgreement;
    TextView tvPrivacyAgreement;
    TextView tvAccount;
    CheckBoxSample checkBoxSample;

    private CountDownTimerUtils countDownTimerUtils;
    //软件盘弹起后所占高度阀值
    private int keyHeight = 0;
    private boolean IS_PSSWOORD_LOGIN = true;
    private boolean IS_DETSIL;
    public static final String LOGIN_DETAIL = "LOGIN_DETAIL";
    private boolean isCheckBox;
    private String IMEI;//设备唯一号
    CheckverionPresenter checkverionPresenter;
    private String updateTitle = "发现新版本";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        rootLayout = findViewById(R.id.root_layout);
        bottomSpace = findViewById(R.id.bottom_space);
        llBg = findViewById(R.id.ll_bg);
        ivLogo = findViewById(R.id.iv_logo);
        ivLogo2 = findViewById(R.id.iv_logo2);
        llLoginContent = findViewById(R.id.ll_longin_content);
        etAccount = findViewById(R.id.et_account);
        etPassword = findViewById(R.id.et_password);
        regView = findViewById(R.id.ll_register);
        radioGroup = findViewById(R.id.rg_random);
        tvCodeHint = findViewById(R.id.tv_code_hint);
        tvForgetPassword = findViewById(R.id.tv_forget_password);
        rlPassword = findViewById(R.id.rl_password);
        rlCode = findViewById(R.id.rl_code);
        etCode = findViewById(R.id.et_code);
        btnGetCode = findViewById(R.id.btn_get_code);
        ivBack = findViewById(R.id.iv_back);
        tvUserAgreement = findViewById(R.id.tv_user_agreement);
        tvPrivacyAgreement = findViewById(R.id.tv_privacy_agreement);
        tvAccount = findViewById(R.id.tv_account);
        checkBoxSample = findViewById(R.id.checkbox);
        findViewById(R.id.tv_forget_password).setOnClickListener(this);
        findViewById(R.id.ll_register).setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_get_code).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_user_agreement).setOnClickListener(this);
        findViewById(R.id.tv_privacy_agreement).setOnClickListener(this);
        findViewById(R.id.checkbox).setOnClickListener(this);
        initView();
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("2", "0");
//        mPresenter.checkVerion();
        IMEI = DeviceUtils.getAndroidID();
//        requestBasicPermission();
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,1,ivLogo);
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppManager.getAppManager().finishActivity(ReSetPasswordActivity.class);
        AppManager.getAppManager().finishActivity(RetrievePasswordActivity.class);
        AppManager.getAppManager().finishActivity(RegisterActivity.class);
    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
        rootLayout.addOnLayoutChangeListener(this);
        etPassword.setShowAnimate(false);
        regView.setVisibility(Config.OPEN_REGISTRATION ? View.VISIBLE : View.GONE);
        radioGroup.setOnCheckedChangeListener(this);

        tvUserAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvPrivacyAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        if (!checkBoxSample.isChecked()) {
            checkBoxSample.toggle();
            isCheckBox = true;
        }

    }

    private void initData() {
        int screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
//        keyHeight = screenHeight / 3;
        //系统自带输入法, 密码输入框比文字输入框矮一点, 正好卡在差一点到1/3的位置(这个差的高度不同尺寸屏幕不一样)
        //华为部分机型开启全屏模式时, 显示虚拟键会将窗口顶起一点...祖传大黑边应该也不能超过1/4吧..
        keyHeight = screenHeight / 4;
        String lastLoginAccount = SPManager.getLastLoginAccount();
        if (!TextUtils.isEmpty(lastLoginAccount)) {
//            etAccount.setText(lastLoginAccount);
        }
//        SPManager.changeLogin(mContext, false);
        IS_DETSIL = getIntent().getBooleanExtra(LOGIN_DETAIL, false);
        requestBasicPermission();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right,
                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        //old是改变前的左上右下坐标点值，没有old的是改变后的左上右下坐标点值
        //现在认为只要控件将Activity向上推的高度超过了1/3屏幕高，就认为软键盘弹起
//        LogUtils.i("top = " + top + "\nbottom = " + bottom + "\noldTop = " + oldTop + "\noldBottom = " + oldBottom);
        if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
            bottomSpace.setVisibility(View.GONE);
            llBg.setVisibility(View.GONE);
            ivLogo.setVisibility(View.INVISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_trans);
        } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
            //监听到软件盘关闭
            bottomSpace.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.VISIBLE);
            llBg.setVisibility(View.VISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo_trans);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_bg);
        }
//        bottomSpace.requestLayout();
    }

    public static final String INTENT_WEB = "userType";

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_forget_password) {//                IntentController.toRetrievePwd(mContext);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.RetrievePasswordActivity");//RetrievePasswordActivity
            startActivity(intent);
        } else if (id == R.id.btn_login) {
            if (IS_PSSWOORD_LOGIN) {
                doLogin();
            } else {
                doLoginCode();
            }
        } else if (id == R.id.ll_register) {
            if (!Config.OPEN_REGISTRATION) {
                return;
            }
        } else if (id == R.id.btn_get_code) {
            getVerifyCode();
        } else if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.tv_user_agreement) {//                IntentController.toWeb(mContext, "1");
            Intent changeNameIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");// WebActivity.class
            changeNameIntent.putExtra(INTENT_WEB, "1");
            startActivity(changeNameIntent);
        } else if (id == R.id.tv_privacy_agreement) {
            Intent changeNameIntent1 = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");// WebActivity.class
            changeNameIntent1.putExtra(INTENT_WEB, "2");
            startActivity(changeNameIntent1);
//                IntentController.toWeb(mContext, "2");
        } else if (id == R.id.checkbox) {
            checkBoxSample.toggle();
            if (checkBoxSample.isChecked()) {
                isCheckBox = true;
            } else {
                isCheckBox = false;
            }
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        KeyboardUtils.hideSoftInput(this);
        etAccount.setText("");
        if (i == R.id.rb_password) {//密码登录
            IS_PSSWOORD_LOGIN = true;
            tvForgetPassword.setVisibility(View.VISIBLE);
            tvCodeHint.setVisibility(View.INVISIBLE);
            rlPassword.setVisibility(View.VISIBLE);
            rlCode.setVisibility(View.GONE);
            etAccount.setHint(getString(R.string.login_acount_hint));
            tvAccount.setText("账号");
            etPassword.setText("");
            etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        } else if (i == R.id.rb_code) {
            IS_PSSWOORD_LOGIN = false;
            tvForgetPassword.setVisibility(View.INVISIBLE);
            tvCodeHint.setVisibility(View.VISIBLE);
            rlPassword.setVisibility(View.GONE);
            rlCode.setVisibility(View.VISIBLE);
            etAccount.setHint(getString(R.string.login_acount_hint1));
            tvAccount.setText("手机号");
            etCode.setText("");
            etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
        }
    }


    public void doLogin() {
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号或账号");
        } else if (TextUtils.isEmpty(password)) {
            ToastUtils.showShort("请输入密码");
        } else {
            mPresenter.login(account, password, IMEI);
        }


    }

    /**
     * 验证码登录
     */
    public void doLoginCode() {
        String account = etAccount.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(account)) {
            ToastUtils.showShort("手机号格式错误");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            mPresenter.loginCode(account, code, IMEI);
        }

    }

    private void getVerifyCode() {
        String phoneNo = etAccount.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            countDownTimerUtils.start();
            mPresenter.getVerifyCode(phoneNo);
        }
    }

    public static final String AUTO_LOGIN = "autoLogin";
    public static final String IS_MAIN_IN = "to_in_subject";   //
    public static final String IS_UPDATE = "isUpdate";   //
    public static final String IS_DETAILS = "isDetails";   // 是否从详情页进入

    @Override
    public void loginSuccess(UserLoginBean loginBean) {
        UserBean userBean = loginBean.getUserDetail();
        SPToken.saveToken(loginBean.getAccess_token());
        SPManager.saveUser(loginBean.getUserDetail());
        SPManager.saveLastLoginAccount(etAccount.getText().toString().trim());
        SPManager.savePwd(etPassword.getText().toString().trim());

        SPManager.changeLogin(mContext, true);
        if (0 != userBean.getSubjectId() || 0 != userBean.getGrade()) {
            SPManager.saveSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());

            SPManager.saveSchoolSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());

            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
            if (IS_DETSIL) {
                this.finish();
            } else {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
                intent.putExtra(AUTO_LOGIN, false);
                startActivity(intent);
            }
        } else {
            //          IntentController.toAccountSetting(mContext);
            //添加两参数  1 从哪跳的  2（带学段？） 3.是否从详情过来的
            if (IS_DETSIL) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
                startIntent.putExtra(IS_MAIN_IN, false);
                startIntent.putExtra(IS_UPDATE, true);
                startIntent.putExtra(IS_DETAILS, true);
                startActivity(startIntent);
//                IntentController.toSelectSubject(mContext, false, true, true);
                this.finish();
            } else {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
                startIntent.putExtra(IS_MAIN_IN, false);
                startIntent.putExtra(IS_UPDATE, true);
                startIntent.putExtra(IS_DETAILS, false);
                startActivity(startIntent);
//                IntentController.toSelectSubject(mContext, false, true, false);
                this.finish();
            }
        }
    }


    @Override
    public void loginFailure(String msg) {
        ToastUtils.showShort(msg);
    }

//    private ProgressDialog pd;
//    private DownloadManager manager;
//    private String updateContent = "1、Kotlin重构版\n2、支持自定义UI\n3、增加md5校验\n4、更多功能等你探索";
//    private String updateTitle = "发现新版本";

/*
    @Override
    public void updateVersion(String updateInfo, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudenthd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂HD");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();


        // 需要更新，弹出更新窗口
      */
/*  UpdateDialog.Builder builder = new UpdateDialog.Builder(LoginActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(LoginActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(LoginActivity.this);
                manager.setApkName("拼课堂.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.live.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();*//*

    }
*/

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        ToastUtils.showShort(msg);
    }


    public static final int BAIJIAYUN_CODE = 10011;

    private void requestBasicPermission() {
        MPermission.with(LoginActivity.this)
                .addRequestCode(BAIJIAYUN_CODE)
                .permissions(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(BAIJIAYUN_CODE)
    public void onBasicPermissionSuccess() {
    }

    @OnMPermissionDenied(BAIJIAYUN_CODE)
    public void onBasicPermissionFailed() {
    }

    @Override
    public void onBackPressed() {
        if (IS_DETSIL) {
            this.finish();
        } else {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
            intent.putExtra(AUTO_LOGIN, false);
            startActivity(intent);
//            IntentController.toMain(mContext, false);
        }
//            AppUtils.exitApp(mContext);
    }


    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = App2Utils.getAppVersionCode(App2.get());//获取版本号
            int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
            if (updateVersion > currVersion) {
                if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                    Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
                }
            }
        }
    }

    private void Updatemethod(String description, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudenthd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂HD");//app名称
        String count = description.replace("|", "\n");
        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
//        ToastUtils.showLong(this, bean);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
//        ToastUtils.showLong(this, msg);
    }

    @Override
    protected void onDestroy() {
        checkverionPresenter.onDestory();
        super.onDestroy();
    }

}
