package com.example.app3home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app3home.R;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.fragment.presenter.ReimbursePresenter;
import com.example.app3libpublic.fragment.view.ReimburseView;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.ReimburseBean;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * 描述：退课申请界面
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/7
 */

public class ReimburseActivity extends BaseMVPActivity<ReimburseView, ReimbursePresenter> implements ReimburseView, View.OnClickListener {
    TitleBar titleBar;
    EditText etReason;
    Button btConfirm;
    LinearLayout llReason;
    TextView btwrite;
    private int orderId;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reimburse;
    }

    @Override
    protected void onInit(Bundle bundle) {
        orderId = getIntent().getIntExtra("orderId", -1);
        titleBar = findViewById(R.id.title_bar);
        etReason = findViewById(R.id.et_reason);
        btConfirm = findViewById(R.id.bt_confirm);
        llReason = findViewById(R.id.ll_reason);
        btwrite = findViewById(R.id.bt_write);
        btConfirm.setOnClickListener(this);
        btwrite.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.bt_write) {
            llReason.setVisibility(View.VISIBLE);
        } else if (id == R.id.bt_confirm) {
            String reason = etReason.getText().toString().trim();
            if (TextUtils.isEmpty(reason)) {
                ToastUtils.showShort("请填写退款理由");
                return;
            }
            mPresenter.dropOut(orderId, reason);
        }
    }



    @Override
    protected ReimbursePresenter createPresenter() {
        return new ReimbursePresenter();
    }

    @Override
    public void dropSuccess(Object o) {
        ToastUtils.showShort("已申请，请耐心等待");
        EventBus.getDefault().post(new OrderPayEvent(false));
        finish();
    }


    @Override
    public void dropError(String msg) {
        ToastUtils.showShort(msg);
    }

}
