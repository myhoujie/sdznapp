package com.example.app3home.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3grzx.activity.WebTaskPadActivity;
import com.example.app3grzx.adapter.MineTaskAdapter;
import com.example.app3grzx.presenter.MineTaskPresenter;
import com.example.app3grzx.view.MineTaskView;
import com.example.app3home.R;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.EmptySchoolLayout;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.bean.MineTaskBean;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.just.agentweb.App2;
import com.just.agentweb.LocalBroadcastManagers;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.fzx.student.libutils.util.Log;

import java.util.ArrayList;
import java.util.List;


/**
 * 描述：我的作业两个模块
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class TaskPageFragment extends BaseMVPFragment<MineTaskView, MineTaskPresenter> implements MineTaskView, BaseRcvAdapter.OnItemClickListener, OnRefreshLoadmoreListener {
    public static final String ARGS_TYPE = "args_page";
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    EmptyLayout emptyLayout;
    RecyclerView recyclerView;
    SmartRefreshLayout refreshLayout;
    private MineTaskAdapter mineTaskAdapter;
    private List<MineTaskBean.RecordsBean> mDataMyCourse = new ArrayList<>();
    private String state;
    private int pageIndex = 1;//当前页
    private int pageSize = 5;//
    private String studentid, AppToken;
    private List<MineTaskBean.RecordsBean> Records;
    private MessageReceiverIndex mMessageReceiver;

    public class MessageReceiverIndex extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if ("WebViewPadEvent".equals(intent.getAction())) {
                    String AnsweringState = intent.getStringExtra("typestate");
                    if (AnsweringState.equals(TYPE_TODY)) {
                        Log.e("ttttaaaaaa", "enbus回调未作答");
                        lazyLoad();
                    } else {
                        Log.e("ttttaaaaaa", "enbus回调已作答");
                        lazyLoad();
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            state = getArguments().getString(ARGS_TYPE);
        }
    }


    public static TaskPageFragment newInstance(String page) {
        Bundle args = new Bundle();
        args.putString(ARGS_TYPE, page);
        TaskPageFragment fragment = new TaskPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_task;

    }

    @Override
    protected MineTaskPresenter createPresenter() {
        return new MineTaskPresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        recyclerView = rootView.findViewById(R.id.swipe_target);
        refreshLayout = rootView.findViewById(R.id.refresh_layout);
        studentid = SPManager.getUser().getUserId();
        AppToken = SPToken.getToken();
        mMessageReceiver = new MessageReceiverIndex();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction("WebViewPadEvent");
        LocalBroadcastManagers.getInstance(App2.get()).registerReceiver(mMessageReceiver, filter);
        initView();
    }

    @Override
    protected void lazyLoad() {
        pageIndex = 1;
        initData();
        isFirst = false;
    }

//    //更新作业
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void setWebViewAvatarEvent(WebViewAvatarEvent webViewAvatarEvent) {
//        if (webViewAvatarEvent.getAnsweringState().equals(TYPE_TODY)) {
//            Log.e("ttttaaaaaa", "enbus回调未作答");
//            lazyLoad();
//        } else {
//            Log.e("ttttaaaaaa", "enbus回调已作答");
//            lazyLoad();
//        }
//    }

    private void initData() {
        if (SPToken.autoLogin(mContext)) {
            switch (state) {
                case TYPE_TODY:
                    mPresenter.getCourse(studentid, "0", pageSize, pageIndex);//getParms()
                    break;
                case TYPE_RECENTLY:
                    mPresenter.getCourse(studentid, "3", pageSize, pageIndex);//getParms()
                    break;
                default:
                    break;
            }
        }

    }

    private void initView() {
        switch (state) {
            case TYPE_TODY:
                mineTaskAdapter = new MineTaskAdapter(mContext, mDataMyCourse, TYPE_TODY);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.setAdapter(mineTaskAdapter);
                mineTaskAdapter.setOnItemClickListener(this);
                break;
            case TYPE_RECENTLY:
                mineTaskAdapter = new MineTaskAdapter(mContext, mDataMyCourse, TYPE_RECENTLY);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.setAdapter(mineTaskAdapter);
                mineTaskAdapter.setOnItemClickListener(this);
                break;
            default:
                break;
        }

        refreshLayout.setOnRefreshLoadmoreListener(this);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lazyLoad();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        //判断当前的item是点播还是直播的
        switch (state) {
            case TYPE_TODY://未作答
                mPresenter.getValidate(mDataMyCourse.get(position).getHomeworkId(), studentid, "0", "pad", TYPE_TODY);
                break;
            case TYPE_RECENTLY://已作答
                mPresenter.getValidate(mDataMyCourse.get(position).getHomeworkId(), studentid, "3", "pad", TYPE_RECENTLY);
                break;
            default:
                break;
        }
    }


    /*跳转到webview*/
    @Override
    public void validatetime(Object url, String AnsweringState) {
//        if (AnsweringState.equals(TYPE_TODY)){
//            EventBus.getDefault().post(new WebViewAvatarEvent(String.valueOf(url),AnsweringState,AppToken));
//        }else{
//            EventBus.getDefault().post(new WebViewAvatarEvent(String.valueOf(url),AnsweringState,AppToken));
//        }
//        if (AnsweringState.equals(TYPE_TODY)){
//            showFragmentAddAnimation(R.id.rl_homework, TaskWebFragment.newInstance(String.valueOf(url),AnsweringState,AppToken), true);
//        }else{
//            showFragmentAddAnimation(R.id.rl_homework, TaskWebFragment.newInstance(String.valueOf(url),AnsweringState,AppToken), true);
//        }
//        EventBus.getDefault().post(new WebViewAvatarEvent(String.valueOf(url),AnsweringState));

        String validateurl = String.valueOf(url);
//        HiosHelperzb.resolveAd(getActivity(), getActivity(), "hios://com.sdzn.live.tablet.hs.act.WebTaskPadActivity{act}?AppToken={s}" + AppToken + "&AnsweringState={s}" + AnsweringState + "&validateurl={s}" + validateurl);
        Intent intent = new Intent(getActivity(), WebTaskPadActivity.class);
        if (AnsweringState.equals(TYPE_TODY)) {
            intent.putExtra("AppToken", AppToken);
            intent.putExtra("validateurl", validateurl);
            intent.putExtra("AnsweringState", AnsweringState);
        } else {
            intent.putExtra("AppToken", AppToken);
            intent.putExtra("validateurl", validateurl);
            intent.putExtra("AnsweringState", AnsweringState);
        }
        startActivity(intent);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        lazyLoad();
    }


    @Override
    public void listCourseSuccess(List<MineTaskBean.RecordsBean> mineTaskBean) {
        Records = mineTaskBean;
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataMyCourse.clear();
            if (mineTaskBean.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(mineTaskBean.size() < pageSize);
        this.mDataMyCourse.addAll(mineTaskBean);
        mineTaskAdapter.notifyDataSetChanged();
    }

    @Override
    public void listCourseEmpty() {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            refreshLayout.setLoadmoreFinished(true);
        }
        clearLoingState();
    }

    @Override
    public void listCourseError(String msg) {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
        clearLoingState();
    }

    //隐藏刷新布局或者底部加载更多的布局
    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManagers.getInstance(App2.get()).unregisterReceiver(mMessageReceiver);
//        EventBus.getDefault().unregister(this);
    }

}