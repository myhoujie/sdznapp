package com.example.app3home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.fragment.SelectFiveFragment;
import com.example.app3home.fragment.SelectSixFragment;
import com.example.app3home.persenter.SelectSubjectPresenter;
import com.example.app3home.view.SelectSubjectView;
import com.example.app3libpublic.adapter.SpellingTitleAdapter;
import com.example.app3libpublic.listener.SendSelectData;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libpublic.widget.pager.PagerSlidingTabStrip;
import com.example.app3libvariants.bean.GradeJson;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


public class SelectSubjectActivity extends  BaseMVPActivity<SelectSubjectView, SelectSubjectPresenter> implements SelectSubjectView, SendSelectData {
    public static final String IS_MAIN_IN = "to_in_subject";   //
    public static final String IS_UPDATE = "isUpdate";   //
    public static final String IS_DETAILS = "isDetails";   // 是否从详情页进入
    private boolean isMainIn = false;
    private boolean isUpdate = false;
    private boolean isDetails = false;
    private SpellingTitleAdapter titleAdapter;
    private List<Fragment> list_fragment;//定义要装fragment的列表
    private ArrayList<String> listTitle;

    TitleBar titleBar;
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    ViewPager vp;
    public static final String AUTO_LOGIN = "autoLogin";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_select_subject;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar = findViewById(R.id.title_bar);
        mPagerSlidingTabStrip = findViewById(R.id.tabs);
        vp = findViewById(R.id.vp_select);

        isMainIn = getIntent().getBooleanExtra(IS_MAIN_IN, false);
        isUpdate = getIntent().getBooleanExtra(IS_UPDATE, false);
        isDetails = getIntent().getBooleanExtra(IS_DETAILS, false);
        initView();
    }

    @Override
    protected SelectSubjectPresenter createPresenter() {
        return new SelectSubjectPresenter();
    }

    private void initView() {
        if (isMainIn) {
            titleBar.setVisibility(View.VISIBLE);
        } else {
            titleBar.setVisibility(View.INVISIBLE);
        }
        listTitle = new ArrayList<>();
        listTitle.add("六三制");
        listTitle.add("五四制");
        titleAdapter = new SpellingTitleAdapter(getSupportFragmentManager());

        list_fragment = new ArrayList<>();
        list_fragment.add(SelectSixFragment.newInstance());
        list_fragment.add(SelectFiveFragment.newInstance());
        titleAdapter.setmDatas(listTitle, list_fragment);

        vp.setAdapter(titleAdapter);
        mPagerSlidingTabStrip.setViewPager(vp);
        vp.setOffscreenPageLimit(2);
        if (SPManager.getUser().getEducationId() > 1) {
            vp.setCurrentItem(1);
        }
    }

    private void initData() {

    }

    @Override
    public void setSuccess() {
        if (isDetails) {//从课程页进来的
            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
            this.finish();
        } else {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
            intent.putExtra(AUTO_LOGIN, false);
            startActivity(intent);
        }
    }

    @Override
    public void onFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onBackPressed() {
        if (isMainIn) {//主页
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
            intent.putExtra(AUTO_LOGIN, false);
            startActivity(intent);
        } else {
            App2Utils.exitApp(mContext);
        }
    }


    @Override
    public void send(String eduId, GradeJson.ChildListBean dataBean) {
        if (isUpdate && SPToken.autoLogin(mContext)) {//toc第一次登录且 学段没数据时 用
            String eduName = "";
            if ("0".equals(eduId)) {
                eduName = "无学制";
            } else if ("1".equals(eduId)) {
                eduName = "六三制";
            } else if ("2".equals(eduId)) {
                eduName = "五四制";
            }
            mPresenter.setSubAndGrade(eduId, eduName, dataBean.getSectionId(), dataBean.getSectionName(), dataBean.getGradeId(), dataBean.getGradeName());
            //第一次数据存  基本信息取
            SPManager.getUser().setGrade(Integer.valueOf(dataBean.getGradeId()));
            SPManager.getUser().setGradeName(dataBean.getGradeName());
            SPManager.getUser().setSubjectId(Integer.valueOf(dataBean.getSectionId()));
            SPManager.getUser().setSubjectName(dataBean.getSectionName());
        } else {
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
            intent.putExtra(AUTO_LOGIN, false);
            startActivity(intent);
        }
    }
}

