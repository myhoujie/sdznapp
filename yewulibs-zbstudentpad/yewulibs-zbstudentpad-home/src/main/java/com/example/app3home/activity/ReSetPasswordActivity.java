package com.example.app3home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.persenter.ChangePwdPresenter;
import com.example.app3home.view.ChangePwdView;
import com.example.app3libpublic.widget.PwdEditText;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;

/**
 * 描述：
 * - 修改密码-》重置
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class ReSetPasswordActivity extends BaseMVPActivity<ChangePwdView, ChangePwdPresenter> implements View.OnClickListener, ChangePwdView {

    PwdEditText etNewPassword;
    PwdEditText etPasswordCertain;
    ImageView ivBack;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected ChangePwdPresenter createPresenter() {
        return new ChangePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        etNewPassword = findViewById(R.id.et_new_password);
        etPasswordCertain = findViewById(R.id.et_password_certain);
        ivBack = findViewById(R.id.iv_back);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.btn_certain).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            onBackPressed();
        } else if (id == R.id.btn_certain) {
            String newPwd = etNewPassword.getText().toString().trim();
            String confirmPwd = etPasswordCertain.getText().toString().trim();
            mPresenter.confirm(newPwd, confirmPwd);
        }
    }

    @Override
    public void changeSuccess() {
        SPToken.saveToken("");
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
        startActivity(intent);
//        IntentController.toLogin(mContext);
    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);
    }


}
