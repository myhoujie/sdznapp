package com.example.app3home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.persenter.RetrievePwdPresenter;
import com.example.app3home.view.RetrievePwdView;
import com.example.app3libpublic.utils.CountDownTimerUtils;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;

/**
 * 描述：
 * - 找回密码
 * 创建人：baoshengxiang
 * 创建时间：2017/7/8
 */
public class RetrievePasswordActivity extends BaseMVPActivity<RetrievePwdView, RetrievePwdPresenter> implements View.OnClickListener, RetrievePwdView {

    EditText etPhone;
    EditText etCode;
    Button btnGetCode;
    ImageView ivBack;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_retrieve_password;
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,0,findViewById(R.id.iv_logo));
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    protected RetrievePwdPresenter createPresenter() {
        return new RetrievePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        etPhone = findViewById(R.id.et_phone);
        etCode = findViewById(R.id.et_code);
        btnGetCode = findViewById(R.id.btn_get_code);
        ivBack = findViewById(R.id.iv_back);
        findViewById(R.id.btn_get_code).setOnClickListener(this);
        findViewById(R.id.btn_certain).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_find).setOnClickListener(this);
        initView();
    }

    private void initData() {

    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_code) {
            getVerifyCode();
        } else if (id == R.id.btn_certain) {
            confirmVerifyCode();
        } else if (id == R.id.iv_back) {
            onBackPressed();
        } else if (id == R.id.tv_find) {//                IntentController.toRegister(mContext);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.RegisterActivity");//RegisterActivity
            startActivity(intent);
        }

    }

    private void confirmVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("验证码不能为空");
        } else {
            mPresenter.confirmVerifyCode(phoneNo, code);
        }
    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (phoneNo.length() != 11) {
            ToastUtils.showShort("请输入正确的手机号");
        } else {
            mPresenter.getVerifyCode(phoneNo);
            countDownTimerUtils.start();
        }
    }

    @Override
    public void onPause() {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        super.onPause();
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        ToastUtils.showShort(msg);
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
    }

    @Override
    public void confirmCodeSuccess() {
//        IntentController.toResetPassword(mContext);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ReSetPasswordActivity");//ReSetPasswordActivity
        startActivity(intent);
    }

    @Override
    public void confirmCodeFailure(String msg) {
        ToastUtils.showShort(msg);
    }


}
