package com.example.app3home.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.app3home.R;
import com.example.app3home.webceshi.BaseAgentWebFragment;
import com.example.app3libpublic.widget.TitleBar;
import com.sdzn.core.utils.ToastUtils;

/**
 * 课程统计
 *
 * @author Reisen at 2017-12-06
 */

public class MineStatisticsFragment extends BaseAgentWebFragment {
    private ViewGroup mViewGroup;
    private TitleBar titleBar;


    public static MineStatisticsFragment newInstance() {
        return new MineStatisticsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return mViewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_agentweb, container, false);
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.mViewGroup.findViewById(R.id.linearLayout);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }


    protected void initView(View view) {
        titleBar = view.findViewById(R.id.title);
        getUrl();
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 10) {
                title = title.substring(0, 10).concat("...");
            }
        }
        titleBar.setTitleText(title);
    }

    @Nullable
    @Override
    protected String getUrl() {
        return "https://www.baidu.com/";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToastUtils.showShort("123123123");
        mAgentWeb.getWebLifeCycle().onDestroy();
        mAgentWeb.clearWebCache();//清楚缓存
    }
}