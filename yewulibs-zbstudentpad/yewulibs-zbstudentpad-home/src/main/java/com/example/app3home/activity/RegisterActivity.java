package com.example.app3home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.persenter.RegisterPresenter;
import com.example.app3home.view.RegisterView;
import com.example.app3libpublic.utils.CountDownTimerUtils;
import com.example.app3libpublic.utils.VerifyUtil;
import com.example.app3libpublic.widget.ClearEditText;
import com.example.app3libpublic.widget.PwdEditText;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * 找回密码  没有手机号的去情况下
 */
public class RegisterActivity extends BaseMVPActivity<RegisterView, RegisterPresenter> implements View.OnClickListener, RegisterView {

    ClearEditText etAccount;
    EditText etPhone;
    EditText etCode;
    PwdEditText etPassword;
    Button btnGetCode;
    ImageView ivBack;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        etAccount = findViewById(R.id.et_account);
        etPhone = findViewById(R.id.et_phone);
        etCode = findViewById(R.id.et_code);
        etPassword = findViewById(R.id.et_password);
        btnGetCode = findViewById(R.id.btn_get_code);
        ivBack = findViewById(R.id.iv_back);
        findViewById(R.id.btn_get_code).setOnClickListener(this);
        findViewById(R.id.btn_certain).setOnClickListener(this);

//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (VerifyUtil.isMobileNO(etPhone.getText().toString())) {
//                        etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                    } else {
//                        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                            etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.red));
//                            etPhone.setError("手机号格式不正确");
//                        }
//                    }
//                } else {
//                    etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                }
//            }
//        });
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_code) {
            getVerifyCode();
        } else if (id == R.id.btn_certain) {
            register();
        } else if (id == R.id.iv_back) {
            onBackPressed();
        }

    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("请输入正确的手机号");
        } else {
            countDownTimerUtils.start();
            mPresenter.getVerifyCode(phoneNo);
        }
    }

    private void register() {
        String account = etAccount.getText().toString().trim();
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("账号不能为空");
        } else if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("验证码不能为空");
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("account", account);
            params.put("phone", phoneNo);
            params.put("code", code);
//            params.put("password", password);
            mPresenter.retrievePassword(account,phoneNo,code);
        }
    }


    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        ToastUtils.showShort(msg);
    }

    @Override
    public void registerSuccess() {
        String phoneNo = etPhone.getText().toString().trim();
        SPManager.saveLastLoginAccount(phoneNo);
        ToastUtils.showShort("注册成功，请登录");
//        IntentController.toLogin(mContext);
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
        startActivity(intent);
    }

    @Override
    public void registerFailure(String msg) {
        ToastUtils.showShort(msg);
    }


}
