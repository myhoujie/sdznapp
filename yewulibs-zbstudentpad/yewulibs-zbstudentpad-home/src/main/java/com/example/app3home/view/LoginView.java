package com.example.app3home.view;

import com.example.app3libvariants.bean.UserLoginBean;
import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface LoginView extends BaseView {

    void loginSuccess(UserLoginBean accountBean);

    void loginFailure(String msg);

//    void updateVersion(String updateInfo, String targetUrl);

    void getCodeSuccess();

    void getCodeFailure(String msg);
}
