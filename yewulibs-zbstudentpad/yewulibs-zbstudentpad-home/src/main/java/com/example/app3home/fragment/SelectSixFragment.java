package com.example.app3home.fragment;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3home.R;
import com.example.app3home.adapter.y_recycleradapter.GeneralRecyclerViewHolder;
import com.example.app3home.adapter.y_recycleradapter.Y_ItemEntityList;
import com.example.app3home.adapter.y_recycleradapter.Y_MultiRecyclerAdapter;
import com.example.app3home.adapter.y_recycleradapter.Y_OnBind;
import com.example.app3libpublic.listener.OnItemTouchListener;
import com.example.app3libpublic.listener.SendSelectData;
import com.example.app3libpublic.utils.GradeIdToNameUtils;
import com.example.app3libvariants.bean.GradeJson;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SelectSixFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    SendSelectData sendSelectData;
    private Y_ItemEntityList itemEntityList = new Y_ItemEntityList();

    private Y_MultiRecyclerAdapter gradeAdapter;

    RecyclerView rv;
    private int fCount = 0;
    private int sCount = 0;

    private int eduId = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sendSelectData = (SendSelectData) getActivity();
    }

    public SelectSixFragment() {
        // Required empty public constructor
    }

    public static SelectSixFragment newInstance() {
        SelectSixFragment fragment = new SelectSixFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_select;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        rv = rootView.findViewById(R.id.swipe_target);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 6);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == fCount || position == sCount) {
                    return 6;
                }
                return 1;
            }
        });
        rv.setLayoutManager(gridLayoutManager);
        gradeAdapter = new Y_MultiRecyclerAdapter(mContext, itemEntityList);
        rv.setAdapter(gradeAdapter);
        rv.addOnItemTouchListener(new OnItemTouchListener(rv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                int position = vh.getAdapterPosition();
                if (position < 0 || position >= itemEntityList.getItemCount()) {
                    return;
                }
                Object itemData = itemEntityList.getItemData(vh.getAdapterPosition());
                for (int i = 0; i < itemEntityList.getItemCount(); i++) {
                    if (itemEntityList.getItems().get(i) instanceof GradeJson.ChildListBean) {
                        ((GradeJson.ChildListBean) itemEntityList.getItems().get(i)).setChecked(false);
                    }
                }

                if (itemData instanceof GradeJson.ChildListBean) {

                    GradeJson.ChildListBean dataBean = (GradeJson.ChildListBean) itemData;
                    dataBean.setChecked(true);
                    gradeAdapter.notifyDataSetChanged();

                    SPManager.setFirstSubjectId(mContext);//本地 第一次选年级
                    SPManager.saveSection(Integer.valueOf(dataBean.getSectionId()), dataBean.getSectionName(), 1);
                    SPManager.saveGrade(Integer.valueOf(dataBean.getGradeId()), dataBean.getGradeName());

                    EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));

                   /*
                   往activity传数据
                    */
                    if (Integer.valueOf(dataBean.getSectionId()) < 3) {
                        eduId = 1;//学制
                    } else {
                        eduId = 0;
                    }
                    sendSelectData.send(String.valueOf(eduId), dataBean);
                }


            }
        });

        onSuccess(GradeIdToNameUtils.getSixGrade());
    }


    private void onSuccess(List<GradeJson> gradeJson) {
        List<Integer> listSize = new ArrayList<>();
        int i = 0, utilInt;
        for (int a = 0; a < gradeJson.size(); a++) {
            itemEntityList.addItem(R.layout.item_recycler_select_title, gradeJson.get(a).getLevelName())
                    .addOnBind(R.layout.item_recycler_select_title, new Y_OnBind() {
                        @Override
                        public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                            holder.setText(R.id.tvDecoration, (String) itemData);
                        }
                    });
            utilInt = 0;//判断是否一行为3个
            for (GradeJson.ChildListBean data : gradeJson.get(a).getChildList()) {
                data.setSectionId(gradeJson.get(a).getLevelId());
                data.setSectionName(gradeJson.get(a).getLevelName());
                if (String.valueOf(SPManager.getSectionId()).equals(data.getSectionId().trim()) &&
                        String.valueOf(SPManager.getgradeId()).equals(data.getGradeId().trim())) {
                    data.setChecked(true);
                }
                itemEntityList.addItem(R.layout.item_recycler_select_content, data)
                        .addOnBind(R.layout.item_recycler_select_content, new Y_OnBind() {
                            @Override
                            public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                bindHolder(holder, (GradeJson.ChildListBean) itemData);
                            }
                        });
                i++;
                utilInt++;
            }
            if (utilInt % 6 > 0) {
                int addInt = 6 - (utilInt % 6);
                for (int c = 0; c < addInt; c++) {
                    itemEntityList.addItem(R.layout.item_recycler_select_content_s, "")
                            .addOnBind(R.layout.item_recycler_select_content_s, new Y_OnBind() {
                                @Override
                                public void onBindChildViewData(GeneralRecyclerViewHolder holder, Object itemData, int position) {
                                }
                            });
                    i++;
                }
            }

            listSize.add(i);

        }
        if (listSize.size() > 1) {
            fCount = listSize.get(0) + 1;
            sCount = listSize.get(1) + 2;
        }

        gradeAdapter.notifyDataSetChanged();


    }


    public void bindHolder(GeneralRecyclerViewHolder holder, GradeJson.ChildListBean childListBean) {
        holder.setText(R.id.tv, childListBean.getGradeName());
        if (childListBean.isChecked()) {
            ((TextView) holder.getChildView(R.id.tv)).setEnabled(true);
        } else {
            ((TextView) holder.getChildView(R.id.tv)).setEnabled(false);
        }

    }


}
