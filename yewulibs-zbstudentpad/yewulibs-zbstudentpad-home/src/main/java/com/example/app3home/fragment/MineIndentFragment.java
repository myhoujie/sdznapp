package com.example.app3home.fragment;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.androidkun.xtablayout.XTabLayout;
import com.example.app3home.R;
import com.example.app3home.adapter.MineIndentFragmentPagerAdapter;
import com.example.app3libpublic.widget.NoScrollViewPager;
import com.example.app3libpublic.widget.TitleBar;
import com.sdzn.core.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的订单
 *
 * @author Reisen at 2017-12-06
 */

public class MineIndentFragment extends BaseFragment {
    TitleBar mTitleBar;
    XTabLayout tablIndent;
    NoScrollViewPager vpIndent;
    MineIndentFragmentPagerAdapter fragmentAdapter;

    private List<IndentFragment> list_fragment;//定义要装fragment的列表
    private final String TAG_SUCCESS = "INIT";//已完成
    private final String TAG_INIT = "SUCCESS";//未完成-- 待付款
    private final String TAG_CANCEL = "CANCEL";//已取消
    private final String TAG_REFUND = "REFUND";//已退款
    private final String TAG_ALL = "ALL";//空时为全部数据

    public static MineIndentFragment newInstance() {
        return new MineIndentFragment();
    }

    private boolean isRefresh = false;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_indent;
    }

    @Override
    protected void onInit(Bundle bundle) {
        mTitleBar = rootView.findViewById(R.id.title_bar);
        tablIndent = rootView.findViewById(R.id.tabl_indent);
        vpIndent = rootView.findViewById(R.id.vp_indent);
        initView();
        initData();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initData() {

    }

    private void initView() {
        mTitleBar.setLeftClickListener(null);
        list_fragment = new ArrayList<>();
        list_fragment.add(IndentFragment.newInstance(TAG_ALL));
        list_fragment.add(IndentFragment.newInstance(TAG_SUCCESS));
        list_fragment.add(IndentFragment.newInstance(TAG_INIT));
        list_fragment.add(IndentFragment.newInstance(TAG_CANCEL));
        list_fragment.add(IndentFragment.newInstance(TAG_REFUND));
        fragmentAdapter = new MineIndentFragmentPagerAdapter(getActivity().getSupportFragmentManager(),
                list_fragment, mContext);
        vpIndent.setAdapter(fragmentAdapter);
        vpIndent.setNoScroll(false);
        vpIndent.setOffscreenPageLimit(4);
        tablIndent.setupWithViewPager(vpIndent);
        list_fragment.get(0).regEventBus();
        vpIndent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < list_fragment.size(); i++) {
                    if (i == position) {
                        list_fragment.get(i).regEventBus();
                        continue;
                    }
                    list_fragment.get(i).unRegEventBus();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


}
