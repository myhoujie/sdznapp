package com.example.app3home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3home.adapter.ShoppingCartAdapter;
import com.example.app3home.persenter.ShoppingCartPresenter;
import com.example.app3home.view.ShoppingCartView;
import com.example.app3libpublic.event.OrderPayEvent;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.ShoppingCartBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * 描述：
 * - 购物车（无法多选结算）
 * 创建人：baoshengxiang
 * 创建时间：2017/7/20
 */
public class ShoppingCartActivity extends BaseMVPActivity<ShoppingCartView, ShoppingCartPresenter> implements View.OnClickListener, ShoppingCartView, com.scwang.smartrefresh.layout.listener.OnRefreshListener {

    SmartRefreshLayout refreshLayout;
    RecyclerView rcvShoppingCart;
    TitleBar titleBar;
    CheckBox cbAllCheck;
    TextView tvTotalPrice;
    Button btnSettlement;
    EmptyLayout emptyLayout;

    public static final String COURSE_TYPE = "courseType";
    public static final String COURSE_ID = "courseId";
    public static final String SHOW_LIVE_BTN = "showLiveBtn";
    public static final String PACKAGE = "PACKAGE";

    private ShoppingCartAdapter shoppingCartAdapter;
    private List<ShoppingCartBean.ShopCartListBean> shoppingBeans;
    private boolean isEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected ShoppingCartPresenter createPresenter() {
        return new ShoppingCartPresenter();
    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
        StatusBarUtil.statusBarLightMode(this);
    }

    @Override
    protected void onInit(Bundle bundle) {
        EventBus.getDefault().register(this);
        initData();
        refreshLayout = findViewById(R.id.refresh_layout);
        rcvShoppingCart = findViewById(R.id.swipe_target);
        titleBar = findViewById(R.id.title_bar);
        cbAllCheck = findViewById(R.id.cb_all_check);
        tvTotalPrice = findViewById(R.id.tv_total_price);
        btnSettlement = findViewById(R.id.btn_settlement);
        emptyLayout = findViewById(R.id.empty_layout);
        findViewById(R.id.btn_settlement).setOnClickListener(this);
        findViewById(R.id.cb_all_check).setOnClickListener(this);

        initView();
        queryCartData();
    }

    private void initData() {
        shoppingBeans = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdit = !isEdit;
                shoppingCartAdapter.setEdit(isEdit);
                if (isEdit) {
                    titleBar.setRightText("取消");
//                    cbAllCheck.setVisibility(View.VISIBLE);
                    tvTotalPrice.setVisibility(View.GONE);
                    btnSettlement.setText("删除");
                    btnSettlement.setBackgroundColor(getResources().getColor(R.color.red));
                } else {
                    titleBar.setRightText("编辑");
//                    cbAllCheck.setVisibility(View.GONE);
                    tvTotalPrice.setVisibility(View.VISIBLE);
                    btnSettlement.setText("支付");
                    btnSettlement.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        });

        refreshLayout.setOnRefreshListener(this);
        rcvShoppingCart.setLayoutManager(new GridLayoutManager(mContext, 4));
        shoppingCartAdapter = new ShoppingCartAdapter(mContext, shoppingBeans);
        rcvShoppingCart.setAdapter((RecyclerView.Adapter) shoppingCartAdapter);
        shoppingCartAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (shoppingCartAdapter.getEdit()) {
                    return;
                }
                ShoppingCartBean.ShopCartListBean course = shoppingBeans.get(position);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");//CourseDetailActivity
                intent.putExtra(PACKAGE,  course.getSellType());
                intent.putExtra(COURSE_ID, course.getCourseId());
                intent.putExtra(SHOW_LIVE_BTN, false);
                startActivity(intent);
//                IntentController.toCourseDetail(mContext, course.getSellType(), course.getCourseId(), false);
            }
        });

        shoppingCartAdapter.setCartCallback(new ShoppingCartAdapter.CartCallback() {
            @Override
            public void isSelectAll(boolean isSelectAll) {
                cbAllCheck.setChecked(isSelectAll);
            }

            @Override
            public void backTotalPrice(double price) {
                String totalPrice = String.format(Locale.getDefault(), "￥%.2f", price);
                SpannableString priceSpan = new SpannableString("合计：" + totalPrice);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textMinor)),
                        0, priceSpan.length() - totalPrice.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                        priceSpan.length() - totalPrice.length(), priceSpan.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                tvTotalPrice.setText(priceSpan);
            }
        });


        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                queryCartData();
            }
        });

    }

    private SpannableString splitString(String firstStr, int firstColor, String secStr, int secColor) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, firstColor)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, secColor)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void deleteShopping() {
        if (shoppingCartAdapter.getCheckedGoods() == null || shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请先选择要删除的物品");
            return;
        }
        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }

        mPresenter.deleteGoods(idSb.toString());
    }

    private void queryCartData() {
        mPresenter.queryShoppingCart();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        queryCartData();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_settlement) {
            if (isEdit) {
                deleteShopping();
            } else {
                tosettlement();
            }
        } else if (id == R.id.cb_all_check) {
            shoppingCartAdapter.setSelectAll(cbAllCheck.isChecked());
        }

    }

    /**
     * 去结算
     */
    private void tosettlement() {
        if (shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请至少选择一门课程去结算");
            return;
        }

//        OrderGoodsBean orderGoodsBean = new OrderGoodsBean();
//        orderGoodsBean.setTotalPrice(shoppingCartAdapter.getTotalPrice());
//        List<OrderGoodsBean.GoodsBean> goods = new ArrayList();
//        for (Map.Entry<String, Integer> entry : shoppingCartAdapter.getCheckedGoods().entrySet()) {
//            int checkPos = entry.getValue();
//            ShoppingCartBean.ShopCartListBean shopCartListBean = shoppingBeans.get(checkPos);
//            OrderGoodsBean.GoodsBean goodsBean = new OrderGoodsBean.GoodsBean();
//            goodsBean.setCourseId(shopCartListBean.getGoodsid());
//            goodsBean.setLogo(shopCartListBean.getCourse().getLogo());
//            goodsBean.setSellType(shopCartListBean.getCourse().getSellType());
//            goodsBean.setPackageType(shopCartListBean.getCourse().getPackageType());
//            goodsBean.setCourseName(shopCartListBean.getCourse().getCourseName());
//            goodsBean.setLessionNum(shopCartListBean.getCourse().getLessionNum());
//            goodsBean.setPrice(shopCartListBean.getCourse().getCurrentPrice());
//            goods.add(goodsBean);
//        }
//        orderGoodsBean.setGoodsBeanList(goods);
        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }
//        IntentController.toOrderSubmit(mContext, idSb.toString());
        Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.OrderSubmitActivity");//OrderSubmitActivity.class
        intent.putExtra("goods", idSb.toString());
        startActivity(intent);
    }

    private void clearRefreshStatus() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    @Override
    public void queryCartSuccess(List<ShoppingCartBean.ShopCartListBean> shoppingCartBeens) {
        clearRefreshStatus();
        titleBar.getView(R.id.tv_right).setVisibility(View.VISIBLE);
        this.shoppingBeans.clear();
        this.shoppingBeans.addAll(shoppingCartBeens);
        shoppingCartAdapter.notifyDataSetChanged();

        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
//        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(totalPrice), R.color.red));
        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(shoppingCartAdapter.getTotalPrice()), R.color.red));

        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void queryCartEmpty() {
        clearRefreshStatus();
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        titleBar.getView(R.id.tv_right).setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.NODATA);
        emptyLayout.setErrorMessage("当前购物车暂无课程");
    }

    @Override
    public void queryCartFailure(String msg) {
        clearRefreshStatus();
        ToastUtils.showShort(msg);
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void delGoodsSuccess() {
        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
        shoppingCartAdapter.removeGoods();
        shoppingCartAdapter.setTotalPrice(0.0f);
        queryCartData();
    }

    @Override
    public void delGoodsFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    /**
     * 提供精确的加法运算。
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */

    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */

    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

}
