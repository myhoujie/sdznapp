package com.example.app3home.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.app3grzx.fragment.MineCollectFragment;
import com.example.app3grzx.fragment.MineCouponFragment;
import com.example.app3home.R;
import com.example.app3libpublic.widget.EmptySchoolLayout;
import com.example.app3libpublic.widget.radioview.FragmentTabUtils;
import com.example.app3libpublic.widget.radioview.RadioLayout;
import com.example.app3libpublic.widget.radioview.RadioView;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * 描述：个人信息
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class UserInfoFragment extends BaseFragment {

    RadioLayout mRadioLayout;//包裹标签
    RadioView mRvIndent;//订单
    RadioView mRvCollect;//收藏
    RadioView mRvAccount;//基本信息
    RadioView mRvCoupon;//优惠券
    RadioView mStatistics;//课程统计
    RadioView mRvZuoye;//作业
    View views;
    EmptySchoolLayout emptySchoolLayout;

    private ArrayList<Fragment> fragments;
    private boolean isRefresh = false;
    private boolean isFirst = true;
    private int defaultPage = 0;

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_user_info;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mRadioLayout = rootView.findViewById(R.id.rl_user_radiolayout);
        mRvIndent = rootView.findViewById(R.id.ll_indent);
        mRvCollect = rootView.findViewById(R.id.ll_collect);
        mRvAccount = rootView.findViewById(R.id.ll_account);
        mRvCoupon = rootView.findViewById(R.id.ll_coupon);
        mRvZuoye = rootView.findViewById(R.id.ll_zuoye);
        mStatistics = rootView.findViewById(R.id.ll_statistics);
        views = rootView.findViewById(R.id.view);
        emptySchoolLayout = rootView.findViewById(R.id.empty_layout_user);
        setInitOnResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            setInitOnResume();
        }
    }

    private void setInitOnResume() {
        //未登录
        if (!SPToken.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_login));
            isFirst = true;
        } else {
//            if (SPManager.isToCLogin()) {
//                mRvZuoye.setVisibility(View.GONE);
//            } else {
//                mRvZuoye.setVisibility(View.VISIBLE);
//            }
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            views.setVisibility(View.GONE);
            if (isFirst) {
                initData();
                initView();
                isFirst = false;
                mRvIndent.setChecked(true);
            }
        }
    }


    private void initData() {
        fragments = new ArrayList<>();
        fragments.add(MineIndentFragment.newInstance());//订单
        fragments.add(MineCollectFragment.newInstance());//收藏
        fragments.add(MyHomeworkFragment.newInstance());//作业
        fragments.add(MineStatisticsFragment.newInstance());//课程统计
        fragments.add(MineAccountSettingFragment.newInstance());//基本信息设置
        fragments.add(MineCouponFragment.newInstance());//优惠券
    }

    private void initView() {
        FragmentTabUtils mTabUtils = new FragmentTabUtils(mContext, getActivity().getSupportFragmentManager(),
                fragments, R.id.user_container, mRadioLayout, defaultPage, true, false);
        mTabUtils.setNeedAnimation(true);
        mRadioLayout.checkedRadioSelector(R.id.ll_indent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            isRefresh = true;
        } else if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_BIND_ACCOUNT)) {
//            if (SPManager.isToCLogin()) {
//                mRvZuoye.setVisibility(View.GONE);
//            } else {
//                mRvZuoye.setVisibility(View.VISIBLE);
//            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
