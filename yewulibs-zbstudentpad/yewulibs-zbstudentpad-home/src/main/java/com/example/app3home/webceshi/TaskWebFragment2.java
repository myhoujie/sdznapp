package com.example.app3home.webceshi;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.app3libpublic.event.WebViewAvatarEvent;
import com.example.app3home.R;
import com.example.app3libpublic.widget.TitleBar;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by cenxiaozhong on 2017/5/15.
 * source code  https://github.com/Justson/AgentWeb
 */

public class TaskWebFragment2 extends BaseAgentWebFragment {

    private ViewGroup mViewGroup;
    private String url, token;
    private TitleBar titleBar;
    private String AnsweringState;
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    public static TaskWebFragment2 newInstance(String url, String AnsweringState, String token) {
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putString("AnsweringState", AnsweringState);
        args.putString("token", token);
        TaskWebFragment2 fragment = new TaskWebFragment2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return mViewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_agentweb, container, false);
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.mViewGroup.findViewById(R.id.linearLayout);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }


    protected void initView(View view) {
        titleBar = view.findViewById(R.id.title);
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        getUrl();
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 10) {
                title = title.substring(0, 10).concat("...");
            }
        }
        titleBar.setTitleText(title);
    }

    @Nullable
    @Override
    protected String getUrl() {
        url = getArguments().getString("url");
        AnsweringState = getArguments().getString("AnsweringState");
        token = getArguments().getString("token");
        String pingtoken = url + "&token=" + token;
        return pingtoken;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToastUtils.showShort("123123123");
        mAgentWeb.getWebLifeCycle().onDestroy();
        mAgentWeb.clearWebCache();//清楚缓存
        if (AnsweringState.equals(TYPE_TODY)) {
            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_TODY));
        } else {
            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_RECENTLY));
        }
    }
}
