package com.example.app3home.persenter;

import android.text.TextUtils;

import com.example.app3home.view.FzxLoginView;
import com.example.app3libpublic.R;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.LoginProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class FzxLoginPresenter extends BasePresenter<FzxLoginView> {
    public void login(final String username, String password, String imei) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .login(username, password, imei, "2")//"安卓手机端"  1    "安卓pad端" 2
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .onErrorResumeNext(new ExceptionFunc<UserLoginBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean userBean) {
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }
}
