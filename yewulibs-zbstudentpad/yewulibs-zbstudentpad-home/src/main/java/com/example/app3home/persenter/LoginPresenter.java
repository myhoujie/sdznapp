package com.example.app3home.persenter;

import android.content.res.Configuration;
import android.text.TextUtils;

import com.example.app3home.view.LoginView;
import com.example.app3libpublic.R;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.LoginProgressSubscriber;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class LoginPresenter extends BasePresenter<LoginView> {

/*
    public void checkVerion() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryVersion(1)//0安卓手机  1安卓pad
                .compose(TransformUtils.<ResultBean<VersionInfoBean>>defaultSchedulers())
                .map(new ResponseFunc<VersionInfoBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfoBean>() {
                    @Override
                    public void onNext(VersionInfoBean versionInfoBean) {
                        int currVersion = App2Utils.getAppVersionCode(App2.get());//获取版本号
                        int updateVersion = versionInfoBean.getVersionNumber();//线上版本号
                        if (updateVersion > currVersion) {
                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        e.printStackTrace();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
*/

    public void login(final String username, String password, String imei) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .login(username, password, imei, "2")//"安卓手机端"  1    "安卓pad端" 2
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .onErrorResumeNext(new ExceptionFunc<UserLoginBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean userBean) {
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }
/*
    public void login(final String username, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("userName", username);
        map.put("password", password);
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .login(requestBody)
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .onErrorResumeNext(new ExceptionFunc<UserBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        userBean.setAccount(username);
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }
*/

    public void getVerifyCode(String phoneNo) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    /**
     *
     */
    public void loginCode(final String phone, String code, String imei) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .loginCode(phone, code, imei, "2")//"安卓手机端"  1    "安卓pad端" 2
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .onErrorResumeNext(new ExceptionFunc<UserLoginBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean userBean) {
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }


    /**
     * 判断当前设备是手机还是平板，代码来自 Google I/O App for Android
     *
     * @return 平板返回 True，手机返回 False
     */
    private boolean isPad() {
        return (App2.get().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
