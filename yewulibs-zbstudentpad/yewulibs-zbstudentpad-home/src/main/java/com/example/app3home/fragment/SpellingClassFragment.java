package com.example.app3home.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3home.R;
import com.example.app3libpublic.adapter.SpellingTitleAdapter;
import com.example.app3libpublic.fragment.HomeFragment;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libpublic.utils.GradeIdToNameUtils;
import com.example.app3libpublic.widget.ExpandViewRect;
import com.example.app3libpublic.widget.pager.PagerSlidingTabStrip;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.example.app3libvariants.bean.SubjectBean;
import com.example.app3pkt.fragment.SpellingContentFragment;
import com.example.app3pkt.presenter.SpellingClassPresenter;
import com.example.app3pkt.view.SpellingClassView;
import com.example.app3libvariants.zbpad.network.SPToken;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * 拼课堂
 */
public class SpellingClassFragment extends BaseMVPFragment<SpellingClassView, SpellingClassPresenter> implements SpellingClassView, View.OnClickListener {
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    ViewPager viewPager;
    TextView tvBarGrade;
    ImageView icBarSearch;
    ImageView ivBarShop;
    TextView update;
    private boolean isSpellRefresh = false;
    public static final String IS_MAIN_IN = "to_in_subject";   //
    private SpellingTitleAdapter spellingTitleAdapter;
    private List<Fragment> spellListFragment = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();
    ;//定义要装fragment的列表

    public SpellingClassFragment() {
        // Required empty public constructor
    }

    public static SpellingClassFragment newInstance() {
        return new SpellingClassFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_spelling_class;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mPagerSlidingTabStrip = rootView.findViewById(R.id.tabs);
        viewPager = rootView.findViewById(R.id.vp_pager);
        tvBarGrade = rootView.findViewById(R.id.tv_bar_grade);
        icBarSearch = rootView.findViewById(R.id.ic_bar_search);
        ivBarShop = rootView.findViewById(R.id.iv_bar_right);
        update = rootView.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.JavaDemoActivity"));//版本更新
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShareIndexActivity"));//分享
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.JpushActivity"));//推送
            }
        });
        ExpandViewRect.expandViewTouchDelegate(ivBarShop, 10, 10, 10, 10);
        tvBarGrade.setOnClickListener(this);
        icBarSearch.setOnClickListener(this);
        ivBarShop.setOnClickListener(this);
        initView();
        loadData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvBarGrade.setText(GradeIdToNameUtils.setName(SPManager.getSectionId(), SPManager.getgradeId()));
    }

    private void loadData() {
        if (SPManager.getSectionId() < 0) {
//            IntentController.toSelectSubject(mContext, false);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
            startIntent.putExtra(IS_MAIN_IN, false);
            startActivity(startIntent);
        } else {
            if (spellingTitleAdapter == null) {
                spellingTitleAdapter = new SpellingTitleAdapter(getChildFragmentManager());
            }
            mPresenter.getSubject();
        }
    }


    private void initView() {

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ic_bar_search) {
            // FragmentController.toSearch(SpellingClassFragment.this, CourseCons.Type.ALL);
            showFragment(R.id.main_container, SearchFragment.newInstance(CourseCons.Type.ALL), true);
        } else if (id == R.id.tv_bar_grade) {
            //IntentController.toSelectSubject(mContext, true);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
            startIntent.putExtra(IS_MAIN_IN, true);
            startActivity(startIntent);
        } else if (id == R.id.iv_bar_right) {
            if (!SPToken.autoLogin(mContext)) {
//                    IntentController.toLogin(mContext);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");//LoginActivity
                startActivity(intent);
                return;
            }
//                IntentController.toShoppingCart(mContext);
            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");//ShoppingCartActivity
            startActivity(intent);
        }
    }

    @Override
    protected SpellingClassPresenter createPresenter() {
        return new SpellingClassPresenter();
    }


    @Override
    public void onSubjectSuccess(List<SubjectBean> subjectList) {
        if (subjectList.size() > 0) {
            this.listTitle.clear();
            this.spellListFragment.clear();
            spellListFragment.add(HomeFragment.newInstance());
            listTitle.add("精品课");

            for (int i = 0; i < subjectList.size(); i++) {
                spellListFragment.add(SpellingContentFragment.newInstance(String.valueOf(subjectList.get(i).getSubjectId()), "", ""));
                listTitle.add(subjectList.get(i).getSubjectName());
            }

            spellingTitleAdapter.setmDatas(listTitle, spellListFragment);
            viewPager.setOffscreenPageLimit(0);
            viewPager.setCurrentItem(0);
            viewPager.setAdapter(spellingTitleAdapter);
            mPagerSlidingTabStrip.setViewPager(viewPager);
            mPagerSlidingTabStrip.clearConfigSet();
        } else {
//            IntentController.toSelectSubject(mContext, false);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");//SelectSubjectActivity.class
            startIntent.putExtra(IS_MAIN_IN, false);
            startActivity(startIntent);
        }

    }

    @Override
    public void onSubjectFailed(String msg) {
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            if (spellingTitleAdapter != null) {
                spellingTitleAdapter.clear(viewPager);
            }
            loadData();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}

