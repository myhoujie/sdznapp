package com.example.app3libvariants.network.api;

import com.example.app3libvariants.bean.CollectBean;
import com.example.app3libvariants.bean.MessageBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.bean.VersionInfoBean;
import com.example.app3libvariants.zbpad.network.api.ApiInterface;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.sdzn.pkt.student.hd.BuildConfig3;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 描述：
 * - 账户相关接口
 * 创建人：baoshengxiang
 * 创建时间：2017/7/13
 */
public interface AccountService {
    //忘记密码   **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.CONFIRM_VERIFY_CODE)
    Observable<ResultBean<UserLoginBean>> confirmVerifyCode(@Field("telephone") String telephone, @Field("code") String code);

    /**
     * 找回密码中的修改密码
     * <p>
     * **form urlencoded**
     */
    @FormUrlEncoded
    @POST(ApiInterface.ACCOUNT_CHANGEPASSWORD)
    Observable<ResultBean<Object>> changePwd(@Field("newPwd") String password);

    /**
     * 根据原始密码修改密码
     * <p>
     * **form urlencoded**
     */
    @FormUrlEncoded
    @POST(ApiInterface.ACCOUNT_CHANGEPASSWORD)
    Observable<ResultBean<Object>> changeOldPwd(@Field("oldPwd") String oldPwd, @Field("newPwd") String newPwd);

    /**
     * 个人设置中绑定手机
     *
     * @param phoneNo
     * @param verifyCode
     * @return
     */
    @FormUrlEncoded
    @POST(ApiInterface.CONFIRM_PHONE_VERIFY_CODE)
    Observable<ResultBean<Object>> confirmPhoneVerifyCode(@Field("newTel") String phoneNo, @Field("code") String verifyCode, @Field("userId") String userId);


    //查询我的收藏
    @FormUrlEncoded
    @POST(ApiInterface.UP_COLLECT)
    Observable<ResultBean<CollectBean>> upDataCollection(@Field("currentPage") int currentPage, @Field("pageSize") int pageSize);

    //删除我的收藏
    @FormUrlEncoded
    @POST(ApiInterface.DEL_COLLECTION)
    Observable<ResultBean<Object>> delCollection(@Field("courseId") String courseId);

    //删除消息(ids消息主键ID，多个用逗号分隔)   **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.DEL_MESSAGE)
    Observable<ResultBean<Object>> delMessage(@Field("ids") String ids);

    // 检查更新
    @FormUrlEncoded
    @POST(BuildConfig3.BASE_ADDRESS + ApiInterface.QUERY_VERSION_INFO)
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Field("programId") String programId, @Field("type") String type);//@Body RequestBody body@Header("Authorization") String token,



    //取消订单
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.URL_CANCEL_INDENT)
    Observable<ResultBean<Object>> cancelIndent(@Body RequestBody body);

    //退课申请
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.URL_DROP_COURSE)
    Observable<ResultBean<Object>> dropCourse(@Body RequestBody body);

    //检查更新
    /*@FormUrlEncoded
    @POST(ApiInterface.QUERY_VERSION_INFO)
    Observable<ResultBean<VersionInfoBean>> queryVersion(@Field("type") int type);*/



    //检查未读消息数
    @GET(ApiInterface.QUERY_UNREAD_MSG_COUNT)
    Observable<ResultBean<Object>> queryUnReadMsgCount(@Query("status") String status, @Query("userId") String userId);


    //上传url
    @POST(ApiInterface.URL_UPLOAD_PHOTO)
    Observable<ResultBean<Object>> upUserPhoto(@Body RequestBody file);

    //退课中  取消退课
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ORDER_CANCEL_COURSE)
    Observable<ResultBean<Object>> cancelCourse(@Body RequestBody body);

    //消息列表
    @GET(ApiInterface.GET_MESSAGE_LIST)
    Observable<ResultBean<List<MessageBean.LetterListBean>>> listMessage(@Query("userId") String userId);


    //消息状态修改
    @FormUrlEncoded
    @POST(ApiInterface.UPDATE_READ)
    Observable<ResultBean<Object>> setMessageDetail(@Field("id") String id, @Field("status") String status);


}
