package com.example.app3libvariants.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class LiveListBean {

    private List<CourseListBean> liveList;

    public List<CourseListBean> getLiveList() {
        return liveList;
    }

    public void setLiveList(List<CourseListBean> liveList) {
        this.liveList = liveList;
    }
}
