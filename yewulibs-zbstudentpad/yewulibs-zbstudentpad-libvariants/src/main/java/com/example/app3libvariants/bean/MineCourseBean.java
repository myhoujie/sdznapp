package com.example.app3libvariants.bean;

import java.util.List;

/**
 * 描述：我的课程
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public class MineCourseBean {
    /**
     * addTime : 1500431706000
     * classId : 0
     * courseId : 78
     * courseName : 直播-会计基础视频教程全套
     * courseStatus: "COMINGSOON"
     * currentPrice : 0.01
     * endTime : 1508256000000
     * isOverdue : true
     * isavaliable : 1
     * lessionNum : 12
     * leastKpointName:"语文老师5第一节课",
     * liveBeginTime : 1467561600000
     * liveEndTime : 1483113600000
     * logo : /images/upload/course/20161222/1482392560946.jpg
     * loseTime : 90
     * loseType : 1
     * now : 1501060767312
     * pageBuycount : 139
     * pageViewcount : 1403
     * sellType : LIVE
     * sourcePrice : 0.01
     * sqlMap : {}
     * studyPercent : 100
     * subjectId : 251
     * teacherList : [{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297957332.jpg","id":74,"name":"马妮妮","education":"中国人民大学附属中学数学一级教师"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","id":81,"name":"李立","education":"上海师范大学法学院副教授"}]
     * title : 会计基础重在提供会计工作的基础信息，了解会计的工作原理，掌握会计实务当中必要的理论基础知识。此次视频选择为会计基础科目里难度较高层次水平。
     * updateTime : 1482277362000
     * userId : 0
     */

    private long addTime;
    private int auditionLessionCount;
    private int classId;
    private int courseId;
    private CourseLiveingBean courseLiveing;
    private String courseName;
    private String leastKpointName;
    private double currentPrice;
    private long endTime;
    private boolean isOverdue;//判断是否过期（true有效期、false已过期）
    private int isavaliable;
    private int lessionNum;
    private long liveBeginTime;
    private long liveEndTime;
    private String logo;
    private String loseTime;
    private int loseType;
    private CourseLiveingBean nextCourseKpoint;
    private long now;
    private int pageBuycount;
    private int pageViewcount;
    private String sellType;
    private int packageType;
    private double sourcePrice;
    private SqlMapBean sqlMap;
    private String studyPercent;
    private int subjectId;
    private String title;
    private long updateTime;
    private int userId;
    private String state;
    private List<TeacherListBean> teacherList;

    public int getAuditionLessionCount() {
        return auditionLessionCount;
    }

    public void setAuditionLessionCount(int auditionLessionCount) {
        this.auditionLessionCount = auditionLessionCount;
    }

    public CourseLiveingBean getCourseLiveing() {
        return courseLiveing;
    }

    public void setCourseLiveing(CourseLiveingBean courseLiveing) {
        this.courseLiveing = courseLiveing;
    }

    public CourseLiveingBean getNextCourseKpoint() {
        return nextCourseKpoint;
    }

    public void setNextCourseKpoint(CourseLiveingBean nextCourseKpoint) {
        this.nextCourseKpoint = nextCourseKpoint;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLeastKpointName() {
        return leastKpointName;
    }

    public void setLeastKpointName(String leastKpointName) {
        this.leastKpointName = leastKpointName;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public long getEndTime() {
        return endTime;
    }

    public boolean isOverdue() {
        return isOverdue;
    }

    public void setOverdue(boolean overdue) {
        isOverdue = overdue;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public int getLessionNum() {
        return lessionNum;
    }

    public void setLessionNum(int lessionNum) {
        this.lessionNum = lessionNum;
    }

    public long getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(long liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public long getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(long liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLoseTime() {
        return loseTime;
    }

    public void setLoseTime(String loseTime) {
        this.loseTime = loseTime;
    }

    public int getLoseType() {
        return loseType;
    }

    public void setLoseType(int loseType) {
        this.loseType = loseType;
    }

    public long getNow() {
        return now;
    }

    public void setNow(long now) {
        this.now = now;
    }

    public int getPageBuycount() {
        return pageBuycount;
    }

    public void setPageBuycount(int pageBuycount) {
        this.pageBuycount = pageBuycount;
    }

    public int getPageViewcount() {
        return pageViewcount;
    }

    public void setPageViewcount(int pageViewcount) {
        this.pageViewcount = pageViewcount;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public int getPackageType() {
        return packageType;
    }

    public void setPackageType(int packageType) {
        this.packageType = packageType;
    }

    public double getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(double sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public SqlMapBean getSqlMap() {
        return sqlMap;
    }

    public void setSqlMap(SqlMapBean sqlMap) {
        this.sqlMap = sqlMap;
    }

    public String getStudyPercent() {
        return studyPercent;
    }

    public void setStudyPercent(String studyPercent) {
        this.studyPercent = studyPercent;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<TeacherListBean> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<TeacherListBean> teacherList) {
        this.teacherList = teacherList;
    }

    public static class SqlMapBean {
    }

    public static class TeacherListBean {
        /**
         * picPath : /images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297957332.jpg
         * id : 74
         * name : 马妮妮
         * education : 中国人民大学附属中学数学一级教师
         */

        private String picPath;
        private int id;
        private String name;
        private String education;

        public String getPicPath() {
            return picPath;
        }

        public void setPicPath(String picPath) {
            this.picPath = picPath;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }
    }

    public static class CourseLiveingBean {
        private long addTime;
        private List childKpoints;
        private String content;
        private int courseId;
        private String fileType;
        private int free;
        private String isCopy;
        private int isavaliable;
        private List kpointAtlasesList;
        private int kpointId;
        private List kpointList;
        private int kpointPrice;
        private String kpointStatus;
        private int kpointType;
        private long liveBeginTime;
        private long liveEndTime;
        private String name;
        private String openType;
        private int pageCount;
        private int parentId;
        private int playCount;
        private String playTime;
        private int queryLimitNum;
        private int sort;
        private SqlMapBean sqlMap;
        private String supplier;
        private TeacherListBean teacher;
        private int teacherId;
        private int userId;
        private String videoType;
        private String videoUrl;
        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }
        public long getAddTime() {
            return addTime;
        }

        public void setChildKpoints(List childKpoints) {
            this.childKpoints = childKpoints;
        }
        public List getChildKpoints() {
            return childKpoints;
        }

        public void setContent(String content) {
            this.content = content;
        }
        public String getContent() {
            return content;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }
        public int getCourseId() {
            return courseId;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }
        public String getFileType() {
            return fileType;
        }

        public void setFree(int free) {
            this.free = free;
        }
        public int getFree() {
            return free;
        }

        public void setIsCopy(String isCopy) {
            this.isCopy = isCopy;
        }
        public String getIsCopy() {
            return isCopy;
        }

        public void setIsavaliable(int isavaliable) {
            this.isavaliable = isavaliable;
        }
        public int getIsavaliable() {
            return isavaliable;
        }

        public void setKpointAtlasesList(List kpointAtlasesList) {
            this.kpointAtlasesList = kpointAtlasesList;
        }
        public List getKpointAtlasesList() {
            return kpointAtlasesList;
        }

        public void setKpointId(int kpointId) {
            this.kpointId = kpointId;
        }
        public int getKpointId() {
            return kpointId;
        }

        public void setKpointList(List kpointList) {
            this.kpointList = kpointList;
        }
        public List getKpointList() {
            return kpointList;
        }

        public void setKpointPrice(int kpointPrice) {
            this.kpointPrice = kpointPrice;
        }
        public int getKpointPrice() {
            return kpointPrice;
        }

        public void setKpointStatus(String kpointStatus) {
            this.kpointStatus = kpointStatus;
        }
        public String getKpointStatus() {
            return kpointStatus;
        }

        public void setKpointType(int kpointType) {
            this.kpointType = kpointType;
        }
        public int getKpointType() {
            return kpointType;
        }

        public void setLiveBeginTime(long liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }
        public long getLiveBeginTime() {
            return liveBeginTime;
        }

        public void setLiveEndTime(long liveEndTime) {
            this.liveEndTime = liveEndTime;
        }
        public long getLiveEndTime() {
            return liveEndTime;
        }

        public void setName(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }

        public void setOpenType(String openType) {
            this.openType = openType;
        }
        public String getOpenType() {
            return openType;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }
        public int getPageCount() {
            return pageCount;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }
        public int getParentId() {
            return parentId;
        }

        public void setPlayCount(int playCount) {
            this.playCount = playCount;
        }
        public int getPlayCount() {
            return playCount;
        }

        public void setPlayTime(String playTime) {
            this.playTime = playTime;
        }
        public String getPlayTime() {
            return playTime;
        }

        public void setQueryLimitNum(int queryLimitNum) {
            this.queryLimitNum = queryLimitNum;
        }
        public int getQueryLimitNum() {
            return queryLimitNum;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
        public int getSort() {
            return sort;
        }

        public void setSqlMap(SqlMapBean sqlMap) {
            this.sqlMap = sqlMap;
        }
        public SqlMapBean getSqlMap() {
            return sqlMap;
        }

        public void setSupplier(String supplier) {
            this.supplier = supplier;
        }
        public String getSupplier() {
            return supplier;
        }

        public void setTeacher(TeacherListBean teacher) {
            this.teacher = teacher;
        }
        public TeacherListBean getTeacher() {
            return teacher;
        }

        public void setTeacherId(int teacherId) {
            this.teacherId = teacherId;
        }
        public int getTeacherId() {
            return teacherId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
        public int getUserId() {
            return userId;
        }

        public void setVideoType(String videoType) {
            this.videoType = videoType;
        }
        public String getVideoType() {
            return videoType;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }
        public String getVideoUrl() {
            return videoUrl;
        }
    }
}
