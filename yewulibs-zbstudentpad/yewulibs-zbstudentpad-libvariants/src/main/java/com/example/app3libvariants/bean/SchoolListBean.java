package com.example.app3libvariants.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/18
 */
public class SchoolListBean {

    private List<SchoolBean> schoolList;

    public List<SchoolBean> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<SchoolBean> schoolList) {
        this.schoolList = schoolList;
    }


}
