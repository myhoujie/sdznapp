package com.example.app3libvariants.bean;

import java.io.Serializable;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/13
 */
public class AccountBean implements Serializable {
    private static final long serialVersionUID = 2144132525195382095L;
    private String token;
    private UserBean user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserBean getUserBean() {
        return user;
    }

    public void setUserBean(UserBean userBean) {
        this.user = userBean;
    }
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
