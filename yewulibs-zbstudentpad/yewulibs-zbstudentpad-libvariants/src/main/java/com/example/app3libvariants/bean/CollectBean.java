package com.example.app3libvariants.bean;

import java.util.List;

/**
 *
 */

public class CollectBean {


    /**
     * favoriteList : [{"addTime":"2017-07-17 16:57:44","courseId":46,"courseName":"企业人力资源管理师二级押题串讲学习班","currentPrice":0.01,"favouriteId":41,"id":41,"logo":"/images/upload/course/20161222/1482394345278.jpg","teacherList":[{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg","id":77,"name":"林奇","education":"人大附中2009届毕业生"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","id":81,"name":"李立","education":"上海师范大学法学院副教授"}]},{"addTime":"2017-07-17 18:07:34","courseId":196,"courseName":"test-7.5","currentPrice":0,"favouriteId":42,"id":42,"logo":"/images/upload/course/20170704/1499159743579.jpg","teacherList":[{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297999141.jpg","id":78,"name":"陈红","education":"华东师范大学数学系硕士生导师，中国数学奥林匹克高级教练"}]},{"addTime":"2017-07-17 23:42:13","courseId":82,"courseName":"直播-企业人力资源管理师三级教程全集","currentPrice":0.01,"favouriteId":43,"id":43,"logo":"/images/upload/course/20161227/1482808594241.jpg","teacherList":[{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297969808.jpg","id":75,"name":"张云唻","education":"毕业于北京大学数学系"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297957332.jpg","id":74,"name":"马妮妮","education":"中国人民大学附属中学数学一级教师"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297935589.jpg","id":80,"name":"潘新强","education":"考研政治辅导实战派专家，全国考研政治命题研究组核心成员。"}]},{"addTime":"2017-07-17 23:42:18","courseId":88,"courseName":"直播-建设工程法规及相关知识","currentPrice":0.01,"favouriteId":44,"id":44,"logo":"/images/upload/course/20161227/1482810259692.jpg","teacherList":[{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg","id":77,"name":"林奇","education":"人大附中2009届毕业生"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297977255.jpg","id":76,"name":"王健化","education":"长期从事考研政治课讲授和考研命题趋势与应试对策研究。考研辅导新锐派的代表。"},{"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442298121626.jpg","id":73,"name":"李诚","education":"毕业于师范大学数学系，热爱教育事业，执教数学思维6年有余"}]}]
     * page : {"currentPage":1,"first":true,"last":true,"pageSize":10,"totalPageSize":1,"totalResultSize":4}
     */

    private PageBean page;
    private List<FavoriteListBean> rows;

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public List<FavoriteListBean> getFavoriteList() {
        return rows;
    }

    public void setFavoriteList(List<FavoriteListBean> favoriteList) {
        this.rows = favoriteList;
    }

    public static class PageBean {
        /**
         * currentPage : 1
         * first : true
         * last : true
         * pageSize : 10
         * totalPageSize : 1
         * totalResultSize : 4
         */

        private int currentPage;
        private boolean first;
        private boolean last;
        private int pageSize;
        private int totalPageSize;
        private int totalResultSize;

    }

    public static class FavoriteListBean {


        /**
         * addTime : 2017-07-17 16:57:44
         * courseId : 46
         * courseName : 企业人力资源管理师二级押题串讲学习班
         * currentPrice : 0.01
         * favouriteId : 41
         * id : 41
         * isavaliable : 1
         * liveBeginTime: "2016-07-01 00:00:00"
         * "packageType": 2
         * lessionNum : 4
         * logo : /images/upload/course/20161222/1482394345278.jpg
         * sellType : COURSE
         * subjectName : 人力二级
         */

        private String addTime;
        private int courseId;
        private String courseName;
        private double currentPrice;
        private int favouriteId;
        private int id;
        private int isAvaliable;
        private int lessionNum;

        private String liveBeginTime;
        private String logo;
        private int packageType;
        private String sellType;
        private String subjectName;
        private String teacherName;
        private boolean islose;


        public boolean isIslose() {
            return islose;
        }

        public void setIslose(boolean islose) {
            this.islose = islose;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getLiveBeginTime() {
            return liveBeginTime;
        }

        public void setLiveBeginTime(String liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }

        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }



        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public double getCurrentPrice() {
            return currentPrice;
        }

        public void setCurrentPrice(double currentPrice) {
            this.currentPrice = currentPrice;
        }

        public int getFavouriteId() {
            return favouriteId;
        }

        public void setFavouriteId(int favouriteId) {
            this.favouriteId = favouriteId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsavaliable() {
            return isAvaliable;
        }

        public void setIsavaliable(int isavaliable) {
            this.isAvaliable = isavaliable;
        }

        public int getLessionNum() {
            return lessionNum;
        }

        public void setLessionNum(int lessionNum) {
            this.lessionNum = lessionNum;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public static class TeacherListBean {
            /**
             * picPath : /images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg
             * id : 77
             * name : 林奇
             * education : 人大附中2009届毕业生
             */

            private String picPath;
            private int id;
            private String name;
            private String education;

            public String getPicPath() {
                return picPath;
            }

            public void setPicPath(String picPath) {
                this.picPath = picPath;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }
        }
    }
}
