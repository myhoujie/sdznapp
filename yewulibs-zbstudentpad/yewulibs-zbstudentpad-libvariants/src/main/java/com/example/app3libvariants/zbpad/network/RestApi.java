package com.example.app3libvariants.zbpad.network;

import com.example.app3libvariants.zbpad.network.interceptor.CommonInterceptor;
import com.example.app3libvariants.zbpad.network.subscriber.LoggingInterceptor;
import com.sdzn.core.network.interceptor.HttpCacheInterceptor;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.pkt.student.hd.BuildConfig3;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 描述：
 * - retrofit请求管理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class RestApi {
    public static boolean isDebug = false;

    // create retrofit singleton
    private Retrofit createApiClient(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient(isDebug))
                .build();
    }

    //在访问HttpMethods时创建单例
    private static class SingletonHolder {
        private static final RestApi INSTANCE = new RestApi();

        private SingletonHolder() {
            throw new UnsupportedOperationException("u can't instantiate SingletonHolder...");
        }
    }

    public static synchronized RestApi getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * 8090端口
     */
    public <T> T createNew(Class<T> clz) {
//        String serviceUrl = Config.SERVER_HOST + Config.SERVER_PORT_NEW;
        String serviceUrl = BuildConfig3.BASE_ADDRESS;
        return createApiClient(serviceUrl).create(clz);
    }

    /**
     * 创建一个okhttp实例
     *
     * @param debug
     * @return
     */
    private OkHttpClient createOkHttpClient(boolean debug) {
        //缓存
        int size = 1024 * 1024 * 100;
        File cacheFile = new File(App2.get().getCacheDir(), "OkHttpCache");
        Cache cache = new Cache(cacheFile, size);

        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(new LoggingInterceptor()) //日志,所有的请求响应度看到 LoggingInterceptor
                .addInterceptor(new CommonInterceptor())
                .addNetworkInterceptor(new HttpCacheInterceptor())
//                .addInterceptor(new HttpLoggingInterceptor().setLevel(
//                        debug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .cache(cache)
                .build();

    }


}
