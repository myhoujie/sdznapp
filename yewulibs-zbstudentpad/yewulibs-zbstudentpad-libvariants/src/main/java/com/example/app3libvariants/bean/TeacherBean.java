package com.example.app3libvariants.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Reisen at 2018-07-13
 */
public class TeacherBean implements Serializable {
    private static final long serialVersionUID = -928932080495591810L;
    private TeacherInfoBean teacherInfo;

    public TeacherInfoBean getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfoBean teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public static class TeacherInfoBean implements Serializable {
        private static final long serialVersionUID = 8680728266157762973L;
        private List<TeacherListBean> teacherList;

        public List<TeacherListBean> getTeacherList() {
            return teacherList;
        }

        public void setTeacherList(List<TeacherListBean> teacherList) {
            this.teacherList = teacherList;
        }
    }
}
