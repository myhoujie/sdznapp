package com.example.app3libvariants.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/27
 */
public class AnswerCardBean {

    /**
     * cardFlag : card-2017927158
     * detail : [{"num":0,"subject":"single","answerNum":4,"point":"5","correctAnswer":[0]},{"num":1,"subject":"single","answerNum":4,"point":"5","correctAnswer":[1]},{"num":2,"subject":"single","answerNum":4,"point":"5","correctAnswer":[2]},{"num":3,"subject":"single","answerNum":4,"point":"5","correctAnswer":[3]},{"num":4,"subject":"single","answerNum":4,"point":"5","correctAnswer":[2]},{"num":5,"subject":"single","answerNum":4,"point":"5","correctAnswer":[1]},{"num":6,"subject":"multi","answerNum":4,"point":"5","correctAnswer":[0,1,2,3]},{"num":7,"subject":"judge","answerNum":2,"point":"5","correctAnswer":[0]},{"num":8,"subject":"judge","answerNum":2,"point":"5","correctAnswer":[0]},{"num":9,"subject":"judge","answerNum":2,"point":"5","correctAnswer":[0]}]
     */
    private String cardFlag;
    private List<DetailBean> cardDetail;
    private List<DetailBean> detail;
    private String stuID;
    private boolean isAnswer;
    private int totalPoint;

    public String getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(String cardFlag) {
        this.cardFlag = cardFlag;
    }

    public List<DetailBean> getDetail() {
        return cardDetail;
    }

    public void setDetail(List<DetailBean> cardDetail) {
        this.cardDetail = cardDetail;
        this.detail = cardDetail;
    }

    public List<DetailBean> getCardDetail() {
        return cardDetail;
    }

    public String getStuID() {
        return stuID;
    }

    public void setStuID(String stuID) {
        this.stuID = stuID;
    }

    public boolean isIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(boolean isAnswer) {
        this.isAnswer = isAnswer;
    }

    public int getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(int totalPoint) {
        this.totalPoint = totalPoint;
    }

    public static class DetailBean {
        /**
         * num : 0
         * subject : single
         * answerNum : 4
         * point : 5
         * correctAnswer : [0]
         */
        private boolean isCorrect;
        private int num;
        private String subject;
        private int answerNum;
        private String point;
        private List<Integer> correctAnswer;
        private List<Integer> respondence = new ArrayList<>();

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public int getAnswerNum() {
            return answerNum;
        }

        public void setAnswerNum(int answerNum) {
            this.answerNum = answerNum;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public List<Integer> getCorrectAnswer() {
            return correctAnswer;
        }

        public void setCorrectAnswer(List<Integer> correctAnswer) {
            this.correctAnswer = correctAnswer;
        }

        public boolean isIsCorrect() {
            return isCorrect;
        }

        public void setIsCorrect(boolean isCorrect) {
            this.isCorrect = isCorrect;
        }

        public List<Integer> getRespondence() {
            return respondence;
        }

        public void setRespondence(List<Integer> respondence) {
            this.respondence = respondence;
        }

        @Override
        public String toString() {
            return "DetailBean{" +
                    "isCorrect=" + isCorrect +
                    ", num=" + num +
                    ", subject='" + subject + '\'' +
                    ", answerNum=" + answerNum +
                    ", point='" + point + '\'' +
                    ", correctAnswer=" + correctAnswer +
                    ", respondence=" + respondence +
                    '}';
        }
    }


}
