package com.example.app3libvariants.network.download;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.SDCardUtils;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 描述：
 * - 文件下载线程
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class DownFileThread implements Runnable {
    private final static int DOWNLOAD_COMPLETE = -2;
    private final static int DOWNLOAD_FAIL = -1;
    private final static String TAG = "DownFileThread";
    private Handler mHandler; //传入的Handler,用于像Activity或service通知下载进度
    private String urlStr;  //下载URL
    private File apkFile;   //文件保存路径
    private boolean isFinished; //下载是否完成
    private boolean interupted = false;  //是否强制停止下载线程
    private InputStream iStream = null;
    private FileOutputStream fos = null;
    private BufferedInputStream bis = null;

    public DownFileThread(Handler handler, String urlStr, File targetFile) {
        Log.i(TAG, urlStr);
        this.mHandler = handler;
        this.urlStr = urlStr;
        this.apkFile = targetFile;
        isFinished = false;
    }

    public File getApkFile() {
        if (isFinished)
            return apkFile;
        else
            return null;
    }

    public boolean isFinished() {
        return isFinished;
    }

    /**
     * 强行终止文件下载
     */
    public void interuptThread() {
        interupted = true;
    }

    @Override
    public void run() {
        if (SDCardUtils.isSDCardEnable()) {
            try {
                HttpURLConnection conn = connection(urlStr);
                // 必须设置false，否则会自动redirect到Location的地址
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK
                        || conn.getResponseCode() == HttpURLConnection.HTTP_PARTIAL) {
                    receiverStream(conn);
                } else if (conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
                    String location = conn.getHeaderField("Location");
                    HttpURLConnection reConn = connection(location);
                    receiverStream(reConn);
                } else {
                    Log.i(TAG, "请求失败:" + conn.getResponseCode());
                    mHandler.sendEmptyMessage(DOWNLOAD_FAIL);
                }

            } catch (Exception e) {
                Log.i(TAG, "获取文件流失败");
                mHandler.sendEmptyMessage(DOWNLOAD_FAIL);
            } finally {
                closeStream(iStream, bis, fos);
            }
        } else {
            Log.i(TAG, "外部存储卡不存在，下载失败！");
            mHandler.sendEmptyMessage(DOWNLOAD_FAIL);
        }
    }

    private HttpURLConnection connection(String targetUrl) throws IOException {
        URL url = new URL(targetUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(5000);
        conn.setReadTimeout(20000);
        conn.setRequestProperty("Range", "bytes=0-");
        conn.setInstanceFollowRedirects(false);
        return conn;
    }

    private void receiverStream(HttpURLConnection conn) throws IOException {

        iStream = conn.getInputStream();
        fos = new FileOutputStream(apkFile);
        bis = new BufferedInputStream(iStream);
        byte[] buffer = new byte[1024];
        // 获取文件总长度
        int length = conn.getContentLength();
        LogUtils.i("connLength = " + length);
        double rate = (double) 100 / length;  //最大进度转化为100
        int total = 0;
//        int times = 0;//设置更新频率，频繁操作ＵＩ线程会导致系统奔溃
        long time = System.currentTimeMillis();
        Log.i("threadStatus", "开始下载");
        int len = 0;
        while (!interupted && ((len = bis.read(buffer)) != -1)) {
            fos.write(buffer, 0, len);
            // 获取已经读取长度
            total += len;
            int p = (int) (total * rate);
//            Log.i("num", rate + "," + total + "," + p);
            if ((System.currentTimeMillis() - time) >= 2000 || p == 100) {//这里更新时间更改为2秒...三星的一些平板低于2秒系统会崩溃
                                    /*这是防止频繁地更新通知，而导致系统变慢甚至崩溃。 非常重要。。。。。*/
//                times = 0;
                time = System.currentTimeMillis();
                Message msg = Message.obtain();
                msg.what = p;
                mHandler.sendMessage(msg);
            }
//            times++;
        }
        if (total == length) {
            isFinished = true;
            mHandler.sendEmptyMessage(DOWNLOAD_COMPLETE);
            Log.i(TAG, "下载完成结束");
        } else {
            Log.i(TAG, "强制中途结束");
            mHandler.sendEmptyMessage(DOWNLOAD_FAIL);
        }
    }

    private void closeStream(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            try {
                closeable.close();
            } catch (Exception e) {
                mHandler.sendEmptyMessage(DOWNLOAD_FAIL);
            }
        }
    }

}
