package com.example.app3libvariants.zbpad.network.interceptor;

import android.text.TextUtils;

import com.example.app3libvariants.zbpad.network.NewApiEqualUtil;
import com.example.app3libvariants.zbpad.network.SPToken;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 描述：
 * - okHttp拦截器，用于添加公共参数
 * 创建人：baoshengxiang
 * 创建时间：2017/6/6
 */
public class CommonInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request oldRequest = chain.request();
        String url = oldRequest.url().toString();

        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());
        String token = SPToken.getToken();

        Request.Builder builder = oldRequest.newBuilder();
        if (!TextUtils.isEmpty(token) && NewApiEqualUtil.isNoToken(url)) {
            builder.addHeader("Authorization", "Bearer " + token);
        }
        Request request = builder.method(oldRequest.method(), oldRequest.body())
                .url(authorizedUrlBuilder.build())
                .build();


        return chain.proceed(request);
    }
}