package com.example.app3libvariants.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class LiveCourseDetaiBean {
    private CourseDetailBean liveDetail;
    private String shareUrl;//分享地址
    private boolean isok;//是否已购买

    public CourseDetailBean getLiveDetail() {
        return liveDetail;
    }

    public void setLiveDetail(CourseDetailBean liveDetail) {
        this.liveDetail = liveDetail;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public boolean isok() {
        return isok;
    }

    public void setIsok(boolean isok) {
        this.isok = isok;
    }
}
