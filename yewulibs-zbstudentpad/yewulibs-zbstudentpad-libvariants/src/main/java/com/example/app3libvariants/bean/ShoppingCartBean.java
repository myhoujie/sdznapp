package com.example.app3libvariants.bean;

import java.util.List;


/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class ShoppingCartBean {



    private List<ShopCartListBean> shopcartList;
    private List<CouponBean> couponCodeList;
    private double totalPrice;
    private UserAccountBean userAccount;

    public List<ShopCartListBean> getShopcartList() {
        return shopcartList;
    }

    public void setShopcartList(List<ShopCartListBean> shopcartList) {
        this.shopcartList = shopcartList;
    }


    public List<CouponBean> getCouponCodeList() {
        return couponCodeList;
    }

    public void setCouponCodeList(List<CouponBean> couponCodeList) {
        this.couponCodeList = couponCodeList;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public UserAccountBean getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccountBean userAccount) {
        this.userAccount = userAccount;
    }

    public static class ShopCartListBean {
        /**
         * addTime : 2017-07-16 00:25:14
         * course : {"addTime":"2017-07-16 00:25:14","classId":0,"context":"","courseId":46,"courseName":"企业人力资源管理师二级押题串讲学习班","currentPrice":0.01,"isavaliable":1,"lessionNum":4,"logo":"/images/upload/course/20161222/1482394345278.jpg","loseTime":"365","loseType":1,"pageBuycount":722,"pageViewcount":2299,"sourcePrice":0.01,"subjectId":203,"teacherList":[{"customerId":0,"id":77,"invalid":false,"isStar":1,"name":"林奇","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":81,"invalid":false,"isStar":1,"name":"李立","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","sort":0,"status":0,"subjectId":0}],"title":"考前论文辅导+押题串讲","updateTime":"2016-12-21 08:12:26"}
         * goodsid : 46
         * id : 104
         * type : 1
         * userid : 1464
         */

//        private String addTime;
        private CourseBean course;
//        private int goodsid;
//        private int type;
//        private int userid;

        private int id;
        private int courseId;
        private String courseName;
        private double currentPrice;
        private int packageType;
        private String sellType;
        private String logo;

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public double getCurrentPrice() {
            return currentPrice;
        }

        public void setCurrentPrice(double currentPrice) {
            this.currentPrice = currentPrice;
        }

        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }


        public CourseBean getCourse() {
            return course;
        }

        public void setCourse(CourseBean course) {
            this.course = course;
        }


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }



        public static class CourseBean {
            /**
             * addTime : 2017-07-16 00:25:14
             * classId : 0
             * context :
             * courseId : 46
             * courseName : 企业人力资源管理师二级押题串讲学习班
             * currentPrice : 0.01
             * isavaliable : 1
             * lessionNum : 4
             * logo : /images/upload/course/20161222/1482394345278.jpg
             * loseTime : 365
             * loseType : 1
             * pageBuycount : 722
             * pageViewcount : 2299
             * sourcePrice : 0.01
             * subjectId : 203
             * teacherList : [{"customerId":0,"id":77,"invalid":false,"isStar":1,"name":"林奇","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":81,"invalid":false,"isStar":1,"name":"李立","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","sort":0,"status":0,"subjectId":0}]
             * title : 考前论文辅导+押题串讲
             * updateTime : 2016-12-21 08:12:26
             */

            private String addTime;
            private int classId;
            private String context;
            private int courseId;
            private String courseName;
            private double currentPrice;
            private int isavaliable;
            private int lessionNum;
            private String logo;
            private String loseTime;
            private int loseType;
            private int pageBuycount;
            private int pageViewcount;
            private double sourcePrice;
            private int subjectId;
            private String title;
            private String updateTime;
            private List<TeacherListBean> teacherList;
            private String sellType;
            private int packageType;//1直播套餐，2点播套餐

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public int getClassId() {
                return classId;
            }

            public void setClassId(int classId) {
                this.classId = classId;
            }

            public String getContext() {
                return context;
            }

            public void setContext(String context) {
                this.context = context;
            }

            public int getCourseId() {
                return courseId;
            }

            public void setCourseId(int courseId) {
                this.courseId = courseId;
            }

            public String getCourseName() {
                return courseName;
            }

            public void setCourseName(String courseName) {
                this.courseName = courseName;
            }

            public double getCurrentPrice() {
                return currentPrice;
            }

            public void setCurrentPrice(double currentPrice) {
                this.currentPrice = currentPrice;
            }

            public int getIsavaliable() {
                return isavaliable;
            }

            public void setIsavaliable(int isavaliable) {
                this.isavaliable = isavaliable;
            }

            public int getLessionNum() {
                return lessionNum;
            }

            public void setLessionNum(int lessionNum) {
                this.lessionNum = lessionNum;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getLoseTime() {
                return loseTime;
            }

            public void setLoseTime(String loseTime) {
                this.loseTime = loseTime;
            }

            public int getLoseType() {
                return loseType;
            }

            public void setLoseType(int loseType) {
                this.loseType = loseType;
            }

            public int getPageBuycount() {
                return pageBuycount;
            }

            public void setPageBuycount(int pageBuycount) {
                this.pageBuycount = pageBuycount;
            }

            public int getPageViewcount() {
                return pageViewcount;
            }

            public void setPageViewcount(int pageViewcount) {
                this.pageViewcount = pageViewcount;
            }

            public double getSourcePrice() {
                return sourcePrice;
            }

            public void setSourcePrice(double sourcePrice) {
                this.sourcePrice = sourcePrice;
            }

            public int getSubjectId() {
                return subjectId;
            }

            public void setSubjectId(int subjectId) {
                this.subjectId = subjectId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public List<TeacherListBean> getTeacherList() {
                return teacherList;
            }

            public void setTeacherList(List<TeacherListBean> teacherList) {
                this.teacherList = teacherList;
            }

            public String getSellType() {
                return sellType;
            }

            public void setSellType(String sellType) {
                this.sellType = sellType;
            }

            public int getPackageType() {
                return packageType;
            }

            public void setPackageType(int packageType) {
                this.packageType = packageType;
            }

        }
    }

    public static class UserAccountBean {
        /**
         * accountStatus : ACTIVE
         * backAmount : 0
         * balance : 0.01
         * cashAmount : 0.01
         * createTime : 2017-07-14 10:00:41
         * forzenAmount : 0
         * id : 1108
         * lastUpdateTime : 2017-07-19 17:41:18
         * userId : 1464
         * version : 3
         * vmAmount : 0
         */

        private String accountStatus;
        private int backAmount;
        private double balance;
        private double cashAmount;
        private String createTime;
        private int forzenAmount;
        private int id;
        private String lastUpdateTime;
        private int userId;
        private int version;
        private int vmAmount;

        public String getAccountStatus() {
            return accountStatus;
        }

        public void setAccountStatus(String accountStatus) {
            this.accountStatus = accountStatus;
        }

        public int getBackAmount() {
            return backAmount;
        }

        public void setBackAmount(int backAmount) {
            this.backAmount = backAmount;
        }

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public double getCashAmount() {
            return cashAmount;
        }

        public void setCashAmount(double cashAmount) {
            this.cashAmount = cashAmount;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getForzenAmount() {
            return forzenAmount;
        }

        public void setForzenAmount(int forzenAmount) {
            this.forzenAmount = forzenAmount;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLastUpdateTime() {
            return lastUpdateTime;
        }

        public void setLastUpdateTime(String lastUpdateTime) {
            this.lastUpdateTime = lastUpdateTime;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public int getVmAmount() {
            return vmAmount;
        }

        public void setVmAmount(int vmAmount) {
            this.vmAmount = vmAmount;
        }
    }
}
