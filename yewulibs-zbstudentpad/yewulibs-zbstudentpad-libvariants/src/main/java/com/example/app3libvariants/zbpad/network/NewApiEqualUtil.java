package com.example.app3libvariants.zbpad.network;

import com.example.app3libvariants.zbpad.network.api.ApiInterface;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.pkt.student.hd.BuildConfig3;

/**
 * 是否跑新接口
 */

public class NewApiEqualUtil {


    private static String getUrl(String url) {
        return BuildConfig3.BASE_ADDRESS + url;
    }

    /**
     * 有的新接口不需要token    false -->不加token
     */
    public static boolean isNoToken(String url) {
        String allNewApi[] = {
                getUrl(ApiInterface.SELECT_SUBJECT_LIST),
                getUrl(ApiInterface.SELECT_TOPIC_LIST),
                getUrl(ApiInterface.QUERY_APP_RECOMMENDED),
                getUrl(ApiInterface.QUERY_APP_TOPIC),
                getUrl(ApiInterface.HOME_RECOMMENDED),
                getUrl(ApiInterface.COURSE_SELECTION),
                getUrl(ApiInterface.QUERY_BANNER),
                getUrl(ApiInterface.QUERY_VERSION_INFO),
        };

        for (int a = 0; a < allNewApi.length; a++) {
            if (url.equalsIgnoreCase(allNewApi[a])) {
                return false;
            }
        }
        if (url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_PACKAGE_DETAILS)) || url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_DETAILS_NORMAL))) {
            if (!SPToken.autoLogin(App2.get())) {
                //未登录
                return false;
            }
        }
        return true;

    }


/*    public static boolean isEqual(String url) {
        String allNewApi[] = {getUrl(ApiInterface.ENTER_LIVE_ROOM),
                getUrl(ApiInterface.VIEW_PLAY_BACK),
                getUrl(ApiInterface.VIEW_ONDEMAND),
//                getUrl(ApiInterface.QUWRY_RECOMMENDED_COURSE),
                getUrl(ApiInterface.COURSE_APPLY),
                getUrl(ApiInterface.COURSE_PACKAGE_DETAILS),
                getUrl(ApiInterface.COURSE_DETAILS_NORMAL),
//                getUrl(ApiInterface.INDEX_RECOMMEND),
                getUrl(ApiInterface.CHANGE_USER_INFO),
                getUrl(ApiInterface.CHANGE_SCHOOL_CENTER),
                getUrl(ApiInterface.CHANGE_TODY),
                getUrl(ApiInterface.COURSE_RECENTLY),
                getUrl(ApiInterface.COURSE_ALL_COURSE),
                getUrl(ApiInterface.APP_RESOURCE),
                getUrl(ApiInterface.CHANGE_TOC_PASSWORD),
                getUrl(ApiInterface.OPINION),
                getUrl(ApiInterface.GET_PASSWORD_FZX)
        };

        for (int a = 0; a < allNewApi.length; a++) {
            if (url.equalsIgnoreCase(allNewApi[a])) {
                return true;
            }
        }
        return false;
    }

    private static String getUrl(String url) {
        return Config.SERVER_HOST + Config.SERVER_PORT_NEW + url;
    }

    *//**
     * 有的新接口不需要token    false 不加token
     *//*
    public static boolean isNoToken(String url) {
        String allNewApi[] = {getUrl(ApiInterface.GRADE_JSON),
                getUrl(ApiInterface.SELECT_SUBJECT_LIST),
                getUrl(ApiInterface.QUERY_APP_RECOMMENDED),
                getUrl(ApiInterface.COURSE_SELECTION),
                getUrl(ApiInterface.QUERY_BANNER)
        };

        for (int a = 0; a < allNewApi.length; a++) {
            if (url.equalsIgnoreCase(allNewApi[a])) {
                return false;
            }
        }
        if (url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_PACKAGE_DETAILS)) || url.equalsIgnoreCase(getUrl(ApiInterface.COURSE_DETAILS_NORMAL))) {
            if (!SPManager.autoLogin(App2.get())) {
                //未登录
                return false;
            }
        }
        return true;

    }*/

}
