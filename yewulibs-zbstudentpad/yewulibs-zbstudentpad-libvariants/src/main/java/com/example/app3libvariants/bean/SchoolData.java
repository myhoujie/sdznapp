package com.example.app3libvariants.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/13
 */
public class SchoolData {
    private String id;
    private String name;
    private String sortLetters;//首字母
    private String abridge;//拼音缩写

    public SchoolData() {
    }

    public SchoolData(String name, String sortLetters) {
        this.name = name;
        this.sortLetters = sortLetters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortLetters() {
        return sortLetters;
    }

    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }

    public void setAbridge(String abridge) {
        this.abridge = abridge;
    }

    public String getAbridge() {
        return abridge;
    }
}
