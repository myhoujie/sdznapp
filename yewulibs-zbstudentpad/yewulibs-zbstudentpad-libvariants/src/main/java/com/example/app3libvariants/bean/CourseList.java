package com.example.app3libvariants.bean;

import java.util.List;
public class CourseList {
    /**
     * courseId : 306
     * courseName : 专题
     * isSchool : 0
     * schoolId : 62
     * schoolName : null
     * isAvaliable : 3
     * classId : null
     * className : null
     * subjectId : 0
     * subjectName : null
     * addTime : 2020-07-01 14:19:01
     * sourcePrice : 11
     * currentPrice : 111
     * title :
     * context : <p>111111</p>
     * lessionNum : 0
     * logo : https://csfile.fuzhuxian.com/1593584338180
     * updateTime : null
     * pageViewcount : 0
     * endTime : 2020-07-09 00:00:00
     * losetype : 0
     * loseTime :
     * sequence : 0
     * sellType : SPECIAL
     * liveBeginTime : null
     * liveEndTime : 2020-07-01 14:19:00
     * signEndTime : 2020-07-02 00:00:00
     * refundEndTime : null
     * auditionLessionCount : 0
     * packageType : null
     * grade : null
     * courseTerm : 3
     * liveCourseId : 0
     * isArrange : 0
     * isPublic : null
     * appointSchoolId : null
     * appointSchoolName : null
     * projectId : 7
     * projectName : 专题名称
     * freeLessionNum : 0
     * gradeType : null
     * teacherId : null
     * teacherName : null
     * kpointNum : null
     * packageEduCourseList : null
     * courseKpointLits : null
     * isPurchase : null
     * isCollection : null
     * time : null
     * isRelationLiveCourse : null
     */

    private int courseId;
    private String courseName;
    private double currentPrice;
    private String liveBeginTime;
    private String liveEndTime;
    private String logo;
    private int packageType;
    private String sellType;
    private double sourcePrice;
    private String teacherName;

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }


    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(double sourcePrice) {
        this.sourcePrice = sourcePrice;
    }


    public String getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(String liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public int getPackageType() {
        return packageType;
    }

    public void setPackageType(int packageType) {
        this.packageType = packageType;
    }


    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }


    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

}
