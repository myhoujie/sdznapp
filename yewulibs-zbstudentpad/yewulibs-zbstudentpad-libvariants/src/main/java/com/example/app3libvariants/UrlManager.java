package com.example.app3libvariants;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UrlManager {
    //测试
    public static final String CCC = "测试";
    public static final String FLAVOR1 = "_192.168.0.217";
    public static final int VERSION_CODE1 = getAppVersionCode();
    public static final String VERSION_NAME1 = getAppVersionName(2);
    public static final String BASE_ADDRESS1 = "http://192.168.0.217:7799/";
    public static final String BUGLY_CHANNEL1 = "2本地环境";


    //预生产
    public static final String YYY = "预生产";
    public static final String FLAVOR2 = "_znclass";
    public static final int VERSION_CODE2 = getAppVersionCode();
    public static final String VERSION_NAME2 = getAppVersionName(2);
    public static final String BASE_ADDRESS2 = "http://doc.znclass.com";
    public static final String BUGLY_CHANNEL2 = "预生产环境";

    //线上
    public static final String OOO = "线上";
    public static final String FLAVOR3 = "_znclass";
    public static final int VERSION_CODE3 = getAppVersionCode();
    public static final String VERSION_NAME3 = getAppVersionName(1);
    public static final String BASE_ADDRESS3 = "http://doc.znclass.com";
    public static final String BUGLY_CHANNEL3 = "线上环境";


    public static String versionCount() {
        return "01";
    }

    public String getTinkerVersionCode() {
        //每次发新版本/热更新这个值都要增加
        return "1";
    }

    public static String getAppVersionPre() {
        return "v4.1s_";
    }

    // 获取 version name
    public static String getAppVersionName(int type) {
        String ver = getAppVersionPre();
        String today = new SimpleDateFormat("MMdd").format(new Date());
        return ver + today + '_' + versionCount() + '_' + type;
    }

    public static int getAppVersionCode() {
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date()) + versionCount();
        return Integer.parseInt(today);
    }
}
