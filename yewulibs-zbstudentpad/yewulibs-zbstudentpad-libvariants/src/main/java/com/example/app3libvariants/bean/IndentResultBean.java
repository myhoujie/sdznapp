package com.example.app3libvariants.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/21
 */

public class IndentResultBean {


    /**
     * page : {"currentPage":1,"first":true,"last":true,"pageSize":10,"totalPageSize":1,"totalResultSize":8}
     */

    private PageBean page;
    private List<IndentBean> rows;

    public List<IndentBean> getRows() {
        return rows;
    }

    public void setRows(List<IndentBean> rows) {
        this.rows = rows;
    }

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public static class PageBean {
        /**
         * currentPage : 1
         * first : true
         * last : true
         * pageSize : 10
         * totalPageSize : 1
         * totalResultSize : 8
         */

        private int currentPage;
        private boolean first;
        private boolean last;
        private int pageSize;
        private int totalPageSize;
        private int totalResultSize;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalPageSize() {
            return totalPageSize;
        }

        public void setTotalPageSize(int totalPageSize) {
            this.totalPageSize = totalPageSize;
        }

        public int getTotalResultSize() {
            return totalResultSize;
        }

        public void setTotalResultSize(int totalResultSize) {
            this.totalResultSize = totalResultSize;
        }
    }
}