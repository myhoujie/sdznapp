package com.example.app3libvariants.bean;

import java.io.Serializable;


public class UserBean implements Serializable {
    private static final long serialVersionUID = -6542134942264304245L;


    /**
     * createTime : 2020-06-15 15:22:58
     * updateTime : 2020-06-15 15:23:01
     * id : 1111111
     * name : 赵森
     * sort : 1
     * schoolId : 24
     * schoolName : 清华大学
     * gender : 1
     * status : 1
     * isDelete : 0
     * deleteTime :
     * schoolYear : 2019
     * avatar : 123456
     * gradeId : 7
     * gradeName : 七年级（六三制）
     * classInfoId : 24
     * className : aaa
     * province :
     * city :
     * district :
     * districtId : 0
     * registerSource : 0
     * account : 11110000
     * mobile : 15168802751
     * email :
     * qq :
     * weixin :
     * freeznTime :
     * password : 123456
     * zbStatus : 1
     * levelId : 0
     * levelName :
     * fzxStatus : 1
     * educationId : 0
     * educationName :
     * studyCoin : 0
     */

    private String createTime;
    private String updateTime;
    private String id;
    private String name;
    private String schoolName;
    private String deleteTime;
    private String avatar;
    private int gradeId;
    private String gradeName;
    private String className;
    private String province;
    private String city;
    private String district;
    private String account;
    private String mobile;
    private String email;
    private String qq;
    private String weixin;
    private String freeznTime;
    private String password;
    private int levelId;
    private String levelName;
    private int fzxStatus;
    private int educationId;
    private String educationName;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUserId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }


    public String getPicImg() {
        return avatar;
    }

    public void setPicImg(String avatar) {
        this.avatar = avatar;
    }

    public int getGrade() {
        return gradeId;
    }

    public void setGrade(int gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getClasses() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getUserName() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getFreeznTime() {
        return freeznTime;
    }

    public void setFreeznTime(String freeznTime) {
        this.freeznTime = freeznTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSubjectId() {
        return levelId;
    }

    public void setSubjectId(int levelId) {
        this.levelId = levelId;
    }

    public String getSubjectName() {
        return levelName;
    }

    public void setSubjectName(String levelName) {
        this.levelName = levelName;
    }

    public int getFzxStatus() {
        return fzxStatus;
    }

    public void setFzxStatus(int fzxStatus) {
        this.fzxStatus = fzxStatus;
    }

    public int getEducationId() {
        return educationId;
    }

    public void setEducationId(int educationId) {
        this.educationId = educationId;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }
}