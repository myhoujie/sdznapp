package com.example.app3libvariants.bean;

import java.util.List;

public class RecmdInfoBean {

    /**
     * classId : 0
     * courseId : 46
     * courseMemberList : []
     * courseName : 企业人力资源管理师二级押题串讲学习班
     * currentPrice : 0.01
     * isavaliable : 0
     * lessionNum : 4
     * logo : /images/upload/course/20161222/1482394345278.jpg
     * loseType : 0
     * orderNum : 0
     * pageBuycount : 722
     * pageViewcount : 2292
     * recommendId : 2
     * sellType : COURSE
     * sourcePrice : 0.01
     * subjectId : 0
     * title : 考前论文辅导+押题串讲
     */

    private int classId;
    private int courseId;
    private String courseName;
    private double currentPrice;
    private int isavaliable;
    private int lessionNum;
    private String logo;
    private int loseType;
    private int orderNum;
    private int pageBuycount;
    private int pageViewcount;
    private int recommendId;
    private String sellType;
    private double sourcePrice;
    private int subjectId;
    private String title;
    private long liveBeginTime;
    private long liveEndTime;
    private int packageType;
    private List<?> courseMemberList;

    // TODO: 2017-11-29 add value
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public int getLessionNum() {
        return lessionNum;
    }

    public void setLessionNum(int lessionNum) {
        this.lessionNum = lessionNum;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getLoseType() {
        return loseType;
    }

    public void setLoseType(int loseType) {
        this.loseType = loseType;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getPageBuycount() {
        return pageBuycount;
    }

    public void setPageBuycount(int pageBuycount) {
        this.pageBuycount = pageBuycount;
    }

    public int getPageViewcount() {
        return pageViewcount;
    }

    public void setPageViewcount(int pageViewcount) {
        this.pageViewcount = pageViewcount;
    }

    public int getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(int recommendId) {
        this.recommendId = recommendId;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public double getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(double sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<?> getCourseMemberList() {
        return courseMemberList;
    }

    public long getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(long liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public long getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(long liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public void setCourseMemberList(List<?> courseMemberList) {
        this.courseMemberList = courseMemberList;
    }

    public int getPackageType() {
        return packageType;
    }

    public void setPackageType(int packageType) {
        this.packageType = packageType;
    }
}