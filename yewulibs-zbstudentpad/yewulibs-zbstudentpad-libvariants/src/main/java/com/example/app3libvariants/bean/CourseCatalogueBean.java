package com.example.app3libvariants.bean;

import java.io.Serializable;
import java.util.List;
/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseCatalogueBean implements Serializable {
    private static final long serialVersionUID = -6386204602855160792L;
    /**
     * classId : 0
     * courseId : 76
     * courseKpointList : [{"addTime":"2016-06-24 16:25:04","childKpoints":[],"courseId":76,"fileType":"VIDEO","free":1,"isavaliable":0,"kpointAtlasesList":[],"kpointId":251,"kpointList":[],"kpointType":0,"name":"第一章：JDK核心API","pageCount":0,"parentId":0,"playCount":18,"playTime":"","queryLimitNum":0,"sort":0,"supplier":"","teacherId":0,"videoType":"INXEDUCLOUD","videoUrl":"e5fe03541b9de5ffdf5d9f5453f510a8|drmkeytempplay33J47|ucodetempplayP8g14"},{"addTime":"2016-06-24 16:26:41","childKpoints":[],"courseId":76,"fileType":"VIDEO","free":1,"isavaliable":0,"kpointAtlasesList":[],"kpointId":252,"kpointList":[],"kpointType":1,"name":"第一讲：语言核心包","openType":"WEB","pageCount":0,"parentId":251,"playCount":56,"playTime":"","queryLimitNum":0,"sort":0,"teacherId":0,"videoType":"INXEDUCLOUD","videoUrl":"d105ba85ee6850800b163e9509ddb1e4|drmkeytempplay99A46|ucodetempplayiqjmT"},{"addTime":"2016-06-24 16:26:42","childKpoints":[],"courseId":76,"fileType":"AUDIO","free":2,"isavaliable":0,"kpointAtlasesList":[],"kpointId":253,"kpointList":[],"kpointType":1,"name":"第二讲：异常处理","openType":"WEB","pageCount":0,"parentId":251,"playCount":4,"playTime":"","queryLimitNum":0,"sort":0,"teacherId":0,"videoType":"INXEDUCLOUD","videoUrl":"/images/upload/courseKpoint/audio/20161224/1489112618140.mp3"}]
     * courseName : JDK核心API与JavaSE
     * currentPrice : 0.01
     * isavaliable : 0
     * lessionNum : 10
     * logo : /images/upload/course/20161222/1482391451927.jpg
     * loseType : 0
     * pageBuycount : 89
     * pageViewcount : 642
     * recommendId : 0
     * sourcePrice : 50
     * subjectId : 263
     * teacherList : [{"career":"上海师范大学法学院副教授、清华大学法学博士。自2004年至今已有9年的司法考试培训经验。长期从事司法考试辅导，深知命题规律，了解解题技巧。内容把握准确，授课重点明确，层次分明，调理清晰，将法条法理与案例有机融合，强调综合，深入浅出。","createTime":"2015-04-03 14:23:06","customerId":0,"education":"上海师范大学法学院副教授","id":81,"invalid":false,"isStar":1,"name":"李立","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","sort":9,"status":0,"subjectId":209,"updateTime":"2015-09-15 14:18:48"},{"career":"政治学博士、管理学博士后，北京师范大学马克思主义学院副教授。多年来总结出了一套行之有效的应试技巧与答题方法，针对性和实用性极强，能帮助考生在轻松中应考，在激励的竞争中取得高分，脱颖而出。","createTime":"2015-04-03 14:13:51","customerId":0,"education":"长期从事考研政治课讲授和考研命题趋势与应试对策研究。考研辅导新锐派的代表。","id":76,"invalid":false,"isStar":1,"name":"王健化","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297977255.jpg","sort":0,"status":0,"subjectId":202,"updateTime":"2015-09-15 14:19:38"}]
     * title : JDK核心API与JavaSE（以T-DMS V1项目贯穿）
     */

    private int classId;
    private int courseId;
    private String courseName;
    private double currentPrice;
    private int isavaliable;
    private int lessionNum;
    private String logo;
    private int loseType;
    private int pageBuycount;
    private int pageViewcount;
    private int recommendId;
    private double sourcePrice;
    private int subjectId;
    private String subjectName;
    private String title;
    private List<CourseKpointListBean> courseKpointLits;
    private List<TeacherListBean> teacherList;
    private String teacherName;


    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public int getLessionNum() {
        return lessionNum;
    }

    public void setLessionNum(int lessionNum) {
        this.lessionNum = lessionNum;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getLoseType() {
        return loseType;
    }

    public void setLoseType(int loseType) {
        this.loseType = loseType;
    }

    public int getPageBuycount() {
        return pageBuycount;
    }

    public void setPageBuycount(int pageBuycount) {
        this.pageBuycount = pageBuycount;
    }

    public int getPageViewcount() {
        return pageViewcount;
    }

    public void setPageViewcount(int pageViewcount) {
        this.pageViewcount = pageViewcount;
    }

    public int getRecommendId() {
        return recommendId;
    }

    public double getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(double sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public void setSourcePrice(int sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CourseKpointListBean> getCourseKpointList() {
        return courseKpointLits;
    }

    public void setCourseKpointList(List<CourseKpointListBean> courseKpointList) {
        this.courseKpointLits = courseKpointList;
    }

    public List<TeacherListBean> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<TeacherListBean> teacherList) {
        this.teacherList = teacherList;
    }
}
