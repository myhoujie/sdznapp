package com.example.app3libvariants.network.api;

import android.content.Intent;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.app.App2;

import rx.functions.Func1;

/**
 * 描述：
 * - 通用请求返回结果处理
 * 新接口  code=1时，且 errorCode=4002 时 登录失效
 */
public class ResponseNewFunc<T> implements Func1<ResultBean<T>, T> {

    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;
    public static final String AUTO_LOGIN = "autoLogin";

    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0==httpResult.getCode()) {//httpResult.isSuccess()
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getCode()) {//登录失效
                SPManager.changeLogin(App2.get(), false);
                AppManager.getAppManager().finishActivity(AppUtils.getAppPackageName() + ".hs.act.mainactivity");
                ToastUtils.showShort(httpResult.getMessage() + "");
//                IntentController.toMain(App.mContext,false);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.mainactivity");//MainActivity.class
                intent.putExtra(AUTO_LOGIN, false);
                App2.get().startActivity(intent);
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMessage() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            }
        }
    }
}