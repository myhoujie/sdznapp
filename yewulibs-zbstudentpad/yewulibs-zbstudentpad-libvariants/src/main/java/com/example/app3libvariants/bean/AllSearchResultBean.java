package com.example.app3libvariants.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/20
 */
public class AllSearchResultBean {


    private PageBean page;
    private List<CourseListBean> liveCourseList;

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public List<CourseListBean> getLiveCourseList() {
        return liveCourseList;
    }

    public void setLiveCourseList(List<CourseListBean> liveCourseList) {
        this.liveCourseList = liveCourseList;
    }


}
