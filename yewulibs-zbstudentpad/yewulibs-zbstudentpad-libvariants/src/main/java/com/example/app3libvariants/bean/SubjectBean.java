package com.example.app3libvariants.bean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public class SubjectBean {

    /**
     * subjectId : 203
     * subjectName : 人力二级
     */

    private int id;
    private String name;

    private String createTime;
    private String logo;
    private int parentId;
    private int sort;
    private int status;
    private String type;

    public SubjectBean(int subjectId, String subjectName) {
        this.id = subjectId;
        this.name = subjectName;
    }

    public int getSubjectId() {
        return id;
    }

    public void setSubjectId(int subjectId) {
        this.id = subjectId;
    }

    public String getSubjectName() {
        return name;
    }

    public void setSubjectName(String subjectName) {
        this.name = subjectName;
    }
}