package com.example.app3libvariants.bean;

import java.io.Serializable;
import java.util.List;

public class CourseKpointListBean implements Serializable {
    private static final long serialVersionUID = 3613056944647189117L;
    /**
     * addTime : 2016-06-24 16:25:04
     * childKpoints : []
     * courseId : 76
     * fileType : VIDEO
     * free : 1
     * isavaliable : 0
     * kpointAtlasesList : []
     * kpointId : 251
     * kpointList : []
     * kpointType : 0  0目录1视频
     * name : 第一章：JDK核心API
     * pageCount : 0
     * parentId : 0
     * playCount : 18
     * playTime :
     * queryLimitNum : 0
     * sort : 0
     * supplier :
     * teacherId : 0
     * videoType : INXEDUCLOUD
     * videoUrl : e5fe03541b9de5ffdf5d9f5453f510a8|drmkeytempplay33J47|ucodetempplayP8g14
     * openType : WEB
     */


    private String addTime;
    private int courseId;
    private String fileType;//LIVE
    private int isFree;
    private int isavaliable;
    private int kpointId;
    private int kpointType;
    private String name;
    private int pageCount;
    private int parentId;
    private int playCount;
    private String playTime;
    private int queryLimitNum;
    private int sort;
    private String supplier;
    private int teacherId;
    private String videoType;
    private String videoUrl;
    private String openType;
    private String liveBeginTime;
    private String liveEndTime;
    private String state;
    private String kpointStatus;//直播状态的
    private TeacherListBean teacher;
    private List<?> childKpoints;
    private List<?> kpointAtlasesList;
    private List<?> kpointList;
    private String teacherName;
    private boolean isStudy;
    private String liveStates;
    private String videoLenth;
    private List<CourseKpointListBean> kpointVoList;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<CourseKpointListBean> getKpointVoList() {
        return kpointVoList;
    }

    public void setKpointVoList(List<CourseKpointListBean> kpointVoList) {
        this.kpointVoList = kpointVoList;
    }

    private List<CourseFileBean> kpointFileList;

    public List<CourseFileBean> getKpointFileList() {
        return kpointFileList;
    }

    public void setKpointFileList(List<CourseFileBean> kpointFileList) {
        this.kpointFileList = kpointFileList;
    }

    public String getVideoLenth() {
        return videoLenth;
    }

    public void setVideoLenth(String videoLenth) {
        this.videoLenth = videoLenth;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }


    public String getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(String liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public boolean isStudy() {
        return isStudy;
    }

    public void setStudy(boolean study) {
        isStudy = study;
    }

    public String getLiveStates() {
        return liveStates;
    }

    public void setLiveStates(String liveStates) {
        this.liveStates = liveStates;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getFree() {
        return isFree;
    }

    public void setFree(int isFree) {
        this.isFree = isFree;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public int getKpointId() {
        return kpointId;
    }

    public void setKpointId(int kpointId) {
        this.kpointId = kpointId;
    }

    public int getKpointType() {
        return kpointType;
    }

    public void setKpointType(int kpointType) {
        this.kpointType = kpointType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    public int getQueryLimitNum() {
        return queryLimitNum;
    }

    public void setQueryLimitNum(int queryLimitNum) {
        this.queryLimitNum = queryLimitNum;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }


    public String getKpointStatus() {
        return kpointStatus;
    }

    public void setKpointStatus(String kpointStatus) {
        this.kpointStatus = "改成现在需要的";
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public TeacherListBean getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherListBean teacher) {
        this.teacher = teacher;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public List<?> getChildKpoints() {
        return childKpoints;
    }

    public void setChildKpoints(List<?> childKpoints) {
        this.childKpoints = childKpoints;
    }

    public List<?> getKpointAtlasesList() {
        return kpointAtlasesList;
    }

    public void setKpointAtlasesList(List<?> kpointAtlasesList) {
        this.kpointAtlasesList = kpointAtlasesList;
    }

    public List<?> getKpointList() {
        return kpointList;
    }

    public void setKpointList(List<?> kpointList) {
        this.kpointList = kpointList;
    }
}