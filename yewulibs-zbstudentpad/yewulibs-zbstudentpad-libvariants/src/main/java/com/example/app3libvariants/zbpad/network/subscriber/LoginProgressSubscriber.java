package com.example.app3libvariants.zbpad.network.subscriber;

import android.content.Context;

import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.exception.CodeException;
import com.sdzn.core.network.exception.HttpTimeException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.subscriber.ProgressSubscriber;

/**
 * 登录处抛异常的处理  znxt
 */
public class LoginProgressSubscriber<T> extends ProgressSubscriber<T> {


    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context) {
        super(mSubscriberOnNextListener, context);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress) {
        super(mSubscriberOnNextListener, context, isShowProgress);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress, String showMsg) {
        super(mSubscriberOnNextListener, context, isShowProgress, showMsg);
    }

    public LoginProgressSubscriber(SubscriberOnNextListener<T> subscriberOnNextListener) {
        super(subscriberOnNextListener);
    }

    @Override
    public void onError(Throwable e) {
        if (isShowProgress) {
            dismissProgressDialog();
        }
        if (e instanceof ApiException) {
            mSubscriberOnNextListener.onFail(e);
        } else if (e instanceof HttpTimeException) {
            HttpTimeException exception = (HttpTimeException) e;
            mSubscriberOnNextListener.onFail(new ApiException(exception, CodeException.RUNTIME_ERROR));
        } else {
//            mSubscriberOnNextListener.onFail(new ApiException(e, CodeException.UNKNOWN_ERROR));
        }
    }

    @Override
    public void onNext(T t) {
        mSubscriberOnNextListener.onNext(t);
    }

}