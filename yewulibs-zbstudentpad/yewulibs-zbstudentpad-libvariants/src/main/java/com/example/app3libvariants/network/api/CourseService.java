package com.example.app3libvariants.network.api;

import com.example.app3libvariants.bean.AddToCartBean;
import com.example.app3libvariants.bean.BannerInfoBean;
import com.example.app3libvariants.bean.CollectBean;
import com.example.app3libvariants.bean.CourseDetailBean;
import com.example.app3libvariants.bean.CourseFileBean;
import com.example.app3libvariants.bean.CourseIncludeBean;
import com.example.app3libvariants.bean.CourseListBean;
import com.example.app3libvariants.bean.CourseListRows;
import com.example.app3libvariants.bean.HomeRecommedBean;
import com.example.app3libvariants.bean.IndentResultBean;
import com.example.app3libvariants.bean.MineListRows;
import com.example.app3libvariants.bean.MineTaskBean;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;
import com.example.app3libvariants.bean.OrderDetail;
import com.example.app3libvariants.bean.OrderInfoBean;
import com.example.app3libvariants.bean.PayInfoBean;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.ShoppingCartBean;
import com.example.app3libvariants.bean.SubjectBean;
import com.example.app3libvariants.bean.TopicBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.zbpad.network.api.ApiInterface;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 描述：
 * - 课程相关接口
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public interface CourseService {
    /*  -----  -----new新接口  -----  -----*/

    //发送验证码  **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.SEND_VERIFY)
    Observable<ResultBean<Object>> getSendVerify(@Field("telephone") String phone);


    //密码登录   **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.USER_LOGIN)
    Observable<ResultBean<UserLoginBean>> login(@Field("username") String username, @Field("password") String password,
                                                @Field("uniqueNumber") String uniqueNumber, @Field("clientType") String clientType);


    //验证码登录  **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.USER_CODE)
    Observable<ResultBean<UserLoginBean>> loginCode(@Field("telephone") String phone, @Field("code") String code,
                                                    @Field("uniqueNumber") String uniqueNumber, @Field("clientType") String clientType);


    //更新学生的学制、学段、年级信息  **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.UPDATE_LEVEL_GRADE)
    Observable<ResultBean<UserBean>> updateEducaLevel(@Field("educationId") String educationId, @Field("educationName") String educationName,
                                                      @Field("levelId") String levelId, @Field("levelName") String levelName,
                                                      @Field("gradeId") String gradeId, @Field("gradeName") String gradeName);

    //拼课堂首页(精品课,专题课)
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.HOME_RECOMMENDED)
    Observable<ResultBean<HomeRecommedBean>> getHomeRecommendCourse(@Body RequestBody body);

    //根据学段选学科  **form urlencoded**
    @GET(ApiInterface.SELECT_SUBJECT_LIST)
    Observable<ResultBean<List<SubjectBean>>> getSubjectSpell(@Query("levelId") int levelId);


    //精品课列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.QUERY_APP_RECOMMENDED)
    Observable<ResultBean<CourseListRows>> getRecommendCourse(@Body RequestBody body);

    //专题名称列表  **form urlencoded**
    @GET(ApiInterface.SELECT_TOPIC_LIST)
    Observable<ResultBean<TopicBean>> getTopicSpell(@Query("status") int status);

    //专题课列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.QUERY_APP_TOPIC)
    Observable<ResultBean<CourseListBean>> getTopicCourse(@Body RequestBody body);

    //查询购物车
    @GET(ApiInterface.QUERY_SHOPPING_CART_URL)
    Observable<ResultBean<List<ShoppingCartBean.ShopCartListBean>>> queryShoppingCart();


    //根据条件进行 查询购物车
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.QUERY_SHOPPING_CART_FILTER)
    Observable<ResultBean<ShoppingCartBean>> queryShoppingCart(@Body RequestBody body);


    //删除购物车
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.DELETE_SHOPPING_CART_URL)
    Observable<ResultBean<Object>> delShoppingCart(@Body RequestBody body);

    //加入购物车
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ADD_SHOPPING_CART_URL)
    Observable<ResultBean<AddToCartBean>> addShoppingCart(@Body RequestBody body);

    //直播项目
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ENTER_LIVE_ROOM)
    Observable<ResultBean<NewLiveInfo>> getNewLiveInfo(@Body RequestBody body);

    //回放
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.VIEW_PLAY_BACK)
    Observable<ResultBean<NewVideoInfo>> getNewReplayInfo(@Body RequestBody body);

    //点播
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.VIEW_ONDEMAND)
    Observable<ResultBean<NewVideoInfo>> getNewVideoInfo(@Body RequestBody body);

    //添加收藏
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ADD_FAVORITE_URL)
    Observable<ResultBean<Object>> addFavorite(@Body RequestBody body);

    //查询我的收藏
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.UP_COLLECT)
    Observable<ResultBean<CollectBean>> upDataCollection(@Body RequestBody body);

    //删除我的收藏
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.DEL_COLLECTION)
    Observable<ResultBean<Object>> delCollection(@Body RequestBody body);

    //报名
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_APPLY)
    Observable<ResultBean<Object>> getIsPurchase(@Body RequestBody body);

    //获取组合课程详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_PACKAGE_DETAILS)
    Observable<ResultBean<CourseDetailBean>> getCoursePackageDetail(@Body RequestBody body);

    //获取普通课程详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_DETAILS_NORMAL)
    Observable<ResultBean<CourseIncludeBean>> getCourseDetailNormal(@Body RequestBody body);

    //提交订单
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.SUBMIT_ORDER_URL)
    Observable<ResultBean<OrderInfoBean>> submitOrder(@Body RequestBody body);

    //获取支付信息
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ORDER_PAY_URL)
    Observable<ResultBean<PayInfoBean>> getOrderPayInfo(@Body RequestBody body);

    /**
     * 订单列表
     */
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ORDER_LIST)
    Observable<ResultBean<IndentResultBean>> orderList(@Body RequestBody body);

    //获取订单详情
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.ORDER_DETAILS_URL)
    Observable<ResultBean<OrderDetail>> getOrderDetails(@Body RequestBody body);

    //今日直播
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.CHANGE_TODY)
    Observable<ResultBean<MineListRows>> getTodyCourse(@Body RequestBody body);

    //课程详情资料列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.APP_RESOURCE)
    Observable<ResultBean<List<CourseFileBean>>> getAppResource(@Body RequestBody body);

    //全部课程
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_ALL_COURSE)
    Observable<ResultBean<MineListRows>> getALLCourse(@Body RequestBody body);

    //近期播放
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_RECENTLY)
    Observable<ResultBean<MineListRows>> getRecentlyCourse(@Body RequestBody body);

    //找回密码没有绑定手机  **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.RETRIEVE_PASSWORD)
    Observable<ResultBean<Object>> getRetrievePassword(@Field("account") String account, @Field("code") String code,
                                                       @Field("mobile") String mobile);


    //修改个人资料  **form urlencoded**    className avatar  name  schoolName
    @FormUrlEncoded
    @POST(ApiInterface.CHANGE_USER_INFO)
    Observable<ResultBean<UserBean>> getChangeInfo(@Field("className") String className, @Field("avatar") String avatar,
                                                   @Field("name") String name, @Field("schoolName") String schoolName);

    //与后台生成的账号绑定  **form urlencoded**
    @FormUrlEncoded
    @POST(ApiInterface.BIND_ACCOUNT_USER)
    Observable<ResultBean<UserLoginBean>> bindAccount(@Field("account") String account);

    //首页banner图
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.QUERY_BANNER)
    Observable<ResultBean<List<BannerInfoBean>>> getBanner(@Body RequestBody body);

    //选课中心 拼课堂根据学科获取科目，根据name搜索
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.COURSE_SELECTION)
    Observable<ResultBean<CourseListRows>> getCourseSelection(@Body RequestBody body);

    //学校中心
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(ApiInterface.CHANGE_SCHOOL_CENTER)
    Observable<ResultBean<CourseListRows>> getCourseSchool(@Body RequestBody body);

    //我的作业
    @FormUrlEncoded
    @POST(ApiInterface.POST_MINE_TASK)
    Observable<ResultBean<MineTaskBean>> getMineTask(@Field("studentId") String studentId, @Field("type") String type,@Field("limit") int limit,@Field("page") int page);

    //学生作答校验
    @FormUrlEncoded
    @POST(ApiInterface.POST_VALIDATE_TIME)
    Observable<ResultBean<Object>> getValidate(@Field("homeworkId") String homeworkId, @Field("studentId") String studentId,@Field("type") String type,@Field("terminal") String terminal);



}

