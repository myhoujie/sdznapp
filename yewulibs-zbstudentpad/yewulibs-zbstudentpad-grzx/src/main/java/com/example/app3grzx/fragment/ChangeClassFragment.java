package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.UpdateAccountPresenter;
import com.example.app3grzx.view.UpdateAccountView;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * zs
 */

public class ChangeClassFragment extends BaseMVPFragment<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {
    public static final String KEY_STUDENTNAME = "classes";
    public static final int NAME_MAX_LENGTH = 8;

    EditText etName;
    TitleBar titleBar;
    private String studentName;

    public static ChangeClassFragment newInstance(String studentName) {
        Bundle args = new Bundle();
        args.putString(KEY_STUDENTNAME, studentName);
        ChangeClassFragment fragment = new ChangeClassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_change_class;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        studentName = getArguments().getString(KEY_STUDENTNAME);
        if (!TextUtils.isEmpty(studentName)) {
            etName.setText(studentName);
            etName.setSelection(studentName.length());
        }
        etName = rootView.findViewById(R.id.et_name);
        titleBar = rootView.findViewById(R.id.title);
        rootView.findViewById(R.id.btn_certain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String className = etName.getText().toString().trim();
                if (TextUtils.isEmpty(className)) {
                    ToastUtils.showShort("班级不能为空");
                } else {
                    mPresenter.updateUserInfo(className,"","");
                }
            }
        });
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_CLASSES));
        onBackPressed();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
