package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.LiveRoomBean;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;
import com.example.app3libvariants.bean.VideoRoomBean;

/**
 * 描述：直播的和观看回放
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/7
 */

public interface LiveRoomView extends BaseView {

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void liveRoomInfoOnError(String msg);

    void getVideoRoomInfoSuccrss(NewVideoInfo videoRoomBean);

    void videoRoomInfoOnError(String msg);

    void getReplayInfoSuccess(NewVideoInfo info);

    void applySuccess(int type);
}
