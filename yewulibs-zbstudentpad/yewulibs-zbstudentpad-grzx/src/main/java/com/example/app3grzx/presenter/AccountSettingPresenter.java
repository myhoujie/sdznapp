package com.example.app3grzx.presenter;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.example.app3grzx.R;
import com.example.app3grzx.view.AccountSettingView;
import com.example.app3libpublic.utils.CacheUtils;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.api.AccountService;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseFunc;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.ImageUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class AccountSettingPresenter extends BasePresenter<AccountSettingView> {

    private File takeImageFile;
//
//    /**
//     * 修改性别
//     *
//     * @param sexType
//     */
//    public void changeSex(String sexType) {
//
//        Subscription subscription = RestApi.getInstance()
//                .create(AccountService.class)
//                .changeSex(sexType)
//                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
//                .map(new ResponseFunc<Object>())
//                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {
//
//                    @Override
//                    public void onNext(Object o) {
//                        getView().changeSexSuccess();
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        String msg = mActivity.getString(R.string.request_failure_try_again);
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
//                        getView().changeSexError(msg);
//                    }
//                }, mActivity, true, "请求中..."));
//        addSubscribe(subscription);
//
//    }

    /**
     * 修改头像
     *
     * @param path
     */
    public void upAvatar(String path, boolean isAlbum) {
        // 创建 RequestBody，用于封装构建RequestBody
        Bitmap bitmap = ImageUtils.comp(path);
        //判断是拍照还是再相册
        if (isAlbum) {
            takeImageFile = new File(CacheUtils.getAvatarCache(), "head.jpg");
            path = takeImageFile.getAbsolutePath();
        }
        ImageUtils.saveBitmapFile(bitmap, path);
      /*  RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), new File(path));

        // MultipartBody.Part  和后端约定好Key，这里的partName是用image
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploadfile", "head.jpg", requestFile);*/
        File file = new File(path);
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", "head.jpg", RequestBody.create(MediaType.parse("image/*"), file))
                .build();

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .upUserPhoto(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseFunc<Object>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        String photo = o.toString();
                        if (photo != null) {
                            changeUserPhoto(photo);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        FileUtils.deleteFile(takeImageFile);
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().changePhotoError(msg);

                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);
    }

    private void changeUserPhoto(final String strPhoto) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getChangeInfo("", strPhoto, "", "")// className avatar  name  schoolName
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean o) {
                        FileUtils.deleteFile(takeImageFile);
                        getView().changePhotoSuccess(strPhoto);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().changePhotoError(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    /**
     * 完善个人信息     更新学制,学段,年级信息
     *
     * @param
     */
    public void perfectAccount(String eduId, String eduName, String levelId, String levelName, String gradeId, String gradeName) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .updateEducaLevel(eduId, eduName, levelId, levelName, gradeId, gradeName)
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        getView().perfectAccountSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().perfectAccountFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    /**
     * 按行读取txt
     * 解析输入流，返回txt中的字符串
     *
     * @param is
     * @return
     * @throws Exception
     */
    private String readTextFromSDcard(InputStream is) throws Exception {
        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuffer buffer = new StringBuffer("");
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            buffer.append(str);
            buffer.append("\n");
        }
        return buffer.toString();
    }


    /**
     * 获取年级
     */
    /*public List<GradeJson> getGrade() {
        try {
            InputStream open = mActivity.getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }*/

//    /**
//     * 获取学段
//     */
//    public List<SectionBean> getSection() {
//        try {
//            InputStream open = mActivity.getResources().getAssets().open("school_phase.json");
//            byte[] buffer = new byte[open.available()];
//            int read = open.read(buffer);
//            if (read != 0) {
//                String json = new String(buffer, "utf-8");
//                return new Gson().fromJson(json, new TypeToken<List<SectionBean>>() {
//                }.getType());
//            }
//        } catch (Exception e) {
//            LogUtils.e(e);
//        }
//        return new ArrayList<>();
//    }

}

