package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.MobilePresenter;
import com.example.app3grzx.view.MobileView;
import com.example.app3libpublic.event.BindEvent;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;


/**
 * 绑定手机
 *
 * @author Reisen at 2017-12-07
 */

public class BindingMobileFragment extends BaseMVPFragment<MobileView, MobilePresenter> implements MobileView, View.OnClickListener {

    EditText mMobile;
    EditText mCode;
    Button bt_confirm;
    TextView tv_getCode;
    TitleBar titleBar;

    private UserBean userBean;
    private TextWatcher mWatcher;

    public static BindingMobileFragment newInstance() {
        return new BindingMobileFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_bindmobile;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        mMobile = rootView.findViewById(R.id.tv_mobile);
        mCode = rootView.findViewById(R.id.tv_code);
        bt_confirm = rootView.findViewById(R.id.bt_confirm);
        tv_getCode = rootView.findViewById(R.id.tv_getcode);
        titleBar = rootView.findViewById(R.id.title);
        intView();
    }

    private void initData() {
        userBean = SPManager.getUser();
    }

    private void intView() {
        tv_getCode.setOnClickListener(this);
        bt_confirm.setOnClickListener(this);
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String mobile = mMobile.getText().toString().trim();
                String code = mCode.getText().toString().trim();
                tv_getCode.setEnabled(!TextUtils.isEmpty(mobile));
                bt_confirm.setEnabled(!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(code));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        mMobile.addTextChangedListener(mWatcher);
        mCode.addTextChangedListener(mWatcher);
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        final String mobile = mMobile.getText().toString().trim();
        int id = v.getId();
        if (id == R.id.bt_confirm) {
            KeyboardUtils.hideSoftInput(getActivity());
            if (!checkPhone(mobile)) {
                return;
            }
            final String code = mCode.getText().toString().trim();
            mPresenter.confirm(mobile, code);
        } else if (id == R.id.tv_getcode) {
            if (!checkPhone(mobile)) {
                return;
            }
            mPresenter.gainCode(mobile, tv_getCode);
        }
    }

    private boolean checkPhone(String mobile) {
        if (mobile == null) {
            ToastUtils.showShort("请输入手机号");
            return false;
        }
        if (userBean == null) {
            userBean = SPManager.getUser();
        }
       /* if (userBean.isBundlingState() && TextUtils.equals(userBean.getMobile(), mobile)) {
            ToastUtils.showShort("新手机号不能与原手机号相同");
            return false;
        }*/
        return true;
    }

    @Override
    protected MobilePresenter createPresenter() {
        return new MobilePresenter();
    }

    @Override
    public void onSuccess() {
        String mobile = mMobile.getText().toString().trim();
        userBean.setMobile(mobile);
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        ToastUtils.showShort("绑定成功");
        onBackPressed();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onDestroy() {
        ToastUtils.showShort("123123123");
        super.onDestroy();
    }
}
