package com.example.app3grzx.presenter;

import android.text.TextUtils;

import com.example.app3grzx.R;
import com.example.app3grzx.view.UpdateAccountView;
import com.example.app3libvariants.bean.ResultBean;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.bean.UserLoginBean;
import com.example.app3libvariants.network.api.CourseService;
import com.example.app3libvariants.network.api.ResponseNewFunc;
import com.example.app3libvariants.zbpad.network.RestApi;
import com.example.app3libvariants.zbpad.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/16
 */
public class UpdateAccountPresenter extends BasePresenter<UpdateAccountView> {

    public void updateAccount(String bindAccount) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .bindAccount(bindAccount)//className avatar  name  schoolName
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean loginBean) {
                        if (loginBean != null) {
                            getView().updateAccountSuccess(loginBean.getUserDetail());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().updateAccountFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void updateUserInfo(String className, String name, String schoolName) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getChangeInfo(className, "", name, schoolName)//className avatar  name  schoolName
                .compose(TransformUtils.<ResultBean<UserBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {
                        getView().updateAccountSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().updateAccountFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }


}
