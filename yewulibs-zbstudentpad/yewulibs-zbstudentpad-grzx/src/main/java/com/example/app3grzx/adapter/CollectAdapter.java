package com.example.app3grzx.adapter;

import android.content.Context;
import android.view.View;

import com.example.app3grzx.R;
import com.example.app3libpublic.widget.SwipeItemLayout;
import com.example.app3libvariants.bean.CollectBean;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;

import java.util.List;

/**
 * 描述：我的收藏
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/10
 */

public class CollectAdapter extends BaseRcvAdapter<CollectBean.FavoriteListBean> {
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";
    private OnCollectListener onCollectListener;

    public CollectAdapter(Context context, List<CollectBean.FavoriteListBean> mList) {
        super(context, R.layout.item_collect, mList);
    }

    public void setOnCollectListener(OnCollectListener onCollectListener) {
        this.onCollectListener = onCollectListener;
    }

    @Override
    public void convert(final BaseViewHolder holder, final int position, final CollectBean.FavoriteListBean favoriteListBean) {
        holder.getView(R.id.main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favoriteListBean.isIslose()) {
                    onCollectListener.mainListener(position);
                }
            }
        });
        holder.getView(R.id.bt_cancel_collect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeItemLayout swipeItemLayout = holder.getView(R.id.swipeItemLayout);
                swipeItemLayout.close();
                onCollectListener.cancelListener(position);
            }
        });
        holder.setInVisible(R.id.tv_liveing_time, false);
        holder.setText(R.id.tv_class, favoriteListBean.getCourseName());

        holder.setImageView(R.id.img_video, "" + favoriteListBean.getLogo());
        holder.setText(R.id.tv_course_video_money, "¥" + String.valueOf(favoriteListBean.getCurrentPrice()));

        //通过sellType判断video是组合还是单科，sellType为PACKAGE时组合，sellType为LIVE时单科直播，sellType为COURSE时单科点播
        if (TYPE_PACKAGE.equalsIgnoreCase(favoriteListBean.getSellType())) {
            holder.setText(R.id.tv_subject_teacher, "科目: " + favoriteListBean.getSubjectName() != null ? favoriteListBean.getSubjectName() : "");
            switch (favoriteListBean.getPackageType()) {
                case 1:
                    holder.setInVisible(R.id.tv_liveing_time, false);
                    holder.setText(R.id.tv_liveing_time, "开课时间：" + favoriteListBean.getLiveBeginTime());

                    holder.setText(R.id.tv_status, "直");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
                    break;
                case 2:
                    holder.setText(R.id.tv_status, "点");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);

                    break;
            }

        } else if (TYPE_COURSE.equalsIgnoreCase(favoriteListBean.getSellType())) {
            holder.setText(R.id.tv_status, "点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
            holder.setText(R.id.tv_subject_teacher, "讲师: " + favoriteListBean.getTeacherName());
        } else if (TYPE_LIVE.equalsIgnoreCase(favoriteListBean.getSellType())) {
            holder.setInVisible(R.id.tv_liveing_time, true);
            holder.setText(R.id.tv_liveing_time, "开课时间：" + favoriteListBean.getLiveBeginTime());

            holder.setText(R.id.tv_status, "直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
            holder.setText(R.id.tv_subject_teacher, "讲师: " + favoriteListBean.getTeacherName());

        }
        //判断收藏的课程是否有效    点击事件处也需要
        if (favoriteListBean.isIslose()) {//有效
            holder.setVisible(R.id.iv_lose_efficacy, false);
        } else {//无效
            holder.setVisible(R.id.iv_lose_efficacy, true);
        }
    }

    public interface OnCollectListener {
        void cancelListener(int position);

        void mainListener(int position);
    }
}
