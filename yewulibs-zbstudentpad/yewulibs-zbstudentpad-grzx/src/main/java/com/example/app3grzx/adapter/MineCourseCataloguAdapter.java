package com.example.app3grzx.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app3grzx.R;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libvariants.bean.CourseKpointListBean;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;

import java.util.List;

/**
 * 描述：
 * - 单一课程章节adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class MineCourseCataloguAdapter extends BaseRcvAdapter<CourseKpointListBean> {
    private int courseType;
    private LiveCoursePlayerListener listener;
    private boolean isFree;
    private int courseIsvisiable = 1;//传过来课程的失效或有效的标志
    private boolean isRelationLiveCourse;


    public MineCourseCataloguAdapter(Context context, int courseType, List mList, int courseIsvisiable, boolean isFree, boolean isRelationLiveCourse) {
        super(context, mList);
        this.courseType = courseType;
//        this.courseIsvisiable = courseIsvisiable;
        this.isRelationLiveCourse = isRelationLiveCourse;
        this.isFree = isFree;
    }

    public void setListener(LiveCoursePlayerListener listener) {
        this.listener = listener;
    }


    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getKpointType();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        switch (viewType) {
            case 0:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_chapter);
                break;
            case 1:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_mine_course_catalogue_child);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, final CourseKpointListBean kpointBean) {
        switch (holder.getItemViewType()) {
            case 0:
                holder.setText(R.id.tv_chacpter, kpointBean.getName());
                break;
            case 1:
                holder.setText(R.id.tv_catalogue, kpointBean.getName());
                if (courseType == CourseCons.Type.LIVING) {
                    holder.setVisible(R.id.tv_date, true);
                    holder.setVisible(R.id.tv_teacher, true);
                    String day = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "yyyy-MM-dd");
                    String strartTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "HH:mm");
                    String endDay = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "yyyy-MM-dd");
                    String endTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "HH:mm");
                    if (day.equals(endDay)) {
                        holder.setText(R.id.tv_date, day + " " + strartTime + "~" + endTime);
                    } else {
                        holder.setText(R.id.tv_date, day + " " + strartTime + "~" + endDay + " " + endTime);
                    }
                    holder.setText(R.id.tv_teacher, "讲师:"+kpointBean.getTeacherName());
                    holder.setText(R.id.tv_course_isok, CourseCons.LiveStatus.upStatus(kpointBean.getLiveStates()));
                    switch (kpointBean.getLiveStates()) {//直播
                        case CourseCons.LiveStatus.LIVE_NOTBEGIN_NEW://未开始
                            setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                            break;
                        case CourseCons.LiveStatus.LIVE_LIVING_NEW://观看直播
                            setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");
                            break;
                        case CourseCons.LiveStatus.LIVE_FINISHED_NEW://已结束
                            setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                            break;
                        case CourseCons.LiveStatus.LIVE_REPLAY_NEW://查看回放
                            setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_replay, "#1A70AE");
                            break;
                        default:
                            holder.setText(R.id.tv_course_isok, "敬请期待");
                            setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                            break;
                    }
                } else {
                    holder.setText(R.id.tv_date, kpointBean.getVideoLenth()+"分钟");
                    holder.setText(R.id.tv_teacher, kpointBean.getTeacherName());
                    holder.setVisible(R.id.tv_teacher, true);
                    if (isFree) {
                        holder.setText(R.id.tv_course_isok, "开始学习");
                        setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");
                    } else {
                        holder.setText(R.id.tv_course_isok, "开始学习");
                        setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");

                    }
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //判断是直播还是点播
                        if (courseType == CourseCons.Type.LIVING) {
                            //此处需要判断是点播还是直播的界面
                            if (CourseCons.LiveStatus.LIVE_LIVING_NEW.equalsIgnoreCase(kpointBean.getLiveStates())) {
                                listener.ontoLiving(kpointBean);
                            } else if (CourseCons.LiveStatus.LIVE_REPLAY_NEW.equalsIgnoreCase(kpointBean.getLiveStates())) {
                                listener.ontoReplay(kpointBean);
                            }
                        } else {
                            if (courseIsvisiable == 1) {
                                if (isRelationLiveCourse) {
                                    listener.ontoReplay(kpointBean);
                                } else {
                                    listener.ontoVideo(kpointBean);
                                }
                            }
                        }
                    }
                });
                if (kpointBean.getFree() == 1) {
                    holder.setVisible(R.id.tv_audition, true);
                } else {
                    holder.setVisible(R.id.tv_audition, false);
                }
                break;
            default:
                break;
        }
    }

    private void setImgviewtoText(TextView textView, int imgId, String colorStr) {
        textView.setTextColor(Color.parseColor(colorStr));
        textView.setBackgroundResource(imgId);
    }

    public interface LiveCoursePlayerListener {
        void ontoLiving(CourseKpointListBean kpointBean);//直播

        void ontoVideo(CourseKpointListBean kpointBean);//点播

        void ontoReplay(CourseKpointListBean kpointBean);//回放
    }
}
