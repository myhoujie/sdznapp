package com.example.app3grzx.fragment;

import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app3grzx.R;
import com.example.app3grzx.adapter.CourseCataloguAdapter;
import com.example.app3grzx.adapter.CourseGroupCatalogueAdapter;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libvariants.bean.CourseCatalogueBean;
import com.example.app3libvariants.bean.CourseDetailBean;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * 描述：
 * - 组合课程目录
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseGroupCatalogueFragment extends BaseFragment {

    RecyclerView rcvCourseCatalogue;

    private CourseGroupCatalogueAdapter courseGroupCatalogueAdapter;
    private CourseCataloguAdapter courseCataloguAdapter;
    private List courseCatalogueBeans;
    private CourseDetailBean courseDetailBean;
    private int courseType = -1;

    public CourseGroupCatalogueFragment() {

    }

    public static CourseGroupCatalogueFragment newInstance(int courseType) {
        CourseGroupCatalogueFragment courseGroupCatalogueFragment = new CourseGroupCatalogueFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("courseType", courseType);
        courseGroupCatalogueFragment.setArguments(bundle);
        return courseGroupCatalogueFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            courseType = getArguments().getInt("courseType");
        }
        rcvCourseCatalogue = rootView.findViewById(R.id.rcv_course_catalogue);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_catalogue;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {
        courseCatalogueBeans = new ArrayList<>();

        if (courseType == CourseCons.Type.VIDEO) {
            courseType = CourseCons.Type.VIDEO;
        } else if (courseType == CourseCons.Type.LIVING) {
            courseType = CourseCons.Type.LIVING;
        }


    }

    private void initView() {
        rcvCourseCatalogue.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL, ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        rcvCourseCatalogue.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if ("PACKAGE".equals(courseDetailBean.getSellType())) {
            this.courseCatalogueBeans.clear();
            this.courseCatalogueBeans.addAll(courseDetailBean.getCourseList());
            courseGroupCatalogueAdapter = new CourseGroupCatalogueAdapter(mContext,
                    courseType, courseCatalogueBeans);
            int[] section = getLiveingSection();
            courseGroupCatalogueAdapter.setSectionIsOpen(section[0], true);
            rcvCourseCatalogue.setAdapter((RecyclerView.Adapter) courseGroupCatalogueAdapter);
            //滑动到正在直播的章节
            rcvCourseCatalogue.smoothScrollToPosition(section[1]);
        } else {
            this.courseCatalogueBeans.clear();
            this.courseCatalogueBeans.addAll(courseDetailBean.getCourseKpointList());
            courseCataloguAdapter = new CourseCataloguAdapter(mContext, courseType, courseCatalogueBeans);
            rcvCourseCatalogue.setAdapter((RecyclerView.Adapter) courseCataloguAdapter);
        }
    }

    /**
     * 获取当前正在直播的课程, 没有则返回[0,0]
     *
     * @return 返回长度为2的数组, 第一位是正在直播的课程所在位置, 第二位是直播课程的章节所在位置
     */
    private int[] getLiveingSection() {
        int[] sections = new int[2];
        if (courseType != CourseCons.Type.LIVING) {
            return sections;
        }
        int section = 0;
        int position;
        CourseCatalogueBean courseCatalogueBean;
        String status;
        for (; section < courseCatalogueBeans.size(); section++) {
            courseCatalogueBean = (CourseCatalogueBean) courseCatalogueBeans.get(section);
            position = 0;
            for (; position < courseCatalogueBean.getCourseKpointList().size(); position++) {
                status = courseCatalogueBean.getCourseKpointList().get(position).getKpointStatus();
                if (CourseCons.LiveStatus.isLiving(status) || CourseCons.LiveStatus.isRest(status)) {
                    sections[0] = section;
                    sections[1] = section + position + 1;
                    return sections;
                }
            }
        }
        return sections;
    }

    public void setData(CourseDetailBean courseDetailBean) {
        this.courseDetailBean = courseDetailBean;
    }
}
