package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.UpdateAccountPresenter;
import com.example.app3grzx.view.UpdateAccountView;
import com.example.app3libpublic.event.BindEvent;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;


/**
 * 绑定账号
 */

public class BindingAccountFragment extends BaseMVPFragment<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {
    public static final String KEY_EMAIL = "bindAccount";

    EditText etEmail;
    Button btEmailAffirm;
    TitleBar titleBar;
    private String account;

    public static BindingAccountFragment newInstance(String email) {
        Bundle args = new Bundle();
        args.putString(KEY_EMAIL, email);
        BindingAccountFragment fragment = new BindingAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_bindeaccount;
    }

    @Override
    protected void onInit(Bundle bundle) {
        account = getArguments().getString(KEY_EMAIL);
        if (!TextUtils.isEmpty(account)) {
//            etEmail.setText(email);
//            etEmail.setSelection(email.length());
        }
        etEmail = rootView.findViewById(R.id.et_email);
        btEmailAffirm = rootView.findViewById(R.id.bt_email_affirm);
        titleBar = rootView.findViewById(R.id.title);
        rootView.findViewById(R.id.bt_email_affirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accountStr = etEmail.getText().toString().trim();
                if (TextUtils.isEmpty(accountStr)) {
                    ToastUtils.showShort("请输入绑定账号,账号不能为空");
                } else {
                    mPresenter.updateAccount(accountStr);
                }
            }
        });
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        //主要是去刷新学校课程（筛选条件学段固定）
        /*SPManager.saveSchoolSection(userBean.getSubjectId(), "");
        SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());*/
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 学校课程
        onBackPressed();

    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
