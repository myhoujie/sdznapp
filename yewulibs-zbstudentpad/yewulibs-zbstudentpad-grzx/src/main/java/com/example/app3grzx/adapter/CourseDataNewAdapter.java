package com.example.app3grzx.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;

import com.example.app3grzx.R;
import com.example.app3libpublic.service.DownLoadDataService;
import com.example.app3libpublic.utils.CacheUtils;
import com.example.app3libpublic.utils.DialogUtil;
import com.example.app3libpublic.widget.ProgressLinearLayout;
import com.example.app3libvariants.annotation.DownloadState;
import com.example.app3libvariants.bean.CourseFileBean;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.NetworkUtils;
import com.sdzn.core.utils.ToastUtils;

import java.io.File;
import java.util.List;

/**
 * 单科资料
 */

public class CourseDataNewAdapter extends BaseRcvAdapter<CourseFileBean> {
    private Activity mActivity;
    private Handler mHandler;
    private DownLoadDataService service;

    private static final int WAIT_SERVICE = 1;

    public CourseDataNewAdapter(Activity context, List<CourseFileBean> mList) {
        super(context, R.layout.item_course_data_data, mList);
        this.mActivity = context;
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what != WAIT_SERVICE) return;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        service = DownLoadDataService.getService();
        if (service == null) {
            mHandler.sendEmptyMessageDelayed(WAIT_SERVICE, 100);
            return 0;
        }
        return super.getItemCount();
    }

    @Override
    public void convert(BaseViewHolder holder, int position, final CourseFileBean bean) {
        holder.setText(R.id.tv_name, bean.getSrcFileName());
        holder.setText(R.id.tv_type, "类型: " + bean.getFileTypeName());
//        holder.setText(R.id.tv_updater, "上传人: ");
        holder.setVisible(R.id.tv_updater, false);
        holder.setText(R.id.tv_date, "日期: " + bean.getCreateTime());
        ProgressLinearLayout pb = holder.getView(R.id.pb_ll);
        @DownloadState
        int state = service.getDownloadState(bean);
        switch (state) {
            case DownloadState.UnDownLoad:
                pb.setProgress(0);
                holder.setText(R.id.tv_download, "下载");
                holder.setVisible(R.id.tv_download, true);
                holder.setVisible(R.id.tv_chakan, false);
                holder.setVisible(R.id.tv_delete, false);
                break;
            case DownloadState.Downloading:
                pb.setProgress(bean.getProgress());
                holder.setText(R.id.tv_download, "下载中");
                holder.setVisible(R.id.tv_download, true);
                holder.setVisible(R.id.tv_chakan, false);
                holder.setVisible(R.id.tv_delete, false);
                break;
            case DownloadState.Pause:
            case DownloadState.Cancel:
                break;
            case DownloadState.Finish:
                pb.setProgress(0);
                holder.setText(R.id.tv_download, "下载");
                holder.setVisible(R.id.tv_download, false);
                holder.setVisible(R.id.tv_chakan, true);
                holder.setVisible(R.id.tv_delete, true);
                break;
            case DownloadState.Error://出错直接删除文件
                pb.setProgress(0);
                service.delete(bean);
                holder.setText(R.id.tv_download, "下载");
                holder.setVisible(R.id.tv_download, true);
                holder.setVisible(R.id.tv_chakan, false);
                holder.setVisible(R.id.tv_delete, false);
                break;
            default:
                break;
        }
        holder.getView(R.id.tv_download).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (NetworkUtils.getNetType(mActivity)) {
                    case NETWORK_NO:
                        ToastUtils.showShort("未连接网络");
                        return;
                    case NETWORK_2G:
                    case NETWORK_3G:
                    case NETWORK_4G:
                        DialogUtil.showDialog(mActivity, "当前非WiFi环境, 确认继续下载? ", true,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        service.download(bean);
                                        notifyDataSetChanged();
                                    }
                                });
                        break;
                    default:
                        service.download(bean);
                        notifyDataSetChanged();
                        break;
                }
            }
        });
        holder.getView(R.id.tv_chakan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int fileState = service.getDownloadState(bean);
                if (fileState != DownloadState.Finish) {
                    ToastUtils.showShort("文件不存在");
                    notifyDataSetChanged();
                    return;
                }
                FileUtils.openFile(new File(CacheUtils.getDownloadCache(), bean.getSrcFileName()), mActivity);
            }
        });
        holder.getView(R.id.tv_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtil.showDialog(mActivity, "确定删除该资料?", true, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        service.delete(bean);
                        notifyDataSetChanged();
                    }
                });

            }
        });
    }
}
