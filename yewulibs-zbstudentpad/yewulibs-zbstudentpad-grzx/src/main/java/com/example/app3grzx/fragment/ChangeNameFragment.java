package com.example.app3grzx.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.example.app3grzx.R;
import com.example.app3grzx.presenter.UpdateAccountPresenter;
import com.example.app3grzx.view.UpdateAccountView;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.UserBean;
import com.example.app3libvariants.network.SPManager;
import com.example.app3libvariants.network.UpdateAccountEvent;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * 修改姓名
 *
 * @author Reisen at 2017-12-07
 */

public class ChangeNameFragment extends BaseMVPFragment<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {
    public static final String KEY_STUDENTNAME = "studentName";
    public static final int NAME_MAX_LENGTH = 20;

    EditText etName;
    TitleBar titleBar;
    private String studentName;

    public static ChangeNameFragment newInstance(String studentName) {
        Bundle args = new Bundle();
        args.putString(KEY_STUDENTNAME, studentName);
        ChangeNameFragment fragment = new ChangeNameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_change_name;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        studentName = getArguments().getString(KEY_STUDENTNAME);
        if (!TextUtils.isEmpty(studentName)) {
            etName.setText(studentName);
            etName.setSelection(studentName.length());
        }
        etName = rootView.findViewById(R.id.et_name);
        TitleBar titleBar = rootView.findViewById(R.id.title);
        rootView.findViewById(R.id.btn_certain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etName.getText().toString().trim();
                if (TextUtils.isEmpty(username)) {
                    ToastUtils.showShort("姓名不能为空");
                } else {
                    mPresenter.updateUserInfo("", username,"");

                }
            }
        });

        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_NAME));
        onBackPressed();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
