package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.CollectBean;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public interface CollectView extends BaseView {
    void upDataSuccess(CollectBean collectBean);

    void onError(String msg);

    void delSuccess();

    void delError(String msg);
}
