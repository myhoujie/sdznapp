package com.example.app3grzx.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app3grzx.R;
import com.example.app3grzx.adapter.CollectAdapter;
import com.example.app3grzx.presenter.CollectPresenter;
import com.example.app3grzx.view.CollectView;
import com.example.app3libpublic.event.CollectEvent;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libpublic.widget.EmptyLayout;
import com.example.app3libpublic.widget.SwipeItemLayout;
import com.example.app3libpublic.widget.TitleBar;
import com.example.app3libvariants.bean.CollectBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的收藏
 *
 * @author Reisen at 2017-12-06
 */

public class MineCollectFragment extends BaseMVPFragment<CollectView, CollectPresenter>
        implements CollectView, OnRefreshLoadmoreListener {
    TitleBar titleBar;
    RecyclerView recyclerCollect;
    SmartRefreshLayout swipeToLoadLayout;
    EmptyLayout emptyLayout;

    private List<CollectBean.FavoriteListBean> mData = new ArrayList<>();
    private CollectAdapter collectAdapter;
    private int pageIndex = 1;
    private int pageSize = 10;
    private int currentPosition = 0;
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";
    private boolean isRefresh = false;

    public static final String COURSE_TYPE = "courseType";
    public static final String COURSE_ID = "courseId";
    public static final String SHOW_LIVE_BTN = "showLiveBtn";
    public static final String PACKAGE = "PACKAGE";

    public static MineCollectFragment newInstance() {
        return new MineCollectFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_collect;
    }

    @Override
    protected void onInit(Bundle bundle) {
        EventBus.getDefault().register(this);
        titleBar = rootView.findViewById(R.id.title_bar);
        recyclerCollect = rootView.findViewById(R.id.swipe_target);
        swipeToLoadLayout = rootView.findViewById(R.id.swipeToLoadLayout);
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        initView();
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            pageIndex = 1;
            initData();
        }
    }

    private void initData() {
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    private void initView() {
        titleBar.setLeftClickListener(null);
        collectAdapter = new CollectAdapter(mContext, mData);
        recyclerCollect.addOnItemTouchListener(new SwipeItemLayout.OnSwipeItemTouchListener(mContext));
        recyclerCollect.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerCollect.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_f6, null), 10));
        recyclerCollect.setAdapter((RecyclerView.Adapter) collectAdapter);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });

        swipeToLoadLayout.setOnRefreshLoadmoreListener(this);

        collectAdapter.setOnCollectListener(new CollectAdapter.OnCollectListener() {
            @Override
            public void cancelListener(int position) {
                currentPosition = position;
                mPresenter.delCollection(String.valueOf(mData.get(position).getCourseId()));
            }

            @Override
            public void mainListener(int position) {
//                currentPosition = position;
//                //判断当前的item是点播还是直播的
//                int courseType = -1;
//                if (TYPE_PACKAGE.equalsIgnoreCase(mData.get(position).getSellType())) {
//                    courseType = mData.get(position).getPackageType();
//                } else {
//                    if (TYPE_LIVE.equalsIgnoreCase(mData.get(position).getSellType())) {//直播
//                        courseType = CourseCons.Type.LIVING;
//                    } else if (TYPE_COURSE.equalsIgnoreCase(mData.get(position).getSellType())) {//点播
//                        courseType = CourseCons.Type.VIDEO;
//                    }
//                }
//                IntentController.toCourseDetailForResult(mContext, courseType, mData.get(position).getCourseId(), false);
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");//CourseDetailActivity.class
                intent.putExtra(PACKAGE, mData.get(position).getSellType());
                intent.putExtra(COURSE_ID, mData.get(position).getCourseId());
                intent.putExtra(SHOW_LIVE_BTN, true);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        mPresenter.upDataCollect(pageIndex, pageSize);
    }

    @Override
    protected CollectPresenter createPresenter() {
        return new CollectPresenter();
    }

    @Override
    public void upDataSuccess(CollectBean collectBean) {

        if (collectBean.getFavoriteList() != null && !collectBean.getFavoriteList().isEmpty()) {
            if (pageIndex == 1) {
                mData.clear();
            }
            mData.addAll(collectBean.getFavoriteList());
            collectAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            if (pageIndex == 1) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多,到底了");
            }
        }
        goneSwipView();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        }
        goneSwipView();

    }

    @Override
    public void delSuccess() {
        mData.remove(currentPosition);
        collectAdapter.notifyDataSetChanged();
    }

    @Override
    public void delError(String msg) {
        ToastUtils.showShort(msg);

    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (swipeToLoadLayout.isRefreshing()) {
            swipeToLoadLayout.finishRefresh();
        }
        if (swipeToLoadLayout.isLoading()) {
            swipeToLoadLayout.finishLoadmore();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void upCollect(CollectEvent event) {
        isRefresh = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
