package com.example.app3grzx.view;

import com.sdzn.core.base.BaseView;
import com.example.app3libvariants.bean.LiveRoomBean;
import com.example.app3libvariants.bean.NewLiveInfo;
import com.example.app3libvariants.bean.NewVideoInfo;
import com.example.app3libvariants.bean.VideoRoomBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public interface PurchasedCourseDetailView extends BaseView {

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void liveRoomInfoOnError(String msg);

    void addShoppingCartSuccess(boolean isExsits, int cartNum);

    void addShoppingCartFailure(String msg);

    void addFavoriteSuccess();

    void addFavoriteFailure(String msg);

    void delFavoriteSuccess();

    void delFavoriteFailure(String msg);

    void getCartNumSuccess(int cartNum);

    void toSettlement(boolean isExsits,String carIdStr, int cartNum);

    void getVideoRoomInfoSuccrss(NewVideoInfo videoRoomBean);

    void videoRoomInfoOnError(String msg);

    void getReplayInfoSuccess(NewVideoInfo info);

    void applySuccess();
}
