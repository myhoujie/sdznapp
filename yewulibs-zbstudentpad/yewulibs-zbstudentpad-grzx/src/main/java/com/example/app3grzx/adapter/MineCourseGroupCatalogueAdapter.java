package com.example.app3grzx.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app3grzx.R;
import com.example.app3libpublic.adapter.SectionedRecyclerViewAdapter;
import com.example.app3libpublic.manager.constant.CourseCons;
import com.example.app3libvariants.bean.CourseCatalogueBean;
import com.example.app3libvariants.bean.CourseKpointListBean;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;

import java.util.List;

/**
 * 描述：我的课程列表适配器
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class MineCourseGroupCatalogueAdapter extends SectionedRecyclerViewAdapter {

    private SparseBooleanArray mBooleanMap;//记录下哪个section是被打开的
    private int courseType;
    private LiveCoursePlayerListener listener;
    private int courseIsvisiable = 1;//传过来课程的失效或有效的标志
    private boolean isFree;
    private boolean isRelationLiveCourse;

    public MineCourseGroupCatalogueAdapter(Context context, int courseType, List mList, int courseIsvisiable, boolean isFree, boolean isRelationLiveCourse) {
        super(context, 0, mList);
        this.courseType = courseType;
        this.isFree = isFree;
//        this.courseIsvisiable = courseIsvisiable;
        this.isRelationLiveCourse = isRelationLiveCourse;
        mBooleanMap = new SparseBooleanArray();
    }

    public void setListener(LiveCoursePlayerListener listener) {
        this.listener = listener;
    }

    @Override
    protected int getSectionItemViewType(int section, int position) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        return courseCatalogueBean.getCourseKpointList().get(position).getKpointType();
    }

    @Override
    protected int getSectionCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    protected int getItemCountForSection(int section) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        int count = courseCatalogueBean.getCourseKpointList().size();
        if (!mBooleanMap.get(section)) {
            count = 0;
        }

        return courseCatalogueBean.getCourseKpointList().isEmpty() ? 0 : count;
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected BaseViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        View headView = mLayoutInflater.inflate(R.layout.item_course_catalogue_group, parent, false);
        return new BaseViewHolder(context, headView);
    }

    @Override
    protected BaseViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected BaseViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        switch (viewType) {
            case 0:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_chapter);
                break;
            case 1:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_mine_course_catalogue_child);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(final BaseViewHolder holder, final int section) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        holder.setText(R.id.tv_catalogue, courseCatalogueBean.getCourseName());
        boolean isOpen = mBooleanMap.get(section);
        holder.setImageView(R.id.iv_open, isOpen ? R.mipmap.ic_arrow_up : R.mipmap.ic_arrow_down);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpen = mBooleanMap.get(section);
                mBooleanMap.put(section, !isOpen);
                notifyDataSetChanged();
            }
        });
    }

    public void setSectionIsOpen(int section, boolean isOpen) {
        if (section < 0 || section > mList.size()) {
            return;
        }
        mBooleanMap.put(section, isOpen);
    }

    @Override
    protected void onBindSectionFooterViewHolder(BaseViewHolder holder, int section) {

    }

    @Override
    protected void onBindItemViewHolder(BaseViewHolder holder, int section, int position) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        CourseKpointListBean kpointBean = courseCatalogueBean.getCourseKpointList().get(position);
        switch (holder.getItemViewType()) {
            case 0:
                holder.setText(R.id.tv_chacpter, kpointBean.getName());
                break;
            case 1:
                setItemData(holder, kpointBean);
                break;
            default:
                break;
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, Object o) {

    }

    private void setImgviewtoText(TextView textView, int imgId, String colorStr) {
        textView.setTextColor(Color.parseColor(colorStr));
        textView.setBackgroundResource(imgId);
    }

    public void setItemData(BaseViewHolder holder, final CourseKpointListBean kpointBean) {
        holder.setText(R.id.tv_catalogue, kpointBean.getName());
        if (courseType == CourseCons.Type.LIVING) {//直播
            holder.setVisible(R.id.tv_date, true);
            String startDay = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "yyyy-MM-dd");
            String strartTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "HH:mm");
            String endDay = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "yyyy-MM-dd");
            String endTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "HH:mm");
            if (startDay.equals(endDay)) {
                holder.setText(R.id.tv_date, startDay + " " + strartTime + "~" + endTime);
            } else {
                holder.setText(R.id.tv_date, startDay + " " + strartTime + "~" + endDay + " " + endTime);
            }
            holder.setText(R.id.tv_teacher, "讲师:"+kpointBean.getTeacherName());
            holder.setText(R.id.tv_course_isok, CourseCons.LiveStatus.upStatus(kpointBean.getLiveStates()));
            switch (kpointBean.getLiveStates()) {//直播
                case CourseCons.LiveStatus.LIVE_NOTBEGIN_NEW://未开始
                    setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                    break;
                case CourseCons.LiveStatus.LIVE_LIVING_NEW://观看直播
                    setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");
                    break;
                case CourseCons.LiveStatus.LIVE_FINISHED_NEW://已结束
                    setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                    break;
                case CourseCons.LiveStatus.LIVE_REPLAY_NEW://查看回放
                    setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_replay, "#1A70AE");
                    break;
                default:
                    holder.setText(R.id.tv_course_isok, "敬请期待");
                    setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_course_shape, "#B1B9BF");
                    break;
            }
        } else {//点播
            holder.setText(R.id.tv_date, kpointBean.getVideoLenth()+"分钟");
            if (isFree) {
                holder.setText(R.id.tv_course_isok, "开始学习");
                setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");
            } else {
                holder.setText(R.id.tv_course_isok, "开始学习");
                setImgviewtoText((TextView) holder.getView(R.id.tv_course_isok), R.drawable.bg_circle_course_enter, "#ED6D00");
            }
        }
        holder.setText(R.id.tv_teacher, "讲师:"+kpointBean.getTeacherName());
        //点击跳转
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kpointBean.getKpointType() == 1) {
                    //判断是否是免费的课程
                    //判断是直播还是点播
                    if (courseType == CourseCons.Type.LIVING) {
                        //此处需要判断是点播还是直播的界面
                        if (CourseCons.LiveStatus.LIVE_LIVING_NEW.equalsIgnoreCase(kpointBean.getLiveStates())) {
                            listener.ontoLiving(kpointBean);
                        } else if (CourseCons.LiveStatus.LIVE_REPLAY_NEW.equalsIgnoreCase(kpointBean.getLiveStates())) {
                            listener.ontoReplay(kpointBean);
                        }
                    } else {
                        if (courseIsvisiable == 1) {
                            if (isRelationLiveCourse) {
                                listener.ontoReplay(kpointBean);
                            } else {
                                listener.ontoVideo(kpointBean);
                            }
                        }
                    }
                }
            }
        });
        if (kpointBean.getFree() == 1) {
            holder.setVisible(R.id.tv_audition, true);
        } else {
            holder.setVisible(R.id.tv_audition, false);
        }
    }

    public interface LiveCoursePlayerListener {
        void ontoLiving(CourseKpointListBean kpointBean);//直播

        void ontoVideo(CourseKpointListBean kpointBean);//点播

        void ontoReplay(CourseKpointListBean kpointBean);//回放
    }
}
