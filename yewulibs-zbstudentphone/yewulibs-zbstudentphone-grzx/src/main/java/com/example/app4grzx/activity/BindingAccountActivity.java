package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.event.BindEvent;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4public.manager.SPManager;
import com.example.app4grzx.presenter.UpdateAccountPresenter;
import com.example.app4grzx.view.UpdateAccountView;

import org.greenrobot.eventbus.EventBus;




/**
 * 描述：
 * - 绑定账号
 *  zs
 */
public class BindingAccountActivity extends BaseMVPActivity1<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView, View.OnClickListener {

    EditText etName;
    Button btnCertain;
    private String studentName;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_binding_account;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入BindingAccountActivity成功" );
                }
            }
        }
        etName = (EditText) findViewById(R.id.et_name);
        studentName = getIntent().getStringExtra("bindAccount");
        btnCertain = (Button) findViewById(R.id.btn_certain);
        btnCertain.setOnClickListener(this);
        if (!TextUtils.isEmpty(studentName)) {
//            etName.setText(studentName);
//            etName.setSelection(studentName.length());
        }

    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        //主要是去刷新学校课程（筛选条件学段固定）
//        SPManager.saveSchoolSection(userBean.getSubjectId(),"");
//        SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 学校课程
        BindingAccountActivity.this.finish();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onClick(View v) {
        String bindAccount = etName.getText().toString().trim();
        if (TextUtils.isEmpty(bindAccount)) {
            ToastUtils.showShort("请输入绑定账号,账号不能为空");
        } else {
            mPresenter.updateAccount(bindAccount);
        }
    }
}
