package com.example.app4grzx.adapter;

import android.content.Context;

import com.example.app4grzx.R;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.variants.bean.CouponBean;

import java.util.List;

/**
 * 描述：优惠券的Adapter
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class CouponAdapter extends BaseRcvAdapter<CouponBean> {

    public CouponAdapter(Context context, List<CouponBean> mList) {
        super(context, R.layout.item_coupon, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, CouponBean couponBean) {
        holder.setText(R.id.tv_coupon_number, "编号：" + couponBean.getCouponCode());
        holder.setText(R.id.tv_coupon_price, String.valueOf(couponBean.getAmount()));
        if (couponBean.getLimitAmount() != 0) {
            holder.setText(R.id.tv_coupon_limit_amount, "满" + couponBean.getLimitAmount() + "元可用");
        }
        String state;
        if (2 == couponBean.getStatus()) {
            state = "已使用";
        } else if (3 == couponBean.getStatus()) {
            state = "已过期";
        } else if (4 == couponBean.getStatus()) {
            state = "已作废";
        } else if (5 == couponBean.getStatus()) {
            state = "即将过期";
        } else if (6 == couponBean.getStatus()) {
            state = "已冻结";
        } else {
            state = "未使用";
        }
        if (couponBean.getStatus() != 1 && couponBean.getStatus() != 5) {
            holder.setText(R.id.tv_coupon_timeofvalidity, state);
            holder.setTextColorRes(R.id.tv_coupon_number, R.color.textMinor);
            holder.setTextColorRes(R.id.tv_coupon_tv1, R.color.textMinor);
            holder.setTextColorRes(R.id.tv_coupon_price, R.color.textMinor);
            holder.setTextColorRes(R.id.tv_coupon_limit_amount, R.color.textMinor);
            holder.setTextColorRes(R.id.tv_coupon_timeofvalidity, R.color.textMinor);
            holder.setTextColorRes(R.id.tv_coupon_limit_amount, R.color.textMinor);
            holder.setImageView(R.id.iv_combined, R.mipmap.combined_trans);
            holder.setImageView(R.id.iv_coupon, R.mipmap.ic_coupon_trans);
            holder.setImageView(R.id.iv_coupon_text, R.mipmap.ic_coupon_text_trans);
            return;
        }
        holder.setTextColorRes(R.id.tv_coupon_number, R.color.colorPrimary);
        holder.setTextColorRes(R.id.tv_coupon_tv1, R.color.red);
        holder.setTextColorRes(R.id.tv_coupon_price, R.color.red);
        holder.setTextColorRes(R.id.tv_coupon_limit_amount, R.color.red);

        String startTime = TimeUtils.millis2String(couponBean.getStartTime(), "yyyy.MM.dd");
        String endTime = TimeUtils.millis2String(couponBean.getEndTime(), "yyyy.MM.dd");
        if (couponBean.getStatus() == 1) {
            holder.setText(R.id.tv_coupon_timeofvalidity, "有效期：" + startTime + "~" + endTime);
            holder.setTextColorRes(R.id.tv_coupon_timeofvalidity, R.color.textMinor);//即将过期->红 正常->灰
        } else {
            holder.setText(R.id.tv_coupon_timeofvalidity, "即将过期  " + startTime + "~" + endTime);
            holder.setTextColorRes(R.id.tv_coupon_timeofvalidity, R.color.red);//即将过期->红 正常->灰
        }

        holder.setImageView(R.id.iv_combined, R.mipmap.combined);
        holder.setImageView(R.id.iv_coupon, R.mipmap.ic_coupon);
        holder.setImageView(R.id.iv_coupon_text, R.mipmap.ic_coupon_text);
    }
}
