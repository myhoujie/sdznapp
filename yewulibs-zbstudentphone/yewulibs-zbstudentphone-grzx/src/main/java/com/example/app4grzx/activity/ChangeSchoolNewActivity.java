package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4public.manager.SPManager;
import com.example.app4grzx.presenter.UpdateAccountPresenter;
import com.example.app4grzx.view.UpdateAccountView;

import org.greenrobot.eventbus.EventBus;




/**
 * zs
 */
public class ChangeSchoolNewActivity extends BaseMVPActivity1<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView, View.OnClickListener {

    EditText etName;
    Button btnCertain;
    private String studentName;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_change_school;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入ChangeSchoolNewActivity成功" );
                }
            }
        }
        etName = (EditText) findViewById(R.id.et_name);
        btnCertain = (Button) findViewById(R.id.btn_certain);
        btnCertain.setOnClickListener(this);
        studentName = getIntent().getStringExtra("schoolName");
        if (!TextUtils.isEmpty(studentName)) {
            etName.setText(studentName);
            etName.setSelection(studentName.length());
        }

    }


    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_SCHOOL_NAME));
        ChangeSchoolNewActivity.this.finish();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void onClick(View v) {
        String username = etName.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            ToastUtils.showShort("学校不能为空");
        } else {
            mPresenter.updateUserInfo("","",username);
        }
    }
}
