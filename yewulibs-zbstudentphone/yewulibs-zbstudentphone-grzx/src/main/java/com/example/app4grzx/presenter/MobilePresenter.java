package com.example.app4grzx.presenter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.example.app4grzx.R;
import com.example.app4grzx.view.MobileView;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4public.manager.SPManager;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;
import com.example.app4public.utils.ChangePhoneCountDownTimerUtils;
import com.example.app4public.utils.VerifyUtil;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class MobilePresenter extends BasePresenter<MobileView> {

    private View code_view;

    /**
     * 获取验证码
     *
     * @param mobile
     * @param rootView
     */
    public void gainCode(String mobile, View rootView) {
        code_view = rootView;
        //先判断下手机号是否为空
        if (TextUtils.isEmpty(mobile.toString().trim())) {
            ToastUtils.showShort("请输入手机号");
            return;
        }
        if (!VerifyUtil.isMobileNO(mobile.toString().trim())) {
            ToastUtils.showShort("手机号格式错误");
            return;
        }


        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(mobile)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        //当操作成功时才进行倒计时操作
                        ChangePhoneCountDownTimerUtils mCountDownTimerUtils = new ChangePhoneCountDownTimerUtils((TextView) code_view, 60000, 1000);
                        mCountDownTimerUtils.start();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        ToastUtils.showShort(msg);
                    }
                }, mActivity, true,"请求中..."));
        addSubscribe(subscribe);


        //判断手机号是否为当前手机号
//        if (UserController.getUser(appContext).getTelephone().equals(mobile.toString().trim())){
//            ToastUtils.showShort(mActivity,"您输入的是当前的手机号，请正确输入手机号。");
//            return;
//        }
    }


    /**
     * @param mobile
     * @param code
     */
    public void confirm(String mobile, String code) {
        //先判断下手机号是否为空
        if (TextUtils.isEmpty(mobile.trim())) {
            ToastUtils.showShort("请输入手机号");
            return;
        }
        if (!VerifyUtil.isMobileNO(mobile.trim())) {
            ToastUtils.showShort("手机号格式错误");
            return;
        }
        //判断验证码是否为空
        if (TextUtils.isEmpty(code.trim())) {
            ToastUtils.showShort("请输入验证码");
            return;
        }
        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .confirmPhoneVerifyCode(mobile, code, SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().onSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);

    }
}
