package com.example.app4grzx.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4grzx.R;
import com.example.app4grzx.adapter.MineTaskAdapter;
import com.example.app4grzx.presenter.MineTaskPresenter;
import com.example.app4grzx.view.MineTaskView;
import com.sdzn.variants.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.variants.bean.MineTaskBean;
import com.example.app4public.event.WebViewAvatarEvent;
import com.example.app4public.manager.SPManager;
import com.example.app4public.widget.EmptyLayout;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * * 描述：我的作业两个模块
 * 创建人：baoshengxiang
 * 创建时间：2017/9/26
 */

/**
 * 描述：我的作业两个模块
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class TaskPageFragment extends BaseMVPFragment<MineTaskView, MineTaskPresenter> implements MineTaskView, BaseRcvAdapter.OnItemClickListener, OnRefreshLoadmoreListener {
    public static final String ARGS_TYPE = "args_page";
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    private EmptyLayout emptyLayout;
    private RecyclerView recyclerView;
    private SmartRefreshLayout refreshLayout;
    private MineTaskAdapter mineTaskAdapter;
    private List<MineTaskBean.RecordsBean> mDataMyCourse = new ArrayList<>();
    private String state;
    private int pageIndex = 1;//当前页
    private int pageSize = 10;//
    private String studentid;
    private String AppToken;
//    private List<MineTaskBean.RecordsBean> Records;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            state = getArguments().getString(ARGS_TYPE);
        }
    }


    public static TaskPageFragment newInstance(String page) {
        Bundle args = new Bundle();
        args.putString(ARGS_TYPE, page);
        TaskPageFragment fragment = new TaskPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine_task;

    }

    @Override
    protected MineTaskPresenter createPresenter() {
        return new MineTaskPresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        emptyLayout = rootView.findViewById(R.id.empty_layout);
        recyclerView = rootView.findViewById(R.id.swipe_target);
        refreshLayout = rootView.findViewById(R.id.refresh_layout);
        studentid = SPManager.getUser().getUserId();
        AppToken = SPToken.getToken();
        initView();
    }

    @Override
    protected void lazyLoad() {
        pageIndex = 1;
        initData();
        isFirst = false;
    }

//    private Map<String, String> getParms() {
//        Map<String, String> params = new HashMap<>();
//        params.put("index", String.valueOf(pageIndex));
//        params.put("size", String.valueOf(pageSize));
//        return params;
//    }

    private void initData() {
        if (SPToken.autoLogin(mContext)) {
            if (TYPE_TODY.equals(state)) {
                mPresenter.getCourse(studentid, "0", pageSize, pageIndex);//getParms()
            } else if (TYPE_RECENTLY.equals(state)) {
                mPresenter.getCourse(studentid, "3", pageSize, pageIndex);
            }
        }

    }

    private void initView() {
        if (TYPE_TODY.equals(state)) {//未作答
            mineTaskAdapter = new MineTaskAdapter(mContext, mDataMyCourse, TYPE_TODY);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.setAdapter((RecyclerView.Adapter) mineTaskAdapter);
            mineTaskAdapter.setOnItemClickListener(this);
        } else if (TYPE_RECENTLY.equals(state)) {//已作答
            mineTaskAdapter = new MineTaskAdapter(mContext, mDataMyCourse, TYPE_RECENTLY);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.setAdapter((RecyclerView.Adapter) mineTaskAdapter);
            mineTaskAdapter.setOnItemClickListener(this);

        }

        refreshLayout.setOnRefreshLoadmoreListener(this);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lazyLoad();
            }
        });
    }


    @Override
    public void onItemClick(View view, int position) {
        //判断当前的item是点播还是直播的
        if (TYPE_TODY.equals(state)) {//未作答
            mPresenter.getValidate(mDataMyCourse.get(position).getHomeworkId(), studentid, "0", "phone", TYPE_TODY);
        } else if (TYPE_RECENTLY.equals(state)) {//已作答
            mPresenter.getValidate(mDataMyCourse.get(position).getHomeworkId(), studentid, "3", "phone", TYPE_RECENTLY);
        }


    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        lazyLoad();
    }

    @Override
    public void listCourseSuccess(List<MineTaskBean.RecordsBean> mineTaskBean) {//1过期
//        Records = mineTaskBean;
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataMyCourse.clear();
            if (mineTaskBean.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(mineTaskBean.size() < pageSize);
        this.mDataMyCourse.addAll(mineTaskBean);
        mineTaskAdapter.notifyDataSetChanged();
    }

    @Override
    public void listCourseEmpty() {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            refreshLayout.setLoadmoreFinished(true);
        }
        clearLoingState();
    }

    @Override
    public void listCourseError(String msg) {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
        clearLoingState();
    }

    @Override
    public void validatetime(Object url, String AnsweringState) {
        String validateurl = String.valueOf(url);

        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebTaskActivity");

        if (AnsweringState.equals(TYPE_TODY)) {
            startIntent.putExtra("AppToken", AppToken);
            startIntent.putExtra("validateurl", validateurl);
            startIntent.putExtra("AnsweringState", AnsweringState);
        } else {
            startIntent.putExtra("AppToken", AppToken);
            startIntent.putExtra("validateurl", validateurl);
            startIntent.putExtra("AnsweringState", AnsweringState);
        }
        startActivity(startIntent);
    }

    //更新作业
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setWebViewAvatarEvent(WebViewAvatarEvent webViewAvatarEvent) {
        if (webViewAvatarEvent.getType().equals(TYPE_TODY)) {
            Log.e("ttttaaaaaa", "enbus回调未作答");
            lazyLoad();
        } else {
            Log.e("ttttaaaaaa", "enbus回调已作答");
            lazyLoad();
        }
    }

    //隐藏刷新布局或者底部加载更多的布局
    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}