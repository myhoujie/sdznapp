package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.app4grzx.R;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;



/**
 * 描述：关于我们
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class AboutActivity extends BaseActivity1 implements View.OnClickListener {
    TextView tvQQ;
    TextView tvPhone;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入AboutActivity成功" );
                }
            }
        }
        tvQQ = (TextView) findViewById(R.id.tv_qq);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvQQ.setOnClickListener(this);
        tvPhone.setOnClickListener(this);
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.statusBarLightMode(this);
    }


    private void startQQ() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin=2495065899")));
        } catch (Exception e) {
            ToastUtils.showShort("请检查是否安装QQ");
            e.printStackTrace();
        }
    }

    private void callPhone() {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:053189922971"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            ToastUtils.showShort("无法拨号");
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (R.id.tv_qq==view.getId()){
            startQQ();
        }else if (R.id.tv_phone==view.getId()){
            callPhone();
        }
    }
}
