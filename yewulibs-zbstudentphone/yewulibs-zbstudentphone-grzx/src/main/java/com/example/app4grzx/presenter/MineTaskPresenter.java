package com.example.app4grzx.presenter;

import com.example.app4grzx.view.MineTaskView;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.variants.bean.MineTaskBean;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewSchoolFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;
import rx.Subscription;

/**
 * zs
 */

public class MineTaskPresenter extends BasePresenter<MineTaskView> {
    public void getCourse(final String studentId, String type, int limit, int page) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getMineTask(studentId, type, limit, page)
                .compose(TransformUtils.<ResultBean<MineTaskBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<MineTaskBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<MineTaskBean>() {
                    @Override
                    public void onNext(MineTaskBean mineTaskBean) {
                        if (mineTaskBean.getRecords() == null) {
                            getView().listCourseEmpty();
                        } else {
                            getView().listCourseSuccess(mineTaskBean.getRecords());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    public void getValidate(String homeworkId, String studentId, String type, String terminal, final String AnsweringState) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getValidate(homeworkId, studentId, type, terminal)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object mineTaskBean) {
                        getView().validatetime(mineTaskBean, AnsweringState);
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        ToastUtils.showLong(mActivity, e.getMessage());
//                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
}
