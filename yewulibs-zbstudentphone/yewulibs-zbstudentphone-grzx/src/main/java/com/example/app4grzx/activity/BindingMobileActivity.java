package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.event.BindEvent;

import com.example.app4public.manager.SPManager;
import com.example.app4grzx.presenter.MobilePresenter;
import com.example.app4grzx.view.MobileView;

import org.greenrobot.eventbus.EventBus;

/**
 * zs
 */

public class BindingMobileActivity extends BaseMVPActivity1<MobileView, MobilePresenter> implements MobileView, View.OnClickListener {

    private EditText mMobile, mCode;
    private Button bt_confirm;
    private TextView tv_getCode;
    private UserBean userBean;

    private TextWatcher mWatcher;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_bindmobile;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入BindingMobileActivity成功" );
                }
            }
        }
        initData();
        intView();
    }

    private void initData() {
        userBean = SPManager.getUser();
    }

    private void intView() {
        mMobile = (EditText) findViewById(R.id.tv_mobile);
        mCode = (EditText) findViewById(R.id.tv_code);
        tv_getCode = (TextView) findViewById(R.id.tv_getcode);
        bt_confirm = (Button) findViewById(R.id.bt_confirm);
        tv_getCode.setOnClickListener(this);
        bt_confirm.setOnClickListener(this);
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String mobile = mMobile.getText().toString().trim();
                String code = mCode.getText().toString().trim();
                tv_getCode.setEnabled(!TextUtils.isEmpty(mobile));
                bt_confirm.setEnabled(!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(code));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        mMobile.addTextChangedListener(mWatcher);
        mCode.addTextChangedListener(mWatcher);
    }

    @Override
    public void onClick(View v) {
        if (R.id.bt_confirm == v.getId()) {
            final String mobile = mMobile.getText().toString().trim();
            final String code = mCode.getText().toString().trim();

            mPresenter.confirm(mobile, code);
//                CustomDialog dialog = new CustomDialog.Builder(this)
//                        .setTitle("提示")
//                        .setTitleColor(R.color.textPrimary)
//                        .setMessage("手机号码" + userBean.getMobile() +
//                                "已经被其他账号绑定, 确认解除此绑定, 同时将该手机号与本账号绑定吗？")
//                        .setPositive("确认", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                mPresenter.confirm(mobile, code);
//                            }
//                        }).setNegative("取消", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        }).create();
//                dialog.show();
        } else if (R.id.tv_getcode == v.getId()) {

            mPresenter.gainCode(mMobile.getText().toString().trim(), tv_getCode);
        }

    }

    @Override
    protected MobilePresenter createPresenter() {
        return new MobilePresenter();
    }

    @Override
    public void onSuccess() {
        String mobile = mMobile.getText().toString().trim();
//        boolean isBundling = userBean.isBundlingState();
//        userBean.setBundlingState(isBundling ? 0 : 1);
        userBean.setMobile(mobile);
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        ToastUtils.showShort("绑定成功");
        onBackPressed();
    }

    private void reLogin() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
         Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
    }
}
