package com.example.app4grzx.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.UserBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/16
 */
public interface UpdateAccountView extends BaseView {

    void updateAccountSuccess(UserBean userBean);

    void updateAccountFailure(String msg);
}
