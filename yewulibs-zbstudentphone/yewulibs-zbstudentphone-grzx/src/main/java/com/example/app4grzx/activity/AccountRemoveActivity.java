package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.sdzn.core.base.BaseActivity1;
import com.example.app4grzx.R;

public class AccountRemoveActivity extends BaseActivity1 {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_account_remove;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入AccountRemoveActivity成功" );
                }
            }
        }
    }

}
