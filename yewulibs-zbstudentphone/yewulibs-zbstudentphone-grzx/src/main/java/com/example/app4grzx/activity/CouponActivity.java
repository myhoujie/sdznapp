package com.example.app4grzx.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app4grzx.R;
import com.example.app4grzx.adapter.CouponAdapter;
import com.example.app4grzx.presenter.CouponPresenter;
import com.example.app4grzx.view.CouponView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.variants.bean.CouponBean;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;



/**
 * 描述：优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class CouponActivity extends BaseMVPActivity1<CouponView, CouponPresenter> implements CouponView, com.scwang.smartrefresh.layout.listener.OnRefreshListener {
    TitleBar titleBar;
    RecyclerView recyclerCoupon;
    SmartRefreshLayout swipeToLoadLayout;
    EmptyLayout emptyLayout;

    private CouponAdapter couponAdapter;
    //    private List<String> mData = new ArrayList<>();
    private List<CouponBean> mData = new ArrayList<>();
    private String titleName = "我的优惠券";
    private boolean isNeedBack = false;//是否需要回传优惠券信息

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_coupon;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入CouponActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        swipeToLoadLayout = (SmartRefreshLayout) findViewById(R.id.swipeToLoadLayout);
        recyclerCoupon = (RecyclerView) findViewById(R.id.swipe_target);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);

        titleName = getIntent().getStringExtra("titleName");
        List<CouponBean> coupons = getIntent().getParcelableArrayListExtra("coupons");
        if (coupons != null) {
            mData.clear();
            mData.addAll(coupons);
            isNeedBack = true;
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }

        initView();
        initData();


    }

    private void initData() {
        if (mData.isEmpty()) {
            mPresenter.getListCoupon();
        }
    }

    private void initView() {
        titleBar.setTitleText(titleName);
        couponAdapter = new CouponAdapter(mContext, mData);
        recyclerCoupon.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerCoupon.setAdapter(couponAdapter);
        recyclerCoupon.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 10));
        couponAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isNeedBack) {
                    Intent intent = new Intent();
                    intent.putExtra("coupon", mData.get(position));
                    CouponActivity.this.setResult(RESULT_OK, intent);
                    CouponActivity.this.finish();
                }
            }
        });
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        swipeToLoadLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        if (isNeedBack) {
            mPresenter.queryShoppingCart();
        } else {
            mPresenter.getListCoupon();
        }
    }

    @Override
    protected CouponPresenter createPresenter() {
        return new CouponPresenter();
    }

    @Override
    public void listOnSuccess(List<CouponBean> couponBeanList) {
        if (couponBeanList != null && !couponBeanList.isEmpty()) {
            mData.clear();
            mData.addAll(couponBeanList);
            couponAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        }

        swipeToLoadLayout.finishRefresh();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        swipeToLoadLayout.finishRefresh();
    }

}
