package com.example.app4grzx.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.example.app4grzx.R;
import com.just.agentweb.AgentWeb;
import com.example.app4public.event.WebViewAvatarEvent;
import com.example.app4grzx.activity.BaseAgentWebActivity;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/*横屏webView*/
public class WebTaskActivitybeifen extends BaseAgentWebActivity {
    private String url;
    private String validateurl;
    private String AppToken;
    private String AnsweringState;
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    private TextView mTitleTextView;
//    private String callbackData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_task);
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入WebTaskActivitybeifen成功" );
                }
            }
        }
        LinearLayout mLinearLayout = (LinearLayout) this.findViewById(R.id.container);
        Toolbar mToolbar = (Toolbar) this.findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setTitle("");
        mTitleTextView = (TextView) this.findViewById(R.id.toolbar_title);
        this.setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebTaskActivitybeifen.this.finish();
            }
        });
        getUrl();
    }


    protected String getUrl() {
        validateurl = getIntent().getStringExtra("validateurl");
        AppToken = getIntent().getStringExtra("AppToken");
        AnsweringState = getIntent().getStringExtra("AnsweringState");
//        mAgentWeb.getJsAccessEntrace().quickCallJs("getAppToken", callbackData);
        if (TextUtils.isEmpty(validateurl)) {
            finish();
        }
        //初始化js交互对象
        if (mAgentWeb != null) {
            //注入对象
            Log.e("testaaa", "AppToken: "+AppToken );
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterfaceWeb(mAgentWeb, this));
        }
        return validateurl;
    }

    private Handler deliver = new Handler(Looper.getMainLooper());

    public class AndroidInterfaceWeb {
        private AgentWeb agent;
        private Context context;
        private String callbackData;

        public AndroidInterfaceWeb(AgentWeb agent, Context context) {
            this.agent = agent;
            this.context = context;
        }

        @JavascriptInterface
        public String getAppToken() {
            deliver.post(new Runnable() {
                @Override
                public void run() {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("token", AppToken);
                        callbackData = obj.toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            return callbackData;
        }
        /*@JavascriptInterface
        public String getAppToken() throws JSONException {
            deliver.post(new Runnable() {
                @Override
                public void run() {
            JSONObject obj = new JSONObject();
            obj.put("token", AppToken);
            final String callbackData = obj.toString();
            LogUtils.e("ssssssssssss", callbackData);
            return callbackData;
        }*/
    }


    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.container);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected int getIndicatorColor() {
        return Color.parseColor("#ff0000");
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 10) {
                title = title.substring(0, 10).concat("...");
            }
        }
        mTitleTextView.setText(title);
    }

    @Override
    protected int getIndicatorHeight() {
        return 3;
    }


    @Override
    protected void onDestroy() {
        if (AnsweringState.equals(TYPE_TODY)) {
            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_TODY));
        } else {
            EventBus.getDefault().post(new WebViewAvatarEvent(TYPE_RECENTLY));
        }
        super.onDestroy();
    }
}

