package com.example.app4grzx.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4public.manager.SPManager;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4grzx.R;
import com.example.app4grzx.presenter.ChangePwdPresenter;
import com.example.app4grzx.view.ChangePwdView;
import com.example.app4public.widget.DialogUtil;
import com.example.app4public.widget.PwdEditText;
import com.example.app4public.widget.TitleBar;

/**
 * 描述：
 * - 修改密码
 *  zs
 */

public class ChangePwdActivity extends BaseMVPActivity1<ChangePwdView, ChangePwdPresenter> implements ChangePwdView, View.OnClickListener {
    TitleBar titleBar;
    PwdEditText etOldpwd;
    PwdEditText etNewpwd;
    PwdEditText etAgainpwd;
    Button btAffirm;
    RelativeLayout rlOld;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_changepwd;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入ChangePwdActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        rlOld = (RelativeLayout) findViewById(R.id.rl_old);
        etOldpwd = (PwdEditText) findViewById(R.id.et_oldpwd);
        etNewpwd = (PwdEditText) findViewById(R.id.et_newpwd);
        etAgainpwd = (PwdEditText) findViewById(R.id.et_againpwd);
        btAffirm = (Button) findViewById(R.id.bt_affirm);
        btAffirm.setOnClickListener(this);
        initView();
        initData();

    }

    private void initData() {
    }

    private void initView() {
        if (SPManager.getUser().getPassword()==null||SPManager.getUser().getPassword().isEmpty()){//toc没设置密码
            rlOld.setVisibility(View.GONE);
        }
    }

    @Override
    protected ChangePwdPresenter createPresenter() {
        return new ChangePwdPresenter();
    }


    @Override
    public void changeSuccess() {
        SPManager.getUser().setPassword(etNewpwd.getText().toString());
                DialogUtil.showReLoginDialog(this, "密码修改成功", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //密码修改成功之后应该关掉所有界面并跳转到登录界面，
//                logout();
                ChangePwdActivity.this.finish();
            }
        });

    }

    @Override
    public void changeFailure(String msg) {
        ToastUtils.showShort(msg);

    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);

        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity");
        startIntent.putExtra("autoLogin",false);
        startActivity(startIntent);
    }

    @Override
    public void onClick(View v) {
        String old = etOldpwd.getText().toString().trim();
        String newP = etNewpwd.getText().toString().trim();
        String again = etAgainpwd.getText().toString().trim();
        if (SPManager.getUser().getPassword()==null||SPManager.getUser().getPassword().isEmpty()){
            mPresenter.confirm(newP, again);
        }else {
            mPresenter.confirmPwd(old, newP, again);
        }
    }
}
