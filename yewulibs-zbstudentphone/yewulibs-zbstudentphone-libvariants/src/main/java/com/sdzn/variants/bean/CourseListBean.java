package com.sdzn.variants.bean;

/**
 * 精品课 专题课 外层
 *  */
public class CourseListBean {

    /**
     * page : 1
     * pages : 0
     * total : 2
     * pageSize : 10
     * list : {"total":2,"rows":[{"courseId":306,"courseName":"专题","isSchool":0,"schoolId":62,"schoolName":null,"isAvaliable":3,"classId":null,"className":null,"subjectId":0,"subjectName":null,"addTime":"2020-07-01 14:19:01","sourcePrice":11,"currentPrice":111,"title":"","context":"<p>111111<\/p>","lessionNum":0,"logo":"https://csfile.fuzhuxian.com/1593584338180","updateTime":null,"pageViewcount":0,"endTime":"2020-07-09 00:00:00","losetype":0,"loseTime":"","sequence":0,"sellType":"SPECIAL","liveBeginTime":null,"liveEndTime":"2020-07-01 14:19:00","signEndTime":"2020-07-02 00:00:00","refundEndTime":null,"auditionLessionCount":0,"packageType":null,"grade":null,"courseTerm":3,"liveCourseId":0,"isArrange":0,"isPublic":null,"appointSchoolId":null,"appointSchoolName":null,"projectId":7,"projectName":"专题名称","freeLessionNum":0,"gradeType":null,"teacherId":null,"teacherName":null,"kpointNum":null,"packageEduCourseList":null,"courseKpointLits":null,"isPurchase":null,"isCollection":null,"time":null,"isRelationLiveCourse":null},{"courseId":307,"courseName":"专题","isSchool":0,"schoolId":62,"schoolName":"","isAvaliable":1,"classId":null,"className":"","subjectId":0,"subjectName":"","addTime":"2020-07-01 14:30:08","sourcePrice":11,"currentPrice":111,"title":"","context":"<p>1111<\/p>","lessionNum":0,"logo":"https://csfile.fuzhuxian.com/1593585003131","updateTime":null,"pageViewcount":0,"endTime":"2020-07-03 00:00:00","losetype":0,"loseTime":"","sequence":0,"sellType":"SPECIAL","liveBeginTime":null,"liveEndTime":null,"signEndTime":"2020-07-03 00:00:00","refundEndTime":null,"auditionLessionCount":0,"packageType":null,"grade":"[]","courseTerm":3,"liveCourseId":0,"isArrange":0,"isPublic":null,"appointSchoolId":null,"appointSchoolName":null,"projectId":7,"projectName":"专题名称","freeLessionNum":0,"gradeType":null,"teacherId":null,"teacherName":null,"kpointNum":null,"packageEduCourseList":null,"courseKpointLits":null,"isPurchase":null,"isCollection":null,"time":null,"isRelationLiveCourse":null}]}
     */

    private int page;
    private int pages;
    private int total;
    private int pageSize;
    private CourseListRows list;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public CourseListRows getList() {
        return list;
    }

    public void setList(CourseListRows list) {
        this.list = list;
    }

}
