package com.sdzn.variants.network.subscriber;

import com.blankj.utilcode.util.ToastUtils;
import com.sdzn.fzx.student.libutils.util.MyLogUtil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okio.Buffer;
import okio.BufferedSource;

public class LoggingInterceptor implements Interceptor {
    private final Charset UTF8 = Charset.forName("UTF-8");

    /*公共拦截器*/
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        RequestBody requestBody = request.body();
        String body = null;
        if (requestBody != null) {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            Charset charset = UTF8;
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
            }
            body = buffer.readString(charset);
        }
//        MyLogUtil.d("HTTP_LOG", String.format("发送请求\nmethod：%s\nurl：%s\nheaders: %sbody：%s", request.method(), request.url(), request.headers(), body));
        long startNs = System.nanoTime();
        Response response = chain.proceed(request);
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
        ResponseBody responseBody = response.body();
        String rBody = null;
        if (HttpHeaders.hasBody(response)) {
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE); // Buffer the entire body.
            Buffer buffer = source.buffer();
            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                try {
                    charset = contentType.charset(UTF8);
                } catch (UnsupportedCharsetException e) {
                    e.printStackTrace();
                }
            }
            rBody = buffer.clone().readString(charset);
        }
//        MyLogUtil.d("HTTP_LOG", String.format("收到响应 %s%s %ss\n请求url：%s\n请求body：%s\n响应body：%s", response.code(), response.message(), tookMs, response.request().url(), body, rBody));
        MyLogUtil.d("HTTP_LOG", "                                              ");
        MyLogUtil.d("HTTP_LOG", "---------------HTTP_LOG请求start--------------");
//        MyLogUtil.d("HTTP_LOG", String.format("发送请求\nmethod：%s\nurl：%s\nheaders: %sbody：%s", request.method(), request.url(), request.headers(), body));
        MyLogUtil.d("HTTP_LOG", String.format("发送请求"));
        MyLogUtil.d("HTTP_LOG", String.format("method：%s", request.method()));
        MyLogUtil.d("HTTP_LOG", String.format("url：%s", request.url()));
        MyLogUtil.d("HTTP_LOG", String.format("请求headers: %s", request.headers()));
        MyLogUtil.d("HTTP_LOG", String.format("请求body: %s", body));
//        MyLogUtil.d("HTTP_LOG", String.format("收到响应 %s%s %ss\n响应body：%s", response.code(), response.message(), tookMs, rBody));
        MyLogUtil.d("HTTP_LOG", String.format("收到响应 %s %s %ss", response.code(), response.message(), tookMs));
        MyLogUtil.d("HTTP_LOG", String.format("响应body：%s", rBody));
        MyLogUtil.d("HTTP_LOG", "---------------HTTP_LOG请求end----------------");
        MyLogUtil.d("HTTP_LOG", "                                              ");
//        try {
//            ResponseSlbBean responseSlbBean = GsonUtils.fromJson(rBody, ResponseSlbBean.class);
//            if (null != responseSlbBean && responseSlbBean.getCode() == 401) {
//                set_token_out();
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
        return response;
    }

    private void set_token_out() {
        ToastUtils.showLong("登录失效，请重新登录！");

    }
}

