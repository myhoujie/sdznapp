package com.sdzn.variants.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class CourseDetailBean {

//    /**
//     * addTime : 2017-01-23 03:52:46
//     * classId : 0
//     * context : <ol class="exp-conent-orderlist list-paddingleft-2"><li class="exp-content-list "><p>如果你是零基础，你首先要学习一定的乐理知识，在这里给大家推荐一本书，是李重光的《基础乐理》，乐理是学习音乐的基础，没有乐理知识，学音乐知识纸上谈兵而已。如果你有一定的乐理基础，或者会某些乐器，那你就可以直接到第二步了。</p></li><li class="exp-content-list "><p>2</p><p>学完乐理后，就到最重要的一步了。这就是和声学。和声学是音乐的灵魂，是音乐不可缺少的色彩，因为之后的编曲肯定会用到和声，所以和声是不可或缺的一门知识。在这里给大家推荐一本书，是苏联作家斯波索宾等人写的《和声学》，很经典的一本书。</p></li><li class="exp-content-list "><p>3</p><p>要想像那些音乐人一样写出一首歌，要准备的东西完全还不止这些。这些只是理论准备，你要写出优美动听的好歌曲，好旋律，好音乐，作曲是必须要学的，其实，当你学完前面所说的知识后，你会发现你已经差不多会作曲了，学作曲只是丰富你的作曲知识而已，想做出现在的流行歌曲，如果你不深学作曲这门知识，其实也可以，当然学了更好。</p></li><li class="exp-content-list "><p>4</p><p>现在你已经会了这么多，在这里，大家不要忽视一件事情，就是在你学习以上所说的知识时，最好能有一个乐器相伴，这样最好。下来，我们就要开始实践了，这就用到电脑了。有一个软件cubase，是一个比较专业的做音乐的软件，当然这类软件很多，网上可以找得到。这个软件是做什么的呢？它就是用来将我们之前所学习的所有理论知识，运用在实践中，说白了，我们要用这个软件做出一首完整的歌曲。</p></li></ol><p><br/></p>
//     * courseId : 133
//     * courseKpointList : [{"addTime":"2017-01-24 11:52:51","childKpoints":[],"courseId":133,"fileType":"VIDEO","free":2,"isavaliable":0,"kpointAtlasesList":[],"kpointId":514,"kpointList":[],"kpointType":1,"name":"古典音乐","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"10:30","queryLimitNum":0,"sort":1,"teacherId":0,"videoType":"INXEDUCLOUD","videoUrl":"d105ba85ee6850800b163e9509ddb1e4|drmkeytempplay99A46|ucodetempplayiqjmT"}]
//     * courseName : 高中零基础怎样学好音乐
//     * currentPrice : 3
//     * isavaliable : 1
//     * lessionNum : 5
//     * logo : /images/upload/course/20170124/1485229913614.jpg
//     * loseTime : 90
//     * loseType : 1
//     * pageBuycount : 0
//     * pageViewcount : 23
//     * sellType : COURSE
//     * sourcePrice : 12
//     * subjectId : 327
//     * teacherList : [{"career":"上海师范大学法学院副教授、清华大学法学博士。自2004年至今已有9年的司法考试培训经验。长期从事司法考试辅导，深知命题规律，了解解题技巧。内容把握准确，授课重点明确，层次分明，调理清晰，将法条法理与案例有机融合，强调综合，深入浅出。","createTime":"2015-04-03 14:23:06","customerId":0,"education":"上海师范大学法学院副教授","id":81,"invalid":false,"isStar":1,"name":"李立","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","sort":9,"status":0,"subjectId":209,"updateTime":"2015-09-15 14:18:48"},{"career":"法学博士，北京师范大学马克思主义学院副教授，专攻毛泽东思想概论、邓小平理论，长期从事考研辅导。出版著作两部，发表学术论文30余篇，主持国家社会科学基金项目和教育部重大课题子课题各一项，参与中央实施马克思主义理论研究和建设工程项目。","createTime":"2015-04-03 14:21:03","customerId":0,"education":"考研政治辅导实战派专家，全国考研政治命题研究组核心成员。","id":80,"invalid":false,"isStar":2,"name":"潘新强","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297935589.jpg","sort":8,"status":0,"subjectId":221,"updateTime":"2017-02-20 16:27:54"},{"career":"曾参与北京市及全国多项数学活动的命题和组织工作，多次带领北京队参加高中、初中、小学的各项数学竞赛，均取得优异成绩。教学活而新，能够调动学生的学习兴趣并擅长对学生进行思维点拨，对学生学习习惯的养成和非智力因素培养有独到之处，是一位深受学生喜爱的老师。","createTime":"2015-04-03 14:19:28","customerId":0,"education":"华东师范大学数学系硕士生导师，中国数学奥林匹克高级教练","id":78,"invalid":false,"isStar":1,"name":"陈红","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297999141.jpg","sort":0,"status":0,"subjectId":224,"updateTime":"2015-09-15 14:20:00"}]
//     * title : 高中零基础怎样学好音乐
//     * updateTime : 2017-01-23 03:52:46
//     * courseList : []
//     * liveBeginTime : 2016-08-01 00:00:00
//     * liveEndTime : 2016-12-08 00:00:00
//     * recommendId : 0
//     * state : 直播结束
//     */



    private String context;
    private int courseId;
    private String courseName;
    private double currentPrice;
    private double sourcePrice;
    private int isavaliable;
    private String logo;
    private String sellType;
    private List<CourseKpointListBean> courseKpointLits;
    private List<CourseCatalogueBean> packageEduCourseList;//page 包
    private String liveBeginTime;
    private String liveEndTime;
    private String packageType;
    private String signEndTime;
    private boolean isPurchase;//true 已经报名  是否已购买
    private boolean isRelationLiveCourse;//true   直播转点播
    private boolean isCollection;  //是否收藏

    private String courseDescString;// 科目 及 老师
    private String date;//回放有效期

    private int projectId;
    private String projectName;

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(double sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public String getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(String liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public String getCourseDescString() {
        if (courseDescString!=null) {
            return courseDescString;
        }
        return "";
    }

    public void setCourseDescString(String courseDescString) {
        this.courseDescString = courseDescString;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public void setCollection(boolean collection) {
        isCollection = collection;
    }

    public boolean isRelationLiveCourse() {
        return isRelationLiveCourse;
    }

    public void setRelationLiveCourse(boolean relationLiveCourse) {
        isRelationLiveCourse = relationLiveCourse;
    }

    public boolean isPurchase() {
        return isPurchase;
    }

    public void setPurchase(boolean purchase) {
        isPurchase = purchase;
    }


    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }



    public List<CourseKpointListBean> getCourseKpointList() {
        if (courseKpointLits == null) {
            courseKpointLits = new ArrayList<>();
        }
        return courseKpointLits;
    }

    public void setCourseKpointList(List<CourseKpointListBean> courseKpointList) {
        this.courseKpointLits = courseKpointList;
    }

    public List<CourseCatalogueBean> getCourseList() {
        if (packageEduCourseList == null) {
            packageEduCourseList = new ArrayList<>();
        }
        return packageEduCourseList;
    }

    public void setCourseList(List<CourseCatalogueBean> courseList) {
        this.packageEduCourseList = courseList;
    }


    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getSignEndTime() {
        return signEndTime;
    }

    public void setSignEndTime(String signEndTime) {
        this.signEndTime = signEndTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
