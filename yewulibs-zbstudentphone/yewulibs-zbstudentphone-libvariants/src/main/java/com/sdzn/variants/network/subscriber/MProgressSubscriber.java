package com.sdzn.variants.network.subscriber;

import android.content.Context;

import com.blankj.utilcode.util.NetworkUtils;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.exception.CodeException;
import com.sdzn.core.network.exception.HttpTimeException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.subscriber.ProgressSubscriber;
import com.sdzn.core.utils.AppManager;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.ParseException;

/**
 * 描述：
 * - 可实现进度管理Subscriber
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class MProgressSubscriber<T> extends ProgressSubscriber<T> {


    public MProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context) {
        super(mSubscriberOnNextListener, context);
    }

    public MProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress) {
        super(mSubscriberOnNextListener, context, isShowProgress);
    }

    public MProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress, String showMsg) {
        super(mSubscriberOnNextListener, context, isShowProgress, showMsg);
    }

    public MProgressSubscriber(SubscriberOnNextListener<T> subscriberOnNextListener) {
        super(subscriberOnNextListener);
    }

    @Override
    public void onError(Throwable e) {
        if (isShowProgress) {
            dismissProgressDialog();
        }
        if (!NetworkUtils.isConnected()){
            ApiException apiException = new ApiException(e);
            apiException.setCode(CodeException.HTTP_ERROR);
            apiException.setMessage("请检查网络");
            mSubscriberOnNextListener.onFail(apiException);
        }

        if (e instanceof ApiException) {
            ApiException exception = (ApiException) e;
//            if (NetExceptionCode.TOKEN_NONE == exception.getCode()
//                    || NetExceptionCode.TOKEN_NULL == exception.getCode()) {//token异常重新登陆
//                reLogin();
//                return;
//            }
            mSubscriberOnNextListener.onFail(e);
        } else if (e instanceof HttpTimeException) {
            HttpTimeException exception = (HttpTimeException) e;
            ApiException apiException = new ApiException(exception);
            apiException.setCode(CodeException.RUNTIME_ERROR);
            apiException.setMessage("连接失败,请检查网络");
            mSubscriberOnNextListener.onFail(apiException);
        } else if (e instanceof ConnectException || e instanceof SocketTimeoutException) {
            /*链接异常*/
            ApiException apiException = new ApiException(e);
            apiException.setCode(CodeException.HTTP_ERROR);
            apiException.setMessage("连接失败,请检查网络");
            mSubscriberOnNextListener.onFail(apiException);
        } else if (e instanceof JSONException || e instanceof ParseException) {
            ApiException apiException = new ApiException(e);
            apiException.setCode(CodeException.JSON_ERROR);
            apiException.setMessage("连接失败,请检查网络");
            mSubscriberOnNextListener.onFail(apiException);
        } else {
            mSubscriberOnNextListener.onFail(new ApiException(e, CodeException.UNKNOWN_ERROR));
        }

    }

    private void reLogin() {
        AppManager.getAppManager().appExit();
//        SPManager.changeLogin(context, false);
//        IntentController.toMain(context, false);
    }

    @Override
    public void onNext(T t) {
        mSubscriberOnNextListener.onNext(t);
    }

}