package com.sdzn.variants.network;

import android.text.TextUtils;

import com.sdzn.pkt.student.phone.BuildConfig3;
import com.sdzn.variants.network.interceptor.CommonInterceptor;
import com.sdzn.core.base.BaseApplication;
import com.sdzn.core.network.interceptor.HttpCacheInterceptor;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.variants.network.subscriber.LoggingInterceptor;

import java.io.File;
import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 描述：
 * - retrofit请求管理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class RestApi {
    public static boolean isDebug = true;

    // create retrofit singleton
    private Retrofit createApiClient(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient(isDebug))
                .build();
    }

    //在访问HttpMethods时创建单例
    private static class SingletonHolder {
        private static final RestApi INSTANCE = new RestApi();

        private SingletonHolder() {
            throw new UnsupportedOperationException("u can't instantiate SingletonHolder...");
        }
    }

    public static synchronized RestApi getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void deBug(boolean isDebug) {
        this.isDebug = isDebug;
    }


    public <T> T create(String baseUrl, Class<T> clz) {
        String serviceUrl = "";
        if (!TextUtils.isEmpty(baseUrl)) {
            serviceUrl = baseUrl;
        } else {
            try {
                Field field1 = clz.getField("BASE_URL");
                serviceUrl = (String) field1.get(clz);
            } catch (Exception e) {
                LogUtils.i("exception", e);
            }
        }

        return createApiClient(serviceUrl).create(clz);
    }

    public <T> T create(Class<T> clz) {
        String serviceUrl = "";
        try {
            Field field1 = clz.getField("BASE_URL");
            serviceUrl = (String) field1.get(clz);
            if (TextUtils.isEmpty(serviceUrl)) {
                throw new NullPointerException("base_url is null");
            }
        } catch (Exception e) {
            LogUtils.i("exception", e);
        }
        return createApiClient(serviceUrl).create(clz);
    }

    /**
     * 8090端口
     */
    public <T> T createNew(Class<T> clz) {
        String serviceUrl = BuildConfig3.BASE_ADDRESS;
        return createApiClient(serviceUrl).create(clz);
    }

    /**
     * 创建一个okhttp实例
     *
     * @param debug
     * @return
     */
    private OkHttpClient createOkHttpClient(boolean debug) {
        //缓存
        int size = 1024 * 1024 * 100;
        File cacheFile = new File(BaseApplication.getInstance().getCacheDir(), "OkHttpCache");
        Cache cache = new Cache(cacheFile, size);

        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(new CommonInterceptor())
                .addNetworkInterceptor(new HttpCacheInterceptor())
//                .addInterceptor(new HttpLoggingInterceptor().setLevel(
//                        debug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .addInterceptor(new LoggingInterceptor())
                .cache(cache)
                .build();

    }
}
