package com.sdzn.variants.bean;

/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public class MessageDetailBean {

    /**
     * msgReceive : {"addTime":1502963407000,"content":"您的验证码为2R017T0817H1723X1674，欢迎注册因酷网校平台 ","cusId":0,"friNum":0,"groupNum":0,"id":1786,"letterNum":0,"receivingCusId":1462,"showname":"","status":1,"systemNum":0,"type":1,"unReadNum":0,"updateTime":1503017069000}
     */

    private MsgReceiveBean msgReceive;

    public MsgReceiveBean getMsgReceive() {
        return msgReceive;
    }

    public void setMsgReceive(MsgReceiveBean msgReceive) {
        this.msgReceive = msgReceive;
    }

    public static class MsgReceiveBean {
        /**
         * addTime : 1502963407000
         * content : 您的验证码为2R017T0817H1723X1674，欢迎注册因酷网校平台
         * cusId : 0
         * friNum : 0
         * groupNum : 0
         * id : 1786
         * letterNum : 0
         * receivingCusId : 1462
         * showname :
         * status : 1
         * systemNum : 0
         * type : 1
         * unReadNum : 0
         * updateTime : 1503017069000
         */

        private long addTime;
        private String content;
        private int cusId;
        private int friNum;
        private int groupNum;
        private int id;
        private int letterNum;
        private int receivingCusId;
        private String showname;
        private int status;
        private int systemNum;
        private int type;
        private int unReadNum;
        private long updateTime;

        public long getAddTime() {
            return addTime;
        }

        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getCusId() {
            return cusId;
        }

        public void setCusId(int cusId) {
            this.cusId = cusId;
        }

        public int getFriNum() {
            return friNum;
        }

        public void setFriNum(int friNum) {
            this.friNum = friNum;
        }

        public int getGroupNum() {
            return groupNum;
        }

        public void setGroupNum(int groupNum) {
            this.groupNum = groupNum;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLetterNum() {
            return letterNum;
        }

        public void setLetterNum(int letterNum) {
            this.letterNum = letterNum;
        }

        public int getReceivingCusId() {
            return receivingCusId;
        }

        public void setReceivingCusId(int receivingCusId) {
            this.receivingCusId = receivingCusId;
        }

        public String getShowname() {
            return showname;
        }

        public void setShowname(String showname) {
            this.showname = showname;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getSystemNum() {
            return systemNum;
        }

        public void setSystemNum(int systemNum) {
            this.systemNum = systemNum;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUnReadNum() {
            return unReadNum;
        }

        public void setUnReadNum(int unReadNum) {
            this.unReadNum = unReadNum;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }
    }
}
