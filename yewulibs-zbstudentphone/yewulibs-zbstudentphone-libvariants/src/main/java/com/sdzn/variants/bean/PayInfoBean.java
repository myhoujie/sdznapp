package com.sdzn.variants.bean;

import com.google.gson.annotations.SerializedName;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/24
 */
public class PayInfoBean {

    /**
     * wxpayParams : {"appid":"wxef456396158c20cc","noncestr":"9565f1cd832c9675c76672081c819342","package":"Sign=WXPay","partnerid":"1480136002","prepayid":"wx20170724101642907a8b87c40223247207","sign":"9358DB7373D73D475984525F9167847F","timestamp":"1500891437"}
     * payType : WEIXIN
     */

    /**
     * alipayOrderStr : alipay_sdk=alipay-sdk-java-dynamicVersionNo&app_id=2017071907810458&biz_content=%7B%22body%22%3A%221464-146420170724181807389-PAY1500891487614%22%2C%22out_trade_no%22%3A%22PAY1500891487614%22%2C%22passback_params%22%3A%221464%2C146420170724181807389%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%2C%22seller_id%22%3A%222088721068690094%22%2C%22subject%22%3A%22%E6%99%BA%E5%9B%8A%E5%AD%A6%E5%A0%82%3A146420170724181807389%22%2C%22timeout_express%22%3A%221d%22%2C%22total_amount%22%3A%22111.02%22%7D&charset=utf-8&format=json&method=alipay.trade.app.pay¬ify_url=http%3A%2F%2F192.168.0.213%3A8080%2Fzfbpay%2Forder%2Falipaynotify%2F1&sign=otw%2Ft2AnGCBPDl3YtDyKcsJeFvHx8Pe0mWoQVnmgFmOgW4TmaPH0NdAMIC5N6VcTuDjZ61T7QLPBknLGnmLTyl9OgTyVdkseAPTzrDLsOt681iljgZFDWrZVvfRaI6wRFCYiGK1EQv93bYeaNy5i4BBWf%2FB%2FGPD7HBtnsxSdMPbsmUjZZhegPOXz%2BJCq918GBky037FVCNyUjLXN%2BODrFn1GeYTnJNjiU8p72EyStYmqx1yfSeC0Z7jRly1cayYQOWts7A3hyMv6qX3CvTIlInrfJDZqL9oZEM98jh%2FfdvDMwCcUSFvLUwT08w%2Byv4C8bpdgKookoYJSS4VlhItwLg%3D%3D&sign_type=RSA2×tamp=2017-07-24+18%3A18%3A07&version=1.0
     */

    private String payType;
    private WxpayParamsBean wxpayParams;
    private String alipayOrderStr;

    public WxpayParamsBean getWxpayParams() {
        return wxpayParams;
    }

    public void setWxpayParams(WxpayParamsBean wxpayParams) {
        this.wxpayParams = wxpayParams;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getAlipayOrderStr() {
        return alipayOrderStr;
    }

    public void setAlipayOrderStr(String alipayOrderStr) {
        this.alipayOrderStr = alipayOrderStr;
    }

    public static class WxpayParamsBean {
        /**
         * appid : wxef456396158c20cc
         * noncestr : 9565f1cd832c9675c76672081c819342
         * package : Sign=WXPay
         * partnerid : 1480136002
         * prepayid : wx20170724101642907a8b87c40223247207
         * sign : 9358DB7373D73D475984525F9167847F
         * timestamp : 1500891437
         */


        private String appid;
        private String noncestr;
        @SerializedName("package")
        private String packageX;
        private String partnerid;
        private String prepayid;
        private String sign;
        private String timestamp;

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }
}
