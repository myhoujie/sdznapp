package com.sdzn.variants.network;

import android.content.Context;

import com.sdzn.core.utils.SPUtils;
import com.sdzn.fzx.student.libutils.app.App2;

/**
 * 描述：
 * - sp文件管理类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SPToken {
    private static final String SP_NAME = "AppConfig";
    /**
     * 改变用户登录状态
     */
    public static void saveToken(String token) {
        SPUtils.put(App2.get(), SP_NAME, "token", token);
    }

    /**
     * 改变用户登录状态
     */
    public static String getToken() {
        return (String) SPUtils.get(App2.get(), SP_NAME, "token", "");
    }

    /**
     * 处于登录状态
     */
    public static boolean autoLogin(Context context) {
        return isLogin();
    }

    /**
     * 用户是否已登录
     *
     * @return
     */
    public static boolean isLogin() {
        return (boolean) SPUtils.get(App2.get(), SP_NAME, "islogin", false);
    }
}
