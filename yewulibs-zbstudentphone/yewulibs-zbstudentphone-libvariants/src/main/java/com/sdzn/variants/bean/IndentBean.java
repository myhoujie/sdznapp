package com.sdzn.variants.bean;

import java.util.List;

/**
 * 描述：订单
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public class IndentBean {


    /**
     * {
     * "cashAmount": 0.01,
     * "courseList": [
     * {
     * "courseId": 22,
     * "courseName": "乡间晚风",
     * "currentPrice": "0.01",
     * "logo": "http://114.115.128.225:8030/images/upload/course/20171221/chuzhong3zuhe.jpg",
     * "packageType": 1,
     * "sellType": "PACKAGE",
     * "subjectName": "政治"
     * }
     * ],
     * "createTime": "2020-06-09 11:39",
     * "orderId": 1644,
     * "orderNo": "1965420200609113930356",
     * "states": "INIT"
     * }
     */


    private String cashAmount;
    private String createTime;
    private int orderId;
    private String orderNo;
    private String states;
    private List<TrxorderDetailListBean> courseList;


    public String getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(String cashAmount) {
        this.cashAmount = cashAmount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }


    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }


    public List<TrxorderDetailListBean> getTrxorderDetailList() {
        return courseList;
    }

    public void setTrxorderDetailList(List<TrxorderDetailListBean> trxorderDetailList) {
        this.courseList = trxorderDetailList;
    }

    public static class TrxorderDetailListBean {
        /**
         *
         */

        private int courseId;
        private String courseName;
        private String currentPrice;//currentPrice
        private String logo;
        private int packageType;
        private String sellType;
        private String subjectName;


        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getCourseImgUrl() {
            return logo;
        }

        public void setCourseImgUrl(String courseImgUrl) {
            this.logo = courseImgUrl;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }


        public String getCurrentPirce() {
            return currentPrice;
        }

        public void setCurrentPirce(String currentPirce) {
            this.currentPrice = currentPirce;
        }


        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

    }
}
