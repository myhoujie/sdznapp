package com.sdzn.variants.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/19
 */
public class OrderGoodsBean implements Parcelable {
    private double totalPrice;
    private List<GoodsBean> goodsBeanList;

    public static class GoodsBean implements Parcelable {
        private int courseId;
        private double price;
        private String logo;
        private int packageType;
        private String sellType;
        private String courseName;
        private int lessionNum;

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public int getPackageType() {
            return packageType;
        }

        public void setPackageType(int packageType) {
            this.packageType = packageType;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public int getLessionNum() {
            return lessionNum;
        }

        public void setLessionNum(int lessionNum) {
            this.lessionNum = lessionNum;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.courseId);
            dest.writeDouble(this.price);
            dest.writeString(this.logo);
            dest.writeInt(this.packageType);
            dest.writeString(this.sellType);
            dest.writeString(this.courseName);
            dest.writeInt(this.lessionNum);
        }

        public GoodsBean() {
        }

        protected GoodsBean(Parcel in) {
            this.courseId = in.readInt();
            this.price = in.readDouble();
            this.logo = in.readString();
            this.packageType = in.readInt();
            this.sellType = in.readString();
            this.courseName = in.readString();
            this.lessionNum = in.readInt();
        }

        public static final Creator<GoodsBean> CREATOR = new Creator<GoodsBean>() {
            @Override
            public GoodsBean createFromParcel(Parcel source) {
                return new GoodsBean(source);
            }

            @Override
            public GoodsBean[] newArray(int size) {
                return new GoodsBean[size];
            }
        };
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<GoodsBean> getGoodsBeanList() {
        return goodsBeanList;
    }

    public void setGoodsBeanList(List<GoodsBean> goodsBeanList) {
        this.goodsBeanList = goodsBeanList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.totalPrice);
        dest.writeList(this.goodsBeanList);
    }

    public OrderGoodsBean() {
    }

    protected OrderGoodsBean(Parcel in) {
        this.totalPrice = in.readDouble();
        this.goodsBeanList = new ArrayList<GoodsBean>();
        in.readList(this.goodsBeanList, GoodsBean.class.getClassLoader());
    }

    public static final Creator<OrderGoodsBean> CREATOR = new Creator<OrderGoodsBean>() {
        @Override
        public OrderGoodsBean createFromParcel(Parcel source) {
            return new OrderGoodsBean(source);
        }

        @Override
        public OrderGoodsBean[] newArray(int size) {
            return new OrderGoodsBean[size];
        }
    };
}
