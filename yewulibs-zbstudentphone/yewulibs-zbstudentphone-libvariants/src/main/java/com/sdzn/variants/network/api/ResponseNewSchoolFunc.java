package com.sdzn.variants.network.api;

import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.variants.bean.ResultBean;

import org.greenrobot.eventbus.EventBus;

import rx.functions.Func1;

/**
 * 将学校课程单独摘出来
 */
public class ResponseNewSchoolFunc<T> implements Func1<ResultBean<T>, T> {

    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;
    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0 == httpResult.getCode()) {
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getCode()) {//登录失效
//                SPManager.changeLogin(App.mContext, false);
//                EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程 我的课程
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMessage() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(String.valueOf(httpResult.getMessage())), httpResult.getCode());
            }
        }
    }
}