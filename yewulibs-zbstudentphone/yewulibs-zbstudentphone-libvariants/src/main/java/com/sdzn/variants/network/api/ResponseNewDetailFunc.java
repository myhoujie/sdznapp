package com.sdzn.variants.network.api;

import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.variants.bean.ResultBean;


import rx.functions.Func1;

/**
 * 将课程详情单独拿出来
 * 失效后返回详情页
 */
public class ResponseNewDetailFunc<T> implements Func1<ResultBean<T>, T> {
    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;

    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0 == httpResult.getCode()) {
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getCode()) {//登录失效
//                SPManager.changeLogin(App.mContext, false);
//                ToastUtils.showShort(httpResult.getMessage() + "");
//                IntentController.toLogin(App.mContext, true);
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMessage() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(String.valueOf(httpResult.getMessage())), httpResult.getCode());
            }
        }
    }
}