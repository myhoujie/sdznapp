package com.sdzn.variants.bean;

import java.util.List;

public class GradeJson {


    /**
     * levelId : 1
     * levelName : 小学六三制
     * educationId : 1
     * educationName : 六三制
     * childList : [{"gradeId":"11","gradeName":"一年级"},{"gradeId":"12","gradeName":"二年级"},{"gradeId":"13","gradeName":"三年级"},{"gradeId":"14","gradeName":"四年级"},{"gradeId":"15","gradeName":"五年级"},{"gradeId":"16","gradeName":"六年级"}]
     */

    private String levelId;
    private String levelName;
    private String educationId;
    private String educationName;
    private List<ChildListBean> childList;
    /**
     * LocalId : 1    1到 5
     * LocalName : 小学六三制
     */

    private String LocalId;
    private String LocalName;


    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    public List<ChildListBean> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildListBean> childList) {
        this.childList = childList;
    }

    public String getLocalId() {
        return LocalId;
    }

    public void setLocalId(String LocalId) {
        this.LocalId = LocalId;
    }

    public String getLocalName() {
        return LocalName;
    }

    public void setLocalName(String LocalName) {
        this.LocalName = LocalName;
    }

    public static class ChildListBean {
        /**
         * gradeName : 一年级
         * gradeId : 1
         */

        private String gradeName;
        private String gradeId;

        private String levelId;
        private String levelName;

        private boolean isChecked;

        public String getSectionId() {
            return levelId;
        }

        public String getSectionName() {
            return levelName;
        }

        public void setSectionName(String levelName) {
            this.levelName = levelName;
        }

        public void setSectionId(String levelId) {
            this.levelId = levelId;
        }



        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getGradeName() {
            return gradeName;
        }

        public void setGradeName(String gradeName) {
            this.gradeName = gradeName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }
    }


}
