package com.sdzn.variants.bean;

import java.util.List;

/**
 * 普通课程 最外层
 */

public class CourseIncludeBean {

    /**
     * eduCourse : {"addTime":"2020-02-25 16:04:16","appointSchoolId":"","assistantTeacherId":0,"auditionLessionCount":0,"classId":313,"context":"<p>直播教师端测试课<\/p>","courseGrossIncome":0,"courseId":152,"courseKpointLits":[{"addTime":"2020-02-25 16:04:20","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":226,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-25 16:04:45","liveEndTime":"2020-02-25 23:04:48","liveStates":"6","liveUrl":"","name":"测试机测试课1","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":1,"supplier":"undefined,undefined","teacherId":784,"teacherName":"讲师六","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-02-25 16:05:02","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":227,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-25 20:05:17","liveEndTime":"2020-02-25 21:05:19","liveStates":"6","liveUrl":"","name":"测试机测试课2","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":2,"supplier":"undefined,undefined","teacherId":784,"teacherName":"讲师六","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-02-26 16:59:15","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":228,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-26 16:59:37","liveEndTime":"2020-02-26 21:59:40","liveStates":"6","liveUrl":"","name":"测试机测试课3","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":3,"supplier":"undefined,undefined","teacherId":784,"teacherName":"讲师六","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-02-26 16:59:51","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":229,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-26 21:00:09","liveEndTime":"2020-02-26 22:00:15","liveStates":"6","liveUrl":"","name":"测试机测试课4","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":4,"supplier":"undefined,undefined","teacherId":784,"teacherName":"讲师六","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-02-27 14:05:48","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":230,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-27 14:06:12","liveEndTime":"2020-02-27 23:06:14","liveStates":"6","liveUrl":"","name":"测试机测试课5","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":5,"supplier":"undefined,undefined","teacherId":784,"teacherName":"讲师六","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-02-27 14:42:15","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":231,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-02-27 14:42:31","liveEndTime":"2020-02-27 22:42:33","liveStates":"6","liveUrl":"","name":"测试机测试课6","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":6,"supplier":"undefined,undefined","teacherId":856,"teacherName":"讲师7","videoLenth":0,"videoType":"undefined","videoUrl":""},{"addTime":"2020-03-02 10:59:26","assistantTeacherId":0,"classOver":null,"content":"","courseId":152,"fileType":"LIVE","isCopy":"no","isFree":2,"isStudy":false,"kpointId":235,"kpointPrice":0,"kpointType":1,"kpointVoList":[],"liveBeginTime":"2020-03-02 10:59:43","liveEndTime":"2020-03-02 23:59:45","liveStates":"4","liveUrl":"","name":"测试机测试课7","openType":"WEB","pageCount":0,"parentId":0,"playCount":0,"playTime":"","pushWay":1,"sort":7,"supplier":"undefined,undefined","teacherId":856,"teacherName":"讲师7","videoLenth":0,"videoType":"undefined","videoUrl":""}],"courseName":"直播测试课程","courseTerm":1,"currentPrice":0,"customerId":42,"endTime":null,"grade":2,"isAvaliable":1,"isCollection":false,"isPublic":0,"isPurchase":true,"isRelationLiveCourse":false,"kpointNum":0,"lessionNum":7,"levelId":0,"liveBeginTime":"2020-02-25 16:04:00","liveCourseId":0,"liveEndTime":"2020-03-02 23:59:00","logo":"http://114.115.128.225:8030/images/upload/course/20171221/gaozhong2yuwen.jpg","loseTime":"30","losetype":1,"mainTeacherId":0,"orgId":62,"packageEduCourseList":[],"packageType":0,"pageBuycount":0,"pageViewcount":0,"refundEndTime":"2020-02-29 16:02:39","sellType":"LIVE","sequence":0,"signEndTime":"2020-03-19 16:02:00","sourcePrice":0,"subjectId":326,"subjectName":"语文","teacherId":0,"teacherName":"","time":null,"title":"直播教师端测试课","updateTime":"2020-03-02 10:52:07"}
     * eduTeacherList : [{"accountNo":"","career":"","createTime":null,"customerId":0,"education":"","email":"","id":784,"isInvalid":0,"isShow":0,"isStar":0,"levelId":0,"name":"讲师六","orgId":0,"password":"","phoneNo":"","picPath":"http://file.znclass.com/6da165a9-6d6c-4654-a6cc-ad3858d2fe06微信截图_20190919111826.png","sort":0,"status":0,"subjectId":0,"token":"","updateTime":null}]
     */

    private CourseDetailBean eduCourse;
    private List<EduTeacherListBean> eduTeacherList;

    public CourseDetailBean getEduCourse() {
        return eduCourse;
    }

    public void setEduCourse(CourseDetailBean eduCourse) {
        this.eduCourse = eduCourse;
    }

    public List<EduTeacherListBean> getEduTeacherList() {
        return eduTeacherList;
    }

    public void setEduTeacherList(List<EduTeacherListBean> eduTeacherList) {
        this.eduTeacherList = eduTeacherList;
    }


    public static class EduTeacherListBean {
        /**
         * accountNo :
         * career :
         * createTime : null
         * customerId : 0
         * education :
         * email :
         * id : 784
         * isInvalid : 0
         * isShow : 0
         * isStar : 0
         * levelId : 0
         * name : 讲师六
         * orgId : 0
         * password :
         * phoneNo :
         * picPath : http://file.znclass.com/6da165a9-6d6c-4654-a6cc-ad3858d2fe06微信截图_20190919111826.png
         * sort : 0
         * status : 0
         * subjectId : 0
         * token :
         * updateTime : null
         */


        private Object createTime;
        private int customerId;
        private String email;
        private int id;
        private int levelId;
        private String name;
        private int orgId;
        private String password;
        private String phoneNo;
        private String picPath;
        private int sort;
        private int status;
        private int subjectId;
        private String token;
        private Object updateTime;


        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }


        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOrgId() {
            return orgId;
        }

        public void setOrgId(int orgId) {
            this.orgId = orgId;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPicPath() {
            return picPath;
        }

        public void setPicPath(String picPath) {
            this.picPath = picPath;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }
    }
}
