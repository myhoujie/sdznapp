package com.sdzn.variants.bean;

public class MineList {

    /**
     * courseName : 测试课程888888
     * time : 2020-01-09 12:12:43
     * recentCourseName : 新创建视频
     * courseId : 1
     * courseType : LIVE
     * progress : 100
     * count : 1
     * logo : http://114.115.128.225:8030/images/upload/course/20171221/chuzhong1yuwen.jpg
     */

    //公共
    private int courseId;
    private String logo;
    //我的课程 近期学习
    private String courseName;
    private String courseType;
    private double progress;
    //我的课程
    private String time;
    private String recentCourseName;
    private int count;
    //今日直播  近期学习
    private String teacherName;
    //今日直播
    private String CourseName;
    private String liveEndTime;
    private int courseKpointId;
    private String CourseKpointName;
    private String liveBeginTime;
    private String liveStates;




    public String getTodyCourseName() {
        return CourseName;
    }

    public void setTodyCourseName(String courseTodyName) {
        this.CourseName = courseTodyName;
    }


    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTime() {
        if (time!=null){
            return time;
        }
        return "2020-01-01";
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRecentCourseName() {
        return recentCourseName;
    }

    public void setRecentCourseName(String recentCourseName) {
        this.recentCourseName = recentCourseName;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public int getCourseKpointId() {
        return courseKpointId;
    }

    public void setCourseKpointId(int courseKpointId) {
        this.courseKpointId = courseKpointId;
    }

    public String getCourseKpointName() {
        return CourseKpointName;
    }

    public void setCourseKpointName(String courseKpointName) {
        CourseKpointName = courseKpointName;
    }

    public String getLiveBeginTime() {
        if (liveBeginTime!=null) {
            return liveBeginTime;
        }
        return "";
    }

    public void setLiveBeginTime(String liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public String getLiveStates() {
        return liveStates;
    }

    public void setLiveStates(String liveStates) {
        this.liveStates = liveStates;
    }
}
