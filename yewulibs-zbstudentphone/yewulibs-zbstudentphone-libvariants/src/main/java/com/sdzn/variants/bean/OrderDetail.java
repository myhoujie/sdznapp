package com.sdzn.variants.bean;

import java.util.List;

public class OrderDetail {

    /**
     * refund_info : {"notRefundReason":"","refundAmount":0,"refundEduReason":"","refundTime":null,"status":"","submitTime":null}
     * course_info : [{"courseName":"时代智囊点播","currentPrice":1,"logo":"https://csfile.fuzhuxian.com/1593584020269","sellType":"","status":4}]
     * order_info : {"createTime":null,"orderId":535,"orderNo":"222222220200704143139306","payTime":null,"payType":"alipay","states":"CANCEL","sumMoney":1}
     */

    private RefundInfoBean refund_info;
    private OrderInfoBean order_info;
    private List<CourseList> course_info;

    public RefundInfoBean getRefund_info() {
        return refund_info;
    }

    public void setRefund_info(RefundInfoBean refund_info) {
        this.refund_info = refund_info;
    }

    public OrderInfoBean getOrder_info() {
        return order_info;
    }

    public void setOrder_info(OrderInfoBean order_info) {
        this.order_info = order_info;
    }

    public List<CourseList> getCourse_info() {
        return course_info;
    }

    public void setCourse_info(List<CourseList> course_info) {
        this.course_info = course_info;
    }

    public static class RefundInfoBean {
        /**
         * notRefundReason :
         * refundAmount : 0
         * refundEduReason :
         * refundTime : null
         * status :
         * submitTime : null
         */

        private String notRefundReason;
        private String refundAmount;
        private String refundEduReason;
        private String refundTime;
        private String status;
        private String submitTime;

        public String getNotRefundReason() {
            return notRefundReason;
        }

        public void setNotRefundReason(String notRefundReason) {
            this.notRefundReason = notRefundReason;
        }

        public String getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(String refundAmount) {
            this.refundAmount = refundAmount;
        }

        public String getRefundEduReason() {
            return refundEduReason;
        }

        public void setRefundEduReason(String refundEduReason) {
            this.refundEduReason = refundEduReason;
        }

        public String getRefundTime() {
            return refundTime;
        }

        public void setRefundTime(String refundTime) {
            this.refundTime = refundTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSubmitTime() {
            return submitTime;
        }

        public void setSubmitTime(String submitTime) {
            this.submitTime = submitTime;
        }
    }

    public static class OrderInfoBean {
        /**
         * createTime : null
         * orderId : 535
         * orderNo : 222222220200704143139306
         * payTime : null
         * payType : alipay
         * states : CANCEL
         * sumMoney : 1
         */

        private String createTime;
        private int orderId;
        private String orderNo;
        private String payTime;
        private String payType;
        private String states;
        private String sumMoney;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public String getPayType() {
            return payType;
        }

        public void setPayType(String payType) {
            this.payType = payType;
        }

        public String getStates() {
            return states;
        }

        public void setStates(String states) {
            this.states = states;
        }

        public String getSumMoney() {
            return sumMoney;
        }

        public void setSumMoney(String sumMoney) {
            this.sumMoney = sumMoney;
        }
    }

}
