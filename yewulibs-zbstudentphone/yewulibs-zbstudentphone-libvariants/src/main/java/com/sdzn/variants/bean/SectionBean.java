package com.sdzn.variants.bean;

/**
 *
 */
public class SectionBean {

    /**
     * sectionId : 224
     * sectionName : IT互联网
     */

    private int sectionId;
    private String sectionName;
    private int educationId;
    private String educationName;
    private String gradeId;
    private String gradeName;
    private String sectionNameShow;

    public SectionBean(int sectionId, String sectionName) {
        this.sectionId = sectionId;
        this.sectionName = sectionName;

    }
    public SectionBean(int sectionId, String sectionName,int educationId,String educationName,String gradeId,String gradeName) {
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.educationId = educationId;
        this.educationName = educationName;
        this.gradeId = gradeId;
        this.gradeName = gradeName;
    }
    public SectionBean(int sectionId, String sectionName,int educationId,String educationName,String gradeId,String gradeName,String sectionNameShow) {
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.educationId = educationId;
        this.educationName = educationName;
        this.gradeId = gradeId;
        this.gradeName = gradeName;
        this.sectionNameShow = sectionNameShow;
    }

    public String getSectionNameShow() {
        return sectionNameShow;
    }

    public void setSectionNameShow(String sectionNameShow) {
        this.sectionNameShow = sectionNameShow;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public int getEducationId() {
        return educationId;
    }

    public void setEducationId(int educationId) {
        this.educationId = educationId;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
