package com.sdzn.pkt.student.phone;


import com.sdzn.variants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.OOO;

    public static final String FLAVOR = UrlManager.FLAVOR3;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE3;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME3;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS3;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL3;
    public static final String WXAPP_ID = UrlManager.WX_APP_ID3;

}
