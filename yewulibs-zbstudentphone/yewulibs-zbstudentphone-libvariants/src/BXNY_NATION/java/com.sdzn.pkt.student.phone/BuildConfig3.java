package com.sdzn.pkt.student.phone;

import com.sdzn.variants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.YYY;

    public static final String FLAVOR = UrlManager.FLAVOR2;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE2;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME2;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS2;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL2;
    public static final String WXAPP_ID = UrlManager.WX_APP_ID2;
}
