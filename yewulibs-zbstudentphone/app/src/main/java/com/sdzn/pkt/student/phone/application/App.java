package com.sdzn.pkt.student.phone.application;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.baijiayun.BJYPlayerSDK;
import com.example.app4public.manager.CrashHandler;
import com.example.app4public.manager.DaoManger;
import com.example.app4public.widget.ProgressHeader;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.just.agentweb.App2;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.sdzn.core.base.BaseApplication;
import com.sdzn.core.utils.App2Utils;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import me.jessyan.autosize.AutoSize;
import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.unit.Subunits;
import update.UpdateAppUtils;

//import com.tencent.bugly.crashreport.CrashReport;

public class App extends BaseApplication {
    public static App mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

//        initLog();//初始化日志配置
//        initCrashHandler();//全局捕获异常
        //网络初始化
        configRetrofitNet();
        initAutoSize();
        initUmeng();
        initRefreshFram(); //初始化下拉刷新控件
        DaoManger.getInstance();//初始化GreenDao
        new BJYPlayerSDK.Builder(this)
                .setDevelopMode(false)
                .setCustomDomain("b96152240")
                .build();
        UpdateAppUtils.init(this);//初始化软件更新
        configHios();
        initBugly();

    }

    private void configRetrofitNet() {
        RetrofitNetNew.config();
    }

    private void initAutoSize() {
        AutoSizeConfig.getInstance().getUnitsManager()
                .setSupportDP(false)
                .setSupportSP(false)
                .setSupportSubunits(Subunits.MM);
        AutoSize.initCompatMultiProcess(App2.get());

    }


    private void initBugly() {

        String packageName = App2Utils.getAppPackageName(mContext);
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());

        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setAppVersion(App2Utils.getAppVersionName(mContext));
        strategy.setAppPackageName(packageName);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));

        //正式发布 false    测试true
//        CrashReport.initCrashReport(this, "d65405595d", true, strategy);
        Bugly.init(this, "d65405595d", true, strategy);
    }

    private void configHios() {
        HiosHelper.config(App2.get().getPackageName() + ".slbapp.ad.web.page", App2.get().getPackageName() + ".slbapp.web.page");

    }

    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init();
    }
//    private void initBugly(){
//        CrashReport.initCrashReport(getApplicationContext(),"" , false);
//    }

    /**
     * 友盟分享
     */
    private void initUmeng() {
//        //推送
//        UMConfigure.init(this,null,null,UMConfigure.DEVICE_TYPE_PHONE,"471615d3904c021efd921e046b90e4f6");
//        PushAgent mPushAgent = PushAgent.getInstance(this);
//        //注册推送服务，每次调用register方法都会回调该接口
//        mPushAgent.register(new IUmengRegisterCallback() {
//            @Override
//            public void onSuccess(String deviceToken) {
//                //注册成功会返回device token
//                LogUtils.i("Token = "+ deviceToken);
//            }
//
//            @Override
//            public void onFailure(String s, String s1) {
//            }
//        });
//
//
//
//
//
//
//        .setWeixin(Config.WX_APP_ID, Config.WX_APP_KEY);
//        PlatformConfig.setQQZone(Config.QQ_ZOOM_APP_ID, Config.QQ_APP_KEY);
//        UMShareAPI.get(this);
    }


    //设置全局的Header构建器
    private void initRefreshFram() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @NonNull
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                return new ProgressHeader(context).setSpinnerStyle(SpinnerStyle.Translate);
            }
        });
    }

    private void initLog() {
//        new LogUtils.Builder(this)
//                .setGlobalTag(getPackageName())
//                .setBorderSwitch(false)
//                .setLogSwitch(BuildConfig.DEBUG);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1) {
            getResources();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Resources getResources() {
        Resources resources = super.getResources();
        if (resources.getConfiguration().fontScale != 1) {
            Configuration configuration = new Configuration();
            configuration.setToDefaults();
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return resources;
    }
}