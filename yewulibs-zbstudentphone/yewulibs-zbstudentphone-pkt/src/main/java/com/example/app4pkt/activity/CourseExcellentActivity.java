package com.example.app4pkt.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.R;
import com.example.app4pkt.presenter.CourseExcellentPresenter;
import com.example.app4pkt.view.CourseExcellentView;
import com.sdzn.variants.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.base.BaseRcvAdapter;
import com.example.app4public.adapter.RecommendAdapter;
import com.example.app4public.adapter.WrapAdapter;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;
import com.sdzn.variants.bean.CourseList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 精品课程列表
 */

public class CourseExcellentActivity extends BaseMVPActivity1<CourseExcellentView, CourseExcellentPresenter>
        implements CourseExcellentView, OnRefreshListener, OnRefreshLoadmoreListener {

    SmartRefreshLayout refreshLayout;
    EmptyLayout emptyLayout;
    RecyclerView rvCourseRec;
    TitleBar titleBar;

    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private int pageIndex = 1;//当前页
    private int pageSize = 10;//


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_excellent;
    }


    @Override
    protected CourseExcellentPresenter createPresenter() {
        return new CourseExcellentPresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入CourseExcellentActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refresh_layout);
        rvCourseRec = (RecyclerView) findViewById(R.id.rv_course);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);
        pageIndex = 1;
        initData();
        initView();
        loadNetData();
    }

    private void initData() {
        recommendCourses = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
                     Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                    return;
                }
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);

            }
        });
        refreshLayout.setOnRefreshLoadmoreListener(this);
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2));
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                    startIntent.putExtra("PACKAGE", recommendCourses.get(position).getSellType());
                    startIntent.putExtra("courseId", recommendCourses.get(position).getCourseId());
                    startIntent.putExtra("showLiveBtn",false );
                    startActivity(startIntent);
                }

            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                loadNetData();
            }
        });
    }

    private void loadNetData() {
        Map<String, String> map = new HashMap<>();
        map.put("size", String.valueOf(pageSize));
        map.put("index", String.valueOf(pageIndex));
        mPresenter.getCourse(map);

    }


    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }


    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        if (pageIndex == 1) {
            this.recommendCourses.clear();
            if (courses.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        refreshLayout.setLoadmoreFinished(courses.size() < pageSize);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);


    }


    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        loadNetData();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        loadNetData();

    }

    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
