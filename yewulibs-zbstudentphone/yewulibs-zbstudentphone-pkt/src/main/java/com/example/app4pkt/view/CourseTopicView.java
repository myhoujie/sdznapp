package com.example.app4pkt.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.CourseList;
import com.sdzn.variants.bean.SubjectBean;

import java.util.List;

/**
 * 专题课
 */
public interface CourseTopicView extends BaseView {
    void onTopicSuccess(List<SubjectBean> subjectList);

    void onTopicFailed(String msg);

}
