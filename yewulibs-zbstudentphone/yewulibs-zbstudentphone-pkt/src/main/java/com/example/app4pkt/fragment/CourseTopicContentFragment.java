package com.example.app4pkt.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.R;
import com.example.app4pkt.presenter.CourseTopicContentPresenter;
import com.example.app4pkt.view.SpellingContentView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.example.app4public.adapter.RecommendAdapter;
import com.example.app4public.adapter.WrapAdapter;
import com.sdzn.variants.bean.CourseList;
import com.example.app4public.widget.EmptyLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * A simple {@link } subclass.
 */
public class CourseTopicContentFragment extends BaseMVPFragment<SpellingContentView, CourseTopicContentPresenter> implements SpellingContentView, OnRefreshLoadmoreListener {
    public static final String SUBJECT_TYPE = "topic_page";
    EmptyLayout emptyLayout;
    RecyclerView rvCourseRec;
    SmartRefreshLayout refreshLayout;
    private int pageIndex = 1;//当前页
    private int pageSize = 10;//

    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private String topicType = "";// 专题课类型


    public CourseTopicContentFragment() {
        // Required empty public constructor
    }

    public static CourseTopicContentFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(SUBJECT_TYPE, id);
        CourseTopicContentFragment fragment = new CourseTopicContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_spelling_content;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        rvCourseRec = (RecyclerView) rootView.findViewById(R.id.swipe_target);
        emptyLayout = (EmptyLayout) rootView.findViewById(R.id.empty_layout);
        if (getArguments() != null) {
            topicType = getArguments().getString(SUBJECT_TYPE);
        }
        pageIndex = 1;
        recommendCourses = new ArrayList<>();
        initView();
        loadData();
    }


    @Override
    protected CourseTopicContentPresenter createPresenter() {
        return new CourseTopicContentPresenter();
    }

    private void initView() {
        refreshLayout.setOnRefreshLoadmoreListener(this);
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2));
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                    startIntent.putExtra("PACKAGE", recommendCourses.get(position).getSellType());
                    startIntent.putExtra("courseId", recommendCourses.get(position).getCourseId());
                    startIntent.putExtra("showLiveBtn", false);
                    startActivity(startIntent);
                      }

            }
        });


        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                loadData();
            }
        });
    }

    private void loadData() {
        Map<String, String> map = new HashMap<>();
        map.put("projectId", topicType);
        map.put("limit", String.valueOf(pageSize));
        map.put("page", String.valueOf(pageIndex));
        mPresenter.getCourse(map);

    }

    @Override
    public void getDataCourse(List<CourseList> courses) {
        clearLoingState();
        if (pageIndex == 1) {
            this.recommendCourses.clear();
            if (courses.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        refreshLayout.setLoadmoreFinished(courses.size() < pageSize);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);

    }

    @Override
    public void onFailed(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        loadData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        loadData();

    }

    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }
}
