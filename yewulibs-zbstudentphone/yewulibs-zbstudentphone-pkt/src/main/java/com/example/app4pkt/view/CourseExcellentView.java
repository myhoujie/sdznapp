package com.example.app4pkt.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.CourseList;

import java.util.List;

/**
 * 精品课列表
 */
public interface CourseExcellentView extends BaseView {

    void getDataFailure(String msg);


    void getDataCourse(List<CourseList> recommendCourses);

}
