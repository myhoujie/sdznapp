package com.example.app4pkt.presenter;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.variants.bean.BannerInfoBean;
import com.sdzn.variants.bean.HomeRecommedBean;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4pkt.view.HomeView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewSchoolFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public class HomePresenter extends BasePresenter<HomeView> {
    /**
     * 获取推荐课程
     */

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getHomeRecommendCourse(requestBody)
                .compose(TransformUtils.<ResultBean<HomeRecommedBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<HomeRecommedBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<HomeRecommedBean>() {
                    @Override
                    public void onNext(HomeRecommedBean courses) {
                        if (courses != null) {
                            //精品
                            getView().getDataCourse(courses.getBoutique().getRows());
                            //专题
                            getView().getSubjectDataCourse(courses.getSubject().getRows());
                        }else {
                            getView().getCourseEmpty();
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().getDataFailure("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    /**
     * 获取banner
     */

    public void getBanner() {
        Map<String, String> map = new HashMap<>();
        map.put("typeId", "2");
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getBanner(requestBody)
                .compose(TransformUtils.<ResultBean<List<BannerInfoBean>>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<List<BannerInfoBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<BannerInfoBean>>() {
                    @Override
                    public void onNext(List<BannerInfoBean> infoBeanList) {
                        if (infoBeanList != null && infoBeanList.size() > 0) {
                            getView().getBannerData(infoBeanList);
                        } else {
                            getView().getBannerEmpty();
                        }

                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().getDataFailure("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

}
