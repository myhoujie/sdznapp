package com.example.app4pkt.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.app4pkt.R;
import com.example.app4pkt.presenter.HomePresenter;
import com.example.app4pkt.view.HomeView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;

import com.example.app4public.adapter.RecommendAdapter;
import com.example.app4public.adapter.WrapAdapter;
import com.sdzn.variants.bean.BannerInfoBean;
import com.sdzn.variants.bean.CourseList;
import com.example.app4public.utils.PermissionUtils;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.RoundImageView;
import com.example.app4public.widget.bannerview.MZBannerView;
import com.example.app4public.widget.bannerview.holder.MZHolderCreator;
import com.example.app4public.widget.bannerview.holder.MZViewHolder;
import com.example.app4public.widget.bannerview.transformer.CustomTransformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;





/**
 * 描述：
 * - 首页
 * 创建人：baoshengxiang
 * 创建时间：
 */
public class HomeFragment extends BaseMVPFragment<HomeView, HomePresenter> implements HomeView, OnRefreshListener, OnRefreshLoadmoreListener, View.OnClickListener {

    SmartRefreshLayout refreshLayout;
    EmptyLayout emptyLayout;
    MZBannerView fvBanner;
    RecyclerView rvCourseRec;
    RecyclerView rvSubject;
    TextView tvCourse;
    TextView tvSubject;

    private List<BannerInfoBean> bannerInfos;
    private List<String> bannerUrls;
    private List<String> bannerlinkAddress;
    private List<String> bannerTypeid;
    private List<String> bannerselltype;
    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private List<CourseList> subjectCourses;
    private RecommendAdapter subjectAdapter;
    private WrapAdapter<RecommendAdapter> subjectwrapAdapter;
    private String typeid;


    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        fvBanner.start();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        fvBanner = (MZBannerView) rootView.findViewById(R.id.fv_banner);
        rvCourseRec = (RecyclerView) rootView.findViewById(R.id.rv_course);
        rvSubject = (RecyclerView) rootView.findViewById(R.id.rv_subject);
        emptyLayout = (EmptyLayout) rootView.findViewById(R.id.empty_layout);
        tvCourse = (TextView) rootView.findViewById(R.id.tv_course);
        tvSubject = (TextView) rootView.findViewById(R.id.tv_subject);
        tvSubject.setOnClickListener(this);
        tvCourse.setOnClickListener(this);
        initData();
        initView();
        loadNetData();
        mPresenter.getBanner();
    }

    private void initData() {
        bannerInfos = new ArrayList<>();
        bannerUrls = new ArrayList<>();
        bannerlinkAddress = new ArrayList<>();
        bannerTypeid = new ArrayList<>();
        bannerselltype = new ArrayList<>();
        recommendCourses = new ArrayList<>();
        subjectCourses = new ArrayList<>();
    }

    private void initView() {
        refreshLayout.setOnRefreshLoadmoreListener(this);
//        rvCourseRec.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                    startIntent.putExtra("PACKAGE", recommendCourses.get(position).getSellType());
                    startIntent.putExtra("courseId", recommendCourses.get(position).getCourseId());
                    startIntent.putExtra("showLiveBtn", false);
                    startActivity(startIntent);
                         }

            }
        });

        rvSubject.setFocusable(false);
        rvSubject.setLayoutManager(new GridLayoutManager(mContext, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        subjectAdapter = new RecommendAdapter(mContext, subjectCourses);
        subjectwrapAdapter = new WrapAdapter<>(subjectAdapter);
        subjectwrapAdapter.adjustSpanSize(rvSubject);
        rvSubject.setAdapter(subjectwrapAdapter);

        subjectAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < subjectCourses.size()) {
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                    startIntent.putExtra("PACKAGE",subjectCourses.get(position).getSellType() );
                    startIntent.putExtra("courseId", subjectCourses.get(position).getCourseId());
                    startIntent.putExtra("showLiveBtn", false);
                    startActivity(startIntent);
                       }

            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNetData();
            }
        });
    }


    private void loadNetData() {

        mPresenter.getCourse(new HashMap<String, String>());

    }

    private void initBanner() {
        bannerUrls.clear();
        fvBanner.pause();
        for (BannerInfoBean bannerInfoBean : bannerInfos) {
            bannerUrls.add("" + bannerInfoBean.getImageUrl());
            bannerlinkAddress.add("" + bannerInfoBean.getLinkAddress());
            bannerTypeid.add("" + bannerInfoBean.getRelationType());
            bannerselltype.add("" + bannerInfoBean.getSellType());
        }
        fvBanner.setBannerPageClickListener(bannerPageClickListener);
        // 设置数据
        fvBanner.setPages(bannerUrls, new MZHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });
        fvBanner.setPageTransformer(new CustomTransformer());
        fvBanner.start();

    }

    /**
     * banner跳转
     */
    MZBannerView.BannerPageClickListener bannerPageClickListener = new MZBannerView.BannerPageClickListener() {
        @Override
        public void onPageClick(View view, int position) {
//            HiosHelper.resolveAd(getActivity(), getActivity(), "hios://com.haier.cellarette.libwebview.base.WebViewMainActivity2?aid={s}"+bannerlinkAddress.get(position));
//            String validateurl = String.valueOf(bannerlinkAddress.get(position));
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebViewMainActivity2");
//            intent.putExtra("validateurl", validateurl);
//            startActivity(intent);
            if (bannerTypeid.get(position).equals("0")) {//跳转H5地址
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebBannerActivity");
                startIntent.putExtra("validateurl", bannerlinkAddress.get(position));
                startActivity(startIntent);
            } else {//跳转课程
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                startIntent.putExtra("PACKAGE", bannerselltype.get(position));
                startIntent.putExtra("courseId", Integer.valueOf(bannerlinkAddress.get(position)));
                startIntent.putExtra("showLiveBtn", true);
                startActivity(startIntent);

            }
        }
    };

    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void getCourseEmpty() {
        clearLoingState();
    }

    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        this.recommendCourses.clear();
        if (courses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);


    }

    @Override
    public void getSubjectDataCourse(List<CourseList> subjectCourses) {
        clearLoingState();
        this.subjectCourses.clear();
        if (subjectCourses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.subjectCourses.addAll(subjectCourses);
        subjectwrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void getBannerData(List<BannerInfoBean> infoBeanList) {
        this.bannerInfos.clear();
        this.bannerInfos.addAll(infoBeanList);
        initBanner();

    }

    @Override
    public void getBannerEmpty() {
        clearLoingState();
        fvBanner.setVisibility(View.GONE);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionUtils.checkPermissionResult(mContext, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        loadNetData();
        mPresenter.getBanner();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {

    }

    @Override
    public void onClick(View view) {
        if (R.id.tv_course == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseExcellentActivity");
            startActivity(startIntent);
        } else if (R.id.tv_subject == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseTopicActivity");
            startActivity(startIntent);
        }
    }

    private class BannerViewHolder implements MZViewHolder<String> {
        private RoundImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局
            mImageView = new RoundImageView(context);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);// X和Y方向都填满
            mImageView.setType(RoundImageView.TYPE_ROUND);
            mImageView.setBorderRadius(8);
            mImageView.setImageResource(R.mipmap.place);
            return mImageView;
        }

        @Override
        public void onBind(Context context, int position, String data) {
            // 数据绑定
//            GlideImgManager.loadImage(context, data, R.mipmap.place, R.mipmap.place, mImageView);
            Glide.with(mContext).load(data).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    mImageView.setImageBitmap(resource);
                }
            });
        }
    }

    private void clearLoingState() {
        if (refreshLayout != null) {
            if (refreshLayout.isRefreshing()) {
                refreshLayout.finishRefresh();
            }
            if (refreshLayout.isLoading()) {
                refreshLayout.finishLoadmore();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fvBanner.pause();
    }


}
