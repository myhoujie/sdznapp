package com.example.app4home.course.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.CourseKpointListBean;
import com.example.app4public.manager.constant.CourseCons;

import java.util.List;

/**
 * 描述：
 * - 单一课程章节adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class CourseCataloguAdapter extends BaseRcvAdapter<CourseKpointListBean> {
    private int courseType;

    public CourseCataloguAdapter(Context context, int courseType, List mList) {
        super(context, mList);
        this.courseType = courseType;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getKpointType();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        if (0 == viewType) {
            holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_chapter);
        } else if (1 == viewType) {
            holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_section);
        }
        return holder;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, CourseKpointListBean kpointBean) {

        if (0 == holder.getItemViewType()) {
            holder.setText(R.id.tv_chacpter, kpointBean.getName());
        } else if (1 == holder.getItemViewType()) {
            if (!TextUtils.isEmpty(kpointBean.getTeacherName())) {
                holder.setText(R.id.tv_teacher, "讲师：" + kpointBean.getTeacherName());
            }
            if (courseType == CourseCons.Type.LIVING) {
                holder.setText(R.id.tv_catalogue, kpointBean.getName());
                holder.setVisible(R.id.tv_date, true);
                String day = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "yyyy-MM-dd");
                String strartTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "HH:mm");
                String endDay = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "yyyy-MM-dd");
                String endTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "HH:mm");
                if (day.equals(endDay)) {
                    holder.setText(R.id.tv_date, day + " " + strartTime + "~" + endTime);
                } else {
                    holder.setText(R.id.tv_date, day + " " + strartTime + "~" + endDay + " " + endTime);
                }
                String status = CourseCons.LiveStatus.upStatus(kpointBean.getLiveStates());
                holder.setInVisible(R.id.img_liveing,
                        CourseCons.LiveStatus.isLiving(status) || CourseCons.LiveStatus.isRest(status));
            } else {
                holder.setText(R.id.tv_chapter, kpointBean.getName());
                holder.setVisible(R.id.tv_date, false);
            }

        }
    }
}
