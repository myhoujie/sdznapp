package com.example.app4home.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4pkt.fragment.HomeFragment;
import com.example.app4pkt.fragment.SpellingContentFragment;
import com.sdzn.variants.network.SPToken;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;
import com.example.app4public.adapter.SpellingTitleAdapter;
import com.sdzn.variants.bean.SubjectBean;
import com.example.app4public.event.UpdateAccountEvent;

import com.example.app4public.manager.SPManager;
import com.example.app4public.manager.constant.CourseCons;
import com.example.app4home.mvp.presenter.SpellingClassPresenter;
import com.example.app4home.mvp.view.SpellingClassView;
import com.example.app4public.utils.GradeIdToNameUtils;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.pager.PagerSlidingTabStrip;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.runtime.Permission;
import com.yzq.zxinglibrary.android.CaptureActivity;
import com.yzq.zxinglibrary.bean.ZxingConfig;
import com.yzq.zxinglibrary.common.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import com.yanzhenjie.permission.Action;


/**
 * 拼课堂
 */
public class SpellingClassFragment extends BaseMVPFragment<SpellingClassView, SpellingClassPresenter> implements SpellingClassView {
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    ViewPager viewPager;
    TextView tvBarGrade;
    ImageView icBarSearch;
    ImageView ivBarShop;
     ImageView saoyisao;
    EmptyLayout emptyLayout;

    private SpellingTitleAdapter spellingTitleAdapter;
    private List<Fragment> spellListFragment = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();
    //定义要装fragment的列表

    public SpellingClassFragment() {
        // Required empty public constructor
    }

    public static SpellingClassFragment newInstance() {
        return new SpellingClassFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_spelling_class;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        tvBarGrade = (TextView) rootView.findViewById(R.id.tv_bar_grade);
        ivBarShop = (ImageView) rootView.findViewById(R.id.iv_bar_right);
        saoyisao = (ImageView) rootView.findViewById(R.id.saoyisao);
        icBarSearch = (ImageView) rootView.findViewById(R.id.ic_bar_search);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        viewPager = (ViewPager) rootView.findViewById(R.id.vp_pager);
        emptyLayout = (EmptyLayout) rootView.findViewById(R.id.empty_layout);
        initView();
        loadData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvBarGrade.setText(GradeIdToNameUtils.setName(SPManager.getSectionId(), SPManager.getgradeId()));
    }

    private void loadData() {
        if (SPManager.getSectionId() < 0) {
            Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
            startIntent.putExtra("to_in_subject", false);
            startActivity(startIntent);
        } else {
            spellingTitleAdapter = new SpellingTitleAdapter(getChildFragmentManager());
            mPresenter.getSubject();
        }
    }
    private int REQUEST_CODE_SCAN = 111;


    private void initView() {
        saoyisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndPermission.with(getActivity())
                        .runtime()
                        .permission(Permission.CAMERA)
                        .onGranted(new Action<List<String>>() {
                            @Override
                            public void onAction(List<String> data) {
                                Intent intent = new Intent(getActivity(), CaptureActivity.class);
                                /*ZxingConfig是配置类
                                 *可以设置是否显示底部布局，闪光灯，相册，
                                 * 是否播放提示音  震动
                                 * 设置扫描框颜色等
                                 * 也可以不传这个参数
                                 * */
                                ZxingConfig config = new ZxingConfig();
                                // config.setPlayBeep(false);//是否播放扫描声音 默认为true
                                //  config.setShake(false);//是否震动  默认为true
                                // config.setDecodeBarCode(false);//是否扫描条形码 默认为true
//                                config.setReactColor(R.color.colorAccent);//设置扫描框四个角的颜色 默认为白色
//                                config.setFrameLineColor(R.color.colorAccent);//设置扫描框边框颜色 默认无色
//                                config.setScanLineColor(R.color.colorAccent);//设置扫描线的颜色 默认白色
                                config.setFullScreenScan(false);//是否全屏扫描  默认为true  设为false则只会在扫描框中扫描
                                intent.putExtra(Constant.INTENT_ZXING_CONFIG, config);
                                startActivityForResult(intent, REQUEST_CODE_SCAN);
                            }
                        })
                        .onDenied(new Action<List<String>>() {
                            @Override
                            public void onAction(List<String> data) {
                                Uri packageURI = Uri.parse("package:" + getActivity().getPackageName());
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Toast.makeText(getActivity(), "没有权限无法扫描呦", Toast.LENGTH_LONG).show();
                            }
                        })
                        .start();
            }
        });
        icBarSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SearchActivity");
                startIntent.putExtra("courseType", CourseCons.Type.ALL);
                startActivity(startIntent);
            }
        });
        tvBarGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
                startIntent.putExtra("to_in_subject", true);
                startActivity(startIntent);
            }
        });
        ivBarShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!SPToken.autoLogin(mContext)) {
                     Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                    return;
                }
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);
            }
        });
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }


    public static final String TITLE_NUMBER = "titlnumber";
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == REQUEST_CODE_SCAN && resultCode == getActivity().RESULT_OK) {
            if (data != null) {
                String content = data.getStringExtra(Constant.CODED_CONTENT);
//                ToastUtils.showLong(getActivity(),"扫描结果为：" + content);
                HiosHelper.resolveAd(getActivity(), getActivity(), "hios://com.haier.cellarette.libwebview.base.WebViewMainActivity2?aid={s}"+content);
                /*Intent intent=new Intent(getActivity(), SubmitAssignmentActivity.class);
                intent.putExtra(TITLE_NUMBER, "8,9");
                startActivity(intent);*/
            }
        }
    }

    @Override
    protected SpellingClassPresenter createPresenter() {
        return new SpellingClassPresenter();
    }


    @Override
    public void onSubjectSuccess(List<SubjectBean> subjectList) {
        if (subjectList.size() > 0) {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            this.listTitle.clear();
            this.spellListFragment.clear();
            spellListFragment.add(HomeFragment.newInstance());
            listTitle.add("精品课");

            for (int i = 0; i < subjectList.size(); i++) {
                spellListFragment.add(SpellingContentFragment.newInstance(String.valueOf(subjectList.get(i).getSubjectId()), "", ""));
                listTitle.add(subjectList.get(i).getSubjectName());
            }
            spellingTitleAdapter.setmDatas(listTitle, spellListFragment);
            viewPager.setAdapter(spellingTitleAdapter);
            viewPager.setCurrentItem(0);
            mPagerSlidingTabStrip.setViewPager(viewPager);
            viewPager.setOffscreenPageLimit(0);

            mPagerSlidingTabStrip.clearConfigSet();
        } else {
            Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
            startIntent.putExtra("to_in_subject", false);
            startActivity(startIntent);
        }

    }

    @Override
    public void onSubjectFailed(String msg) {
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            if (spellingTitleAdapter != null) {
                spellingTitleAdapter.clear(viewPager);
            }
            loadData();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
