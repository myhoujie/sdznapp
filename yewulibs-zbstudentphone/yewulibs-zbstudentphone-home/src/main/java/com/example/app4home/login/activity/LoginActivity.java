package com.example.app4home.login.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azhon.appupdate.manager.DownloadManager;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.example.app4home.login.presenter.CheckverionPresenter;
import com.example.app4home.login.view.CheckverionView;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4public.manager.Config;
import com.example.app4public.manager.SPManager;
import com.example.app4public.utils.CountDownTimerUtils;
import com.example.app4public.utils.VerifyUtil;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.variants.bean.VersionInfoBean;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.AndroidBug5497Workaround;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;
import com.example.app4home.login.presenter.LoginPresenter;
import com.example.app4home.login.view.LoginView;
import com.example.app4public.permission.MPermission;
import com.example.app4public.permission.annotation.OnMPermissionDenied;
import com.example.app4public.permission.annotation.OnMPermissionGranted;
import com.example.app4public.widget.CheckBoxSample;
import com.example.app4public.widget.ClearEditText;
import com.example.app4public.widget.PwdEditText;
import com.sdzn.variants.bean.UserBean;
import com.sdzn.variants.bean.UserLoginBean;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;

/**
 * 描述：
 * - 用户登录
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */

public class LoginActivity extends BaseMVPActivity1<LoginView, LoginPresenter> implements CheckverionView,LoginView, View.OnLayoutChangeListener, RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    public static final String INTENT_WEB = "userType";
    public static final String LOGIN_DETAIL = "LOGIN_DETAIL";
    View rootLayout;
    View bottomSpace;
    LinearLayout llBg;
    ImageView ivLogo;
    ImageView ivLogo2;
    LinearLayout llLoginContent;
    ClearEditText etAccount;
    PwdEditText etPassword;
    View regView;
    RadioGroup radioGroup;
    TextView tvCodeHint;
    TextView tvForgetPassword;
    RelativeLayout rlPassword;
    RelativeLayout rlCode;
    ClearEditText etCode;
    Button btnGetCode;
    ImageView ivBack;
    TextView tvUserAgreement;
    TextView tvPrivacyAgreement;
    TextView tvAccount;
    CheckBoxSample checkBoxSample;
    Button btnLogin;


    private CountDownTimerUtils countDownTimerUtils;
    //软件盘弹起后所占高度阀值
    private int keyHeight = 0;
    private boolean IS_PSSWOORD_LOGIN = true;
    private boolean IS_DETSIL;

    private boolean isCheckBox;
    private String IMEI;//设备唯一号
    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入LoginActivity成功" );
                }
            }
        }
        rootLayout = (View) findViewById(R.id.root_layout);
        llBg = (LinearLayout) findViewById(R.id.ll_bg);
        ivLogo = (ImageView) findViewById(R.id.iv_logo);
        llLoginContent = (LinearLayout) findViewById(R.id.ll_longin_content);
        ivLogo2 = (ImageView) findViewById(R.id.iv_logo2);
        radioGroup = (RadioGroup) findViewById(R.id.rg_random);
        tvAccount = (TextView) findViewById(R.id.tv_account);
        etAccount = (ClearEditText) findViewById(R.id.et_account);
        rlPassword = (RelativeLayout) findViewById(R.id.rl_password);
        etPassword = (PwdEditText) findViewById(R.id.et_password);
        rlCode = (RelativeLayout) findViewById(R.id.rl_code);
        etCode = (ClearEditText) findViewById(R.id.et_code);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        tvForgetPassword = (TextView) findViewById(R.id.tv_forget_password);
        tvCodeHint = (TextView) findViewById(R.id.tv_code_hint);
        checkBoxSample = (CheckBoxSample) findViewById(R.id.checkbox);
        tvUserAgreement = (TextView) findViewById(R.id.tv_user_agreement);
        tvPrivacyAgreement = (TextView) findViewById(R.id.tv_privacy_agreement);
        regView = (View) findViewById(R.id.ll_register);
        bottomSpace = (View) findViewById(R.id.bottom_space);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        btnLogin = (Button) findViewById(R.id.btn_login);

        tvForgetPassword.setOnClickListener(this);
        regView.setOnClickListener(this);
        btnGetCode.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        tvUserAgreement.setOnClickListener(this);
        tvPrivacyAgreement.setOnClickListener(this);
        checkBoxSample.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("3","0");
//        mPresenter.checkVerion();
        IMEI = DeviceUtils.getAndroidID();
        initData();
        initView();

//        requestBasicPermission();
    }


    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,1,ivLogo);
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppManager.getAppManager().finishActivity(ReSetPasswordActivity.class);
        AppManager.getAppManager().finishActivity(RetrievePasswordActivity.class);
        AppManager.getAppManager().finishActivity(RegisterActivity.class);
    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
        rootLayout.addOnLayoutChangeListener(this);
        etPassword.setShowAnimate(false);
        regView.setVisibility(Config.OPEN_REGISTRATION ? View.VISIBLE : View.GONE);
        radioGroup.setOnCheckedChangeListener(this);

        tvUserAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvPrivacyAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        if (!checkBoxSample.isChecked()) {
            checkBoxSample.toggle();
            isCheckBox = true;
        }

    }

    private void initData() {
        int screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
//        keyHeight = screenHeight / 3;
        //系统自带输入法, 密码输入框比文字输入框矮一点, 正好卡在差一点到1/3的位置(这个差的高度不同尺寸屏幕不一样)
        //华为部分机型开启全屏模式时, 显示虚拟键会将窗口顶起一点...祖传大黑边应该也不能超过1/4吧..
        keyHeight = screenHeight / 4;
        String lastLoginAccount = SPManager.getLastLoginAccount();
        if (!TextUtils.isEmpty(lastLoginAccount)) {
//            etAccount.setText(lastLoginAccount);
        }
        SPManager.changeLogin(mContext, false);
        IS_DETSIL = getIntent().getBooleanExtra(LOGIN_DETAIL, false);
        requestBasicPermission();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right,
                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        //old是改变前的左上右下坐标点值，没有old的是改变后的左上右下坐标点值
        //现在认为只要控件将Activity向上推的高度超过了1/3屏幕高，就认为软键盘弹起
//        LogUtils.i("top = " + top + "\nbottom = " + bottom + "\noldTop = " + oldTop + "\noldBottom = " + oldBottom);
        if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
            bottomSpace.setVisibility(View.GONE);
            llBg.setVisibility(View.GONE);
            ivLogo.setVisibility(View.INVISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_trans);
        } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
            //监听到软件盘关闭
            bottomSpace.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.VISIBLE);
            llBg.setVisibility(View.VISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo_trans);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_bg);
        }
//        bottomSpace.requestLayout();
    }



    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        KeyboardUtils.hideSoftInput(this);
        etAccount.setText("");
        if (R.id.rb_password == i) {
            IS_PSSWOORD_LOGIN = true;
            tvForgetPassword.setVisibility(View.VISIBLE);
            tvCodeHint.setVisibility(View.INVISIBLE);
            rlPassword.setVisibility(View.VISIBLE);
            rlCode.setVisibility(View.GONE);
            etAccount.setHint(getString(R.string.login_acount_hint));
            tvAccount.setText("账号");
            etPassword.setText("");
            etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});

        } else if (R.id.rb_code == i) {
            IS_PSSWOORD_LOGIN = false;
            tvForgetPassword.setVisibility(View.INVISIBLE);
            tvCodeHint.setVisibility(View.VISIBLE);
            rlPassword.setVisibility(View.GONE);
            rlCode.setVisibility(View.VISIBLE);
            etAccount.setHint(getString(R.string.login_acount_hint1));
            tvAccount.setText("手机号");
            etCode.setText("");
            etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
        } else {

        }
    }


    public void doLogin() {
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号或账号");
        } else if (TextUtils.isEmpty(password)) {
            ToastUtils.showShort("请输入密码");
        } else {
            mPresenter.login(account, password, IMEI);
        }


    }

    /**
     * 验证码登录
     */
    public void doLoginCode() {
        String account = etAccount.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(account)) {
            ToastUtils.showShort("手机号格式错误");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            mPresenter.loginCode(account, code, IMEI);
        }

    }

    private void getVerifyCode() {
        String phoneNo = etAccount.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            countDownTimerUtils.start();
            mPresenter.getVerifyCode(phoneNo);
        }
    }

    @Override
    public void loginSuccess(UserLoginBean loginBean) {
        UserBean userBean = loginBean.getUserDetail();
        SPToken.saveToken(loginBean.getAccess_token());
        SPManager.saveUser(loginBean.getUserDetail());
        SPManager.saveLastLoginAccount(etAccount.getText().toString().trim());
        SPManager.savePwd(etPassword.getText().toString().trim());

        SPManager.changeLogin(mContext, true);
        if (0 != userBean.getSubjectId() || 0 != userBean.getGrade()) {
            SPManager.saveSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());

            SPManager.saveSchoolSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());

            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
            if (IS_DETSIL) {
                this.finish();
            } else {
                Intent startIntent = new Intent(App2Utils.getAppPackageName(mContext) + ".hs.act.MainActivity");
                startIntent.putExtra("autoLogin",false);
                startActivity(startIntent);
            }
        } else {
            //          IntentController.toAccountSetting(mContext);
            //添加两参数  1 从哪跳的  2（带学段？） 3.是否从详情过来的
            if (IS_DETSIL) {
                Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
                startIntent.putExtra("to_in_subject", false);
                startIntent.putExtra("isUpdate", true);
                startIntent.putExtra("isDetails", true);
                startActivity(startIntent);
                this.finish();
            } else {
                Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
                startIntent.putExtra("to_in_subject", false);
                startIntent.putExtra("isUpdate", true);
                startIntent.putExtra("isDetails", false);
                startActivity(startIntent);
                this.finish();
            }
        }


    }


    @Override
    public void loginFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    private ProgressDialog pd;
    private DownloadManager manager;
    private String updateTitle = "发现新版本";

/*
    @Override
    public void updateVersion(String updateInfo, String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudentphone");//apk下载位置
        forceconfig.setApkSaveName("拼课堂");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }
*/

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        ToastUtils.showShort(msg);
    }


    public static final int BAIJIAYUN_CODE = 10011;

    private void requestBasicPermission() {
        MPermission.with(LoginActivity.this)
                .addRequestCode(BAIJIAYUN_CODE)
                .permissions(
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(BAIJIAYUN_CODE)
    public void onBasicPermissionSuccess() {
    }

    @OnMPermissionDenied(BAIJIAYUN_CODE)
    public void onBasicPermissionFailed() {
    }

    @Override
    public void onBackPressed() {
        if (IS_DETSIL) {
            this.finish();
        } else {
            Intent startIntent = new Intent(App2Utils.getAppPackageName(mContext) + ".hs.act.MainActivity");
            startIntent.putExtra("autoLogin",false);
            startActivity(startIntent);
        }
//            AppUtils.exitApp(mContext);

    }

    @Override
    public void onClick(View view) {
        if (R.id.tv_forget_password == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.RetrievePasswordActivity");
            startActivity(startIntent);
        } else if (R.id.btn_login == view.getId()) {
            if (IS_PSSWOORD_LOGIN) {
                doLogin();
            } else {
                doLoginCode();
            }
        } else if (R.id.ll_register == view.getId()) {
            if (!Config.OPEN_REGISTRATION) {
                return;
            }

        } else if (R.id.btn_get_code == view.getId()) {
            getVerifyCode();
        } else if (R.id.iv_back == view.getId()) {
            finish();
        } else if (R.id.tv_user_agreement == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"1");
            startActivity(startIntent);
        } else if (R.id.tv_privacy_agreement == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"2");
            startActivity(startIntent);
        } else if (R.id.checkbox == view.getId()) {
            checkBoxSample.toggle();
            if (checkBoxSample.isChecked()) {
                isCheckBox = true;
            } else {
                isCheckBox = false;
            }
        } else {

        }
    }
    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = App2Utils.getAppVersionCode(App2.get());//获取版本号
            int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
            if (updateVersion > currVersion) {
                if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                    Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
                }
            }
        }
    }

    private void Updatemethod(String description, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudentphone");//apk下载位置
        forceconfig.setApkSaveName("拼课堂phone");//app名称
        String count = description.replace("|", "\n");


        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
//        ToastUtils.showLong(this, bean);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
//        ToastUtils.showLong(this, msg);
    }

    @Override
    protected void onDestroy() {
        checkverionPresenter.onDestory();
        super.onDestroy();
    }
}
