package com.example.app4home.login.presenter;

import android.text.TextUtils;

import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.UserLoginBean;
import com.example.app4home.login.view.RetrievePwdView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

/**
 *
 */
public class RetrievePwdPresenter extends BasePresenter<RetrievePwdView> {

    public void getVerifyCode(String phoneNo) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void confirmVerifyCode(String phoneNo, String virifyCode) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .confirmVerifyCode(phoneNo,virifyCode)
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean loginBean) {
                        if (loginBean!=null&&!loginBean.getAccess_token().isEmpty()) {
                            String token = loginBean.getAccess_token();
                            SPToken.saveToken(token);
                                getView().confirmCodeSuccess();
                        }else {
                            onFail(new ApiException(new Throwable(),10010));
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().confirmCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }
}
