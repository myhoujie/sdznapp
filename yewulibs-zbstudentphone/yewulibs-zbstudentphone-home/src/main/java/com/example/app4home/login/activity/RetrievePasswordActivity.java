package com.example.app4home.login.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ToastUtils;
import com.example.app4home.R;

import com.example.app4home.login.presenter.RetrievePwdPresenter;
import com.example.app4home.login.view.RetrievePwdView;
import com.example.app4public.utils.CountDownTimerUtils;
import com.example.app4public.utils.VerifyUtil;




import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * 描述：
 * - 找回密码
 * 创建人：
 * 创建时间：2017/7/8
 */
public class RetrievePasswordActivity extends BaseMVPActivity1<RetrievePwdView, RetrievePwdPresenter> implements RetrievePwdView, View.OnClickListener {

    EditText etPhone;
    EditText etCode;
    Button btnGetCode;
    Button btnCertain;
    TextView tvFind;
    ImageView ivBack;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_retrieve_password;
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,0,findViewById(R.id.iv_logo));
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    protected RetrievePwdPresenter createPresenter() {
        return new RetrievePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入RetrievePasswordActivity成功" );
                }
            }
        }
        etPhone = (EditText) findViewById(R.id.et_phone);
        etCode = (EditText) findViewById(R.id.et_code);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnCertain = (Button) findViewById(R.id.btn_certain);
        tvFind = (TextView) findViewById(R.id.tv_find);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        btnGetCode.setOnClickListener(this);
        btnCertain.setOnClickListener(this);
        tvFind.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }


    private void confirmVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            mPresenter.confirmVerifyCode(phoneNo, code);
        }
    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            mPresenter.getVerifyCode(phoneNo);
            countDownTimerUtils.start();
        }
    }

    @Override
    public void onPause() {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        super.onPause();
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        ToastUtils.showShort(msg);
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
    }

    @Override
    public void confirmCodeSuccess() {
        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ReSetPasswordActivity");
        startActivity(startIntent);
    }

    @Override
    public void confirmCodeFailure(String msg) {
        ToastUtils.showShort(msg);
    }


    @Override
    public void onClick(View view) {
        if (R.id.btn_get_code == view.getId()) {
            getVerifyCode();
        } else if (R.id.btn_certain == view.getId()) {
            confirmVerifyCode();
        } else if (R.id.iv_back == view.getId()) {
            onBackPressed();
        } else if (R.id.tv_find == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.RegisterActivity");
            startActivity(startIntent);
        } else {

        }
    }
}
