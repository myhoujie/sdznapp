package com.example.app4home.login.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4public.manager.SPManager;
import com.sdzn.core.base.BaseActivity1;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.widget.SweetAlertDialog;
import com.example.app4home.R;
import com.example.app4home.login.adapter.PageAdapterWithView;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * - 欢迎页
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WelcomeActivity extends BaseActivity1 implements ViewPager.OnPageChangeListener, View.OnClickListener {

    public static final String INTENT_WEB = "userType";
    ViewPager guideVp;
    LinearLayout ll;
    TextView tvUserAgreement;
    TextView tvPrivacyAgreement;
    TextView tvAgreementNo;
    TextView tvAgreementYes;

    private final int[] IMGS = {R.mipmap.welcome1, R.mipmap.welcome2};
    private List<View> views;
    private PageAdapterWithView pageAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入WelcomeActivity成功" );
                }
            }
        }
        guideVp = (ViewPager) findViewById(R.id.guide_vp);
        ll = (LinearLayout) findViewById(R.id.ll);

        tvUserAgreement = (TextView) findViewById(R.id.tv_user_agreement);
        tvPrivacyAgreement = (TextView) findViewById(R.id.tv_privacy_agreement);
        tvAgreementNo = (TextView) findViewById(R.id.tv_agreement_no);
        tvAgreementYes = (TextView) findViewById(R.id.tv_agreement_yes);

        tvUserAgreement.setOnClickListener(this);
        tvPrivacyAgreement.setOnClickListener(this);
        tvAgreementNo.setOnClickListener(this);
        tvAgreementYes.setOnClickListener(this);

        intiData();
        initView();
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.setColor(this, Color.WHITE, 0);
        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


//        DisplayMetrics dm = new DisplayMetrics();
//        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//        manager.getDefaultDisplay().getRealMetrics(dm);
//        int widthPixels = dm.widthPixels;//单位为像素 px
//        int height = dm.heightPixels;//单位为像素 px
//        float density = dm.density;
//        float scaledDensity = dm.scaledDensity;
//        int densityDpi = dm.densityDpi;
//        Log.e("gongshi", String.valueOf(densityDpi) + "----" + widthPixels + "--" + height);
//        Log.e("gongshi", (Math.sqrt(Math.pow(1280, 2) + (Math.pow(800, 2))) / 272 + ""));
    }

    private void initView() {
        pageAdapter = new PageAdapterWithView(views);
        guideVp.setAdapter(pageAdapter);
        guideVp.addOnPageChangeListener(this);
        guideVp.setVisibility(View.GONE);
    }

    private void intiData() {
        views = new ArrayList<>();
        initImgs();
    }


    private void initImgs() {
        ImageView imageView = null;
        for (int i = 0; i < IMGS.length; i++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams imageviewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageviewParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageResource(IMGS[i]);
            views.add(imageView);
        }
    }

    private void showDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("若不同意，则无法继续使用")
                .setPositiveButton("我再考虑下", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {

                    }
                })
                .setNegativeButton("不同意并退出", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        SPManager.setFirstEnterApp(mContext);
                        AppManager.getAppManager().appExit();
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == IMGS.length - 1) {
            views.get(position).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterApp();
                }
            });
        }
    }

    private void enterApp() {
        if (SPManager.isFirstSubjectId(mContext) && String.valueOf(SPManager.getgradeId()).isEmpty()) {
            Intent startIntent = new Intent(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".hs.act.SelectSubjectActivity");
            startIntent.putExtra("to_in_subject", false);
            startActivity(startIntent);
        } else {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MainActivity");
            startIntent.putExtra("autoLogin",false);
            startActivity(startIntent);
        }
    }


    @Override
    public void onClick(View view) {
        if (R.id.tv_user_agreement == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"1");
            startActivity(startIntent);

        } else if (R.id.tv_privacy_agreement == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebActivity");
            startIntent.putExtra(INTENT_WEB,"2");
            startActivity(startIntent);
        } else if (R.id.tv_agreement_no == view.getId()) {
            showDialog();
        } else if (R.id.tv_agreement_yes == view.getId()) {
            ll.setVisibility(View.GONE);
            guideVp.setVisibility(View.VISIBLE);

        }
    }
}
