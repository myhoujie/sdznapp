package com.example.app4home.login.presenter;

import android.text.TextUtils;

import com.example.app4home.R;
import com.example.app4home.login.view.ReSetPwdView;
import com.example.app4public.utils.PasswordUtils;
import com.sdzn.variants.network.RestApi;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class ReSetPwdPresenter extends BasePresenter<ReSetPwdView> {

    public void confirm(String newPwd, String affirmPwd) {
        if (verifyPwd(newPwd, affirmPwd)) {
            modifyPwd(newPwd, affirmPwd);
        }
    }


    public void confirmPwd(String oldPwd, String newPwd, String affirmPwd) {
        if (verifyPwd(oldPwd, newPwd, affirmPwd)) {
            modifyPwd(oldPwd, newPwd, affirmPwd);
        }
    }

    /**
     * @param newPwd
     * @param affirmPwd
     * @return
     */
    private boolean verifyPwd(String newPwd, String affirmPwd) {
        if (TextUtils.isEmpty(newPwd)) {
            ToastUtils.showShort("请输入密码");
            return false;
        } else if (TextUtils.isEmpty(affirmPwd)) {
            ToastUtils.showShort("请输入确认密码");
            return false;
        } else if (!newPwd.equals(affirmPwd)) {
            ToastUtils.showShort("新密码和确认密码不一致");
            return false;
        }else if (PasswordUtils.isNo(newPwd)) {
            ToastUtils.showShort("请输入6-16位数字和字母组合");
            return false;
        }
        return true;
    }

    private void modifyPwd(String newPwd, String affirmPwd) {
        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .changePwd(newPwd)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().changeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().changeFailure(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);
    }

    /**
     * @param oldPwd
     * @param newPwd
     * @param affirmPwd
     * @return
     */
    private boolean verifyPwd(String oldPwd, String newPwd, String affirmPwd) {
        if (TextUtils.isEmpty(oldPwd)) {
            ToastUtils.showShort("请输入原密码！");
            return false;
        } else  if (TextUtils.isEmpty(newPwd)) {
            ToastUtils.showShort("请输入密码");
            return false;
        } else if (TextUtils.isEmpty(affirmPwd)) {
            ToastUtils.showShort("请输入确认密码");
            return false;
        }  else if (PasswordUtils.isNo(newPwd)) {
            ToastUtils.showShort("请输入6-16位数字和字母组合");
            return false;
        } else if (!newPwd.equals(affirmPwd)) {
            ToastUtils.showShort("新密码和确认密码不一致");
            return false;
        }
        return true;
    }

    private void modifyPwd(String oldPwd, String newPwd, String affirmPwd) {
        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .changeOldPwd(oldPwd, newPwd)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<Object>())
                .subscribe(new MProgressSubscriber<Object>(new SubscriberOnNextListener<Object>() {

                    @Override
                    public void onNext(Object o) {
                        getView().changeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().changeFailure(msg);
                    }
                }, mActivity, true, "请求中..."));
        addSubscribe(subscription);
    }


    private boolean pwdMatcher(String pwd) {
        String match = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";
        Pattern pattern = Pattern.compile(match);
        Matcher matcher = pattern.matcher(pwd);
        return matcher.matches();
    }
}
