package com.example.app4home.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.variants.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.example.app4home.R;
import com.example.app4home.adapter.CAdapter;
import com.example.app4public.adapter.WrapAdapter;
import com.sdzn.variants.bean.CourseList;
import com.sdzn.variants.bean.GradeBean;
import com.sdzn.variants.bean.GradeJson;
import com.sdzn.variants.bean.LiveStatusBean;
import com.sdzn.variants.bean.SectionBean;

import com.sdzn.variants.bean.SortBean;
import com.sdzn.variants.bean.SubjectBean;
import com.example.app4public.event.UpdateAccountEvent;

import com.example.app4public.manager.SPManager;
import com.example.app4public.manager.constant.CourseCons;
import com.example.app4home.mvp.presenter.CoursePresenter;
import com.example.app4home.mvp.view.CourseView;
import com.example.app4public.pop.ConditionFilterPop;
import com.example.app4public.utils.GradeIdToNameUtils;
import com.example.app4public.widget.DrawableCenterRadioButton;
import com.example.app4public.widget.DrawableCenterTextView;
import com.example.app4public.widget.EmptySchoolLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * 学校课程的列表
 */
public class CourseFragment extends BaseMVPFragment<CourseView, CoursePresenter> implements CourseView, OnRefreshLoadmoreListener, View.OnClickListener {

    EmptySchoolLayout emptyLayout;
    SmartRefreshLayout refreshLayout;
    RecyclerView rcvCourseCatalogue;
    View cutline;
    LinearLayout rgCondition;
    DrawableCenterTextView tvCourseStatus;
    DrawableCenterTextView tvCourseSection;
    DrawableCenterTextView tvCourseGrade;
    DrawableCenterTextView tvCourseSugject;
    DrawableCenterRadioButton rbCourseSort;


    private static final int STATUS_OUR = 10001;
    private static final int STATUS_OTHER = 10002;

    private CAdapter courseAdapter;
    private WrapAdapter<CAdapter> wrapAdapter;
    private List<CourseList> courseSchoolBeans = new ArrayList<>();
    private int pageIndex = 1;
    private int pageSize = 10;

    private static final String COURSE_TYPE_PARAMS = "type";
    private static final String SEARCH_KEYWORDS_PARAMS = "searchStr";
    private int courseType;
    private String searchStr;

    private ConditionFilterPop sectionPop;
    private List<SectionBean> sections;
    private int sectionId = -1;
    private int eduId = -1;
    private ConditionFilterPop subjectPop = null;
    private List<SubjectBean> subjects;
    private int subjectId = -1;
    private ConditionFilterPop statusPop;
    private List<LiveStatusBean> status;
    private int statusId = 10001;
    private ConditionFilterPop sortPop;
    private List<SortBean> sorts;
    private int sortId = -1;

    private ConditionFilterPop gradePop = null;
    private List<GradeBean> grades;
    private int gradeId = -1;

    private Drawable triangleUpDw;
    private Drawable triangleDownDw;
    private String schoolId = "";

    private List<GradeJson> gradeJsonList;

    public static CourseFragment newInstance(@CourseCons.Type.CourseType int type, String searchStr) {
        CourseFragment courseFragment = new CourseFragment();
        Bundle args = new Bundle();
        args.putInt(COURSE_TYPE_PARAMS, type);
        args.putString(SEARCH_KEYWORDS_PARAMS, searchStr);
        courseFragment.setArguments(args);
        return courseFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            courseType = getArguments().getInt(COURSE_TYPE_PARAMS);
            searchStr = getArguments().getString(SEARCH_KEYWORDS_PARAMS);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course;
    }

    @Override
    protected CoursePresenter createPresenter() {
        return new CoursePresenter();
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        rgCondition = (LinearLayout) rootView.findViewById(R.id.rg_condition);
        tvCourseStatus = (DrawableCenterTextView) rootView.findViewById(R.id.tv_course_status);
        tvCourseSection = (DrawableCenterTextView) rootView.findViewById(R.id.tv_course_section);
        tvCourseGrade = (DrawableCenterTextView) rootView.findViewById(R.id.tv_course_grade);
        tvCourseSugject = (DrawableCenterTextView) rootView.findViewById(R.id.tv_course_sugject);
        rbCourseSort = (DrawableCenterRadioButton) rootView.findViewById(R.id.rb_course_sort);
        cutline = (View) rootView.findViewById(R.id.cutline);
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        rcvCourseCatalogue = (RecyclerView) rootView.findViewById(R.id.swipe_target_school);
        emptyLayout = (EmptySchoolLayout) rootView.findViewById(R.id.empty_layout);

        tvCourseGrade.setOnClickListener(this);
        tvCourseSugject.setOnClickListener(this);
        tvCourseStatus.setOnClickListener(this);
        rbCourseSort.setOnClickListener(this);
        tvCourseSection.setOnClickListener(this);


        pageIndex = 1;
        initView();
        initData();
    }
    private void initData() {
        gradeJsonList = new ArrayList<>();
        grades = new ArrayList<>();
        sections = new ArrayList<>();
        subjects = new ArrayList<>();
        status = new ArrayList<>();
        sorts = new ArrayList<>();
        this.gradeJsonList.clear();
        this.gradeJsonList.addAll(GradeIdToNameUtils.getGrade());
        setInitData();

        setSectionAndGrade(gradeJsonList);

        status.add(new LiveStatusBean(10001, "本校课程"));
        status.add(new LiveStatusBean(10002, "其他课程"));

        sorts.add(new SortBean(-1, "综合排序"));
        sorts.add(new SortBean(20002, "价格从低到高"));
        sorts.add(new SortBean(20001, "价格从高到低"));
        getCourseData();

    }

    private void setInitData() {
        tvCourseStatus.setText("本校课程");
        statusId = 10001;
        if (SPToken.autoLogin(getContext()) && !TextUtils.isEmpty(String.valueOf(SPManager.getSchoolgradeId()))) {

            tvCourseGrade.setText(GradeIdToNameUtils.setName(SPManager.getSchoolSectionId(), SPManager.getSchoolgradeId(), ""));
            gradeId = SPManager.getSchoolgradeId();
            sectionId = SPManager.getSchoolSectionId();
            eduId = SPManager.getSchoolEduId();
            setSectionAndGrade(gradeJsonList);//添加数据
//            SPManager.saveSchoolSection(SPManager.getSchoolSectionId(), SPManager.getUser().getSubjectName(), SPManager.getSchoolEduId());

            mPresenter.getSubject(sectionId);
        }

    }

    private void setInitOnResume() {
        if (!SPToken.autoLogin(mContext)) {
            clearLoingState();
            emptyLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);

        } else if (SPManager.isToCLogin()) {
            clearLoingState();
            emptyLayout.setErrorType(EmptySchoolLayout.NODATA_ENABLE_CLICK);
            emptyLayout.setErrorMessage(getString(R.string.error_view_load_toc));
        }

    }

    private void initView() {

        triangleUpDw = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_triangle_up, null);
        triangleDownDw = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_triangle_down, null);

        refreshLayout.setOnRefreshLoadmoreListener(this);

        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2);
        rcvCourseCatalogue.setLayoutManager(layoutManager);
        courseAdapter = new CAdapter(mContext, courseSchoolBeans);

        wrapAdapter = new WrapAdapter<>(courseAdapter);
        wrapAdapter.adjustSpanSize(rcvCourseCatalogue);
        rcvCourseCatalogue.setAdapter(wrapAdapter);

        courseAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CourseList courseListBean = courseSchoolBeans.get(position);
                 Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                startIntent.putExtra("PACKAGE", courseListBean.getSellType());
                startIntent.putExtra("courseId", courseListBean.getCourseId());
                startIntent.putExtra("showLiveBtn", false);
                startActivity(startIntent);
            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                getCourseData();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setInitOnResume();
    }

    private void getCourseData() {
//        ToastUtils.showShort(mContext,statusId+"\n"+sectionId+"****"+gradeId+"\n"+subjectId);
        if (SPToken.autoLogin(mContext) && !SPManager.isToCLogin()) {
            mPresenter.getCourse(getCourseParams());
        }

    }

    private Map<String, String> getCourseParams() {
        Map<String, String> params = new HashMap<>();
        params.put("schoolType", String.valueOf(statusId));
        if (sectionId != -1) {//学段
            params.put("classId", String.valueOf(sectionId));
            params.put("gradeType", String.valueOf(eduId));
        }
        if (getString(R.string.school_all_grade).equalsIgnoreCase(tvCourseGrade.getText().toString())) {
            params.put("grade", "");
        } else {
            params.put("grade", tvCourseGrade.getText().toString().trim());
        }
        if (subjectId != -1) {//学科
            params.put("subjectId", String.valueOf(subjectId));
        }
        params.put("index", String.valueOf(pageIndex));
        params.put("size", String.valueOf(pageSize));
        return params;
    }


    /**
     * 学段下拉列表
     *
     * @param textView
     */
    private void showSectionPop(final TextView textView) {
        setSectionAndGrade(gradeJsonList);//添加数据
        if (sectionPop == null) {
            sectionPop = new ConditionFilterPop(mContext, sections);
//            sectionPop.showAsDropDown(textView);
        } else {
            sectionPop.setData(sections);
        }

        for (int i = 0; i < sections.size(); i++) {
            if (sectionId == sections.get(i).getSectionId() && String.valueOf(eduId).equals(sections.get(i).getEducationId())) {
                sectionPop.setSelectPos(i);
                break;
            }
        }

        sectionPop.setConditionCallback(new ConditionFilterPop.ConditionCallback() {
            @Override
            public void selectCondition(int position) {
                SectionBean sectionBean = sections.get(position);
//                SPManager.saveSection(sectionBean);
                textView.setText(sectionBean.getSectionNameShow());

                sectionId = sectionBean.getSectionId();
                eduId = Integer.valueOf(sectionBean.getEducationId());
                gradeId = -1;
                tvCourseGrade.setText(getString(R.string.school_all_grade));
                subjectId = -1;
                tvCourseSugject.setText(getString(R.string.school_all_subject));
                if (sectionId != -1) {//根据学段去请求科目
                    mPresenter.getSubject(sectionId);
                }

                pageIndex = 1;
                getCourseData();
            }
        });
        showFilterPop(sectionPop, textView);
    }

    /**
     * 年级下拉列表
     *
     * @param textView
     */
    private void showGradePop(final TextView textView) {
        setSectionAndGrade(gradeJsonList);//添加数据
        if (gradePop == null) {
            gradePop = new ConditionFilterPop(mContext, grades);
        } else {
            gradePop.setData(grades);
        }

        for (int i = 0; i < grades.size(); i++) {
            if (gradeId == grades.get(i).getGradeId()) {
                gradePop.setSelectPos(i);
                break;
            }
        }

        gradePop.setConditionCallback(new ConditionFilterPop.ConditionCallback() {
            @Override
            public void selectCondition(int position) {
                GradeBean sectionBean = grades.get(position);
                textView.setText(sectionBean.getGradeName());
                gradeId = sectionBean.getGradeId();
                pageIndex = 1;
                getCourseData();
            }
        });
        showFilterPop(gradePop, textView);
    }

    /**
     * 学科下拉列表
     *
     * @param textView
     */
    private void showSubjectPop(final TextView textView) {
        if (subjectPop == null) {
            subjectPop = new ConditionFilterPop(mContext, subjects);
        } else {
            subjectPop.setData(subjects);
        }
        for (int i = 0; i < subjects.size(); i++) {
            if (subjectId == subjects.get(i).getSubjectId()) {
                subjectPop.setSelectPos(i);
                break;
            }
        }
        subjectPop.setConditionCallback(new ConditionFilterPop.ConditionCallback() {
            @Override
            public void selectCondition(int position) {
                SubjectBean subjectBean = subjects.get(position);
                textView.setText(subjectBean.getSubjectName());
                subjectId = subjectBean.getSubjectId();
                pageIndex = 1;
                getCourseData();

            }
        });
        showFilterPop(subjectPop, textView);
    }

    /**
     * 状态 下拉列表  本校,外销
     *
     * @param textView
     */
    private void showStatusPop(final TextView textView) {
        if (statusPop == null) {
            statusPop = new ConditionFilterPop(mContext, status);

        }
        for (int i = 0; i < status.size(); i++) {
            if (statusId == status.get(i).getStatusId()) {
                statusPop.setSelectPos(i);
                break;
            }
        }

        statusPop.setConditionCallback(new ConditionFilterPop.ConditionCallback() {
            @Override
            public void selectCondition(int position) {
                LiveStatusBean statusBean = status.get(position);
                textView.setText(statusBean.getStatusName());
                statusId = statusBean.getStatusId();

                subjectId = -1;
                tvCourseSugject.setText(getString(R.string.school_all_subject));
                if (STATUS_OUR == statusId) {//本校时
                    gradeId = SPManager.getSchoolgradeId();
                    tvCourseGrade.setText(GradeIdToNameUtils.setName(SPManager.getSchoolSectionId(), SPManager.getSchoolgradeId(), ""));
                    sectionId = SPManager.getSchoolSectionId();
                    eduId = SPManager.getSchoolEduId();
                    setSectionAndGrade(gradeJsonList);//添加数据
                    mPresenter.getSubject(sectionId);
                } else {
                    sectionId = -1;
                    eduId = -1;
                    tvCourseSection.setText(getString(R.string.school_all_section));
                    gradeId = -1;
                    tvCourseGrade.setText(getString(R.string.school_all_grade));
                }

                pageIndex = 1;
                getCourseData();
            }
        });
        showFilterPop(statusPop, textView);
    }

    /**
     * 排序下拉列表
     *
     * @param textView
     */
    private void showOrderPop(final TextView textView) {
        if (sortPop == null) {
            sortPop = new ConditionFilterPop(mContext, sorts);
        } else {
            sortPop.setData(sections);
        }

        sortPop.setConditionCallback(new ConditionFilterPop.ConditionCallback() {
            @Override
            public void selectCondition(int position) {
                SortBean sortBean = sorts.get(position);
                textView.setText(sortBean.getSortName());
                sortId = sortBean.getSortId();
                pageIndex = 1;
                getCourseData();
            }
        });
        showFilterPop(sortPop, textView);
    }

    private void showFilterPop(final ConditionFilterPop filterPop, final TextView textView) {
        filterPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (filterPop == sortPop && sortPop.getSelectPos() == -1) {

                    rbCourseSort.setChecked(false);
                }
//                masking.setVisibility(View.GONE);
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, triangleDownDw, null);
            }
        });
        if (!filterPop.isShowing()) {
            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, triangleUpDw, null);
//            masking.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT < 24) {
                filterPop.showAsDropDown(cutline);
            } else {
               /* int[] location = new int[2];
                textView.getLocationInWindow(location);
                if (Build.VERSION.SDK_INT == 25) {
                    int tempheight = filterPop.getHeight();
                    int screenHeight = ScreenUtil.getScreenHeight(mContext);
                    if (tempheight == WindowManager.LayoutParams.MATCH_PARENT || screenHeight <= tempheight) {
                        filterPop.setHeight(screenHeight - location[1] - textView.getHeight());
                    }
                }
                filterPop.showAtLocation(textView, Gravity.NO_GRAVITY, 0, textView.getHeight() + location[1]);
                filterPop.update();*/
                Rect rect = new Rect();
                textView.getGlobalVisibleRect(rect);
                int height = textView.getResources().getDisplayMetrics().heightPixels - rect.bottom;
                filterPop.setHeight(height);
                filterPop.showAtLocation(textView, Gravity.BOTTOM, 0, 0);
            }
        }
    }

    /**
     * 本校课程  是否能点学段    即默认选中本校
     */
    private boolean isSectionClick() {
        if (statusId == STATUS_OUR) {
            return true;
        }
        return false;
    }

    /**
     * 其他课程  学段选全部时  是否能点年级和学科
     */
    private boolean isOtherClick() {
        if (statusId == STATUS_OTHER && sectionId == -1) {
            return true;
        }
        return false;
    }


    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        getCourseData();
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        getCourseData();
    }

    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }


    @Override
    public void getSubjectSuccess(List<SubjectBean> subjectBeens) {
        this.subjects.clear();
        this.subjects.add(new SubjectBean(-1, getString(R.string.school_all_subject)));
        this.subjects.addAll(subjectBeens);
//        if (subjectPop != null) {
//            subjectPop.setData(subjectBeens);
//        }
    }

    @Override
    public void getSubjectEmpty() {
        this.subjects.clear();
        this.subjects.add(new SubjectBean(-1, getString(R.string.school_all_subject)));
        if (subjectPop != null) {
            subjectPop.setData(subjects);
        }
    }

    @Override
    public void onGradeSuccess(List<GradeJson> gradeJson) {
        this.gradeJsonList.clear();
        this.gradeJsonList.addAll(gradeJson);
//        this.grades.add(new GradeBean(-1, getString(R.string.school_all_grade)));
        setSectionAndGrade(gradeJsonList);


    }

    private void setSectionAndGrade(List<GradeJson> gradeJson) {
        if (sections == null) {
            return;
        }
        this.sections.clear();
        this.grades.clear();
        this.sections.add(new SectionBean(-1, getString(R.string.school_all_section), 0, "", "-1", "", getString(R.string.school_all_section)));
        this.grades.add(new GradeBean(-1, getString(R.string.school_all_grade)));
        for (int i = 0; i < gradeJson.size(); i++) {
            this.sections.add(new SectionBean(Integer.valueOf(gradeJson.get(i).getLevelId()), gradeJson.get(i).getLocalName(),
                    Integer.valueOf(gradeJson.get(i).getEducationId()), gradeJson.get(i).getEducationName(),
                    gradeJson.get(i).getChildList().get(0).getGradeId(), gradeJson.get(i).getChildList().get(0).getGradeName(),
                    gradeJson.get(i).getLevelName()));//添加学段列表
            if (gradeJson.get(i).getLevelId().equalsIgnoreCase(String.valueOf(sectionId)) &&
                    gradeJson.get(i).getEducationId().equalsIgnoreCase(String.valueOf(eduId))) {
                tvCourseSection.setText(gradeJson.get(i).getLevelName());
                for (int j = 0; j < gradeJson.get(i).getChildList().size(); j++) {
                    this.grades.add(new GradeBean(Integer.valueOf(gradeJson.get(i).getChildList().get(j).getGradeId()), gradeJson.get(i).getChildList().get(j).getGradeName()));
                }

            }
        }
    }

    @Override
    public void onGradeEmpty() {
        this.grades.clear();
//        this.grades.add(new GradeBean(-1, "全部年级"));
        if (gradePop != null) {
            gradePop.setData(grades);
        }

        this.sections.clear();
        if (sectionPop != null) {
            sectionPop.setData(sections);
        }

    }

    @Override
    public void getCourseSuccess(List<CourseList> courseListBeen) {
        clearLoingState();
        if (pageIndex == 1) {
            this.courseSchoolBeans.clear();
            if (courseListBeen.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(courseListBeen.size() < pageSize);
        this.courseSchoolBeans.addAll(courseListBeen);
        wrapAdapter.notifyDataSetChanged();

    }

    /**
     * 请登录学校下发的学生账号查看
     */
    @Override
    public void getTocFailedEmpty() {
        clearLoingState();
//        refreshLayout.setLoadMoreEnabled(false);
        if (pageIndex == 1) {
            this.courseSchoolBeans.clear();
            wrapAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptySchoolLayout.NODATA_ENABLE_CLICK);
            emptyLayout.setErrorMessage(getString(R.string.error_view_load_toc));
        } else {
            refreshLayout.setLoadmoreFinished(true);
        }


    }

    @Override
    public void getTobFailedEmpty() {
        clearLoingState();
//        refreshLayout.setLoadMoreEnabled(false);
        if (pageIndex == 1) {
            this.courseSchoolBeans.clear();
            wrapAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptySchoolLayout.NETWORK_ERROR);
        } else {
            refreshLayout.setLoadmoreFinished(true);
        }


    }

    @Override
    public void getToLoginEmpty() {
        clearLoingState();
//        refreshLayout.setLoadMoreEnabled(false);
        if (pageIndex == 1) {
            this.courseSchoolBeans.clear();
            wrapAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
        } else {
            refreshLayout.setLoadmoreFinished(true);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            if (SPToken.autoLogin(mContext)) {
                setInitData();
                getCourseData();
            } else {
                setInitOnResume();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        if (SPToken.autoLogin(getContext()) && !SPManager.isToCLogin()) {

            if (R.id.tv_course_status == view.getId()) {
                showStatusPop((TextView) view);
            } else if (R.id.tv_course_section == view.getId()) {
                if (!isSectionClick()) {
                    showSectionPop((TextView) view);
                }
            } else if (R.id.tv_course_grade == view.getId()) {
                if (!isOtherClick()) {
                    showGradePop((TextView) view);
                }
            } else if (R.id.tv_course_sugject == view.getId()) {
                if (!isOtherClick()) {
                    showSubjectPop((TextView) view);
                }
            } else if (R.id.rb_course_sort == view.getId()) {
//                showOrderPop(view);
            } else {

            }
        }
    }
}
