package com.example.app4home.mvp.view;

import com.sdzn.core.base.BaseView;

public interface UserInfoView extends BaseView {
    void loginSuccess();

    void onFailed();
}
