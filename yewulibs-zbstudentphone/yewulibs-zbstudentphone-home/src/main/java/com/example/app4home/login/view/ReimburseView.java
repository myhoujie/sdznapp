package com.example.app4home.login.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：申请退课
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/3
 */

public interface ReimburseView extends BaseView {

    void dropSuccess(Object o);

    void dropError(String msg);
}
