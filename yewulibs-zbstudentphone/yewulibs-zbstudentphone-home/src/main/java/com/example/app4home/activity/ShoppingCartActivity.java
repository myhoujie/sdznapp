package com.example.app4home.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.example.app4home.R;
import com.example.app4home.adapter.ShoppingCartAdapter;
import com.sdzn.variants.bean.ShoppingCartBean;
import com.example.app4public.event.OrderPayEvent;

import com.example.app4home.mvp.presenter.ShoppingCartPresenter;
import com.example.app4home.mvp.view.ShoppingCartView;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;




/**
 * 描述：
 * - 购物车（无法多选结算）
 * 创建人：baoshengxiang
 * 创建时间：2017/7/20
 */
public class ShoppingCartActivity extends BaseMVPActivity1<ShoppingCartView, ShoppingCartPresenter> implements ShoppingCartView, OnRefreshListener, View.OnClickListener {

    SmartRefreshLayout refreshLayout;
    RecyclerView rcvShoppingCart;
    TitleBar titleBar;
    CheckBox cbAllCheck;
    TextView tvTotalPrice;
    Button btnSettlement;
    EmptyLayout emptyLayout;

    private ShoppingCartAdapter shoppingCartAdapter;
    private List<ShoppingCartBean.ShopCartListBean> shoppingBeans;
    private boolean isEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected ShoppingCartPresenter createPresenter() {
        return new ShoppingCartPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入ShoppingCartActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        refreshLayout = (SmartRefreshLayout) findViewById(R.id.refresh_layout);
        rcvShoppingCart = (RecyclerView) findViewById(R.id.swipe_target);
        cbAllCheck = (CheckBox) findViewById(R.id.cb_all_check);
        btnSettlement = (Button) findViewById(R.id.btn_settlement);
        tvTotalPrice = (TextView) findViewById(R.id.tv_total_price);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);

        btnSettlement.setOnClickListener(this);
        cbAllCheck.setOnClickListener(this);
        EventBus.getDefault().register(this);
        initData();
        initView();
        queryCartData();
    }
    private void initData() {
        shoppingBeans = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdit = !isEdit;
                shoppingCartAdapter.setEdit(isEdit);
                if (isEdit) {
                    titleBar.setRightText("取消");
//                    cbAllCheck.setVisibility(View.VISIBLE);
                    tvTotalPrice.setVisibility(View.GONE);
                    btnSettlement.setText("删除");
                } else {
                    titleBar.setRightText("编辑");
//                    cbAllCheck.setVisibility(View.GONE);
                    tvTotalPrice.setVisibility(View.VISIBLE);
                    btnSettlement.setText("支付");
                }
            }
        });

        refreshLayout.setOnRefreshListener(this);
        rcvShoppingCart.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 10));
        rcvShoppingCart.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        shoppingCartAdapter = new ShoppingCartAdapter(mContext, shoppingBeans);
        rcvShoppingCart.setAdapter(shoppingCartAdapter);
        shoppingCartAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (shoppingCartAdapter.getEdit()) {
                    return;
                }
                ShoppingCartBean.ShopCartListBean course = shoppingBeans.get(position);
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
                startIntent.putExtra("PACKAGE", course.getSellType());
                startIntent.putExtra("courseId",course.getCourseId() );
                startIntent.putExtra("showLiveBtn", false);
                startActivity(startIntent);
            }
        });

        shoppingCartAdapter.setCartCallback(new ShoppingCartAdapter.CartCallback() {
            @Override
            public void isSelectAll(boolean isSelectAll) {
                cbAllCheck.setChecked(isSelectAll);
            }

            @Override
            public void backTotalPrice(double price) {
//                ToastUtils.showShort(""+price);
                String totalPrice = String.format(Locale.getDefault(), "￥%.2f", price);
                SpannableString priceSpan = new SpannableString("合计：" + totalPrice);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textMinor)),
                        0, priceSpan.length() - totalPrice.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                        priceSpan.length() - totalPrice.length(), priceSpan.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                tvTotalPrice.setText(priceSpan);
            }
        });


        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                queryCartData();
            }
        });

    }

    private SpannableString splitString(String firstStr, int firstColor, String secStr, int secColor) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, firstColor)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, secColor)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void deleteShopping() {
        if (shoppingCartAdapter.getCheckedGoods() == null || shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请先选择要删除的物品");
            return;
        }
        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }

        mPresenter.deleteGoods(idSb.toString());
    }

    private void queryCartData() {
        mPresenter.queryShoppingCart();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        queryCartData();
    }



    /**
     * 去结算
     */
    private void tosettlement() {
        if (shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请至少选择一门课程去结算");
            return;
        }

        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }

        Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.OrderSubmitActivity");
        startIntent.putExtra("goods", idSb.toString());
        startActivity(startIntent);
    }

    private void clearRefreshStatus() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    @Override
    public void queryCartSuccess(List<ShoppingCartBean.ShopCartListBean> shoppingCartBeens) {
        clearRefreshStatus();
        this.shoppingBeans.clear();
        this.shoppingBeans.addAll(shoppingCartBeens);
        shoppingCartAdapter.notifyDataSetChanged();
        titleBar.getView(R.id.tv_right).setVisibility(View.VISIBLE);
        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
//        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(totalPrice), R.color.red));
        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(shoppingCartAdapter.getTotalPrice()), R.color.red));

        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void queryCartEmpty() {
        clearRefreshStatus();
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        titleBar.getView(R.id.tv_right).setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.NODATA);
        emptyLayout.setErrorMessage("当前购物车暂无课程");
    }

    @Override
    public void queryCartFailure(String msg) {
        clearRefreshStatus();
        ToastUtils.showShort(msg);
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void delGoodsSuccess() {
        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
        shoppingCartAdapter.removeGoods();
//        shoppingCartAdapter.setTotalPrice(0.0f);
        queryCartData();
    }

    @Override
    public void delGoodsFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (R.id.btn_settlement == view.getId()) {
            if (isEdit) {
                deleteShopping();
            } else {
                tosettlement();
            }
        } else if (R.id.cb_all_check == view.getId()) {
            shoppingCartAdapter.setSelectAll(cbAllCheck.isChecked());

        }
    }
}
