package com.example.app4home.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.azhon.appupdate.manager.DownloadManager;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app4home.R;
import com.example.app4home.fragment.LiveCourseFragment;
import com.example.app4home.fragment.MineFragment;
import com.example.app4home.fragment.SpellingClassFragment;
import com.example.app4home.fragment.UserInfoFragment;
import com.example.app4home.login.presenter.CheckverionPresenter;
import com.example.app4home.login.view.CheckverionView;
import com.example.app4home.mvp.presenter.MainPresenter;
import com.example.app4home.mvp.view.MainView;
import com.example.app4public.permission.MPermission;
import com.example.app4public.permission.annotation.OnMPermissionDenied;
import com.example.app4public.permission.annotation.OnMPermissionGranted;
import com.example.app4public.service.DownLoadDataService;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.FragmentTabUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.variants.bean.VersionInfoBean;
import com.sdzn.variants.network.SPToken;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;


/**
 * saveGrade
 */
public class MainActivity extends BaseMVPActivity1<MainView, MainPresenter> implements CheckverionView, MainView, MineFragment.OnFragmentInteractionListener {

    RadioGroup rgNaviBottom;

    private List<Fragment> fragments;
    private FragmentTabUtils fragmentTabUtils;


    public static final String AUTO_LOGIN = "autoLogin";
    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    ToastUtils.showLong("进入MainActivity成功");
                }
            }
        }
        rgNaviBottom = (RadioGroup) findViewById(R.id.rg_navi_bottom);
        requestBasicPermission();
        initData();
        initView();
    }


    @Override
    protected void setStatusBar() {
        int color = getResources().getColor(R.color.colorPrimary);
        StatusBarUtil.setColor(this, color, 0);
    }


    private void initData() {
        fragments = new ArrayList<>();
        fragments.add(SpellingClassFragment.newInstance());
        fragments.add(LiveCourseFragment.newInstance());
        fragments.add(MineFragment.newInstance());
        fragments.add(UserInfoFragment.newInstance());
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        //检查更新
        checkverionPresenter.checkVerion("3", "0");
        if (SPToken.autoLogin(mContext)) {
            //获取未读消息数
            mPresenter.getUnReadMessageCount();
        }
    }


    private void initView() {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.fl_container, UserInfoFragment.newInstance());
//        fragmentTransaction.commit();

        fragmentTabUtils = new FragmentTabUtils(mContext, getSupportFragmentManager(), fragments, R.id.main_container, rgNaviBottom);
        fragmentTabUtils.setNeedAnimation(false);


    }


    @Override
    public void onBackPressed() {
        App2Utils.exitApp(mContext);
    }

    @Override
    public void onFragmentInteraction() {
    }

    @Override
    public void autoLoginSuccess() {
        initData();
        initView();
        //判断是否更新
    }


    private ProgressDialog pd;
    private DownloadManager manager;
    private String updateTitle = "发现新版本";

/*
    @Override
    public void updateVersion(String updateInfo, final String targetUrl) {
       */
/* // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudentphone");//apk下载位置
        forceconfig.setApkSaveName("拼课堂");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();*//*

//        // 需要更新，弹出更新窗口
//        UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
//        if (!TextUtils.isEmpty(updateInfo)) {
//            builder.setMessage(updateInfo);
//        }
//        builder.setPositive(new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                // 如果是强制升级，显示一个进度条
//                if (true) {
//                    pd = new ProgressDialog(MainActivity.this);
//                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                    pd.setTitle("下载中");
//                    pd.setCancelable(false);
//                    pd.setMax(100);
//                    pd.show();
//                }
//                UpdateConfiguration configuration = new UpdateConfiguration()
//                        .setForcedUpgrade(true)
//                        .setOnDownloadListener(new OnDownloadListener() {
//                            @Override
//                            public void start() {
//
//                            }
//
//                            @Override
//                            public void downloading(int max, int progress) {
//                                if (pd != null) {
//                                    int curr = (int) (progress / (double) max * 100.0);
//                                    pd.setProgress(curr);
//                                }
//                            }
//
//                            @Override
//                            public void done(File apk) {
//
//                            }
//
//                            @Override
//                            public void cancel() {
//
//                            }
//
//                            @Override
//                            public void error(Exception e) {
//                                if (pd != null) {
//                                    pd.dismiss();
//                                }
//                            }
//                        });
//                manager = DownloadManager.getInstance(MainActivity.this);
//                manager.setApkName("拼课堂.apk")
//                        .setApkUrl(targetUrl)
//                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setConfiguration(configuration)
//                        .setAuthorities("com.sdzn.live.fileprovider")
//                        .download();
//
//
//            }
//        });
//        CustomDialog dialog = builder.create();
//        dialog.setCancelable(false);
//        dialog.show();
    }
*/


    @Override
    protected void onDestroy() {
        DownLoadDataService.stopDownloadService(this);
        super.onDestroy();
        checkverionPresenter.onDestory();
//        if (BuildConfig.ISDEBUG) {
        // FIXME: 2017-11-16 蒲公英sdk有问题, 会导致应用crash
//            PgyUpdateManager.unregister();
//        }
    }

    public static final int PERMISSION_CODE = 1200;

    private void requestBasicPermission() {
        MPermission.with(this)
                .addRequestCode(PERMISSION_CODE)
                .permissions(
                        Manifest.permission.ACCESS_NETWORK_STATE,
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(PERMISSION_CODE)
    public void onBasicPermissionSuccess() {


    }

    @OnMPermissionDenied(PERMISSION_CODE)
    public void onBasicPermissionFailed() {
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = App2Utils.getAppVersionCode(App2.get());//获取版本号
            int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
            if (updateVersion > currVersion) {
                if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                    Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
                }
            }
        }
    }

    private void Updatemethod(String description, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/zbstudentphone");//apk下载位置
        forceconfig.setApkSaveName("拼课堂phone");//app名称
        String count = description.replace("|", "\n");


        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
//        com.sdzn.core.utils.ToastUtils.showLong(this, bean);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
//        com.sdzn.core.utils.ToastUtils.showLong(this, msg);
    }


}