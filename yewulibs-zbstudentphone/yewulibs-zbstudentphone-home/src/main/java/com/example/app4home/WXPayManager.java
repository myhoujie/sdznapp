package com.example.app4home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import com.just.agentweb.App2;
import com.sdzn.core.utils.ToastUtils;

import com.sdzn.variants.bean.PayInfoBean;
import com.example.app4public.event.OrderPayEvent;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

/**
 * 描述：
 * - 微信支付管理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WXPayManager {
    private Context context;
    IWXAPI msgApi;
    private Dialog payDialog;

    public WXPayManager(Context context) {
        this.context = context;
    }


    public void doStartWXPayPlugin(PayInfoBean.WxpayParamsBean wxpayParamsBean) {
        msgApi = WXAPIFactory.createWXAPI(context, null);
        if (!isWXAppInstalledAndSupported(msgApi)) {
            EventBus.getDefault().post(new OrderPayEvent(false));
            ((Activity) context).finish();
            return;
        }
        msgApi.registerApp(wxpayParamsBean.getAppid());
        try {
            startPay(wxpayParamsBean);
        } catch (Exception e) {

        }
    }

    private void startPay(PayInfoBean.WxpayParamsBean info) throws Exception {
        PayReq req = new PayReq();
        req.appId = info.getAppid();
        req.partnerId = info.getPartnerid();
        req.prepayId = info.getPrepayid();
        req.nonceStr = info.getNoncestr();
        req.timeStamp = info.getTimestamp();
        req.packageValue = info.getPackageX();
        req.sign = info.getSign();
//        req.extData = "app data"; // optional
        msgApi.sendReq(req);
    }

    private static boolean isWXAppInstalledAndSupported(IWXAPI api) {
        boolean sIsWXAppInstalledAndSupported = api.isWXAppInstalled();
        if (!sIsWXAppInstalledAndSupported) {
            ToastUtils.showLong(App2.get(),"微信客户端未安装，请确认后重新支付");
        } else if (api.getWXAppSupportAPI() < Build.PAY_SUPPORTED_SDK_INT) {
            ToastUtils.showShort(App2.get(),"微信客户端版本过低，请升级后重新支付");
        }

        return sIsWXAppInstalledAndSupported;
    }

}