package com.example.app4home.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;

import com.sdzn.core.base.BaseActivity1;
import com.example.app4home.R;
import com.example.app4public.widget.TitleBar;



public class WebActivity extends BaseActivity1 {
    public static final String INTENT_WEB = "userType";
    WebView mWebView;
    TitleBar titleBar;
    private String nameType = "";//1 拼课堂用户协议  2拼课堂用户隐私协议
    private String url = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_web;
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入WebActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        mWebView = (WebView) findViewById(R.id.web);

        nameType = getIntent().getStringExtra("userType");
        if (!TextUtils.isEmpty(nameType)) {
            if ("1".equals(nameType)) {
                url = "http://www.znclass.com/video/Useragreement.html";
                nameType = "用户协议";
            } else if ("2".equals(nameType)) {
                url = "http://www.znclass.com/video/agreement.html";
                nameType = "隐私协议";
            } else {
                nameType = "暂无";

            }
            titleBar.setTitleText(nameType);

            mWebView.loadUrl(url);
//            WebSettings settings = mWebView.getSettings();
//            settings.setJavaScriptEnabled(true);
//            settings.setUseWideViewPort(true);
//            settings.setLoadWithOverviewMode(true);
//            settings.setLoadsImagesAutomatically(true);
//            settings.setJavaScriptCanOpenWindowsAutomatically(true);
        }
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.this.finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        WebActivity.this.finish();
    }
}
