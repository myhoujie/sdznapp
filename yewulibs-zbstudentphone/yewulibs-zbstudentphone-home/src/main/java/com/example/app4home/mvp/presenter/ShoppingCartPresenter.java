package com.example.app4home.mvp.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.ShoppingCartBean;
import com.example.app4home.mvp.view.ShoppingCartView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public class ShoppingCartPresenter extends BasePresenter<ShoppingCartView> {

    /**
     * 查询购物车
     */
    public void queryShoppingCart() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .queryShoppingCart()
                .compose(TransformUtils.<ResultBean<List<ShoppingCartBean.ShopCartListBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<ShoppingCartBean.ShopCartListBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<ShoppingCartBean.ShopCartListBean>>() {


                    @Override
                    public void onNext(List<ShoppingCartBean.ShopCartListBean> shopCartList) {
                        if (shopCartList == null ||shopCartList.isEmpty()) {
                            getView().queryCartEmpty();
                        } else {
                            getView().queryCartSuccess(shopCartList);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().queryCartFailure(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 删除购物车课程
     */
    public void deleteGoods(String goodsId) {
        Map<String, String> map = new HashMap<>();
        map.put("ids", goodsId);
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .delShoppingCart(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<Object>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {


                    @Override
                    public void onNext(Object obj) {
                        getView().delGoodsSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().delGoodsFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

}
