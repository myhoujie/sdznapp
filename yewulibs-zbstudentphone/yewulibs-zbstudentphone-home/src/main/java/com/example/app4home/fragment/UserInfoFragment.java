package com.example.app4home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.example.app4home.R;
import com.sdzn.variants.bean.UserBean;
import com.example.app4public.event.MsgCountEvent;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4public.event.UpdateAvatarEvent;

import com.example.app4public.manager.SPManager;
import com.example.app4home.mvp.presenter.UserInfoPresenter;
import com.example.app4home.mvp.view.UserInfoView;
import com.example.app4public.utils.GradeIdToNameUtils;
import com.example.app4public.widget.RoundRectImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;




import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * 描述：个人信息
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class UserInfoFragment extends BaseMVPFragment<UserInfoView, UserInfoPresenter> implements UserInfoView, View.OnClickListener {

    RoundRectImageView imgAvatar;
    TextView tvUserName;
    TextView tvUserClass;
    TextView llIndent;
    TextView llCollect;
    TextView llNotes;
    TextView llAccount;
    TextView llCoupon;
    ImageView ivPoint;
    ImageView ivGoInfo;
    TextView llZuoye;
    TextView llStatistics;
    TextView tvSystemSettings;
    TextView tvMessage;
    RelativeLayout rlToInfo;

    UserBean userBean;


    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_user_info;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        imgAvatar = (RoundRectImageView) rootView.findViewById(R.id.img_avatar);
        tvUserName = (TextView) rootView.findViewById(R.id.tv_user_name);
        tvUserClass = (TextView) rootView.findViewById(R.id.tv_user_class);
        ivGoInfo = (ImageView) rootView.findViewById(R.id.iv_go_info);
        llIndent = (TextView) rootView.findViewById(R.id.ll_indent);
        llCollect = (TextView) rootView.findViewById(R.id.ll_collect);
        llZuoye = (TextView) rootView.findViewById(R.id.ll_zuoye);
        llStatistics = (TextView) rootView.findViewById(R.id.ll_statistics);
        llNotes = (TextView) rootView.findViewById(R.id.ll_notes);
        llAccount = (TextView) rootView.findViewById(R.id.ll_account);
        llCoupon = (TextView) rootView.findViewById(R.id.ll_coupon);
        ivPoint = (ImageView) rootView.findViewById(R.id.iv_point);
        tvSystemSettings = (TextView) rootView.findViewById(R.id.tv_system_settings);
        tvMessage = (TextView) rootView.findViewById(R.id.tv_message);
        rlToInfo = (RelativeLayout) rootView.findViewById(R.id.rl_to_info);

        llCoupon.setOnClickListener(this);
        imgAvatar.setOnClickListener(this);
        llIndent.setOnClickListener(this);
        llCollect.setOnClickListener(this);
        llNotes.setOnClickListener(this);
        llZuoye.setOnClickListener(this);
        llAccount.setOnClickListener(this);
        llStatistics.setOnClickListener(this);
        tvSystemSettings.setOnClickListener(this);
        tvMessage.setOnClickListener(this);
        rlToInfo.setOnClickListener(this);
        initData();
    }

     @Override
    public void onResume() {
        super.onResume();
        initView();

    }

    private void initData() {

    }

    private void initView() {
        if (SPToken.autoLogin(mContext)) {

            ivGoInfo.setVisibility(View.VISIBLE);
            GlideImgManager.loadAvatarImage(mContext, "" + SPManager.getUser().getPicImg(), imgAvatar);
            if (SPManager.isToCLogin()) {
                tvUserClass.setText("" + GradeIdToNameUtils.setName(SPManager.getUser().getSubjectId(), SPManager.getUser().getGrade()));
//                    llZuoye.setVisibility(View.GONE);
                if (SPManager.getUser().getStudentName() == null || SPManager.getUser().getStudentName().isEmpty()) {
                    tvUserName.setText(SPManager.getUser().getMobile() + "");
                } else {
                    tvUserName.setText(SPManager.getUser().getStudentName() + "");
                }
            } else {
                tvUserClass.setText("" + GradeIdToNameUtils.setName(SPManager.getSchoolSectionId(), SPManager.getSchoolgradeId()));
//                    llZuoye.setVisibility(View.VISIBLE);
                tvUserName.setText(SPManager.getUser().getStudentName() + "");
            }
        } else {
            tvUserName.setText(getString(R.string.string_user_title));
            tvUserClass.setText(getString(R.string.string_user_content));
            GlideImgManager.loadImage(mContext, R.mipmap.ic_user_avatar, imgAvatar);
            ivGoInfo.setVisibility(View.GONE);
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            userBean = SPManager.getUser();
            GlideImgManager.loadAvatarImage(mContext, "" + userBean.getPicImg(), imgAvatar);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        userBean = SPManager.getUser();
        initView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void queryMessageCount(MsgCountEvent countEvent) {
        ivPoint.setVisibility(0 == countEvent.getCount() ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected UserInfoPresenter createPresenter() {
        return new UserInfoPresenter();
    }

    @Override
    public void loginSuccess() {

    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onClick(View view) {
        if (!SPToken.autoLogin(mContext)) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
            startActivity(startIntent);
            return;
        }
        if (R.id.ll_indent == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.IndentActivity");
            startActivity(startIntent);
        } else if (R.id.ll_collect == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CollectActivity");
            startActivity(startIntent);
        } else if (R.id.ll_notes == view.getId()) {
        } else if (R.id.img_avatar == view.getId() || R.id.ll_account == view.getId() || R.id.rl_to_info == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.AccountSettingActivity");
            startActivity(startIntent);
        } else if (R.id.ll_coupon == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CouponActivity");
            startIntent.putExtra("titleName", "我的优惠券");
            startActivity(startIntent);
        } else if (R.id.tv_system_settings == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.SystemSettingActivity");
            startActivity(startIntent);
        } else if (R.id.tv_message == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MessageActivity");
            startActivity(startIntent);
        } else if (R.id.ll_zuoye == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.MineTaskActivity");
            startActivity(startIntent);
        } else if (R.id.ll_statistics == view.getId()) {
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebStatisticsActivity");
            startActivity(startIntent);
        }
    }
}
