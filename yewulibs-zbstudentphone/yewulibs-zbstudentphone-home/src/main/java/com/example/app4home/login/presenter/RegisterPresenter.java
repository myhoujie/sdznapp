package com.example.app4home.login.presenter;

import android.text.TextUtils;

import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4home.login.view.RegisterView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/9/15
 */
public class RegisterPresenter extends BasePresenter<RegisterView> {


    public void getVerifyCode(String phoneNo) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }

    public void retrievePassword(String account,String phoneNo,String code) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getRetrievePassword(account,code,phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().registerSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().registerFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);

    }

}
