package com.example.app4home.adapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.core.content.ContextCompat;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.ConvertUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.ShoppingCartBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：
 * - 购物车（可多选结算）
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class ShoppingCartAdapter1 extends BaseRcvAdapter<ShoppingCartBean.ShopCartListBean> {
    private HashMap<String, Integer> checkedGoods;
    private CartCallback cartCallback;
    private double totalPrice;

    public ShoppingCartAdapter1(Context context, List mList) {
        super(context, R.layout.item_shopping_cart, mList);
        checkedGoods = new HashMap<>();
    }

    @Override
    public void convert(BaseViewHolder holder, final int position, final ShoppingCartBean.ShopCartListBean shoppingCartBean) {
        final ShoppingCartBean.ShopCartListBean.CourseBean courseBean = shoppingCartBean.getCourse();
        CheckBox cbChecked = holder.getView(R.id.cb_checked);
        cbChecked.setOnCheckedChangeListener(null);
        cbChecked.setChecked(checkedGoods.containsKey(String.valueOf(shoppingCartBean.getId())));
        cbChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkedGoods.put(String.valueOf(shoppingCartBean.getId()), position);
                    totalPrice += courseBean.getCurrentPrice();
                } else {
                    checkedGoods.remove(String.valueOf(shoppingCartBean.getId()));
                    totalPrice -= courseBean.getCurrentPrice();
                }
                cartCallback.isSelectAll(judgeSelectAll());
                cartCallback.backTotalPrice(totalPrice);
            }
        });

        holder.setImageView(R.id.iv_cover, "" + courseBean.getLogo());
        holder.setText(R.id.tv_recmd_title, courseBean.getCourseName());
        String lessionNum = String.valueOf(courseBean.getLessionNum());
        SpannableString lessonNumSpan = new SpannableString(lessionNum + "课次");
        lessonNumSpan.setSpan(new AbsoluteSizeSpan(ConvertUtils.dp2px(context, 14)),
                0, lessionNum.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        lessonNumSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary)),
                0, lessionNum.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        lessonNumSpan.setSpan(new AbsoluteSizeSpan(ConvertUtils.dp2px(context, 10)),
                lessionNum.length(), lessonNumSpan.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        lessonNumSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textMinor)),
                lessionNum.length(), lessonNumSpan.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        holder.setText(R.id.tv_course_count, lessonNumSpan);
        holder.setText(R.id.tv_recmd_price, "￥" + courseBean.getCurrentPrice());

        if ("PACKAGE".equals(courseBean.getSellType())) {
            holder.setText(R.id.tv_type, "组合");
            if (courseBean.getPackageType() == 1) {
                holder.setText(R.id.tv_course_type, "「直播」");
            } else {
                holder.setText(R.id.tv_course_type, "「点播」");
            }
        } else {
            holder.setText(R.id.tv_type, "单科");
            if ("COURSE".equals(courseBean.getSellType())) {
                holder.setText(R.id.tv_course_type, "「点播」");
            } else if ("LIVE".equals(courseBean.getSellType())) {
                holder.setText(R.id.tv_course_type, "「直播」");
            }

        }


    }

    /**
     * 判断是否全选
     *
     * @return
     */
    private boolean judgeSelectAll() {
        return checkedGoods.size() == mList.size();

    }

    public void setSelectAll(boolean isSelectAll) {
        checkedGoods.clear();
        totalPrice = 0.00f;
        if (isSelectAll) {
            for (int i = 0; i < mList.size(); i++) {
                ShoppingCartBean.ShopCartListBean shopCartListBean = mList.get(i);
                checkedGoods.put(String.valueOf(shopCartListBean.getId()), i);
                totalPrice += shopCartListBean.getCourse().getCurrentPrice();
            }
        }
        setTotalPrice(totalPrice);
        notifyDataSetChanged();
    }

    public Map<String, Integer> getCheckedGoods() {
        return checkedGoods;
    }

    public void removeGoods() {
        checkedGoods.clear();
        notifyDataSetChanged();
    }

    public void setCartCallback(CartCallback cartCallback) {
        this.cartCallback = cartCallback;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public interface CartCallback {
        void isSelectAll(boolean isSelectAll);

        void backTotalPrice(double price);
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        cartCallback.backTotalPrice(totalPrice);
    }
}
