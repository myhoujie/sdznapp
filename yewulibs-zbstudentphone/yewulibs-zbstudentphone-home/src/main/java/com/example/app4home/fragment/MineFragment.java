package com.example.app4home.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4mykc.adapter.MineCourseFragmentPagerAdapter;
import com.example.app4mykc.fragment.CoursePageFragment;
import com.google.android.material.tabs.TabLayout;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseFragment;
import com.example.app4home.R;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4public.event.UpdateAvatarEvent;

import com.example.app4public.widget.EmptySchoolLayout;
import com.example.app4public.widget.NoScrollViewPager;
import com.example.app4public.widget.RoundRectImageView;
import com.example.app4public.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;



/**
 * 描述：
 * - 我的页面
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class MineFragment extends BaseFragment {

    EmptySchoolLayout emptySchoolLayout;
    TitleBar titleBar;
    TabLayout tabLayout;
    NoScrollViewPager vpCourse;
    RoundRectImageView imgAvatar;
    View views;

    private MineCourseFragmentPagerAdapter fragmentAdapter;
    private List<CoursePageFragment> listFragment;//定义要装fragment的列表
    private OnFragmentInteractionListener mListener;

    public MineFragment() {
        // Required empty public constructor
    }

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        titleBar = (TitleBar) rootView.findViewById(R.id.title_bar);
        imgAvatar = (RoundRectImageView) rootView.findViewById(R.id.img_avatar);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabl_course);
        vpCourse = (NoScrollViewPager) rootView.findViewById(R.id.vp_course);
        views = (View) rootView.findViewById(R.id.view);
        emptySchoolLayout = (EmptySchoolLayout) rootView.findViewById(R.id.empty_layout_mine);
        EventBus.getDefault().register(this);
        initView();
        initData();
    }

    private void initData() {
//        userBean = SPManager.getUser();
//        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(), R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);

    }

    @Override
    public void onResume() {
        super.onResume();
        setInitOnResume();
    }

    private void initView() {
        titleBar.getView(R.id.iv_left).setVisibility(View.VISIBLE);
        titleBar.setLeftClickListener(null);
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPToken.autoLogin(mContext)) {
                     Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.LoginActivity");
                startActivity(startIntent);
                    return;
                }
                    Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ShoppingCartActivity");
                startActivity(startIntent);
            }
        });

        listFragment = new ArrayList<>();
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_TODY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_RECENTLY));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE));
        listFragment.add(CoursePageFragment.newInstance(CoursePageFragment.TYPE_COURSE_OVER));

        fragmentAdapter = new MineCourseFragmentPagerAdapter(getChildFragmentManager(), listFragment, mContext);
        vpCourse.setAdapter(fragmentAdapter);
        vpCourse.setNoScroll(false);
        vpCourse.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(vpCourse);

    }

//    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onFragmentInteraction();
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setInitOnResume() {
        if (!SPToken.autoLogin(mContext)) {
            views.setVisibility(View.VISIBLE);
            emptySchoolLayout.setErrorType(EmptySchoolLayout.DATA_TO_LOGIN);
            emptySchoolLayout.setErrorMessage(getString(R.string.error_view_load_mine));
        } else {
            emptySchoolLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
            views.setVisibility(View.GONE);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            initData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            setInitOnResume();
        }
    }
}
