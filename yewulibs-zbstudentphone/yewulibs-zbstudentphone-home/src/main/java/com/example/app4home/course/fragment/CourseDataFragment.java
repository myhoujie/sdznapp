package com.example.app4home.course.fragment;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app4public.service.DownLoadDataService;
import com.google.gson.Gson;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.example.app4home.R;
import com.example.app4home.course.adapter.CourseDataNewAdapter;
import com.sdzn.variants.bean.CourseDetailBean;
import com.sdzn.variants.bean.CourseFileBean;
import com.sdzn.variants.bean.ResultBean;
import com.example.app4public.event.UpdateAccountEvent;

import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewSchoolFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;
import com.example.app4public.permission.MPermission;
import com.example.app4public.permission.annotation.OnMPermissionDenied;
import com.example.app4public.permission.annotation.OnMPermissionGranted;
import com.example.app4public.utils.PriceUtil;
import com.example.app4public.widget.EmptyResorceLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import okhttp3.RequestBody;

/**
 * 课程资料
 */
public class CourseDataFragment extends BaseFragment implements DownLoadDataService.DownloadListener {

    RecyclerView mRecyclerView;
    EmptyResorceLayout emptyResorceLayout;

    private List<CourseFileBean> courseFileList = new ArrayList<>();
    //    private CourseDataAdapter mDataAdapter;
    private CourseDataNewAdapter mDataAdapter;

    private CourseDetailBean courseDetailBean;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course_data;
    }

    public static CourseDataFragment newInstance(int courseType) {
        CourseDataFragment fragment = new CourseDataFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("courseType", courseType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        getService();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IS_REFRESH) {
            initData();
            IS_REFRESH = false;
        }
        requestBasicPermission();
    }

    @Override
    protected void handleMessage(Message msg) {
        if (msg.what != 1) return;
        getService();
    }

    private void getService() {
        DownLoadDataService service = DownLoadDataService.getService();
        if (service == null) {
            mHandler.sendEmptyMessageDelayed(1, 100);
        } else {
            service.addDownloadListener(CourseDataFragment.this);
        }
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rcv_course_data);
        emptyResorceLayout = (EmptyResorceLayout) rootView.findViewById(R.id.empty_layout_rescore);

        IS_REFRESH = true;
        initView();
    }

    private void initView() {
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL, ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));


        mDataAdapter = new CourseDataNewAdapter(getActivity(), courseFileList);
        mRecyclerView.setAdapter(mDataAdapter);
    }

    private void initData() {
        boolean showData = PriceUtil.isFree(courseDetailBean.getCurrentPrice()) || courseDetailBean.isPurchase();
        if (SPToken.autoLogin(mContext) && showData) {
            getCourse();
        } else if (!SPToken.autoLogin(mContext)) {//去登录
            emptyResorceLayout.setErrorType(EmptyResorceLayout.DATA_TO_LOGIN);
        } else {
            emptyResorceLayout.setErrorType(EmptyResorceLayout.NODATA);
            emptyResorceLayout.setErrorMessage("暂无权限！");
        }
    }


    public void setData(CourseDetailBean courseDetailBean) {
        this.courseDetailBean = courseDetailBean;
    }


    @Override
    public void onDownload() {
        if (mDataAdapter != null) {
            mDataAdapter.notifyDataSetChanged();
        }
    }

    private boolean IS_REFRESH = false;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            IS_REFRESH = true;
        }
    }

    @Override
    public void onDestroy() {
        DownLoadDataService service = DownLoadDataService.getService();
        if (service != null) {
            service.removeDownloadListener(this);
        }
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getCourse() {
        Map<String, String> map = new HashMap<>();
        map.put("courseId", String.valueOf(courseDetailBean.getCourseId()));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        RestApi.getInstance()
                .createNew(CourseService.class)
                .getAppResource(requestBody)
                .compose(TransformUtils.<ResultBean<List<CourseFileBean>>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<List<CourseFileBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<CourseFileBean>>() {
                    @Override
                    public void onNext(List<CourseFileBean> list) {
                        if (list != null) {
                            courseFileList.clear();
                            courseFileList.addAll(list);
                            mDataAdapter.notifyDataSetChanged();
                            if (courseFileList.size() > 0) {
                                emptyResorceLayout.setErrorType(EmptyResorceLayout.HIDE_LAYOUT);
                            } else {
                                emptyResorceLayout.setErrorType(EmptyResorceLayout.NODATA);
                                emptyResorceLayout.setErrorMessage("暂无相关资料！");
                            }
                        } else {
                            emptyResorceLayout.setErrorType(EmptyResorceLayout.NODATA);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        emptyResorceLayout.setErrorType(EmptyResorceLayout.NODATA);
                        emptyResorceLayout.setErrorMessage("数据请求错误");
                    }
                }, mContext, false));

    }

    public static final int BAIJIAYUN_CODE = 10011;

    private void requestBasicPermission() {
        MPermission.with(getActivity())
                .addRequestCode(BAIJIAYUN_CODE)
                .permissions(
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(BAIJIAYUN_CODE)
    public void onBasicPermissionSuccess() {
    }

    @OnMPermissionDenied(BAIJIAYUN_CODE)
    public void onBasicPermissionFailed() {
        Intent startIntent = new Intent(App2Utils.getAppPackageName(mContext) + ".hs.act.MainActivity");
        startIntent.putExtra("autoLogin",false);
        startActivity(startIntent);
    }
}
