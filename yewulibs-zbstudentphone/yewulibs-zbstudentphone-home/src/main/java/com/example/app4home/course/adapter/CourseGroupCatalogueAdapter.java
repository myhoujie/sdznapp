package com.example.app4home.course.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.example.app4home.R;
import com.sdzn.variants.bean.CourseCatalogueBean;
import com.sdzn.variants.bean.CourseKpointListBean;
import com.example.app4public.manager.constant.CourseCons;

import java.util.List;

/**
 * 描述：
 * - 课程详情章节列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class CourseGroupCatalogueAdapter extends SectionedRecyclerViewAdapter {

    private SparseBooleanArray mBooleanMap;//记录下哪个section是被打开的
    private int courseType;

    public CourseGroupCatalogueAdapter(Context context, int courseType, List mList) {
        super(context, 0, mList);
        this.courseType = courseType;
        mBooleanMap = new SparseBooleanArray();
    }

    @Override
    protected int getSectionCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    protected int getSectionItemViewType(int section, int position) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        return courseCatalogueBean.getCourseKpointList().get(position).getKpointType();
    }

    @Override
    protected int getItemCountForSection(int section) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        int count = courseCatalogueBean.getCourseKpointList().size();
        if (!mBooleanMap.get(section)) {
            count = 0;
        }

        return courseCatalogueBean.getCourseKpointList().isEmpty() ? 0 : count;
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected BaseViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        View headView = mLayoutInflater.inflate(R.layout.item_course_catalogue_group, parent, false);
        return new BaseViewHolder(context, headView);
    }

    @Override
    protected BaseViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected BaseViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        switch (viewType) {
            case 0:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_chapter);
                break;
            case 1:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_course_catalogue_section);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(final BaseViewHolder holder, final int section) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        holder.setText(R.id.tv_catalogue, courseCatalogueBean.getCourseName());
        boolean isOpen = mBooleanMap.get(section);
        holder.setImageView(R.id.iv_open, isOpen ? R.mipmap.ic_arrow_up : R.mipmap.ic_arrow_down);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpen = mBooleanMap.get(section);
                mBooleanMap.put(section, !isOpen);
                notifyDataSetChanged();
            }
        });
    }

    public void setSectionIsOpen(int section, boolean isOpen) {
        if (section < 0 || section > mList.size()) {
            return;
        }
        mBooleanMap.put(section, isOpen);
    }

    @Override
    protected void onBindSectionFooterViewHolder(BaseViewHolder holder, int section) {

    }

    @Override
    protected void onBindItemViewHolder(BaseViewHolder holder, int section, int position) {
        CourseCatalogueBean courseCatalogueBean = (CourseCatalogueBean) mList.get(section);
        CourseKpointListBean kpointBean = courseCatalogueBean.getCourseKpointList().get(position);
        switch (holder.getItemViewType()) {
            case 0:
                holder.setText(R.id.tv_chacpter, kpointBean.getName());
                break;
            case 1:
                holder.itemView.setBackgroundResource(R.color.gray_f6);
                if (courseType == CourseCons.Type.LIVING) {
                    holder.setText(R.id.tv_catalogue, kpointBean.getName());
                    holder.setVisible(R.id.tv_date, true);
                    String day = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "yyyy-MM-dd");
                    String strartTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveBeginTime()), "HH:mm");
                    String endDay = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "yyyy-MM-dd");
                    String endTime = TimeUtils.millis2String(TimeUtils.string2Millis(kpointBean.getLiveEndTime()), "HH:mm");
                    if (day.equals(endDay)) {
                        holder.setText(R.id.tv_date, day + " " + strartTime + "~" + endTime);
                    }else {
                        holder.setText(R.id.tv_date, day + " " + strartTime + "~" +endDay + " " +  endTime);
                    }
                    String status = CourseCons.LiveStatus.upStatus(kpointBean.getLiveStates());
                    holder.setInVisible(R.id.img_liveing,
                            CourseCons.LiveStatus.isLiving(status) || CourseCons.LiveStatus.isRest(status));
                } else {
                    holder.setVisible(R.id.tv_date, false);
                    holder.setInVisible(R.id.img_liveing, false);
                    holder.setText(R.id.tv_chapter, kpointBean.getName());
                }
                if (!TextUtils.isEmpty(kpointBean.getTeacherName())) {
                    holder.setText(R.id.tv_teacher, "讲师：" + kpointBean.getTeacherName());
                }
                if (kpointBean.getFree()==1) {
                    holder.setVisible(R.id.tv_audition, true);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void convert(BaseViewHolder holder, int position, Object o) {

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }
}
