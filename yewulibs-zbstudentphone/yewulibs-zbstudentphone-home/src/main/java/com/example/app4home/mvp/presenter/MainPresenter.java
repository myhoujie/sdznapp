package com.example.app4home.mvp.presenter;

import android.text.TextUtils;

import com.just.agentweb.App2;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.App2Utils;
import com.example.app4home.R;

import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.VersionInfoBean;
import com.example.app4public.event.MsgCountEvent;
import com.example.app4public.manager.SPManager;
import com.example.app4home.mvp.view.MainView;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.api.ResponseNewFuncLogin;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import org.greenrobot.eventbus.EventBus;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/8/11
 */
public class MainPresenter extends BasePresenter<MainView> {
/*
    public void checkVerion() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryVersion(0)
                .compose(TransformUtils.<ResultBean<VersionInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<VersionInfoBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfoBean>() {
                    @Override
                    public void onNext(VersionInfoBean versionInfoBean) {
                        int currVersion = App2Utils.getAppVersionCode(App2.get());
//                        VersionInfoBean versionInfo = versionInfoBean.getVersionInfo();
                        if (versionInfoBean.getVersionNumber() > currVersion) {//此处为
                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        e.printStackTrace();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }
*/

    public void getUnReadMessageCount() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryUnReadMsgCount("0", SPManager.getUser().getUserId())
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFuncLogin<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
//                        getView().queryUnReadMsgCountSuccess((int) resultBean.getUnreadMsgCount());
                        Double obj = (Double) o;
                        EventBus.getDefault().post(new MsgCountEvent(obj.intValue()));
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
//                        ToastUtils.showShort(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }





}
