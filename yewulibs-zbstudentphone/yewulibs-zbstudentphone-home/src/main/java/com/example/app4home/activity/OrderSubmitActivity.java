package com.example.app4home.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4home.WXPayManager;
import com.sdzn.core.base.BaseMVPActivity1;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.core.widget.SweetAlertDialog;
import com.example.app4home.R;
import com.example.app4home.adapter.OrderInfoAdapter;
import com.sdzn.variants.bean.CouponBean;
import com.sdzn.variants.bean.PayInfoBean;
import com.sdzn.variants.bean.ShoppingCartBean;
import com.example.app4public.event.OrderPayEvent;
import com.example.app4public.manager.AlipayManager;

import com.example.app4public.manager.constant.PayType;
import com.example.app4home.mvp.presenter.OrderSubmitPresenter;
import com.example.app4home.mvp.view.OrderSubmitView;
import com.example.app4public.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;




/**
 * 描述：
 * - 订单提交
 */
public class OrderSubmitActivity extends BaseMVPActivity1<OrderSubmitView, OrderSubmitPresenter>
        implements OrderSubmitView, RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    RecyclerView rcvCourse;
    TextView tvOrderAmount;
    TextView tvDiscountAmount;
    TextView tvActualPayment;
    TitleBar titleBar;
    RadioGroup rgPayment;
    RadioButton rbWxpay;
    RadioButton rbAlipay;
    TextView tvCoupon;
    Button btnCertainPay;
    RelativeLayout rlSettlement;

    private List<CouponBean> couponBeans;
    private List<ShoppingCartBean.ShopCartListBean> orderInfoBeans;
    private OrderInfoAdapter orderInfoAdapter;
    private double totalPrice;
    private double discountPrice;
    private String payType;
    private String couponCode;

    private String shopCarIdStr = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_submit;
    }

    @Override
    protected OrderSubmitPresenter createPresenter() {
        return new OrderSubmitPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入OrderSubmitActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        rcvCourse = (RecyclerView) findViewById(R.id.rcv_course);
        rgPayment = (RadioGroup) findViewById(R.id.rg_payment);
        rbAlipay = (RadioButton) findViewById(R.id.rb_alipay);
        rbWxpay = (RadioButton) findViewById(R.id.rb_wxpay);
        tvCoupon = (TextView) findViewById(R.id.tv_coupon);
        tvOrderAmount = (TextView) findViewById(R.id.tv_order_amount);
        tvDiscountAmount = (TextView) findViewById(R.id.tv_discount_amount);
        rlSettlement = (RelativeLayout) findViewById(R.id.rl_settlement);
        tvActualPayment = (TextView) findViewById(R.id.tv_actual_payment);
        btnCertainPay = (Button) findViewById(R.id.btn_certain_pay);

        tvCoupon.setOnClickListener(this);
        btnCertainPay.setOnClickListener(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initData();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        orderInfoBeans = new ArrayList<>();
        couponBeans = new ArrayList<>();
        payType = PayType.ALIPAY;
        //通过课程详情或购物车传过来的课程数据
        shopCarIdStr = getIntent().getStringExtra("goods");
    }

    private void initView() {
        rgPayment.setOnCheckedChangeListener(this);
        orderInfoAdapter = new OrderInfoAdapter(mContext, orderInfoBeans);
        mPresenter.queryShoppingCart(shopCarIdStr);//获取购物车数据
        rcvCourse.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 1));
        rcvCourse.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rcvCourse.setAdapter(orderInfoAdapter);
        Drawable wxDrawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_share_wx);
        wxDrawable.setBounds(0, 0, ConvertUtils.dp2px(mContext, 44), ConvertUtils.dp2px(mContext, 44));//必须设置图片大小，否则不显示
        rbWxpay.setCompoundDrawables(wxDrawable, null, null, null);

        Drawable aliDrawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_alipay);
        aliDrawable.setBounds(0, 0, ConvertUtils.dp2px(mContext, 44), ConvertUtils.dp2px(mContext, 44));//必须设置图片大小，否则不显示
        rbAlipay.setCompoundDrawables(aliDrawable, null, null, null);


        String discount = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
        tvDiscountAmount.setText(splitString("优惠金额：", discount));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            CouponBean couponBean = data.getParcelableExtra("coupon");
            this.couponCode = couponBean.getCouponCode();
            String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
            tvOrderAmount.setText(splitString("订单金额：", orderPrice));
            discountPrice = couponBean.getAmount();
            String couponPrice = String.format(Locale.getDefault(), "￥%.2f", discountPrice);
            tvDiscountAmount.setText(splitString("优惠金额：", couponPrice));
            tvCoupon.setText(String.format("-%s", couponPrice));
            double price = totalPrice - discountPrice;
            if (price < 0) {
                price = 0;
            }
            String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
            tvActualPayment.setText(splitString("实付款：", actualPrice));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart(shopCarIdStr);
        if (orderPayEvent.isSuccess()) {
            new SweetAlertDialog.Builder(mContext).setMessage("支付成功")
                    .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                        @Override
                        public void onClick(Dialog dialog, int which) {
                            finish();
                        }
                    }).show();
        } else {
            finish();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

        if (R.id.rb_wxpay == checkedId) {
            payType = PayType.WXPAY;
        } else if (R.id.rb_alipay == checkedId) {
            payType = PayType.ALIPAY;
        }
    }

    private Map<String, String> getOrderParams() {
        Map<String, String> params = new HashMap<>();
//        params.put("reqIp", NetworkUtils.getIPAddress(true));//请求ip地址
//        params.put("orderType", "COURSE");//订单类型（COURSE课程、MEMBER会员、ACCOUNT账户充值）
//        params.put("payType", payType);//支付方式，可选值： ALIPAY,支付宝；WEIXIN,微信
//        if (!TextUtils.isEmpty(couponCode)) {
//            params.put("couponCode", couponCode);//优惠券编码
//        }
        params.put("couponPrice", "0");
        params.put("couponCode", "");
        params.put("ids", shopCarIdStr);
        return params;
    }

    private SpannableString splitString(String firstStr, String secStr) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textPrimary)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    @Override
    public void queryCartSuccess(ShoppingCartBean shoppingCartBeen) {
        orderInfoBeans.addAll(shoppingCartBeen.getShopcartList());
        this.totalPrice = shoppingCartBeen.getTotalPrice();
        String orderPrice = String.format(Locale.getDefault(), "￥%.2f", totalPrice);
        tvOrderAmount.setText(splitString("订单金额：", orderPrice));
        double price = totalPrice - discountPrice;
        if (price < 0) {
            price = 0;
        }
        String actualPrice = String.format(Locale.getDefault(), "￥%.2f", price);
        tvActualPayment.setText(splitString("实付款：", actualPrice));
        orderInfoAdapter.notifyDataSetChanged();
        if (shoppingCartBeen.getCouponCodeList().isEmpty()) {
            tvCoupon.setText("暂无可用优惠券");
        } else {
            tvCoupon.setText("有可用优惠券");
            couponBeans.clear();
            couponBeans.addAll(shoppingCartBeen.getCouponCodeList());
        }
    }

    @Override
    public void getPayInfoSuccess(PayInfoBean payInfoBean) {
        if (PayType.WXPAY.equals(payInfoBean.getPayType())) {
            WXPayManager wxPayManager = new WXPayManager(mContext);
            wxPayManager.doStartWXPayPlugin(payInfoBean.getWxpayParams());
        } else {
            AlipayManager alipayManager = new AlipayManager(mContext);
            alipayManager.doStartALiPayPlugin(payInfoBean.getAlipayOrderStr());
        }
    }

    @Override
    public void getPayInfoFailure(String msg) {
        ToastUtils.showLong(mContext, msg);
    }

    @Override
    public void queryCartEmpty() {
        ToastUtils.showShort("课程获取失败，请返回重试");
    }

    @Override
    public void queryCartFailure(String msg) {
        ToastUtils.showShort("课程获取失败，请返回重试");
    }

    @Override
    public void submitOrderSuccess() {

    }

    @Override
    public void submitOrderFailure(String msg) {
        ToastUtils.showShort("课程购买失败，请稍后重试");
    }


    @Override
    public void onClick(View view) {
        if (R.id.btn_certain_pay == view.getId()) {
            if (TextUtils.isEmpty(payType)) {
                ToastUtils.showShort("请选择支付方式后结算");
                return;
            }
            if (totalPrice - discountPrice <= 0) {
                ToastUtils.showShort("结算价格不能为负");
                return;
            }
            if (TextUtils.isEmpty(shopCarIdStr)) {
                ToastUtils.showShort("数据错误，请重新选择支付课程");
                return;
            }
            mPresenter.submitOrder(getOrderParams(), payType);
        } else if (R.id.tv_coupon == view.getId()) {
            if (!couponBeans.isEmpty()) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CouponActivity");
                startIntent.putExtra("titleName", "选择优惠券");
                startIntent.putParcelableArrayListExtra("coupons", (ArrayList<? extends Parcelable>) couponBeans);
                startActivity(startIntent);
            }

        }
    }
}
