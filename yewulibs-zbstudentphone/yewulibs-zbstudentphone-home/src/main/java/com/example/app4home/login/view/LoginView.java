package com.example.app4home.login.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.AccountBean;
import com.sdzn.variants.bean.UserBean;
import com.sdzn.variants.bean.UserLoginBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface LoginView extends BaseView {

    void loginSuccess(UserLoginBean accountBean);

    void loginFailure(String msg);

//    void updateVersion(String updateInfo, String targetUrl);

    void getCodeSuccess();

    void getCodeFailure(String msg);


}
