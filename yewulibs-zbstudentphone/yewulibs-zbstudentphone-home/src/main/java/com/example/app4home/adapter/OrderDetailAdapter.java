package com.example.app4home.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.example.app4home.R;
import com.sdzn.variants.bean.CourseList;

import java.util.List;

/**
 * 描述：
 */
public class OrderDetailAdapter extends BaseRcvAdapter<CourseList> {
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";

    public OrderDetailAdapter(Context context, List mList) {
        super(context, R.layout.item_order_info, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, CourseList goodsBean) {

        holder.setImageView(R.id.iv_cover, "" + goodsBean.getLogo());
        holder.setText(R.id.tv_recmd_title, goodsBean.getCourseName());
        holder.setText(R.id.tv_recmd_price, "￥" + goodsBean.getCurrentPrice());

        if(goodsBean.getSellType()==null||goodsBean.getSellType().isEmpty()){
            return;
        }
        if (TYPE_PACKAGE.equals(goodsBean.getSellType())) {
            switch (goodsBean.getPackageType()) {
                case 1://LIVING
                    holder.setText(R.id.tv_status,"直");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);

                    break;
                case 2://VIDEO
                    holder.setText(R.id.tv_status,"点");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
                      break;
                default:
                    break;
            }
        }else if (TYPE_LIVE.equals(goodsBean.getSellType())){//LIVING
            holder.setText(R.id.tv_status,"直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);

        }else if (TYPE_COURSE.equals(goodsBean.getSellType())){//VIDEO
            holder.setText(R.id.tv_status,"点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);

        }
    }
}
