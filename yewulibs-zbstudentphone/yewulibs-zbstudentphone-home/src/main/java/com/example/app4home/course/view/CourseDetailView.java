package com.example.app4home.course.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.CourseDetailBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/15
 */
public interface CourseDetailView extends BaseView {

    void getCourseDetailSuccess(CourseDetailBean courseDetailBean);

    void getCourseDetailFailure(String msg);

}
