package com.example.app4home.course.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.app4public.service.DownLoadDataService;
import com.sdzn.core.base.BaseMVPActivity1;
import com.example.app4home.R;
import com.sdzn.variants.bean.CourseDetailBean;
import com.example.app4public.event.OrderPayEvent;
import com.example.app4public.event.UpdateAccountEvent;
import com.example.app4home.course.fragment.PurchasedCourseDetailFragment;
import com.example.app4public.manager.constant.CourseCons;
import com.example.app4home.course.presenter.CourseDetailPresenter;
import com.example.app4home.course.view.CourseDetailView;

import com.example.app4public.utils.PermissionUtils;
import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 描述：
 * - 课程详情（包含可购买课程详情和未购买课程详情）
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class CourseDetailActivity extends BaseMVPActivity1<CourseDetailView, CourseDetailPresenter> implements CourseDetailView {

    TitleBar titleBar;
    EmptyLayout emptyLayout;

    private int courseType;
    private int courseId;
    private boolean isShowLiveBtn;
    //    private boolean isFree;
    private CourseDetailBean courseDetailBean;
    private PurchasedCourseDetailFragment purchasedCourseDetailFragment; //所有课程

    public static final String COURSE_TYPE = "courseType";
    public static final String COURSE_ID = "courseId";
    public static final String SHOW_LIVE_BTN = "showLiveBtn";
    public static final String PACKAGE = "PACKAGE";
    private String packAgeType;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_detail;
    }

    @Override
    protected CourseDetailPresenter createPresenter() {
        return new CourseDetailPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    com.blankj.utilcode.util.ToastUtils.showLong("进入CourseDetailActivity成功" );
                }
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_bar);
        emptyLayout = (EmptyLayout) findViewById(R.id.empty_layout);
        EventBus.getDefault().register(this);
        initData();
        initView();
        loadNetData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IS_SUCCESS) {
            //成功购买
            if (PACKAGE.equals(packAgeType)) {
                mPresenter.getCoursePackageDetailData(courseId);
            } else {
                mPresenter.getCourseDetaiNormal(courseId);
            }
            IS_SUCCESS = false;
        }
    }

    private void initData() {
        DownLoadDataService.startDownloadService();
        packAgeType = getIntent().getStringExtra(PACKAGE);
        courseId = getIntent().getIntExtra(COURSE_ID, -1);
        isShowLiveBtn = getIntent().getBooleanExtra(SHOW_LIVE_BTN, false);

    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSharePop();
            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNetData();
            }
        });
    }

    private void loadNetData() {
        if (PACKAGE.equals(packAgeType)) {
            mPresenter.getCoursePackageDetailData(courseId);
        } else {
            mPresenter.getCourseDetaiNormal(courseId);
        }
    }

    /**
     * 分享
     */
    private void showSharePop() {
    }

    @Override
    public void getCourseDetailSuccess(CourseDetailBean courseDetailBean) {
        this.courseDetailBean = courseDetailBean;
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        if ("PACKAGE".equals(courseDetailBean.getSellType())) {
            if ("1".equals(courseDetailBean.getPackageType())) {
                courseType = CourseCons.Type.LIVING;
            } else if ("2".equals(courseDetailBean.getPackageType())) {
                courseType = CourseCons.Type.VIDEO;
            }
        } else if ("LIVE".equals(courseDetailBean.getSellType())) {
            courseType = CourseCons.Type.LIVING;
        } else if ("COURSE".equals(courseDetailBean.getSellType())) {
            courseType = CourseCons.Type.VIDEO;
        }
        if (courseDetailBean.isRelationLiveCourse()) {
            courseType = CourseCons.Type.LIVING;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        int state = 0;// 默认 收费.点播.未购买
        int isFree = 1;//收费
        int type = 2;//点播
        if ((courseDetailBean.getCurrentPrice() >= -0.000001) && (courseDetailBean.getCurrentPrice() <= 0.000001)) {//是否免费
            isFree = 0;
        } else if (courseDetailBean.isPurchase()) {//若收费, 是否已购买
            state = state | CourseCons.State.IS_PURCHASE;
        }
        if (courseType == CourseCons.Type.LIVING) {//是否直播
            type = 1;
        }
        purchasedCourseDetailFragment = PurchasedCourseDetailFragment.newInstance(courseId, state, isFree, type);//
        fragmentTransaction.replace(R.id.fl_container, purchasedCourseDetailFragment);
        fragmentTransaction.commit();
    }

    public CourseDetailBean getCourseDetails() {
        return courseDetailBean;
    }

    @Override
    public void getCourseDetailFailure(String msg) {
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        emptyLayout.setErrorMessage(msg);
    }


    /**
     * 分享中的权限请求回调
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionUtils.checkPermissionResult(mContext, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean IS_SUCCESS = false;

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        if (orderPayEvent.isSuccess()) {
            IS_SUCCESS = true;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {
            IS_SUCCESS = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
