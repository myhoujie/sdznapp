package com.example.app4home.login.presenter;

import android.text.TextUtils;

import com.blankj.utilcode.util.AppUtils;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.subscriber.LoginProgressSubscriber;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4home.R;
import com.example.app4home.login.view.LoginView;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.UserLoginBean;
import com.sdzn.variants.bean.VersionInfoBean;
import com.sdzn.variants.network.api.AccountService;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;

import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class LoginPresenter extends BasePresenter<LoginView> {
/*
    public void checkVerion() {
        Subscription subscribe = RestApi.getInstance()
                .createNew(AccountService.class)
                .queryVersion(0)
                .compose(TransformUtils.<ResultBean<VersionInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<VersionInfoBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfoBean>() {
                    @Override
                    public void onNext(VersionInfoBean versionInfoBean) {
                        int currVersion = AppUtils.getAppVersionCode();
//                        VersionInfoBean versionInfo = versionInfoBean.getVersionInfo();
                        if (versionInfoBean.getVersionNumber() > currVersion) {//此处为
                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        e.printStackTrace();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);






    }
*/
    public void login(final String username, String password,String imei) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .login(username,password,imei,"1")//"安卓手机端"  1    "安卓pad端" 2
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .onErrorResumeNext(new ExceptionFunc<UserLoginBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean userBean) {
//                        userBean.setAccount(username);
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

//        Subscription subscribe = RestApi.getInstance()
//                .create(AccountService.class)
//                .login(username, password, PriceUtil.isPad())
//                .compose(TransformUtils.<ResultBean<AccountBean>>defaultSchedulers())
//                .map(new ResponseFunc<AccountBean>())
//                .onErrorResumeNext(new ExceptionFunc<AccountBean>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<AccountBean>() {
//                    @Override
//                    public void onNext(AccountBean accountBean) {
//
//                        accountBean.getUserBean().setAccount(username);
//                        getView().loginSuccess(accountBean);
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        String msg = mActivity.getString(R.string.request_failure_try_again);
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
//                        getView().loginFailure(msg);
//                    }
//                }, mActivity, true, "正在登录..."));
//        addSubscribe(subscribe);
    }

    public void getVerifyCode(String phoneNo) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSendVerify(phoneNo)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object obj) {
                        getView().getCodeSuccess();
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getCodeFailure(msg);
                    }
                }, mActivity, true));
        addSubscribe(subscribe);
    }
    /**
     *
     */
    public void loginCode(final String phone, String code,String imei) {

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .loginCode(phone,code,imei,"1")//"安卓手机端"  1    "安卓pad端" 2
                .compose(TransformUtils.<ResultBean<UserLoginBean>>defaultSchedulers())
                .map(new ResponseNewFunc<UserLoginBean>())
                .onErrorResumeNext(new ExceptionFunc<UserLoginBean>())
                .subscribe(new LoginProgressSubscriber<>(new SubscriberOnNextListener<UserLoginBean>() {
                    @Override
                    public void onNext(UserLoginBean userBean) {
                        getView().loginSuccess(userBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().loginFailure(msg);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }
}
