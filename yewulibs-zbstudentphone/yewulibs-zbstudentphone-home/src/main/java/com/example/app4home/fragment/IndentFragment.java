package com.example.app4home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.AppUtils;
import com.example.app4home.WXPayManager;
import com.example.app4public.event.OrderPayEvent;
import com.example.app4public.manager.AlipayManager;

import com.example.app4public.manager.constant.PayType;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.core.widget.SweetAlertDialog;
import com.example.app4home.R;
import com.example.app4home.adapter.IndentAdapter;
import com.example.app4home.mvp.presenter.IndentPresenter;
import com.example.app4home.mvp.view.IndentView;
import com.example.app4public.widget.EmptyLayout;
import com.sdzn.variants.bean.IndentBean;
import com.sdzn.variants.bean.IndentResultBean;
import com.sdzn.variants.bean.PayInfoBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * 描述：订单列表
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class IndentFragment extends BaseMVPFragment<IndentView, IndentPresenter> implements IndentView, OnRefreshLoadmoreListener {
    public static final String ARGS_TYPE = "args_states";
    RecyclerView recyclerIndent;
    SmartRefreshLayout swipLayout;
    EmptyLayout emptyLayout;
    private IndentAdapter indentAdapter;
    private List<IndentBean> mData = new ArrayList<>();
    private String state;
    private int pageIndex = 1;
    private int pageSize = 10;
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";

    public static IndentFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString(ARGS_TYPE, type);
        IndentFragment fragment = new IndentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            state = getArguments().getString(ARGS_TYPE);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_indent;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        swipLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        recyclerIndent = (RecyclerView) rootView.findViewById(R.id.swipe_target);
        emptyLayout = (EmptyLayout) rootView.findViewById(R.id.empty_layout);
        initView();
//        initData();
    }

    public void regEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            return;
        }
        EventBus.getDefault().register(this);
    }

    public void unRegEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void lazyLoad() {
        initData();
        isFirst = false;
    }

    private void initData() {
        mPresenter.getIndentList(state, pageIndex, pageSize);
    }

    private void initView() {
        indentAdapter = new IndentAdapter(mContext, mData);
        recyclerIndent.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray, null), 0));
        recyclerIndent.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        recyclerIndent.setAdapter(indentAdapter);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });
        indentAdapter.setOnItemViewListener(new IndentAdapter.OnItemViewListener() {
            @Override
            public void onItemClick(IndentBean indentBean, IndentBean.TrxorderDetailListBean trxorderDetailListBean, int position) {

                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.OrderDetailActivity");
                startIntent.putExtra("to_in_order", indentBean.getOrderId());
                startActivity(startIntent);

            }

            @Override
            public void refundClick(IndentBean indentBean) {
                //退款申请的界面
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.ReimburseActivity");
                startIntent.putExtra("orderId", indentBean.getOrderId());
                startActivity(startIntent);
            }
        });

        indentAdapter.setOnBuyListener(new IndentAdapter.OnBuyListener() {
            @Override
            public void buyClick(IndentBean indentBean, String payType) {//支付类型

                mPresenter.toBuyIndent(indentBean, payType);
            }

        });
        indentAdapter.setOnCancelListener(new IndentAdapter.OnCancelListener() {
            @Override
            public void cancelIndent(IndentBean indentBean) {
                mPresenter.toCancelIndent(indentBean);
            }
        });
        //
        indentAdapter.setOnCancelApplyListener(new IndentAdapter.OnCanceApplylListener() {
            @Override
            public void cancelCourse(IndentBean indentBean) {
                mPresenter.toCancelCourse(indentBean);

            }
        });
        swipLayout.setOnRefreshLoadmoreListener(this);
    }


    @Override
    protected IndentPresenter createPresenter() {
        return new IndentPresenter();
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        mPresenter.getIndentList(state, pageIndex, pageSize);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        mPresenter.getIndentList(state, pageIndex, pageSize);
    }

    @Override
    public void listindents(IndentResultBean bean) {
        if (bean.getRows() != null && !bean.getRows().isEmpty()) {
            if (pageIndex == 1) {
                mData.clear();
            }
            mData.addAll(bean.getRows());
            indentAdapter.getmBooleanMap().clear();
            indentAdapter.notifyDataSetChanged();
            swipLayout.setLoadmoreFinished(bean.getRows().size() < pageSize);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            if (pageIndex == 1) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多订单了");
                swipLayout.setLoadmoreFinished(true);
            }
        }

        goneSwipView();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        }
        goneSwipView();
    }

    @Override
    public void getPayInfoSuccess(PayInfoBean payInfoBean) {
        if (PayType.WXPAY.equals(payInfoBean.getPayType())) {
            WXPayManager wxPayManager = new WXPayManager(mContext);
            wxPayManager.doStartWXPayPlugin(payInfoBean.getWxpayParams());
        } else {
            AlipayManager alipayManager = new AlipayManager(mContext);
            alipayManager.doStartALiPayPlugin(payInfoBean.getAlipayOrderStr());
        }
    }

    @Override
    public void getPayInfoFailure() {
        ToastUtils.showShort("支付失败，请稍候重试");
    }

    @Override
    public void cancelSuccess(IndentBean indentBean) {
        pageIndex = 1;
        mPresenter.getIndentList(state, pageIndex, pageSize);
//        if (mData.contains(indentBean)) {
//            mData.remove(indentBean);
//            indentAdapter.notifyDataSetChanged();
//        }

    }

    @Override
    public void cancelError(String msg) {
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        pageIndex = 1;
        mPresenter.getIndentList(state, pageIndex, pageSize);
        if (orderPayEvent.isSuccess()) {
            SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
            builder.setMessage("支付成功");
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
            sweetAlertDialog.show();
        }
    }


    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (swipLayout.isRefreshing()) {
            swipLayout.finishRefresh();
        }
        if (swipLayout.isLoading()) {
            swipLayout.finishLoadmore();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
