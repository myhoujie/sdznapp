package com.example.app4home.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.SubjectBean;

import java.util.List;

public interface SpellingClassView extends BaseView {
    void onSubjectSuccess(List<SubjectBean> subjectList);

    void onSubjectFailed(String msg);
}
