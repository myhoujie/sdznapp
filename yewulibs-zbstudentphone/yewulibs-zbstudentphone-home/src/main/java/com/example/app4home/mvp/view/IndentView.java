package com.example.app4home.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.IndentBean;
import com.sdzn.variants.bean.IndentResultBean;
import com.sdzn.variants.bean.PayInfoBean;

/**
 * 描述：订单
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public interface IndentView extends BaseView {
    void listindents(IndentResultBean bean);

    void onError(String msg);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void getPayInfoFailure();

    void cancelSuccess(IndentBean indentBean);

    void cancelError(String msg);

}
