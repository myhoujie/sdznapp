package com.example.app4home.mvp.presenter;

import com.google.gson.Gson;
import com.sdzn.variants.network.RestApi;
import com.sdzn.variants.network.SPToken;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.example.app4public.manager.SPManager;
import com.example.app4home.mvp.view.CourseView;
import com.sdzn.variants.bean.CourseListRows;
import com.sdzn.variants.bean.ResultBean;
import com.sdzn.variants.bean.SubjectBean;
import com.sdzn.variants.network.api.CourseService;
import com.sdzn.variants.network.api.ResponseNewFunc;
import com.sdzn.variants.network.api.ResponseNewSchoolFunc;
import com.sdzn.variants.network.subscriber.MProgressSubscriber;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/11
 */
public class CoursePresenter extends BasePresenter<CourseView> {

    /**
     * 获取学科列表
     */

    public void getSubject(int sectionId) {
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(sectionId)
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<List<SubjectBean>>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().getSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {

                        getView().getSubjectEmpty();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 获取 年级
     */
//    public void getGrade() {
//        Subscription subscribe = RestApi.getInstance()
//                .createNew(CourseService.class)
//                .getGradeJson()
//                .compose(TransformUtils.<ResultBean<List<GradeJson>>>defaultSchedulers())
//                .map(new ResponseNewFunc<>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<GradeJson>>() {
//                    @Override
//                    public void onNext(List<GradeJson> gradeJson) {
//                        if (gradeJson!=null){
//                            getView().onGradeSuccess(gradeJson);
//                        }
//
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        getView().onGradeEmpty();
//                    }
//                }, mActivity, false));
//        addSubscribe(subscribe);
//
//    }

    /**
     * 获取 课程
     */

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCourseSchool(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListRows>() {
                    @Override
                    public void onNext(CourseListRows courses) {
                        if (courses != null) {
                            getView().getCourseSuccess(courses.getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        if (!SPToken.autoLogin(mActivity) || "40002".equals(e.getMessage())) {
                            getView().getToLoginEmpty();
                        } else if (SPManager.isToCLogin()) {//java.lang.Throwable: 请登录学校下发的学生账号查看！
                            getView().getTocFailedEmpty();
                        } else {
                            getView().getTobFailedEmpty();
                        }
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }


}
