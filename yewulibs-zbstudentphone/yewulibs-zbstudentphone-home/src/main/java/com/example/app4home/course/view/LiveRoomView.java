package com.example.app4home.course.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.variants.bean.NewLiveInfo;
import com.sdzn.variants.bean.NewVideoInfo;

/**
 * 描述：直播的和观看回放
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/7
 */

public interface LiveRoomView extends BaseView {

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void liveRoomInfoOnError(String msg);

    void getVideoRoomInfoSuccrss(NewVideoInfo videoRoomBean);

    void videoRoomInfoOnError(String msg);

    void getReplayInfoSuccess(NewVideoInfo info);

    void applySuccess(int type);


}
