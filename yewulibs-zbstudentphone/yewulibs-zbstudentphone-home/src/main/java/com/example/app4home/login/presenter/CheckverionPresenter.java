package com.example.app4home.login.presenter;

import com.example.app4home.login.view.CheckverionView;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;
import com.sdzn.variants.bean.VersionInfoBean;
import com.sdzn.variants.network.api.AccountService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class CheckverionPresenter extends Presenter<CheckverionView> {

    public void checkVerion(String programId, String type) {
        RetrofitNetNew.build(AccountService.class, getIdentifier())
                .queryVersion(programId,type)
                .enqueue(new Callback<ResponseSlbBean1<VersionInfoBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<VersionInfoBean>> call, Response<ResponseSlbBean1<VersionInfoBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                       /* if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().OnUpdateVersionNodata(response.body().getMsg());
                            return;
                        }*/
                        getView().OnUpdateVersionSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<VersionInfoBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().OnUpdateVersionFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
