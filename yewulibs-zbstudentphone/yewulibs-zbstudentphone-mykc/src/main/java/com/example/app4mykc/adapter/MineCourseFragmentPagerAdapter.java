package com.example.app4mykc.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.example.app4mykc.fragment.CoursePageFragment;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class MineCourseFragmentPagerAdapter extends FragmentPagerAdapter {
    private String[] titles = new String[]{"今日直播", "近期学习", "全部课程", "过期课程"};
    private Context context;
    private List<CoursePageFragment> mData;

    public MineCourseFragmentPagerAdapter(FragmentManager fm, List<CoursePageFragment> list, Context context) {
        super(fm);
        this.context = context;
        this.mData = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
