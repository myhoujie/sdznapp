package com.example.app4mykc.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baijiayun.live.ui.LiveSDKWithUI;
import com.baijiayun.livecore.LiveSDK;
import com.baijiayun.livecore.context.LPConstants;
import com.baijiayun.videoplayer.ui.playback.PBRoomUI;
import com.blankj.utilcode.util.AppUtils;
import com.example.app4mykc.R;
import com.example.app4mykc.adapter.MineCourseAdapter;
import com.example.app4mykc.adapter.MineRecentlyCourseAdapter;
import com.example.app4mykc.adapter.MineTodyCourseAdapter;
import com.example.app4mykc.presenter.MineCoursePresenter;
import com.example.app4mykc.view.MineCourseView;
import com.sdzn.variants.network.SPToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;

import com.sdzn.variants.bean.MineList;
import com.sdzn.variants.bean.NewLiveInfo;
import com.sdzn.variants.bean.NewVideoInfo;
import com.example.app4public.event.MineCourseEvent;
import com.example.app4public.event.OrderPayEvent;
import com.example.app4public.event.UpdateAccountEvent;

import com.example.app4public.widget.EmptyLayout;
import com.example.app4public.widget.EmptySchoolLayout;
import com.example.app4public.widget.SimpleDividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 描述：我的课程中单个模块
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class CoursePageFragment extends BaseMVPFragment<MineCourseView, MineCoursePresenter> implements MineCourseView, BaseRcvAdapter.OnItemClickListener, OnRefreshLoadmoreListener {
    public static final String ARGS_TYPE = "args_page";
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";
    public static final String TYPE_COURSE = "303";
    public static final String TYPE_COURSE_OVER = "304";

    EmptyLayout emptyLayout;
    RecyclerView recyclerView;
    SmartRefreshLayout refreshLayout;
    private MineCourseAdapter mineCourseAdapter;
    private List<MineList> mDataMyCourse = new ArrayList<>();
    private MineRecentlyCourseAdapter recentlyCourseAdapter;
    private List<MineList> mDataRecentlyCourse = new ArrayList<>();
    private MineTodyCourseAdapter todyCourseAdapter;
    private List<MineList> mDataTodyCourse = new ArrayList<>();
    private String state;
    private int pageIndex = 1;//当前页
    private int pageSize = 10;//

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            state = getArguments().getString(ARGS_TYPE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        if (orderPayEvent.isSuccess()) {
            pageIndex = 1;
            initData();
        }
    }

    public static CoursePageFragment newInstance(String page) {
        Bundle args = new Bundle();
        args.putString(ARGS_TYPE, page);
        CoursePageFragment fragment = new CoursePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_page;

    }

    @Override
    protected MineCoursePresenter createPresenter() {
        return new MineCoursePresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        refreshLayout = (SmartRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.swipe_target);
        emptyLayout = (EmptyLayout) rootView.findViewById(R.id.empty_layout);
        initView();
    }

    @Override
    protected void lazyLoad() {
        initData();
        isFirst = false;
    }

    private Map<String, String> getParms() {
        Map<String, String> params = new HashMap<>();
        if (state.equals(TYPE_COURSE) || state.equals(TYPE_COURSE_OVER)) {
            params.put("sellType", "ALL");
            if (state.equals(TYPE_COURSE)) {
                params.put("type", "2");
            } else if (state.equals(TYPE_COURSE_OVER)) {//过期  1    全部课程 2
                params.put("type", "1");

            }
        }
        params.put("index", String.valueOf(pageIndex));
        params.put("size", String.valueOf(pageSize));

        return params;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirst && TYPE_TODY.equalsIgnoreCase(state)) {
            mPresenter.getTodyCourse(getParms());
        }
    }

    private void initData() {
        if (SPToken.autoLogin(mContext)) {
            if (TYPE_TODY.equals(state)) {
                mPresenter.getTodyCourse(getParms());
            } else if (TYPE_RECENTLY.equals(state)) {
                mPresenter.getRecentlyCourse(getParms());
            } else if (TYPE_COURSE.equals(state)) {
                mPresenter.getCourse(getParms(), 2);
            } else if (TYPE_COURSE_OVER.equals(state)) {
                mPresenter.getCourse(getParms(), 1);
            } else {

            }

        }

    }

    private void initView() {
        if (TYPE_TODY.equals(state)) {
            todyCourseAdapter = new MineTodyCourseAdapter(mContext, mDataTodyCourse);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
            recyclerView.setAdapter(todyCourseAdapter);
//                todyCourseAdapter.setOnItemClickListener(this);
            todyCourseAdapter.setListener(new MineTodyCourseAdapter.LivePlayerListener() {
                @Override
                public void ontoDetailLive(String status, String kpoint, String courseId) {
                    if ("4".equals(status)) {
                        mPresenter.getLivingInfo(kpoint);
                    } else if ("6".equals(status)) {
                        mPresenter.getReplayInfo(kpoint, courseId);
                    }
                }
            });
            todyCourseAdapter.setOnItemClickListener(this);
        } else if (TYPE_RECENTLY.equals(state)) {
            recentlyCourseAdapter = new MineRecentlyCourseAdapter(mContext, mDataRecentlyCourse);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
            recyclerView.setAdapter(recentlyCourseAdapter);
            recentlyCourseAdapter.setOnItemClickListener(this);
        } else if (TYPE_COURSE.equals(state)) {
            mineCourseAdapter = new MineCourseAdapter(mContext, mDataMyCourse, 2);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//        recyclerVideo.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
            recyclerView.setAdapter(mineCourseAdapter);
            mineCourseAdapter.setOnItemClickListener(this);
        } else if (TYPE_COURSE_OVER.equals(state)) {
            mineCourseAdapter = new MineCourseAdapter(mContext, mDataMyCourse, 1);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
            recyclerView.setAdapter(mineCourseAdapter);
//                mineCourseAdapter.setOnItemClickListener(this);

        }

        refreshLayout.setOnRefreshLoadmoreListener(this);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lazyLoad();
            }
        });
    }


    @Override
    public void onItemClick(View view, int position) {
        //判断当前的item是点播还是直播的
        if (TYPE_TODY.equals(state)) {
            MineList mineListToday = mDataTodyCourse.get(position);
//                直接去直播
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
            startIntent.putExtra("PACKAGE", "COURSE");
            startIntent.putExtra("courseId", mineListToday.getCourseId());
            startIntent.putExtra("showLiveBtn", true);
            startActivity(startIntent);
        } else if (TYPE_RECENTLY.equals(state)) {
            MineList mineListRecent = mDataRecentlyCourse.get(position);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
            startIntent.putExtra("PACKAGE", mineListRecent.getCourseType());
            startIntent.putExtra("courseId", mineListRecent.getCourseId());
            startIntent.putExtra("showLiveBtn", true);
            startActivity(startIntent);
        } else if (TYPE_COURSE.equals(state)) {
            MineList mineCourseBean = mDataMyCourse.get(position);
            Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.CourseDetailActivity");
            startIntent.putExtra("PACKAGE", mineCourseBean.getCourseType());
            startIntent.putExtra("courseId", mineCourseBean.getCourseId());
            startIntent.putExtra("showLiveBtn", true);
            startActivity(startIntent);
        } else if (TYPE_COURSE_OVER.equals(state)) {
//                ToastUtils.showShort("课程失效，不能进入！");
        }


    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        lazyLoad();
    }

//    @Override
//    public void listCourseSuccess(List<MineCourseBean> list) {
//        if (list != null) {
//            if (pageIndex == 1) {
//                mData.clear();
//            }
//            mData.addAll(list);//sellType="PACKAGE", title="接口测试", packageType=1
//            mineCourseAdapter.notifyDataSetChanged();
//            refreshLayout.setLoadmoreFinished(list.size() < pageSize);
//        }
//        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//        goneSwipView();
//    }

    @Override
    public void listTodyCourseSuccess(List<MineList> list) {
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataTodyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataTodyCourse.addAll(list);
        todyCourseAdapter.notifyDataSetChanged();
    }

    @Override
    public void listRecentlyCourseSuccess(List<MineList> list) {
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataRecentlyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataRecentlyCourse.addAll(list);
        recentlyCourseAdapter.notifyDataSetChanged();

    }

    @Override
    public void listCourseSuccess(List<MineList> list, int type) {//1过期
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataMyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataMyCourse.addAll(list);
        mineCourseAdapter.notifyDataSetChanged();


    }

    @Override
    public void listCourseEmpty() {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            refreshLayout.setLoadmoreFinished(true);
        }
        clearLoingState();
    }

    @Override
    public void listCourseError(String msg) {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
        clearLoingState();
    }

    @Override
    public void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        LiveSDKWithUI.enterRoomWithVerticalTemplate(mContext, Long.valueOf(liveRoomBean.getRoom_id().trim()), liveRoomBean.getSign(), new LiveSDKWithUI.LiveRoomUserModel(liveRoomBean.getUser_info().getUserName(), liveRoomBean.getUser_info().getUserAvatar(), String.valueOf(liveRoomBean.getUser_info().getUserNumber()), LPConstants.LPUserType.Student), new LiveSDKWithUI.LiveSDKEnterRoomListener() {
            @Override
            public void onError(String msg) {

            }
        });
    }

    /**
     * 回放
     */
    @Override
    public void getReplayInfoSuccess(NewVideoInfo info) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        PBRoomUI.enterPBRoom(getActivity(), info.getRoomId(), info.getToken(), "0", new PBRoomUI.OnEnterPBRoomFailedListener() {

            @Override
            public void onEnterPBRoomFailed(String msg) {
                ToastUtils.showShort(msg);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {//刷新
            if (SPToken.autoLogin(mContext)) {
                lazyLoad();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateCourse(MineCourseEvent courseEvent) {
        if (courseEvent.isStatus()) {//刷新
            if (SPToken.autoLogin(mContext)) {
                lazyLoad();
            }
        }
    }

    //隐藏刷新布局或者底部加载更多的布局
    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }
}