package com.example.app4mykc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.example.app4mykc.R;
import com.sdzn.variants.bean.MineList;

import java.util.List;

/**
 * 描述：今日直播
 */

public class MineTodyCourseAdapter extends BaseRcvAdapter<MineList> {
    private boolean isLiving;
    private static final String LIVE = "LIVE";
    private static final String COURSE = "COURSE";
    private LivePlayerListener listener;

    public MineTodyCourseAdapter(Context context, List<MineList> mList) {
        super(context, R.layout.item_mine_course, mList);
    }

    public void setListener(LivePlayerListener listener) {
        this.listener = listener;
    }

    @Override
    public void convert(BaseViewHolder holder, int position,final MineList mineCourseBean) {
//        ((TextView) (holder.getView(R.id.tv_class))).setLines(2);
        holder.setText(R.id.tv_class, mineCourseBean.getTodyCourseName());
        holder.setImageView(R.id.img_video, "" + mineCourseBean.getLogo());

        holder.setText(R.id.tv_status,"直");
        holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
        isLiving = true;
        holder.setText(R.id.tv_video_type, "「直播」");
        holder.setTextColor(R.id.tv_video_type, context.getResources().getColor(R.color.red));


        holder.setText(R.id.tv_course_content, mineCourseBean.getCourseKpointName());

        holder.setText(R.id.tv_schedule, "讲师：" + mineCourseBean.getTeacherName());
        holder.setText(R.id.tv_date_value, "直播时间：" + TimeUtils.millis2String(TimeUtils.string2Millis(mineCourseBean.getLiveBeginTime())));

        holder.setVisible(R.id.tv_video_type, false);
        holder.setVisible(R.id.tv_liveing, false);
        holder.setVisible(R.id.tv_statue, true);
        if ("3".equals(mineCourseBean.getLiveStates())) {
            setRes(holder, "未开始", R.drawable.shape_enter_other,"#697886");
        } else if ("4".equals(mineCourseBean.getLiveStates())) {
            setRes(holder, "进入直播", R.drawable.shape_enter_live,"#ffffff");
        }else if ("5".equals(mineCourseBean.getLiveStates())) {
            setRes(holder, "已结束", R.drawable.shape_enter_other,"#697886");
        }else if ("6".equals(mineCourseBean.getLiveStates())) {
            setRes(holder, "观看回放", R.drawable.shape_enter_other,"#1A70AE");
        }else {
            holder.setVisible(R.id.tv_statue, false);
        }

        if (isLiving) {
            //区分右上角课程状态
            holder.setVisible(R.id.ll_schedule_value, false);
            holder.setVisible(R.id.tv_per, false);
            holder.setVisible(R.id.progress_schedule, false);
        }
        holder.getView(R.id.tv_statue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.ontoDetailLive(mineCourseBean.getLiveStates(), String.valueOf(mineCourseBean.getCourseKpointId()), String.valueOf(mineCourseBean.getCourseId()));
            }
        });
    }

    private void setRes(BaseViewHolder holder, String statue, int resid,String colorStr) {
        ((TextView) (holder.getView(R.id.tv_statue))).setText(statue);
        ((TextView) (holder.getView(R.id.tv_statue))).setBackgroundResource(resid);
        ((TextView) (holder.getView(R.id.tv_statue))).setTextColor(Color.parseColor(colorStr));
    }
    public interface LivePlayerListener {
        void ontoDetailLive(String status, String kpoint, String courseId);//直播
    }
}
