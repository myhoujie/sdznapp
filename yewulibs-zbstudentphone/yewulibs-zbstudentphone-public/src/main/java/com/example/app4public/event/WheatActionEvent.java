package com.example.app4public.event;

/**
 * 描述：连麦的事件
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/2
 */

public class WheatActionEvent {

    private int action;
    public final static int TYPE_WHEAT = 1;

    public WheatActionEvent(int action) {
        this.action = action;
    }
}
