package com.example.app4public.manager;

import android.content.Context;

import com.sdzn.variants.network.SPToken;
import com.just.agentweb.App2;
import com.sdzn.core.base.BaseApplication;

import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.EncodeUtils;
import com.sdzn.core.utils.SPUtils;
import com.sdzn.variants.bean.SectionBean;
import com.sdzn.variants.bean.UserBean;

import java.util.List;

/**
 * 描述：
 * - sp文件管理类
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SPManager {
    private static final String SP_NAME = "AppConfig";

    /**
     * 是否是第一次加载应用
     *
     * @param context
     * @return
     */
    public static boolean isFirst(Context context) {
        String version = (String) SPUtils.get(context, SP_NAME, "version", App2Utils.getVersion());
        boolean isFirst = (boolean) SPUtils.get(context, SP_NAME, "isfirst", true);
        if (App2Utils.getVersion().equals(version) || isFirst) {
            SPUtils.put(context, SP_NAME, "version", App2Utils.getVersion());
            SPUtils.put(context, SP_NAME, "isfirst", false);
        }
        return isFirst;
    }

    public static void setFirstEnterApp(Context context) {
        SPUtils.put(context, SP_NAME, "isfirst", true);
        SPUtils.put(context, SP_NAME, "version", "");
    }

    /**
     * 是否第一弹出 选中年级
     */
    public static boolean isFirstSubjectId(Context context) {
        String version = (String) SPUtils.get(context, SP_NAME, "version_sub", App2Utils.getVersion());
        boolean isFirst = (boolean) SPUtils.get(context, SP_NAME, "firstSub", true);
        return isFirst;
    }

    public static void setFirstSubjectId(Context context) {
        SPUtils.put(context, SP_NAME, "version_sub", App2Utils.getVersion());
        SPUtils.put(context, SP_NAME, "firstSub", false);
    }


    /**
     * 改变用户登录状态
     *
     * @param context
     * @param loginStatus
     */
    public static void changeLogin(Context context, boolean loginStatus) {
        SPUtils.put(context, SP_NAME, "islogin", loginStatus);
        if (loginStatus) {
            SPUtils.put(context, SP_NAME, "lastLoginTime", System.currentTimeMillis());
        }
        if (!loginStatus) {
            SPToken.saveToken("");
        }
    }


    /**
     * 是否登录超时
     *
     * @param context
     * @return
     */
    public static boolean isLoginOverdue(Context context) {
        long overdueTime = 1000L * 60L * 60L * 24L * 7L;
        long currentTime = System.currentTimeMillis();
        long lastLoginTime = (long) SPUtils.get(context, SP_NAME, "lastLoginTime", currentTime);
        long subTime = currentTime - lastLoginTime;//20天的登录有效期
        return subTime < overdueTime;
    }

//    public static boolean autoLogin(Context context) {
//        return isLogin() && isLoginOverdue(context);
//    }

    /**
     * 处于登录  toc
     */

    public static boolean isToCLogin() {
//        return SPManager.getUser().getUserName().isEmpty();
        return 0 == SPManager.getUser().getFzxStatus();
    }


    /**
     * 存储登录账号
     *
     * @param account
     */
    public static void saveLastLoginAccount(String account) {
        SPUtils.put(BaseApplication.getInstance(), SP_NAME, "lastLoginAccount", account);
    }

    /**
     * 获取上次登录账号
     */
    public static String getLastLoginAccount() {
        return (String) SPUtils.get(App2.get(), SP_NAME, "lastLoginAccount", "");
    }

    /**
     * 存储用户信息
     *
     * @param userBean
     */
    public static void saveUser(UserBean userBean) {
        SPUtils.putObject(BaseApplication.getInstance(), SP_NAME, "userBean", userBean);
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static UserBean getUser() {
        UserBean userBean = SPUtils.getObject(BaseApplication.getInstance(),
                SP_NAME, "userBean", UserBean.class);
        return userBean != null ? userBean : new UserBean();
    }

    /**
     * 存储密码
     *
     * @param password
     */
    public static void savePwd(String password) {
        //md5加密无法还原, 暂用base64
        String b64 = new String(EncodeUtils.base64Encode(password));
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "pwd", b64);
//        SPUtils.put(BaseApplication.getInstance(), getSpName(), "pwd", EncryptUtils.encryptMD5ToString(password));
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public static String getPwd() {
        String code = (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "pwd", "");
        return new String(EncodeUtils.base64Decode(code));
//        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "pwd", "");
    }


    /**
     * 存储历史搜索记录
     *
     * @param keywords
     */
    public static void saveSearchStr(List<String> keywords) {
        SPUtils.putList(BaseApplication.getInstance(), getSpName(), "searchHistory", keywords);
    }

    /**
     * 保存学段信息
     */
    public static void saveSection(SectionBean bean) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionId", bean.getSectionId());
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionName", bean.getSectionName());
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "eduId", bean.getEducationId());
    }

    public static void saveSection(int id, String name, int eduId) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionId", id);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionName", name);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "eduId", eduId);
    }

    /**
     * 获取学段ID, 默认为-1
     */
    public static int getSectionId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "sectionId", -1);
    }

    /**
     * 获得学制  默认为0
     */
    public static int getEduId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "eduId", 0);
    }

    /**
     * 获取学段名称默认为""
     */
    public static String getSectionName() {
        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "sectionName", "");
    }

    /**
     * 保存年级 年级名
     */
    public static void saveGrade(int gradeId, String gradeName) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "gradeId", gradeId);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "gradeName", gradeName);
    }

    /**
     * 获取年级ID, 默认为-1
     */
    public static int getgradeId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "gradeId", -1);
    }

    /**
     * 获取年级名称默认为""
     */
    public static String getgradeName() {
        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "gradeName",


                "");
    }

    /**
     * 保存学段信息 *********学校课程********* 学制
     */
    public static void saveSchoolSection(int id, String name, int
            eduId) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionSchoolId", id);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionSchoolName", name);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "eduSchoolId", eduId);
    }

    /**
     * 获取学段ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolSectionId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "sectionSchoolId", -1);
    }

    /**
     * 获取学制ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolEduId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "eduSchoolId", -1);
    }

    //    /**
//     * 获取学段名称默认为""  *********学校课程*********
//     */
    public static String getSchoolSectionName() {
        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "sectionSchoolName", "");
    }

    /**
     * 保存年级 年级名*********学校课程*********
     */
    public static void saveSchoolGrade(int gradeId, String gradeName) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "gradeSchoolId", gradeId);
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "gradeSchoolName", gradeName);
    }

    /**
     * 获取年级ID, 默认为-1 *********学校课程*********
     */
    public static int getSchoolgradeId() {
        return (int) SPUtils.get(BaseApplication.getInstance(), getSpName(), "gradeSchoolId", -1);
    }

    /**
     * 获取年级名称默认为""*********学校课程*********
     */
    public static String getSchoolgradeName() {
        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "gradeSchoolName", "");
    }

    /**
     * 学制
     */


    /**
     * 获取历史搜索记录
     *
     * @return
     */
    public static List getSearchStr() {
        return SPUtils.getList(BaseApplication.getInstance(), getSpName(), "searchHistory");
    }

    /**
     * 获取教师搜索记录
     */
    public static List getSearchTeacherStr() {
        return SPUtils.getList(BaseApplication.getInstance(), getSpName(), "searchTeacherHistory");
    }

    /**
     * 保存教师搜索记录
     */
    public static void saveSearchTeacherStr(List<String> keywords) {
        SPUtils.putList(BaseApplication.getInstance(), getSpName(), "searchTeacherHistory", keywords);
    }

    /**
     * 移动网络是否可用
     */
    public static boolean getMobileNetSwitch() {
        return (boolean) SPUtils.get(BaseApplication.getInstance(), getSpName(), "mobileNetSwitch", true);
    }

    /**
     * 移动网络是否可用
     */
    public static void changeMobileNetSwitch(boolean checked) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "mobileNetSwitch", checked);
    }


    /**
     * 获取当前用户的sp文件名
     *
     * @return
     */
    private static String getSpName() {
        UserBean userBean = getUser();
        if (userBean != null) {
            return SP_NAME + "_" + userBean.getUserId();
        } else {
            return SP_NAME;
        }
    }

    /**
     * 基本配置  其实就是年级学段
     */
    public static void setBaseConfig(String configStr) {
        SPUtils.put(BaseApplication.getInstance(), getSpName(), "gradeJson", configStr);
    }

    /**
     * 基本配置
     */
    public static String getBaseConfig() {
        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "gradeJson", "");
    }


}
