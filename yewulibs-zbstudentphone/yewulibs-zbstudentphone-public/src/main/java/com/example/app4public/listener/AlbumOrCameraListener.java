package com.example.app4public.listener;

/**
 * Created by wgk on 2017/5/3.
 */

public interface AlbumOrCameraListener {
    void selectAlbum();

    void selectCamera();
}
