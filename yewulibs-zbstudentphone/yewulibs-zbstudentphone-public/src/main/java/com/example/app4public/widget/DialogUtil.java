package com.example.app4public.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.example.app4public.R;
import com.example.app4public.listener.AlbumOrCameraListener;
import com.example.app4public.listener.WechatOrAlipayListener;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class DialogUtil {



    public static Dialog showSelectImgDialog(Activity activity, final AlbumOrCameraListener listener) {
        final Dialog dialog = new Dialog(activity, R.style.Dialog);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_album_or_camera, null);
        view.findViewById(R.id.tv_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlbum();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectCamera();
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dialog.show();
        return dialog;
    }

    public static void showExitDialog(Activity activity, final DialogInterface.OnClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage("确定要退出登录吗？");
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
                dialog.dismiss();
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 修改密码后点击重新登录
     *
     * @param activity
     * @param msg
     * @param type
     */
    public static void showReLoginDialog(final Activity activity, String msg, boolean type, final DialogInterface.OnClickListener listener) {
//        if (!AndroidUtil.getActivityOnLive(activity)) {
//            return;
//        }
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
                dialog.dismiss();
            }
        });
        final CustomDialog mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(type);
        mDialog.setCancelable(type);
        mDialog.show();
    }

    /**
     *
     * @param activity
     * @param msg
     * @param type
     */
    public static void showDialog(final Activity activity, String msg, boolean type, final DialogInterface.OnClickListener listener) {
//        if (!AndroidUtil.getActivityOnLive(activity)) {
//            return;
//        }
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
                dialog.dismiss();
            }
        }).setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final CustomDialog mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(type);
        mDialog.setCancelable(type);
        mDialog.show();
    }

    /**
     * 选择支付方式
     */
    public static Dialog showSelectPaymentDialog(Activity activity, final WechatOrAlipayListener listener) {
        final Dialog dialog = new Dialog(activity, R.style.Dialog);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_wxpay_or_alipay, null);
        view.findViewById(R.id.ll_wxpay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectWx();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.ll_alipay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.selectAlipay();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.addContentView(view, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }


}
