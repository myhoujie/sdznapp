package com.example.app4public.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.just.agentweb.App2;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.variants.bean.GradeJson;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GradeIdToNameUtils {

    public static String setName(int sectionId, int gradeId) {// 学制 学段  班级
        List<GradeJson> gradeJsons = getGrade();
        for (int i = 0; i < gradeJsons.size(); i++) {
            if (String.valueOf(sectionId).equalsIgnoreCase(gradeJsons.get(i).getLevelId())) {
                for (int j = 0; j < gradeJsons.get(i).getChildList().size(); j++) {
                    if (String.valueOf(gradeId).equalsIgnoreCase(gradeJsons.get(i).getChildList().get(j).getGradeId())) {
                        return gradeJsons.get(i).getChildList().get(j).getGradeName();
                    }
                }
            }
        }

        return "";
    }

    public static String setName(int sectionId, int gradeId,String edu) {// 学制 学段  班级
        List<GradeJson> gradeJsons = getGrade();
        for (int i = 0; i < gradeJsons.size(); i++) {
            if (String.valueOf(sectionId).equalsIgnoreCase(gradeJsons.get(i).getLevelId())) {
                for (int j = 0; j < gradeJsons.get(i).getChildList().size(); j++) {
                    if (String.valueOf(gradeId).equalsIgnoreCase(gradeJsons.get(i).getChildList().get(j).getGradeId())) {
                        return gradeJsons.get(i).getChildList().get(j).getGradeName();
                    }
                }
            }
        }

        return "全部年级";
    }

    /**
     * 获取年级
     */
    public static List<GradeJson> getGrade() {
        try {
            InputStream open = App2.get().getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }

    /**
     * 获取六三年级
     */
    public static List<GradeJson> getSixGrade() {
        try {
            InputStream open = App2.get().getResources().getAssets().open("gradesix.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }

    /**
     * 获取五四年级
     */
    public static List<GradeJson> getFiveGrade() {
        try {
            InputStream open = App2.get().getResources().getAssets().open("gradefive.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }

}
