package com.example.app4public.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */
public final class FileUtil {

    /**
     * 拷贝文件
     *
     * @param srcPath
     * @param targetPath
     * @throws IOException
     */
    public static void copyFile(String srcPath, String targetPath)
            throws IOException {
        File srcFile = new File(srcPath);
        File targetFile = new File(targetPath);
        FileInputStream is = new FileInputStream(srcFile);
        FileOutputStream fos = new FileOutputStream(targetFile);
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = is.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }
        fos.close();
        is.close();
    }

}
