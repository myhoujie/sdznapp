package com.example.app4public.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.example.app4public.R;


/**
 * 描述：
 * - 圆角矩形ImageView（可以设置单独设置四个角的半径）
 * 创建人：baoshengxiang
 * 创建时间：2017/7/12
 */
public class RoundRectImageView extends AppCompatImageView {
    private float[] rids;
    private Path path;
    private RectF rect;
    private Context mContext;
    private float radius;
    private float leftTopRadius;
    private float leftBottomRadius;
    private float rightTopRadius;
    private float rightBottomRadius;


    public RoundRectImageView(Context context) {
        this(context, null);
    }


    public RoundRectImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setLayerType(LAYER_TYPE_HARDWARE, null);
        path = new Path();
        rect = new RectF();
        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.RoundRectImageView);
        radius = typedArray.getDimension(R.styleable.RoundRectImageView_radius, 0);
        leftTopRadius = typedArray.getDimension(R.styleable.RoundRectImageView_left_top_radius, radius);
        leftBottomRadius = typedArray.getDimension(R.styleable.RoundRectImageView_left_bottom_radius, radius);
        rightTopRadius = typedArray.getDimension(R.styleable.RoundRectImageView_right_top_radius, radius);
        rightBottomRadius = typedArray.getDimension(R.styleable.RoundRectImageView_right_bottom_radius, radius);
        typedArray.recycle();
        /*圆角的半径，依次为左上角xy半径，右上角，右下角，左下角*/
        rids = new float[]{leftTopRadius, leftTopRadius, rightTopRadius, rightTopRadius, rightBottomRadius,
                rightBottomRadius, leftBottomRadius, leftBottomRadius};
    }


    public RoundRectImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs);
    }

    public float getRadius() {
        return radius;
    }

    public synchronized void setRadius(float radius) {
        this.radius = radius;
        leftTopRadius = radius;
        rightTopRadius = radius;
        rightBottomRadius = radius;
        leftBottomRadius = radius;
        rids = new float[]{leftTopRadius, leftTopRadius, rightTopRadius, rightTopRadius, rightBottomRadius,
                rightBottomRadius, leftBottomRadius, leftBottomRadius};
        postInvalidate();

    }

    public synchronized void setRadius(float leftTopRadius, float rightTopRadius,
                                       float rightBottomRadius, float leftBottomRadius) {
        rids = new float[]{leftTopRadius, leftTopRadius, rightTopRadius, rightTopRadius, rightBottomRadius,
                rightBottomRadius, leftBottomRadius, leftBottomRadius};
        postInvalidate();
    }

    /**
     * 画图
     * by Hankkin at:2015-08-30 21:15:53
     *
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {
        int w = this.getWidth();
        int h = this.getHeight();
        rect.set(0, 0, w, h);
        path.addRoundRect(rect, rids, Path.Direction.CW);
        canvas.clipPath(path);
        super.onDraw(canvas);
    }
}