package com.example.app4public.utils;

import android.os.Looper;

import com.bumptech.glide.Glide;
import com.just.agentweb.App2;
import com.sdzn.core.utils.FileUtils;


/**
 * 描述：
 * - 图片缓存清理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class GlideCatchUtil {
    private static GlideCatchUtil instance;

    public static GlideCatchUtil getInstance() {
        if (null == instance) {
            instance = new GlideCatchUtil();
        }
        return instance;
    }

    // 获取Glide磁盘缓存大小
    public String getCacheSize() {
        try {
            return FileUtils.getDirSize(CacheUtils.getImageCache());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 清除Glide磁盘缓存，自己获取缓存文件夹并删除方法
    public boolean cleanCatchDisk() {
        return FileUtils.deleteFilesInDir(CacheUtils.getImageCache());
    }

    // 清除图片磁盘缓存，调用Glide自带方法
    public boolean cleanCacheDiskSelf() {
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(App2.get()).clearDiskCache();
                    }
                }).start();
            } else {
                Glide.get(App2.get()).clearDiskCache();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // 清除Glide内存缓存
    public boolean cleanCacheMemory() {
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) { //只能在主线程执行
                Glide.get(App2.get()).clearMemory();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}