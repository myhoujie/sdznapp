package com.example.app4public.manager.constant;

import android.app.Activity;

/**
 * @author Reisen at 2017-12-06
 */

public final class IntentCons {
    private IntentCons() {
    }
    public static final int RESULT_COURSEDETAIL_CODE = 2;
    public static final int RESULT_OK = Activity.RESULT_OK;
    public static final int RESULT_FINISH = 1;
    public static final int BASIC_PERMISSION_REQUEST_CODE = 110;
    public static final int REQ_CODE_ALBUM = 1000;
    public static final int REQ_CODE_CAMERA = 1001;
    public static final int REQ_CODE_ALBUM_MSG = 1002;
    public static final int REQ_CODE_CAMERA_MSG = 1003;
}
