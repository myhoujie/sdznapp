package com.example.app4public.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 加点字符span样式
 * @author Reisen at 2019-01-09
 */
public class UnderPointSpan extends ReplacementSpan {

    private Paint mPaint;
    private int mWidth;

    public UnderPointSpan() {
        mPaint = new Paint();
        mPaint.setStrokeWidth(5);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setAntiAlias(true);
    }

    @Override
    public int getSize(@NonNull Paint paint, CharSequence text, int start, int end, @Nullable Paint.FontMetricsInt fm) {
        mWidth = (int) paint.measureText(text, start, end);
        return mWidth;
    }

    @Override
    public void draw(@NonNull Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, @NonNull Paint paint) {
        mPaint.setColor(paint.getColor());
        int charCount = end - start;
        int singleLen = mWidth / charCount;
        float firstLen = singleLen * 0.5f;
        int pointY = bottom - 3;
        float pointX = x + firstLen;
        float[] floats = new float[charCount * 2];
        for (int i = 0; i < floats.length; i+=2) {
//            canvas.drawPoint(pointX,pointY,mPaint);
            floats[i] = pointX;
            floats[i + 1] = pointY;
            pointX += singleLen;
        }
        canvas.drawPoints(floats,mPaint);
        canvas.drawText(text, start, end, x, y, paint);
    }
}
