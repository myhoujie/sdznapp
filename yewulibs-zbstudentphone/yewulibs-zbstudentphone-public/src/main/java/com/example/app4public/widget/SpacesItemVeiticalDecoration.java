package com.example.app4public.widget;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemVeiticalDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemVeiticalDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) != 0) {
            outRect.top = space;
        }
        outRect.bottom = space;
    }
}
