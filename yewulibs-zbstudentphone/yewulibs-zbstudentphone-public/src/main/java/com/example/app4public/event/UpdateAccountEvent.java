package com.example.app4public.event;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 描述：
 * - 学生个人信息修改事件
 * 创建人：baoshengxiang
 * 创建时间：2017/8/22
 */
public class UpdateAccountEvent {
    public static final String CHANGE_NAME = "name";//修改姓名
    public static final String CHANGE_SCHOOL = "schoolId";//修改学校
    public static final String CHANGE_EMAIL = "email";//修改邮箱
    public static final String CHANGE_PHASE = "levelId";//修改学段
    public static final String CHANGE_GRADE = "grade";//修改年级

    public static final String CHANGE_CLASSES = "classes";//修改年级
    public static final String CHANGE_SCHOOL_NAME = "schoolName";//学校
    public static final String CHANGE_GRADE_NAME = "gradeName";//修改年级

    public static final String CHANGE_BIND_ACCOUNT = "bindAccount";//绑定账号

    @StringDef({CHANGE_NAME, CHANGE_SCHOOL, CHANGE_EMAIL, CHANGE_PHASE, CHANGE_GRADE, CHANGE_CLASSES})
    @Retention(RetentionPolicy.SOURCE)
    public @interface UpdateName {
    }

    private String name;
    private String value;
    private String extra;

    public UpdateAccountEvent() {
    }

    public UpdateAccountEvent(String name) {
        this.name = name;
    }

    public UpdateAccountEvent(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public UpdateAccountEvent(@UpdateName String name, String value, String extra) {
        this.name = name;
        this.value = value;
        this.extra = extra;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getExtra() {
        return extra;
    }


}
