package com.example.app4public.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.widget.TextView;

import com.example.app4public.R;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.variants.bean.CourseList;
import com.example.app4public.utils.CustomClicklistener;
import com.example.app4public.utils.PriceUtil;

import java.math.BigDecimal;
import java.util.List;

/**
 * 推荐课程  首页及推荐页   精品课  专题课程
 */
public class RecommendAdapter extends BaseRcvAdapter<CourseList> {
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";
    private final int time_length=10;

    public RecommendAdapter(Context context, List<CourseList> mList) {
        super(context, R.layout.item_course_recommend_home, mList);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public void convert(final BaseViewHolder holder, final int position, CourseList liveInfoBean) {
        holder.setImageView(R.id.iv_cover, liveInfoBean.getLogo());
        holder.setText(R.id.tv_recmd_title, liveInfoBean.getCourseName());
        holder.getView(R.id.ll).setOnClickListener(new CustomClicklistener() {
            @Override
            protected void onSingleClick() {
                onItemClickListener.onItemClick(holder.getView(R.id.ll), position);
            }

            @Override
            protected void onFastClick() {

            }
        });
//        2DD4CA

        if (PriceUtil.isFree(liveInfoBean.getCurrentPrice())) {
            holder.setText(R.id.tv_recmd_price, context.getString(R.string.free));
            holder.setTextColorRes(R.id.tv_recmd_price, R.color.free_green);
        } else {
//            holder.setText(R.id.tv_recmd_price, String.format(Locale.getDefault(),
//                    "￥%1.2f", liveInfoBean.getCurrentPrice()¥¥));
            holder.setText(R.id.tv_recmd_price,"¥"+ new BigDecimal(String.valueOf(liveInfoBean.getCurrentPrice())).stripTrailingZeros().toPlainString());
            holder.setTextColorRes(R.id.tv_recmd_price, R.color.red);
        }
        if (PriceUtil.isFree(liveInfoBean.getSourcePrice())){
            holder.setText(R.id.tv_chapter,"¥0");
        }else {
            holder.setText(R.id.tv_chapter,"¥"+ new BigDecimal(String.valueOf(liveInfoBean.getSourcePrice())).stripTrailingZeros().toPlainString());
        }
        ((TextView)holder.getView(R.id.tv_chapter)).getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        if (TYPE_PACKAGE.equals(liveInfoBean.getSellType())) {
            switch (liveInfoBean.getPackageType()) {
                case 1://LIVING
                    holder.setText(R.id.tv_status,"直");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
                    if (liveInfoBean.getLiveBeginTime() != null &&liveInfoBean.getLiveEndTime() != null) {
                        holder.setText(R.id.tv_recmd_content, TimeUtils.millis2String(TimeUtils.string2Millis(liveInfoBean.getLiveBeginTime()), "yyyy-MM-dd") + "-" + TimeUtils.millis2String(TimeUtils.string2Millis(liveInfoBean.getLiveEndTime()), "yyyy-MM-dd"));
                        holder.setInVisible(R.id.tv_recmd_content, true);
                    }else {
                        holder.setInVisible(R.id.tv_recmd_content, false);
                    }

                    break;
                case 2://VIDEO
                    holder.setText(R.id.tv_status,"点");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
                    holder.setText(R.id.tv_recmd_content,""+liveInfoBean.getTeacherName());
                    break;
                default:
                    break;
            }
        }else if (TYPE_LIVE.equals(liveInfoBean.getSellType())){//LIVING
            holder.setText(R.id.tv_status,"直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);
            if (liveInfoBean.getLiveBeginTime() != null &&liveInfoBean.getLiveEndTime() != null) {
                holder.setText(R.id.tv_recmd_content, TimeUtils.millis2String(TimeUtils.string2Millis(liveInfoBean.getLiveBeginTime()), "yyyy-MM-dd") + "-" + TimeUtils.millis2String(TimeUtils.string2Millis(liveInfoBean.getLiveEndTime()), "yyyy-MM-dd"));
                holder.setInVisible(R.id.tv_recmd_content, true);
            }else {
                holder.setInVisible(R.id.tv_recmd_content, false);
            }

        }else if (TYPE_COURSE.equals(liveInfoBean.getSellType())){//VIDEO
            holder.setText(R.id.tv_status,"点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
            holder.setText(R.id.tv_recmd_content,""+liveInfoBean.getTeacherName());

        }
    }

}
