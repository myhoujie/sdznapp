package com.example.app4public.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.DrawableRes;
import androidx.core.content.res.ResourcesCompat;

import com.sdzn.core.utils.ConvertUtils;
import com.example.app4public.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;



/**
 * 描述：
 * - 封装titlebar
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class TitleBar extends RelativeLayout {
    private OnClickListener leftClickListener;
    private TitleViewHolder titleViewHolder;

//    @IntDef({R.id.tv_left, R.id.iv_left, R.id.tv_title, R.id.tv_right, R.id.iv_right})
    @Retention(RetentionPolicy.SOURCE)
    private @interface Views {
    }

    public TitleBar(Context context) {
        this(context, null);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(final Context context, AttributeSet attrs) {
        LayoutInflater from = LayoutInflater.from(context);
        View rootLayout = from.inflate(R.layout.titlebar, this);
        titleViewHolder = new TitleViewHolder(rootLayout);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleBar);

        int defaultTextColor = ResourcesCompat.getColor(getResources(), R.color.black, null);
        String leftText = typedArray.getString(R.styleable.TitleBar_leftText);
        int leftTextColor = typedArray.getColor(R.styleable.TitleBar_leftText,
                defaultTextColor);
        int deafultTextSize = ConvertUtils.dp2px(context, 20);
        float leftTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_leftTextSize, deafultTextSize));
        int leftImg = typedArray.getResourceId(R.styleable.TitleBar_leftImg, R.mipmap.ic_back);
        String titleText = typedArray.getString(R.styleable.TitleBar_titleText);
        int titleTextColor = typedArray.getColor(R.styleable.TitleBar_titleTextColor,
                defaultTextColor);
        float titleTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_titleTextSize, deafultTextSize));
        String rightText = typedArray.getString(R.styleable.TitleBar_rightText);
        int rightTextColor = typedArray.getColor(R.styleable.TitleBar_rightTextColor,
                defaultTextColor);
        float rightTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_rightTextSize, deafultTextSize));
        int rightImg = typedArray.getResourceId(R.styleable.TitleBar_rightImg, 0);
        typedArray.recycle();
        if (!TextUtils.isEmpty(leftText)) {
            titleViewHolder.tvLeft.setText(leftText);
            titleViewHolder.tvLeft.setTextColor(leftTextColor);
            titleViewHolder.tvLeft.setTextSize(leftTextSize);
        }
        if (!TextUtils.isEmpty(titleText)) {
            titleViewHolder.tvTitle.setText(titleText);
            titleViewHolder.tvTitle.setTextColor(titleTextColor);
            titleViewHolder.tvTitle.setTextSize(titleTextSize);
        }
        if (!TextUtils.isEmpty(rightText)) {
            titleViewHolder.tvRight.setText(rightText);
            titleViewHolder.tvRight.setTextColor(rightTextColor);
            titleViewHolder.tvRight.setTextSize(rightTextSize);
        }

        if (leftClickListener == null) {
            leftClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) context).onBackPressed();
                }
            };
        }

        if (leftImg != 0) {
            titleViewHolder.ivLeft.setImageResource(leftImg);
        }

        if (rightImg != 0) {
            titleViewHolder.ivRight.setImageResource(rightImg);
        }

        titleViewHolder.tvLeft.setOnClickListener(leftClickListener);
        titleViewHolder.ivLeft.setOnClickListener(leftClickListener);

    }

    public TitleBar setLeftText(String leftText) {
        titleViewHolder.tvLeft.setText(leftText);
        return this;
    }

    public TitleBar setLeftTextColor(int leftTextColor) {
        titleViewHolder.tvLeft.setTextColor(leftTextColor);
        return this;
    }

    public TitleBar setLeftTextSize(@Dimension float leftTextSize) {
        titleViewHolder.tvLeft.setTextSize(leftTextSize);
        return this;
    }

    public TitleBar setTitleText(String titleText) {
        titleViewHolder.tvTitle.setText(titleText);
        return this;
    }

    public TitleBar setTitleTextColor(int titleTextColor) {
        titleViewHolder.tvTitle.setTextColor(titleTextColor);
        return this;
    }

    public TitleBar setTitleTextSize(@Dimension float titleTextSize) {
        titleViewHolder.tvTitle.setTextSize(titleTextSize);
        return this;
    }

    public TitleBar setRightText(String rightText) {
        titleViewHolder.tvRight.setText(rightText);
        return this;
    }


    public TitleBar setRightTextColor(int rightTextColor) {
        titleViewHolder.tvRight.setTextColor(rightTextColor);
        return this;
    }

    public TitleBar setRightTextSize(@Dimension float rightTextSize) {
        titleViewHolder.tvRight.setTextSize(rightTextSize);
        return this;
    }

    public TitleBar setLeftImg(@DrawableRes int leftImg) {
        titleViewHolder.ivLeft.setImageResource(leftImg);
        return this;
    }

    public TitleBar setRightImg(@DrawableRes int rightImg) {
        titleViewHolder.ivRight.setImageResource(rightImg);
        return this;
    }


    public TitleBar setLeftClickListener(OnClickListener leftClickListener) {
        titleViewHolder.ivLeft.setOnClickListener(leftClickListener);
        titleViewHolder.tvLeft.setOnClickListener(leftClickListener);
        return this;
    }

    public TitleBar setRightClickListener(OnClickListener rightClickListener) {
        titleViewHolder.ivRight.setOnClickListener(rightClickListener);
        titleViewHolder.tvRight.setOnClickListener(rightClickListener);
        return this;
    }

    public View getView(@Views int id) {
        if (id == R.id.iv_left) {
            return titleViewHolder.ivLeft;
        } else if (id == R.id.tv_left) {
            return titleViewHolder.tvLeft;
        } else if (id == R.id.tv_title) {
            return titleViewHolder.tvTitle;
        } else if (id == R.id.iv_right) {
            return titleViewHolder.ivRight;
        } else if (id == R.id.tv_right) {
            return titleViewHolder.tvRight;
        }
        return null;
    }


    static class TitleViewHolder {

        ImageView ivLeft;
        TextView tvLeft;
        TextView tvTitle;
        TextView tvRight;
        ImageView ivRight;

        TitleViewHolder(View view) {
            ivLeft = view.findViewById(R.id.iv_left);
            tvLeft = view.findViewById(R.id.tv_left);
            tvTitle = view.findViewById(R.id.tv_title);
            tvRight = view.findViewById(R.id.tv_right);
            ivRight = view.findViewById(R.id.iv_right);
        }
    }

}
