package com.example.app4public.utils;

import android.content.res.Configuration;

import com.just.agentweb.App2;
import com.example.app4public.manager.Config;

/**
 * @author Reisen at 2018-01-19
 */

public final class PriceUtil {
    private PriceUtil() {
    }

    public static boolean isFree(double price) {
        return price >= -0.000001 && price <= 0.000001;
    }

    /**
     * 判断当前设备是手机还是平板，代码来自 Google I/O App for Android
     */
    public static String isPad() {
        return (App2.get().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE ? Config.TABLET : Config.PHONE;
    }
}
