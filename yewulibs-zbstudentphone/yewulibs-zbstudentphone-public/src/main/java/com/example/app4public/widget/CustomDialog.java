package com.example.app4public.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app4public.R;


/**
 * 描述：普通的Dialog
 * <p/>
 * 创建人：wangchunxiao
 * 创建时间：16/7/28
 */
public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String title;
        private int titleColor = -1;
        private float titleSize = -1;
        private String message;
        private String positiveText;
        private String negativeText;
        private View contentView;
        private OnClickListener positiveClickListener;
        private OnClickListener negativeClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        /**
         * 设置显示内容
         *
         * @param message
         * @return
         */
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * 设置显示内容的资源文件
         *
         * @param message
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * 设置标题的资源文件
         *
         * @param title
         * @return
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        /**
         * 设置标题
         *
         * @param title
         * @return
         */

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        /**
         * 设置标题文字颜色
         *
         * @param colorId
         * @return
         */
        public Builder setTitleColor(int colorId) {
            this.titleColor = colorId;
            return this;
        }

        /**
         * 设置标题文字大小
         *
         * @param titleSize
         * @return
         */
        public Builder setTitleSize(float titleSize) {
            this.titleSize = titleSize;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * 设置确定按钮
         *
         * @param positiveText
         * @return
         */
        public Builder setPositive(int positiveText,
                                   OnClickListener listener) {
            this.positiveText = (String) context
                    .getText(positiveText);
            this.positiveClickListener = listener;
            return this;
        }

        /**
         * 设置确定按钮
         *
         * @param positiveText
         * @return
         */
        public Builder setPositive(String positiveText,
                                   OnClickListener listener) {
            this.positiveText = positiveText;
            this.positiveClickListener = listener;
            return this;
        }

        /**
         * 设置取消按钮
         *
         * @param negativeText
         * @return
         */
        public Builder setNegative(int negativeText,
                                   OnClickListener listener) {
            this.negativeText = (String) context
                    .getText(negativeText);
            this.negativeClickListener = listener;
            return this;
        }

        /**
         * 设置取消按钮
         *
         * @param negativeText
         * @return
         */
        public Builder setNegative(String negativeText,
                                   OnClickListener listener) {
            this.negativeText = negativeText;
            this.negativeClickListener = listener;
            return this;
        }

        /**
         * 创建Dialog
         *
         * @return
         */
        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final CustomDialog dialog = new CustomDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_normal, null);
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            if (title != null) {
                ((TextView) layout.findViewById(R.id.tvTitle)).setText(title);
            } else {
                layout.findViewById(R.id.llTitle).setVisibility(
                        View.GONE);
            }
            if (titleColor != -1) {
                ((TextView) layout.findViewById(R.id.tvTitle)).setTextColor(context.getResources().getColor(titleColor));
            }
            if (titleSize != -1) {
                ((TextView) layout.findViewById(R.id.tvTitle)).setTextSize(titleSize);
            }
            if (positiveText != null) {
                ((TextView) layout.findViewById(R.id.tvPositive))
                        .setText(positiveText);
                if (positiveClickListener != null) {
                    (layout.findViewById(R.id.tvPositive))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    positiveClickListener.onClick(dialog,
                                            DialogInterface.BUTTON_POSITIVE);
                                }
                            });
                }
            }
            if (negativeText != null) {
                ((TextView) layout.findViewById(R.id.tvNegative))
                        .setText(negativeText);
                if (negativeClickListener != null) {
                    (layout.findViewById(R.id.tvNegative))
                            .setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    negativeClickListener.onClick(dialog,
                                            DialogInterface.BUTTON_NEGATIVE);
                                }
                            });
                }
            } else {
                layout.findViewById(R.id.llNegative).setVisibility(
                        View.GONE);
                layout.findViewById(R.id.tvPositive).setBackgroundResource(R.drawable.selector_dialog_confirm);
            }
            if (message != null) {
                ((TextView) layout.findViewById(R.id.tvMessage)).setText(message);
            } else if (contentView != null) {
                ((LinearLayout) layout.findViewById(R.id.llContent))
                        .removeAllViews();
                ((LinearLayout) layout.findViewById(R.id.llContent)).addView(
                        contentView, new LayoutParams(
                                LayoutParams.MATCH_PARENT,
                                LayoutParams.MATCH_PARENT));
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}
