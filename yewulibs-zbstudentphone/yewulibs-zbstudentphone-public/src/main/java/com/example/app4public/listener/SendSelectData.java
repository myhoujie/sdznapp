package com.example.app4public.listener;

import com.sdzn.variants.bean.GradeJson;

/*
年级选中时回传
 */
public interface SendSelectData {
    void send(String eduId, GradeJson.ChildListBean bean);
}
