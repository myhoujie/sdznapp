package com.example.app4public.event;

public class MineCourseEvent {

    private boolean status;

    public MineCourseEvent(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
