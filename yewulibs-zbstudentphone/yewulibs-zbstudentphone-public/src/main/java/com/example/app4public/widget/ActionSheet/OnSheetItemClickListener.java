package com.example.app4public.widget.ActionSheet;

public interface OnSheetItemClickListener {
    void onClick(int which);
}