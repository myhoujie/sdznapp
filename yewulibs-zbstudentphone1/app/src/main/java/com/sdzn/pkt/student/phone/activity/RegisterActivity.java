package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.RegisterPresenter;
import com.sdzn.pkt.student.phone.mvp.view.RegisterView;
import com.sdzn.pkt.student.phone.utils.CountDownTimerUtils;
import com.sdzn.pkt.student.phone.utils.VerifyUtil;
import com.sdzn.pkt.student.phone.widget.ClearEditText;
import com.sdzn.pkt.student.phone.widget.PwdEditText;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ---->   找回密码  没有手机号的去情况下
 */
public class RegisterActivity extends BaseMVPActivity<RegisterView, RegisterPresenter> implements RegisterView {

    @BindView(R.id.et_account)
    ClearEditText etAccount;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.et_password)
    PwdEditText etPassword;
    @BindView(R.id.et_password_s)
    PwdEditText etPasswords;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected RegisterPresenter createPresenter() {
        return new RegisterPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
//        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (VerifyUtil.isMobileNO(etPhone.getText().toString())) {
//                        etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                    } else {
//                        if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                            etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.red));
//                            etPhone.setError("手机号格式不正确");
//                        }
//                    }
//                } else {
//                    etPhone.setTextColor(ContextCompat.getColor(mContext, R.color.textPrimary));
//                }
//            }
//        });
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }


    @OnClick({R.id.btn_get_code, R.id.btn_certain, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                getVerifyCode();
                break;
            case R.id.btn_certain:
                register();
                break;
            case R.id.iv_back:
                onBackPressed();
            default:

                break;
        }
    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            countDownTimerUtils.start();
            mPresenter.getVerifyCode(phoneNo);
        }
    }

    private void register() {
        String account = etAccount.getText().toString().trim();
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String passwordS = etPasswords.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入账号");
        } else if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        }
        else {
            Map<String, String> params = new HashMap<>();
            params.put("account", account);
            params.put("phone", phoneNo);
            params.put("code", code);
//            params.put("password", password);
            mPresenter.retrievePassword(account,phoneNo,code);
        }
    }


    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        ToastUtils.showShort(msg);
    }

    @Override
    public void registerSuccess() {
        String phoneNo = etPhone.getText().toString().trim();
        SPManager.saveLastLoginAccount(phoneNo);
        ToastUtils.showShort("请登录");
        IntentController.toLogin(mContext);
    }

    @Override
    public void registerFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
