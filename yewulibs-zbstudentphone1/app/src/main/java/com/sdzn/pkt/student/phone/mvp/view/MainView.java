package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/8/11
 */
public interface MainView extends BaseView {
    void updateVersion(String updateInfo, String targetUrl);

    void autoLoginSuccess();

}
