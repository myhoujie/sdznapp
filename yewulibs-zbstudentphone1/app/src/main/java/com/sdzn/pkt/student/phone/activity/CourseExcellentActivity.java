package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.RecommendAdapter;
import com.sdzn.pkt.student.phone.adapter.WrapAdapter;
import com.sdzn.pkt.student.phone.bean.CourseList;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.CourseExcellentPresenter;
import com.sdzn.pkt.student.phone.mvp.view.CourseExcellentView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 精品课程列表
 */

public class CourseExcellentActivity extends BaseMVPActivity<CourseExcellentView, CourseExcellentPresenter>
        implements CourseExcellentView, OnRefreshListener, OnRefreshLoadmoreListener {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;

    @BindView(R.id.rv_course)
    RecyclerView rvCourseRec;
    @BindView(R.id.title_bar)
    TitleBar titleBar;

    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private int pageIndex = 1;//当前页
    private int pageSize = 10;//


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_excellent;
    }


    @Override
    protected CourseExcellentPresenter createPresenter() {
        return new CourseExcellentPresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        pageIndex = 1;
        initData();
        initView();
        loadNetData();
    }

    private void initData() {
        recommendCourses = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPManager.autoLogin(mContext)) {
                    IntentController.toLogin(mContext);
                    return;
                }
                IntentController.toShoppingCart(mContext);
            }
        });
        refreshLayout.setOnRefreshLoadmoreListener(this);
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2));
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    IntentController.toCourseDetail(mContext, recommendCourses.get(position).getSellType(), recommendCourses.get(position).getCourseId(), false);
                }

            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                loadNetData();
            }
        });
    }

    private void loadNetData() {
        Map<String, String> map = new HashMap<>();
        map.put("size", String.valueOf(pageSize));
        map.put("index", String.valueOf(pageIndex));
        mPresenter.getCourse(map);

    }


    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }


    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        if (pageIndex == 1) {
            this.recommendCourses.clear();
            if (courses.size() == 0) {
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                return;
            }
        }
        refreshLayout.setLoadmoreFinished(courses.size() < pageSize);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);


    }


    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        loadNetData();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        loadNetData();

    }

    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
