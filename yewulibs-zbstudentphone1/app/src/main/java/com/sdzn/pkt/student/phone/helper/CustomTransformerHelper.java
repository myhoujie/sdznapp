package com.sdzn.pkt.student.phone.helper;

import android.annotation.SuppressLint;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

/**
 * 描述：
 * - 反映出在ViewPager滑动过程中，各个View的位置变化。
 * 创建人：baoshengxiang
 * 创建时间：2017/7/6
 */
public class CustomTransformerHelper implements ViewPager.PageTransformer {
    private float minScale = 0.85F;

    public CustomTransformerHelper() {
    }

    public CustomTransformerHelper(float scaleRatio) {
        this.minScale = scaleRatio;
    }

    private static final float MIN_SCALE = 0.85f;

    @SuppressLint("NewApi")
    public void transformPage(View view, float position) {
        //a页滑动至b页 ； a页从 0.0 -1 ；b页从1 ~ 0.0
        if (position <= 1 || position >= 1) { // [-1,1]
            // Modify the default slide transition to shrink the page as well
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
        }
    }

}