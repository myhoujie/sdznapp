package com.sdzn.pkt.student.phone.event;

/**
 * 描述：
 * - 订单支付事件
 * 创建人：baoshengxiang
 * 创建时间：2017/8/8
 */
public class OrderPayEvent {
    private boolean isSuccess;

    public OrderPayEvent(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
