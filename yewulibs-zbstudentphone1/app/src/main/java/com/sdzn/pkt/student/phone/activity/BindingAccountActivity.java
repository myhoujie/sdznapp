package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.event.BindEvent;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.UpdateAccountPresenter;
import com.sdzn.pkt.student.phone.mvp.view.UpdateAccountView;
import com.sdzn.pkt.student.phone.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 绑定账号
 * zs
 */
public class BindingAccountActivity extends BaseMVPActivity<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {

    @BindView(R.id.et_name)
    EditText etName;
    private String studentName;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_binding_account;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        studentName = getIntent().getStringExtra("bindAccount");
        if (!TextUtils.isEmpty(studentName)) {
//            etName.setText(studentName);
//            etName.setSelection(studentName.length());
        }

    }

    @OnClick(R.id.btn_certain)
    public void onViewClicked() {
        String bindAccount = etName.getText().toString().trim();
        if (TextUtils.isEmpty(bindAccount)) {
            ToastUtils.showShort("请输入绑定账号,账号不能为空");
        } else {
            mPresenter.updateAccount(bindAccount);
        }
    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new BindEvent());
        //主要是去刷新学校课程（筛选条件学段固定）
//        SPManager.saveSchoolSection(userBean.getSubjectId(),"");
//        SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 学校课程
        BindingAccountActivity.this.finish();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
