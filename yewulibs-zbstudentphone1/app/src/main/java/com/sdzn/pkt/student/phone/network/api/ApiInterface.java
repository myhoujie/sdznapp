package com.sdzn.pkt.student.phone.network.api;

/**
 * 描述：API请求地址
 * -
 */
public class ApiInterface {

    //    public static final String QUERY_VERSION_INFO = "app/api/version/selectVersionInfo";//查询版本信息
    public static final String QUERY_VERSION_INFO = "usercenter/sysVersionInfo/selectOne";//查询版本信息
    public static final String POST_MINE_TASK = "practice/stuhomework/queryHomeworkListApp";//获取我的作业
    public static final String POST_VALIDATE_TIME = "practice/stuhomework/validateTime";//学生作答校验
    /*
    新接口
     */
    public static final String QUWRY_RECOMMENDED_COURSE = "eduLive/api/queryRecommendedCourses";//推荐课程
    public static final String SYS_SCHOOL_LIST = "eduLive/api/sysSchoolList";//学校列表
    public static final String ENTER_LIVE_ROOM = "eduLive/api/live/enterLiveRoom";//直播
    public static final String VIEW_PLAY_BACK = "eduLive/api/live/viewPlayBack";//回放
    public static final String VIEW_ONDEMAND = "eduLive/api/live/viewOnDemand";//点播
    public static final String COURSE_APPLY = "eduLive/api/course/courseApply";//免费报名接口
    public static final String COURSE_PACKAGE_DETAILS = "eduLive/api/course/queryCoursePackageDetailsByCourseId";//组合课程详情
    public static final String COURSE_DETAILS_NORMAL = "eduLive/api/course/queryCourseDetailsByCourseId";//普通章节课程详情

    /*
    需要加进  NewApiEqualUtil  isNoToken中   不传token
     */
    public static final String USER_LOGIN = "student/login/token";//密码登录   **替换成form urlencoded**   api/userLogin
    public static final String SEND_VERIFY = "student/login/sendVerifyCode";//验证码 **替换成form urlencoded**
    public static final String USER_CODE = "student/login/smsToken";//验证码登录   **替换成form urlencoded**
    public static final String RETRIEVE_PASSWORD = "student/login/bindMobileBySms";//找回密码没有绑定手机 **替换成form urlencoded**

    //    public static final String GRADE_JSON = "eduLive/api/gradeJson";//获取年级基础信息
    public static final String SELECT_SUBJECT_LIST = "usercenter/baseSubject/list";//根据学段选学科   **替换成form urlencoded**
    public static final String SELECT_TOPIC_LIST = "educationLive/eduProjectName/list";//专题名称列表   **替换成form urlencoded**
    public static final String QUERY_APP_RECOMMENDED = "eduLive/api/queryAPPRecommendedCourses";//手机端推荐接口  精品课程列表
    public static final String QUERY_APP_TOPIC = "eduLive/api/queryAPPHomeSubjectCourses";//  专题课程列表
    public static final String COURSE_SELECTION = "eduLive/api/course/courseSelectionCenter";//选课中心 拼课堂根据学科获取科目，根据name搜索
    public static final String HOME_RECOMMENDED = "eduLive/api/queryAPPHomeRecommendedCourses";//拼课堂首页(精品课,专题课)
    public static final String QUERY_BANNER = "eduLive/api/queryBannerList";//首页banner


    public static final String CHANGE_SCHOOL_CENTER = "eduLive/api/course/courseSchoolCenter";//学校中心  学校课程
    public static final String CHANGE_TODY = "eduLive/api/course/myTodayCourse";//今日直播
    public static final String COURSE_RECENTLY = "eduLive/api/course/myRecentlyCourse";//近期直播
    public static final String COURSE_ALL_COURSE = "eduLive/api/course/myAllCourse";//全部课程
    public static final String APP_RESOURCE = "eduLive/api/AppStudentResourceList";//
    public static final String CHANGE_TOC_PASSWORD = "eduLive/api/changeToCUserPassword";//toc修改密码
    public static final String OPINION = "eduLive/api/opinion";//反馈


    public static final String ADD_SHOPPING_CART_URL = "eduLive/api/shopcart/add";  //加入购物车
    public static final String QUERY_SHOPPING_CART_URL = "eduLive/api/shopcart/list";  //查询购物车
    public static final String QUERY_SHOPPING_CART_FILTER = "eduLive/api/shopcart/settle";  //根据条件 查询购物车
    public static final String DELETE_SHOPPING_CART_URL = "eduLive/api/shopcart/delete";  //购物车删除


    public static final String ADD_FAVORITE_URL = "eduLive/api/course/addCollectCourse";   //添加收藏
    public static final String DEL_COLLECTION = "eduLive/api/course/delCollectCourse";    //删除我的收藏
    public static final String UP_COLLECT = "eduLive/api/course/collectCourseList";
    public static final String URL_UPLOAD_PHOTO = "eduLive/api/upload";//上传返回url
    public static final String CHANGE_USER_PHOTO = "eduLive/api/changeUserPhoto";//通过url修改头像


    //没有token
    public static final String CONFIRM_VERIFY_CODE = "student/login/forgetPassword/smsToken";//忘记密码通过手机获取token       **form urlencoded**


    public static final String ACCOUNT_CHANGEPASSWORD = "student/current/changePassword";//修改密码  **form urlencoded**
    public static final String UPDATE_LEVEL_GRADE = "usercenter/student/updateEducationLevelGrade";//更新学生的学制、学段、年级信息  **form urlencoded**
    public static final String CONFIRM_PHONE_VERIFY_CODE = "usercenter/student/update/mobile";  //个人设置中的绑定手机  **form urlencoded**
    public static final String CHANGE_USER_INFO = "usercenter/student/updateAvatarNameSchoolNameClassName";//修改个人资料
    public static final String BIND_ACCOUNT_USER = "usercenter/student/updateMobileUserBindAccountUser";//与后台生成的账号绑定

    public static final String GET_MESSAGE_LIST = "educationLive/eduMsgReceive/listByUserId";//消息列表 **form urlencoded**
    public static final String DEL_MESSAGE = "educationLive/eduMsgReceive/delete";//删除消息   **form urlencoded**
    public static final String QUERY_UNREAD_MSG_COUNT = "educationLive/eduMsgReceive/listByStateCount";//获取未读消息数 **form urlencoded**
    public static final String UPDATE_READ = "educationLive/eduMsgReceive/updateState";//消息状态修改  **form urlencoded**

    public static final String ORDER_LIST = "eduLive/api/order/list";//订单列表
    public static final String URL_CANCEL_INDENT = "eduLive/api/order/cancel";//取消订单
    public static final String URL_DROP_COURSE = "eduLive/api/order/refundApplyEduCourse";//退课申请  退款订单接口
    public static final String SUBMIT_ORDER_URL = "eduLive/api/app/order/submit";    //提交订单
    public static final String ORDER_PAY_URL = "eduLive/api/app/order/pay";    //订单支付接口
    public static final String ORDER_DETAILS_URL = "eduLive/api/order/orderInfoDetails";    //订单详情
    public static final String ORDER_CANCEL_COURSE = "eduLive/api/order/cancelRefundEdu";    //取消退课


}
