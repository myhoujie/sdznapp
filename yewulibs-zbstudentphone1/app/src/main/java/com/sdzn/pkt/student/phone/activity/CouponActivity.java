package com.sdzn.pkt.student.phone.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.CouponAdapter;
import com.sdzn.pkt.student.phone.bean.CouponBean;
import com.sdzn.pkt.student.phone.mvp.presenter.CouponPresenter;
import com.sdzn.pkt.student.phone.mvp.view.CouponView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 描述：优惠券
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public class CouponActivity extends BaseMVPActivity<CouponView, CouponPresenter> implements CouponView, com.scwang.smartrefresh.layout.listener.OnRefreshListener {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.swipe_target)
    RecyclerView recyclerCoupon;
    @BindView(R.id.swipeToLoadLayout)
    SmartRefreshLayout swipeToLoadLayout;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;

    private CouponAdapter couponAdapter;
    //    private List<String> mData = new ArrayList<>();
    private List<CouponBean> mData = new ArrayList<>();
    private String titleName = "我的优惠券";
    private boolean isNeedBack = false;//是否需要回传优惠券信息

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_coupon;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleName = getIntent().getStringExtra("titleName");
        List<CouponBean> coupons = getIntent().getParcelableArrayListExtra("coupons");
        if (coupons != null) {
            mData.clear();
            mData.addAll(coupons);
            isNeedBack = true;
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }

        initView();
        initData();


    }

    private void initData() {
        if (mData.isEmpty()) {
            mPresenter.getListCoupon();
        }
    }

    private void initView() {
        titleBar.setTitleText(titleName);
        couponAdapter = new CouponAdapter(mContext, mData);
        recyclerCoupon.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerCoupon.setAdapter(couponAdapter);
        recyclerCoupon.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 10));
        couponAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isNeedBack) {
                    Intent intent = new Intent();
                    intent.putExtra("coupon", mData.get(position));
                    CouponActivity.this.setResult(RESULT_OK, intent);
                    CouponActivity.this.finish();
                }
            }
        });
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        swipeToLoadLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        if (isNeedBack) {
            mPresenter.queryShoppingCart();
        } else {
            mPresenter.getListCoupon();
        }
    }

    @Override
    protected CouponPresenter createPresenter() {
        return new CouponPresenter();
    }

    @Override
    public void listOnSuccess(List<CouponBean> couponBeanList) {
        if (couponBeanList != null && !couponBeanList.isEmpty()) {
            mData.clear();
            mData.addAll(couponBeanList);
            couponAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        } else {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        }

        swipeToLoadLayout.finishRefresh();
    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        swipeToLoadLayout.finishRefresh();
    }

}
