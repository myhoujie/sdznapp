package com.sdzn.pkt.student.phone.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.PageAdapterWithView;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 欢迎页
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WelcomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.guide_vp)
    ViewPager guideVp;
    @BindView(R.id.ll)
    LinearLayout ll;

    private final int[] IMGS = {R.mipmap.welcome1, R.mipmap.welcome2};
    private List<View> views;
    private PageAdapterWithView pageAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onInit(Bundle bundle) {
        intiData();
        initView();
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.setColor(this, Color.WHITE, 0);
        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        DisplayMetrics dm = new DisplayMetrics();
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getRealMetrics(dm);
        int widthPixels = dm.widthPixels;//单位为像素 px
        int height = dm.heightPixels;//单位为像素 px
        float density = dm.density;
        float scaledDensity = dm.scaledDensity;
        int densityDpi = dm.densityDpi;
//        Log.e("gongshi", String.valueOf(densityDpi) + "----" + widthPixels + "--" + height);
//        Log.e("gongshi", (Math.sqrt(Math.pow(1280, 2) + (Math.pow(800, 2))) / 272 + ""));
    }

    private void initView() {
        pageAdapter = new PageAdapterWithView(views);
        guideVp.setAdapter(pageAdapter);
        guideVp.addOnPageChangeListener(this);
        guideVp.setVisibility(View.GONE);
    }

    private void intiData() {
        views = new ArrayList<>();
        initImgs();
    }


    private void initImgs() {
        ImageView imageView = null;
        for (int i = 0; i < IMGS.length; i++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams imageviewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageviewParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageResource(IMGS[i]);
            views.add(imageView);
        }
    }

    @OnClick({R.id.tv_user_agreement, R.id.tv_privacy_agreement, R.id.tv_agreement_no, R.id.tv_agreement_yes})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_user_agreement:
                IntentController.toWeb(mContext, "1");
                break;
            case R.id.tv_privacy_agreement:
                IntentController.toWeb(mContext, "2");
                break;
            case R.id.tv_agreement_no:
                showDialog();
                break;
            case R.id.tv_agreement_yes:
                ll.setVisibility(View.GONE);
                guideVp.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void showDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("若不同意，则无法继续使用")
                .setPositiveButton("我再考虑下", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {

                    }
                })
                .setNegativeButton("不同意并退出", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        SPManager.setFirstEnterApp(mContext);
                        AppManager.getAppManager().appExit();
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == IMGS.length - 1) {
            views.get(position).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterApp();
                }
            });
        }
    }

    private void enterApp() {
        if (SPManager.isFirstSubjectId(mContext) && String.valueOf(SPManager.getgradeId()).isEmpty()) {
            IntentController.toSelectSubject(mContext, false);
        } else {
            IntentController.toMain(mContext, false);
        }
    }


}
