package com.sdzn.pkt.student.phone.network.api;

/**
 * 描述：
 * - 异常类型
 * 创建人：baoshengxiang
 * 创建时间：2017/7/28
 */
public class NetExceptionCode {
    /*token不存在*/
    public static final int TOKEN_NONE = 90001;
    /*token唯恐*/
    public static final int TOKEN_NULL = 90003;

}
