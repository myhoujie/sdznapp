package com.sdzn.pkt.student.phone.bean;

import java.util.ArrayList;

/**
 * @author Reisen at 2018-05-23
 */

public class DataDownLoadBean {
    private PageBean page;
    private ArrayList<CourseFileBean> courseFileList;

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public ArrayList<CourseFileBean> getCourseFileList() {
        return courseFileList;
    }

    public void setCourseFileList(ArrayList<CourseFileBean> courseFileList) {
        this.courseFileList = courseFileList;
    }
}