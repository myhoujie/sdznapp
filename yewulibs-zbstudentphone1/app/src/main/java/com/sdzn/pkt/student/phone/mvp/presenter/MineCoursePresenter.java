package com.sdzn.pkt.student.phone.mvp.presenter;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.bean.MineListRows;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.mvp.view.MineCourseView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.api.ResponseNewSchoolFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 *zs
 */

public class MineCoursePresenter extends BasePresenter<MineCourseView> {

    public void getTodyCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTodyCourse(requestBody)
                .compose(TransformUtils.<ResultBean<MineListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<MineListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<MineListRows>() {
                    @Override
                    public void onNext(MineListRows list) {
                        if (list == null) {
                            getView().listCourseEmpty();
                        } else {
                            getView().listTodyCourseSuccess(list.getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    public void getRecentlyCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getRecentlyCourse(requestBody)
                .compose(TransformUtils.<ResultBean<MineListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<MineListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<MineListRows>() {
                    @Override
                    public void onNext(MineListRows list) {
                        if (list == null) {
                            getView().listCourseEmpty();
                        } else {
                            getView().listRecentlyCourseSuccess(list.getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    /**
     * type   1 过期   2 全部课程
     */
    public void getCourse(Map<String, String> map, int type) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getALLCourse(requestBody)
                .compose(TransformUtils.<ResultBean<MineListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<MineListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<MineListRows>() {
                    @Override
                    public void onNext(MineListRows list) {
                        if (list == null) {
                            getView().listCourseEmpty();
                        } else {
                            getView().listCourseSuccess(list.getRows(), type);
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().listCourseError("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }

    /**
     * 去看直播
     */
    public void getLivingInfo(String kpointId) {

        Map<String, String> map = new HashMap<>();
        map.put("kpointId", kpointId);
        map.put("channel", "2");
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getNewLiveInfo(requestBody)
                .compose(TransformUtils.<ResultBean<NewLiveInfo>>defaultSchedulers())
                .map(new ResponseNewFunc<NewLiveInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewLiveInfo>() {
                    @Override
                    public void onNext(NewLiveInfo courses) {
                        if (courses != null) {
                            getView().getLiveRoomInfoSuccrss(courses);
                        }

                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        ToastUtils.showShort("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 回放
     *
     * @param kpointId
     */
    public void getReplayInfo(String kpointId,String courseId) {

        Map<String, String> map = new HashMap<>();
        map.put("kpointId", kpointId);
        map.put("courseId",courseId);
        map.put("channel", "2");
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getNewReplayInfo(requestBody)
                .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                .map(new ResponseNewFunc<NewVideoInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                    @Override
                    public void onNext(NewVideoInfo courses) {
                        if (courses != null) {
                            getView().getReplayInfoSuccess(courses);
                        } else {
                            ToastUtils.showShort("回放教室不存在或已删除");
                        }

                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
//                        String msg = "回放教室不存在或已删除";
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
                        ToastUtils.showShort("" + e.getMessage());

                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
