package com.sdzn.pkt.student.phone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.AnswerCardAdapter;
import com.sdzn.pkt.student.phone.bean.AnswerCardBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

import static com.sdzn.pkt.student.phone.R.id.tv_answer;

/**
 * 描述：
 * - 直播间-》答题卡
 * 创建人：baoshengxiang
 * 创建时间：2017/9/26
 */
public class AnswerFragment extends BaseFragment {
    @BindView(tv_answer)
    TextView tvAnswer;
    @BindView(R.id.rcv_answer)
    RecyclerView rcvAnswer;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;

    private OnFragmentExitListener mListener;
    private List<AnswerCardBean.DetailBean> answerCardBeans;
    private AnswerCardAdapter answerCardAdapter;
    private String cardFlag;//答题卡编号
    private AnswerCardBean answerCardBean;


    public static AnswerFragment newInstance() {
        return new AnswerFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentExitListener) {
            mListener = (OnFragmentExitListener) context;
        } else {
            throw new UnsupportedOperationException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
//        if (context instanceof AudienceActivity) {
//            answerCardBean = ((AudienceActivity) context).getAnswerCardData();
//        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_answer;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        rcvAnswer.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ContextCompat.getColor(mContext, R.color.gray_ea), 1));
        rcvAnswer.setLayoutManager(new LinearLayoutManager(mContext));
        answerCardBeans = new ArrayList<>();
        initData();
        answerCardAdapter = new AnswerCardAdapter(mContext, answerCardBeans);
        answerCardAdapter.setEdit(true);
        rcvAnswer.setAdapter(answerCardAdapter);

    }

    private void initData() {
        if (answerCardBean != null && answerCardBean.getDetail() != null) {
            cardFlag = answerCardBean.getCardFlag();
            answerCardBeans.addAll(answerCardBean.getDetail());
        }
    }


    @OnClick(R.id.tv_submit)
    public void submitAnswer() {
        if (answerCardAdapter.getEdit()) {
            answerCardAdapter.setEdit(false);
            AnswerCardBean answerResult = answerCardAdapter.getAnswerResult();
            answerResult.setStuID("");
            answerResult.setCardFlag(cardFlag);
            tvAnswer.setText(String.format(Locale.getDefault(), "得分：%d分", answerResult.getTotalPoint()));
            tvAnswer.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            mListener.onAnswerCardSubmit(new Gson().toJson(answerResult));
            tvSubmit.setText("取消");
        } else {
            mListener.onAnswerCardClose();
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.push_bottom_enter);
        } else {
            return null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentExitListener {

        void onAnswerCardSubmit(String json);

        void onAnswerCardClose();

    }


}
