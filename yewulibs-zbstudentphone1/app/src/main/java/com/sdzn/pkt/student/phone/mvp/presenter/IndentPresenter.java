package com.sdzn.pkt.student.phone.mvp.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.IndentBean;
import com.sdzn.pkt.student.phone.bean.IndentResultBean;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.mvp.view.IndentView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.AccountService;
import com.sdzn.pkt.student.phone.network.api.CourseService;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：关于订单的部分
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/17
 */

public class IndentPresenter extends BasePresenter<IndentView> {

    public void getIndentList(String states, int currentPage, int pageSize) {

        Map<String, String> map = new HashMap<>();
        map.put("states", states);
        map.put("index", String.valueOf(currentPage));
        map.put("size", String.valueOf(pageSize));
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Subscription subscription = RestApi.getInstance()
                .createNew(CourseService.class)
                .orderList(requestBody)
                .compose(TransformUtils.<ResultBean<IndentResultBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<IndentResultBean>(new SubscriberOnNextListener<IndentResultBean>() {

                    @Override
                    public void onNext(IndentResultBean bean) {
                        getView().listindents(bean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().onError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);

    }

    /**
     * 支付的接口
     */

    public void toBuyIndent(IndentBean indentBean,String payType) {
        Map<String, String> map = new HashMap<>();
        map.put("payType", payType);
        map.put("orderId", String.valueOf(indentBean.getOrderId()));
        map.put("deviceType", "0");//0 安卓手机
        String jsonPay = new Gson().toJson(map);//要传递的json
        RequestBody requestBodyPay = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonPay);
        RestApi.getInstance()
                .createNew(CourseService.class)
                .getOrderPayInfo(requestBodyPay)
                .compose(TransformUtils.<ResultBean<PayInfoBean>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<PayInfoBean>() {
                    @Override
                    public void onNext(PayInfoBean payInfoBean) {
                        getView().getPayInfoSuccess(payInfoBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().getPayInfoFailure();
                    }
                }, mActivity, false));
    }

    /**
     * 取消订单
     */

    public void toCancelIndent(final IndentBean indentBean) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(indentBean.getOrderId()));
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .cancelIndent(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().cancelSuccess(indentBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().cancelError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }

    /**
     * 取消退课
     */

    public void toCancelCourse(final IndentBean indentBean) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("orderId", String.valueOf(indentBean.getOrderId()));
        String json = new Gson().toJson(requestParams);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscription = RestApi.getInstance()
                .createNew(AccountService.class)
                .cancelCourse(requestBody)
                .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                    @Override
                    public void onNext(Object o) {
                        getView().cancelSuccess(indentBean);
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        getView().cancelError(msg);
                    }
                }, mActivity, false));
        addSubscribe(subscription);
    }


}
