package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.MessageAdapter;
import com.sdzn.pkt.student.phone.bean.MessageBean;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.mvp.presenter.MessagePresenter;
import com.sdzn.pkt.student.phone.mvp.view.MessageView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 描述：消息列表
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class MessageActivity extends BaseMVPActivity<MessageView, MessagePresenter> implements MessageView, OnRefreshLoadmoreListener, BaseRcvAdapter.OnItemClickListener {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.swipe_target)
    RecyclerView recyclerMessage;
    @BindView(R.id.ll_selectall)
    LinearLayout llSelectall;
    @BindView(R.id.tv_del)
    TextView tvDel;
    @BindView(R.id.rl_compile)
    RelativeLayout rlCompile;
    TextView edit;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.img_checkbox)
    ImageView imgCheckbox;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    private MessageAdapter messageAdapter;
    private List<MessageBean.LetterListBean> mData = new ArrayList<>();
    private List<String> ids = new ArrayList<>();//存储id用于批量删除
    private List<MessageBean.LetterListBean> mDelData = new ArrayList<>();//存储用于批量删除bean
    private boolean isOkShow = false;//默认是编辑字体，当完成出现时设为true，进入编辑状态
    private int pageIndex = 1;
    private int pageSize = 20;
    private String messageType;//消息的类型


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_message;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initView();
        initData();

    }

    private void initData() {
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    private void initTitleRight() {
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOkShow) {
                    //当点击的是编辑时
                    edit.setText("完成");
                    ids.clear();
                    refreshLayout.setEnableRefresh(false);
                    refreshLayout.setEnableLoadmore(false);
                    for (MessageBean.LetterListBean bean : mData) {
                        bean.setSelected(false);
                    }
                    rlCompile.setVisibility(View.VISIBLE);
                    messageAdapter.setEdit(true);

                } else {//当点击的是完成时
                    edit.setText("编辑");
                    refreshLayout.setEnableRefresh(true);
                    refreshLayout.setEnableLoadmore(true);
                    rlCompile.setVisibility(View.GONE);
                    messageAdapter.setEdit(false);
                    imgCheckbox.setSelected(false);
                }
                isOkShow = !isOkShow;
                messageAdapter.notifyDataSetChanged();

            }
        });
    }

    private void initView() {
        edit = (TextView) titleBar.getView(R.id.tv_right);

        tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.delMessage(ids);

            }
        });
        recyclerMessage.setLayoutManager(new LinearLayoutManager(mContext));
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageIndex = 1;
                initData();
            }
        });
        messageAdapter = new MessageAdapter(mContext, mData);
        recyclerMessage.setAdapter(messageAdapter);
        refreshLayout.setOnRefreshLoadmoreListener(this);
        messageAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
//        pageIndex++;
//        mPresenter.getMessageList(pageIndex, pageSize);
        ToastUtils.showShort("没有更多消息了");
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        pageIndex = 1;
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    @OnClick(R.id.ll_selectall)
    public void onViewClicked() {
        if (imgCheckbox.isSelected()) {
            //是全选时
            ids.clear();
            imgCheckbox.setSelected(false);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(false);
            }
        } else {
            //非全选时
            for (MessageBean.LetterListBean bean : mData) {
                ids.add(String.valueOf(bean.getId()));
            }
            imgCheckbox.setSelected(true);
            for (MessageBean.LetterListBean bean : mData) {
                bean.setSelected(true);
            }
        }
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    protected MessagePresenter createPresenter() {
        return new MessagePresenter();
    }

    @Override
    public void listMessage(List<MessageBean.LetterListBean> messageList) {
        if (messageList != null && messageList.size() > 0) {
            titleBar.getView(R.id.tv_right).setVisibility(View.VISIBLE);
            initTitleRight();
            if (pageIndex == 1) {
                ids.clear();
                mData.clear();
            }
            mData.addAll(messageList);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            messageAdapter.notifyDataSetChanged();
        } else {
            if (pageIndex == 1) {
                titleBar.getView(R.id.tv_right).setVisibility(View.GONE);
                emptyLayout.setErrorType(EmptyLayout.NODATA);
            } else {
                ToastUtils.showShort("没有更多消息了");
            }
        }
        goneSwipView();
        mDelData.clear();

    }

    @Override
    public void onListError(String msg) {
        if (pageIndex == 1) {
            mData.clear();
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        }
        goneSwipView();
    }

    @Override
    public void delMessage() {
//           mData.removeAll(mDelData);
        pageIndex = 1;
        mPresenter.getMessageList(pageIndex, pageSize);
    }

    @Override
    public void delMessageOnError(String msg) {
        ToastUtils.showShort(msg);

    }

    @Override
    public void delAll() {

    }

    @Override
    public void delAllOnError(String msg) {

    }

    //隐藏刷新布局或者底部加载更多的布局
    private void goneSwipView() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore(false);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (isOkShow) {
            //焦点应该作用在item的选择框上
            mData.get(position).setSelected(!mData.get(position).isSelected());
            messageAdapter.notifyDataSetChanged();
            if (mData.get(position).isSelected()) {
                ids.add(String.valueOf(mData.get(position).getId()));
                mDelData.add(mData.get(position));
            } else {
                ids.remove(String.valueOf(mData.get(position).getId()));
                mDelData.remove(mData.get(position));
            }
            //判断是否全选
            boolean isAll = true;
            for (MessageBean.LetterListBean bean : mData) {
                if (!bean.isSelected()) {
                    isAll = false;
                }

            }
            imgCheckbox.setSelected(isAll);
        } else {
            mData.get(position).setStatus("1");//进入详情标记为已读
            switch (mData.get(position).getType()) {
                case 0:
                    messageType = "系统消息";
                    break;
                case 2:
                    messageType = "站内信";
                    break;
                case 5:
                    messageType = "课程消息";
                    break;
                case 6:
                    messageType = "优惠券过期";
                    break;
                default:
                    break;
            }
            IntentController.toMessageStatic(mContext, String.valueOf(mData.get(position).getId()), messageType,mData.get(position).getContent(),mData.get(position).getAddTime());
            messageAdapter.notifyDataSetChanged();//表示已读消息
        }
    }
}
