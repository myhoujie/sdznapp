package com.sdzn.pkt.student.phone.bean;

public class BannerInfoBean {


    /**
     * createTime : null
     * id : 26
     * imageName :
     * imageUrl : https://file.znclass.com/1595467105083
     * isDelete : 0
     * linkAddress : https://www.baidu.com/
     * moduleId : 2
     * moduleName : app端
     * relationType : 0
     * sellType :
     * seriesNumber : 1
     * title : appbanner1
     */

    private Object createTime;
    private int id;
    private String imageName;
    private String imageUrl;
    private int isDelete;
    private String linkAddress;
    private int moduleId;
    private String moduleName;
    private String relationType;
    private String sellType;
    private int seriesNumber;
    private String title;

    public Object getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Object createTime) {
        this.createTime = createTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(int seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
    /**
     * color : #cee7ff
     * describe :
     * imageId : 284
     * imagesUrl : /images/upload/image/20161201/1480559338591.jpg
     * linkAddress : /front/showcoulist
     * previewUrl : /images/upload/image/20150915/1442313577205.jpg
     * seriesNumber : 2
     * title : 首页banner图片02
     * typeId : 1
     */

  /**  private String color;
    private String describe;
    private int imageId;



    private String imageUrl;
    private String linkAddress;
    private String previewUrl;
    private int seriesNumber;
    private String title;
    private String typeId;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(int seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}*/