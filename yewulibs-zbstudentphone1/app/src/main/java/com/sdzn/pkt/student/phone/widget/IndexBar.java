package com.sdzn.pkt.student.phone.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.pkt.student.phone.R;

/**
 * 描述：
 * - 首字母索引布局
 * 创建人：baoshengxiang
 * 创建时间：2017/9/13
 */
public class IndexBar extends View {
    private Context mContext;
    private static final String[] INDEX_LETTERS = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "#"};
    private int choose = -1;// 选中
    private Paint paint = new Paint();
    private TextView mTextDialog;
    // 触摸事件
    private OnTouchingLetterChangedListener onTouchingLetterChangedListener;

    public void setTextView(TextView mTextDialog) {
        this.mTextDialog = mTextDialog;
    }


    public IndexBar(Context context) {
        this(context, null);
    }

    public IndexBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndexBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
    }

    /**
     * 重写这个方法
     */
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 获取焦点改变背景颜色.
        int height = getHeight();// 获取对应高度
        int width = getWidth(); // 获取对应宽度
        int singleHeight = height / INDEX_LETTERS.length;// 获取每一个字母的高度

        for (int i = 0; i < INDEX_LETTERS.length; i++) {
            paint.setColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setAntiAlias(true);
            paint.setTextSize(ConvertUtils.dp2px(mContext, 10));
            // 选中的状态
            if (i == choose) {
                paint.setColor(ContextCompat.getColor(mContext, R.color.gray_ea));
                paint.setFakeBoldText(true);
            }
            // x坐标等于中间-字符串宽度的一半.
            float xPos = (width - paint.measureText(INDEX_LETTERS[i])) / 2;
            float yPos = singleHeight * i + (singleHeight + paint.measureText(INDEX_LETTERS[i])) / 2;
            canvas.drawText(INDEX_LETTERS[i], xPos, yPos, paint);

            paint.reset();// 重置画笔
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();// 点击y坐标
        final int oldChoose = choose;
        final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;
        final int c = (int) (y / getHeight() * INDEX_LETTERS.length);// 点击y坐标所占总高度的比例*b数组的长度就等于点击b中的个数.

        if (action == MotionEvent.ACTION_UP) {
            setBackgroundColor(ContextCompat.getColor(mContext, R.color.background));
            choose = -1;//
            invalidate();
            if (mTextDialog != null) {
                mTextDialog.setVisibility(View.INVISIBLE);
            }
        } else {
            setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            if (oldChoose != c) {
                if (c >= 0 && c < INDEX_LETTERS.length) {
                    if (listener != null) {
                        listener.onTouchingLetterChanged(INDEX_LETTERS[c]);
                    }
                    if (mTextDialog != null) {
                        mTextDialog.setText(INDEX_LETTERS[c]);
                        mTextDialog.setVisibility(View.VISIBLE);
                    }
                    choose = c;
                    invalidate();
                }
            }


        }
        return true;
    }

    /**
     * 向外公开的方法
     *
     * @param onTouchingLetterChangedListener
     */
    public void setOnTouchingLetterChangedListener(
            OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
    }

    /**
     * 接口
     *
     * @author coder
     */
    public interface OnTouchingLetterChangedListener {
        void onTouchingLetterChanged(String s);
    }

}