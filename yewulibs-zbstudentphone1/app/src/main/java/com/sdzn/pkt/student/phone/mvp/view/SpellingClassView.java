package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.SubjectBean;

import java.util.List;

public interface SpellingClassView extends BaseView {
    void onSubjectSuccess(List<SubjectBean> subjectList);

    void onSubjectFailed(String msg);
}
