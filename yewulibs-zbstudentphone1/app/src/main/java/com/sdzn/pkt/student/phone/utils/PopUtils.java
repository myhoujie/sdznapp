package com.sdzn.pkt.student.phone.utils;

import android.app.Activity;
import android.content.Context;
import android.view.WindowManager;

/**
 * 描述：
 * - 弹出pop北京管理
 * 创建人：baoshengxiang
 * 创建时间：2017/7/7
 */
public class PopUtils {
    /**
     * 设置添加屏幕的背景透明度  1,：全透明；0.5：半透明  0~1，取自己想到的透明度
     *
     * @param bgAlpha
     */
    public static void backgroundAlpha(Context mContext, float bgAlpha) {
        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        ((Activity) mContext).getWindow().setAttributes(lp);
        if (bgAlpha < 1f) {
            ((Activity) mContext).getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        } else {
            ((Activity) mContext).getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
    }
}
