package com.sdzn.pkt.student.phone.widget.ActionSheet;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ScreenUtil;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.pkt.student.phone.R;

import java.util.ArrayList;
import java.util.List;

public class ActionSheetDialog implements BaseRcvAdapter.OnItemClickListener {
    private Context mContext;
    private Dialog dialog;
    private TextView tvTitle;
    private TextView tvCancel;
    private RecyclerView rcvContent;
    private List<SheetItem> sheetItemList;
    private ActionSheetAdapter actionSheetAdapter;
    private BaseRcvAdapter.OnItemClickListener onItemClickListener;

    public ActionSheetDialog(Context context, List<SheetItem> datas) {
        this.mContext = context;
        if (datas == null) {
            this.sheetItemList = new ArrayList<>();
        } else {
            this.sheetItemList = datas;
        }

    }

    public ActionSheetDialog builder() {
        // 获取Dialog布局
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_action_sheet, null);
        // 设置Dialog最小宽度为屏幕宽度
        view.setMinimumWidth(ScreenUtil.getScreenWidth(mContext));
        // 获取自定义Dialog布局中的控件
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        rcvContent = (RecyclerView) view.findViewById(R.id.rcv_content);
        tvCancel = (TextView) view.findViewById(R.id.txt_cancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // 定义Dialog布局和参数
        dialog = new Dialog(mContext, R.style.ActionSheetDialogStyle);
        dialog.setContentView(view);
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            dialogWindow.setGravity(Gravity.LEFT | Gravity.BOTTOM);
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.x = 0;
            lp.y = 0;
            dialogWindow.setAttributes(lp);
        }
        return this;
    }

    public void setShowTitle(boolean showTitle) {
        tvTitle.setVisibility(showTitle ? View.VISIBLE : View.GONE);
    }

    public ActionSheetDialog setTitle(String title) {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
        return this;
    }

    public ActionSheetDialog setCancelable(boolean cancel) {
        dialog.setCancelable(cancel);
        return this;
    }

    public ActionSheetDialog setCanceledOnTouchOutside(boolean cancel) {
        dialog.setCanceledOnTouchOutside(cancel);
        return this;
    }

    /**
     * @param strItem  条目名称
     * @param color    条目字体颜色，设置null则默认蓝色
     * @param listener
     * @return
     */
    public ActionSheetDialog addSheetItem(String strItem, SheetItemColor color, OnSheetItemClickListener listener) {
        sheetItemList.add(new SheetItem(strItem, color, listener));
        return this;
    }

    public ActionSheetDialog addSheetItem(BaseRcvAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return this;
    }

    /**
     * 设置条目布局
     */
    private void setSheetItems() {
        if (sheetItemList == null || sheetItemList.size() <= 0) {
            return;
        }
        int size = sheetItemList.size();
        // 添加条目过多的时候控制高度
        if (size >= 7) {
            ViewGroup.LayoutParams params = rcvContent.getLayoutParams();
            params.height = ScreenUtil.getScreenHeight(mContext) / 2;
            rcvContent.setLayoutParams(params);
        }
        actionSheetAdapter = new ActionSheetAdapter(mContext, sheetItemList);
        actionSheetAdapter.setOnItemClickListener(this);
        rcvContent.setLayoutManager(new LinearLayoutManager(mContext));
        rcvContent.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ContextCompat.getColor(mContext, R.color.gray_ea), 1));
        rcvContent.setAdapter(actionSheetAdapter);
    }

    public void show() {
        setSheetItems();
        dialog.show();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(view, position);
        }
        OnSheetItemClickListener itemClickListener = sheetItemList.get(position).getItemClickListener();
        if (itemClickListener != null) {
            itemClickListener.onClick(position);
        }
        dialog.dismiss();
    }
}