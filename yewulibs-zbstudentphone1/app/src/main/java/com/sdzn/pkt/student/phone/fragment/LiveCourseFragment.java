package com.sdzn.pkt.student.phone.fragment;


import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.sdzn.core.base.BaseFragment;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;

/**
 * zs
 * <p>
 * 学校课程
 */
public class LiveCourseFragment extends BaseFragment {

    @BindView(R.id.title_bar)
    TitleBar titleBar;

    public LiveCourseFragment() {

    }

    public static LiveCourseFragment newInstance() {
        return new LiveCourseFragment();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_live;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {

        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPManager.autoLogin(mContext)) {
                    IntentController.toLogin(mContext);
                    return;
                }
                IntentController.toShoppingCart(mContext);
            }
        });

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_live_container, CourseFragment.newInstance(CourseCons.Type.LIVING, null)).commit();
    }

}
