package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.TeacherListBean;
import com.sdzn.pkt.student.phone.network.api.ApiInterface;

import java.util.List;

/**
 * 描述：
 * - 课程详情老师列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class CourseTeacherAdapter extends BaseRcvAdapter<TeacherListBean> {
    public CourseTeacherAdapter(Context context, List mList) {
        super(context, R.layout.item_course_teacher, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, TeacherListBean teacherListBean) {
        holder.setImageView(R.id.iv_avatar, "" + teacherListBean.getPicPath(), R.mipmap.ic_avatar);
        holder.setText(R.id.tv_username, teacherListBean.getName());
        holder.setText(R.id.tv_subject, teacherListBean.getSubjectName());
        holder.setText(R.id.tv_teacher_info, teacherListBean.getCareer());
    }
}
