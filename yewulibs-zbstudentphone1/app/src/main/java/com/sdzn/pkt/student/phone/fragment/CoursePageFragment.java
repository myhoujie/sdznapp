package com.sdzn.pkt.student.phone.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baijiayun.live.ui.LiveSDKWithUI;
import com.baijiayun.livecore.LiveSDK;
import com.baijiayun.livecore.context.LPConstants;
import com.baijiayun.videoplayer.ui.playback.PBRoomUI;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.MineCourseAdapter;
import com.sdzn.pkt.student.phone.adapter.MineRecentlyCourseAdapter;
import com.sdzn.pkt.student.phone.adapter.MineTodyCourseAdapter;
import com.sdzn.pkt.student.phone.bean.MineList;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.event.MineCourseEvent;
import com.sdzn.pkt.student.phone.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.MineCoursePresenter;
import com.sdzn.pkt.student.phone.mvp.view.MineCourseView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.EmptySchoolLayout;
import com.sdzn.pkt.student.phone.widget.SimpleDividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 描述：我的课程中单个模块
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class CoursePageFragment extends BaseMVPFragment<MineCourseView, MineCoursePresenter> implements MineCourseView, BaseRcvAdapter.OnItemClickListener, OnRefreshLoadmoreListener {
    public static final String ARGS_TYPE = "args_page";
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";
    public static final String TYPE_COURSE = "303";
    public static final String TYPE_COURSE_OVER = "304";

    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    @BindView(R.id.swipe_target)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    private MineCourseAdapter mineCourseAdapter;
    private List<MineList> mDataMyCourse = new ArrayList<>();
    private MineRecentlyCourseAdapter recentlyCourseAdapter;
    private List<MineList> mDataRecentlyCourse = new ArrayList<>();
    private MineTodyCourseAdapter todyCourseAdapter;
    private List<MineList> mDataTodyCourse = new ArrayList<>();
    private String state;
    private int pageIndex = 1;//当前页
    private int pageSize = 10;//

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            state = getArguments().getString(ARGS_TYPE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        if (orderPayEvent.isSuccess()) {
            pageIndex = 1;
            initData();
        }
    }

    public static CoursePageFragment newInstance(String page) {
        Bundle args = new Bundle();
        args.putString(ARGS_TYPE, page);
        CoursePageFragment fragment = new CoursePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_page;

    }

    @Override
    protected MineCoursePresenter createPresenter() {
        return new MineCoursePresenter();
    }


    @Override
    protected void onInit(Bundle savedInstanceState) {
        initView();
    }

    @Override
    protected void lazyLoad() {
        initData();
        isFirst = false;
    }

    private Map<String, String> getParms() {
        Map<String, String> params = new HashMap<>();
        if (state.equals(TYPE_COURSE) || state.equals(TYPE_COURSE_OVER)) {
            params.put("sellType", "ALL");
            if (state.equals(TYPE_COURSE)) {
                params.put("type", "2");
            } else if (state.equals(TYPE_COURSE_OVER)) {//过期  1    全部课程 2
                params.put("type", "1");

            }
        }
        params.put("index", String.valueOf(pageIndex));
        params.put("size", String.valueOf(pageSize));

        return params;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isFirst && TYPE_TODY.equalsIgnoreCase(state)) {
            mPresenter.getTodyCourse(getParms());
        }
    }

    private void initData() {
        if (SPManager.autoLogin(mContext)) {
            switch (state) {
                case TYPE_TODY:
                    mPresenter.getTodyCourse(getParms());
                    break;
                case TYPE_RECENTLY:
                    mPresenter.getRecentlyCourse(getParms());
                    break;
                case TYPE_COURSE:
                    mPresenter.getCourse(getParms(), 2);
                    break;
                case TYPE_COURSE_OVER:
                    mPresenter.getCourse(getParms(), 1);
                    break;
                default:
                    break;
            }
        }

    }

    private void initView() {
        switch (state) {
            case TYPE_TODY:
                todyCourseAdapter = new MineTodyCourseAdapter(mContext, mDataTodyCourse);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
                recyclerView.setAdapter(todyCourseAdapter);
//                todyCourseAdapter.setOnItemClickListener(this);
                todyCourseAdapter.setListener(new MineTodyCourseAdapter.LivePlayerListener() {
                    @Override
                    public void ontoDetailLive(String status, String kpoint, String courseId) {
                        switch (status) {
                            case "4":
                                mPresenter.getLivingInfo(kpoint);
                                break;
                            case "6":
                                mPresenter.getReplayInfo(kpoint, courseId);
                                break;
                            default:
                                break;
                        }
                    }
                });
                todyCourseAdapter.setOnItemClickListener(this);
                break;
            case TYPE_RECENTLY:
                recentlyCourseAdapter = new MineRecentlyCourseAdapter(mContext, mDataRecentlyCourse);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
                recyclerView.setAdapter(recentlyCourseAdapter);
                recentlyCourseAdapter.setOnItemClickListener(this);
                break;
            case TYPE_COURSE:
                mineCourseAdapter = new MineCourseAdapter(mContext, mDataMyCourse, 2);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//        recyclerVideo.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
                recyclerView.setAdapter(mineCourseAdapter);
                mineCourseAdapter.setOnItemClickListener(this);
                break;
            case TYPE_COURSE_OVER:
                mineCourseAdapter = new MineCourseAdapter(mContext, mDataMyCourse, 1);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.addItemDecoration(SimpleDividerItemDecoration.createVertical(getContext(), Color.parseColor("#e5e5e5"), 1));
                recyclerView.setAdapter(mineCourseAdapter);
//                mineCourseAdapter.setOnItemClickListener(this);
                break;
            default:
                break;
        }

        refreshLayout.setOnRefreshLoadmoreListener(this);
        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lazyLoad();
            }
        });
    }


    @Override
    public void onItemClick(View view, int position) {
        //判断当前的item是点播还是直播的
        switch (state) {
            case TYPE_TODY:
                MineList mineListToday = mDataTodyCourse.get(position);
//                直接去直播
                IntentController.toCourseDetail(mContext, "COURSE", mineListToday.getCourseId(), true);
//                IntentController.toCourseDetail(mContext, mineListToday.getCourseType(), mineListToday.getCourseId(), true); //有组合非组合之分才能跳转
                break;
            case TYPE_RECENTLY:
                MineList mineListRecent = mDataRecentlyCourse.get(position);
                IntentController.toCourseDetail(mContext, mineListRecent.getCourseType(), mineListRecent.getCourseId(), true);
                break;
            case TYPE_COURSE:
                MineList mineCourseBean = mDataMyCourse.get(position);
                IntentController.toCourseDetail(mContext, mineCourseBean.getCourseType(), mineCourseBean.getCourseId(), true);
                break;
            case TYPE_COURSE_OVER:
//                ToastUtils.showShort("课程失效，不能进入！");
                break;
            default:
                break;
        }


    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        pageIndex++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        lazyLoad();
    }

//    @Override
//    public void listCourseSuccess(List<MineCourseBean> list) {
//        if (list != null) {
//            if (pageIndex == 1) {
//                mData.clear();
//            }
//            mData.addAll(list);//sellType="PACKAGE", title="接口测试", packageType=1
//            mineCourseAdapter.notifyDataSetChanged();
//            refreshLayout.setLoadmoreFinished(list.size() < pageSize);
//        }
//        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//        goneSwipView();
//    }

    @Override
    public void listTodyCourseSuccess(List<MineList> list) {
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataTodyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataTodyCourse.addAll(list);
        todyCourseAdapter.notifyDataSetChanged();
    }

    @Override
    public void listRecentlyCourseSuccess(List<MineList> list) {
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataRecentlyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataRecentlyCourse.addAll(list);
        recentlyCourseAdapter.notifyDataSetChanged();

    }

    @Override
    public void listCourseSuccess(List<MineList> list, int type) {//1过期
        clearLoingState();
        if (pageIndex == 1) {
            this.mDataMyCourse.clear();
            if (list.size() == 0) {
                emptyLayout.setErrorType(EmptySchoolLayout.NODATA);
                return;
            }
        }
        emptyLayout.setErrorType(EmptySchoolLayout.HIDE_LAYOUT);
        refreshLayout.setLoadmoreFinished(list.size() < pageSize);
        this.mDataMyCourse.addAll(list);
        mineCourseAdapter.notifyDataSetChanged();


    }

    @Override
    public void listCourseEmpty() {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
            refreshLayout.setLoadmoreFinished(true);
        }
        clearLoingState();
    }

    @Override
    public void listCourseError(String msg) {
        if (pageIndex == 1) {
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else {
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
        clearLoingState();
    }

    @Override
    public void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        LiveSDKWithUI.enterRoomWithVerticalTemplate(mContext, Long.valueOf(liveRoomBean.getRoom_id().trim()), liveRoomBean.getSign(), new LiveSDKWithUI.LiveRoomUserModel(liveRoomBean.getUser_info().getUserName(), liveRoomBean.getUser_info().getUserAvatar(), String.valueOf(liveRoomBean.getUser_info().getUserNumber()), LPConstants.LPUserType.Student), new LiveSDKWithUI.LiveSDKEnterRoomListener() {
            @Override
            public void onError(String msg) {

            }
        });
    }

    /**
     * 回放
     */
    @Override
    public void getReplayInfoSuccess(NewVideoInfo info) {
        LiveSDK.customEnvironmentPrefix = "b96152240";
        PBRoomUI.enterPBRoom(getActivity(), info.getRoomId(), info.getToken(), "0", new PBRoomUI.OnEnterPBRoomFailedListener() {

            @Override
            public void onEnterPBRoomFailed(String msg) {
                ToastUtils.showShort(msg);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
        if (userInfoEvent.getName().equals(UpdateAccountEvent.CHANGE_PHASE)) {//刷新
            if (SPManager.autoLogin(mContext)) {
                lazyLoad();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateCourse(MineCourseEvent courseEvent) {
        if (courseEvent.isStatus()) {//刷新
            if (SPManager.autoLogin(mContext)) {
                lazyLoad();
            }
        }
    }

    //隐藏刷新布局或者底部加载更多的布局
    private void clearLoingState() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
        if (refreshLayout.isLoading()) {
            refreshLayout.finishLoadmore();
        }
    }
}