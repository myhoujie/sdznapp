package com.sdzn.pkt.student.phone.application;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.baijiayun.BJYPlayerSDK;
import com.haier.cellarette.libwebview.BuildConfig;
import com.haier.cellarette.libwebview.hois2.HiosHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.sdzn.core.base.BaseApplication;
import com.sdzn.core.utils.AppUtils;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.pkt.student.phone.manager.CrashHandler;
import com.sdzn.pkt.student.phone.manager.DaoManger;
import com.sdzn.pkt.student.phone.widget.ProgressHeader;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;

import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.onAdaptListener;
import me.jessyan.autosize.utils.AutoSizeLog;

//import com.tencent.bugly.crashreport.CrashReport;

public class App extends BaseApplication {
    public static App mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

//        initLog();//初始化日志配置
//        initCrashHandler();//全局捕获异常
        initUmeng();
        initRefreshFram(); //初始化下拉刷新控件
        DaoManger.getInstance();//初始化GreenDao
        new BJYPlayerSDK.Builder(this)
                .setDevelopMode(false)
                .setCustomDomain("b96152240")
                .build();
        configHios();
        initBugly();
        initAutoSize();
    }

    private void initAutoSize() {
        AutoSizeConfig.getInstance()

                //是否让框架支持自定义 Fragment 的适配参数, 由于这个需求是比较少见的, 所以须要使用者手动开启
                //如果没有这个需求建议不开启
                .setCustomFragment(true)

                //是否屏蔽系统字体大小对 AndroidAutoSize 的影响, 如果为 true, App 内的字体的大小将不会跟随系统设置中字体大小的改变
                //如果为 false, 则会跟随系统设置中字体大小的改变, 默认为 false
                .setExcludeFontScale(true)

                //区别于系统字体大小的放大比例, AndroidAutoSize 允许 APP 内部可以独立于系统字体大小之外，独自拥有全局调节 APP 字体大小的能力
                //当然, 在 APP 内您必须使用 sp 来作为字体的单位, 否则此功能无效, 不设置或将此值设为 0 则取消此功能
//                .setPrivateFontScale(0.8f)

                //屏幕适配监听器
                .setOnAdaptListener(new onAdaptListener() {
                    @Override
                    public void onAdaptBefore(Object target, Activity activity) {
                        //使用以下代码, 可以解决横竖屏切换时的屏幕适配问题
                        //使用以下代码, 可支持 Android 的分屏或缩放模式, 但前提是在分屏或缩放模式下当用户改变您 App 的窗口大小时
                        //系统会重绘当前的页面, 经测试在某些机型, 某些情况下系统不会重绘当前页面, ScreenUtils.getScreenSize(activity) 的参数一定要不要传 Application!!!
//                        AutoSizeConfig.getInstance().setScreenWidth(ScreenUtils.getScreenSize(activity)[0]);
//                        AutoSizeConfig.getInstance().setScreenHeight(ScreenUtils.getScreenSize(activity)[1]);
                        AutoSizeLog.d(String.format(Locale.ENGLISH, "%s onAdaptBefore!", target.getClass().getName()));
                    }

                    @Override
                    public void onAdaptAfter(Object target, Activity activity) {
                        AutoSizeLog.d(String.format(Locale.ENGLISH, "%s onAdaptAfter!", target.getClass().getName()));
                    }
                })

                //是否打印 AutoSize 的内部日志, 默认为 true, 如果您不想 AutoSize 打印日志, 则请设置为 false
                .setLog(false)

        //是否使用设备的实际尺寸做适配, 默认为 false, 如果设置为 false, 在以屏幕高度为基准进行适配时
        //AutoSize 会将屏幕总高度减去状态栏高度来做适配
        //设置为 true 则使用设备的实际屏幕高度, 不会减去状态栏高度
        //在全面屏或刘海屏幕设备中, 获取到的屏幕高度可能不包含状态栏高度, 所以在全面屏设备中不需要减去状态栏高度，所以可以 setUseDeviceSize(true)
//                .setUseDeviceSize(true)

        //是否全局按照宽度进行等比例适配, 默认为 true, 如果设置为 false, AutoSize 会全局按照高度进行适配
//                .setBaseOnWidth(false)

        //设置屏幕适配逻辑策略类, 一般不用设置, 使用框架默认的就好
//                .setAutoAdaptStrategy(new AutoAdaptStrategy())
        ;
    }


    private void initBugly() {

        String packageName = AppUtils.getAppPackageName(mContext);
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());

        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setAppVersion(AppUtils.getAppVersionName(mContext));
        strategy.setAppPackageName(packageName);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));

        //正式发布 false    测试true
//        CrashReport.initCrashReport(this, "d65405595d", true, strategy);
        Bugly.init(this, "d65405595d", true, strategy);
    }

    private void configHios() {
        HiosHelper.config(com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".slbapp.ad.web.page", com.blankj.utilcode.util.AppUtils.getAppPackageName() + ".slbapp.web.page");

    }

    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initCrashHandler() {
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init();
    }
//    private void initBugly(){
//        CrashReport.initCrashReport(getApplicationContext(),"" , false);
//    }

    /**
     * 友盟分享
     */
    private void initUmeng() {
//        //推送
//        UMConfigure.init(this,null,null,UMConfigure.DEVICE_TYPE_PHONE,"471615d3904c021efd921e046b90e4f6");
//        PushAgent mPushAgent = PushAgent.getInstance(this);
//        //注册推送服务，每次调用register方法都会回调该接口
//        mPushAgent.register(new IUmengRegisterCallback() {
//            @Override
//            public void onSuccess(String deviceToken) {
//                //注册成功会返回device token
//                LogUtils.i("Token = "+ deviceToken);
//            }
//
//            @Override
//            public void onFailure(String s, String s1) {
//            }
//        });
//
//        PlatformConfig.setWeixin(Config.WX_APP_ID, Config.WX_APP_KEY);
//        PlatformConfig.setQQZone(Config.QQ_ZOOM_APP_ID, Config.QQ_APP_KEY);
//        UMShareAPI.get(this);
    }


    //设置全局的Header构建器
    private void initRefreshFram() {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @NonNull
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                return new ProgressHeader(context).setSpinnerStyle(SpinnerStyle.Translate);
            }
        });
    }

    private void initLog() {
        new LogUtils.Builder(this)
                .setGlobalTag(getPackageName())
                .setBorderSwitch(false)
                .setLogSwitch(BuildConfig.DEBUG);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1) {
            getResources();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Resources getResources() {
        Resources resources = super.getResources();
        if (resources.getConfiguration().fontScale != 1) {
            Configuration configuration = new Configuration();
            configuration.setToDefaults();
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return resources;
    }
}