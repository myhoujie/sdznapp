package com.sdzn.pkt.student.phone.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.activity.MineTaskActivity;
import com.sdzn.pkt.student.phone.activity.WebStatisticsActivity;
import com.sdzn.pkt.student.phone.activity.WebTaskActivity;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.event.MsgCountEvent;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.event.UpdateAvatarEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.UserInfoPresenter;
import com.sdzn.pkt.student.phone.mvp.view.UserInfoView;
import com.sdzn.pkt.student.phone.utils.GradeIdToNameUtils;
import com.sdzn.pkt.student.phone.widget.RoundRectImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.http.Body;

/**
 * 描述：个人信息
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class UserInfoFragment extends BaseMVPFragment<UserInfoView, UserInfoPresenter> implements UserInfoView {

    @BindView(R.id.img_avatar)
    RoundRectImageView imgAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_class)
    TextView tvUserClass;
    @BindView(R.id.ll_indent)
    TextView llIndent;
    @BindView(R.id.ll_collect)
    TextView llCollect;
    @BindView(R.id.ll_notes)
    TextView llNotes;
    @BindView(R.id.ll_account)
    TextView llAccount;
    @BindView(R.id.ll_coupon)
    TextView llCoupon;
    @BindView(R.id.iv_point)
    ImageView ivPoint;
    @BindView(R.id.iv_go_info)
    ImageView ivGoInfo;
    @BindView(R.id.ll_zuoye)
    TextView llZuoye;
    @BindView(R.id.ll_statistics)
    TextView llStatistics;
    UserBean userBean;


    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_user_info;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();

    }

    private void initData() {

    }

    private void initView() {
        if (SPManager.autoLogin(mContext)){

            ivGoInfo.setVisibility(View.VISIBLE);
            GlideImgManager.loadAvatarImage(mContext, "" + SPManager.getUser().getPicImg(), imgAvatar);
                if (SPManager.isToCLogin()) {
                    tvUserClass.setText( "" + GradeIdToNameUtils.setName(SPManager.getUser().getSubjectId(),SPManager.getUser().getGrade()));
//                    llZuoye.setVisibility(View.GONE);
                    if (SPManager.getUser().getStudentName()==null||SPManager.getUser().getStudentName().isEmpty()){
                        tvUserName.setText(SPManager.getUser().getMobile()+"");
                    }else {
                        tvUserName.setText(SPManager.getUser().getStudentName()+"");
                    }
                }else {
                    tvUserClass.setText( "" + GradeIdToNameUtils.setName(SPManager.getSchoolSectionId(),SPManager.getSchoolgradeId()));
//                    llZuoye.setVisibility(View.VISIBLE);
                    tvUserName.setText(SPManager.getUser().getStudentName()+"");
                }
        }else {
            tvUserName.setText(getString(R.string.string_user_title));
            tvUserClass.setText(getString(R.string.string_user_content));
            GlideImgManager.loadImage(mContext, R.mipmap.ic_user_avatar, imgAvatar);
            ivGoInfo.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.ll_coupon, R.id.img_avatar, R.id.ll_indent, R.id.ll_collect, R.id.ll_notes,R.id.ll_zuoye,
            R.id.ll_account, R.id.tv_system_settings, R.id.tv_message,R.id.rl_to_info,R.id.ll_statistics})
    public void onViewClicked(View view) {
        if (!SPManager.autoLogin(mContext)){
            IntentController.toLogin(mContext);
            return;
        }
        switch (view.getId()) {

            case R.id.ll_indent:
                IntentController.toIndent(mContext);
                break;
            case R.id.ll_collect:
                IntentController.toCollect(mContext);
                break;
            case R.id.ll_notes:
                break;
            case R.id.img_avatar:
            case R.id.ll_account:
            case R.id.rl_to_info:
                IntentController.toAccountSetting(mContext);
                break;
            case R.id.ll_coupon:
                IntentController.toCoupon(mContext, "我的优惠券");
                break;
            case R.id.tv_system_settings:
                IntentController.toSystemSetting(mContext);
                break;
            case R.id.tv_message:
                IntentController.toMessage(mContext);
                break;
            case R.id.ll_zuoye:
                Intent intent = new Intent(getActivity(), MineTaskActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_statistics:
                Intent intent1 = new Intent(getActivity(), WebStatisticsActivity.class);
                startActivity(intent1);
                break;
            default:
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            userBean = SPManager.getUser();
            GlideImgManager.loadAvatarImage(mContext, "" + userBean.getPicImg(), imgAvatar);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(UpdateAccountEvent userInfoEvent) {
            userBean = SPManager.getUser();
            initView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void queryMessageCount(MsgCountEvent countEvent) {
        ivPoint.setVisibility(0 == countEvent.getCount() ? View.GONE : View.VISIBLE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected UserInfoPresenter createPresenter() {
        return new UserInfoPresenter();
    }

    @Override
    public void loginSuccess() {

    }

    @Override
    public void onFailed() {

    }
}
