package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.LiveRoomBean;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.bean.VideoRoomBean;

/**
 * 描述：直播的和观看回放
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/7
 */

public interface LiveRoomView extends BaseView {

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void liveRoomInfoOnError(String msg);

    void getVideoRoomInfoSuccrss(NewVideoInfo videoRoomBean);

    void videoRoomInfoOnError(String msg);

    void getReplayInfoSuccess(NewVideoInfo info);

    void applySuccess(int type);


}
