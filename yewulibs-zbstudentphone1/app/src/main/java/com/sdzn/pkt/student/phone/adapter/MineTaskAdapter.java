package com.sdzn.pkt.student.phone.adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.MineTaskBean;

import java.util.List;

/**
 * 描述：我的课程列表
 * -
 * type =1  过时
 */

public class MineTaskAdapter extends BaseRcvAdapter<MineTaskBean.RecordsBean> {
    public static final String TYPE_TODY = "301";
    public static final String TYPE_RECENTLY = "302";

    public MineTaskAdapter(Context context, List<MineTaskBean.RecordsBean> mList, String type) {
        super(context, R.layout.item_mine_task, mList, type);
    }


    @SuppressLint("NewApi")
    @Override
    public void convert(BaseViewHolder holder, int position, MineTaskBean.RecordsBean mineCourseBean) {
        if (type.equals(TYPE_TODY)) {
            holder.setText(R.id.tv_class, mineCourseBean.getSubjectName() + " | " + mineCourseBean.getHomeworkName());
            if (mineCourseBean.getColorType().equals("#36DB57")) {
                holder.setBackgroundRes(R.id.tv_video_type, R.drawable.selector_btn_green_time);
            } else {
                holder.setBackgroundRes(R.id.tv_video_type, R.drawable.selector_btn_red_time);
            }
            holder.setText(R.id.tv_video_type, mineCourseBean.getRemainingTime());
            holder.setText(R.id.tv_course_content, "章节名称：" + mineCourseBean.getChapterName());
            holder.setText(R.id.tv_date_value, "截止时间：" + mineCourseBean.getCloseTime());
            holder.setText(R.id.tv_statue, mineCourseBean.getStuHomeworkStatus());
        } else {
            holder.setText(R.id.tv_class, mineCourseBean.getSubjectName() + " | " + mineCourseBean.getHomeworkName());
            holder.setVisible(R.id.tv_video_type, false);
//            holder.setText(R.id.tv_video_type, mineCourseBean.getRemainingTime());
            holder.setText(R.id.tv_course_content, "章节名称：" + mineCourseBean.getChapterName());
            holder.setText(R.id.tv_date_value, "截止时间：" + mineCourseBean.getCloseTime());
            holder.setText(R.id.tv_statue, mineCourseBean.getStuHomeworkStatus());
        }

    }
}
