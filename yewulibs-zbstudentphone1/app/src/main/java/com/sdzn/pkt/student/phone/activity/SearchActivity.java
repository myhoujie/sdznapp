package com.sdzn.pkt.student.phone.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.SearchHistoryAdapter;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.widget.ClearEditText;
import com.sdzn.pkt.student.phone.widget.flowlayout.FlowLayout;
import com.sdzn.pkt.student.phone.widget.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.sdzn.pkt.student.phone.R.id.et_search;

/**
 * 描述：
 * - 课程搜索（包含主页、直播、点播课程搜搜）
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class SearchActivity extends BaseActivity {


    @BindView(et_search)
    ClearEditText etSearch;
    @BindView(R.id.tag_search_history)
    TagFlowLayout tagSearchHistory;
    @BindView(R.id.Rl_history)
    RelativeLayout rlHistory;

    private List<String> searchHistories;
    private SearchHistoryAdapter<String> searchHistoryAdapter;

    private int courseType;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean isSearchTeacher;
    public static final String IS_SEARCH_TEACHER = "isSearchTeacher";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        initView();
    }

    private void initData() {
        isSearchTeacher = getIntent().getBooleanExtra(IS_SEARCH_TEACHER,false);
        courseType = getIntent().getIntExtra("courseType", CourseCons.Type.ALL);
        searchHistories = new ArrayList<>();
        searchHistories.addAll(isSearchTeacher?SPManager.getSearchTeacherStr(): SPManager.getSearchStr());
        if (searchHistories.isEmpty()) {
            rlHistory.setVisibility(View.GONE);
        } else {
            rlHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initView() {
        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
        etSearch.requestFocusFromTouch();
        searchHistoryAdapter = new SearchHistoryAdapter<>(mContext, searchHistories);
        tagSearchHistory.setAdapter(searchHistoryAdapter);
        tagSearchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                search(searchHistories.get(position));
                return true;
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND
                        || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        String searchStr = etSearch.getText().toString().trim();
                        search(searchStr);
                        KeyboardUtils.hideSoftInput(SearchActivity.this);
                    }
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnFocusChangedListener(new ClearEditText.onFocusChangedListener() {
            @Override
            public void onFocusChanged(View v, boolean hasFocus) {
                if (etSearch == v) {
                    if (hasFocus) {
                        KeyboardUtils.showSoftInput(mContext,etSearch);
                    }else {
                        KeyboardUtils.hideSoftInput(SearchActivity.this);
                    }
                }
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyboardUtils.showSoftInput(mContext,etSearch);
            }
        },200);
    }


    @OnClick({R.id.tv_cancel, R.id.iv_clear_history})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                KeyboardUtils.hideSoftInput(this);
                onBackPressed();
                break;
            case R.id.iv_clear_history:
                showClearDialot();
                break;
            default:
                break;
        }
    }

    private void showClearDialot() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定删除全部历史记录？")
                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        clearHistory();
                    }
                }).setCancelable(true).show();
    }

    private void search(String searchStr) {
//        if (TextUtils.isEmpty(searchStr)) {
//            ToastUtils.showShort("请输入搜索内容");
//        } else {
            rlHistory.setVisibility(View.VISIBLE);
            saveSearchStr(searchStr);
            searchHistoryAdapter.notifyDataChanged();
            if (isSearchTeacher) {
//                IntentController.toSearchTeacherResult(this,searchStr);
            }else {
                IntentController.toSearchResult(this, courseType, searchStr);
            }
//        }
    }

    private void saveSearchStr(String searchStr) {
        if (searchStr.isEmpty()){
            return;
        }
        if (searchHistories.contains(searchStr)) {
            searchHistories.remove(searchStr);
        }
        searchHistories.add(0, searchStr);

        if (searchHistories.size() > 10) {
            for(int i = searchHistories.size() - 1; i > 9; i--) {
                searchHistories.remove(i);
            }
        }

        SPManager.saveSearchStr(searchHistories);

    }

    private void clearHistory() {
        rlHistory.setVisibility(View.GONE);
        searchHistories.clear();
        if (isSearchTeacher) {
            SPManager.saveSearchTeacherStr(searchHistories);
        }else {
            SPManager.saveSearchStr(searchHistories);
        }
        searchHistoryAdapter.notifyDataChanged();
    }
}
