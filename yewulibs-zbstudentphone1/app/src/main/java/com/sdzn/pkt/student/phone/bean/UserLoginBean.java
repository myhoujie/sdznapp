package com.sdzn.pkt.student.phone.bean;

public class UserLoginBean {
    /**
     * access_token : c868aef5-6c42-49ee-88ae-6e8a9259b06a
     * domain : @student.com
     * expires_in : 15967
     * openid : 1111111
     * refresh_token : 1f678859-1d02-4bdd-bfd0-f5b5551249aa
     * scope : userProfile
     * token_type : bearer
     * userDetail : {"createTime":"2020-06-15 15:22:58","updateTime":"2020-06-15 15:23:01","id":"1111111","name":"赵森","sort":1,"schoolId":24,"schoolName":"清华大学","gender":1,"status":1,"isDelete":0,"deleteTime":"","schoolYear":2019,"avatar":"123456","gradeId":7,"gradeName":"七年级（六三制）","classInfoId":24,"className":"aaa","province":"","city":"","district":"","districtId":0,"registerSource":0,"account":"11110000","mobile":"15168802751","email":"","qq":"","weixin":"","freeznTime":"","password":"123456","zbStatus":1,"levelId":0,"levelName":"","fzxStatus":1,"educationId":0,"educationName":"","studyCoin":0}
     */

    private String access_token;
    private String domain;
    private int expires_in;
    private String openid;
    private UserBean userDetail;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public UserBean getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserBean userDetail) {
        this.userDetail = userDetail;
    }
}
