package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.UpdateAccountPresenter;
import com.sdzn.pkt.student.phone.mvp.view.UpdateAccountView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 修改姓名
 *  zs
 */
public class ChangeNameActivity extends BaseMVPActivity<UpdateAccountView, UpdateAccountPresenter> implements UpdateAccountView {

    @BindView(R.id.et_name)
    EditText etName;
    private String studentName;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_change_name;
    }

    @Override
    protected UpdateAccountPresenter createPresenter() {
        return new UpdateAccountPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        studentName = getIntent().getStringExtra("studentName");
        if (!TextUtils.isEmpty(studentName)) {
            etName.setText(studentName);
            etName.setSelection(studentName.length());
        }

    }

    @OnClick(R.id.btn_certain)
    public void onViewClicked() {
        String username = etName.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            ToastUtils.showShort("姓名不能为空");
        } else {
            mPresenter.updateUserInfo("", username,"");

        }
    }

    @Override
    public void updateAccountSuccess(UserBean userBean) {
        SPManager.saveUser(userBean);
        EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_NAME));
        ChangeNameActivity.this.finish();
    }

    @Override
    public void updateAccountFailure(String msg) {
        ToastUtils.showShort(msg);
    }
}
