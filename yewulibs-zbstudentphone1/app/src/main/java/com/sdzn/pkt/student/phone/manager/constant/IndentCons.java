package com.sdzn.pkt.student.phone.manager.constant;

/**
 * 描述：订单的状态信息
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public class IndentCons {
    public static final String INDENT_SUCCESS = "SUCCESS";//已完成
    public static final String INDENT_INIT = "INIT";//未完成
    public static final String INDENT_CANCEL = "CANCEL";//已取消
    public static final String INDENT_REFUND = "REFUND";//已退款
    public static final String INDENT_CHECKING = "CHECKING";//退款审核中
    public static final String INDENT_REFUSE = "REFUSE";//退款审核未通过
    public static final String INDENT_WAITREFUND = "WAITREFUND";//退款审核通过等待退款中

}
