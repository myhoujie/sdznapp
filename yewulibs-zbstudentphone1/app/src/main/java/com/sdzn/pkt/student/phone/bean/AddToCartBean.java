package com.sdzn.pkt.student.phone.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/25
 */
public class AddToCartBean {
    /**
     * isexsits : true
     * shopcartList : [{"addTime":"2017-07-24 22:23:48","course":{"addTime":"2017-07-24 22:23:48","classId":0,"context":"","courseId":68,"courseName":"二级诊断技能与咨询技能","currentPrice":0.01,"isavaliable":1,"lessionNum":12,"logo":"/images/upload/course/20161222/1482391768628.jpg","loseTime":"365","loseType":1,"pageBuycount":944,"pageViewcount":1812,"sellType":"COURSE","sourcePrice":0.01,"subjectId":259,"teacherList":[{"customerId":0,"id":77,"invalid":false,"isStar":1,"name":"林奇","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297987091.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":80,"invalid":false,"isStar":2,"name":"潘新强","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297935589.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":81,"invalid":false,"isStar":1,"name":"李立","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297927029.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":73,"invalid":false,"isStar":1,"name":"李诚","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442298121626.jpg","sort":0,"status":0,"subjectId":0},{"customerId":0,"id":75,"invalid":false,"isStar":1,"name":"张云唻","orgId":0,"phoneNo":0,"picPath":"/images/upload/com.sdzn.pkt.teacher.hd/20150915/1442297969808.jpg","sort":0,"status":0,"subjectId":0}],"title":"属于操作技能，考试必修课程。","updateTime":"2016-12-21 07:29:32"},"goodsid":68,"id":283,"type":1,"userid":1464},{"addTime":"2017-07-25 00:02:33","course":{"addTime":"2016-05-11 01:55:37","classId":0,"context":"<p>该套餐中包括：JDK核心API与JavaSE，SQL与Oracle。<br/><br/>JDK核心API与JavaSE（以T-DMS V1项目贯穿）：<br/><br/>JDK核心API：语言核心包、异常处理、常用工具包、集合框架。 熟练掌握JDK核心API编程技术；理解API设计原则；具备熟练的阅读API文档的能力；为后续的课程学习打下坚实的语言基础。<br/><br/>JavaSE核心：异常处理、多线程基础、IO系统、网络编程、Java反射机制、JVM性能调优（JVM内存结构剖析、GC分析及调优、JVM内存参数优化）、Java泛型、JDK新特性。熟练掌握JavaSE核心内容，特别是IO和多线程；初步具备面向对象设计和编程的能力；掌握基本的JVM优化策略。<br/><br/>SQL与Oracle（以T-DMS V2项目贯穿）：<br/><br/>SQL语言：SQL语句基础和提高、SQL语句调优。 熟练掌握SQL语句；掌握一定的数据库查询技巧及SQL语句优化技巧。<br/><\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px 0px 15px;list-style:none;border:0px;line-height:26px;white-space:normal;widows:1;\">\r\n\tOracle数据库：Oracle体系结构及系统管理、Oracle数据库高级编程、数据库设计基础。 掌握Oracle体系结构及核心编程技术。<\/p>","courseId":37,"courseName":"技术开发套餐","currentPrice":0.01,"isavaliable":1,"lessionNum":4,"logo":"/images/upload/course/20161222/1482393428320.jpg","loseTime":"90","loseType":1,"packageType":2,"pageBuycount":15,"pageViewcount":1051,"sellType":"PACKAGE","sourcePrice":168,"subjectId":224,"title":"java技术开发套餐","updateTime":"2016-12-21 07:57:09"},"goodsid":37,"id":287,"type":1,"userid":1464},{"addTime":"2017-07-25 18:36:56","course":{"addTime":"2016-06-20 10:40:12","classId":0,"context":"<p>《会计基础》重在提供会计工作的基础信息，了解会计的工作原理，掌握会计实务当中必要的理论基础知识。此次视频选择为会计基础科目里难度较高层次水平，主要章节有：会计基本概念、会计科目、会计记账法、会计凭证以及会计账簿概述，以已最简练的语言，让学员们更容易消化。<br/><br/><\/p>","courseId":39,"courseName":"会计基础视频教程全套","currentPrice":0.01,"isavaliable":1,"lessionNum":12,"logo":"/images/upload/course/20161222/1482393055128.jpg","loseTime":"365","loseType":1,"pageBuycount":26,"pageViewcount":930,"sellType":"COURSE","sourcePrice":0.01,"subjectId":251,"title":"会计基础重在提供会计工作的基础信息，了解会计的工作原理，掌握会计实务当中必要的理论基础知识。此次视频选择为会计基础科目里难度较高层次水平。","updateTime":"2016-12-23 03:13:25"},"goodsid":39,"id":290,"type":1,"userid":1464},{"addTime":"2017-07-25 23:45:25","course":{"addTime":"2016-06-21 08:33:20","classId":0,"context":"<p>本套《财经法规与会计职业道德》共六章36课时，主要讲述的是会计工作相关的法律法规，包括会计法律制度、支付结算法律制度、税收法律制度、财政法律制度，这是我们做会计工作法律上的规定，同时还有职业道德方面的约束，所以本教程最后一章讲的是会计职业道德。这一门课程的特点是法律条文多，记忆量大，让人不自觉间觉得枯燥。希望大家通过这门课程的学习不仅能达到通过考试的目的，同时也为我们在实际工作中打下坚实的基础。<br/><br/><\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t《财经法规与会计职业道德》主要与做会计工作相关的法规制度相关，是会计从业资格考试的考试科目之一，是会计从业资格的必考科目。该科目是三门科目中通过率相对比较低的一门，考生们要对本科目的学习给予足够的重视。<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t《财经法规与会计职业道德》学习方法<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;1、课要听，书要看，题要做<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;2、理解为主，记忆为辅<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;3、学习与提问相结合<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;4、学习与提问相结合<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;5、先《会计基础》后《财经法规与会计职业道德》<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:1.6;white-space:normal;\">\r\n\t&nbsp;6、利用好思维导图辅助学习<br/><br/>适合对象：<br/><\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:2;white-space:normal;\">\r\n\t\t1、《财经法规与会计职业道德》基础弱、平时工作学习较忙的学员。\t<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:2;white-space:normal;\">\r\n\t\t2、职场提升，学生 。\t<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:2;white-space:normal;\">\r\n\t\t3、准备2015年会计从业资格考试人群。<br/><br/>课程目标：\t\t<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:2;white-space:normal;\">\r\n\t\t\t1、帮助学员熟练、精确地掌握《财经法规与会计职业道德》全书的考点。\t\t<\/p><p style=\"margin-top:0px;margin-bottom:0px;padding:0px;line-height:2;white-space:normal;\">\r\n\t\t\t2、帮助学员掌握答题技巧，在考场上快速准确答题，轻松通过《财经法规与会计职业道德》科目考试。\t\t<\/p>","courseId":41,"courseName":"财经法规与会计职业道德","currentPrice":0.01,"isavaliable":1,"lessionNum":36,"logo":"/images/upload/course/20161222/1482393115020.jpg","loseTime":"365","loseType":1,"pageBuycount":99,"pageViewcount":655,"sellType":"COURSE","sourcePrice":0.01,"subjectId":256,"title":"《财经法规》可作为高职高专会计专业和财经管理类相关专业的教学用书，也可作为社会人员参加会计从业资格考试的培训和复习参考书。","updateTime":"2016-12-21 07:51:56"},"goodsid":41,"id":295,"type":1,"userid":1464},{"addTime":"2017-07-26 00:13:14","course":{"addTime":"2016-06-22 08:57:00","classId":0,"context":"<p>这部Excel实战技巧视频教程从Excel的工作环境和基本操作开始介绍，逐步深入地揭示了数据处理和分析、函数和公式应用、图表和图形的使用以及VBA和宏的应用等各个部分的实战经验技巧。Microsoft Excel是微软公司的办公软件Microsoft office的组件之一，是由Microsoft为Windows和Apple Macintosh操作系统的电脑而编写和运行的一款试算表软件。Excel 是微软办公套装软件的一个重要的组成部分，它可以进行各种数据的处理、统计分析和辅助决策操作，广泛地应用于管理、统计财经、金融等众多领域。现在Excel2003、和老一点的Excel2000较为多见，Excel2002版本好像用的不是很多。2000以前的版本很少见了。最新的版本增添了许多功能。使Excel功能更为强大。Excel2003支持VBA编程，VBA是Visual Basic For Application的简写形式。VBA的使用可以达成执行特定功能或是重复性高的操作。微软的OFFICE是目前最为流行的办公软件，目前主要有OFFICE2000和OFFICE<\/p>","courseId":71,"courseName":"Excel实战技巧视频教程","currentPrice":0.01,"isavaliable":1,"lessionNum":24,"logo":"/images/upload/course/20161222/1482391558235.jpg","loseTime":"365","loseType":1,"pageBuycount":584,"pageViewcount":1694,"sellType":"COURSE","sourcePrice":0.01,"subjectId":250,"title":"这部Excel实战技巧视频教程从Excel的工作环境和基本操作开始介绍，逐步深入地揭示了数据处理和分析、函数和公式应用、图表和图形的使用以及VBA和宏的应用等各个部分的实战经验技巧。","updateTime":"2017-01-19 02:19:57"},"goodsid":71,"id":297,"type":1,"userid":1464},{"addTime":"2017-07-26 00:19:15","course":{"addTime":"2016-06-21 10:36:22","classId":0,"context":"<p>考前论文辅导+押题串讲，让学员真正做到心中有数！<\/p>","courseId":46,"courseName":"企业人力资源管理师二级押题串讲学习班","currentPrice":0.01,"isavaliable":1,"lessionNum":4,"logo":"/images/upload/course/20161222/1482394345278.jpg","loseTime":"365","loseType":1,"pageBuycount":727,"pageViewcount":2328,"sellType":"COURSE","sourcePrice":0.01,"subjectId":203,"title":"考前论文辅导+押题串讲","updateTime":"2016-12-21 08:12:26"},"goodsid":46,"id":298,"type":1,"userid":1464}]
     */

    private boolean isexsits;
    private String id;
    private List<ShoppingCartBean.ShopCartListBean> shopcartList;

    public boolean isIsexsits() {
        return isexsits;
    }

    public void setIsexsits(boolean isexsits) {
        this.isexsits = isexsits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ShoppingCartBean.ShopCartListBean> getShopcartList() {
        return shopcartList;
    }

    public void setShopcartList(List<ShoppingCartBean.ShopCartListBean> shopcartList) {
        this.shopcartList = shopcartList;
    }

}
