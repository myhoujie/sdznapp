package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/17
 */
public interface ShoppingCartView extends BaseView {

    void queryCartSuccess( List<ShoppingCartBean.ShopCartListBean> shoppingCartBeens);

    void queryCartEmpty();

    void queryCartFailure(String msg);

    void delGoodsSuccess();

    void delGoodsFailure(String msg);
}
