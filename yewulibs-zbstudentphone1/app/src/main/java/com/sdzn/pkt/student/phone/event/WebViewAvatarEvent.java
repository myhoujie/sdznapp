package com.sdzn.pkt.student.phone.event;

/**
 * 描述：更新作业WebView
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/12
 */

public class WebViewAvatarEvent {

    private String type;

    public WebViewAvatarEvent(String type) {
        this.type = type;
    }

    public String setType() {
        return type;
    }
    public String getType() {
        return type;
    }
}
