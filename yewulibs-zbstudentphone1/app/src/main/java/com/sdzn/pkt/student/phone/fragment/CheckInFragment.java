package com.sdzn.pkt.student.phone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.sdzn.core.base.BaseFragment;
import com.sdzn.pkt.student.phone.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 直播间-》课堂点名
 * 创建人：baoshengxiang
 * 创建时间：2017/9/26
 */
public class CheckInFragment extends BaseFragment {
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.bu_checkin)
    Button buCheckIn;

    private OnFragmentExitListener mListener;
    private int time = 10;

    public CheckInFragment() {
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case 1:
                time--;
                tvTime.setText("" + time);
                if (time > 0) {
                    mHandler.sendEmptyMessageDelayed(1, 1000);
                } else {
                    mListener.onCheckInSubmit(false);
                }
                break;
        }
    }

    public static CheckInFragment newInstance() {
        return new CheckInFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentExitListener) {
            mListener = (OnFragmentExitListener) context;
        } else {
            throw new UnsupportedOperationException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_check_in;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        tvTime.setText("" + time);
        mHandler.sendEmptyMessageDelayed(1, 1000);
    }


    @OnClick(R.id.bu_checkin)
    public void checkIn() {
        mHandler.removeMessages(1);
        mListener.onCheckInSubmit(true);
    }

    public void resetTimer() {
        clearTimer();
        mHandler.sendEmptyMessageDelayed(1,1000);
    }

    public void clearTimer() {
        mHandler.removeMessages(1);
        time = 10;
    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return AnimationUtils.loadAnimation(getActivity(), R.anim.push_bottom_enter);
        } else {
            return null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentExitListener {

        void onCheckInSubmit(boolean checkIn);
    }


}
