package com.sdzn.pkt.student.phone.bean;

public class StudentResourceBean {

    /**
     * courseId : 152
     * courseName : 直播语文课程
     * createTime : 2020-03-12 16:04:56
     * downloadCount : 0
     * filePath : http://csfile.fuzhuxian.com/fa049e9a-b969-4e17-83a7-39a658f11974WX20181220-132918@2x.png
     * fileType : 0
     * id : 35
     * kpointId : 226
     * kpointName : 测试机测试课1
     * srcFileName : WX20181220-132918@2x.png
     * teacherId : 0
     * type : 2
     */

    private int courseId;
    private String courseName;
    private String createTime;
    private int downloadCount;
    private String filePath;
    private int fileType;
    private int id;
    private int kpointId;
    private String kpointName;
    private String srcFileName;
    private int teacherId;
    private int type;

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKpointId() {
        return kpointId;
    }

    public void setKpointId(int kpointId) {
        this.kpointId = kpointId;
    }

    public String getKpointName() {
        return kpointName;
    }

    public void setKpointName(String kpointName) {
        this.kpointName = kpointName;
    }

    public String getSrcFileName() {
        return srcFileName;
    }

    public void setSrcFileName(String srcFileName) {
        this.srcFileName = srcFileName;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
