package com.sdzn.pkt.student.phone.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Reisen at 2018-07-12
 */
public class TeacherBean implements Serializable {
    private static final long serialVersionUID = -7639998331357217468L;
    private TeacherInfoBean teacherInfo;

    public TeacherInfoBean getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfoBean teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public class TeacherInfoBean implements Serializable {
        private static final long serialVersionUID = 6202263954159879892L;
        private List<TeacherListBean> teacherList;

        public List<TeacherListBean> getTeacherList() {
            return teacherList;
        }

        public void setTeacherList(List<TeacherListBean> teacherList) {
            this.teacherList = teacherList;
        }
    }
}
