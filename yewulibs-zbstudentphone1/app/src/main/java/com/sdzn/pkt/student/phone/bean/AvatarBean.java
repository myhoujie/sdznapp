package com.sdzn.pkt.student.phone.bean;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/12
 */

public class AvatarBean {


    /**
     * picImg : /images/upload/customer/20170812/1502510133290.jpg
     */

    private String picImg;

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }
}
