package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.MovieBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/19
 */
public interface MovieView extends BaseView {

    void getMovieSucess(List<MovieBean> movieBeen);

    void getMovieFailure(String message);

    void getMovieEmpty();
}
