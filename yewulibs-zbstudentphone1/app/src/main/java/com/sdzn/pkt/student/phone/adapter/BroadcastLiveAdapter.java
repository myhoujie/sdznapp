package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.LiveInfoBean;
import com.sdzn.pkt.student.phone.network.api.ApiInterface;
import com.sdzn.pkt.student.phone.utils.PriceUtil;

import java.util.List;
import java.util.Locale;

/**
 * 描述：
 * - 首页正在直播列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/6
 */
public class BroadcastLiveAdapter extends BaseRcvAdapter<LiveInfoBean> {

    public BroadcastLiveAdapter(Context context, List<LiveInfoBean> mList) {
        super(context, R.layout.item_broadcast_live, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, LiveInfoBean liveInfoBean) {
        holder.setImageView(R.id.iv_cover, "" + liveInfoBean.getCourseLogo());
        holder.setText(R.id.tv_recmd_title, liveInfoBean.getCourseName());
        holder.setText(R.id.tv_chapter, liveInfoBean.getName());
        if (PriceUtil.isFree(liveInfoBean.getCurrentPrice())) {
            holder.setText(R.id.tv_recmd_price,context.getString(R.string.free));
            holder.setTextSize(R.id.tv_recmd_price,14);
            holder.setTextColorRes(R.id.tv_recmd_price,R.color.free_green);
        }else {
            holder.setText(R.id.tv_recmd_price, String.format(Locale.getDefault(),
                    "￥%1.2f", liveInfoBean.getCurrentPrice()));
            holder.setTextSize(R.id.tv_recmd_price,16);
            holder.setTextColorRes(R.id.tv_recmd_price,R.color.red);
        }
    }
}
