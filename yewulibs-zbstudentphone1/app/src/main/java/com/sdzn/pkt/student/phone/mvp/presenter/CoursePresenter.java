package com.sdzn.pkt.student.phone.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.pkt.student.phone.bean.CourseListRows;
import com.sdzn.pkt.student.phone.bean.Grade;
import com.sdzn.pkt.student.phone.bean.GradeJson;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.bean.SubjectBean;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.view.CourseView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.api.ResponseNewSchoolFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/11
 */
public class CoursePresenter extends BasePresenter<CourseView> {

    /**
     * 获取学科列表
     */

    public void getSubject(int sectionId){
        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getSubjectSpell(sectionId)
                .compose(TransformUtils.<ResultBean<List<SubjectBean>>>defaultSchedulers())
                .map(new ResponseNewFunc<>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<SubjectBean>>() {
                    @Override
                    public void onNext(List<SubjectBean> subjectSpellBeanList) {
                        getView().getSubjectSuccess(subjectSpellBeanList);
                    }

                    @Override
                    public void onFail(Throwable e) {

                        getView().getSubjectEmpty();
                    }
                }, mActivity, false));
        addSubscribe(subscribe);
    }

    /**
     * 获取 年级
     */
//    public void getGrade() {
//        Subscription subscribe = RestApi.getInstance()
//                .createNew(CourseService.class)
//                .getGradeJson()
//                .compose(TransformUtils.<ResultBean<List<GradeJson>>>defaultSchedulers())
//                .map(new ResponseNewFunc<>())
//                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<List<GradeJson>>() {
//                    @Override
//                    public void onNext(List<GradeJson> gradeJson) {
//                        if (gradeJson!=null){
//                            getView().onGradeSuccess(gradeJson);
//                        }
//
//                    }
//
//                    @Override
//                    public void onFail(Throwable e) {
//                        getView().onGradeEmpty();
//                    }
//                }, mActivity, false));
//        addSubscribe(subscribe);
//
//    }

    /**
     * 获取 课程
     */

    public void getCourse(Map<String,String> map) {
        String json= new Gson().toJson(map);//要传递的json
        RequestBody requestBody=RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getCourseSchool(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListRows>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListRows>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListRows>() {
                    @Override
                    public void onNext(CourseListRows courses) {
                        if (courses!=null) {
                            getView().getCourseSuccess(courses.getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        if (!SPManager.autoLogin(mActivity)||"40002".equals(e.getMessage())){
                            getView().getToLoginEmpty();
                        }else if (SPManager.isToCLogin()){//java.lang.Throwable: 请登录学校下发的学生账号查看！
                            getView().getTocFailedEmpty();
                        }else {
                            getView().getTobFailedEmpty();
                        }
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }



}
