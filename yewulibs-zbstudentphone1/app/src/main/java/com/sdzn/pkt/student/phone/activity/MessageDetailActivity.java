package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.TimeUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.MessageDetailBean;
import com.sdzn.pkt.student.phone.mvp.presenter.MessageDetailPresenter;
import com.sdzn.pkt.student.phone.mvp.view.MessageDetailView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class MessageDetailActivity extends BaseMVPActivity<MessageDetailView, MessageDetailPresenter> implements MessageDetailView {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_message_content)
    TextView tvMessageContent;
    public static final String ID_MESSAGE = "message_id";
    public static final String TYPE_MESSAGE = "message_type";
    public static final String CONTENT = "message_content";
    public static final String ADD_TIME = "message_add_time";
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_messagestatic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        if (!TextUtils.isEmpty(getIntent().getStringExtra(ID_MESSAGE))) {
            //更改状态
            mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
        }
        if (!TextUtils.isEmpty(getIntent().getStringExtra(TYPE_MESSAGE))) {
            titleBar.setTitleText(getIntent().getStringExtra(TYPE_MESSAGE));
        }
//        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mPresenter.getMessageDetail(getIntent().getStringExtra(ID_MESSAGE));
//            }
//        });

        if (!TextUtils.isEmpty(getIntent().getStringExtra(CONTENT))) {
            tvMessageContent.setText(getIntent().getStringExtra(CONTENT));

            String addTime = getIntent().getStringExtra(ADD_TIME);
            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }


    }

    @Override
    protected MessageDetailPresenter createPresenter() {
        return new MessageDetailPresenter();
    }

    @Override
    public void getMessageSuccess() {
        //获取未读消息数
        mPresenter.getUnReadMessageCount();
//        if (messageDetailBean != null) {
//            if (!TextUtils.isEmpty(messageDetailBean.getMsgReceive().getContent())) {
//                tvMessageContent.setText(messageDetailBean.getMsgReceive().getContent());
//            }
//            String addTime = TimeUtils.millis2String(messageDetailBean.getMsgReceive().getAddTime(), "yyyy.MM.dd HH:mm");
//            tvDate.setText(addTime);
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//        } else {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
//        }

    }

    @Override
    public void onError(String msg) {
        ToastUtils.showShort(msg);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}
