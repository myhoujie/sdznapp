package com.sdzn.pkt.student.phone.widget.ActionSheet;

public class SheetItem {
    private String name;
    private OnSheetItemClickListener itemClickListener;
    private SheetItemColor color;

    public SheetItem() {
    }

    public SheetItem(String name, SheetItemColor color, OnSheetItemClickListener itemClickListener) {
        this.name = name;
        this.color = color;
        this.itemClickListener = itemClickListener;
    }

    public OnSheetItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(OnSheetItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SheetItemColor getColor() {
        return color;
    }

    public void setColor(SheetItemColor color) {
        this.color = color;
    }
}