package com.sdzn.pkt.student.phone.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * 描述：
 * - viewpager 内容为fragment的adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class PageAdapterWithFragment extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;

    public PageAdapterWithFragment(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

}