package com.sdzn.pkt.student.phone.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.SpellingTitleAdapter;
import com.sdzn.pkt.student.phone.bean.SubjectBean;
import com.sdzn.pkt.student.phone.fragment.CourseTopicContentFragment;
import com.sdzn.pkt.student.phone.fragment.HomeFragment;
import com.sdzn.pkt.student.phone.fragment.SpellingContentFragment;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.CourseTopicPresenter;
import com.sdzn.pkt.student.phone.mvp.view.CourseTopicView;
import com.sdzn.pkt.student.phone.widget.TitleBar;
import com.sdzn.pkt.student.phone.widget.pager.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 专题课程
 */

public class CourseTopicActivity extends BaseMVPActivity<CourseTopicView, CourseTopicPresenter>
        implements CourseTopicView {
    @BindView(R.id.tabs)
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    @BindView(R.id.vp_pager)
    ViewPager viewPager;
    @BindView(R.id.title_bar)
    TitleBar titleBar;

    private SpellingTitleAdapter spellingTitleAdapter;
    private List<Fragment> spellListFragment = new ArrayList<>();
    private ArrayList<String> listTitle = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_course_topic;
    }

    @Override
    protected void onInit(Bundle bundle) {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SPManager.autoLogin(mContext)) {
                    IntentController.toLogin(mContext);
                    return;
                }
                IntentController.toShoppingCart(mContext);
            }
        });
        loadData();
    }

    @Override
    protected CourseTopicPresenter createPresenter() {
        return new CourseTopicPresenter();
    }

    private void loadData() {
        spellingTitleAdapter = new SpellingTitleAdapter(getSupportFragmentManager());
        mPresenter.getTopicTitle();
    }


    @Override
    public void onTopicSuccess(List<SubjectBean> subjectList) {
        this.listTitle.clear();
        this.spellListFragment.clear();
        spellListFragment.add(CourseTopicContentFragment.newInstance("-1"));
        listTitle.add("全部");

        for (int i = 0; i < subjectList.size(); i++) {
            spellListFragment.add(CourseTopicContentFragment.newInstance(String.valueOf(subjectList.get(i).getSubjectId())));
            listTitle.add(subjectList.get(i).getSubjectName());
        }
        spellingTitleAdapter.setmDatas(listTitle, spellListFragment);
        viewPager.setAdapter(spellingTitleAdapter);
        viewPager.setCurrentItem(0);
        mPagerSlidingTabStrip.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(0);

        mPagerSlidingTabStrip.clearConfigSet();
    }

    @Override
    public void onTopicFailed(String msg) {

    }
}
