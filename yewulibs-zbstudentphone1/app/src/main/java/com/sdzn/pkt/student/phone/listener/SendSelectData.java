package com.sdzn.pkt.student.phone.listener;

import com.sdzn.pkt.student.phone.bean.GradeJson;

/*
年级选中时回传
 */
public interface SendSelectData {
    void send(String eduId, GradeJson.ChildListBean bean);
}
