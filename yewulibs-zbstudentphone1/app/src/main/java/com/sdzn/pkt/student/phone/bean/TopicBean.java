package com.sdzn.pkt.student.phone.bean;

import java.util.List;

/**
 * 专题名称实体
 */
public class TopicBean {
    /**
     * records : [{"id":7,"name":"专题名称","status":1,"createTime":"2020-07-01 14:18:28","classPicturesUrl":"https://csfile.fuzhuxian.com/1593584306218"},{"id":6,"name":"什么什么","status":1,"createTime":"2020-06-30 15:53:57","classPicturesUrl":"https://csfile.fuzhuxian.com/1593503584546"},{"id":5,"name":"222","status":1,"createTime":"2020-06-19 17:55:12","classPicturesUrl":"https://csfile.fuzhuxian.com/1592560539198"},{"id":4,"name":"11","status":1,"createTime":"2020-06-19 17:54:17","classPicturesUrl":"https://csfile.fuzhuxian.com/1592560466685"},{"id":2,"name":"test001","status":1,"createTime":"2020-06-05 17:16:36","classPicturesUrl":"test001"}]
     * total : 5
     * size : 10
     * current : 1
     * page : 1
     * limit : 10
     * sort :
     * order :
     * requestMap : {"status":"1"}
     * orderBy :
     * searchCount : true
     * pages : 1
     */

    private String total;
    private String size;
    private String current;
    private int page;
    private int limit;
    private String sort;
    private String order;
    private RequestMapBean requestMap;
    private String orderBy;
    private boolean searchCount;
    private String pages;
    private List<SubjectBean> records;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public RequestMapBean getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(RequestMapBean requestMap) {
        this.requestMap = requestMap;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public List<SubjectBean> getRecords() {
        return records;
    }

    public void setRecords(List<SubjectBean> records) {
        this.records = records;
    }

    public static class RequestMapBean {
        /**
         * status : 1
         */

        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }



}
