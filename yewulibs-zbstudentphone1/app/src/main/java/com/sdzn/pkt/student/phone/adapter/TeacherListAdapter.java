package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.TeacherListBean;
import com.sdzn.pkt.student.phone.network.api.ApiInterface;

import java.util.List;

/**
 * 名师专栏列表
 */

public class TeacherListAdapter extends BaseRcvAdapter<TeacherListBean> {
    public TeacherListAdapter(Context context, List<TeacherListBean> mList) {
        super(context, R.layout.item_teacher, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, TeacherListBean bean) {
        holder.setImageView(R.id.iv_head, "" + bean.getPicPath(), R.mipmap.ic_avatar);
        holder.setText(R.id.tv_name,bean.getName());
        holder.setText(R.id.tv_info,bean.getEducation());
        holder.setText(R.id.tv_section,bean.getSubjectName());
    }

}
