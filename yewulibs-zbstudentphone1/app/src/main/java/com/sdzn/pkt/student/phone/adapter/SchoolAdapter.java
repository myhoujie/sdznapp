package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.SchoolBean;

import java.util.List;

/**
 * 描述：
 * - 选择学校-》学校列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/9/13
 */
public class SchoolAdapter extends BaseRcvAdapter<SchoolBean> {

    public SchoolAdapter(Context context, List<SchoolBean> mList) {
        super(context, R.layout.item_school, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, SchoolBean schoolBean) {
        holder.setText(R.id.tv_school, schoolBean.getSchoolName());
    }

    public void updateData(List<SchoolBean> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public SchoolBean getDataFromPosition(int position) {
        return mList.get(position);
    }

}
