package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.MessageDetailBean;

/**
 * 描述：消息详情
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/21
 */

public interface MessageDetailView extends BaseView {
    void getMessageSuccess();

    void onError(String msg);
}
