package com.sdzn.pkt.student.phone.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azhon.appupdate.config.UpdateConfiguration;
import com.azhon.appupdate.listener.OnDownloadListener;
import com.azhon.appupdate.manager.DownloadManager;
import com.blankj.utilcode.util.DeviceUtils;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AndroidBug5497Workaround;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.util.Log;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.bean.UserLoginBean;
import com.sdzn.pkt.student.phone.event.UpdateAccountEvent;
import com.sdzn.pkt.student.phone.manager.Config;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.LoginPresenter;
import com.sdzn.pkt.student.phone.mvp.view.LoginView;
import com.sdzn.pkt.student.phone.permission.MPermission;
import com.sdzn.pkt.student.phone.permission.annotation.OnMPermissionDenied;
import com.sdzn.pkt.student.phone.permission.annotation.OnMPermissionGranted;
import com.sdzn.pkt.student.phone.utils.CountDownTimerUtils;
import com.sdzn.pkt.student.phone.utils.UpdateDialog;
import com.sdzn.pkt.student.phone.utils.VerifyUtil;
import com.sdzn.pkt.student.phone.widget.CheckBoxSample;
import com.sdzn.pkt.student.phone.widget.ClearEditText;
import com.sdzn.pkt.student.phone.widget.CustomDialog;
import com.sdzn.pkt.student.phone.widget.PwdEditText;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 用户登录
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */

public class LoginActivity extends BaseMVPActivity<LoginView, LoginPresenter> implements LoginView, View.OnLayoutChangeListener, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.root_layout)
    View rootLayout;
    @BindView(R.id.bottom_space)
    View bottomSpace;
    @BindView(R.id.ll_bg)
    LinearLayout llBg;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.iv_logo2)
    ImageView ivLogo2;
    @BindView(R.id.ll_longin_content)
    LinearLayout llLoginContent;
    @BindView(R.id.et_account)
    ClearEditText etAccount;
    @BindView(R.id.et_password)
    PwdEditText etPassword;
    @BindView(R.id.ll_register)
    View regView;
    @BindView(R.id.rg_random)
    RadioGroup radioGroup;
    @BindView(R.id.tv_code_hint)
    TextView tvCodeHint;
    @BindView(R.id.tv_forget_password)
    TextView tvForgetPassword;
    @BindView(R.id.rl_password)
    RelativeLayout rlPassword;
    @BindView(R.id.rl_code)
    RelativeLayout rlCode;
    @BindView(R.id.et_code)
    ClearEditText etCode;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_user_agreement)
    TextView tvUserAgreement;
    @BindView(R.id.tv_privacy_agreement)
    TextView tvPrivacyAgreement;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.checkbox)
    CheckBoxSample checkBoxSample;
    private String IMEI;//设备唯一号

    private CountDownTimerUtils countDownTimerUtils;
    //软件盘弹起后所占高度阀值
    private int keyHeight = 0;
    private boolean IS_PSSWOORD_LOGIN = true;
    private boolean IS_DETSIL;
    public static final String LOGIN_DETAIL = "LOGIN_DETAIL";
    private boolean isCheckBox;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        mPresenter.checkVerion();
        IMEI = DeviceUtils.getAndroidID();
//        IMEI = AndroidUtil.getImei(App2.get());
        Log.e("test", IMEI);
        initData();
        initView();

//        requestBasicPermission();
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,1,ivLogo);
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppManager.getAppManager().finishActivity(ReSetPasswordActivity.class);
        AppManager.getAppManager().finishActivity(RetrievePasswordActivity.class);
        AppManager.getAppManager().finishActivity(RegisterActivity.class);
    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
        rootLayout.addOnLayoutChangeListener(this);
        etPassword.setShowAnimate(false);
        regView.setVisibility(Config.OPEN_REGISTRATION ? View.VISIBLE : View.GONE);
        radioGroup.setOnCheckedChangeListener(this);

        tvUserAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvPrivacyAgreement.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        if (!checkBoxSample.isChecked()) {
            checkBoxSample.toggle();
            isCheckBox = true;
        }

    }

    private void initData() {
        int screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
//        keyHeight = screenHeight / 3;
        //系统自带输入法, 密码输入框比文字输入框矮一点, 正好卡在差一点到1/3的位置(这个差的高度不同尺寸屏幕不一样)
        //华为部分机型开启全屏模式时, 显示虚拟键会将窗口顶起一点...祖传大黑边应该也不能超过1/4吧..
        keyHeight = screenHeight / 4;
        String lastLoginAccount = SPManager.getLastLoginAccount();
        if (!TextUtils.isEmpty(lastLoginAccount)) {
//            etAccount.setText(lastLoginAccount);
        }
        SPManager.changeLogin(mContext, false);
        IS_DETSIL = getIntent().getBooleanExtra(LOGIN_DETAIL, false);
        requestBasicPermission();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right,
                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        //old是改变前的左上右下坐标点值，没有old的是改变后的左上右下坐标点值
        //现在认为只要控件将Activity向上推的高度超过了1/3屏幕高，就认为软键盘弹起
//        LogUtils.i("top = " + top + "\nbottom = " + bottom + "\noldTop = " + oldTop + "\noldBottom = " + oldBottom);
        if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
            bottomSpace.setVisibility(View.GONE);
            llBg.setVisibility(View.GONE);
            ivLogo.setVisibility(View.INVISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_trans);
        } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
            //监听到软件盘关闭
            bottomSpace.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.VISIBLE);
            llBg.setVisibility(View.VISIBLE);
            ivLogo2.setImageResource(R.mipmap.ic_login_logo_trans);
            llLoginContent.setBackgroundResource(R.mipmap.login_content_bg);
        }
//        bottomSpace.requestLayout();
    }


    @OnClick({R.id.tv_forget_password, R.id.ll_register, R.id.btn_login, R.id.btn_get_code, R.id.iv_back, R.id.tv_user_agreement, R.id.tv_privacy_agreement, R.id.checkbox})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_forget_password:
                IntentController.toRetrievePwd(mContext);
                break;
            case R.id.btn_login:
                if (IS_PSSWOORD_LOGIN) {
                    doLogin();
                } else {
                    doLoginCode();
                }
                break;
            case R.id.ll_register:
                if (!Config.OPEN_REGISTRATION) {
                    return;
                }

                break;
            case R.id.btn_get_code:
                getVerifyCode();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_user_agreement:
                IntentController.toWeb(mContext, "1");
                break;
            case R.id.tv_privacy_agreement:
                IntentController.toWeb(mContext, "2");
                break;
            case R.id.checkbox:
                checkBoxSample.toggle();
                if (checkBoxSample.isChecked()) {
                    isCheckBox = true;
                } else {
                    isCheckBox = false;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        KeyboardUtils.hideSoftInput(this);
        etAccount.setText("");
        switch (i) {
            case R.id.rb_password://密码登录
                IS_PSSWOORD_LOGIN = true;
                tvForgetPassword.setVisibility(View.VISIBLE);
                tvCodeHint.setVisibility(View.INVISIBLE);
                rlPassword.setVisibility(View.VISIBLE);
                rlCode.setVisibility(View.GONE);
                etAccount.setHint(getString(R.string.login_acount_hint));
                tvAccount.setText("账号");
                etPassword.setText("");
                etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});

                break;
            case R.id.rb_code:
                IS_PSSWOORD_LOGIN = false;
                tvForgetPassword.setVisibility(View.INVISIBLE);
                tvCodeHint.setVisibility(View.VISIBLE);
                rlPassword.setVisibility(View.GONE);
                rlCode.setVisibility(View.VISIBLE);
                etAccount.setHint(getString(R.string.login_acount_hint1));
                tvAccount.setText("手机号");
                etCode.setText("");
                etAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
                break;
            default:
                break;
        }
    }


    public void doLogin() {
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号或账号");
        } else if (TextUtils.isEmpty(password)) {
            ToastUtils.showShort("请输入密码");
        } else {
            mPresenter.login(account, password, IMEI);
        }


    }

    /**
     * 验证码登录
     */
    public void doLoginCode() {
        String account = etAccount.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(account)) {
            ToastUtils.showShort("手机号格式错误");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            mPresenter.loginCode(account, code, IMEI);
        }

    }

    private void getVerifyCode() {
        String phoneNo = etAccount.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            countDownTimerUtils.start();
            mPresenter.getVerifyCode(phoneNo);
        }
    }

    @Override
    public void loginSuccess(UserLoginBean loginBean) {
        UserBean userBean = loginBean.getUserDetail();
        SPManager.saveToken(loginBean.getAccess_token());
        SPManager.saveUser(loginBean.getUserDetail());
        SPManager.saveLastLoginAccount(etAccount.getText().toString().trim());
        SPManager.savePwd(etPassword.getText().toString().trim());

        SPManager.changeLogin(mContext, true);
        if (0 != userBean.getSubjectId() || 0 != userBean.getGrade()) {
            SPManager.saveSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveGrade(userBean.getGrade(), userBean.getGradeName());

            SPManager.saveSchoolSection(userBean.getSubjectId(), "", userBean.getEducationId());
            SPManager.saveSchoolGrade(userBean.getGrade(), userBean.getGradeName());

            EventBus.getDefault().post(new UpdateAccountEvent(UpdateAccountEvent.CHANGE_PHASE));//去刷新 拼课堂和学校课程
            if (IS_DETSIL) {
                this.finish();
            } else {
                IntentController.toMain(mContext, false);
            }
        } else {
            //          IntentController.toAccountSetting(mContext);
            //添加两参数  1 从哪跳的  2（带学段？） 3.是否从详情过来的
            if (IS_DETSIL) {
                IntentController.toSelectSubject(mContext, false, true, true);
                this.finish();
            } else {
                IntentController.toSelectSubject(mContext, false, true, false);
                this.finish();
            }
        }


    }


    @Override
    public void loginFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    private ProgressDialog pd;
    private DownloadManager manager;

    @Override
    public void updateVersion(String updateInfo, String targetUrl) {
        // 需要更新，弹出更新窗口
        UpdateDialog.Builder builder = new UpdateDialog.Builder(LoginActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(LoginActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(LoginActivity.this);
                manager.setApkName("拼课堂.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.pkt.student.phone.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        ToastUtils.showShort(msg);
    }


    public static final int BAIJIAYUN_CODE = 10011;

    private void requestBasicPermission() {
        MPermission.with(LoginActivity.this)
                .addRequestCode(BAIJIAYUN_CODE)
                .permissions(
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(BAIJIAYUN_CODE)
    public void onBasicPermissionSuccess() {
    }

    @OnMPermissionDenied(BAIJIAYUN_CODE)
    public void onBasicPermissionFailed() {
    }

    @Override
    public void onBackPressed() {
        if (IS_DETSIL) {
            this.finish();
        } else {
            IntentController.toMain(mContext, false);
        }
//            AppUtils.exitApp(mContext);

    }
}
