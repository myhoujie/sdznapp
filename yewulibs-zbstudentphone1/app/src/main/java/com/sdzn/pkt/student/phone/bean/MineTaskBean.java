package com.sdzn.pkt.student.phone.bean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/13
 */
public class MineTaskBean {
    /**
     * records : [{"homeworkName":"test001","homeworkId":7,"subjectName":"语文","courseName":"","stuHomeworkStatus":1,"remainingTime":"104小时","closeTime":""}]
     */
    private List<RecordsBean> records;

    public List<RecordsBean> getRecords() {
        return records;
    }

    public void setRecords(List<RecordsBean> records) {
        this.records = records;
    }


    public static class RecordsBean {
        /**
         * homeworkName : test001
         * homeworkId : 7
         * subjectName : 语文
         * courseName :
         * stuHomeworkStatus : 1
         * remainingTime : 104小时
         * closeTime :
         */

        private String homeworkName;
        private String homeworkId;
        private String subjectName;
        private String chapterName;
        private String stuHomeworkStatus;
        private String remainingTime;
        private String closeTime;
        private String colorType;

        public String getColorType() {
            return colorType;
        }

        public void setColorType(String colorType) {
            this.colorType = colorType;
        }

        public String getHomeworkName() {
            return homeworkName;
        }

        public void setHomeworkName(String homeworkName) {
            this.homeworkName = homeworkName;
        }

        public String getHomeworkId() {
            return homeworkId;
        }

        public void setHomeworkId(String homeworkId) {
            this.homeworkId = homeworkId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getChapterName() {
            return chapterName;
        }

        public void setChapterName(String chapterName) {
            this.chapterName = chapterName;
        }

        public String getStuHomeworkStatus() {
            return stuHomeworkStatus;
        }

        public void setStuHomeworkStatus(String stuHomeworkStatus) {
            this.stuHomeworkStatus = stuHomeworkStatus;
        }

        public String getRemainingTime() {
            return remainingTime;
        }

        public void setRemainingTime(String remainingTime) {
            this.remainingTime = remainingTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }
    }


    /**
     * records : [{"homeworkName":"test001","homeworkId":7,"subjectName":"语文","courseName":"","stuHomeworkStatus":1,"remainingTime":"106小时","closeTime":""}]
     *//*
    private List<RecordsBean> recordsBeanList;

    public List<RecordsBean> getRecordsBeanList() {
        return recordsBeanList;
    }

    public void setRecordsBeanList(List<RecordsBean> recordsBeanList) {
        this.recordsBeanList = recordsBeanList;
    }

    public static class RecordsBean {
        *//**
     * homeworkName : test001
     * homeworkId : 7
     * subjectName : 语文
     * courseName :
     * stuHomeworkStatus : 1
     * remainingTime : 106小时
     * closeTime :
     *//*

        private String homeworkName;
        private int homeworkId;
        private String subjectName;
        private String courseName;
        private int stuHomeworkStatus;
        private String remainingTime;
        private String closeTime;

        public String getHomeworkName() {
            return homeworkName;
        }

        public void setHomeworkName(String homeworkName) {
            this.homeworkName = homeworkName;
        }

        public int getHomeworkId() {
            return homeworkId;
        }

        public void setHomeworkId(int homeworkId) {
            this.homeworkId = homeworkId;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public int getStuHomeworkStatus() {
            return stuHomeworkStatus;
        }

        public void setStuHomeworkStatus(int stuHomeworkStatus) {
            this.stuHomeworkStatus = stuHomeworkStatus;
        }

        public String getRemainingTime() {
            return remainingTime;
        }

        public void setRemainingTime(String remainingTime) {
            this.remainingTime = remainingTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }
    }*/


    /**
     * homeworkName : test001
     * subjectName : 语文
     * courseName : 初中语文
     * stuHomeworkStatus : 1
     * closeTime : 2020-07-09 00:00:00
     */

   /* private String homeworkName;
    private String subjectName;
    private String courseName;
    private int stuHomeworkStatus;
    private String closeTime;

    public String getHomeworkName() {
        return homeworkName;
    }

    public void setHomeworkName(String homeworkName) {
        this.homeworkName = homeworkName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getStuHomeworkStatus() {
        return stuHomeworkStatus;
    }

    public void setStuHomeworkStatus(int stuHomeworkStatus) {
        this.stuHomeworkStatus = stuHomeworkStatus;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }*/
}
