package com.sdzn.pkt.student.phone.annotation;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Reisen at 2018-05-28
 */

@IntDef({DownloadState.UnDownLoad, DownloadState.Downloading, DownloadState.Pause,
        DownloadState.Finish, DownloadState.Error, DownloadState.Cancel})
@Retention(RetentionPolicy.SOURCE)
public @interface DownloadState {
    int UnDownLoad = 0;//未下载(点击下载
    int Downloading = 1;//下载中(点击暂停
    int Pause = 2;//暂停(点击继续
    int Finish = 3;//完成(点击打开
    int Error = 4;//出错(点击提示重新下载
    int Cancel = 5;//取消(临时状态, 要立刻改为UnDownload
}
