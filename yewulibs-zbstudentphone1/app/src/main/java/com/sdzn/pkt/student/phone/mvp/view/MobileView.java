package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：绑定手机
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public interface MobileView extends BaseView {

    void onSuccess();

    void onError(String msg);
}
