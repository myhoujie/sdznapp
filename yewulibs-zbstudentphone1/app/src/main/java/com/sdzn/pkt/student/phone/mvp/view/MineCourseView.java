package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.MineList;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/24
 */

public interface MineCourseView extends BaseView {
    void listTodyCourseSuccess(List<MineList> list);

    void listRecentlyCourseSuccess(List<MineList> list);

    void listCourseSuccess(List<MineList> list, int type);

    void listCourseEmpty();

    void listCourseError(String msg);

    void getLiveRoomInfoSuccrss(NewLiveInfo liveRoomBean);

    void getReplayInfoSuccess(NewVideoInfo info);

}
