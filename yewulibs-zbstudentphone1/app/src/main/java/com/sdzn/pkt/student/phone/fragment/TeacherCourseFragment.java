package com.sdzn.pkt.student.phone.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.TeacherCourseAdapter;
import com.sdzn.pkt.student.phone.adapter.WrapAdapter;
import com.sdzn.pkt.student.phone.bean.TeacherCourseBean;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 描述：
 * - 课程目录
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class TeacherCourseFragment extends BaseFragment {

    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    @BindView(R.id.swipe_target)
    RecyclerView rcvCourseCatalogue;

    private TeacherCourseAdapter courseAdapter;
    private WrapAdapter<TeacherCourseAdapter> wrapAdapter;
    private List<TeacherCourseBean.TeacherCourseList> courseBeans;

    private static final String COURSE_TYPE_PARAMS = "type";
    private int courseType;

    public static TeacherCourseFragment newInstance(@CourseCons.Type.CourseType int type) {
        TeacherCourseFragment courseFragment = new TeacherCourseFragment();
        Bundle args = new Bundle();
        args.putInt(COURSE_TYPE_PARAMS, type);
        courseFragment.setArguments(args);
        return courseFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            courseType = getArguments().getInt(COURSE_TYPE_PARAMS);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_teacher_course;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {
        courseBeans = new ArrayList<>();
    }

    private void initView() {
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, getResources().getBoolean(R.bool.isTablet) ? 3 : 2);
        rcvCourseCatalogue.setLayoutManager(layoutManager);
        courseAdapter = new TeacherCourseAdapter(mContext, courseBeans, courseType);
        wrapAdapter = new WrapAdapter<>(courseAdapter);
        wrapAdapter.adjustSpanSize(rcvCourseCatalogue);
        rcvCourseCatalogue.setAdapter(wrapAdapter);
        courseAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TeacherCourseBean.TeacherCourseList courseListBean = courseBeans.get(position);
                IntentController.toCourseDetail(mContext, courseListBean.getSellType(), courseListBean.getCourseId(), false);
            }
        });
    }

    public void setData(List<TeacherCourseBean.TeacherCourseList> list) {
        courseBeans.clear();
        if (list == null) {
            courseAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
        } else if (list.isEmpty()) {
            courseAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            courseBeans.addAll(list);
            courseAdapter.notifyDataSetChanged();
            emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
        }
    }
}
