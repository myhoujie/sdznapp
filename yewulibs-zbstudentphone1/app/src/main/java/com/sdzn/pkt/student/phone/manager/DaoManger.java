package com.sdzn.pkt.student.phone.manager;

import com.sdzn.pkt.student.phone.application.App;
import com.sdzn.pkt.student.phone.bean.dao.DaoMaster;
import com.sdzn.pkt.student.phone.bean.dao.DaoSession;

/**
 * @author Reisen at 2018-05-31
 */

public class DaoManger {

    private static DaoManger instance;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;

    public static DaoManger getInstance() {
        if (instance == null) {
            synchronized (DaoManger.class) {
                if (instance == null) {
                    instance = new DaoManger();
                }
            }
        }
        return instance;
    }

    private DaoManger(){
        if (instance == null) {
            DaoMaster.DevOpenHelper devOpenHelper = new
                    DaoMaster.DevOpenHelper(App.getInstance(), "znclass-db", null);//此处为自己需要处理的表
            mDaoMaster = new DaoMaster(devOpenHelper.getWritableDatabase());
            mDaoSession = mDaoMaster.newSession();
        }
    }

    public DaoMaster getMaster() {
        return mDaoMaster;
    }
    public DaoSession getSession() {
        return mDaoSession;
    }
    public DaoSession getNewSession() {
        mDaoSession = mDaoMaster.newSession();
        return mDaoSession;
    }
}
