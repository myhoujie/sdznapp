package com.sdzn.pkt.student.phone.bean;

import java.util.List;

/**
 * 描述：消息
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class MessageBean {


    /**
     * letterList : [{"addTime":"2017-07-17 19:08:25","content":"让天下没有难的教与学，让教与学成为美好的体验\u201d。十年不倦、上下求索，时代智囊一直在路上。","cusId":0,"friNum":0,"groupNum":0,"id":1009,"letterNum":0,"receivingCusId":1463,"showname":"1360275254@qq.com","status":1,"systemNum":0,"type":1,"unReadNum":0,"updateTime":"2017-07-17 19:08:25"}]
     * page : {"currentPage":1,"first":true,"last":true,"pageSize":10,"totalPageSize":1,"totalResultSize":2}
     */

    private PageBean page;
    private List<LetterListBean> letterList;


    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public List<LetterListBean> getLetterList() {
        return letterList;
    }

    public void setLetterList(List<LetterListBean> letterList) {
        this.letterList = letterList;
    }

    public static class PageBean {
        /**
         * currentPage : 1
         * first : true
         * last : true
         * pageSize : 10
         * totalPageSize : 1
         * totalResultSize : 2
         */

        private int currentPage;
        private boolean first;
        private boolean last;
        private int pageSize;
        private int totalPageSize;
        private int totalResultSize;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalPageSize() {
            return totalPageSize;
        }

        public void setTotalPageSize(int totalPageSize) {
            this.totalPageSize = totalPageSize;
        }

        public int getTotalResultSize() {
            return totalResultSize;
        }

        public void setTotalResultSize(int totalResultSize) {
            this.totalResultSize = totalResultSize;
        }
    }

    public static class LetterListBean {

        private boolean isSelected;
        /**
         * addTime : 2017-07-19 17:41:00
         * content : 您的订单146420170719173252288，已退款成功，联系客服处理转账！
         * cusId : 0
         * friNum : 0
         * groupNum : 0
         * id : 1536
         * letterNum : 0
         * receivingCusId : 1464
         * showname :
         * status : 1
         * systemNum : 0
         * type : 1
         * unReadNum : 0
         * updateTime : 2017-07-20 01:41:50
         */

        private String addTime;
        private String content;
        private int id;
        private String status;
        private int type;
        private String updateTime;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }


        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }



        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }
    }
}
