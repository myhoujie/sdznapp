package com.sdzn.pkt.student.phone.bean;

public class NewLiveInfo {

    /**
     * room_id : 19112196048627
     * user_info : {"userName":"crt1107","userNumber":19652,"userType":"0"}
     * sign : 6c24fd45d992916bdbbf46bf60ec0f03
     */

    private String room_id;
    private UserInfoBean user_info;
    private String sign;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static class UserInfoBean {
        /**
         * userName : crt1107
         * userNumber : 19652
         * userType : 0
         */

        private String userName;
        private int userNumber;
        private String userType;
        private String userAvatar;

        public String getUserAvatar() {
            return userAvatar;
        }

        public void setUserAvatar(String userAvatar) {
            this.userAvatar = userAvatar;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getUserNumber() {
            return userNumber;
        }

        public void setUserNumber(int userNumber) {
            this.userNumber = userNumber;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}
