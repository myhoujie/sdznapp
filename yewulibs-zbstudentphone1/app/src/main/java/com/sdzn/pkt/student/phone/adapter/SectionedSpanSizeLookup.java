package com.sdzn.pkt.student.phone.adapter;

import androidx.recyclerview.widget.GridLayoutManager;

/**
 * 描述：
 * -  是用来自定义每个item需要占据的空间
 * 创建人：baoshengxiang
 * 创建时间：2017/7/4
 */
public class SectionedSpanSizeLookup<T> extends GridLayoutManager.SpanSizeLookup {

    protected SectionedRecyclerViewAdapter<T> adapter = null;
    protected GridLayoutManager layoutManager = null;

    public SectionedSpanSizeLookup(SectionedRecyclerViewAdapter<T> adapter, GridLayoutManager layoutManager) {
        this.adapter = adapter;
        this.layoutManager = layoutManager;
    }

    @Override
    public int getSpanSize(int position) {
        if (adapter.isSectionHeaderPosition(position) || adapter.isSectionFooterPosition(position)) {
            return layoutManager.getSpanCount();
        } else {
            return 1;
        }

    }
}