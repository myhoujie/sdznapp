package com.sdzn.pkt.student.phone.event;

public class MineCourseEvent {

    private boolean status;

    public MineCourseEvent(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
