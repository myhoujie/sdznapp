package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.mvp.presenter.RetrievePwdPresenter;
import com.sdzn.pkt.student.phone.mvp.view.RetrievePwdView;
import com.sdzn.pkt.student.phone.utils.CountDownTimerUtils;
import com.sdzn.pkt.student.phone.utils.VerifyUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 找回密码
 * 创建人：baoshengxiang
 * 创建时间：2017/7/8
 */
public class RetrievePasswordActivity extends BaseMVPActivity<RetrievePwdView, RetrievePwdPresenter> implements RetrievePwdView {

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.btn_get_code)
    Button btnGetCode;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    private CountDownTimerUtils countDownTimerUtils;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_retrieve_password;
    }

    @Override
    protected void setStatusBar() {
//        StatusBarUtil.setColor(this, Color.WHITE,0);
//        StatusBarUtil.setTranslucentForImageView(this,0,findViewById(R.id.iv_logo));
//        StatusBarUtil.statusBarLightMode(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        AndroidBug5497Workaround.assistActivity(this);
    }

    @Override
    protected RetrievePwdPresenter createPresenter() {
        return new RetrievePwdPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        initView();
    }

    private void initData() {

    }

    private void initView() {
        countDownTimerUtils = new CountDownTimerUtils(btnGetCode, 60000, 1000);
    }

    @OnClick({R.id.btn_get_code, R.id.btn_certain, R.id.iv_back,R.id.tv_find})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                getVerifyCode();
                break;
            case R.id.btn_certain:
                confirmVerifyCode();
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.tv_find:
                IntentController.toRegister(mContext);
                break;
            default:
                break;
        }
    }

    private void confirmVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        String code = etCode.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("请输入手机号");
        } else if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
        } else {
            mPresenter.confirmVerifyCode(phoneNo, code);
        }
    }

    private void getVerifyCode() {
        String phoneNo = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNo)) {
            ToastUtils.showShort("手机号不能为空");
        } else if (!VerifyUtil.isMobileNO(phoneNo)) {
            ToastUtils.showShort("手机号格式错误");
        } else {
            mPresenter.getVerifyCode(phoneNo);
            countDownTimerUtils.start();
        }
    }

    @Override
    public void onPause() {
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
        super.onPause();
    }

    @Override
    public void getCodeSuccess() {
        ToastUtils.showShort("验证码发送成功");
    }

    @Override
    public void getCodeFailure(String msg) {
        ToastUtils.showShort(msg);
        countDownTimerUtils.cancel();
        countDownTimerUtils.onFinish();
    }

    @Override
    public void confirmCodeSuccess() {
        IntentController.toResetPassword(mContext);
    }

    @Override
    public void confirmCodeFailure(String msg) {
        ToastUtils.showShort(msg);
    }


}
