package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;

import java.util.List;

/**
 * 描述：
 * - 订单提交-》订单列表adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class OrderInfoAdapter extends BaseRcvAdapter<ShoppingCartBean.ShopCartListBean> {
    private final String TYPE_LIVE = "LIVE";
    private final String TYPE_COURSE = "COURSE";
    private final String TYPE_PACKAGE = "PACKAGE";

    public OrderInfoAdapter(Context context, List mList) {
        super(context, R.layout.item_order_info, mList);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, ShoppingCartBean.ShopCartListBean goodsBean) {

        holder.setImageView(R.id.iv_cover, "" + goodsBean.getLogo());
        holder.setText(R.id.tv_recmd_title, goodsBean.getCourseName());
        holder.setText(R.id.tv_recmd_price, "￥" + goodsBean.getCurrentPrice());


        if (TYPE_PACKAGE.equals(goodsBean.getSellType())) {
            switch (goodsBean.getPackageType()) {
                case 1://LIVING
                    holder.setText(R.id.tv_status,"直");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);

                    break;
                case 2://VIDEO
                    holder.setText(R.id.tv_status,"点");
                    holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);
                      break;
                default:
                    break;
            }
        }else if (TYPE_LIVE.equals(goodsBean.getSellType())){//LIVING
            holder.setText(R.id.tv_status,"直");
            holder.setTextColorRes(R.id.tv_status, R.color.color_FDB850);

        }else if (TYPE_COURSE.equals(goodsBean.getSellType())){//VIDEO
            holder.setText(R.id.tv_status,"点");
            holder.setTextColorRes(R.id.tv_status, R.color.color_2DD4CA);

        }
    }
}
