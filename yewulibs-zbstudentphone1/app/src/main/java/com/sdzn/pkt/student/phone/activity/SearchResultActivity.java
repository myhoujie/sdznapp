package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.fragment.SpellingContentFragment;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.manager.constant.CourseCons;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 搜索结果
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class SearchResultActivity extends BaseActivity {
    @BindView(R.id.tv_search)
    TextView tvSearch;
    private String searchStr;

    private int courseType;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search_result;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        initView();
    }

    private void initData() {
        searchStr = getIntent().getStringExtra("searchStr");
        courseType = getIntent().getIntExtra("courseType", CourseCons.Type.ALL);
        //courseType  此处网listfragment传数据，是区分  所有学科还是单一学科
    }

    private void initView() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.fl_search_container, SpellingContentFragment.newInstance(courseType, searchStr)).commit();
        fragmentTransaction.add(R.id.fl_search_container, SpellingContentFragment.newInstance("", searchStr, SpellingContentFragment.IS_SEARCH_IN)).commit();
        tvSearch.setText(searchStr);
    }

    @OnClick({R.id.iv_back, R.id.tv_search, R.id.iv_cart_shop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.tv_search:
                onBackPressed();
                break;
            case R.id.iv_cart_shop:
                if (!SPManager.autoLogin(mContext)) {
                    IntentController.toLogin(mContext);
                    return;
                }
                IntentController.toShoppingCart(mContext);
                break;
            default:
                break;
        }
    }

}
