package com.sdzn.pkt.student.phone.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.azhon.appupdate.config.UpdateConfiguration;
import com.azhon.appupdate.listener.OnDownloadListener;
import com.azhon.appupdate.manager.DownloadManager;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AppUtils;
import com.sdzn.core.utils.FragmentTabUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.fragment.LiveCourseFragment;
import com.sdzn.pkt.student.phone.fragment.MineFragment;
import com.sdzn.pkt.student.phone.fragment.SpellingClassFragment;
import com.sdzn.pkt.student.phone.fragment.UserInfoFragment;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.presenter.MainPresenter;
import com.sdzn.pkt.student.phone.mvp.view.MainView;
import com.sdzn.pkt.student.phone.network.download.DownLoadDataService;
import com.sdzn.pkt.student.phone.permission.MPermission;
import com.sdzn.pkt.student.phone.permission.annotation.OnMPermissionDenied;
import com.sdzn.pkt.student.phone.permission.annotation.OnMPermissionGranted;
import com.sdzn.pkt.student.phone.utils.UpdateDialog;
import com.sdzn.pkt.student.phone.widget.CustomDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * saveGrade
 */
public class MainActivity extends BaseMVPActivity<MainView, MainPresenter> implements MainView, MineFragment.OnFragmentInteractionListener {
    @BindView(R.id.rg_navi_bottom)
    RadioGroup rgNavi;

    private List<Fragment> fragments;
    private FragmentTabUtils fragmentTabUtils;


    public static final String AUTO_LOGIN = "autoLogin";

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        requestBasicPermission();
        initData();
        initView();
    }


    @Override
    protected void setStatusBar() {
        int color = getResources().getColor(R.color.colorPrimary);
        StatusBarUtil.setColor(this, color, 0);
    }


    private void initData() {
        fragments = new ArrayList<>();
        fragments.add(SpellingClassFragment.newInstance());
        fragments.add(LiveCourseFragment.newInstance());
        fragments.add(MineFragment.newInstance());
        fragments.add(UserInfoFragment.newInstance());
        if (SPManager.autoLogin(mContext)) {
            //检查更新
            mPresenter.checkVerion();
            //获取未读消息数
            mPresenter.getUnReadMessageCount();
        }
    }


    private void initView() {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.add(R.id.fl_container, UserInfoFragment.newInstance());
//        fragmentTransaction.commit();

        fragmentTabUtils = new FragmentTabUtils(mContext, getSupportFragmentManager(), fragments, R.id.main_container, rgNavi);
        fragmentTabUtils.setNeedAnimation(false);


    }


    @Override
    public void onBackPressed() {
        AppUtils.exitApp(mContext);
    }

    @Override
    public void onFragmentInteraction() {
    }

    @Override
    public void autoLoginSuccess() {
        initData();
        initView();
        //判断是否更新
    }


    private ProgressDialog pd;
    private DownloadManager manager;

    @Override
    public void updateVersion(String updateInfo, String targetUrl) {
        // 需要更新，弹出更新窗口
        UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(MainActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(MainActivity.this);
                manager.setApkName("拼课堂.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.pkt.student.phone.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    @Override
    protected void onDestroy() {
        DownLoadDataService.stopDownloadService(this);
        super.onDestroy();
//        if (BuildConfig.ISDEBUG) {
        // FIXME: 2017-11-16 蒲公英sdk有问题, 会导致应用crash
//            PgyUpdateManager.unregister();
//        }
    }

    public static final int PERMISSION_CODE = 1200;

    private void requestBasicPermission() {
        MPermission.with(this)
                .addRequestCode(PERMISSION_CODE)
                .permissions(
                        Manifest.permission.ACCESS_NETWORK_STATE,
//                        Manifest.permission.READ_PHONE_STATE,
//                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermission.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnMPermissionGranted(PERMISSION_CODE)
    public void onBasicPermissionSuccess() {


    }

    @OnMPermissionDenied(PERMISSION_CODE)
    public void onBasicPermissionFailed() {
    }
}