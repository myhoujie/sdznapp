package com.sdzn.pkt.student.phone.mvp.presenter;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.bean.NewLiveInfo;
import com.sdzn.pkt.student.phone.bean.NewVideoInfo;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.event.MineCourseEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.mvp.view.LiveRoomView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

/**
 * 描述：查询直播频道和聊天室信息,回放的视频
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/8/7
 */

public class LiveRoomPresenter extends BasePresenter<LiveRoomView> {


    /**
     * 直播
     *
     * @param kpointId
     */
    public void getLivingInfo(int kpointId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            map.put("channel", "2");
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewLiveInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewLiveInfo>>defaultSchedulers())
                    .map(new ResponseNewFunc<NewLiveInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewLiveInfo>() {
                        @Override
                        public void onNext(NewLiveInfo courses) {
                            if (courses != null) {
                                getView().getLiveRoomInfoSuccrss(courses);
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());

                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }

    }
    /**
     * 回放
     *
     * @param kpointId
     */
    public void getReplayInfo(int kpointId,int courseId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            map.put("courseId", String.valueOf(courseId));
            map.put("channel", "2");
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewReplayInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                    .map(new ResponseNewFunc<NewVideoInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                        @Override
                        public void onNext(NewVideoInfo courses) {
                            if (courses != null) {
                                getView().getReplayInfoSuccess(courses);
                            } else {
                                ToastUtils.showShort("回放教室不存在或已删除");
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
//                        String msg = "回放教室不存在或已删除";
//                        if (e != null) {
//                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
//                        }
                            ToastUtils.showShort("" + e.getMessage());

                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }

    }

    /**
     * 点播   。。
     *
     * @param kpointId
     */
    public void getVideoInfo(int kpointId) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("kpointId", String.valueOf(kpointId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getNewVideoInfo(requestBody)
                    .compose(TransformUtils.<ResultBean<NewVideoInfo>>defaultSchedulers())
                    .map(new ResponseNewFunc<NewVideoInfo>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<NewVideoInfo>() {
                        @Override
                        public void onNext(NewVideoInfo courses) {
                            if (courses != null) {
                                getView().getVideoRoomInfoSuccrss(courses);
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());
                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }

    }
    /**
     * 免费 报名接口
     */
    public void getIsPurchase(int courseId,int type) {
        if (isToLogin()) {
            Map<String, String> map = new HashMap<>();
            map.put("courseId", String.valueOf(courseId));
            String json = new Gson().toJson(map);//要传递的json
            RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            Subscription subscribe = RestApi.getInstance()
                    .createNew(CourseService.class)
                    .getIsPurchase(requestBody)
                    .compose(TransformUtils.<ResultBean<Object>>defaultSchedulers())
                    .map(new ResponseNewFunc<Object>())
                    .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<Object>() {
                        @Override
                        public void onNext(Object courses) {
                            if (courses != null) {
                                getView().applySuccess(type);
                                EventBus.getDefault().post(new MineCourseEvent(true));
                            }

                        }

                        @Override
                        public void onFail(Throwable e) {// 空数据亦报错
                            ToastUtils.showShort("" + e.getMessage());

                        }
                    }, mActivity, false));
            addSubscribe(subscribe);
        }
    }
    /**
     * 点击判断是否登录   未登录 点击 即跳转
     */
    private boolean isToLogin(){
        if (!SPManager.autoLogin(mActivity)){
            IntentController.toLogin(mActivity,true);
            return false;
        }
        return true;
    }
}
