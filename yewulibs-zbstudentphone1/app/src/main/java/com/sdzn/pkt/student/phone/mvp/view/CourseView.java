package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.CourseList;
import com.sdzn.pkt.student.phone.bean.GradeJson;
import com.sdzn.pkt.student.phone.bean.SubjectBean;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/11
 */
public interface CourseView extends BaseView {

//    void getCourseEmpty();
//
//    void getCourseFailure(String message);

//    void getSectionSuccess(List<SectionBean> sectionBeens);
//
//    void getSectionEmpty();

    void getSubjectSuccess(List<SubjectBean> subjectBeens);

    void getSubjectEmpty();

    void onGradeSuccess(List<GradeJson> gradeJson);//section & grade

    void onGradeEmpty();

    void getCourseSuccess(List<CourseList> courseListBeen);

    void getTocFailedEmpty();

    void getTobFailedEmpty();

    void getToLoginEmpty();

}
