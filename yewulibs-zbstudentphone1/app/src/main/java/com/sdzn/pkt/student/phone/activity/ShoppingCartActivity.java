package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.widget.DividerItemDecoration;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.ShoppingCartAdapter;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;
import com.sdzn.pkt.student.phone.event.OrderPayEvent;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.mvp.presenter.ShoppingCartPresenter;
import com.sdzn.pkt.student.phone.mvp.view.ShoppingCartView;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：
 * - 购物车（无法多选结算）
 * 创建人：baoshengxiang
 * 创建时间：2017/7/20
 */
public class ShoppingCartActivity extends BaseMVPActivity<ShoppingCartView, ShoppingCartPresenter> implements ShoppingCartView, com.scwang.smartrefresh.layout.listener.OnRefreshListener {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.swipe_target)
    RecyclerView rcvShoppingCart;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.cb_all_check)
    CheckBox cbAllCheck;
    @BindView(R.id.tv_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.btn_settlement)
    Button btnSettlement;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;

    private ShoppingCartAdapter shoppingCartAdapter;
    private List<ShoppingCartBean.ShopCartListBean> shoppingBeans;
    private boolean isEdit;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected ShoppingCartPresenter createPresenter() {
        return new ShoppingCartPresenter();
    }

    @Override
    protected void onInit(Bundle bundle) {
        EventBus.getDefault().register(this);
        initData();
        initView();
        queryCartData();
    }

    private void initData() {
        shoppingBeans = new ArrayList<>();
    }

    private void initView() {
        titleBar.setRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdit = !isEdit;
                shoppingCartAdapter.setEdit(isEdit);
                if (isEdit) {
                    titleBar.setRightText("取消");
//                    cbAllCheck.setVisibility(View.VISIBLE);
                    tvTotalPrice.setVisibility(View.GONE);
                    btnSettlement.setText("删除");
                } else {
                    titleBar.setRightText("编辑");
//                    cbAllCheck.setVisibility(View.GONE);
                    tvTotalPrice.setVisibility(View.VISIBLE);
                    btnSettlement.setText("支付");
                }
            }
        });

        refreshLayout.setOnRefreshListener(this);
        rcvShoppingCart.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL,
                ResourcesCompat.getColor(getResources(), R.color.gray_ea, null), 10));
        rcvShoppingCart.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        shoppingCartAdapter = new ShoppingCartAdapter(mContext, shoppingBeans);
        rcvShoppingCart.setAdapter(shoppingCartAdapter);
        shoppingCartAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (shoppingCartAdapter.getEdit()) {
                    return;
                }
                ShoppingCartBean.ShopCartListBean course = shoppingBeans.get(position);
                IntentController.toCourseDetail(mContext, course.getSellType(), course.getCourseId(), false);
            }
        });

        shoppingCartAdapter.setCartCallback(new ShoppingCartAdapter.CartCallback() {
            @Override
            public void isSelectAll(boolean isSelectAll) {
                cbAllCheck.setChecked(isSelectAll);
            }

            @Override
            public void backTotalPrice(double price) {
//                ToastUtils.showShort(""+price);
                String totalPrice = String.format(Locale.getDefault(), "￥%.2f", price);
                SpannableString priceSpan = new SpannableString("合计：" + totalPrice);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.textMinor)),
                        0, priceSpan.length() - totalPrice.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                priceSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.red)),
                        priceSpan.length() - totalPrice.length(), priceSpan.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                tvTotalPrice.setText(priceSpan);
            }
        });


        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                queryCartData();
            }
        });

    }

    private SpannableString splitString(String firstStr, int firstColor, String secStr, int secColor) {
        SpannableString spannableString = new SpannableString(firstStr + secStr);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, firstColor)),
                0, firstStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, secColor)),
                firstStr.length(), spannableString.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void deleteShopping() {
        if (shoppingCartAdapter.getCheckedGoods() == null || shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请先选择要删除的物品");
            return;
        }
        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }

        mPresenter.deleteGoods(idSb.toString());
    }

    private void queryCartData() {
        mPresenter.queryShoppingCart();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        queryCartData();
    }

    @OnClick({R.id.btn_settlement, R.id.cb_all_check})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_settlement:
                if (isEdit) {
                    deleteShopping();
                } else {
                    tosettlement();
                }
                break;
            case R.id.cb_all_check:
                shoppingCartAdapter.setSelectAll(cbAllCheck.isChecked());
                break;
            default:
                break;
        }
    }

    /**
     * 去结算
     */
    private void tosettlement() {
        if (shoppingCartAdapter.getCheckedGoods().isEmpty()) {
            ToastUtils.showShort("请至少选择一门课程去结算");
            return;
        }

        StringBuilder idSb = new StringBuilder();
        for (String goodsId : shoppingCartAdapter.getCheckedGoods().keySet()) {
            idSb.append(goodsId).append(",");
        }
        if (idSb.length() != 0) {
            idSb.deleteCharAt(idSb.lastIndexOf(","));
        }
        IntentController.toOrderSubmit(mContext, idSb.toString());
    }

    private void clearRefreshStatus() {
        if (refreshLayout.isRefreshing()) {
            refreshLayout.finishRefresh();
        }
    }

    @Override
    public void queryCartSuccess(List<ShoppingCartBean.ShopCartListBean> shoppingCartBeens) {
        clearRefreshStatus();
        this.shoppingBeans.clear();
        this.shoppingBeans.addAll(shoppingCartBeens);
        shoppingCartAdapter.notifyDataSetChanged();
        titleBar.getView(R.id.tv_right).setVisibility(View.VISIBLE);
        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
//        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(totalPrice), R.color.red));
        tvTotalPrice.setText(splitString("合计：", R.color.textPrimary, String.valueOf(shoppingCartAdapter.getTotalPrice()), R.color.red));

        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void queryCartEmpty() {
        clearRefreshStatus();
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        titleBar.getView(R.id.tv_right).setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.NODATA);
        emptyLayout.setErrorMessage("当前购物车暂无课程");
    }

    @Override
    public void queryCartFailure(String msg) {
        clearRefreshStatus();
        ToastUtils.showShort(msg);
        this.shoppingBeans.clear();
        shoppingCartAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void delGoodsSuccess() {
        if (cbAllCheck.isChecked()) {
            cbAllCheck.setChecked(false);
        }
        shoppingCartAdapter.removeGoods();
//        shoppingCartAdapter.setTotalPrice(0.0f);
        queryCartData();
    }

    @Override
    public void delGoodsFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderPayEvent(OrderPayEvent orderPayEvent) {
        mPresenter.queryShoppingCart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
