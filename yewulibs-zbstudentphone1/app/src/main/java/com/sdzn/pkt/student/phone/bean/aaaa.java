package com.sdzn.pkt.student.phone.bean;

import java.util.List;

public class aaaa {


    /**
     * code : 0
     * errorCode : 0
     * message :
     * result : {"refund_info":{"notRefundReason":"","refundAmount":0,"refundEduReason":"","refundTime":null,"status":"","submitTime":null},"course_info":[{"courseName":"时代智囊点播","currentPrice":1,"logo":"https://csfile.fuzhuxian.com/1593584020269","sellType":"","status":4}],"order_info":{"createTime":null,"orderId":535,"orderNo":"222222220200704143139306","payTime":null,"payType":"alipay","states":"CANCEL","sumMoney":1}}
     * success : true
     */

    private int code;
    private int errorCode;
    private String message;
    private ResultBean result;
    private boolean success;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class ResultBean {
        /**
         * refund_info : {"notRefundReason":"","refundAmount":0,"refundEduReason":"","refundTime":null,"status":"","submitTime":null}
         * course_info : [{"courseName":"时代智囊点播","currentPrice":1,"logo":"https://csfile.fuzhuxian.com/1593584020269","sellType":"","status":4}]
         * order_info : {"createTime":null,"orderId":535,"orderNo":"222222220200704143139306","payTime":null,"payType":"alipay","states":"CANCEL","sumMoney":1}
         */

        private RefundInfoBean refund_info;
        private OrderInfoBean order_info;
        private List<CourseInfoBean> course_info;

        public RefundInfoBean getRefund_info() {
            return refund_info;
        }

        public void setRefund_info(RefundInfoBean refund_info) {
            this.refund_info = refund_info;
        }

        public OrderInfoBean getOrder_info() {
            return order_info;
        }

        public void setOrder_info(OrderInfoBean order_info) {
            this.order_info = order_info;
        }

        public List<CourseInfoBean> getCourse_info() {
            return course_info;
        }

        public void setCourse_info(List<CourseInfoBean> course_info) {
            this.course_info = course_info;
        }

        public static class RefundInfoBean {
            /**
             * notRefundReason :
             * refundAmount : 0
             * refundEduReason :
             * refundTime : null
             * status :
             * submitTime : null
             */

            private String notRefundReason;
            private int refundAmount;
            private String refundEduReason;
            private Object refundTime;
            private String status;
            private Object submitTime;

            public String getNotRefundReason() {
                return notRefundReason;
            }

            public void setNotRefundReason(String notRefundReason) {
                this.notRefundReason = notRefundReason;
            }

            public int getRefundAmount() {
                return refundAmount;
            }

            public void setRefundAmount(int refundAmount) {
                this.refundAmount = refundAmount;
            }

            public String getRefundEduReason() {
                return refundEduReason;
            }

            public void setRefundEduReason(String refundEduReason) {
                this.refundEduReason = refundEduReason;
            }

            public Object getRefundTime() {
                return refundTime;
            }

            public void setRefundTime(Object refundTime) {
                this.refundTime = refundTime;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getSubmitTime() {
                return submitTime;
            }

            public void setSubmitTime(Object submitTime) {
                this.submitTime = submitTime;
            }
        }

        public static class OrderInfoBean {
            /**
             * createTime : null
             * orderId : 535
             * orderNo : 222222220200704143139306
             * payTime : null
             * payType : alipay
             * states : CANCEL
             * sumMoney : 1.0
             */

            private Object createTime;
            private int orderId;
            private String orderNo;
            private Object payTime;
            private String payType;
            private String states;
            private double sumMoney;

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public int getOrderId() {
                return orderId;
            }

            public void setOrderId(int orderId) {
                this.orderId = orderId;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public Object getPayTime() {
                return payTime;
            }

            public void setPayTime(Object payTime) {
                this.payTime = payTime;
            }

            public String getPayType() {
                return payType;
            }

            public void setPayType(String payType) {
                this.payType = payType;
            }

            public String getStates() {
                return states;
            }

            public void setStates(String states) {
                this.states = states;
            }

            public double getSumMoney() {
                return sumMoney;
            }

            public void setSumMoney(double sumMoney) {
                this.sumMoney = sumMoney;
            }
        }

        public static class CourseInfoBean {
            /**
             * courseName : 时代智囊点播
             * currentPrice : 1.0
             * logo : https://csfile.fuzhuxian.com/1593584020269
             * sellType :
             * status : 4
             */

            private String courseName;
            private double currentPrice;
            private String logo;
            private String sellType;
            private int status;

            public String getCourseName() {
                return courseName;
            }

            public void setCourseName(String courseName) {
                this.courseName = courseName;
            }

            public double getCurrentPrice() {
                return currentPrice;
            }

            public void setCurrentPrice(double currentPrice) {
                this.currentPrice = currentPrice;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getSellType() {
                return sellType;
            }

            public void setSellType(String sellType) {
                this.sellType = sellType;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }
}
