package com.sdzn.pkt.student.phone.bean;

/**
 * @author Reisen at 2017-12-25
 */

public class CheckInBean {
    private boolean rollcallstatus;

    public CheckInBean() {
    }

    public CheckInBean(boolean rollcallstatus) {
        this.rollcallstatus = rollcallstatus;
    }

    public boolean isRollcallstatus() {
        return rollcallstatus;
    }

    public void setRollcallstatus(boolean rollcallstatus) {
        this.rollcallstatus = rollcallstatus;
    }
}
