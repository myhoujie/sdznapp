package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public interface RetrievePwdView extends BaseView {

    void getCodeSuccess();

    void getCodeFailure(String msg);

    void confirmCodeSuccess();

    void confirmCodeFailure(String msg);

}
