package com.sdzn.pkt.student.phone.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/7/14
 */
public class HomeBean {

    private List<BannerInfoBean> bannerInfo;
    private List<LiveInfoBean> liveInfo;
    private List<RecmdInfoBean> recmdInfo;
    private TeacherInfo teacherInfo;

    public TeacherInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(TeacherInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public List<BannerInfoBean> getBannerInfo() {
        return bannerInfo;
    }

    public void setBannerInfo(List<BannerInfoBean> bannerInfo) {
        this.bannerInfo = bannerInfo;
    }

    public List<LiveInfoBean> getLiveInfo() {
        return liveInfo;
    }

    public void setLiveInfo(List<LiveInfoBean> liveInfo) {
        this.liveInfo = liveInfo;
    }

    public List<RecmdInfoBean> getRecmdInfo() {
        return recmdInfo;
    }

    public void setRecmdInfo(List<RecmdInfoBean> recmdInfo) {
        this.recmdInfo = recmdInfo;
    }

    public static class TeacherInfo implements Serializable {
        private static final long serialVersionUID = -2255372451517285877L;
        private List<TeacherListBean> teacherList;

        public List<TeacherListBean> getTeacherList() {
            return teacherList;
        }

        public void setTeacherList(List<TeacherListBean> teacherList) {
            this.teacherList = teacherList;
        }
    }

}
