package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.pkt.student.phone.bean.GradeJson;

import java.util.List;

public interface SelectSubjectView {


    void setSuccess();

    void onFailed(String msg);
}
