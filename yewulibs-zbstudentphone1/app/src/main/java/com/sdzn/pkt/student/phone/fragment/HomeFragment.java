package com.sdzn.pkt.student.phone.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.sdzn.core.base.BaseMVPFragment;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.activity.WebBannerActivity;
import com.sdzn.pkt.student.phone.adapter.RecommendAdapter;
import com.sdzn.pkt.student.phone.adapter.WrapAdapter;
import com.sdzn.pkt.student.phone.bean.BannerInfoBean;
import com.sdzn.pkt.student.phone.bean.CourseList;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.mvp.presenter.HomePresenter;
import com.sdzn.pkt.student.phone.mvp.view.HomeView;
import com.sdzn.pkt.student.phone.utils.PermissionUtils;
import com.sdzn.pkt.student.phone.widget.EmptyLayout;
import com.sdzn.pkt.student.phone.widget.RoundImageView;
import com.sdzn.pkt.student.phone.widget.bannerview.MZBannerView;
import com.sdzn.pkt.student.phone.widget.bannerview.holder.MZHolderCreator;
import com.sdzn.pkt.student.phone.widget.bannerview.holder.MZViewHolder;
import com.sdzn.pkt.student.phone.widget.bannerview.transformer.CustomTransformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 描述：
 * - 首页
 * 创建人：baoshengxiang
 * 创建时间：
 */
public class HomeFragment extends BaseMVPFragment<HomeView, HomePresenter> implements HomeView, OnRefreshListener, OnRefreshLoadmoreListener {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.empty_layout)
    EmptyLayout emptyLayout;
    @BindView(R.id.fv_banner)
    MZBannerView fvBanner;

    @BindView(R.id.rv_course)
    RecyclerView rvCourseRec;
    @BindView(R.id.rv_subject)
    RecyclerView rvSubject;

    private List<BannerInfoBean> bannerInfos;
    private List<String> bannerUrls;
    private List<String> bannerlinkAddress;
    private List<String> bannerTypeid;
    private List<String> bannerselltype;
    private List<CourseList> recommendCourses;
    private RecommendAdapter recommendAdapter;
    private WrapAdapter<RecommendAdapter> wrapAdapter;

    private List<CourseList> subjectCourses;
    private RecommendAdapter subjectAdapter;
    private WrapAdapter<RecommendAdapter> subjectwrapAdapter;
    private String typeid;


    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Override
    public void onResume() {
        super.onResume();
        fvBanner.start();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        initData();
        initView();
        loadNetData();
        mPresenter.getBanner();
    }

    private void initData() {
        bannerInfos = new ArrayList<>();
        bannerUrls = new ArrayList<>();
        bannerlinkAddress = new ArrayList<>();
        bannerTypeid = new ArrayList<>();
        bannerselltype = new ArrayList<>();
        recommendCourses = new ArrayList<>();
        subjectCourses = new ArrayList<>();
    }

    private void initView() {
        refreshLayout.setOnRefreshLoadmoreListener(this);
//        rvCourseRec.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        rvCourseRec.setFocusable(false);
        rvCourseRec.setLayoutManager(new GridLayoutManager(mContext, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recommendAdapter = new RecommendAdapter(mContext, recommendCourses);
        wrapAdapter = new WrapAdapter<>(recommendAdapter);
        wrapAdapter.adjustSpanSize(rvCourseRec);
        rvCourseRec.setAdapter(wrapAdapter);

        recommendAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < recommendCourses.size()) {
                    IntentController.toCourseDetail(mContext, recommendCourses.get(position).getSellType(), recommendCourses.get(position).getCourseId(), false);
                }

            }
        });

        rvSubject.setFocusable(false);
        rvSubject.setLayoutManager(new GridLayoutManager(mContext, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        subjectAdapter = new RecommendAdapter(mContext, subjectCourses);
        subjectwrapAdapter = new WrapAdapter<>(subjectAdapter);
        subjectwrapAdapter.adjustSpanSize(rvSubject);
        rvSubject.setAdapter(subjectwrapAdapter);

        subjectAdapter.setOnItemClickListener(new BaseRcvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position < subjectCourses.size()) {
                    IntentController.toCourseDetail(mContext, subjectCourses.get(position).getSellType(), subjectCourses.get(position).getCourseId(), false);
                }

            }
        });

        emptyLayout.setOnLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNetData();
            }
        });
    }

    @OnClick({R.id.tv_course, R.id.tv_subject})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_course:
                IntentController.toRecActivity(mContext);
                break;
            case R.id.tv_subject:
                IntentController.toTopicActivity(mContext);
                break;

        }

    }

    private void loadNetData() {

        mPresenter.getCourse(new HashMap<>());

    }

    private void initBanner() {
        bannerUrls.clear();
        fvBanner.pause();
        for (BannerInfoBean bannerInfoBean : bannerInfos) {
            bannerUrls.add("" + bannerInfoBean.getImageUrl());
            bannerlinkAddress.add("" + bannerInfoBean.getLinkAddress());
            bannerTypeid.add("" + bannerInfoBean.getRelationType());
            bannerselltype.add("" + bannerInfoBean.getSellType());
        }
        fvBanner.setBannerPageClickListener(bannerPageClickListener);
        // 设置数据
        fvBanner.setPages(bannerUrls, new MZHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });
        fvBanner.setPageTransformer(new CustomTransformer());
        fvBanner.start();

    }

    /**
     * banner跳转
     */
    MZBannerView.BannerPageClickListener bannerPageClickListener = new MZBannerView.BannerPageClickListener() {
        @Override
        public void onPageClick(View view, int position) {
//            HiosHelper.resolveAd(getActivity(), getActivity(), "hios://com.haier.cellarette.libwebview.base.WebViewMainActivity2?aid={s}"+bannerlinkAddress.get(position));
//            String validateurl = String.valueOf(bannerlinkAddress.get(position));
//            Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.WebViewMainActivity2");
//            intent.putExtra("validateurl", validateurl);
//            startActivity(intent);
            if (bannerTypeid.get(position).equals("0")) {//跳转H5地址
                Intent intent = new Intent(getActivity(), WebBannerActivity.class);
                intent.putExtra("validateurl", bannerlinkAddress.get(position));
                startActivity(intent);
            } else {//跳转课程
                IntentController.toCourseDetail(mContext, bannerselltype.get(position), Integer.valueOf(bannerlinkAddress.get(position)), true);
            }
        }
    };

    @Override
    public void getDataFailure(String msg) {
        clearLoingState();
        refreshLayout.finishRefresh(false);
        emptyLayout.setErrorType(EmptyLayout.NETWORK_ERROR);
    }

    @Override
    public void getCourseEmpty() {
        clearLoingState();
    }

    @Override
    public void getDataCourse(List<CourseList> courses) {//recommendCourses
        clearLoingState();
        this.recommendCourses.clear();
        if (courses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.recommendCourses.addAll(courses);
        wrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);


    }

    @Override
    public void getSubjectDataCourse(List<CourseList> subjectCourses) {
        clearLoingState();
        this.subjectCourses.clear();
        if (subjectCourses.size() == 0) {
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
            return;
        }
        refreshLayout.setLoadmoreFinished(true);
        this.subjectCourses.addAll(subjectCourses);
        subjectwrapAdapter.notifyDataSetChanged();
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    public void getBannerData(List<BannerInfoBean> infoBeanList) {
        this.bannerInfos.clear();
        this.bannerInfos.addAll(infoBeanList);
        initBanner();

    }

    @Override
    public void getBannerEmpty() {
        clearLoingState();
        fvBanner.setVisibility(View.GONE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionUtils.checkPermissionResult(mContext, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        loadNetData();
        mPresenter.getBanner();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {

    }

    private class BannerViewHolder implements MZViewHolder<String> {
        private RoundImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局
            mImageView = new RoundImageView(context);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);// X和Y方向都填满
            mImageView.setType(RoundImageView.TYPE_ROUND);
            mImageView.setBorderRadius(8);
            mImageView.setImageResource(R.mipmap.place);
            return mImageView;
        }

        @Override
        public void onBind(Context context, int position, String data) {
            // 数据绑定
//            GlideImgManager.loadImage(context, data, R.mipmap.place, R.mipmap.place, mImageView);
            Glide.with(mContext).load(data).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    mImageView.setImageBitmap(resource);
                }
            });
        }
    }

    private void clearLoingState() {
        if (refreshLayout != null) {
            if (refreshLayout.isRefreshing()) {
                refreshLayout.finishRefresh();
            }
            if (refreshLayout.isLoading()) {
                refreshLayout.finishLoadmore();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fvBanner.pause();
    }


}
