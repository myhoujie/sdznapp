package com.sdzn.pkt.student.phone.bean;

import java.util.List;

public class LiveInfoBean {
    /**
     * childKpoints : []
     * courseBuycount : 136
     * courseId : 78
     * courseLiveBeginTime : 2016-07-04 00:00:00
     * courseLiveEndTime : 2016-12-31 00:00:00
     * courseLogo : /images/upload/course/20161222/1482392560946.jpg
     * courseName : 直播-会计基础视频教程全套
     * currentPrice : 0.01
     * free : 0
     * isavaliable : 0
     * kpointAtlasesList : []
     * kpointId : 0
     * kpointList : []
     * kpointType : 0
     * liveBeginTime : 2017-01-01 10:00:00
     * liveEndTime : 2017-08-10 17:00:00
     * memberCourseId : 0
     * name : 第一节
     * pageCount : 0
     * parentId : 0
     * playCount : 0
     * queryLimitNum : 0
     * sort : 0
     * teacherId : 0
     * teacherList : [{"customerId":0,"id":81,"invalid":false,"isStar":0,"name":"李立","orgId":0,"phoneNo":0,"sort":0,"status":0,"subjectId":0},{"customerId":0,"id":74,"invalid":false,"isStar":0,"name":"马妮妮","orgId":0,"phoneNo":0,"sort":0,"status":0,"subjectId":0}]
     */

    private int courseBuycount;
    private int courseId;
    private String courseLiveBeginTime;
    private String courseLiveEndTime;
    private String courseLogo;
    private String courseName;
    private double currentPrice;
    private int free;
    private int isavaliable;
    private int kpointId;
    private int kpointType;
    private String liveBeginTime;
    private String liveEndTime;
    private int memberCourseId;
    private String name;
    private int pageCount;
    private int parentId;
    private int playCount;
    private int queryLimitNum;
    private int sort;
    private int teacherId;
    private List<?> childKpoints;
    private List<?> kpointAtlasesList;
    private List<?> kpointList;
    private List<TeacherListBean> teacherList;

    public int getCourseBuycount() {
        return courseBuycount;
    }

    public void setCourseBuycount(int courseBuycount) {
        this.courseBuycount = courseBuycount;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseLiveBeginTime() {
        return courseLiveBeginTime;
    }

    public void setCourseLiveBeginTime(String courseLiveBeginTime) {
        this.courseLiveBeginTime = courseLiveBeginTime;
    }

    public String getCourseLiveEndTime() {
        return courseLiveEndTime;
    }

    public void setCourseLiveEndTime(String courseLiveEndTime) {
        this.courseLiveEndTime = courseLiveEndTime;
    }

    public String getCourseLogo() {
        return courseLogo;
    }

    public void setCourseLogo(String courseLogo) {
        this.courseLogo = courseLogo;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getFree() {
        return free;
    }

    public void setFree(int free) {
        this.free = free;
    }

    public int getIsavaliable() {
        return isavaliable;
    }

    public void setIsavaliable(int isavaliable) {
        this.isavaliable = isavaliable;
    }

    public int getKpointId() {
        return kpointId;
    }

    public void setKpointId(int kpointId) {
        this.kpointId = kpointId;
    }

    public int getKpointType() {
        return kpointType;
    }

    public void setKpointType(int kpointType) {
        this.kpointType = kpointType;
    }

    public String getLiveBeginTime() {
        return liveBeginTime;
    }

    public void setLiveBeginTime(String liveBeginTime) {
        this.liveBeginTime = liveBeginTime;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public int getMemberCourseId() {
        return memberCourseId;
    }

    public void setMemberCourseId(int memberCourseId) {
        this.memberCourseId = memberCourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    public int getQueryLimitNum() {
        return queryLimitNum;
    }

    public void setQueryLimitNum(int queryLimitNum) {
        this.queryLimitNum = queryLimitNum;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public List<?> getChildKpoints() {
        return childKpoints;
    }

    public void setChildKpoints(List<?> childKpoints) {
        this.childKpoints = childKpoints;
    }

    public List<?> getKpointAtlasesList() {
        return kpointAtlasesList;
    }

    public void setKpointAtlasesList(List<?> kpointAtlasesList) {
        this.kpointAtlasesList = kpointAtlasesList;
    }

    public List<?> getKpointList() {
        return kpointList;
    }

    public void setKpointList(List<?> kpointList) {
        this.kpointList = kpointList;
    }

    public List<TeacherListBean> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<TeacherListBean> teacherList) {
        this.teacherList = teacherList;
    }

}