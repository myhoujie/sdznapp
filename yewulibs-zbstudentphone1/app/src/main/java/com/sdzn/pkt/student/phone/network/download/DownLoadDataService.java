package com.sdzn.pkt.student.phone.network.download;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.Nullable;

import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.CloseUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.pkt.student.phone.activity.MainActivity;
import com.sdzn.pkt.student.phone.annotation.DownloadState;
import com.sdzn.pkt.student.phone.bean.CourseFileBean;
import com.sdzn.pkt.student.phone.manager.DaoManger;
import com.sdzn.pkt.student.phone.utils.CacheUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Reisen at 2018-05-24
 */

public class DownLoadDataService extends Service {

    private static final int MESSAGE = 123;
    private OkHttpClient client;
    private static Handler mHandler;
    private LinkedList<DownloadListener> mListeners = new LinkedList<>();

    private static Intent sIntent;


    /*================  ================*/
    private static DownLoadDataService sService;

    /**
     * 避免重复启动service
     */
    private static boolean serviceOnStart = false;

    /**
     * 启动service
     */
    public static void startDownloadService() {
        if (serviceOnStart || checkServiceRunning(DownLoadDataService.class.getName())) {
            return;
        }
        Activity main = AppManager.getAppManager().getActivity(MainActivity.class);
        if (main == null) {
            main = AppManager.getAppManager().currentActivity();
        }
        if (main == null) {
            ToastUtils.showShort("资料下载服务启动失败, 请尝试重启应用恢复服务");
            return;
        }
        boolean isRunning = checkServiceRunning(DownLoadDataService.class.getName());
        if (!isRunning) {
            sIntent = new Intent(main, DownLoadDataService.class);
            main.startService(sIntent);
        }
        main.bindService(sIntent, connection, Context.BIND_AUTO_CREATE);
    }

    private static ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DownloadBinder binder = (DownloadBinder) service;
            sService = binder.getService();
//            serviceOnStart = false;
            serviceOnStart = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Nullable
    public static DownLoadDataService getService() {
        if (sService != null) {
            return sService;
        }
        startDownloadService();
        return sService;
    }

    public static void stopDownloadService(Activity activity, ServiceConnection connection) {
        if (!checkServiceRunning(activity, DownLoadDataService.class.getName())) {
            return;
        }
        activity.unbindService(connection);
        activity.stopService(sIntent);
    }

    public static void stopDownloadService() {
        Activity main = AppManager.getAppManager().getActivity(MainActivity.class);
        if (main == null) {
            main = AppManager.getAppManager().currentActivity();
        }
        if (main == null) {
            return;
        }
        stopDownloadService(main, connection);
    }

    public static void stopDownloadService(Activity activity) {
        stopDownloadService(activity, connection);
    }

    /*================  ================*/

    /**
     * 检查本服务是否运行
     */

    private static boolean checkServiceRunning(Activity activity, String serviceName) {
        ActivityManager am = (ActivityManager) activity
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (am == null) {
            return false;
        }
        int serviceCount = 64;
        List<ActivityManager.RunningServiceInfo> list;
        do {
            serviceCount = (int) (serviceCount * 1.5);
            list = am.getRunningServices(serviceCount);
            if (list == null) {
                return false;
            }
        } while (list.size() == serviceCount);
        for (ActivityManager.RunningServiceInfo info : list) {
            if (serviceName.equals(info.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkServiceRunning(String serviceName) {
        Activity activity = AppManager.getAppManager().currentActivity();
        if (activity == null) {
            ToastUtils.showShort("资料下载服务启动失败, 请尝试重启应用恢复服务");
            return false;
        }
        return checkServiceRunning(activity, serviceName);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new DownloadBinder();
    }

    public class DownloadBinder extends Binder {
        public DownLoadDataService getService() {
            return DownLoadDataService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what != MESSAGE) {
                    return;
                }
                if (mListeners != null) {
                    synchronized (DownLoadDataService.class) {
                        for (DownloadListener listener : mListeners) {
                            listener.onDownload();
                        }
                    }
                }
            }
        };
        client = new OkHttpClient.Builder()
                .connectTimeout(12, TimeUnit.SECONDS)
                .writeTimeout(12, TimeUnit.SECONDS)
                .build();
    }

    /**
     * 查询某个资料下载状态
     *
     * @return {@link DownloadState#UnDownLoad,DownloadState#Downloading,DownloadState#Finish}
     */
    public @DownloadState
    int getDownloadState(CourseFileBean bean) {
        CourseFileBean fileBean = DaoManger.getInstance().getSession().getCourseFileBeanDao().load(bean.getId());
        if (fileBean != null) {
            bean.setProgress(fileBean.getProgress());
            bean.setDownloadState(fileBean.getDownloadState());
        }
        int state = bean.getDownloadState();
        switch (state) {
            case DownloadState.Error:
            case DownloadState.Cancel:
            case DownloadState.Pause:
                state = DownloadState.UnDownLoad;
                break;
            case DownloadState.Finish:
                File file = new File(CacheUtils.getDownloadCache(), bean.getSrcFileName());
                if (!(file.exists() && file.isFile())) {
                    bean.setProgress(0);
                    bean.setDownloadState(DownloadState.UnDownLoad);
                    DaoManger.getInstance().getSession().getCourseFileBeanDao().update(bean);
                    state = DownloadState.UnDownLoad;
                } else {
                    state = DownloadState.Finish;
                }
                break;
            case DownloadState.UnDownLoad:
            case DownloadState.Downloading:
                break;
        }
        return state;
    }

    /**
     * 下载任务
     */
    public void download(CourseFileBean bean) {
        Request request = new Request.Builder().get()
                .addHeader("Accept-Encoding", "identity")
                .url(bean.getFilePath())
                .build();
        client.newCall(request).enqueue(new DownloadCallBack(bean) {
            @Override
            public void onResponse(Call call, Response response, CourseFileBean bean) {
                File targetFile = new File(CacheUtils.getDownloadCache(), bean.getSrcFileName());
                long totalLen = response.body().contentLength();
                long totalProgress = 0;
                int len;
                byte[] bytes = new byte[1024 * 128];
                InputStream is = null;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    fos = new FileOutputStream(targetFile);
                    DaoManger.getInstance().getSession().getCourseFileBeanDao().insertOrReplace(bean);
                    while ((len = is.read(bytes)) != -1) {
                        if (bean.getDownloadState() == DownloadState.Cancel) {
                            CloseUtils.closeIOQuietly(is, fos);
                            delFile(bean);
                            return;
                        }
                        fos.write(bytes, 0, len);
                        fos.flush();
                        totalProgress += len;
                        bean.setProgress(totalProgress * 1f / totalLen);
                        bean.setDownloadState(DownloadState.Downloading);
                        DaoManger.getInstance().getSession().getCourseFileBeanDao().update(bean);
                        mHandler.sendEmptyMessage(MESSAGE);
                    }
                    bean.setProgress(1);
                    bean.setDownloadState(DownloadState.Finish);
                    DaoManger.getInstance().getSession().getCourseFileBeanDao().update(bean);
                    mHandler.sendEmptyMessage(MESSAGE);
                } catch (IOException e) {
                    e.printStackTrace();
                    delFile(bean);
                } finally {
                    CloseUtils.closeIOQuietly(is, fos);
                }
            }
        });
    }

    /**
     * 删除任务和文件
     */
    public void delete(CourseFileBean bean) {
        delFile(bean);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        synchronized (DownLoadDataService.class) {
            mListeners.clear();
        }
        stopSelf();
        super.onDestroy();
    }

    private abstract static class DownloadCallBack implements Callback {
        private CourseFileBean bean;

        DownloadCallBack(CourseFileBean bean) {
            this.bean = bean;
        }

        @Override
        public void onFailure(Call call, IOException e) {
            e.printStackTrace();
            delFile(bean);
        }

        @Override
        public void onResponse(Call call, Response response) {
            try {
                onResponse(call, response, bean);
            } catch (Exception e) {
                e.printStackTrace();
                delFile(bean);
            }
        }

        public abstract void onResponse(Call call, Response response, CourseFileBean bean);
    }

    public interface DownloadListener {
        /**
         * 下载状态更新时回调此接口
         */
        void onDownload();

    }

    public void addDownloadListener(DownloadListener listener) {
        if (listener == null) {
            return;
        }
        mListeners.add(listener);
    }

    public void removeDownloadListener(DownloadListener listener) {
        synchronized (DownLoadDataService.class) {
            for (DownloadListener downloadListener : mListeners) {
                if (downloadListener == listener) {
                    mListeners.remove(downloadListener);
                    return;
                }
            }
        }
    }

    /**
     * 删除文件并发送消息
     */
    private static void delFile(CourseFileBean bean) {
        File file = new File(CacheUtils.getDownloadCache(), bean.getSrcFileName());
        if (file.exists() && file.isFile()) {
            file.delete();
        }
        bean.setProgress(0);
        bean.setDownloadState(DownloadState.UnDownLoad);
        DaoManger.getInstance().getSession().getCourseFileBeanDao().delete(bean);
        mHandler.sendEmptyMessage(MESSAGE);
    }
}
