package com.sdzn.pkt.student.phone.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.AppUtils;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.FileUtils;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.manager.IntentController;
import com.sdzn.pkt.student.phone.manager.SPManager;
import com.sdzn.pkt.student.phone.utils.CacheUtils;
import com.sdzn.pkt.student.phone.widget.DialogUtil;
import com.sdzn.pkt.student.phone.utils.GlideCatchUtil;
import com.sdzn.pkt.student.phone.widget.RoundRectImageView;
import com.sdzn.pkt.student.phone.widget.TitleBar;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：系统设置
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/5
 */

public class SystemSettingActivity extends BaseActivity {
    @BindView(R.id.img_avatar)
    RoundRectImageView imgAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_class)
    TextView tvUserClass;
    @BindView(R.id.tv_cache)
    TextView tvCache;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.ll_about)
    LinearLayout llAbout;
    @BindView(R.id.ll_clearcache)
    LinearLayout llClearcache;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.switch_wifi)
    CheckBox switchWifi;
    UserBean userBean;
    private String grade;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_systemsetting;
    }

    @Override
    protected void onInit(Bundle bundle) {
        userBean = SPManager.getUser();
        initView();

    }

    private void initView() {
        GlideImgManager.loadImage(mContext, "" + userBean.getPicImg(),
                R.mipmap.ic_avatar, R.mipmap.ic_avatar, imgAvatar);
        if (userBean.getStudentName() != null) {
            tvUserName.setText(userBean.getStudentName());
        }
        tvUserClass.setText(userBean.getSubjectName());
        tvVersion.setText(AppUtils.getAppVersionName(mContext));
        switchWifi.setChecked(SPManager.getMobileNetSwitch());
        tvCache.setText(getCacheSizes());
    }

    @OnClick({R.id.switch_wifi, R.id.ll_clearcache, R.id.ll_about,R.id.tv_logout,R.id.ll_fk,R.id.ll_user,R.id.ll_agreement,R.id.ll_close_account})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.switch_wifi:
                SPManager.changeMobileNetSwitch(switchWifi.isChecked());
                break;
            case R.id.ll_clearcache:
                DialogUtil.showDialog(this, "确定要清理缓存吗？", true, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearCaches();//清理缓存
                    }
                });
                break;
            case R.id.ll_about:
                IntentController.toAbout(mContext);
                break;
            case R.id.tv_logout:
                showExitDialog();
                break;
            case R.id.ll_fk:
                IntentController.toFKActivity(mContext);
                break;
            case R.id.ll_user:
                IntentController.toWeb(mContext,"1");
                break;
            case R.id.ll_agreement:
                IntentController.toWeb(mContext,"2");
                break;
            case R.id.ll_close_account:
                IntentController.toAccountRemove(mContext);
                break;
        }
    }

    private void clearCaches() {
        GlideCatchUtil.getInstance().cleanCacheDiskSelf();
        FileUtils.deleteFilesInDir(CacheUtils.getDownloadCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAvatarCache());
        FileUtils.deleteFilesInDir(CacheUtils.getVideoScreenshotCache());
        FileUtils.deleteFilesInDir(CacheUtils.getImageCache());
        FileUtils.deleteFilesInDir(CacheUtils.getAppCache());
        //清除完数据后
        tvCache.setText(getCacheSizes());
        ToastUtils.showShort("清理成功");
    }

    private String getCacheSizes() {
        long imageCacheSize = FileUtils.getDirLength(CacheUtils.getImageCache());
        long appCacheSize = FileUtils.getDirLength(CacheUtils.getAppCache());
        return (imageCacheSize == -1 || appCacheSize == -1) ? "" :
                ConvertUtils.byte2FitMemorySize(imageCacheSize + appCacheSize);
    }
    private void showExitDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("确定要退出登录吗？")
                .setPositiveButton("确定", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        logout();
                    }
                })
                .setNegativeButton("取消", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        //隐藏dialog
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }

    private void logout() {
        AppManager.getAppManager().appExit();
        SPManager.changeLogin(mContext, false);
        IntentController.toMain(mContext,false);
    }

}
