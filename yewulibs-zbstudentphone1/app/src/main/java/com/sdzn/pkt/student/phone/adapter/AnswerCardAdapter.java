package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.core.content.ContextCompat;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.AnswerCardBean;
import com.sdzn.pkt.student.phone.manager.constant.QuestionCons;
import com.sdzn.pkt.student.phone.widget.FlowRadioGroup;
import com.sdzn.pkt.student.phone.widget.flowlayout.FlowLayout;

import java.util.List;

import static com.sdzn.pkt.student.phone.R.id.rg_answer_judge;

/**
 * 描述：
 * - 直播答题卡适配器
 * 创建人：baoshengxiang
 * 创建时间：2017/9/27
 */
public class AnswerCardAdapter extends BaseRcvAdapter<AnswerCardBean.DetailBean> {

    public AnswerCardAdapter(Context context, List<AnswerCardBean.DetailBean> mList) {
        super(context, mList);
    }

    @Override
    public int getItemViewType(int position) {
        String subject = mList.get(position).getSubject();
        switch (subject) {
            case "single":
                return QuestionCons.Type.SINGLE;
            case "multi":
                return QuestionCons.Type.MULTIPLE;
            case "judge":
                return QuestionCons.Type.JUDGE;
            default:
                return -1;
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        switch (viewType) {
            case QuestionCons.Type.SINGLE:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_single);
                break;
            case QuestionCons.Type.MULTIPLE:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_multiple);
                break;
            case QuestionCons.Type.JUDGE:
                holder = BaseViewHolder.get(context, null, parent, R.layout.item_answer_judge);
                break;
            default:
                break;
        }
        return holder;
    }

    @Override
    public void convert(BaseViewHolder holder, final int position, AnswerCardBean.DetailBean cardDetailBean) {
//        holder.setIsRecyclable(false);
        holder.setText(R.id.tv_question_num, (cardDetailBean.getNum() + 1) + "、");
        holder.setText(R.id.tv_question_point, cardDetailBean.getPoint() + "分");
        if (!isEdit && !cardDetailBean.isIsCorrect()) {
            holder.setTextColor(R.id.tv_question_num, ContextCompat.getColor(context, R.color.red));
            holder.setTextColor(R.id.tv_question_type, ContextCompat.getColor(context, R.color.red));
        }

        switch (holder.getItemViewType()) {
            case QuestionCons.Type.SINGLE:
                FlowRadioGroup frgOption = holder.getView(R.id.frg_option);
                addSingleButton(position, frgOption);
                break;
            case QuestionCons.Type.MULTIPLE:
                FlowLayout flowLayout = holder.getView(R.id.fl_option);
                addMultipleButton(position, flowLayout);
                break;
            case QuestionCons.Type.JUDGE:
                RadioGroup rgAnswerJudge = holder.getView(rg_answer_judge);
                addJudgeButton(position, rgAnswerJudge);
                break;
            default:
                break;
        }
    }

    /**
     * 添加判断题选项
     *
     * @param position
     * @param rgAnswerJudge
     */
    private void addJudgeButton(final int position, final RadioGroup rgAnswerJudge) {
        final AnswerCardBean.DetailBean answerCardBean = mList.get(position);
        for (int i = 0; i < rgAnswerJudge.getChildCount(); i++) {
            final RadioButton rbAnswerJudge = (RadioButton) rgAnswerJudge.getChildAt(i);
            final int optionPos = i;
            rbAnswerJudge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        answerCardBean.getRespondence().clear();
                        answerCardBean.getRespondence().add(optionPos);
                        calculation(answerCardBean);
                    }
                }
            });
            changeEidtStatusStyle(rbAnswerJudge, answerCardBean, i);
        }
        rgAnswerJudge.clearCheck();
        if (!answerCardBean.getRespondence().isEmpty()) {
            rgAnswerJudge.check(rgAnswerJudge.getChildAt(answerCardBean.getRespondence().get(0)).getId());
        }
    }

    /**
     * 添加多选题选项
     *
     * @param position
     * @param flowLayout
     */
    private void addMultipleButton(final int position, FlowLayout flowLayout) {
        flowLayout.removeAllViews();
        final AnswerCardBean.DetailBean answerCardBean = mList.get(position);
        for (int i = 0; i < answerCardBean.getAnswerNum(); i++) {
            CheckBox checkBox = new CheckBox(context);
            setOptionStyle(checkBox);
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(answerCardBean.getRespondence().contains(i));
            checkBox.setText(String.valueOf((char) (i + 'A')));
            final int optionPos = i;
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (!answerCardBean.getRespondence().contains(optionPos)) {
                            answerCardBean.getRespondence().add(optionPos);
                        }
                    } else {
                        answerCardBean.getRespondence().remove(Integer.valueOf(optionPos));
                    }
                    calculation(answerCardBean);
                }
            });
            changeEidtStatusStyle(checkBox, answerCardBean, i);
            flowLayout.addView(checkBox);
        }
    }

    /**
     * 添加单选题选项
     *
     * @param position
     * @param frgOption
     */
    private void addSingleButton(final int position, FlowRadioGroup frgOption) {
        frgOption.removeAllViews();
        final AnswerCardBean.DetailBean answerCardBean = mList.get(position);
        for (int i = 0; i < answerCardBean.getAnswerNum(); i++) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setId(position * 1000 + i);
            setOptionStyle(radioButton);
            radioButton.setOnCheckedChangeListener(null);
            radioButton.setChecked(answerCardBean.getRespondence().contains(i));
            radioButton.setText(String.valueOf((char) (i + 'A')));
            final int optionPos = i;
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        answerCardBean.getRespondence().clear();
                        answerCardBean.getRespondence().add(optionPos);
                        calculation(answerCardBean);
                    }
                }
            });
            changeEidtStatusStyle(radioButton, answerCardBean, i);
            frgOption.addView(radioButton);
        }
    }

    /**
     * 修改编辑状态样式
     *
     * @param button         选项
     * @param answerCardBean 数据对象
     * @param pos            当前选项位置
     */
    private void changeEidtStatusStyle(CompoundButton button, AnswerCardBean.DetailBean answerCardBean, int pos) {
        GradientDrawable drawable = (GradientDrawable) ContextCompat.getDrawable(context, R.drawable.shape_answer);
        drawable.setColor(ContextCompat.getColor(context, R.color.color_gray_dfe2e6));
        if (!isEdit) {
            button.setEnabled(false);
            if (answerCardBean.isIsCorrect()) {
                if (button.isChecked()) {
                    drawable.setColor(ContextCompat.getColor(context, R.color.green));
                    button.setTextColor(ContextCompat.getColor(context, R.color.gray_f6));
                }
            } else {
                if (answerCardBean.getCorrectAnswer().contains(pos)) {
                    drawable.setColor(ContextCompat.getColor(context, R.color.green));
                    button.setTextColor(ContextCompat.getColor(context, R.color.gray_f6));
                } else if (button.isChecked()) {
                    drawable.setColor(ContextCompat.getColor(context, R.color.red));
                    button.setTextColor(ContextCompat.getColor(context, R.color.gray_f6));
                }
            }
            button.setBackgroundDrawable(drawable);
        } else {

            button.setBackgroundDrawable(null);
            button.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bg_answer));
        }
    }

    private void calculation(AnswerCardBean.DetailBean answerCardBean) {
        if ("multi".equals(answerCardBean.getSubject())) {
            if (!answerCardBean.getRespondence().isEmpty()
                    && answerCardBean.getRespondence().size() == answerCardBean.getCorrectAnswer().size()) {
                answerCardBean.setIsCorrect(true);
                for (int i = 0; i < answerCardBean.getRespondence().size(); i++) {
                    if (!answerCardBean.getCorrectAnswer().contains(answerCardBean.getRespondence().get(i))) {
                        answerCardBean.setIsCorrect(false);
                    }
                }
            }
        } else {
            if (!answerCardBean.getRespondence().isEmpty()) {
                if (answerCardBean.getCorrectAnswer().contains(answerCardBean.getRespondence().get(0))) {
                    answerCardBean.setIsCorrect(true);
                } else {
                    answerCardBean.setIsCorrect(false);
                }
            }
        }
    }

    public AnswerCardBean getAnswerResult() {
        AnswerCardBean answerCardBean = new AnswerCardBean();
        int score = 0;
        boolean isAnswer = false;
        for (AnswerCardBean.DetailBean detailBean : mList) {
            if (detailBean.isIsCorrect()) {
                score += Integer.valueOf(detailBean.getPoint());
            }
            if (!detailBean.getRespondence().isEmpty()) {
                isAnswer = true;
            }
        }
        answerCardBean.setIsAnswer(isAnswer);
        answerCardBean.setTotalPoint(score);
        answerCardBean.setDetail(mList);
        return answerCardBean;
    }

    private void setOptionStyle(CompoundButton button) {
        int width = ConvertUtils.dp2px(context, 41);
        int margin = ConvertUtils.dp2px(context, 5);
        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(width, width);
        layoutParams.setMargins(margin, margin, margin, margin);
        button.setLayoutParams(layoutParams);
        button.setButtonDrawable(null);
        button.setGravity(Gravity.CENTER);
        button.setTextColor(ContextCompat.getColorStateList(context, R.color.answer_text_selector));
        button.setTextSize(18);
    }

}
