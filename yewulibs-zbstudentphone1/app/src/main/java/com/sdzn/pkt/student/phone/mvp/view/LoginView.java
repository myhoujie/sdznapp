package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.AccountBean;
import com.sdzn.pkt.student.phone.bean.UserBean;
import com.sdzn.pkt.student.phone.bean.UserLoginBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface LoginView extends BaseView {

    void loginSuccess(UserLoginBean accountBean);

    void loginFailure(String msg);

    void updateVersion(String updateInfo, String targetUrl);

    void getCodeSuccess();

    void getCodeFailure(String msg);


}
