package com.sdzn.pkt.student.phone.bean;

import java.util.List;

public class MineListRows {


    /**
     * rows : [{"addTime":"2019-11-14 14:35:22","appointSchoolId":"","assistantTeacherId":0,"auditionLessionCount":0,"classId":313,"context":"<p>33346543<\/p>","courseGrossIncome":0,"courseId":881,"courseKpointLits":[],"courseName":"测试课程001","courseTerm":0,"currentPrice":0,"customerId":0,"endTime":null,"grade":0,"isAvaliable":0,"isPublic":0,"kpointNum":6,"lessionNum":0,"levelId":0,"liveBeginTime":null,"liveCourseId":0,"liveEndTime":null,"logo":"http://file.znclass.com/0c21d23e-4836-42ba-9e63-0e325e254127bg.png","loseTime":"","losetype":0,"mainTeacherId":0,"orgId":0,"packageEduCourseList":[],"packageType":0,"pageBuycount":0,"pageViewcount":0,"refundEndTime":null,"sellType":"LIVE","sequence":0,"signEndTime":"2019-11-27 14:46:53","sourcePrice":0,"subjectId":333,"subjectName":"英语","teacherId":0,"teacherName":"寿大祖","title":"1111","updateTime":null}]
     * total : 1
     */

    private int total;
    private List<MineList> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MineList> getRows() {
        return rows;
    }

    public void setRows(List<MineList> rows) {
        this.rows = rows;
    }
}
