package com.sdzn.pkt.student.phone.mvp.presenter;

import com.google.gson.Gson;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.pkt.student.phone.bean.CourseListBean;
import com.sdzn.pkt.student.phone.bean.CourseListRows;
import com.sdzn.pkt.student.phone.bean.ResultBean;
import com.sdzn.pkt.student.phone.mvp.view.SpellingContentView;
import com.sdzn.pkt.student.phone.network.RestApi;
import com.sdzn.pkt.student.phone.network.api.CourseService;
import com.sdzn.pkt.student.phone.network.api.ResponseNewSchoolFunc;
import com.sdzn.pkt.student.phone.network.subscriber.MProgressSubscriber;

import java.util.Map;

import okhttp3.RequestBody;
import rx.Subscription;

public class CourseTopicContentPresenter extends BasePresenter<SpellingContentView> {

    public void getCourse(Map<String, String> map) {
        String json = new Gson().toJson(map);//要传递的json
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        Subscription subscribe = RestApi.getInstance()
                .createNew(CourseService.class)
                .getTopicCourse(requestBody)
                .compose(TransformUtils.<ResultBean<CourseListBean>>defaultSchedulers())
                .map(new ResponseNewSchoolFunc<CourseListBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<CourseListBean>() {
                    @Override
                    public void onNext(CourseListBean courses) {
                        if (courses != null) {
                            getView().getDataCourse(courses.getList().getRows());
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {// 空数据亦报错
                        getView().onFailed("" + e.getMessage());
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
}
