package com.sdzn.pkt.student.phone.bean;

import java.util.List;

/**
 * @author Reisen at 2018-06-05
 */

public class RecentStudyBean {
    /**
     * recentStudyCourse : {"addTime":1513735717000,"auditionLessionCount":0,"classId":315,"context":"<p>初中七年级上册 数学<\/p>","courseId":379,"courseName":"初中七年级上册-数学","courseStudyhistory":{"courseId":379,"courseName":"初中七年级上册-数学","databack":"2018-03-23 15:52:20,2018-03-23 15:52:07,","id":3514,"kpointId":2108,"kpointName":" 科学计数法","playercount":2,"updateTime":1521791541000,"userId":1775},"courseTerm":3,"currentPrice":0,"customerId":42,"grade":1,"isavaliable":1,"lessionNum":10,"levelSubject":{"createTime":1485162081000,"logo":"","parentId":0,"sort":2,"status":0,"subjectId":315,"subjectName":"初中","type":"course"},"liveBeginTime":1513756800000,"liveCourseId":0,"liveEndTime":1521792000000,"livingKpointCount":0,"logo":"/images/upload/course/20180105/1515116621424.jpg","loseTime":"360","loseType":1,"pageBuycount":0,"pageViewcount":115,"refundEndTime":1513476399000,"sellType":"LIVE","signEndTime":1513476382000,"sourcePrice":0,"sqlMap":{},"state":"直播结束","studyPercent":"100","subjectId":323,"teacherList":[{"picPath":"","id":225,"name":"常清","education":"数学讲师"}],"title":"初中七年级上册 数学","updateTime":1515116623000,"userId":0,"userType":0}
     */

    private RecentStudyCourseBean recentStudyCourse;

    public RecentStudyCourseBean getRecentStudyCourse() {
        return recentStudyCourse;
    }

    public void setRecentStudyCourse(RecentStudyCourseBean recentStudyCourse) {
        this.recentStudyCourse = recentStudyCourse;
    }

    public static class RecentStudyCourseBean {
        /**
         * addTime : 1513735717000
         * auditionLessionCount : 0
         * classId : 315
         * context : <p>初中七年级上册 数学</p>
         * courseId : 379
         * courseName : 初中七年级上册-数学
         * courseStudyhistory : {"courseId":379,"courseName":"初中七年级上册-数学","databack":"2018-03-23 15:52:20,2018-03-23 15:52:07,","id":3514,"kpointId":2108,"kpointName":" 科学计数法","playercount":2,"updateTime":1521791541000,"userId":1775}
         * courseTerm : 3
         * currentPrice : 0
         * customerId : 42
         * grade : 1
         * isavaliable : 1
         * lessionNum : 10
         * levelSubject : {"createTime":1485162081000,"logo":"","parentId":0,"sort":2,"status":0,"subjectId":315,"subjectName":"初中","type":"course"}
         * liveBeginTime : 1513756800000
         * liveCourseId : 0
         * liveEndTime : 1521792000000
         * livingKpointCount : 0
         * logo : /images/upload/course/20180105/1515116621424.jpg
         * loseTime : 360
         * loseType : 1
         * pageBuycount : 0
         * pageViewcount : 115
         * refundEndTime : 1513476399000
         * sellType : LIVE
         * signEndTime : 1513476382000
         * sourcePrice : 0
         * sqlMap : {}
         * state : 直播结束
         * studyPercent : 100
         * subjectId : 323
         * teacherList : [{"picPath":"","id":225,"name":"常清","education":"数学讲师"}]
         * title : 初中七年级上册 数学
         * updateTime : 1515116623000
         * userId : 0
         * userType : 0
         */

        private long addTime;
        private int auditionLessionCount;
        private int classId;
        private String context;
        private int courseId;
        private String courseName;
        private CourseStudyhistoryBean courseStudyhistory;
        private int courseTerm;
        private int currentPrice;
        private int customerId;
        private int grade;
        private int isavaliable;
        private int lessionNum;
        private LevelSubjectBean levelSubject;
        private long liveBeginTime;
        private int liveCourseId;
        private long liveEndTime;
        private int livingKpointCount;
        private String logo;
        private String loseTime;
        private int loseType;
        private int pageBuycount;
        private int pageViewcount;
        private long refundEndTime;
        private String sellType;
        private long signEndTime;
        private int sourcePrice;
        private SqlMapBean sqlMap;
        private String state;
        private String studyPercent;
        private int subjectId;
        private String title;
        private long updateTime;
        private int userId;
        private int userType;
        private List<TeacherListBean> teacherList;

        public long getAddTime() {
            return addTime;
        }

        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }

        public int getAuditionLessionCount() {
            return auditionLessionCount;
        }

        public void setAuditionLessionCount(int auditionLessionCount) {
            this.auditionLessionCount = auditionLessionCount;
        }

        public int getClassId() {
            return classId;
        }

        public void setClassId(int classId) {
            this.classId = classId;
        }

        public String getContext() {
            return context;
        }

        public void setContext(String context) {
            this.context = context;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }

        public String getCourseName() {
            return courseName;
        }

        public void setCourseName(String courseName) {
            this.courseName = courseName;
        }

        public CourseStudyhistoryBean getCourseStudyhistory() {
            return courseStudyhistory;
        }

        public void setCourseStudyhistory(CourseStudyhistoryBean courseStudyhistory) {
            this.courseStudyhistory = courseStudyhistory;
        }

        public int getCourseTerm() {
            return courseTerm;
        }

        public void setCourseTerm(int courseTerm) {
            this.courseTerm = courseTerm;
        }

        public int getCurrentPrice() {
            return currentPrice;
        }

        public void setCurrentPrice(int currentPrice) {
            this.currentPrice = currentPrice;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public int getGrade() {
            return grade;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }

        public int getIsavaliable() {
            return isavaliable;
        }

        public void setIsavaliable(int isavaliable) {
            this.isavaliable = isavaliable;
        }

        public int getLessionNum() {
            return lessionNum;
        }

        public void setLessionNum(int lessionNum) {
            this.lessionNum = lessionNum;
        }

        public LevelSubjectBean getLevelSubject() {
            return levelSubject;
        }

        public void setLevelSubject(LevelSubjectBean levelSubject) {
            this.levelSubject = levelSubject;
        }

        public long getLiveBeginTime() {
            return liveBeginTime;
        }

        public void setLiveBeginTime(long liveBeginTime) {
            this.liveBeginTime = liveBeginTime;
        }

        public int getLiveCourseId() {
            return liveCourseId;
        }

        public void setLiveCourseId(int liveCourseId) {
            this.liveCourseId = liveCourseId;
        }

        public long getLiveEndTime() {
            return liveEndTime;
        }

        public void setLiveEndTime(long liveEndTime) {
            this.liveEndTime = liveEndTime;
        }

        public int getLivingKpointCount() {
            return livingKpointCount;
        }

        public void setLivingKpointCount(int livingKpointCount) {
            this.livingKpointCount = livingKpointCount;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getLoseTime() {
            return loseTime;
        }

        public void setLoseTime(String loseTime) {
            this.loseTime = loseTime;
        }

        public int getLoseType() {
            return loseType;
        }

        public void setLoseType(int loseType) {
            this.loseType = loseType;
        }

        public int getPageBuycount() {
            return pageBuycount;
        }

        public void setPageBuycount(int pageBuycount) {
            this.pageBuycount = pageBuycount;
        }

        public int getPageViewcount() {
            return pageViewcount;
        }

        public void setPageViewcount(int pageViewcount) {
            this.pageViewcount = pageViewcount;
        }

        public long getRefundEndTime() {
            return refundEndTime;
        }

        public void setRefundEndTime(long refundEndTime) {
            this.refundEndTime = refundEndTime;
        }

        public String getSellType() {
            return sellType;
        }

        public void setSellType(String sellType) {
            this.sellType = sellType;
        }

        public long getSignEndTime() {
            return signEndTime;
        }

        public void setSignEndTime(long signEndTime) {
            this.signEndTime = signEndTime;
        }

        public int getSourcePrice() {
            return sourcePrice;
        }

        public void setSourcePrice(int sourcePrice) {
            this.sourcePrice = sourcePrice;
        }

        public SqlMapBean getSqlMap() {
            return sqlMap;
        }

        public void setSqlMap(SqlMapBean sqlMap) {
            this.sqlMap = sqlMap;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStudyPercent() {
            return studyPercent;
        }

        public void setStudyPercent(String studyPercent) {
            this.studyPercent = studyPercent;
        }

        public int getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(int subjectId) {
            this.subjectId = subjectId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public List<TeacherListBean> getTeacherList() {
            return teacherList;
        }

        public void setTeacherList(List<TeacherListBean> teacherList) {
            this.teacherList = teacherList;
        }

        public static class CourseStudyhistoryBean {
            /**
             * courseId : 379
             * courseName : 初中七年级上册-数学
             * databack : 2018-03-23 15:52:20,2018-03-23 15:52:07,
             * id : 3514
             * kpointId : 2108
             * kpointName :  科学计数法
             * playercount : 2
             * updateTime : 1521791541000
             * userId : 1775
             */

            private int courseId;
            private String courseName;
            private String databack;
            private int id;
            private int kpointId;
            private String kpointName;
            private int playercount;
            private long updateTime;
            private int userId;

            public int getCourseId() {
                return courseId;
            }

            public void setCourseId(int courseId) {
                this.courseId = courseId;
            }

            public String getCourseName() {
                return courseName;
            }

            public void setCourseName(String courseName) {
                this.courseName = courseName;
            }

            public String getDataback() {
                return databack;
            }

            public void setDataback(String databack) {
                this.databack = databack;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getKpointId() {
                return kpointId;
            }

            public void setKpointId(int kpointId) {
                this.kpointId = kpointId;
            }

            public String getKpointName() {
                return kpointName;
            }

            public void setKpointName(String kpointName) {
                this.kpointName = kpointName;
            }

            public int getPlayercount() {
                return playercount;
            }

            public void setPlayercount(int playercount) {
                this.playercount = playercount;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }
        }

        public static class LevelSubjectBean {
        }

        public static class SqlMapBean {
        }

        public static class TeacherListBean {
            /**
             * picPath :
             * id : 225
             * name : 常清
             * education : 数学讲师
             */

            private String picPath;
            private int id;
            private String name;
            private String education;

            public String getPicPath() {
                return picPath;
            }

            public void setPicPath(String picPath) {
                this.picPath = picPath;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }
        }
    }
}
