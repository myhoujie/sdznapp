package com.sdzn.pkt.student.phone.mvp.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.pkt.student.phone.bean.PayInfoBean;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/14
 */

public interface OrderSubmitView extends BaseView {

    void queryCartEmpty();

    void queryCartFailure(String msg);

    void submitOrderSuccess();

    void submitOrderFailure(String msg);

    void queryCartSuccess(ShoppingCartBean shoppingCartBeen);

    void getPayInfoSuccess(PayInfoBean payInfoBean);

    void getPayInfoFailure(String msg);
}
