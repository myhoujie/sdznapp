package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.bean.ShoppingCartBean;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：
 * - 购物车列表adpater
 * 创建人：baoshengxiang
 * 创建时间：2017/7/5
 */
public class ShoppingCartAdapter extends BaseRcvAdapter<ShoppingCartBean.ShopCartListBean> {
    private HashMap<String, Integer> checkedGoods;
    private CartCallback cartCallback;
    private double totalPrice;

    public ShoppingCartAdapter(Context context, List mList) {
        super(context, R.layout.item_shopping_cart, mList);
        checkedGoods = new HashMap<>();
    }

    @Override
    public void convert(BaseViewHolder holder, final int position, final ShoppingCartBean.ShopCartListBean shoppingCartBean) {
//        final ShoppingCartBean.ShopCartListBean.CourseBean courseBean = shoppingCartBean.getCourse();
//        holder.setVisible(R.id.cb_checked, isEdit);
        holder.setVisible(R.id.cb_checked, true);
        CheckBox cbChecked = holder.getView(R.id.cb_checked);
        cbChecked.setOnCheckedChangeListener(null);
        cbChecked.setChecked(checkedGoods.containsKey(String.valueOf(shoppingCartBean.getId())));
        cbChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkedGoods.put(String.valueOf(shoppingCartBean.getId()), position);
//                    totalPrice += courseBean.getCurrentPrice();
                    totalPrice = add(totalPrice, shoppingCartBean.getCurrentPrice());
                } else {
                    checkedGoods.remove(String.valueOf(shoppingCartBean.getId()));
//                    totalPrice -= courseBean.getCurrentPrice();
                    totalPrice = sub(totalPrice, shoppingCartBean.getCurrentPrice());
                }
                cartCallback.isSelectAll(judgeSelectAll());
//                    cartCallback.backTotalPrice(totalPrice);
                setTotalPrice(totalPrice);

            }
        });

        holder.setImageView(R.id.iv_cover, "" + shoppingCartBean.getLogo());
        holder.setText(R.id.tv_recmd_title, shoppingCartBean.getCourseName());

        holder.setText(R.id.tv_recmd_price, "￥" + shoppingCartBean.getCurrentPrice());

        if ("PACKAGE".equals(shoppingCartBean.getSellType())) {
            holder.setText(R.id.tv_type, "组合");
            if (shoppingCartBean.getPackageType() == 1) {
                holder.setText(R.id.tv_course_type, "「直播」");
            } else {
                holder.setText(R.id.tv_course_type, "「点播」");
            }
        } else {
            holder.setText(R.id.tv_type, "单科");
            if ("COURSE".equals(shoppingCartBean.getSellType())) {
                holder.setText(R.id.tv_course_type, "「点播」");
            } else if ("LIVE".equals(shoppingCartBean.getSellType())) {
                holder.setText(R.id.tv_course_type, "「直播」");
            }

        }


    }

    /**
     * 判断是否全选
     *
     * @return
     */
    private boolean judgeSelectAll() {
        return checkedGoods.size() == mList.size();

    }

    public void setSelectAll(boolean isSelectAll) {
        checkedGoods.clear();
        totalPrice = 0.00f;
        if (isSelectAll) {
            for (int i = 0; i < mList.size(); i++) {
                ShoppingCartBean.ShopCartListBean shopCartListBean = mList.get(i);
                checkedGoods.put(String.valueOf(shopCartListBean.getId()), i);
//                totalPrice += shopCartListBean.getCourse().getCurrentPrice();
                totalPrice = add(totalPrice, shopCartListBean.getCurrentPrice());
            }
        }
        setTotalPrice(totalPrice);
        notifyDataSetChanged();
    }

    public Map<String, Integer> getCheckedGoods() {
        return checkedGoods;
    }

    public void removeGoods() {
        checkedGoods.clear();
        notifyDataSetChanged();
    }

    public void setCartCallback(CartCallback cartCallback) {
        this.cartCallback = cartCallback;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public interface CartCallback {
        void isSelectAll(boolean isSelectAll);

        void backTotalPrice(double price);
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        cartCallback.backTotalPrice(totalPrice);
    }

    /**
     * 提供精确的加法运算。
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */

    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */

    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }
}
