package com.sdzn.pkt.student.phone.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.sdzn.pkt.student.phone.fragment.IndentFragment;

import java.util.List;

/**
 * 描述：
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/4
 */

public class MineIndentFragmentPagerAdapter extends FragmentPagerAdapter {
    private String[] titles = new String[]{"全部", "待付款", "已完成", "已取消", "已退款"};
    private Context context;
    private List<IndentFragment> mData;

    public MineIndentFragmentPagerAdapter(FragmentManager fm, List<IndentFragment> list, Context context) {
        super(fm);
        this.context = context;
        this.mData = list;
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
