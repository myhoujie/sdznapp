package com.sdzn.pkt.student.phone.activity;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.sdzn.core.base.BaseActivity;
import com.sdzn.pkt.student.phone.R;
import com.sdzn.pkt.student.phone.adapter.IndentTitleAdapter;
import com.sdzn.pkt.student.phone.fragment.IndentFragment;
import com.sdzn.pkt.student.phone.widget.NoScrollViewPager;
import com.sdzn.pkt.student.phone.widget.TitleBar;
import com.sdzn.pkt.student.phone.widget.pager.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 描述：我的订单列表
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class IndentActivity extends BaseActivity {
    @BindView(R.id.tabs)
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    //    @BindView(R.id.tabl_indent)
//    TabLayout tablIndent;
    @BindView(R.id.vp_indent)
    NoScrollViewPager vpIndent;
    //    MineIndentFragmentPagerAdapter fragmentAdapter;
    private IndentTitleAdapter indentTitleAdapter;
    private List<IndentFragment> list_fragment;//定义要装fragment的列表
    private final String TAG_SUCCESS = "SUCCESS";//已完成
    private final String TAG_INIT = "INIT";//未完成-- 待付款
    private final String TAG_CANCEL = "CANCEL";//已取消
    private final String TAG_REFUND = "REFUND";//已退款
    private final String TAG_ALL = "ALL";//空时为全部数据

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_indent;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initView();
        initData();

    }

    private void initData() {

    }

    private void initView() {
        list_fragment = new ArrayList<>();
        list_fragment.add(IndentFragment.newInstance(TAG_ALL));
        list_fragment.add(IndentFragment.newInstance(TAG_INIT));
        list_fragment.add(IndentFragment.newInstance(TAG_SUCCESS));
        list_fragment.add(IndentFragment.newInstance(TAG_CANCEL));
        list_fragment.add(IndentFragment.newInstance(TAG_REFUND));
        indentTitleAdapter = new IndentTitleAdapter(getSupportFragmentManager());
        indentTitleAdapter.setmDatas(list_fragment);
        vpIndent.setAdapter(indentTitleAdapter);
        vpIndent.setNoScroll(false);
        vpIndent.setOffscreenPageLimit(4);
        mPagerSlidingTabStrip.setViewPager(vpIndent);
//        fragmentAdapter = new MineIndentFragmentPagerAdapter(getSupportFragmentManager(), list_fragment, mContext);
//        vpIndent.setAdapter(fragmentAdapter);
//        vpIndent.setNoScroll(false);
//        vpIndent.setOffscreenPageLimit(4);
//        tablIndent.setupWithViewPager(vpIndent);
        list_fragment.get(0).regEventBus();
        vpIndent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < list_fragment.size(); i++) {
                    if (i == position) {
                        list_fragment.get(i).regEventBus();
                        continue;
                    }
                    list_fragment.get(i).unRegEventBus();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
