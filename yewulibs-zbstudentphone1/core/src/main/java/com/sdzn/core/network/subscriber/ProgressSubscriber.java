package com.sdzn.core.network.subscriber;

import android.content.Context;

import com.sdzn.core.network.listener.ProgressCancelListener;
import com.sdzn.core.network.utils.ProgressDialogHandler;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.network.exception.CodeException;
import com.sdzn.core.network.exception.HttpTimeException;

import rx.Subscriber;

public class ProgressSubscriber<T> extends Subscriber<T> implements ProgressCancelListener {

    protected SubscriberOnNextListener mSubscriberOnNextListener;
    protected ProgressDialogHandler mProgressDialogHandler;

    protected Context context;
    protected boolean isShowProgress = true;//默认显示progressdialog

    public ProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
        this.context = context;
        mProgressDialogHandler = new ProgressDialogHandler(context, this, true);
    }

    public ProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
        this.context = context;
        this.isShowProgress = isShowProgress;
        mProgressDialogHandler = new ProgressDialogHandler(context, this, true);
    }

    public ProgressSubscriber(SubscriberOnNextListener mSubscriberOnNextListener, Context context, boolean isShowProgress, String showMsg) {
        this.mSubscriberOnNextListener = mSubscriberOnNextListener;
        this.context = context;
        this.isShowProgress = isShowProgress;
        mProgressDialogHandler = new ProgressDialogHandler(context, this, showMsg, true);
    }

    public ProgressSubscriber(SubscriberOnNextListener<T> subscriberOnNextListener) {

    }


    private void showProgressDialog() {
        if (mProgressDialogHandler != null) {
            mProgressDialogHandler.obtainMessage(ProgressDialogHandler.SHOW_PROGRESS_DIALOG).sendToTarget();
        }
    }

    protected void dismissProgressDialog() {
        if (mProgressDialogHandler != null) {
            mProgressDialogHandler.obtainMessage(ProgressDialogHandler.DISMISS_PROGRESS_DIALOG).sendToTarget();
            mProgressDialogHandler = null;
        }
    }

    @Override
    public void onStart() {
        if (isShowProgress) {
            showProgressDialog();
        }
    }

    @Override
    public void onCompleted() {
        if (isShowProgress) {
            dismissProgressDialog();
        }
    }

    @Override
    public void onError(Throwable e) {
        if (isShowProgress) {
            dismissProgressDialog();
        }
        if (e instanceof ApiException) {
            mSubscriberOnNextListener.onFail(e);
        } else if (e instanceof HttpTimeException) {
            HttpTimeException exception = (HttpTimeException) e;
            mSubscriberOnNextListener.onFail(new ApiException(exception, CodeException.RUNTIME_ERROR));
        } else {
            mSubscriberOnNextListener.onFail(new ApiException(e, CodeException.UNKNOWN_ERROR));
        }

    }

    @Override
    public void onNext(T t) {
        mSubscriberOnNextListener.onNext(t);
    }

    @Override
    public void onCancelProgress() {
        if (!this.isUnsubscribed()) {
            this.unsubscribe();
        }
    }
}