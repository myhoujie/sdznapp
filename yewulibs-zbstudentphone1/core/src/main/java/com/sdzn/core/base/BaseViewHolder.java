package com.sdzn.core.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.util.Linkify;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sdzn.core.R;
import com.sdzn.core.utils.glide.GlideImgManager;


/**
 * 基类viewHolder
 * Created by zxp19920626 on 2016/4/27.
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> views;
    private Context context;
    private Resources resources;
    private View convertView;
    private int mLayoutId;

    public BaseViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        this.convertView = itemView;
        resources = context.getResources();
        this.views = new SparseArray<View>();
        convertView.setTag(this);
    }

    public static BaseViewHolder get(Context context, View convertView, ViewGroup parent, int layoutId) {
        BaseViewHolder holder;
        if (convertView == null) {
            View itemView = LayoutInflater.from(context).inflate(layoutId, parent, false);
            holder = new BaseViewHolder(context, itemView);
            holder.mLayoutId = layoutId;
        } else {
            holder = (BaseViewHolder) convertView.getTag();
        }
        return holder;
    }


    /**
     * 得到相信的view
     *
     * @param viewId view的id
     * @param <T>    view本身
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = convertView.findViewById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }


    /**
     * 设置TextView内容
     *
     * @param viewId
     * @param value
     * @return
     */
    public BaseViewHolder setText(int viewId, CharSequence value) {
        TextView view = getView(viewId);
        if (value == null) {
            return this;
        }
        view.setText(value);
        return this;
    }

    public BaseViewHolder setText(int viewId, int value) {
        TextView view = getView(viewId);
        view.setText(resources.getString(value));
        return this;
    }

    public BaseViewHolder setTextColor(int viewId, int textColor) {
        TextView view = getView(viewId);
        view.setTextColor(textColor);
        return this;
    }

    public BaseViewHolder setTextColorRes(int viewId, int textColorRes) {
        TextView view = getView(viewId);
        view.setTextColor(context.getResources().getColor(textColorRes));
        return this;
    }

    /**
     * 以sp为单位设置字体大小
     */
    public BaseViewHolder setTextSize(int viewId, float textSize) {
        TextView view = getView(viewId);
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        return this;
    }

    /**
     * 设置SimpleDraweeView的图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public BaseViewHolder setImageView(int viewId, @NonNull String url) {
        ImageView view = getView(viewId);
        if (!url.equals(view.getTag())) {
            GlideImgManager.loadImage(context, url, view, 405, 300);
            view.setTag(R.id.image_tag, url);
        }
        return this;
    }

    /**
     * 头像的设置图片
     *
     * @param viewId
     * @param url
     * @param isAvatar
     * @return
     */
    public BaseViewHolder setImageView(int viewId, @NonNull String url, boolean isAvatar) {
        ImageView view = getView(viewId);
        if (!url.equals(view.getTag())) {
            GlideImgManager.loadAvatarImage(context, url, view);
            view.setTag(R.id.image_tag, url);
        }
        return this;
    }

    public BaseViewHolder setImageView(int viewId, @NonNull String url, int placeholderId) {
        ImageView view = getView(viewId);
        if (!url.equals(view.getTag())) {
            GlideImgManager.loadImage(context, url, placeholderId, placeholderId, view);
            view.setTag(R.id.image_tag, url);
        }
        return this;
    }

    public BaseViewHolder setImageView(int viewId, int soruceID) {
        ImageView view = getView(viewId);
        view.setImageResource(soruceID);
        return this;
    }

    public BaseViewHolder setImageDrawable(int viewId, Drawable drawable) {
        ImageView view = getView(viewId);
        view.setImageDrawable(drawable);
        return this;
    }


    /**
     * 设置开/关两种状态的按钮
     *
     * @param viewId
     * @param checked
     * @return
     */
    public BaseViewHolder setCheck(int viewId, boolean checked) {
        View view = getView(viewId);
        // View unable cast to Checkable
        if (view instanceof CompoundButton) {
            ((CompoundButton) view).setChecked(checked);
        } else if (view instanceof CheckedTextView) {
            ((CheckedTextView) view).setChecked(checked);
        }
        return this;
    }

    public BaseViewHolder setTag(int viewId, Object tag) {
        View view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    public BaseViewHolder setTag(int viewId, int key, Object tag) {
        View view = getView(viewId);
        view.setTag(key, tag);
        return this;
    }


    public BaseViewHolder setBackgroundColor(int viewId, int color) {
        View view = getView(viewId);
        view.setBackgroundColor(color);
        return this;
    }

    public BaseViewHolder setBackgroundRes(int viewId, int backgroundRes) {
        View view = getView(viewId);
        view.setBackgroundResource(backgroundRes);
        return this;
    }

    @SuppressLint("NewApi")
    public BaseViewHolder setAlpha(int viewId, float value) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getView(viewId).setAlpha(value);
        } else {
            // Pre-honeycomb hack to set Alpha value
            AlphaAnimation alpha = new AlphaAnimation(value, value);
            alpha.setDuration(0);
            alpha.setFillAfter(true);
            getView(viewId).startAnimation(alpha);
        }
        return this;
    }

    public BaseViewHolder setVisible(int viewId, boolean visible) {
        View view = getView(viewId);
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
        return this;
    }

    public BaseViewHolder setInVisible(int viewId, boolean visible) {
        View view = getView(viewId);
        view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        return this;
    }

    public BaseViewHolder linkify(int viewId) {
        TextView view = getView(viewId);
        Linkify.addLinks(view, Linkify.ALL);
        return this;
    }

    public BaseViewHolder setTypeface(Typeface typeface, int... viewIds) {
        for (int viewId : viewIds) {
            TextView view = getView(viewId);
            view.setTypeface(typeface);
            view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }
        return this;
    }

    public BaseViewHolder setProgress(int viewId, int progress) {
        ProgressBar view = getView(viewId);
        view.setProgress(progress);
        return this;
    }

    public BaseViewHolder setProgress(int viewId, int progress, int max) {
        ProgressBar view = getView(viewId);
        view.setMax(max);
        view.setProgress(progress);
        return this;
    }

    public BaseViewHolder setMax(int viewId, int max) {
        ProgressBar view = getView(viewId);
        view.setMax(max);
        return this;
    }

    public BaseViewHolder setRating(int viewId, float rating) {
        RatingBar view = getView(viewId);
        view.setRating(rating);
        return this;
    }

    public BaseViewHolder setRating(int viewId, float rating, int max) {
        RatingBar view = getView(viewId);
        view.setMax(max);
        view.setRating(rating);
        return this;
    }

    /**
     * 设置adapter(ListView/GridView都继承AdapterView)
     *
     * @param viewId
     * @param adapter
     * @return
     */
    public BaseViewHolder setAdapter(int viewId, Adapter adapter) {
        AdapterView view = getView(viewId);
        view.setAdapter(adapter);
        return this;
    }

    /**
     * 设置Rcv的adapter
     *
     * @param viewId
     * @param adapter
     * @return
     */
    public BaseViewHolder setRcvAdapter(int viewId, RecyclerView.Adapter adapter) {
        RecyclerView view = getView(viewId);
        view.setAdapter(adapter);
        return this;
    }


    public int getLayoutId() {
        return mLayoutId;
    }
}