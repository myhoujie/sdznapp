package com.sdzn.core.base;

import android.os.Bundle;

/**
 * 描述：
 * - mvp模式activity基类
 * 创建人：鲍圣祥
 * 创建时间：2016/12/6
 */
public abstract class BaseMVPActivity<V, P extends BasePresenter<V>> extends BaseActivity {
    protected P mPresenter; //presenter对象

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mPresenter = createPresenter();
        mPresenter.attachView((V) this, this);//view与presenter建立关联
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    protected abstract P createPresenter();


}
