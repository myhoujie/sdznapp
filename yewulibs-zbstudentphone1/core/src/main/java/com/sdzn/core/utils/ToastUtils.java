package com.sdzn.core.utils;

import android.content.Context;
import android.widget.Toast;

import com.sdzn.core.base.BaseApplication;


public class ToastUtils {
    private static boolean isShow = true;

    /**
     * 之前显示的内容
     */
    private static String oldMsg;
    /**
     * Toast对象
     */
    private static Toast toast = null;
    /**
     * 第一次时间
     */
    private static long oneTime = 0;
    /**
     * 第二次时间
     */
    private static long twoTime = 0;

    private ToastUtils() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }


    public static void showShort(Context context, CharSequence message) {
        if (isShow) {
//            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            showSingleToast(context, message.toString(), Toast.LENGTH_SHORT);
        }
    }

    public static void showShort(CharSequence message) {
        if (isShow) {
//            Toast.makeText(BaseApplication.getInstance(), message, Toast.LENGTH_SHORT).show();
            showSingleToast(BaseApplication.getInstance(), message.toString(), Toast.LENGTH_SHORT);
        }
    }


    public static void showShort(Context context, int message) {
        if (isShow) {
//            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            showSingleToast(context, context.getResources().getString(message), Toast.LENGTH_SHORT);
        }
    }


    public static void showLong(Context context, CharSequence message) {
        if (isShow) {
//            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            showSingleToast(context, message.toString(), Toast.LENGTH_LONG);
        }
    }


    public static void showLong(Context context, int message) {
        if (isShow) {
//            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            showSingleToast(context, context.getResources().getString(message), Toast.LENGTH_LONG);
        }
    }


    public static void show(Context context, CharSequence message, int duration) {
        if (isShow) {
//            Toast.makeText(context, message, duration).show();
            showSingleToast(context, message.toString(), duration);
        }
    }


    public static void show(Context context, int message, int duration) {
        if (isShow) {
//            Toast.makeText(context, message, duration).show();
            showSingleToast(context, context.getResources().getString(message), duration);
        }
    }

    /**
     * 显示单例Toast（防止重复显示）
     *
     * @param context contesx
     * @param message 提示内容
     */
    public static void showSingleToast(Context context, String message, int duration) {
        if (toast == null) {
            toast = Toast.makeText(context, message, duration);
            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (message.equals(oldMsg)) {
                if (twoTime - oneTime > duration) {
                    toast.show();
                }
            } else {
                oldMsg = message;
                toast.setText(message);
                toast.show();
            }
        }
        oneTime = twoTime;
    }

    public static void setIsShow(boolean isShow) {
        ToastUtils.isShow = isShow;
    }
}

