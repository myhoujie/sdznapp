package com.sdzn.core.network.listener;

public interface ProgressCancelListener {
    void onCancelProgress();
}