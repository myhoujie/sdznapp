package com.sdzn.core.network.exception;

/**
 * 回调统一请求异常
 * Created by WZG on 2016/12/12.
 */

public class ApiException extends RuntimeException {
    /*错误码*/
    private int code;
    /*显示的信息*/
    private String message;

    public ApiException(Throwable cause, int code) {
        super(cause);
        this.code = code;
        this.message = cause.getMessage();
    }

    public ApiException(Throwable cause) {
        super(cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}