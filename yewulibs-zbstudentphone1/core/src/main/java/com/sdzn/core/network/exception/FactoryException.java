package com.sdzn.core.network.exception;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * 描述：
 * -异常处理工厂
 * 主要是解析异常，输出自定义ApiException
 * 创建人：baoshengxiang
 * 创建时间：2017/6/6
 */

public class FactoryException {
//    private static final String HTTP_EXCEPTION_MSG = "网络错误";
//    private static final String CONNECT_EXCEPTION_MSG = "连接失败,请检查网络";
//    private static final String JSON_EXCEPTION_MSG = "数据解析失败";
//    private static final String UNKNOWN_HOST_EXCEPTION_MSG = "无法解析该域名";
    private static final String HTTP_EXCEPTION_MSG = "连接失败,请检查网络";
    private static final String CONNECT_EXCEPTION_MSG = "连接失败,请检查网络";
    private static final String JSON_EXCEPTION_MSG = "连接失败,请检查网络";
    private static final String UNKNOWN_HOST_EXCEPTION_MSG = "连接失败,请检查网络";

    private FactoryException() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 解析异常
     *
     * @param e
     * @return
     */
    public static ApiException analysisExcetpion(Throwable e) {
        ApiException apiException = new ApiException(e);

        if (e instanceof HttpException) {
             /*网络异常*/
            apiException.setCode(CodeException.HTTP_ERROR);
            apiException.setMessage(HTTP_EXCEPTION_MSG);
        } else if (e instanceof HttpTimeException) {
             /*自定义运行时异常*/
            HttpTimeException exception = (HttpTimeException) e;
            apiException.setCode(CodeException.RUNTIME_ERROR);
//            apiException.setMessage(exception.getMessage());
            apiException.setMessage("连接失败,请检查网络");
        } else if (e instanceof ConnectException || e instanceof SocketTimeoutException) {
             /*链接异常*/
            apiException.setCode(CodeException.HTTP_ERROR);
            apiException.setMessage(CONNECT_EXCEPTION_MSG);
        } else if (e instanceof JSONException || e instanceof ParseException) {
            apiException.setCode(CodeException.JSON_ERROR);
            apiException.setMessage(JSON_EXCEPTION_MSG);
        } else if (e instanceof UnknownHostException) {
            /*无法解析该域名异常*/
            apiException.setCode(CodeException.UNKOWNHOST_ERROR);
            apiException.setMessage(UNKNOWN_HOST_EXCEPTION_MSG);
        } else {
            /*未知异常*/
            apiException.setCode(CodeException.UNKNOWN_ERROR);
            apiException.setMessage(e.getMessage());
        }
        return apiException;
    }
}