package com.sdzn.core.base;

import android.app.Application;



/**
 * 描述：
 * -
 * 创建人：鲍圣祥
 * 创建时间：2016/12/5
 */
public class BaseApplication extends Application {
    private static BaseApplication baseApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        setBaseApplication(this);

    }


    public static BaseApplication getInstance() {
        return baseApplication;
    }

    public static void setBaseApplication(BaseApplication baseApplication) {
        BaseApplication.baseApplication = baseApplication;
    }

}
