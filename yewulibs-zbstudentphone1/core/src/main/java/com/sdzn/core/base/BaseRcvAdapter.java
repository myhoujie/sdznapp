package com.sdzn.core.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * 基类的RecyclerView的适配器
 * Created by zxp19920626 on 2016/4/27.
 */
public abstract class BaseRcvAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {

    protected OnItemClickListener onItemClickListener;          //单击
    protected OnItemLongClickListener onLongClickListener;     //长按
    protected boolean isEdit = false;
    protected int mLayoutId;                          //是否进入编辑模式

    protected Context context;
    protected Fragment fragment;
    protected LayoutInflater mLayoutInflater;
    protected List<T> mList;
    protected String type;

    public BaseRcvAdapter(Context context, List<T> mList) {
        this.mList = mList;
        this.context = context;
        this.mLayoutInflater = LayoutInflater.from(context);
    }


    public BaseRcvAdapter(Context context, int layoutId, List<T> mList) {
        this.mList = mList;
        this.context = context;
        this.mLayoutId = layoutId;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    public BaseRcvAdapter(Fragment fragment, int layoutId, List<T> mList) {
        this.fragment = fragment;
        this.mList = mList;
        this.mLayoutId = layoutId;
        this.context = fragment.getActivity();
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    public BaseRcvAdapter(Context context, int layoutId, List<T> mList, String type) {
        this.mList = mList;
        this.context = context;
        this.mLayoutId = layoutId;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.type = type;
    }


    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = BaseViewHolder.get(context, null, parent, mLayoutId);
        setListener(parent, holder, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        convert(holder, position, mList.get(position));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            updateItem(holder, position, payloads);
        }
    }

    protected void updateItem(BaseViewHolder holder, int position, List<Object> payloads) {
    }

    public abstract void convert(BaseViewHolder holder, int position, T t);


    protected void setListener(final ViewGroup parent, final BaseViewHolder holder, int viewType) {
        if (!isEnabled(viewType)) return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    int position = getPosition(holder);
                    onItemClickListener.onItemClick(v, position);
                }
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (onLongClickListener != null) {
                    int position = getPosition(holder);
                    onLongClickListener.onItemLongClick(v, position);
                    return true;
                }
                return false;
            }
        });
    }


    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
        notifyDataSetChanged();
    }

    public boolean getEdit() {
        return isEdit;
    }


    public void remove(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public void add(Integer position, T item) {
        mList.add(position, item);
        notifyItemInserted(position);
    }

    public void add(T item) {
        mList.add(item);
        notifyDataSetChanged();
    }

    protected boolean isEnabled(int viewType) {
        return true;
    }


    protected int getPosition(BaseViewHolder viewHolder) {
        return viewHolder.getAdapterPosition();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position);
    }

}
