package com.example.app2grzx.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.app2grzx.R;
import com.sdzn.core.base.BaseFragment;
import com.sdzn.core.utils.ToastUtils;


/**
 * 关于
 *
 * @author Reisen at 2017-12-08
 */

public class SettingAboutFragment extends BaseFragment implements View.OnClickListener {
    private static final String about = "拼课堂直播平台基于课堂学习内容，开展课外在线辅导课程。直播平台结合未来课堂辅助线，根据学生课上学习情况及课程体系，系统开展课外辅导，有针对的补充课上学习弱项。"
            + "\n\n"
            + "拼课堂直播平台与未来课堂辅助线共享数据，形成完整的教育生态链，完成教学过程信息化闭环，回归教育本质。在此基础上，应用大数据分析系统，评价教育全过程，提供多角色决策支持。应用教育自适应技术，纠正教学方法与策略，为学生推荐最佳学习路径，实现“互联网+个性化教育”，真正实现因材施教。";

    TextView mTextView;
    TextView tvQQ;
    TextView tvPhone;

    public static SettingAboutFragment newInstance() {
        return new SettingAboutFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_about;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mTextView = rootView.findViewById(R.id.tv_about);
        tvQQ = rootView.findViewById(R.id.tv_qq);
        tvPhone = rootView.findViewById(R.id.tv_phone);
        tvQQ.setOnClickListener(this);
        tvPhone.setOnClickListener(this);
        mTextView.setText(about);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_qq) {
            startQQ();
        } else if (id == R.id.tv_phone) {
            callPhone();
        }
    }

    private void startQQ() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mqqwpa://im/chat?chat_type=wpa&uin=2495065899")));
        } catch (Exception e) {
            ToastUtils.showShort("请检查是否安装QQ");
            e.printStackTrace();
        }
    }

    private void callPhone() {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:053189922971"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            ToastUtils.showShort("无法拨号");
            e.printStackTrace();
        }
    }


}
