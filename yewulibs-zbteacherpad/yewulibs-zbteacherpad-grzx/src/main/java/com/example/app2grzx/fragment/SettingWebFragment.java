package com.example.app2grzx.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;

import com.example.app2grzx.R;
import com.example.app2publics.widget.TitleBar;
import com.sdzn.core.base.BaseFragment;


/**
 *
 */
public class SettingWebFragment extends BaseFragment {
    WebView mWebView;
    TitleBar titleBar;
    private String nameType = "";
    private String url = "";

    public SettingWebFragment() {
        // Required empty public constructor
    }

    public static SettingWebFragment newInstance(String type) {
        SettingWebFragment fragment = new SettingWebFragment();
        fragment.nameType = type;
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_web;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mWebView = rootView.findViewById(R.id.web);
        titleBar = rootView.findViewById(R.id.title_bar);
        if (!TextUtils.isEmpty(nameType)) {
            switch (nameType) {
                case "1":
                    url = "https://m.znclass.com/Useragreement1.html";
                    nameType = "用户协议";
                    break;
                case "2":
                    url = "https://m.znclass.com/agreement1.html";
                    nameType = "隐私协议";
                    break;
                default:
                    nameType = "暂无";
                    break;
            }
            titleBar.setTitleText(nameType);
            mWebView.loadUrl(url);
        }
    }


}
