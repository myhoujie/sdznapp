package com.example.app2grzx.fragment;

import android.os.Bundle;
import android.widget.TextView;

import com.example.app2grzx.R;
import com.example.app2publics.utils.App2Utils;
import com.sdzn.core.base.BaseFragment;


/**
 *
 */

public class SettingVersionFragment extends BaseFragment {
    TextView mVersion;


    public static SettingVersionFragment newInstance() {
        return new SettingVersionFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting_version;
    }

    @Override
    protected void onInit(Bundle savedInstanceState) {
        mVersion = rootView.findViewById(R.id.tv_version);
        mVersion.setText(App2Utils.getAppVersionName(mContext));
    }
}
