package com.example.app2jrzb.adapter;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;

import com.example.app2jrzb.R;
import com.example.app2publics.utils.CustomClicklistener;
import com.sdzn.core.base.BaseRcvAdapter;
import com.sdzn.core.base.BaseViewHolder;
import com.sdzn.zbteacher.variants.bean.LiveTodyBean;

import java.util.List;

/**
 * 描述：我的课程列表
 * -
 * 创建人：yuexingwu
 * 创建时间：2017/7/6
 */

public class LiveTodyAdapter extends BaseRcvAdapter<LiveTodyBean.RowsBean> {
    private final String NOT_STARTED = "3";
    private final String LIVE = "4";
    private final String FINISHI = "5";
    private final String REPLAY = "6";

    private boolean isLiving;
    LiveCourseTodayListener todayListener;

    public LiveTodyAdapter(Context context, List<LiveTodyBean.RowsBean> mList) {
        super(context, R.layout.item_tody_live, mList);
    }

    public void setListener(LiveCourseTodayListener listener) {
        this.todayListener = listener;
    }

    @Override
    public void convert(BaseViewHolder holder, int position, final LiveTodyBean.RowsBean rowsBean) {
        holder.setText(R.id.tv_title, rowsBean.getCourseTitle() + " - " + rowsBean.getKpointTitle());
        holder.setText(R.id.tv_begin, rowsBean.getLiveBeginTime()
                + " - " + rowsBean.getLiveEndTime());
        holder.setImageView(R.id.img_video, "" + rowsBean.getLogo());
//        android:background="@drawable/bg_course_shape_replay"

        if (rowsBean.getLiveStates().trim().equals(LIVE)) {
            holder.setText(R.id.tv_type, "开始直播");
            setImgviewtoText((TextView) holder.getView(R.id.tv_type), R.drawable.bg_course_shape_enter, "#ffffff");
        } else if (rowsBean.getLiveStates().trim().equals(REPLAY)) {
            holder.setText(R.id.tv_type, "查看回放");
            setImgviewtoText((TextView) holder.getView(R.id.tv_type), R.drawable.bg_course_shape_replay, "#ffffff");
        } else if (rowsBean.getLiveStates().trim().equals(NOT_STARTED)) {
            holder.setText(R.id.tv_type, "未开始");
            setImgviewtoText((TextView) holder.getView(R.id.tv_type), R.drawable.bg_course_shape_no, "#B1B9BF");
        } else if (rowsBean.getLiveStates().trim().equals(FINISHI)) {
            holder.setText(R.id.tv_type, "已结束");
            setImgviewtoText((TextView) holder.getView(R.id.tv_type), R.drawable.bg_course_shape_no, "#B1B9BF");
        }
        holder.itemView.setOnClickListener(new CustomClicklistener() {
            @Override
            protected void onSingleClick() {
                if (rowsBean.getLiveStates().trim().equals(LIVE)) {
                    todayListener.toLiving(String.valueOf(rowsBean.getKpointId()) + "");
                } else if (rowsBean.getLiveStates().trim().equals(REPLAY)) {
                    todayListener.toReplay(String.valueOf(rowsBean.getKpointId()) + "", rowsBean.getCourseId());
                }
            }

            @Override
            protected void onFastClick() {

            }
        });

    }

    private void setImgviewtoText(TextView textView, int imgId, String colorStr) {
        textView.setTextColor(Color.parseColor(colorStr));
        textView.setBackgroundResource(imgId);
    }

    public interface LiveCourseTodayListener {
        void toLiving(String kpointId);

        void toReplay(String kpointId, String courseId);
    }
}
