package com.example.app2jrzb.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.zbteacher.variants.bean.LiveTodyBean;
import com.sdzn.zbteacher.variants.bean.NewLiveInfo;
import com.sdzn.zbteacher.variants.bean.NewVideoInfo;

import java.util.List;

public interface LiveTodyView extends BaseView {
    void listCourseSuccess(List<LiveTodyBean.RowsBean> list);

    void listCourseEmpty();

    void listCourseError(String msg);

    void getLiveInfoSuccess(NewLiveInfo liveInfo);

    void getReplayInfoSuccess(NewVideoInfo videoInfo);

    void getInfoFailed(String msg);

}
