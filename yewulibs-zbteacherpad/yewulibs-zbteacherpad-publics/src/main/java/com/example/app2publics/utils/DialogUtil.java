package com.example.app2publics.utils;

import android.app.Activity;
import android.content.DialogInterface;

import com.example.app2publics.widget.CustomDialog;


/**
 * zs
 */

public class DialogUtil {
    public static void showExitDialog(Activity activity, final DialogInterface.OnClickListener listener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage("确定要退出登录吗？");
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
                dialog.dismiss();
            }
        });
        builder.setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * 清理缓存
     *
     * @param activity
     * @param msg
     * @param type
     */
    public static void showDialog(final Activity activity, String msg, boolean type, final DialogInterface.OnClickListener listener) {
//        if (!AndroidUtil.getActivityOnLive(activity)) {
//            return;
//        }
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setPositive("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onClick(dialog, which);
                dialog.dismiss();
            }
        }).setNegative("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final CustomDialog mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(type);
        mDialog.setCancelable(type);
        mDialog.show();
    }
}
