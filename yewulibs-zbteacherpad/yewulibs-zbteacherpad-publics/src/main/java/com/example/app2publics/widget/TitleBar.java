package com.example.app2publics.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;

import com.androidkun.xtablayout.XTabLayout;
import com.example.app2publics.R;
import com.sdzn.core.utils.ConvertUtils;
import com.sdzn.core.utils.KeyboardUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

//import android.support.annotation.Dimension;
//import android.support.annotation.DrawableRes;
//import android.support.annotation.IntDef;
//import android.support.v4.content.res.ResourcesCompat;
//import android.support.v4.view.ViewPager;


/**
 * 描述：
 * - 封装titlebar
 */
public class TitleBar extends RelativeLayout {
    private OnClickListener leftClickListener;
    private TitleViewHolder titleViewHolder;

    //    @IntDef({R.id.tv_left, R.id.iv_left, R.id.tv_title, R.id.tv_right, R.id.iv_right})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Views {
    }

    @IntDef({TitleType.NORMAL, TitleType.SEARCH, TitleType.TAB_TITLE})
    @Retention(RetentionPolicy.SOURCE)
    private @interface TitleType {
        /**
         * 普通标题栏(不显示TabLayout)
         */
        int NORMAL = 0;
        /**
         * 带搜索框的标题栏(不显示TabLayout)
         */
        int SEARCH = 1;

        /**
         * 带TabLayout的标题栏(不显示搜索栏和标题), 需要调用{@link #setTabViewPager(ViewPager)}方法设置关联的ViewPager
         */
        int TAB_TITLE = 2;
    }

    public TitleBar(Context context) {
        this(context, null);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(final Context context, AttributeSet attrs) {
        LayoutInflater from = LayoutInflater.from(context);
        View rootLayout = from.inflate(R.layout.titlebar, this);
        titleViewHolder = new TitleViewHolder(rootLayout);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleBar);

        //默认标题颜色
        int defaultTextColor = ResourcesCompat.getColor(getResources(), R.color.black, null);
        //左侧文本
        String leftText = typedArray.getString(R.styleable.TitleBar_leftText);
        //左侧文本颜色
        int leftTextColor = typedArray.getColor(R.styleable.TitleBar_leftTextColor, defaultTextColor);
        //标题类型(普通/搜索栏/Tablayout)
        int titleType = typedArray.getInt(R.styleable.TitleBar_titleType, TitleType.NORMAL);
        //Tablayout选中加粗
        boolean tabTextBold = typedArray.getBoolean(R.styleable.TitleBar_tabTextBold, false);
        //默认字号
        int deafultTextSize = ConvertUtils.dp2px(context, 20);
        //左侧文本
        float leftTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_leftTextSize, deafultTextSize));
        //左侧图片
        int leftImg = typedArray.getResourceId(R.styleable.TitleBar_leftImg, R.mipmap.ic_back);
        //标题文本(Tablayout状态下不显示)
        String titleText = typedArray.getString(R.styleable.TitleBar_titleText);
        //标题颜色
        int titleTextColor = typedArray.getColor(R.styleable.TitleBar_titleTextColor, defaultTextColor);
        //标题字号
        float titleTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_titleTextSize, deafultTextSize));
        //右侧文本
        String rightText = typedArray.getString(R.styleable.TitleBar_rightText);
        //右侧文本颜色
        int rightTextColor = typedArray.getColor(R.styleable.TitleBar_rightTextColor, defaultTextColor);
        //右侧字号
        float rightTextSize = ConvertUtils.px2dp(context, typedArray.getDimension(R.styleable.TitleBar_rightTextSize, deafultTextSize));
        //右侧图片
        int rightImg = typedArray.getResourceId(R.styleable.TitleBar_rightImg, 0);
        //是否显示两侧控件
        boolean showLeft = typedArray.getBoolean(R.styleable.TitleBar_showLeftView, true);
        boolean showRight = typedArray.getBoolean(R.styleable.TitleBar_showRightView, true);
        typedArray.recycle();
        if (!TextUtils.isEmpty(leftText)) {
            titleViewHolder.tvLeft.setText(leftText);
            titleViewHolder.tvLeft.setTextColor(leftTextColor);
            titleViewHolder.tvLeft.setTextSize(leftTextSize);
        }
        if (!TextUtils.isEmpty(titleText)) {
            titleViewHolder.tvTitle.setText(titleText);
            titleViewHolder.tvTitle.setTextColor(titleTextColor);
            titleViewHolder.tvTitle.setTextSize(titleTextSize);
        }
        if (!TextUtils.isEmpty(rightText)) {
            titleViewHolder.tvRight.setText(rightText);
            titleViewHolder.tvRight.setTextColor(rightTextColor);
            titleViewHolder.tvRight.setTextSize(rightTextSize);
        }

        if (showLeft) {
            titleViewHolder.ivLeft.setVisibility(VISIBLE);
            titleViewHolder.tvLeft.setVisibility(VISIBLE);
            if (leftClickListener == null) {
                leftClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        KeyboardUtils.hideSoftInput((Activity) context);
                        ((Activity) context).onBackPressed();
                    }
                };
            }
        } else {
            titleViewHolder.ivLeft.setVisibility(GONE);
            titleViewHolder.tvLeft.setVisibility(GONE);
        }

        titleViewHolder.ivRight.setVisibility(showRight ? VISIBLE : GONE);
        titleViewHolder.tvRight.setVisibility(showRight ? VISIBLE : GONE);

        if (leftImg != 0) {
            titleViewHolder.ivLeft.setImageResource(leftImg);
        }

        if (rightImg != 0) {
            titleViewHolder.ivRight.setImageResource(rightImg);
        }
        switch (titleType) {
            case TitleType.SEARCH:
                titleViewHolder.llCenter.setVisibility(VISIBLE);
                titleViewHolder.ivSearch.setVisibility(VISIBLE);
                titleViewHolder.tabLayout.setVisibility(GONE);
                titleViewHolder.llCenter.setBackgroundResource(R.drawable.bg_title_search);
                break;
            case TitleType.TAB_TITLE:
                titleViewHolder.llCenter.setVisibility(GONE);
                titleViewHolder.tabLayout.setVisibility(VISIBLE);
                break;
            default:
                titleViewHolder.llCenter.setVisibility(VISIBLE);
                titleViewHolder.ivSearch.setVisibility(GONE);
                titleViewHolder.tabLayout.setVisibility(GONE);
                titleViewHolder.llCenter.setBackgroundResource(0);
                break;
        }

        titleViewHolder.tvLeft.setOnClickListener(leftClickListener);
        titleViewHolder.ivLeft.setOnClickListener(leftClickListener);

        if (!tabTextBold) {
            return;
        }
        titleViewHolder.tabLayout.setOnTabSelectedListener(new XTabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(XTabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (view == null) {
                    tab.setCustomView(R.layout.layout_tab_text);
                }
                TextView textView = (TextView) tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTextColor(titleViewHolder.tabLayout.getTabTextColors());
                textView.setTypeface(Typeface.DEFAULT_BOLD);
            }

            @Override
            public void onTabUnselected(XTabLayout.Tab tab) {
                View view = tab.getCustomView();
                if (view == null) {
                    tab.setCustomView(R.layout.layout_tab_text);
                }
                TextView textView = (TextView) tab.getCustomView().findViewById(android.R.id.text1);
                textView.setTypeface(Typeface.DEFAULT);
            }

            @Override
            public void onTabReselected(XTabLayout.Tab tab) {
            }
        });

    }

    public TitleBar setTabViewPager(ViewPager viewPager) {
        titleViewHolder.tabLayout.setupWithViewPager(viewPager);
        return this;
    }


    public TitleBar setTabSelectedListener(XTabLayout.OnTabSelectedListener listener) {
        titleViewHolder.tabLayout.setOnTabSelectedListener(listener);
        return this;
    }

    public TitleBar setLeftText(String leftText) {
        titleViewHolder.tvLeft.setText(leftText);
        return this;
    }

    public TitleBar setLeftTextColor(int leftTextColor) {
        titleViewHolder.tvLeft.setTextColor(leftTextColor);
        return this;
    }

    public TitleBar setLeftTextSize(@Dimension float leftTextSize) {
        titleViewHolder.tvLeft.setTextSize(leftTextSize);
        return this;
    }

    public TitleBar setTitleText(String titleText) {
        titleViewHolder.tvTitle.setText(titleText);
        return this;
    }

    public TitleBar setTitleTextColor(int titleTextColor) {
        titleViewHolder.tvTitle.setTextColor(titleTextColor);
        return this;
    }

    public TitleBar setTitleTextSize(@Dimension float titleTextSize) {
        titleViewHolder.tvTitle.setTextSize(titleTextSize);
        return this;
    }

    public TitleBar setRightText(String rightText) {
        titleViewHolder.tvRight.setText(rightText);
        return this;
    }


    public TitleBar setRightTextColor(int rightTextColor) {
        titleViewHolder.tvRight.setTextColor(rightTextColor);
        return this;
    }

    public TitleBar setRightTextSize(@Dimension float rightTextSize) {
        titleViewHolder.tvRight.setTextSize(rightTextSize);
        return this;
    }

    public TitleBar setLeftImg(@DrawableRes int leftImg) {
        titleViewHolder.ivLeft.setImageResource(leftImg);
        return this;
    }

    public TitleBar setRightImg(@DrawableRes int rightImg) {
        titleViewHolder.ivRight.setImageResource(rightImg);
        return this;
    }


    public TitleBar setLeftClickListener(OnClickListener leftClickListener) {
        titleViewHolder.ivLeft.setOnClickListener(leftClickListener);
        titleViewHolder.tvLeft.setOnClickListener(leftClickListener);
        return this;
    }

    public TitleBar setRightClickListener(OnClickListener rightClickListener) {
        titleViewHolder.ivRight.setOnClickListener(rightClickListener);
        titleViewHolder.tvRight.setOnClickListener(rightClickListener);
        return this;
    }

    public TitleBar setTitleClickListener(OnClickListener titleClickListener) {
        titleViewHolder.llCenter.setOnClickListener(titleClickListener);
        return this;
    }

    public View getView(@Views int id) {
        if (R.id.iv_left == id) {
            return titleViewHolder.ivLeft;
        } else if (R.id.tv_left == id) {
            return titleViewHolder.tvLeft;
        } else if (R.id.tv_title == id) {
            return titleViewHolder.tvTitle;
        } else if (R.id.iv_right == id) {
            return titleViewHolder.ivRight;
        } else if (R.id.tv_right == id) {
            return titleViewHolder.tvRight;
        } else {
            return null;
        }
    }


    static class TitleViewHolder {
        ImageView ivLeft;
        TextView tvLeft;
        TextView tvTitle;
        TextView tvRight;
        ImageView ivRight;
        RelativeLayout llCenter;
        XTabLayout tabLayout;
        ImageView ivSearch;

        TitleViewHolder(View view) {
            ivLeft = (ImageView) view.findViewById(R.id.iv_left);
            tvLeft = (TextView) view.findViewById(R.id.tv_left);
            ivRight = (ImageView) view.findViewById(R.id.iv_right);
            llCenter = (RelativeLayout) view.findViewById(R.id.rl_center);
            ivSearch = (ImageView) view.findViewById(R.id.iv_search);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tabLayout = (XTabLayout) view.findViewById(R.id.tablayout);
            tvRight = (TextView) view.findViewById(R.id.tv_right);
        }
    }
}
