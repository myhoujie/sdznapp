package com.example.app2publics.widget.radioview;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.AnimRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.sdzn.core.utils.KeyboardUtils;
import com.sdzn.core.utils.LogUtils;

import java.util.List;

//import android.support.annotation.AnimRes;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;

public class FragmentTabUtils implements RadioLayout.OnCheckedChangeListener {
    private List<Fragment> fragments; // 一个tab页面对应一个Fragment
    private Fragment currentFragment;//
    private RadioLayout rgs; // 用于切换tab
    private FragmentManager fragmentManager; // Fragment所属的Activity
    private int fragmentContentId; // Activity中所要被替换的区域的id
    private int currentTab; // 当前Tab页面索引
    private Context context;
    private boolean isNeedAnimation = true;
    private boolean clearPopBackStack = false;
    private boolean addToBackStack = false;
    private int[] animation;
    private OnRgsExtraCheckedChangedListener onRgsExtraCheckedChangedListener;

    public FragmentTabUtils(Context context, FragmentManager fragmentManager,
                            List<Fragment> fragments, int fragmentContentId, RadioLayout rgs) {
        this(context, fragmentManager, fragments, fragmentContentId, rgs, 0,
                false, false);
    }

    /**
     * @param defaultPage       默认选中第几页
     * @param clearPopBackStack 切换Fragment前是否清空回退栈
     * @param addToBackStack    是否加入回退栈
     */
    public FragmentTabUtils(Context context, FragmentManager fragmentManager, List<Fragment> fragments,
                            int fragmentContentId, RadioLayout rgs, int defaultPage,
                            boolean clearPopBackStack, boolean addToBackStack) {
        this.context = context;
        this.fragments = fragments;
        this.rgs = rgs;
        this.fragmentManager = fragmentManager;
        this.fragmentContentId = fragmentContentId;
        this.clearPopBackStack = clearPopBackStack;
        this.addToBackStack = addToBackStack;
        this.currentTab = defaultPage;
        animation = new int[4];
        setAnimation(com.sdzn.core.R.anim.slide_in_from_right, com.sdzn.core.R.anim.slide_out_to_left,
                com.sdzn.core.R.anim.slide_in_from_left, com.sdzn.core.R.anim.slide_out_to_right);
        // 默认显示第一页
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment fragment = fragments.get(defaultPage);
        ft.add(fragmentContentId, fragment, fragment.getClass().getName());
        currentFragment = fragment;
        ft.commit();
        rgs.setOnCheckedChangeListener(this);
    }

    public void setAnimation(@AnimRes int enter, @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit) {
        animation[0] = enter;
        animation[1] = exit;
        animation[2] = popEnter;
        animation[3] = popExit;
    }

    @Override
    public void checkedChange(int resId) {
        int count = rgs.getRadioSelectCount();
        if (context instanceof Activity) {
            KeyboardUtils.hideSoftInput((Activity) context);
        }

        for (int i = 0; i < count; i++) {
            RadioSelector view = rgs.getRadioSelectorAt(i);
            if (view.getId() == resId && i < fragments.size()) {
                if (i == 4) {
                    if (onRgsExtraCheckedChangedListener != null) {
                        onRgsExtraCheckedChangedListener.onRgsExtraCheckedChanged(4);
                    }
                }
                Fragment fragment = fragments.get(i);
                if (fragment == null) {
                    LogUtils.e("Selected Fragment == null! position = " + i + ", size = " + fragments.size());
                    return;
                }
                if (currentTab == i) {
                    return;
                }
                if (clearPopBackStack) {
                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                FragmentTransaction ft = obtainFragmentTransaction(i);
                if (fragment.isAdded()) {
//                    fragment.onStart(); // 启动目标tab的fragment onStart()
                    ft.hide(currentFragment);
                    ft.show(fragment);
                } else {
                    ft.hide(currentFragment);
                    ft.add(fragmentContentId, fragment, fragment.getClass().getName());
                }
                currentFragment = fragment;
                currentTab = i;
                if (addToBackStack) {
                    ft.addToBackStack(fragment.getClass().getName());
                }
                ft.commit();
//                showTab(i); // 显示目标tab
                return;
            }
        }

    }


    //    /**
//     * 切换tab
//     *
//     * @param idx 位置索引
//     */
    private void showTab(int idx) {
        for (int i = 0; i < fragments.size(); i++) {
            Fragment fragment = fragments.get(i);
            FragmentTransaction ft = obtainFragmentTransaction(idx);
            if (idx == i) {
                ft.show(fragment);
            } else {
                ft.hide(fragment);
            }
            ft.commit();
        }
        currentTab = idx; // 更新目标tab为当前tab
    }

    /**
     * 获取一个带动画的FragmentTransaction
     *
     * @param index 位置索引
     */
    private FragmentTransaction obtainFragmentTransaction(int index) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (isNeedAnimation) {
            // 设置切换动画
            if (index > currentTab) {
                ft.setCustomAnimations(animation[0], animation[1]);
            } else {
                ft.setCustomAnimations(animation[2], animation[3]);
            }
        }
        return ft;
    }

    public int getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(int currentTab) {
        this.currentTab = currentTab;
    }

    public Fragment getCurrentFragment() {
        return fragments.get(currentTab);
    }

    public void setNeedAnimation(boolean needAnimation) {
        isNeedAnimation = needAnimation;
    }

    public void setOnRgsExtraCheckedChangedListener(OnRgsExtraCheckedChangedListener onRgsExtraCheckedChangedListener) {
        this.onRgsExtraCheckedChangedListener = onRgsExtraCheckedChangedListener;
    }

    /**
     * 切换tab额外功能功能接口
     */
    public interface OnRgsExtraCheckedChangedListener {
        void onRgsExtraCheckedChanged(int index);
    }

}