package com.example.app2welcome.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.example.app2welcome.IntentController;
import com.example.app2welcome.R;
import com.example.app2welcome.view.MainView;
import com.sdzn.core.base.BasePresenter;
import com.sdzn.core.network.func.ExceptionFunc;
import com.sdzn.core.network.listener.SubscriberOnNextListener;
import com.sdzn.core.network.utils.TransformUtils;
import com.sdzn.core.utils.AppManager;
import com.sdzn.zbteacher.variants.bean.AccountBean;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.bean.UserBean;
import com.sdzn.zbteacher.variants.manager.SPManager;
import com.sdzn.zbteacher.variants.network.RestApi;
import com.sdzn.zbteacher.variants.network.api.AccountService;
import com.sdzn.zbteacher.variants.network.api.ResponseFunc;
import com.sdzn.zbteacher.variants.network.subscriber.MProgressSubscriber;

import rx.Subscription;

public class MainPresenter extends BasePresenter<MainView> {

    public void autoLogin(final Context context) {
        Subscription subscribe = RestApi.getInstance()
                .create(AccountService.class)
                .login(SPManager.getLastLoginAccount(), SPManager.getPwd())
                .compose(TransformUtils.<ResultBean<AccountBean>>defaultSchedulers())
                .map(new ResponseFunc<AccountBean>())
                .onErrorResumeNext(new ExceptionFunc<AccountBean>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<AccountBean>() {
                    @Override
                    public void onNext(AccountBean accountBean) {
                        UserBean userBean = accountBean.getUserDetail();
                        if (userBean != null) {
                            userBean.setAccount(SPManager.getLastLoginAccount());
                            SPManager.saveUser(userBean);
                            SPManager.saveToken(accountBean.getAccess_token());
                            getView().autoLoginSuccess();
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
                        String msg = mActivity.getString(R.string.request_failure_try_again);
                        if (e != null) {
                            msg = TextUtils.isEmpty(e.getMessage()) ? msg : e.getMessage();
                        }
                        SPManager.changeLogin(context, false);
                        AppManager.getAppManager().appExit();
                        IntentController.toLogin(context);
                    }
                }, mActivity, true, "正在登录..."));
        addSubscribe(subscribe);

    }

/*
    public void checkVerion() {
        Subscription subscribe = RestApi.getInstance()
                .create(CourseService.class)
                .queryVersion(7)
                .compose(TransformUtils.<ResultBean<VersionInfo>>defaultSchedulers())
                .map(new ResponseFunc<VersionInfo>())
                .subscribe(new MProgressSubscriber<>(new SubscriberOnNextListener<VersionInfo>() {
                    @Override
                    public void onNext(VersionInfo versionInfoBean) {
                        int currVersion = App2Utils.getAppVersionCode(App2.get());
//                        VersionInfoBean versionInfo = versionInfoBean.getVersionInfo();
                        if (versionInfoBean.getVersionNumber() > currVersion) {//此处为
                            if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                                getView().updateVersion(versionInfoBean.getVersionInfo(), versionInfoBean.getTargetUrl());
                            }
                        }
                    }

                    @Override
                    public void onFail(Throwable e) {
//                        getView().getInfoFailure(e.getMessage()+"");
                    }
                }, mActivity, false));
        addSubscribe(subscribe);

    }
*/
}
