package com.example.app2welcome.adapter;

//import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

/**
 * 描述：
 * - viewpager 内容为view的adapter
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class PageAdapterWithView extends PagerAdapter {
    protected List<View> views;

    public PageAdapterWithView(List<View> views) {
        this.views = views;
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }
}
