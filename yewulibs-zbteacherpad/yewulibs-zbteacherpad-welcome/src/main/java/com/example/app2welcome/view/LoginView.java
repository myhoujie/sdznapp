package com.example.app2welcome.view;

import com.sdzn.core.base.BaseView;
import com.sdzn.zbteacher.variants.bean.AccountBean;

/**
 * 描述：
 */
public interface LoginView extends BaseView {

    void loginSuccess(AccountBean accountBean);

    void loginFailure(String msg);

//    void updateVersion(String updateInfo, String targetUrl);
}
