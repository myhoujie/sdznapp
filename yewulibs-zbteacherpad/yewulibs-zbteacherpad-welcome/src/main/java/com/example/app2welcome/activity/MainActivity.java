package com.example.app2welcome.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.azhon.appupdate.manager.DownloadManager;
import com.example.app2grzx.fragment.SystemSettingFragment;
import com.example.app2jrzb.fragment.LiveFragment;
import com.example.app2publics.utils.App2Utils;
import com.example.app2publics.widget.CircleImageView;
import com.example.app2publics.widget.radioview.FragmentTabUtils;
import com.example.app2publics.widget.radioview.RadioLayout;
import com.example.app2publics.widget.radioview.RadioView;
import com.example.app2welcome.R;
import com.example.app2welcome.presenter.CheckverionPresenter;
import com.example.app2welcome.presenter.MainPresenter;
import com.example.app2welcome.view.CheckverionView;
import com.example.app2welcome.view.MainView;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.core.utils.glide.GlideImgManager;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.zbteacher.variants.bean.UserBean;
import com.sdzn.zbteacher.variants.bean.VersionInfoBean;
import com.sdzn.zbteacher.variants.manager.SPManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;

//import android.support.v4.app.Fragment;


/**
 * adb shell am start -a android.intent.action.VIEW   -c android.intent.category.BROWSABLE  -d "app://cs.znclass.com/com.sdzn.live.tablet.com.sdzn.pkt.teacher.hd.hs.act.main?query3=aaaa&query2=45464&query1=pc"
 * zs
 * 主页
 */
public class MainActivity extends BaseMVPActivity<MainView, MainPresenter> implements CheckverionView, MainView {
    RadioLayout rgNavi;
    CircleImageView iv_head;
    TextView tv_name;

    RadioView mLive;
    RadioView mSetting;
    private FragmentTabUtils fragmentTabUtils;

    public static final String AUTO_LOGIN = "autoLogin";
    private UserBean userBean;
    private List<Fragment> fragments;

    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onInit(Bundle bundle) {
        if (getIntent().getBooleanExtra(AUTO_LOGIN, false)) {
            mPresenter.autoLogin(mContext);
        } else {
            initData();
            initView();
        }
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String aaaa = appLinkData.getQueryParameter("query1");
                    ToastUtils.showLong(this, "进入MainActivity成功" + "query1->" + aaaa);
                }
            }
        }
    }

    @Override
    protected void setStatusBar() {
        int color = getResources().getColor(R.color.colorPrimary);
//        StatusBarUtil.setColorForDrawerLayout(this, (LinearLayout) findViewById(R.id.layout), color, 0);
        StatusBarUtil.setColor(this, color, 0);
    }

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    private void initData() {
        userBean = SPManager.getUser();
        fragments = new ArrayList<>();
        fragments.add(LiveFragment.newInstance());
        fragments.add(SystemSettingFragment.newInstance());

        //检查更新
//        mPresenter.checkVerion();
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("1", "0");
    }

    private void initView() {
        rgNavi = findViewById(R.id.rg_navi_bottom);
        iv_head = findViewById(R.id.img_avatar);
        tv_name = findViewById(R.id.tv_name);
        mLive = findViewById(R.id.rb_live);
        mSetting = findViewById(R.id.rb_setting);
        if (!TextUtils.isEmpty(userBean.getName())) {
            tv_name.setText(userBean.getName());
        }
//        GlideImgManager.loadImageNoErrImg(mContext, "" + userBean.getAvatar(), iv_head);
//        GlideImgManager.loadAvatarImage(mContext, "" + userBean.getAvatar(), iv_head);
//        if (!TextUtils.isEmpty(userBean.getAvatar())) {
//            GlideImgManager.loadImage(mContext, "" + userBean.getAvatar(), iv_head);
//        }
        fragmentTabUtils = new FragmentTabUtils(mContext, getSupportFragmentManager(),
                fragments, R.id.main_container, rgNavi, 0, true, false);
        fragmentTabUtils.setNeedAnimation(true);
        fragmentTabUtils.setAnimation(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        mLive.setChecked(true);
    }

    @Override
    public void autoLoginSuccess() {
        initData();
        initView();
    }

    private ProgressDialog pd;
    private DownloadManager manager;
    private String updateTitle = "发现新版本";

   /* @Override
    public void updateVersion(String updateInfo, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/abteacherhd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂教师端");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
        // 需要更新，弹出更新窗口
       *//* UpdateDialog.Builder builder = new UpdateDialog.Builder(MainActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(MainActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(MainActivity.this);
                manager.setApkName("拼课堂老师端.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.live.tablet.com.sdzn.pkt.teacher.hd.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();*//*
    }*/

    @Override
    public void getInfoFailure(String msg) {
        ToastUtils.showShort(msg);

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            LogUtils.i("backStackCount = " + getSupportFragmentManager().getBackStackEntryCount());
            getSupportFragmentManager().popBackStack();
            return;
        }
        App2Utils.exitApp(mContext);
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = com.sdzn.core.utils.App2Utils.getAppVersionCode(App2.get());//获取版本号
            int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
            if (updateVersion > currVersion) {
                if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                    Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
                }
            }
        }
    }

    private void Updatemethod(String description, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/abteacherhd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂教师端");//app名称
        String count = description.replace("|", "\n");


        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    protected void onDestroy() {
        checkverionPresenter.onDestory();
        super.onDestroy();
    }
}
