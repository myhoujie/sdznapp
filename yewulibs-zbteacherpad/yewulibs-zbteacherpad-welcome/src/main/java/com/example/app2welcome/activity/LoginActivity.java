package com.example.app2welcome.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.azhon.appupdate.manager.DownloadManager;
import com.example.app2publics.utils.App2Utils;
import com.example.app2publics.widget.CheckBoxSample;
import com.example.app2publics.widget.PwdEditText;
import com.example.app2welcome.IntentController;
import com.example.app2welcome.R;
import com.example.app2welcome.presenter.CheckverionPresenter;
import com.example.app2welcome.presenter.LoginPresenter;
import com.example.app2welcome.view.CheckverionView;
import com.example.app2welcome.view.LoginView;
import com.sdzn.core.base.BaseMVPActivity;
import com.sdzn.core.utils.AndroidBug5497Workaround;
import com.sdzn.core.utils.ToastUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.zbteacher.variants.bean.AccountBean;
import com.sdzn.zbteacher.variants.bean.UserBean;
import com.sdzn.zbteacher.variants.bean.VersionInfoBean;
import com.sdzn.zbteacher.variants.manager.SPManager;

import org.jetbrains.annotations.NotNull;

import constant.UiType;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;

/**
 * adb shell am start -a android.intent.action.VIEW   -c android.intent.category.BROWSABLE  -d "app://cs.znclass.com/com.sdzn.live.tablet.com.sdzn.pkt.teacher.hd.hs.act.login?query3=aaaa&query2=45464&query1=pc"
 */
public class LoginActivity extends BaseMVPActivity<LoginView, LoginPresenter> implements CheckverionView, LoginView, View.OnClickListener {

    EditText etAccount;
    PwdEditText etPassword;
    Button btLogin;
    //软件盘弹起后所占高度阀值

    private int keyHeight = 0;
    CheckBoxSample checkBoxSample;
    private boolean isCheckBox;
    private TextView tvuseragreement, tvprivacyagreement;

    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void onInit(Bundle bundle) {
        initData();
        initView();
        SPManager.changeLogin(mContext, false);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
//                    String name = appLinkData.getQueryParameter("name");
//                    String pwd = appLinkData.getQueryParameter("pwd");
//                    if (!name.isEmpty() && !pwd.isEmpty()) {
//                        mPresenter.login(name, pwd);
//                    } else {
//                        com.blankj.utilcode.util.ToastUtils.showLong("账号密码错误!");
//                    }
                }
            }
        }
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected void setStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AndroidBug5497Workaround.assistActivity(this);
    }

    private void initView() {
        etAccount = findViewById(R.id.et_account);
        etPassword = findViewById(R.id.et_password);
        btLogin = findViewById(R.id.btn_login);
        checkBoxSample = findViewById(R.id.checkbox);
        tvuseragreement = findViewById(R.id.tv_user_agreement);
        tvprivacyagreement = findViewById(R.id.tv_privacy_agreement);

        tvprivacyagreement.setOnClickListener(this);
        tvuseragreement.setOnClickListener(this);
        btLogin.setOnClickListener(this);
        checkBoxSample.setOnClickListener(this);
        etPassword.setShowAnimate(false);

        if (!checkBoxSample.isChecked()) {
            checkBoxSample.toggle();
            isCheckBox = true;
        }
    }

    private void initData() {
        int screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
//        keyHeight = screenHeight / 3;
        //系统自带输入法, 密码输入框比文字输入框矮一点, 正好卡在差一点到1/3的位置(这个差的高度不同尺寸屏幕不一样)
        //华为部分机型开启全屏模式时, 显示虚拟键会将窗口顶起一点...祖传大黑边应该也不能超过1/4吧..
        keyHeight = screenHeight / 4;
//        String lastLoginAccount = SPManager.getLastLoginAccount();
//        if (!TextUtils.isEmpty(lastLoginAccount)) {
//            etAccount.setText(lastLoginAccount);
//        }
//        mPresenter.checkVerion();
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("1", "0");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    //    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.btn_login:
//                doLogin();
//                break;
//            default:
//                break;
//        }
//    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_login) {
            doLogin();
        } else if (id == R.id.tv_user_agreement) {
            IntentController.toWeb(mContext, "1");
        } else if (id == R.id.tv_privacy_agreement) {
            IntentController.toWeb(mContext, "2");
        } else if (id == R.id.checkbox) {
            checkBoxSample.toggle();
            if (checkBoxSample.isChecked()) {
                isCheckBox = true;
            } else {
                isCheckBox = false;
            }
        }
    }

    public void doLogin() {
        String account = etAccount.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (!isCheckBox) {
            ToastUtils.showShort("请同意服务条款");
        } else if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入账号");
        } else {
            if (TextUtils.isEmpty(password)) {
                ToastUtils.showShort("请输入密码");
            } else {
                mPresenter.login(account, password);
            }
        }

    }

    @Override
    public void onBackPressed() {
        App2Utils.exitApp(mContext);
    }


    @Override
    public void loginSuccess(AccountBean accountBean) {
        UserBean userBean = accountBean.getUserDetail();
        SPManager.saveUser(userBean);
        SPManager.saveToken(accountBean.getAccess_token());
        SPManager.saveLastLoginAccount(etAccount.getText().toString().trim());
        SPManager.savePwd(etPassword.getText().toString().trim());
        SPManager.changeLogin(mContext, true);

        IntentController.toMain(mContext, false);
    }

    @Override
    public void loginFailure(String msg) {
        ToastUtils.showShort(msg);
    }

    private ProgressDialog pd;
    private DownloadManager manager;
    private String updateTitle = "发现新版本";

/*
    @Override
    public void updateVersion(String updateInfo, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/abteacherhd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂教师端");//app名称

        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(updateInfo)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();

        // 需要更新，弹出更新窗口
       */
/* UpdateDialog.Builder builder = new UpdateDialog.Builder(LoginActivity.this);
        if (!TextUtils.isEmpty(updateInfo)) {
            builder.setMessage(updateInfo);
        }
        builder.setPositive(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // 如果是强制升级，显示一个进度条
                if (true) {
                    pd = new ProgressDialog(LoginActivity.this);
                    pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pd.setTitle("下载中");
                    pd.setCancelable(false);
                    pd.setMax(100);
                    pd.show();
                }
                UpdateConfiguration configuration = new UpdateConfiguration()
                        .setForcedUpgrade(true)
                        .setOnDownloadListener(new OnDownloadListener() {
                            @Override
                            public void start() {

                            }

                            @Override
                            public void downloading(int max, int progress) {
                                if (pd != null) {
                                    int curr = (int) (progress / (double) max * 100.0);
                                    pd.setProgress(curr);
                                }
                            }

                            @Override
                            public void done(File apk) {

                            }

                            @Override
                            public void cancel() {

                            }

                            @Override
                            public void error(Exception e) {
                                if (pd != null) {
                                    pd.dismiss();
                                }
                            }
                        });
                manager = DownloadManager.getInstance(LoginActivity.this);
                manager.setApkName("拼课堂老师端.apk")
                        .setApkUrl(targetUrl)
                        .setDownloadPath(Environment.getExternalStorageDirectory() + "/AppUpdate")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setConfiguration(configuration)
                        .setAuthorities("com.sdzn.live.tablet.com.sdzn.pkt.teacher.hd.fileprovider")
                        .download();


            }
        });
        CustomDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();*//*

    }
*/

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        if (versionInfoBean != null) {
            int currVersion = com.sdzn.core.utils.App2Utils.getAppVersionCode(App2.get());//获取版本号
            int updateVersion = Integer.parseInt(versionInfoBean.getVersionNum());//线上版本号
            if (updateVersion > currVersion) {
                if (!TextUtils.isEmpty(versionInfoBean.getTargetUrl())) {
                    Updatemethod(versionInfoBean.getDescription(), versionInfoBean.getTargetUrl());
                }
            }
        }
    }

    private void Updatemethod(String description, final String targetUrl) {
        // ui配置
        UiConfig uiConfigforce = new UiConfig();
        uiConfigforce.setUiType(UiType.CUSTOM);
        uiConfigforce.setCustomLayoutId(R.layout.view_update_dialog_custom_pkt);

        // 更新配置
        UpdateConfig forceconfig = new UpdateConfig();
        forceconfig.setAlwaysShowDownLoadDialog(true);
        forceconfig.setForce(true);//是否强制更新
        forceconfig.setCheckWifi(true);//检查wifi
        forceconfig.setShowNotification(false);//是否在通知栏显示
        forceconfig.setNotifyImgRes(R.mipmap.ic_launcher);//通知栏图标
        forceconfig.setApkSavePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/abteacherhd");//apk下载位置
        forceconfig.setApkSaveName("拼课堂教师端");//app名称
        String count = description.replace("|", "\n");


        UpdateAppUtils
                .getInstance()
                .apkUrl(targetUrl)
                .updateTitle(updateTitle)
                .updateContent(count)
                .updateConfig(forceconfig)
                .uiConfig(uiConfigforce)
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {
                        Log.e("testaaa", "onStart");
                    }

                    @Override
                    public void onDownload(int progress) {
                        Log.e("testaaa", "onDownload" + progress);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("testaaa", "onFinish");
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        Log.e("testaaa", "onError" + e.getMessage());
                    }
                }).update();
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {

    }

    @Override
    public void OnUpdateVersionFail(String msg) {

    }

    @Override
    protected void onDestroy() {
        checkverionPresenter.onDestory();
        super.onDestroy();
    }
}
