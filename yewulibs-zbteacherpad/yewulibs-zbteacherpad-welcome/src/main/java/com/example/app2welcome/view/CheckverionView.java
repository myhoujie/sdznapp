package com.example.app2welcome.view;


import com.haier.cellarette.libmvp.mvp.IView;
import com.sdzn.zbteacher.variants.bean.VersionInfoBean;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public interface CheckverionView extends IView {
    void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean);

    void OnUpdateVersionNodata(String bean);

    void OnUpdateVersionFail(String msg);
}
