package com.example.app2welcome.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.app2welcome.IntentController;
import com.example.app2welcome.R;
import com.example.app2welcome.adapter.PageAdapterWithView;
import com.sdzn.core.base.BaseActivity;
import com.sdzn.core.utils.AppManager;
import com.sdzn.core.utils.StatusBarUtil;
import com.sdzn.core.widget.SweetAlertDialog;
import com.sdzn.zbteacher.variants.manager.SPManager;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.internal.CancelAdapt;


/**
 * 描述：
 * adb shell am start -a android.intent.action.VIEW   -c android.intent.category.BROWSABLE  -d "app://cs.znclass.com/com.sdzn.live.tablet.com.sdzn.pkt.teacher.hd.hs.act.welcome?query3=aaaa&query2=45464&query1=pc"
 * - 欢迎页
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class WelcomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener, View.OnClickListener, CancelAdapt {
    ViewPager guideVp;
    LinearLayout ll;
    private TextView tvagreementyes, tvagreementno;
    private TextView tvprivacyagreement, tvuseragreement;
    private final int[] IMGS = {R.mipmap.welcome1, R.mipmap.welcome2};
    private List<View> views;
    private PageAdapterWithView pageAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void onInit(Bundle bundle) {
        intiData();
        initView();
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent != null) {
            String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                Uri appLinkData = appLinkIntent.getData();
                if (appLinkData != null) {
                    String name = appLinkData.getQueryParameter("name");
                    String pwd = appLinkData.getQueryParameter("pwd");
//                    //isGuidance=true失败回退到loginactivity
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("app://cs.znclass.com/" + AppUtils.getAppPackageName() + ".hs.act.login?name=" + name + "&pwd=" + pwd));
//                    startActivity(intent);
                    ToastUtils.showLong("进入到了Welcome"+name+"密码"+pwd);
                }
            }
        }
    }

    @Override
    protected void setStatusBar() {
        StatusBarUtil.setColor(this, Color.WHITE, 0);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initView() {
        guideVp = findViewById(R.id.guide_vp);
        ll = findViewById(R.id.ll);
        tvagreementyes = findViewById(R.id.tv_agreement_yes);
        tvagreementno = findViewById(R.id.tv_agreement_no);
        tvuseragreement = findViewById(R.id.tv_user_agreement);
        tvprivacyagreement = findViewById(R.id.tv_privacy_agreement);

        tvagreementyes.setOnClickListener(this);
        tvagreementno.setOnClickListener(this);
        tvuseragreement.setOnClickListener(this);
        tvprivacyagreement.setOnClickListener(this);

        pageAdapter = new PageAdapterWithView(views);
        guideVp.setAdapter(pageAdapter);
        guideVp.addOnPageChangeListener(this);
        guideVp.setVisibility(View.GONE);
    }

    private void intiData() {
        views = new ArrayList<>();
        initImgs();
    }


    private void initImgs() {
        ImageView imageView = null;
        for (int i = 0; i < IMGS.length; i++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams imageviewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageviewParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageResource(IMGS[i]);
            views.add(imageView);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_user_agreement) {
            IntentController.toWeb(mContext, "1");
        } else if (id == R.id.tv_privacy_agreement) {
            IntentController.toWeb(mContext, "2");
        } else if (id == R.id.tv_agreement_no) {
            showDialog();
        } else if (id == R.id.tv_agreement_yes) {
            ll.setVisibility(View.GONE);
            guideVp.setVisibility(View.VISIBLE);
        }
    }

    private void showDialog() {
        SweetAlertDialog.Builder builder = new SweetAlertDialog.Builder(mContext);
        builder.setMessage("若不同意，则无法继续使用")
                .setPositiveButton("我再考虑下", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {

                    }
                })
                .setNegativeButton("不同意并退出", new SweetAlertDialog.OnDialogClickListener() {
                    @Override
                    public void onClick(Dialog dialog, int which) {
                        SPManager.setFirstEnterApp(mContext);
                        AppManager.getAppManager().appExit();
                    }
                });
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(builder);
        sweetAlertDialog.show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == IMGS.length - 1) {
            views.get(position).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterApp();
                }
            });
        }
    }

    private void enterApp() {
        IntentController.toLogin(mContext);
    }
}
