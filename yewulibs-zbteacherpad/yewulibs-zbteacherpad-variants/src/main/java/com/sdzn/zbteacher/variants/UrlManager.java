package com.sdzn.zbteacher.variants;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UrlManager {
    //测试
    public static final String CCC = "测试";
    public static final String FLAVOR1 = "_114.115.128.225";
    public static final int VERSION_CODE1 = getAppVersionCode();
    public static final String VERSION_NAME1 = getAppVersionName(2);
    public static final String BASE_ADDRESS1 = "http://114.115.128.225:8090/";
    public static final String BUGLY_CHANNEL1 = "本地环境";
    public static final String IMAGE_CACHE1 = "image";
    public static final String ROOT_CACHE1 = "com.sdzn.pkt.teacher.hd";
    public static final String APP_DOWNLOAD1 = "download";
    public static final String AVATAR_CACHE1 = "avatar";
    public static final String CRASH_CACHE1 = "crash";


    //预生产
    public static final String YYY = "预生产";
    public static final String FLAVOR2 = "_znclass";
    public static final int VERSION_CODE2 = getAppVersionCode();
    public static final String VERSION_NAME2 = getAppVersionName(2);
    public static final String BASE_ADDRESS2 = "http://doc.znclass.com";
    public static final String BUGLY_CHANNEL2 = "预生产环境";
    public static final String IMAGE_CACHE2 = "image";
    public static final String ROOT_CACHE2 = "com.sdzn.pkt.teacher.hd";
    public static final String APP_DOWNLOAD2 = "download";
    public static final String AVATAR_CACHE2 = "avatar";
    public static final String CRASH_CACHE2 = "crash";

    //线上
    public static final String OOO = "线上";
    public static final String FLAVOR3 = "_znclass";
    public static final int VERSION_CODE3 = getAppVersionCode();
    public static final String VERSION_NAME3 = getAppVersionName(1);
    public static final String BASE_ADDRESS3 = "http://doc.znclass.com";
    public static final String BUGLY_CHANNEL3 = "线上环境";
    public static final String IMAGE_CACHE3 = "image";
    public static final String ROOT_CACHE3 = "com.sdzn.pkt.teacher.hd";
    public static final String APP_DOWNLOAD3 = "download";
    public static final String AVATAR_CACHE3 = "avatar";
    public static final String CRASH_CACHE3 = "crash";

    public static String versionCount() {
        return "01";
    }

    public String getTinkerVersionCode() {
        //每次发新版本/热更新这个值都要增加
        return "1";
    }

    public static String getAppVersionPre() {
        return "v4.1s_";
    }

    // 获取 version name
    public static String getAppVersionName(int type) {
        String ver = getAppVersionPre();
        String today = new SimpleDateFormat("MMdd").format(new Date());
        return ver + today + '_' + versionCount() + '_' + type;
    }

    public static int getAppVersionCode() {
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date()) + versionCount();
        return Integer.parseInt(today);
    }
}
