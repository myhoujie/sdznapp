package com.sdzn.zbteacher.variants.network;

import com.sdzn.core.network.interceptor.HttpCacheInterceptor;
import com.sdzn.core.network.interceptor.LoggingInterceptor2;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.pkt.teacher.hd.BuildConfig3;
import com.sdzn.zbteacher.variants.network.interceptor.CommonInterceptor;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 描述：
 * - retrofit请求管理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class RestApi {
    public static boolean isDebug = true;

    // create retrofit singleton
    private Retrofit createApiClient(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient(isDebug))
                .build();
    }

    //在访问HttpMethods时创建单例
    private static class SingletonHolder {
        private static final RestApi INSTANCE = new RestApi();

        private SingletonHolder() {
            throw new UnsupportedOperationException("u can't instantiate SingletonHolder...");
        }
    }

    public static synchronized RestApi getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void deBug(boolean isDebug) {
        RestApi.isDebug = isDebug;
    }


    //    public <T> T create(String baseUrl, Class<T> clz) {
//        String serviceUrl = "";
//        if (!TextUtils.isEmpty(baseUrl)) {
//            serviceUrl = baseUrl;
//        } else {
//            try {
//                Field field1 = clz.getField("BASE_URL");
//                serviceUrl = (String) field1.get(clz);
//            } catch (Exception e) {
//                LogUtils.i("exception", e);
//            }
//        }
//
//        return createApiClient(serviceUrl).create(clz);
//    }
//
//    public <T> T create(Class<T> clz) {
//        String serviceUrl = BuildConfig3.BASE_ADDRESS;
//        try {
//            Field field1 = clz.getField("BASE_URL");
//            serviceUrl = (String) field1.get(clz);
//            if (TextUtils.isEmpty(serviceUrl)) {
//                throw new NullPointerException("base_url is null");
//            }
//        } catch (Exception e) {
//            LogUtils.i("exception", e);
//        }
//        return createApiClient(serviceUrl).create(clz);
//    }
    public <T> T create(Class<T> clz) {
        String serviceUrl = BuildConfig3.BASE_ADDRESS;
        return createApiClient(serviceUrl).create(clz);
    }

    /**
     * 创建一个okhttp实例
     *
     * @param debug
     * @return
     */
    private OkHttpClient createOkHttpClient(boolean debug) {
        //缓存
        int size = 1024 * 1024 * 100;
        File cacheFile = new File(App2.get().getCacheDir(), "OkHttpCache");
        Cache cache = new Cache(cacheFile, size);

        return new OkHttpClient.Builder()
                .connectTimeout(12, TimeUnit.SECONDS)
                .writeTimeout(12, TimeUnit.SECONDS)
                .addInterceptor(new CommonInterceptor())
                .addNetworkInterceptor(new HttpCacheInterceptor())
//                .addInterceptor(new HttpLoggingInterceptor().setLevel(
//                        debug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .addInterceptor(new LoggingInterceptor2())
                .cache(cache)
                .build();

    }
}
