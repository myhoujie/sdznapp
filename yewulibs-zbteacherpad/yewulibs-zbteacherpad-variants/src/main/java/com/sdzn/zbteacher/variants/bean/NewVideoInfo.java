package com.sdzn.zbteacher.variants.bean;

public class NewVideoInfo {

    /**
     * videoId : 29333747
     * token : x6csWDTmkjxTfOsrKLRlk3ti8hZ6Htqm8UxP4ocqEVJwbOGZexgGGQ
     */

    private long videoId;
    private String token;
    private String roomId;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public long getVideoId() {
        return videoId;
    }

    public void setVideoId(long videoId) {
        this.videoId = videoId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
