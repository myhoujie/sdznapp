package com.sdzn.zbteacher.variants.bean;

import java.io.Serializable;

/**
 *
 */
public class AccountBean implements Serializable {


    /**
     * access_token : b1d512eb-3075-4492-9a58-bf24e24254c1
     * domain : @com.sdzn.pkt.teacher.hd.com
     * expires_in : 43199
     * openid : 2362
     * refresh_token : 2f4eac05-3792-41d8-9a4a-e50feb49e2bd
     * scope : userProfile
     * token_type : bearer
     * userDetail : {"createTime":"2020-07-15 10:32:36","updateTime":"2020-07-15 10:32:36","id":"2362","name":"董大","sort":1,"schoolId":155,"gender":1,"status":1,"isDelete":0,"deleteTime":"","levelId":1,"education":"","career":"","introduction":"","certificateNo":"","certificateImage":"","avatar":"123456","isShow":0,"isSchoolmaster":0,"account":"31019698","mobile":"","email":"","qq":"","weixin":"","freeznTime":"","classManager":"","password":"123456","schoolName":"香港大学","levelName":"小学","schoolTeacherClasses":[],"isFamous":1,"liveSubjectId":0,"liveSubjectName":""}
     */

    private String access_token;
    private String openid;
    private String refresh_token;
    private String scope;
    private String token_type;
    private UserBean userDetail;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }


    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public UserBean getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserBean userDetail) {
        this.userDetail = userDetail;
    }

}
