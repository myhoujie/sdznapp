package com.sdzn.zbteacher.variants.network.api;

import com.sdzn.core.network.exception.ApiException;
import com.sdzn.core.utils.LogUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.zbteacher.variants.bean.ResultBean;
import com.sdzn.zbteacher.variants.manager.IntentController;
import com.sdzn.zbteacher.variants.manager.SPManager;

import rx.functions.Func1;

/**
 * 描述：
 * - 通用请求返回结果处理
 * 创建人：baoshengxiang
 * 创建时间：2017/10/25
 */
public class ResponseFunc<T> implements Func1<ResultBean<T>, T> {
    private static final int CODE = 401;
    private static final int CODE_LOSE = 2000;

    //此处逻辑根据约定报文进行修改，实现逻辑即可
    @Override
    public T call(ResultBean<T> httpResult) {
        if (0 == httpResult.getCode()) {
            LogUtils.i("LoginResult: " + httpResult.getResult());
            return httpResult.getResult();
        } else {
            if (CODE == httpResult.getCode() || CODE_LOSE == httpResult.getErrorCode()) {//登录失效
                SPManager.changeLogin(App2.get(), false);
                IntentController.toLogins(App2.get());
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());

            } else {
                LogUtils.e("LoginError: " + httpResult.getCode() + ", " + httpResult.getMessage() + ", " + httpResult.getResult());
                throw new ApiException(new Throwable(httpResult.getMessage()), httpResult.getCode());
            }
        }
    }
}