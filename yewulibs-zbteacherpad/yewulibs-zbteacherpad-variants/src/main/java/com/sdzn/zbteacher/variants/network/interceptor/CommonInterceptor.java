package com.sdzn.zbteacher.variants.network.interceptor;

import android.text.TextUtils;

import com.sdzn.zbteacher.variants.manager.SPManager;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 */
public class CommonInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request oldRequest = chain.request();
        String url = oldRequest.url().toString();

        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());
        String token = SPManager.getToken();

        Request.Builder builder = oldRequest.newBuilder();

        if (!TextUtils.isEmpty(token)) {//&&NewApiEqualUtil.isEqual(url)
            builder.addHeader("Authorization", "Bearer " + token);
        }
        Request request = builder.method(oldRequest.method(), oldRequest.body())
                .url(authorizedUrlBuilder.build())
                .build();

        return chain.proceed(request);
    }
}