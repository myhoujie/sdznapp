package com.sdzn.zbteacher.variants.bean;

/**
 * 进入直播间
 */
public class NewLiveInfo {
    /**
     * room_id : 20022570667518
     * user_info : {"userAvatar":"http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png","userName":"41206756-未知-41206756","userNumber":26891,"userType":"0"}
     * sign : fc8163bd30435a6bf1b9f8290f398f06
     */

    private String room_id;
    private UserInfoBean user_info;
    private String sign;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static class UserInfoBean {
        /**
         * userAvatar : http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png
         * userName : 41206756-未知-41206756
         * userNumber : 26891
         * userType : 0
         */

        private String userAvatar;
        private String userName;
        private int userNumber;
        private String userType;

        public String getUserAvatar() {
            return userAvatar;
        }

        public void setUserAvatar(String userAvatar) {
            this.userAvatar = userAvatar;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getUserNumber() {
            return userNumber;
        }

        public void setUserNumber(int userNumber) {
            this.userNumber = userNumber;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}
