package com.sdzn.zbteacher.variants.manager;

import android.content.Context;

import com.sdzn.core.utils.App2Utils;
import com.sdzn.core.utils.EncodeUtils;
import com.sdzn.core.utils.SPUtils;
import com.sdzn.fzx.student.libutils.app.App2;
import com.sdzn.zbteacher.variants.bean.UserBean;

import java.util.List;

/**
 * 描述：
 * - sp文件管理类
 */
public class SPManager {
    private static final String SP_NAME = "AppConfig";

    /**
     * 是否是第一次加载应用
     *
     * @param context
     * @return
     */
    public static boolean isFirst(Context context) {
        String version = (String) SPUtils.get(context, SP_NAME, "version", App2Utils.getVersion());
        boolean isFirst = (boolean) SPUtils.get(context, SP_NAME, "isfirst", true);
        if (App2Utils.getVersion().equals(version) || isFirst) {
            SPUtils.put(context, SP_NAME, "version", App2Utils.getVersion());
            SPUtils.put(context, SP_NAME, "isfirst", false);
        }
        return isFirst;
    }

    public static void setFirstEnterApp(Context context) {
        SPUtils.put(context, SP_NAME, "isfirst", true);
        SPUtils.put(context, SP_NAME, "version", "");
    }

    /**
     * 用户是否已登录
     *
     * @return
     */
    public static boolean isLogin() {
        return (boolean) SPUtils.get(App2.get(), SP_NAME, "islogin", false);
    }


    /**
     * 改变用户登录状态
     *
     * @param context
     * @param loginStatus
     */
    public static void changeLogin(Context context, boolean loginStatus) {
        SPUtils.put(context, SP_NAME, "islogin", loginStatus);
        if (loginStatus) {
            SPUtils.put(context, SP_NAME, "lastLoginTime", System.currentTimeMillis());
        }
        if (!loginStatus) {
            saveToken("");
        }

    }

    /**
     * 改变用户登录状态
     */
    public static void saveToken(String token) {
        SPUtils.put(App2.get(), SP_NAME, "token", token);
    }

    /**
     * 改变用户登录状态
     */
    public static String getToken() {
        return (String) SPUtils.get(App2.get(), SP_NAME, "token", "");
    }


    /**
     * 是否登录超时
     *
     * @param context
     * @return
     */
    public static boolean isLoginOverdue(Context context) {
        long overdueTime = 1000L * 60L * 60L * 24L * 7L;
        long currentTime = System.currentTimeMillis();
        long lastLoginTime = (long) SPUtils.get(context, SP_NAME, "lastLoginTime", currentTime);
        long subTime = currentTime - lastLoginTime;//20天的登录有效期
        return subTime < overdueTime;
    }

    public static boolean autoLogin(Context context) {
        return isLogin() && isLoginOverdue(context);
    }

    /**
     * 存储登录账号
     *
     * @param account
     */
    public static void saveLastLoginAccount(String account) {
        SPUtils.put(App2.get(), SP_NAME, "lastLoginAccount", account);
    }

    /**
     * 获取上次登录账号
     */
    public static String getLastLoginAccount() {
        return (String) SPUtils.get(App2.get(), SP_NAME, "lastLoginAccount", "");
    }

    /**
     * 存储用户信息
     *
     * @param userBean
     */
    public static void saveUser(UserBean userBean) {
        SPUtils.putObject(App2.get(), SP_NAME, "userBean", userBean);
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static UserBean getUser() {
        UserBean userBean = SPUtils.getObject(App2.get(),
                SP_NAME, "userBean", UserBean.class);
        return userBean != null ? userBean : new UserBean();
    }

    /**
     * 存储密码
     *
     * @param password
     */
    public static void savePwd(String password) {
        //md5加密无法还原, 暂用base64
        String b64 = new String(EncodeUtils.base64Encode(password));
        SPUtils.put(App2.get(), getSpName(), "pwd", b64);
//        SPUtils.put(BaseApplication.getInstance(), getSpName(), "pwd", EncryptUtils.encryptMD5ToString(password));
    }

    /**
     * 获取密码
     *
     * @return 密码
     */
    public static String getPwd() {
        String code = (String) SPUtils.get(App2.get(), getSpName(), "pwd", "");
        return new String(EncodeUtils.base64Decode(code));
//        return (String) SPUtils.get(BaseApplication.getInstance(), getSpName(), "pwd", "");
    }


    /**
     * 存储历史搜索记录
     */
    public static void saveSearchStr(List<String> keywords) {
        SPUtils.putList(App2.get(), getSpName(), "searchHistory", keywords);
    }

    /**
     * 获取历史搜索记录
     */
    public static List getSearchStr() {
        return SPUtils.getList(App2.get(), getSpName(), "searchHistory");
    }

    /**
     * 获取历史搜索记录
     */
    public static List getSearchTeacherStr() {
        return SPUtils.getList(App2.get(), getSpName(), "searchTeacherHistory");
    }

    /**
     * 存储历史搜索记录
     */
    public static void saveSearchTeacherStr(List<String> keywords) {
        SPUtils.putList(App2.get(), getSpName(), "searchTeacherHistory", keywords);
    }

//    /**
//     * 保存学段信息
//     */
//    public static void saveSection(SectionBean bean) {
//        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionId", bean.getSectionId());
//        SPUtils.put(BaseApplication.getInstance(), getSpName(), "sectionName", bean.getSectionName());
//    }

    public static void saveSection(int id, String name) {
        SPUtils.put(App2.get(), getSpName(), "sectionId", id);
        SPUtils.put(App2.get(), getSpName(), "sectionName", name);
    }

    /**
     * 获取学段ID, 默认为-1
     */
    public static int getSectionId() {
        return (int) SPUtils.get(App2.get(), getSpName(), "sectionId", -1);
    }

    /**
     * 获取学段名称默认为""
     */
    public static String getSectionName() {
        return (String) SPUtils.get(App2.get(), getSpName(), "sectionName", "");
    }

    /**
     * 移动网络是否可用
     */
    public static boolean getMobileNetSwitch() {
        return (boolean) SPUtils.get(App2.get(), getSpName(), "mobileNetSwitch", true);
    }

    /**
     * 移动网络是否可用
     */
    public static void changeMobileNetSwitch(boolean checked) {
        SPUtils.put(App2.get(), getSpName(), "mobileNetSwitch", checked);
    }


    /**
     * 获取当前用户的sp文件名
     *
     * @return
     */
    private static String getSpName() {
        UserBean userBean = getUser();
        if (userBean != null) {
            return SP_NAME + "_" + userBean.getId();
        } else {
            return SP_NAME;
        }
    }


}
