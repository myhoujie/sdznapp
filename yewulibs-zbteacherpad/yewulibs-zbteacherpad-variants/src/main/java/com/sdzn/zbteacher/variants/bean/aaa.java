package com.sdzn.zbteacher.variants.bean;

import com.google.gson.annotations.SerializedName;

public class aaa {
    /**
     * age : 0
     * bannerUrl :
     * bundlingState : 0
     * classes :
     * createTime : null
     * customerId : 42
     * email :
     * grade : 0
     * invitationCode : 1gWbK834fV
     * isAvalible : 1
     * lastSystemTime : null
     * loginAccount :
     * mobile :
     * msgNum : 0
     * neteaseAccid :
     * neteaseToken :
     * originate : 0
     * password : e10adc3949ba59abbe56e057f20f883e
     * picImg : http://file.znclass.com/6da165a9-6d6c-4654-a6cc-ad3858d2fe06微信截图_20190919111826.png
     * registerFrom :
     * schoolId : 62
     * schoolYear : 0
     * sex : 0
     * showName :
     * studentName :
     * subjectId : 0
     * sysMsgNum : 0
     * teacherId : 784
     * token : 6a0bd24b978646809de60d8c4a236dd0
     * type : 2
     * userId : 19675
     * userName : jiangshi6
     */

    private int age;
    private String bannerUrl;
    private int bundlingState;
    private String classes;
    private Object createTime;
    private int customerId;
    private String email;
    private int grade;
    private String invitationCode;
    private int isAvalible;
    private Object lastSystemTime;
    private String loginAccount;
    private String mobile;
    private int msgNum;
    private String neteaseAccid;
    private String neteaseToken;
    private int originate;
    private String password;
    private String picImg;
    private String registerFrom;
    private int schoolId;
    private int schoolYear;
    private int sex;
    private String showName;
    private String studentName;
    private int subjectId;
    private int sysMsgNum;
    private int teacherId;
    private String token;
    private int type;
    private int userId;
    private String userName;
    /**
     * code : 0
     * errorCode : 0
     * message :
     * result : {"room_id":"20022595816935","user_info":{"userAvatar":"http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png","userName":"jiangshi6-未知-jiangshi6","userNumber":19675,"userType":"1"},"sign":"1c1bf39ced7b1350f421f68060d37e92"}
     * success : true
     */

    private int code;
    private int errorCode;
    private String message;
    private ResultBean result;
    private boolean success;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public int getBundlingState() {
        return bundlingState;
    }

    public void setBundlingState(int bundlingState) {
        this.bundlingState = bundlingState;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public Object getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Object createTime) {
        this.createTime = createTime;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public int getIsAvalible() {
        return isAvalible;
    }

    public void setIsAvalible(int isAvalible) {
        this.isAvalible = isAvalible;
    }

    public Object getLastSystemTime() {
        return lastSystemTime;
    }

    public void setLastSystemTime(Object lastSystemTime) {
        this.lastSystemTime = lastSystemTime;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getMsgNum() {
        return msgNum;
    }

    public void setMsgNum(int msgNum) {
        this.msgNum = msgNum;
    }

    public String getNeteaseAccid() {
        return neteaseAccid;
    }

    public void setNeteaseAccid(String neteaseAccid) {
        this.neteaseAccid = neteaseAccid;
    }

    public String getNeteaseToken() {
        return neteaseToken;
    }

    public void setNeteaseToken(String neteaseToken) {
        this.neteaseToken = neteaseToken;
    }

    public int getOriginate() {
        return originate;
    }

    public void setOriginate(int originate) {
        this.originate = originate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicImg() {
        return picImg;
    }

    public void setPicImg(String picImg) {
        this.picImg = picImg;
    }

    public String getRegisterFrom() {
        return registerFrom;
    }

    public void setRegisterFrom(String registerFrom) {
        this.registerFrom = registerFrom;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(int schoolYear) {
        this.schoolYear = schoolYear;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getSysMsgNum() {
        return sysMsgNum;
    }

    public void setSysMsgNum(int sysMsgNum) {
        this.sysMsgNum = sysMsgNum;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class ResultBean {
        /**
         * room_id : 20022595816935
         * user_info : {"userAvatar":"http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png","userName":"jiangshi6-未知-jiangshi6","userNumber":19675,"userType":"1"}
         * sign : 1c1bf39ced7b1350f421f68060d37e92
         */

        private String room_id;
        private UserInfoBean user_info;
        private String sign;

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public static class UserInfoBean {
            /**
             * userAvatar : http://file.znclass.com/31519d7368b89e0eb3cdeb50e796138.png
             * userName : jiangshi6-未知-jiangshi6
             * userNumber : 19675
             * userType : 1
             */

            private String userAvatar;
            @SerializedName("userName")
            private String userNameX;
            private int userNumber;
            private String userType;

            public String getUserAvatar() {
                return userAvatar;
            }

            public void setUserAvatar(String userAvatar) {
                this.userAvatar = userAvatar;
            }

            public String getUserNameX() {
                return userNameX;
            }

            public void setUserNameX(String userNameX) {
                this.userNameX = userNameX;
            }

            public int getUserNumber() {
                return userNumber;
            }

            public void setUserNumber(int userNumber) {
                this.userNumber = userNumber;
            }

            public String getUserType() {
                return userType;
            }

            public void setUserType(String userType) {
                this.userType = userType;
            }
        }
    }
    /*
    {"age":0,"bannerUrl":"","bundlingState":0,"classes":"","createTime":null,"customerId":42,"email":"","grade":0,"invitationCode":"1gWbK834fV","isAvalible":1,"lastSystemTime":null,"loginAccount":"","mobile":"","msgNum":0,"neteaseAccid":"","neteaseToken":"","originate":0,"password":"e10adc3949ba59abbe56e057f20f883e","picImg":"http://file.znclass.com/6da165a9-6d6c-4654-a6cc-ad3858d2fe06微信截图_20190919111826.png","registerFrom":"","schoolId":62,"schoolYear":0,"sex":0,"showName":"","studentName":"","subjectId":0,"sysMsgNum":0,"teacherId":784,"token":"6a0bd24b978646809de60d8c4a236dd0","type":2,"userId":19675,"userName":"jiangshi6"}

     */

}
