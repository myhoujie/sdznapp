package com.sdzn.zbteacher.variants.bean;

import java.io.Serializable;

public class UserBean implements Serializable {
    private static final long serialVersionUID = -6542134942264304245L;
    private String account;
    private String id;
    private String type;
    private String name;
    private String avatar;
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}