package com.sdzn.pkt.teacher.hd;

import com.sdzn.zbteacher.variants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.CCC;

    public static final String FLAVOR = UrlManager.FLAVOR1;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE1;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME1;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS1;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL1;
    public static final String IMAGE_CACHE = UrlManager.IMAGE_CACHE1;
    public static final String ROOT_CACHE = UrlManager.ROOT_CACHE1;
    public static final String APP_DOWNLOAD = UrlManager.APP_DOWNLOAD1;
    public static final String AVATAR_CACHE = UrlManager.AVATAR_CACHE1;
    public static final String CRASH_CACHE = UrlManager.CRASH_CACHE1;
}
