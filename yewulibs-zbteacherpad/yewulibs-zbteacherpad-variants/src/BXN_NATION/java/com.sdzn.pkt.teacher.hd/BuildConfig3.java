package com.sdzn.pkt.teacher.hd;


import com.sdzn.zbteacher.variants.UrlManager;

public class BuildConfig3 {
    public static final String versionNameConfig = UrlManager.OOO;

    public static final String FLAVOR = UrlManager.FLAVOR3;
    public static final int VERSION_CODE = UrlManager.VERSION_CODE3;
    public static final String VERSION_NAME = UrlManager.VERSION_NAME3;
    public static final String BASE_ADDRESS = UrlManager.BASE_ADDRESS3;
    public static final String BUGLY_CHANNEL = UrlManager.BUGLY_CHANNEL3;
    public static final String IMAGE_CACHE = UrlManager.IMAGE_CACHE3;
    public static final String ROOT_CACHE = UrlManager.ROOT_CACHE3;
    public static final String APP_DOWNLOAD = UrlManager.APP_DOWNLOAD3;
    public static final String AVATAR_CACHE = UrlManager.AVATAR_CACHE3;
    public static final String CRASH_CACHE = UrlManager.CRASH_CACHE3;

}
